//
//  ServiceConnector.swift
//  Connection
//
//  Created by victor salazar on 7/18/17.
//  Copyright © 2017 victor salazar. All rights reserved.
//
import Foundation
class ServiceConnector{
    class func connectToUrl(_ url:String, params:Any? = nil, response:@escaping((Any?, Error?) -> Void)){
        var req = URLRequest(url: URL(string: url)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 120)
        req.httpMethod = "POST"
        req.addValue("application/json", forHTTPHeaderField: "Content-Type")
        if params != nil {
            do{
                if let dic = params as? [String: Any] {
                    req.httpBody = try JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions())
                }else if let data = params as? Data {
                    req.httpBody = data
                }
            }catch let error {
                response(nil, error)
                return
            }
        }
        let task = URLSession.shared.dataTask(with: req){(data:Data?, res:URLResponse?, error:Error?) in
            if error == nil {
                var result:AnyObject? = nil
                var finalError:Error? = nil
                do{
                    result = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as AnyObject
                }catch let jsonError {
                    let stringTemp = String(data: data!, encoding: String.Encoding.utf8)
                    print("stringTemp: \(stringTemp ?? "no")")
                    result = nil
                    finalError = jsonError
                }
                DispatchQueue.main.async(execute: {() in
                    response(result, finalError)
                })
            }else{
                DispatchQueue.main.async(execute: {() in
                    response(nil, error)
                })
            }
        }
        task.resume()
    }
}
