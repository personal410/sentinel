//
//  AvisosCobranzaResponse.swift
//  Sentinel
//
//  Created by Victor Salazar on 2/27/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import Foundation
class AvisosCobranzaResponse {
    var codigoWs:String?
    var arrAvisosCobranza:[[String: Any]] = []
    
    init(_ dic:[String:Any]) {
        codigoWs = dic["CodigoWS"] as? String
        if let arr = dic["SDT_CTAvisoCobranza"] as? [[String:Any]] {
            arrAvisosCobranza = arr
        }
    }
}
