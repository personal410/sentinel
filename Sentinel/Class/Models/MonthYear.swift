//
//  MonthYear.swift
//  ControlGastos
//
//  Created by victor salazar on 21/11/15.
//  Copyright © 2015 victor salazar. All rights reserved.
//
import Foundation
class MonthYear:CustomStringConvertible{
    var month:Int
    var year:Int
    var numberDays:Int
    var firstDay:Int
    var numberLines:Int
    init(date:Date){
        let calendar = Toolbox.getCalendar()
        let dateComps = calendar.dateComponents([.month, .year], from: date)
        month = dateComps.month!
        year = dateComps.year!
        numberDays = calendar.range(of: .day, in: .month, for: date)!.count
        let weedayTemp = calendar.component(.weekday, from: date)
        firstDay = (weedayTemp + 5) % 7 + 1
        numberLines = 6
    }
    func dateTemp() -> Date {
        var dateComps = DateComponents()
        dateComps.day = 1
        dateComps.month = month
        dateComps.year = year
        return Toolbox.getCalendar().date(from: dateComps)!
    }
    var description:String {
        return "<month: \(self.month), year: \(self.year), numberDays: \(self.numberDays), firstDay: \(self.firstDay)>"
    }
}
func ==(left:MonthYear, right: MonthYear) -> Bool {
    return (left.month == right.month) && (left.year == right.year)
}
