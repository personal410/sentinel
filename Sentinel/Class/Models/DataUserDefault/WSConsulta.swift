//
//  WSConsulta.swift
//  Sentinel
//
//  Created by Daniel on 14/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import Alamofire
class WSConsulta: NSObject {
    static var sessionManager:SessionManager?
    static let sharedInstance = WSConsulta()
    static func consultaGratuita(notificacion: String, parametros: [String: Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSConsultaCPT)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = UserClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func consultaBusquedaCoincidencias(notificacion: String, parametros: [String: Any]){
        Alamofire.request("\(PATHS.PATH)\(WEBSERVICES.TERCEROS.RWSBusXNomCT)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = TerceroClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func listaMensajesMenu(notificacion: String, parametros: [String: Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSListadoMensajes)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = UserClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func editarMensajeMenu(notificacion: String, parametros: [String: Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSModificaEstMensajes)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = UserClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    
    static func listaAlertasPersonalesMenu(notificacion: String, parametros: [String: Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSListadoAlePer)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = UserClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func cargaCodigoPromociones(notificacion: String, parametros: [String: Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSSet_Promo)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = UserClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func consultaTercero(notificacion: String, parametros: [String: Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSConsultaCPT)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = TerceroClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func consultaTerceroSERV6(notificacion: String, parametros: [String: Any]){
        Alamofire.request("\(PATHS.PATH)\(WEBSERVICES.CONSULTA.RWS_MSConsultaCPT)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = TerceroClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func consultaTerceroSERV6ESPECIAL(notificacion:String, parametros:[String: Any]){
        Alamofire.request("\(PATHS.PATHWS)\(WEBSERVICES.CONSULTA.RWS_BUSQUEDACPT_SERVICIO)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = TerceroClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func consultaMesDetalle(notificacion: String, parametros: [String: Any]){
        print("consultaMesDetalle")
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSConsultaCPTDeudas)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let currentUser = UserClass(userbean: jsonDictionary)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                }
                break
            case .failure(_):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    static func consultaMesDetalleTercero(notificacion: String, parametros: [String: Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSConsultaCPTDeudas)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = TerceroClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func consultaMesDetalleTerceroSERV6(notificacion: String, parametros: [String: Any]){
        print("RWS_MSConsultaCPTDeudas")
        Alamofire.request("\(PATHS.PATH)\(WEBSERVICES.CONSULTA.RWS_MSConsultaCPTDeudas)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let currentUser = TerceroClass(userbean: jsonDictionary)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                }
                break
            case .failure(_):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    static func generarEnviarPDF(notificacion: String, parametros: [String: Any]){
        if sessionManager == nil {
            let config = URLSessionConfiguration.default
            config.timeoutIntervalForRequest = 300
            config.timeoutIntervalForResource = 300
            sessionManager = Alamofire.SessionManager(configuration: config)
        }
        sessionManager?.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSPDFMiSentinel)", method: .post, parameters: parametros, encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = UserClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(let error):
                    print("error: \(error)")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: error)
                    break
            }
        }
    }
    static func totalIndicadores(notificacion: String, parametros: [String: Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSListadoIndicadores)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = UserClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func getTitular(notificacion: String, parametros: [String: Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSTitulares)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = TerceroClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func getListaMisAlertas(notificacion:String, parametros:[String: Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSSTListadoAlertaPago)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = AlertaClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func crearNewAlerta(notificacion:String, parametros:[String: Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSSTRegistroAlertaPago)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = AlertaClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func modificarAlertaPago(notificacion:String, parametros:[String: Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSSTModificarAlertaPago)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = AlertaClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(let error):
                    print("error: \(error)")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func marcarPagadoAlertaPago(notificacion:String, parametros:[String: Any]){
        print("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSSTActivaPagado)")
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSSTActivaPagado)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = AlertaClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(let error):
                    print("error: \(error)")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    
    static func getDisponiblesEmp(notificacion: String, parametros: [String: Any]){
        print("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWSDatosServicio)")
        Alamofire.request("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWSDatosServicio)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            print("response getDisponiblesEmp")
            switch response.result {
            case .success:
                print("success")
                if let jsonDictionary = response.result.value as? [String : Any] {
                    print("transform")
                    let currentUser = MisServiciosClass(serviciobean: jsonDictionary)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                }
                break
            case .failure(_):
                print("failure")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    
    static func listarLugares(notificacion: String, parametros: [String: Any]){
        
        print("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSSTListadoLugaresCercanos)")
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSSTListadoLugaresCercanos)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let currentUser = AlertaClass(userbean: jsonDictionary)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                }
                break
                
            case .failure(_):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    
    static func registroDeudorRecupera(notificacion: String, parametros: [String: Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.TERCEROS.RWS_MSCTRegistroDeudor)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let currentUser = TerceroClass(userbean: jsonDictionary)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                }
                break
            case .failure(_):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    
    static func obtieneHora(notificacion: String, parametros: [String: Any]){
        Alamofire.request("\(PATHS.PATH)\(WEBSERVICES.TERCEROS.RWS_ObtieneFechaHora)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let currentUser = TerceroClass(userbean: jsonDictionary)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                }
                break
            case .failure(_):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    static func obtieneLimite(notificacion: String, parametros: [String: Any]){
        Alamofire.request("\(PATHS.PATH)\(WEBSERVICES.PROMOCIONES.RWS_PRecPar2)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let currentUser = TerceroClass(userbean: jsonDictionary)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                }
                break
            case .failure(_):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    
    static func conceptoDeuda(notificacion: String, parametros: [String: Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.TERCEROS.RWS_MSCTListadoConDeu)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let currentUser = TerceroClass(userbean: jsonDictionary)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                }
                break
                
            case .failure(_):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    
    static func tipoDocumento(notificacion: String, parametros: [String: Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.TERCEROS.RWS_MSCTListadoDocProb)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = TerceroClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    
    static func consultaVencidosMes(notificacion: String, parametros: [String:String]){
        Alamofire.request("(\(WEBSERVICES.CONSULTA.RWSVencidosMes))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func generarPDFSentinel(notificacion: String, parametros: [String:String]){
        Alamofire.request("(\(WEBSERVICES.CONSULTA.RWSGeneraPDFMiSentinel2))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func enviarPDFSentinel(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.CONSULTA.RWS_AFPEnvCorreoPdf))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                break
                
            }
        }
    }
    //consultaQuienMeMira
    static func consultaQuienMeMira(notificacion: String, parametros: [String: Any]){
        
        print("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSVieronMiReporte)")
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSVieronMiReporte)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let currentUser = UserClass(userbean: jsonDictionary)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                }
                break
                
            case .failure(_):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    static func evaluacionCrediticia(notificacion: String, parametros: [String:String]){
        Alamofire.request("(\(WEBSERVICES.CONSULTA.RWS_EvaCred))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func listadoAlertaPersonal(notificacion: String, parametros: [String:Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSListadoAlePer)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = AlertaClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func listadoServicios(notificacion: String, parametros: [String:Any]){
        
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSCargaCombo)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let currentUser = MisServiciosClass(serviciobean: jsonDictionary)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                }
                break
            case .failure(_):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    
    static func urlEmpresarial(notificacion: String, parametros: [String:Any]){
        
        Alamofire.request("\(PATHS.PATH)\(WEBSERVICES.CONSULTA.Rws_usualeatorio)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let currentUser = MisServiciosClass(serviciobean: jsonDictionary)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                }
                break
            case .failure(_):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    
    static func iniPAYU(notificacion: String, parametros: [String:Any]){
        print("\(PATHS.PATH_PAYU_SENTINEL)")
        Alamofire.request("\(PATHS.PATH_PAYU_SENTINEL)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = MisServiciosClass(serviciobean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    
    static func servicioPorTipoProd(notificacion: String, parametros: [String:Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.REGISTER.RWS_MSServicioxTipoProd)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let currentUser = ServicioTipoProdResponse(jsonDictionary)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                }
                break
            case .failure(_):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    
    static func iniPAYUAS(notificacion: String, parametros: [String:Any]){
        print("\(PATHS.PATH_PAYU_SENTINEL_RESPUESTA)")
        Alamofire.request("\(PATHS.PATH_PAYU_SENTINEL_RESPUESTA)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let currentUser = MisServiciosClass(serviciobean: jsonDictionary)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                }
                break
            case .failure(_):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    
    static func historialConsultas(notificacion: String, parametros: [String:Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSMisConsultas)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = UserClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    
    
    static func historialConsultasServ6(notificacion: String, parametros: [String:Any]){
        print("\(PATHS.PATH)\(WEBSERVICES.TERCEROS.RWS_CNPCargaHistoria)")
        Alamofire.request("\(PATHS.PATH)\(WEBSERVICES.TERCEROS.RWS_CNPCargaHistoria)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = UserClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    
    static func obtenerAvisosCobranza(notificacion:String, parametros:[String:Any]){
        print("\(PATHS.PATH)\(WEBSERVICES.CONSULTA.RWS_MSCTAvisoCobranza)")
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSCTAvisoCobranza)", method: .post, parameters: parametros, encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: AvisosCobranzaResponse(jsonDictionary))
                    }else{
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    }
                    break
                case .failure(let error):
                    print("error: \(error)")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    
    static func consultaGenericaUrl(_ url:String, con parametros:[String: Any], notificar notificacion:String){
        Alamofire.request(url, method: .post, parameters: parametros, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
                case .success(let valor):
                    if let jsonDictionary = valor as? [String : Any] {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: jsonDictionary)
                    }else{
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    }
                    break
                case .failure(let error):
                    print("error: \(error)")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
}
