//
//  WSNotificacion.swift
//  Sentinel
//
//  Created by Daniel on 14/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import Alamofire
class WSNotificacion:NSObject {
    static let sharedInstance = WSNotificacion()
    static func setToken(notificacion: String, parametros: [String:Any]){
        Alamofire.request("(\(WEBSERVICES.NOTIFICACION.RWS_RegistraToken))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                break
            case .failure(let error):
                
                break
            }
        }
    }
    static func setTokenUsuario(notificacion: String, parametros: [String:Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.NOTIFICACION.RWS_MSActTokenUsuario)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let currentUser = NotificationClass(notificationbean: jsonDictionary)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                }
                break
            case .failure(_):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    static func cargaCombo(notificacion: String, parametros: [String:Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)(\(WEBSERVICES.NOTIFICACION.RWS_MSActTokenUsuario))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let currentUser = NotificationClass(notificationbean: jsonDictionary)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                }
                break
            case .failure(_):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
}
