//
//  WSSabio.swift
//  Sentinel
//
//  Created by Daniel on 14/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
import Alamofire
class WSSabio: NSObject {
    static let sharedInstance = WSSabio()
    
    static func clienteConyugeDatos(notificacion: String, parametros: [String:Any]){
        print("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWS_ACConsultaCrediticia)")
        Alamofire.request("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWS_ACConsultaCrediticia)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let currentUser = SabioFinancieroEntidad.init(userbean: jsonDictionary)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                }
                break
            case .failure(let error):
                print("error: \(error)")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    static func cargaDatosSC(notificacion: String, parametros: [String:Any]){
        print("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWS_SabioComercial)")
        Alamofire.request("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWS_SabioComercial)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let currentUser = SabioFinancieroEntidad.init(userbean: jsonDictionary)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                }
                break
            case .failure(let error):
                print("error: \(error)")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    static func cargarDeudasSF(notificacion: String, parametros: [String:Any]){
        print("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWS_DeudasSabioFinanciero)")
        Alamofire.request("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWS_DeudasSabioFinanciero)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = SabioFinancieroEntidad.init(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(let error):
                    print("error: \(error)")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    
    static func cargarDatosEntidadSF(notificacion: String, parametros: [String:Any]){
        print("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWSDatosServicio)")
        Alamofire.request("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWSDatosServicio)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let currentUser = SabioFinancieroEntidad.init(userbean: jsonDictionary)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                }
                break
            case .failure(let error):
                print("error: \(error)")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    
    static func sabioFinancieroSF(notificacion: String, parametros: [String:Any]){
        print("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWS_SabioFinancieroV2)")
        Alamofire.request("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWS_SabioFinancieroV2)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = SabioFinancieroEntidad.init(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(let error):
                    print("error: \(error)")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    
    static func cronogramaSF(notificacion: String, parametros: [String:Any]){
        print("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWS_CronogramaCreditoV2)")
        Alamofire.request("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWS_CronogramaCreditoV2)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = SabioFinancieroEntidad.init(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(let error):
                    print("error: \(error)")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    
    static func enviarCorreoCrono(notificacion: String, parametros: [String:Any]){
        print("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWS_SabioFinancieroCronograma)")
        Alamofire.request("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWS_SabioFinancieroCronograma)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    if let currentUserDictionary = jsonDictionary as? [String : Any] {
                        let currentUser = SabioFinancieroEntidad.init(userbean: currentUserDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                }
                break
            case .failure(let error):
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    
    static func aceptarSF(notificacion: String, parametros: [String:Any]){
        print("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWS_SFAceptoCredito)")
        Alamofire.request("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWS_SFAceptoCredito)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    if let currentUserDictionary = jsonDictionary as? [String : Any] {
                        let currentUser = SabioFinancieroEntidad.init(userbean: currentUserDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                }
                break
            case .failure(let error):
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    
    static func listarProductosSabio(notificacion: String, parametros: [String:Any]){
        print("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWS_ACProductosSabio)")
        Alamofire.request("\(PATHS.PATH)\(WEBSERVICES.SABIO.RWS_ACProductosSabio)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    if let currentUserDictionary = jsonDictionary as? [String : Any] {
                        let currentUser = SabioFinancieroEntidad.init(userbean: currentUserDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                }
                break
            case .failure(let error):
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    
    static func SFobtieneSabio(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.SABIO.RWS_ObtieneSabio))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func SFsabioComercial(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.SABIO.RWS_SabioComercial))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let _):
                
                break
                
            }
        }
    }
    
    static func SFdeudasSabioFinanciero(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.SABIO.RWS_DeudasSabioFinanciero))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func SFsabioFinanciero(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.SABIO.RWS_SabioFinancieroV2))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func SFopcionCredito(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.SABIO.RWS_OpcionCredito))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                break
                
            }
        }
    }
    
    static func SFcronogramaCredito(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.SABIO.RWS_CronogramaCreditoV2))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func SFenviarCronogramaCorreo(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.SABIO.RWS_SabioFinancieroCronograma))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func SFaceptaCredito(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.SABIO.RWS_SFAceptoCredito))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func SFlistaProductos(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.SABIO.RWS_ACProductosSabio))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                break
                
            }
        }
    }
    
    static func SFObtieneClienteConyuge(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.SABIO.RWS_ACConsultaCrediticia))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func devuelveMensajeR(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.RWS_MSErrorWS))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    
}
