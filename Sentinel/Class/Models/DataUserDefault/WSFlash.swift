//
//  WSFlash.swift
//  Sentinel
//
//  Created by Daniel on 14/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit
import Alamofire

class WSFlash: NSObject {
    
    static let sharedInstance = WSFlash()
    
    static func consultarBonificacion(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.FLASH.RWS_ConsultaBonifSentinelFlash))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                break
                
            }
        }
    }
    
    static func registrarTipoConsulta(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.FLASH.RWS_RegTipoConsulta))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                break
                
            }
        }
    }
}
