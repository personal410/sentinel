//
//  WSPromociones.swift
//  Sentinel
//
//  Created by Daniel on 14/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit
import Alamofire

class WSPromociones: NSObject {
    
    static let sharedInstance = WSPromociones()
    
    static func validarPromocion(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.PROMOCIONES.RWS_PRecPar2))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func promocionPorUsuario(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.PROMOCIONES.RWS_GuardarSPTD))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
}
