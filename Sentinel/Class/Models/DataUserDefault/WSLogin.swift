//
//  WSLogin.swift
//  Luhho
//
//  Created by Juan Alberto Carlos Vera on 1/22/18.
//  Copyright © 2018 Daniel Montoya. All rights reserved.
//
import Alamofire
class WSLogin:NSObject {
    static let sharedInstance = WSLogin()
    static func getLogin(notificacion:String, parametros:[String: Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.REGISTER.RWS_MSLogin)", method: .post, parameters: parametros, encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = UserClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    
    static func getVersionApp(notificacion:String, parametros:[String:Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.REGISTER.RWS_MSConsultaVersionApp)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = UserClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    
    static func getObtenerUsuario(notificacion: String, parametros: [String:Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.REGISTER.RWS_MSDatosUsuario)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String: Any] {
                        let currentUser = UserClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func registroPasoUno(notificacion:String, parametros:[String:Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.REGISTER.RWS_MSPaso1App)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = UserClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func registroPasoDos(notificacion:String, parametros:[String:Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.REGISTER.RWS_MSPaso2App)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = UserClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func registroPasoTres(notificacion: String, parametros: [String:Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.REGISTER.RWS_MSPaso3App)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = UserClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func registroPasoCuatro(notificacion: String, parametros: [String:Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.REGISTER.RWS_MSPaso4App)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = UserClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func registroContrato(notificacion:String, parametros:[String:Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.REGISTER.RWS_MSActualizaContratoCG)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = UserClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func cambiarClaveLogin(notificacion: String, parametros: [String:Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.REGISTER.RWS_MSCambiarClave)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = UserClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func generarCorreoSNuevaClaveRY(notificacion:String, parametros:[String:Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.REGISTER.RWS_MSOlvideClave)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = UserClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
}
