//
//  WSMisEmpresas.swift
//  Sentinel
//
//  Created by Daniel on 14/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
import Alamofire
class WSMisEmpresas:NSObject {
    static let sharedInstance = WSMisEmpresas()
    
    static func getMisEmpresas(notificacion: String, parametros: [String: Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.MISEMPRESAS.RWS_MSMisEmpresas)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let currentUser = UserClass(userbean: jsonDictionary)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                }
                break
            case .failure(_):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    static func anadirEmpresa(notificacion: String, parametros: [String:String]){
        Alamofire.request("(\(WEBSERVICES.MISEMPRESAS.RWS_AnadirEmpresas))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                break
            case .failure(let error):
                
                break
            }
        }
    }
    
    static func consultaPorNombres(notificacion: String, parametros: [String:String]){
        Alamofire.request("(\(WEBSERVICES.MISEMPRESAS.RWSBusXNomMisEmp))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                break
                
            }
        }
    }
}
