//
//  WSError.swift
//  Sentinel
//
//  Created by Daniel on 14/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import Alamofire
class WSError: NSObject {
    static let sharedInstance = WSError()
    
    static func getMensajeError(notificacion: String, parametros: [String:Any]){
        print("\(PATHS.PATHSENTINEL)\(WEBSERVICES.RWS_MSErrorWS)")
        Alamofire.request("\(PATHS.PATHSENTINEL)\(WEBSERVICES.RWS_MSErrorWS)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    let resultMensaje = jsonDictionary["ErrorDes"] as! String
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: resultMensaje)
                }
                break
            case .failure(_):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
}
