//
//  WSTerceros.swift
//  Sentinel
//
//  Created by Daniel on 14/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import Alamofire
class WSTerceros: NSObject {
    static let sharedInstance = WSTerceros()
    
    static func consultaServicioUsuario(notificacion: String, parametros: [String:Any]){
        
        Alamofire.request("(\(PATHS.PATHSENTINEL)\(WEBSERVICES.TERCEROS.RWS_ServUsuario))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                if let jsonDictionary = response.result.value as? [String : Any] {
                    if let currentUserDictionary = jsonDictionary as? [String : Any] {
                        let currentUser = TerceroClass(userbean: currentUserDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                }
                break
            case .failure(let error):
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                break
            }
        }
    }
    
    static func consultaDatosServicio(notificacion: String, parametros: [String:Any]){
        Alamofire.request("\(PATHS.PATHSENTINEL)RWS_MSDatosServicio", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: TerceroClass(userbean: jsonDictionary))
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    
    static func consultaPorNombres(notificacion: String, parametros: [String:Any]){
        Alamofire.request("\(PATHS.PATH)\(WEBSERVICES.TERCEROS.RWSBusXNomCT)", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    if let jsonDictionary = response.result.value as? [String : Any] {
                        let currentUser = TerceroClass(userbean: jsonDictionary)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: currentUser)
                    }
                    break
                case .failure(_):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: nil)
                    break
            }
        }
    }
    static func consultaPorDNI(notificacion: String, parametros: [String:String]){
        Alamofire.request("(\(WEBSERVICES.TERCEROS.RWS_AFPMAETIT))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
                case .success:
                    
                    
                    break
                case .failure(let error):

                    break
            }
        }
    }
    
    static func consultaPorNombres(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.TERCEROS.RWSBusXNomCT))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func consultaPorNombres_Tipo6(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.TERCEROS.RWS_BusquedaCPT_Servicio))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func consultaAsignarAlerta(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.TERCEROS.Rws_AsignarAlerta))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func consultaMostrarHistorial(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.TERCEROS.RWS_NHistoConsul))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                break
                
            }
        }
    }
    
    static func consultaMostrarHistorialServicio(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.TERCEROS.RWS_CNPCargaHistoria))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                break
                
            }
        }
    }
    
    static func consultaAlertaTerceros(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.TERCEROS.RWS_ListadoMisAlertas))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                break
                
            }
        }
    }
    
    static func validarCorreo(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.TERCEROS.RWS_ValidaEmail))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func listarSolicitudesAC(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.TERCEROS.RWS_CTAvisoCobranza))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func registrarDeudorCT(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.TERCEROS.RWS_CTRegistroDeudor))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func listarConceptosCT(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.TERCEROS.RWS_CTListadoConDeu))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func listarTipoDocCT(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.TERCEROS.RWS_CTListadoDocProb))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                break
                
            }
        }
    }
    
    static func listarMorosos(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.TERCEROS.RWS_CTListadoNotificaciones))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func obtenerMorosoCT(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.TERCEROS.RWS_CTConsultaNotificacion))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func reportarMorosoCT(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.TERCEROS.RWS_CTReportarMoroso))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                break
                
            }
        }
    }
    
    static func devuelveMensajeR(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.RWS_MSErrorWS))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func yaMePago(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.TERCEROS.RWS_CTActEstRegistro))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
    
    static func discrepancia(notificacion: String, parametros: [String:String]){
        
        Alamofire.request("(\(WEBSERVICES.TERCEROS.RWS_CTRegDiscrep))", method: .post, parameters: parametros,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            switch response.result {
            case .success:
                
                
                break
            case .failure(let error):
                
                
                break
            }
        }
    }
}
