//
//  OriginData.swift
//  Luhho
//
//  Created by Juan Alberto Carlos Vera on 1/22/18.
//  Copyright © 2018 Daniel Montoya. All rights reserved.
//
import SystemConfiguration
class OriginData {
    static let sharedInstance = OriginData()
    
    let endInternetConnection = "endInternetConnection"
    
    func isInternetAvailable() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }   
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    func showAlertConnectivity(){
        let alerta = UIAlertController(title: "Alerta", message: "Problemas de conexión.", preferredStyle: UIAlertControllerStyle.alert)
        alerta.addAction(UIAlertAction(title: "OK",
                                       style: .cancel, handler: nil))
        UIApplication.shared.keyWindow?.rootViewController?.present(alerta, animated: true, completion: nil)
    }

    func validarLogin(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSLogin.getLogin(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func validarContrasena(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSLogin.generarCorreoSNuevaClaveRY(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    func getError(notification:String , parametros:[String: Any]){
        if (self.isInternetAvailable()) {
            return WSError.getMensajeError(notificacion: notification, parametros: parametros)
        }else{
            self.showAlertConnectivity()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func getVersion(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            return WSLogin.getVersionApp(notificacion: notification, parametros: parametros)
            
        }else{
            self.showAlertConnectivity()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func endLoginDetalle(notification :String , parametros: [String: Any]){
        if (self.isInternetAvailable()) {
            return WSLogin.getObtenerUsuario(notificacion: notification, parametros: parametros)
        }else{
            self.showAlertConnectivity()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func endConsultaAlertas(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.listadoAlertaPersonal(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func consultaServicioUsuario(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSTerceros.consultaServicioUsuario(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func consultaDatosServicio(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSTerceros.consultaDatosServicio(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func setTokenUsuario(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSNotificacion.setTokenUsuario(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func listadoMisServicios(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.listadoServicios(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func registroUno(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSLogin.registroPasoUno(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func registroDos(notification :String , parametros: [String: Any]){
        if (self.isInternetAvailable()) {
            return WSLogin.registroPasoDos(notificacion: notification, parametros: parametros)
        }else{
            self.showAlertConnectivity()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func registroTres(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSLogin.registroPasoTres(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func registroCuatro(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSLogin.registroPasoCuatro(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    func registroContrato(notification:String, parametros:[String:Any]){
        if (self.isInternetAvailable()){
            return WSLogin.registroContrato(notificacion:notification, parametros:parametros)
        }else{
            self.showAlertConnectivity()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func cambiarClave(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSLogin.cambiarClaveLogin(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func miReporteDNI(notification :String , parametros: [String: Any]){
        if (self.isInternetAvailable()) {
            return WSConsulta.consultaGratuita(notificacion: notification, parametros: parametros)
        }else{
            self.showAlertConnectivity()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func busquedaTerceroCoincidencias(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.consultaBusquedaCoincidencias(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func listaMensajesMenu(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.listaMensajesMenu(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func estadoMensajeMenu(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.editarMensajeMenu(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func listaAlertasMenu(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.listaAlertasPersonalesMenu(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func codigoPromociones(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.cargaCodigoPromociones(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func miReporteDNITercero(notification:String , parametros:[String: Any]){
        if(self.isInternetAvailable()){
            return WSConsulta.consultaTercero(notificacion: notification, parametros: parametros)
        }else{
            self.showAlertConnectivity()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func miReporteDNITerceroSERV6(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.consultaTerceroSERV6(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }


    func miReporteDNITerceroSERV6ESPECIAL(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.consultaTerceroSERV6ESPECIAL(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func vieronMiReporte(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.consultaQuienMeMira(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func mesDetalle(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.consultaMesDetalle(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }

    func mesDetalleTercero(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.consultaMesDetalleTercero(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func mesDetalleTerceroSERV6(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.consultaMesDetalleTerceroSERV6(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }


    func misEmpresas(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSMisEmpresas.getMisEmpresas(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func iniPAYU(notification:String , parametros:[String: Any]){
        if(self.isInternetAvailable()){
            return WSConsulta.iniPAYU(notificacion: notification, parametros: parametros)
        }else{
            self.showAlertConnectivity()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func iniPAYUAS(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.iniPAYUAS(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func servicioTipoProd(notification:String , parametros:[String: Any]){
        if (self.isInternetAvailable()){
            return WSConsulta.servicioPorTipoProd(notificacion: notification, parametros: parametros)
        }else{
            self.showAlertConnectivity()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func obtieneURLOtrosServicios(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.urlEmpresarial(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }

    func getHistorialEmpresas(notification :String , parametros: [String: Any]){
        if (self.isInternetAvailable()) {
            return WSConsulta.historialConsultas(notificacion: notification, parametros: parametros)
        }else{
            self.showAlertConnectivity()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func getHistorialEmpresasSERV6(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.historialConsultasServ6(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func getConsultaNombre(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSTerceros.consultaPorNombres(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func getClienteConyugeSabio(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSSabio.clienteConyugeDatos(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func getProductosSabio(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSSabio.listarProductosSabio(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func getSabioComercial(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSSabio.cargaDatosSC(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func cargarDeudasSabio(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSSabio.cargarDeudasSF(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func cargarDatosEntidadSabio(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSSabio.cargarDatosEntidadSF(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func iniSabioFinanciero(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSSabio.sabioFinancieroSF(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func getCronograma(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSSabio.cronogramaSF(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func aceptaCreditoSF(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSSabio.aceptarSF(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func enviarCorreo(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSSabio.enviarCorreoCrono(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func generarPDF(notification:String , parametros:[String: Any]){
        if (self.isInternetAvailable()) {
            return WSConsulta.generarEnviarPDF(notificacion: notification, parametros: parametros)
        }else{
            self.showAlertConnectivity()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    func listadoIndicadores(notification:String, parametros:[String:Any]){
        if (self.isInternetAvailable()) {
            return WSConsulta.totalIndicadores(notificacion: notification, parametros: parametros)
        }else{
            self.showAlertConnectivity()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    func nombreTitular(notification:String, parametros:[String: Any]){
        if (self.isInternetAvailable()) {
            
            return WSConsulta.getTitular(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func registroDeudor(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.registroDeudorRecupera(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func devuelveHora(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.obtieneHora(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func devuelveLimite(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.obtieneLimite(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func listarConcepto(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.conceptoDeuda(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func listarTipoDocumento(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.tipoDocumento(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func listarAlertasPagos(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.getListaMisAlertas(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func listarLugares(notification :String , parametros: [String: Any]){
        
        if (self.isInternetAvailable()) {
            
            return WSConsulta.listarLugares(notificacion: notification, parametros: parametros)
            
        }else{
            
            self.showAlertConnectivity()
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    func crearAlertasPagos(notification:String, parametros:[String: Any]){
        if (self.isInternetAvailable()) {
            return WSConsulta.crearNewAlerta(notificacion: notification, parametros: parametros)
        }else{
            self.showAlertConnectivity()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    func modificarAlertaPago(notification:String, parametros:[String: Any]){
        if (self.isInternetAvailable()) {
            return WSConsulta.modificarAlertaPago(notificacion: notification, parametros: parametros)
        }else{
            self.showAlertConnectivity()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    func marcarPagadoAlertaPago(notification:String, parametros:[String: Any]){
        if (self.isInternetAvailable()) {
            return WSConsulta.marcarPagadoAlertaPago(notificacion: notification, parametros: parametros)
        }else{
            self.showAlertConnectivity()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    func listarDisponiblesEmp(notification:String, parametros:[String: Any]){
        if(self.isInternetAvailable()){
            return WSConsulta.getDisponiblesEmp(notificacion: notification, parametros: parametros)
        }else{
            self.showAlertConnectivity()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func obtenerAvisosCobranza(notification:String, parametros:[String: Any]){
        if(self.isInternetAvailable()){
            return WSConsulta.obtenerAvisosCobranza(notificacion: notification, parametros: parametros)
        }else{
            self.showAlertConnectivity()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
    
    func consultarUrl(_ url:String, con parametros:[String: Any], notificar notificacion:String){
        if(self.isInternetAvailable()){
            return WSConsulta.consultaGenericaUrl(url, con: parametros, notificar: notificacion)
        }else{
            self.showAlertConnectivity()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: endInternetConnection), object: nil)
        }
    }
}
