//
//  AlertaClass.swift
//  Sentinel
//
//  Created by Daniel on 27/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import Foundation

struct AlertaPersonalBean {
    let CnhCPTTDoc : String?
    let CnhCPTNroDoc : String?
    let CnhFchPro : String?
    let CnhVarCod : String?
    let CnhDesCor : String?
    let CnhDesLar : String?
}

class AlertaClass{
    
    var CodigoWS : String?
    var CodigoValidacion : String?
    var ServicioActivo : String?
    var SDTListadoAlertasPersonales : NSArray?
    var SDTListadoAlertaPagos : NSArray?
    var dictSDTListadoAlertaPagos: NSDictionary = NSDictionary()
    
    var codigoWs : String?
    var listaAlertas : NSArray?
    var listaAlertasxDia : NSArray?
    
    var descripcionCorta : String?
    var descripcionLarga : String?
    var Fecha : String?
    var tipoDoc : String?
    var numDoc : String?
    var nombreCompleto : String?
    
    var STEntTDoc : String?
    var STEntNDoc : String?
    var STAPFecAle : String?
    var STAPHorAle : String?
    var STEntNom : String?
    var STEntLogo : String?
    var STAPMonto : String?
    var STAPEstPago : String?
    var STAPFecPago : String?
    var STEUCodIdPago : String?
    var STEUNomIdPago : String?
    var STAPFecFor : String?
    var STAPHorFor : String?
    
    var SDTLugaresCercanosST : NSArray?
    
    struct UserKeys {
        static let CodigoWS = "CodigoWS"
        static let codigoWs = "codigoWs"
        static let CodigoValidacion = "CodigoValidacion"
        static let ServicioActivo = "ServicioActivo"
        static let SDTListadoAlertasPersonales = "SDTListadoAlertasPersonales"
        static let listaAlertas = "listaAlertas"
        static let listaAlertasxDia = "listaAlertasxDia"
        static let SDTListadoAlertaPagos = "SDTListadoAlertaPagos"
        static let SDTLugaresCercanosST = "SDTLugaresCercanosST"
    }
    
    init(userbean : [String:Any]) {
        CodigoWS = userbean[UserKeys.CodigoWS] as? String
        codigoWs = userbean[UserKeys.codigoWs
            
            ] as? String
        CodigoValidacion = userbean[UserKeys.CodigoValidacion] as? String
        ServicioActivo = userbean[UserKeys.ServicioActivo] as? String
        SDTListadoAlertasPersonales = userbean[UserKeys.SDTListadoAlertasPersonales] as? NSArray
        listaAlertas = userbean[UserKeys.listaAlertas] as? NSArray
        listaAlertasxDia = userbean[UserKeys.listaAlertasxDia] as? NSArray
        SDTListadoAlertaPagos = userbean[UserKeys.SDTListadoAlertaPagos] as? NSArray
        SDTLugaresCercanosST = userbean[UserKeys.SDTLugaresCercanosST] as? NSArray
    } 
}
