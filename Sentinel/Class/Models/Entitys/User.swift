//
//  User.swift
//  Sentinel
//
//  Created by Daniel on 9/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

struct UserBean : Decodable {
    
    var UseCod1 : String = ""
    var UseCod2 : String = ""
    var GrpObjIni : String = ""
    var GrpSisIni : String = ""
    var mail : String = ""
    var nombre : String = ""
    var flagClaveInicial : String = ""
    var TipoDocumento : String = ""
    var flagLeyoContrato : String = ""
    var diasSesion : String = ""
    var fechaPublica : String = ""
    var urlFoto : String = ""
    var fechaProceso : String = ""
    var codigoWS : String = ""
    var codigoWs : String = ""
    var session : String = ""
    var usuario : String = ""
    var numCel : String = ""
    
    var guidbd : String = ""
    var flag : String = ""
    var solKeyCod : String = ""
    var solClaCodAux : String = ""
    
    var paterno : String = ""
    var materno : String = ""
    var nombres : String = ""
    
    var tipoDoc : String = ""
    var tipoDocDes : String = ""
    var numDoc : String = ""
    
    var tipoDocConsultado : String = ""
    var tipoDocConsultadoDes : String = ""
    var numDocConsultado : String = ""
    var nombreConsultado : String = ""
    var urlFotoConsultado : String = ""
    var paternoConsultado : String = ""
    var maternoConsultado : String = ""
    var nombresConsultado : String = ""
    
    //CONYUGE
    var tipoDocConyuge : String = ""
    var tipoDocConyugeDes : String = ""
    var numDocConyuge : String = ""
    var nombresConyuge : String = ""
    //SabioFinancieroEntidad   cliente
    //SabioFinancieroEntidad   conyuge
    
    //
    
    //SabioComercialEntidad   sabio
    
    var hayConyuge : String = ""
    var esConyuge : String = ""
    //
    
    var vieneDelCliCon : String = ""
    
    var codigoServicioSabio : String = ""
    
    var codigoServicio : String = ""
    
    var tagTab : Int = 0
    
    var pass : String = ""
    
    var alertaPersonal : String = ""
    var sinTab : String = ""
    var opcionAbrir : String = ""
    var fotoRutaInterna : String = ""
    var fotoTerceroRutaInterna : String = ""
    var numDocTercero : String = ""
    var nroAleatorio : String = ""
    
    var modulo : String = ""
    //MisServiciosBean  servicioFlash
    //MisServiciosBean  servicioRecupera
    //BonificacionFlashBean  bonificacionFlash
    
    var Servicio : String = ""
    var cantC : String = ""
    var CantP : String = ""
    var cantT : String = ""
    var cantO : String = ""
    var cantME : String = ""
    var CantConsC : String = ""
    var CantConsP : String = ""
    var CantConsT : String = ""
    var CantConsO : String = ""
    var CantRegME : String = ""
    var BanMyPE : String = ""
    
    var UseCorTra : String = ""
    var UseNomCo : String = ""
    var UseFlgClave : String = ""
    var UseTipDoc : String = ""
    var UseFlgContratoCG : String = ""
    var UseSesionDia : String = ""
    var FchPub : String = ""
    var foto : String = ""
    var CnhFchPro : String = ""
    var CodigoWs : String = ""
    
    var tipoServicioBono : String = ""
    var tipo : String = ""
    
    //Consulta  consultaTercero
    
    var tipoConsulta : String = ""
    //....//  ContadorBean  contadores
    
    var representanteLegal : String = ""
    var dniRepresentanteLegal : String = ""
    
    
    var abrirDesdeHome : String = ""
    
    var versionApp : String = ""
    
    // Viene del Switch o del poptoView
    
    var popSwitch : String = ""
    
    var vieneDelUltimo : String = ""
    
    //Alertas
    //AlertaBean  alerta
    var alertaIndice : String = ""
    //var listaAlertas : [AlertaBean]
    //@property (nonatomic, strong) NSMutableArray <AlertaBean  >  listaAlertas ........ igual al de arriba
    
    var cargarMisAlertas : Bool = true
    var cargarCalendario : Bool = true
    var tituloAlertaxDia : String = ""
    var totalAlertaxDia : Double = 0.0
    var totalAlertaxSemana : Double = 0.0
    var calendarioMesActual : String = ""
    
    //Datos a guardar de la tabla
    var porcentaje : String = ""
    var nuevoSaldo : String = ""
    var montoCancelacion : String = ""
    
    //Para avisos cobranza
    var cantAC : String = ""
    var revisadoAC : String = ""
    
    //Recupera
    var EmiFecPago : String = ""
    var VenFecPago : String = ""
    var LimFecPago : String = ""
    
    var sumaContador : Int = 0

}

class User: NSObject {
    
    var UseCod1 : String = ""
    var UseCod2 : String = ""
    var GrpObjIni : String = ""
    var GrpSisIni : String = ""
    var mail : String = ""
    var nombre : String = ""
    var flagClaveInicial : String = ""
    var TipoDocumento : String = ""
    var flagLeyoContrato : String = ""
    var diasSesion : String = ""
    var fechaPublica : String = ""
    var urlFoto : String = ""
    var fechaProceso : String = ""
    var codigoWS : String = ""
    var codigoWs : String = ""
    var session : String = ""
    var usuario : String = ""
    var numCel : String = ""
    
    var guidbd : String = ""
    var flag : String = ""
    var solKeyCod : String = ""
    var solClaCodAux : String = ""
    
    var paterno : String = ""
    var materno : String = ""
    var nombres : String = ""
    
    var tipoDoc : String = ""
    var tipoDocDes : String = ""
    var numDoc : String = ""
    
    var tipoDocConsultado : String = ""
    var tipoDocConsultadoDes : String = ""
    var numDocConsultado : String = ""
    var nombreConsultado : String = ""
    var urlFotoConsultado : String = ""
    var paternoConsultado : String = ""
    var maternoConsultado : String = ""
    var nombresConsultado : String = ""
    
    //CONYUGE
    var tipoDocConyuge : String = ""
    var tipoDocConyugeDes : String = ""
    var numDocConyuge : String = ""
    var nombresConyuge : String = ""
    //SabioFinancieroEntidad   cliente
    //SabioFinancieroEntidad   conyuge
    
    //
    
    //SabioComercialEntidad   sabio
    
    var hayConyuge : String = ""
    var esConyuge : String = ""
    //
    
    var vieneDelCliCon : String = ""
    
    var codigoServicioSabio : String = ""
    
    var codigoServicio : String = ""
    
    var tagTab : Int = 0
    
    var pass : String = ""
    
    var alertaPersonal : String = ""
    var sinTab : String = ""
    var opcionAbrir : String = ""
    var fotoRutaInterna : String = ""
    var fotoTerceroRutaInterna : String = ""
    var numDocTercero : String = ""
    var nroAleatorio : String = ""
    
    var modulo : String = ""
    //MisServiciosBean  servicioFlash
    //MisServiciosBean  servicioRecupera
    //BonificacionFlashBean  bonificacionFlash
    
    var Servicio : String = ""
    var cantC : String = ""
    var CantP : String = ""
    var cantT : String = ""
    var cantO : String = ""
    var cantME : String = ""
    var CantConsC : String = ""
    var CantConsP : String = ""
    var CantConsT : String = ""
    var CantConsO : String = ""
    var CantRegME : String = ""
    var BanMyPE : String = ""
    
    var UseCorTra : String = ""
    var UseNomCo : String = ""
    var UseFlgClave : String = ""
    var UseTipDoc : String = ""
    var UseFlgContratoCG : String = ""
    var UseSesionDia : String = ""
    var FchPub : String = ""
    var foto : String = ""
    var CnhFchPro : String = ""
    var CodigoWs : String = ""
    
    var tipoServicioBono : String = ""
    var tipo : String = ""
    
    //Consulta  consultaTercero
    
    var tipoConsulta : String = ""
    //....//  ContadorBean  contadores
    
    var representanteLegal : String = ""
    var dniRepresentanteLegal : String = ""
    
    
    var abrirDesdeHome : String = ""
    
    var versionApp : String = ""
    
    // Viene del Switch o del poptoView
    
    var popSwitch : String = ""
    
    var vieneDelUltimo : String = ""
    
    //Alertas
    //AlertaBean  alerta
    var alertaIndice : String = ""
    //var listaAlertas : [AlertaBean]
    //@property (nonatomic, strong) NSMutableArray <AlertaBean  >  listaAlertas ........ igual al de arriba
    
    var cargarMisAlertas : Bool = true
    var cargarCalendario : Bool = true
    var tituloAlertaxDia : String = ""
    var totalAlertaxDia : Double = 0.0
    var totalAlertaxSemana : Double = 0.0
    var calendarioMesActual : String = ""
    
    //Datos a guardar de la tabla
    var porcentaje : String = ""
    var nuevoSaldo : String = ""
    var montoCancelacion : String = ""
    
    //Para avisos cobranza
    var cantAC : String = ""
    var revisadoAC : String = ""
    
    //Recupera
    var EmiFecPago : String = ""
    var VenFecPago : String = ""
    var LimFecPago : String = ""
    
    var sumaContador : Int = 0

}

