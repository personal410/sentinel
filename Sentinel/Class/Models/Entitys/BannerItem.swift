//
//  BannerItem.swift
//  Sentinel
//
//  Created by Victor Salazar on 5/16/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import UIKit
class BannerItem{
    var image:UIImage
    var titulo = ""
    var detalle = ""
    init(_ img:UIImage, _ tit:String, _ det:String){
        image = img
        titulo = tit
        detalle = det
    }
    
    class func getItems() -> [BannerItem] {
        var arrBannerItem:[BannerItem] = []
        let premium = UserGlobalData.sharedInstance.userGlobal!.EsPremium!
        arrBannerItem.append(BannerItem(UIImage(named: "bannerRecupera")!, "Recupera", "¡Recupera tu dinero!"))
        if premium != "S" {
            arrBannerItem.append(BannerItem(UIImage(named: "demo01")!, "Sentinel\nPremium", "¡Obtén mucho más!"))
        }
        arrBannerItem.append(BannerItem(UIImage(named: "terceros")!, "Reporte de Deudas\nPersonas / Empresas", "Consulta el Reporte de Deudas Detallado de \nlas personas con las que haces negocios."))
        arrBannerItem.append(BannerItem(UIImage(named: "pyme1")!, "Sentinel\nPyME", "Tu negocio crece en tus manos."))
        return arrBannerItem
    }
}
