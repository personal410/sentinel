//
//  TerceroClass.swift
//  Sentinel
//
//  Created by Daniel on 27/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import Foundation

struct TerceroSentinelBean {
    let DispDet : String?
    let DispRes : String?
    let SalFlash : String?
}

struct TerceroDatosBean {
    let AFSerCod : String?
    let AFSerTip : String?
    
    let AFSerGraDis : String?
    let AFSerGraTCon : String?
    let AFSerGraCon : String?
    let AFSerCPTDis : String?
    let AFSerCPTNDisp : String?
    let afserSBSTipDoc : String?
    let afserSBSNumDoc : String?
    let afserSBSNomAbr : String?
    
    /*
     @property (nonatomic,strong) NSString*servicioValidoAlerta;
     @property (nonatomic,strong) NSString*codigoServicioAlerta;
     @property (nonatomic,strong) NSString*CodigoValidacionAlerta;
     
     
     @property (nonatomic,strong) NSString*teceroConsultado;//S:si - N:no;
     @property (nonatomic,strong) NSString*teceroBusqueda;//S:si - N:no;
     @property (nonatomic,strong) NSString*tipoConsulta; //G gratuita, X Tercero
    */
    
}

class TerceroClass {
    
    var CodigoWS : String?
    var SaldoServicio : NSDictionary?
    
    var CodigoValidacion : String?
    var codigoWS : String?
    var AFSerCPTDis : String? //CantidadDisponible
    var AFSerCod : String? // Codigo servicio
    var ServicioValido : String?
    var SDTDatosservicio : TerceroDatosBean?
    
    //--
    var codigoServicio : String?
    var cantidadDisponible : String?
    //--
    var servicioValidoAlerta : String?
    var codigoServicioAlerta : String?
    var CodigoValidacionAlerta : String?
    
    var teceroConsultado : String? //S:SI, N:NO
    var teceroBusqueda : String? //S:si - N:no;
    var tipoConsulta : String? //G gratuita, X tercero
    
    //--> De la estructura, Datos sueltos
    
    var AFSerGraDis : String?
    var AFSerGraDisAlerta : String?
    var AFSerCPTDisAlerta : String?
    
    //RWS_MSDatosServicio
    var DispDet : String?
    var DispRes : String?
    var SalFlash : String?
    
    //ConsultaReporteTercero
    var InfoTitular : [String : Any]?
    
    //Almacenando Datos de Tercero x Detalle / Resumida
    var documentoTercero : String?
    var tipoDocumentoTercero : String?
    //DetalleMes
    var SDT_MSDeudaTitular : [String : Any]?
    //
    var mesID : String?
    //
    var userid : String?
    var tipodoc : String?
    var numdoc : String?
    var descripcion : String?
    var adicional : String?
    var TipoDocAbr : String?
    
    var SDTBusAlf : NSArray?
    var AFMVApeMat : String?
    var AFMVApePat : String?
    var AFMVNomRaz : String?
    var AFMVNom : String?
    
    //SabioFinanciero
    var objSabioEntidad : SabioFinancieroEntidad?
    var objSabioEntidadConyuge : SabioFinancieroEntidad?
    var tipoDocClienteSabio : String?
    var tipoDocConyugeSabio : String?
    var numClienteSabio : String?
    var numConyugeSabio : String?
    var hayConyuge : String?
    //Listado Productos
    var Productos : NSArray?
    //Cancela otros creditos
    var CancelaOC: String?
    //SabioFinanciero
    var SDTSabioFinanciero: NSDictionary?
    
    //SabioComercial
    var SabioComercial: NSDictionary?
    //Titular
    var ApePat: String?
    var ApeMat: String?
    var NombreRazSoc : String?
    var Nombres : String?
    
    //Recupera
    var xCTNCorr : Int?
    var SDT_CTConceptosDeuda : NSArray?
    var SDT_CTDocProb : NSArray?
    var venFecPago : String = ""
    var EmiFecPago : String = ""
    var LimFecPago : String = ""
    var cantidadDias : String = ""
    var concepto : String = ""
    
    var Fecha : String?
    var Hora : String?
    
    var ParFlg : String?
    var ParCon : String?
    
    var FechaReporte : String = ""
    
    var tipoReporte = ""
    
struct UserKeys {
    static let CodigoWS = "CodigoWS"
    static let SaldoServicio = "SaldoServicio"
    static let CodigoValidacion = "CodigoValidacion"
    static let codigoWS = "codigoWS"
    static let AFSerCPTDis = "AFSerCPTDis"
    static let AFSerCod = "AFSerCod"
    static let ServicioValido = "ServicioValido"
    static let SDTDatosservicio = "SDTDatosservicio"
    static let InfoTitular = "InfoTitular"
    static let SDT_MSDeudaTitular = "SDT_MSDeudaTitular"
    //
    static let userid = "userid"
    static let tipodoc = "tipodoc"
    static let numdoc = "numdoc"
    static let descripcion = "descripcion"
    static let adicional = "adicional"
    static let TipoDocAbr = "TipoDocAbr"
    static let SDTBusAlf = "SDTBusAlf"
    static let AFMVApeMat = "AFMVApeMat"
    static let AFMVApePat = "AFMVApePat"
    static let AFMVNomRaz = "AFMVNomRaz"
    static let AFMVNom = "AFMVNom"
    
    static let Productos = "Productos"
    //
    static let ApePat = "ApePat"
    static let ApeMat = "ApeMat"
    static let NombreRazSoc = "NombreRazSoc"
    static let Nombres = "Nombres"
    static let xCTNCorr = "xCTNCorr"
    static let SDT_CTConceptosDeuda = "SDT_CTConceptosDeuda"
    static let SDT_CTDocProb = "SDT_CTDocProb"
    
    static let Fecha = "Fecha"
    static let Hora  = "Hora"
    
    static let ParFlg = "ParFlg"
    static let ParCon = "ParCon"
}

init(userbean : [String:Any]) {
    CodigoWS = userbean[UserKeys.CodigoWS] as? String
    SaldoServicio = userbean[UserKeys.SaldoServicio] as? NSDictionary
    CodigoValidacion = userbean[UserKeys.CodigoValidacion] as? String
    codigoWS = userbean[UserKeys.codigoWS] as? String
    AFSerCPTDis = userbean[UserKeys.AFSerCPTDis] as? String
    AFSerCod = userbean[UserKeys.AFSerCod] as? String
    ServicioValido = userbean[UserKeys.ServicioValido] as? String
    SDTDatosservicio = userbean[UserKeys.SDTDatosservicio] as? TerceroDatosBean
    //
    InfoTitular = userbean[UserKeys.InfoTitular] as? [String : Any]
    SDT_MSDeudaTitular = (userbean[UserKeys.SDT_MSDeudaTitular] as? [String : Any]?)!
    //
    userid = userbean[UserKeys.userid] as? String
    tipodoc = userbean[UserKeys.tipodoc] as? String
    numdoc = userbean[UserKeys.numdoc] as? String
    descripcion = userbean[UserKeys.descripcion] as? String
    adicional = userbean[UserKeys.adicional] as? String
    TipoDocAbr = userbean[UserKeys.TipoDocAbr] as? String
    SDTBusAlf = userbean[UserKeys.SDTBusAlf] as? NSArray
    AFMVApeMat = userbean[UserKeys.AFMVApeMat] as? String
    AFMVApePat = userbean[UserKeys.AFMVApePat] as? String
    AFMVNomRaz = userbean[UserKeys.AFMVNomRaz] as? String
    AFMVNom = userbean[UserKeys.AFMVNom] as? String
    Productos = userbean[UserKeys.Productos] as? NSArray
    //
    ApePat = userbean[UserKeys.ApePat] as? String
    ApeMat = userbean[UserKeys.ApeMat] as? String
    NombreRazSoc = userbean[UserKeys.NombreRazSoc] as? String
    Nombres = userbean[UserKeys.Nombres] as? String
    xCTNCorr = userbean[UserKeys.xCTNCorr] as? Int
    SDT_CTConceptosDeuda = userbean[UserKeys.SDT_CTConceptosDeuda] as? NSArray
    SDT_CTDocProb = userbean[UserKeys.SDT_CTDocProb] as? NSArray
    Fecha = userbean[UserKeys.Fecha] as? String
    Hora = userbean[UserKeys.Hora] as? String
    ParFlg = userbean[UserKeys.ParFlg] as? String
    ParCon = userbean[UserKeys.ParCon] as? String
}
}
