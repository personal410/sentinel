//
//  MisServiciosClass.swift
//  Sentinel
//
//  Created by Daniel on 28/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import Foundation

struct MiServicioBean {
    let AuxCodigo : String?
    let AuxDescri : String?
    let AuxCoop : String?
    let AuxTip : String?
    let AuxEstado : String?
    let AuxAsomif : String?
    let AuxVerEvaCre : String?
    let AuxSabio : String?
}

struct MiNuevoServicioBean {
    let NroServicio : String?
    let DscServicio : String?
    let FlgCoop : String?
    let TipServ : String?
    let FlgSabio : String?
}


class MisServiciosClass {
    
    var codigoWs : String?
    var CodigoWS : String?
    var ServiciosUsuario : NSArray?
    var ServiciosTipo : [MiObejetoServicio]?
    //->
    var AuxCodigo : String?
    var AuxDescri : String?
    var AuxCoop : String?
    var AuxTip : String?
    var AuxEstado : String?
    var AuxAsomif : String?
    var AuxVerEvaCre : String?
    var AuxSabio : String?
    // URL PATH
    var NroAleatorio : String?
    var UseCod : String?
    var codigoWS : String?
    //->
    var NroServicio : String?
    var DscServicio : String?
    var FlgCoop : String?
    var TipServ : String?
    var FlgSabio : String?
    
    var SDTDatosservicio : NSDictionary?
    
    var referenceCode : String?// = 7420771318121733510001
    
    var ErrorWS : String?
    var CodigoError : String?
    var ErrDescription : String?
    
    struct UserKeys {
        static let CodigoWS = "CodigoWS"
        static let codigoWs = "codigoWs"
        static let ServiciosUsuario = "ServiciosUsuario"
        static let NroAleatorio = "NroAleatorio"
        static let UseCod = "UseCod"
        static let codigoWS = "codigoWS"
        static let SDTDatosservicio = "SDTDatosservicio"
        static let referenceCode = "referenceCode"
        static let ErrorWS = "ErrorWS"
        static let CodigoError = "CodigoError"
        static let ErrDescription = "ErrDescription"
    }
    
    init(serviciobean : [String: Any]) {
        CodigoWS = serviciobean[UserKeys.CodigoWS] as? String
        codigoWs = serviciobean[UserKeys.codigoWs] as? String
        ServiciosUsuario = serviciobean[UserKeys.ServiciosUsuario] as? NSArray
        NroAleatorio = serviciobean[UserKeys.NroAleatorio] as? String
        UseCod = serviciobean[UserKeys.UseCod] as? String
        codigoWS = serviciobean[UserKeys.codigoWS] as? String
        SDTDatosservicio = serviciobean[UserKeys.SDTDatosservicio] as? NSDictionary
        referenceCode = serviciobean[UserKeys.referenceCode]  as? String
        ErrorWS = serviciobean[UserKeys.ErrorWS]  as? String
        CodigoError = serviciobean[UserKeys.CodigoError]  as? String
        ErrDescription = serviciobean[UserKeys.ErrDescription]  as? String
    }
}










