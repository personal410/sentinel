//
//  MenuTabBean.swift
//  Sentinel
//
//  Created by Daniel on 5/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import Foundation
class MenuTabBean: NSObject {
    var titulo = ""
    var imagen = ""
    var detalleCompleto = ""
    var indPantalla = 0
    var cantNotificaciones = 0
}
