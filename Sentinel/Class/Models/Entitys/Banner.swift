//
//  Shoe.swift
//  Nike+Research
//
//  Created by Duc Tran on 3/19/17.
//  Copyright © 2017 Developers Academy. All rights reserved.
//
import UIKit
class Banner{
    var images: [UIImage] = []
    var titulos: [String] = []
    var detalles: [String] = []
    
    class func fetchBanner() -> [Banner] {
        var banners = [Banner]()
        let banner = Banner()
        // 1
        
        let userData = UserGlobalData.sharedInstance.userGlobal!
        let premium : String = userData.EsPremium!
        
        if premium == "S"{
            var bannerImage1 = [UIImage]()
            bannerImage1.append(UIImage(named: "terceros")!)
            bannerImage1.append(UIImage(named: "pyme1")!)
            banner.images = bannerImage1
            
            var bannerTitulos = [String]()
            bannerTitulos = ["Reporte de Deudas \nPersonas / Empresas","Sentinel \nPyME"]
            banner.titulos = bannerTitulos
            
            var bannerDetalles = [String]()
            bannerDetalles = ["Consulta el Reporte de Deudas Detallado de \nlas personas con las que haces negocios.","Tu negocio crece en tus manos."]
            banner.detalles = bannerDetalles
            
            banners.append(banner)
        }else{
            var bannerImage1 = [UIImage]()
            bannerImage1.append(UIImage(named: "demo01")!)
            bannerImage1.append(UIImage(named: "terceros")!)
            bannerImage1.append(UIImage(named: "pyme1")!)
            banner.images = bannerImage1
            
            var bannerTitulos = [String]()
            bannerTitulos = ["Sentinel \nPremium","Reporte de Deudas \nPersonas / Empresas","Sentinel \nPyME"]
            banner.titulos = bannerTitulos
            
            var bannerDetalles = [String]()
            bannerDetalles = ["¡Obtén mucho más!","Consulta el Reporte de Deudas Detallado de \nlas personas con las que haces negocios.","Tu negocio crece en tus manos."]
            banner.detalles = bannerDetalles
            
            banners.append(banner)
        }
        return banners
    }
}
