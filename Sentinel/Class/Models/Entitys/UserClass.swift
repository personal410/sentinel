//
//  UserClass.swift
//  Sentinel
//
//  Created by Daniel on 21/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import Foundation

struct SDTCPTMS {
    let TipoDocumento : String?
    let NroDocumento : String?
    let FechaNacimiento : String?
    let Nombre : String?
    let ApePaterno : String?
    let ApeMaterno : String?
    let NomRazSoc : String?
    let Semaforos : String?
    let Rectificaciones : String?
    let FechaProceso : String?
    let Nota : String?
    let SemActual : String?
    let DeudaTotal : String?
    let DscDeuda : String?
    let SemMit : String?
}

class UserClass
{
    //Login
    var Nombres : String?
    var SemAct: String?
    var FlgClave: String?
    var TDocusu: String?
    var tipoDocumento : String? //Tipo Documento Completo
    var FlgContratoCG: String?
    var MailUsuario: String?
    var MisProductos: NSMutableArray?
    var Foto: String?
    var FotoRutaInterna: String? //Ruta guardada de la foto
    var SesionId: String?
    var Sexo: String?
    var EsPremium : String?
    var NroSerCT: String?
    //
    var NroSerFl: String?
    var NroDocRel: String?
    var SemActRel: String?
    //
    var CodigoWS: String?
    var codigoWS: String?
    var codigoWs: String?
    var NombreConsultado: String? //Consultado
    var ApellidoPaternoConsultado: String? //Consultado
    var ApellidoMaternoConsultado: String? //Consultado
    //getVersion
    var CCOVerId: String?
    //getObtenerLogin
    var ApePat: String?
    var ApeMat: String?
    var Mail: String?
    var codigoValidacion: String?
    var Celular: String?
    //endConsultaAlertas
    var NumDocumento : String?
    var ServicioActivo : String?
    var user : String?
    //RegistroPasoUno
    var MensajeRespuesta : String?
    //error
    var ErrorDes : String?
    //cambiar clave
    var Codigo : String?
    var EsValido : String?
    var Mensaje : String?
    //getMiReporteDNI -> RWS_MSConsultaCPT
    var InfoTitular : [String : Any]?
    var InfoTitularEmpresas : [String : Any]?
    //MisEmpresas
    var MisEmpresas : NSArray?
    var objMisEmpresas : NSDictionary?
    //AYUDAS
    var tabIndicador = 0 // 1,2,0,4,5
    //vieronMiReporte
    var VieronMiReporte : NSArray?
    var VieronMiReporteObj : NSDictionary = NSDictionary()
    //detalleMes
    var SDT_MSDeudaTitular : [String : Any]?
    //mesesUltimos
    var mesesArreglo : NSMutableArray = NSMutableArray()
    //Mis Reportes
    var SDTNConRap : NSArray?
    
    var vieneDelUltimoSF : String?
    var documentoXPDF : String?
    var documentoTipoXPDF : String?
    
    //Listado Indicadores
    var Indicadores : NSArray?
    
    //Fecha Pafo
    var fechaPago : String = ""
    var fechaDia : String = ""
    var horaPago : String = ""
    
    //Contador Disponibles Empresarial
    var AFSerCPTNDisp : String = ""
    var SDTConRap : NSArray?
    var Cont : Int?
    
    //Mensajes del usuario
    var SDTMensajes : NSArray?
    
    //Viene de empresas
    var vieneEmpresas : Bool = false
    var tEmpresa : String = ""
    var nEmpresa : String = ""
    var haciaEmpresas : Bool = false
    
    //Listado Alertas Personales
    var SDTListadoAlertasPersonales : NSArray?
    
    //estado mensajes
    var CodigoValidacion : String?
    
    //promociones
    var MsjActivacion : String?
    
    var urlPDF:String?
    
    //FechaReporte
    var FechaReporte  = ""
    
    //SesionCerrada
    var sesionActive : Bool = false
    
    var flagRecupera = false
    
    struct UserKeys {
        static let Nombres = "Nombres"
        static let SemAct = "SemAct"
        static let FlgClave = "FlgClave"
        static let TDocusu = "TDocusu"
        static let FlgContratoCG = "FlgContratoCG"
        static let MailUsuario = "MailUsuario"
        static let MisProductos = "MisProductos"
        static let Foto = "Foto"
        static let SesionId = "SesionId"
        static let Sexo = "Sexo"
        static let EsPremium = "EsPremium"
        static let NroSerCT = "NroSerCT"
        //
        static let NroSerFl = "NroSerFl"
        static let NroDocRel = "NroDocRel"
        static let SemActRel = "SemActRel"
        //
        static let CodigoWS = "CodigoWS"
        static let codigoWS = "codigoWS"
        //--
        static let CCOVerId = "CCOVerId"
        //-
        static let ApePat = "UsuApePat"
        static let ApeMat = "UsuApeMat"
        //static let Nombres = "Nombres"
        static let Mail = "Mail"
        static let codigoValidacion = "codigoValidacion"
        static let Celular = "Celular"
        //
        static let MensajeRespuesta = "MensajeRespuesta"
        //
        static let ErrorDes = "ErrorDes"
        //
        static let Codigo = "Codigo"
        static let EsValido = "EsValido"
        static let Mensaje = "Mensaje"
        //
        static let InfoTitular = "InfoTitular"
        static let InfoTitularEmpresas = "InfoTitularEmpresas"
        //
        static let MisEmpresas = "MisEmpresas"
        static let objMisEmpresas = "objMisEmpresas"
        //
        static let VieronMiReporte = "VieronMiReporte"
        //
        static let SDT_MSDeudaTitular = "SDT_MSDeudaTitular"
        static let SDTNConRap = "SDTNConRap"
        static let Indicadores = "Indicadores"
        static let SDTConRap = "SDTConRap"
        static let Cont = "Cont"
        static let codigoWs = "codigoWs"
        static let SDTMensajes = "SDTMensajes"
        static let SDTListadoAlertasPersonales = "SDTListadoAlertasPersonales"
        static let CodigoValidacion = "CodigoValidacion"
        static let MsjActivacion = "MsjActivacion"
    }
    
    init(userbean : [String:Any]) {
        Nombres = userbean[UserKeys.Nombres] as? String
        SemAct = userbean[UserKeys.SemAct] as? String
        FlgClave = userbean[UserKeys.FlgClave] as? String
        TDocusu = userbean[UserKeys.TDocusu] as? String
        FlgContratoCG = userbean[UserKeys.FlgContratoCG] as? String
        MailUsuario = userbean[UserKeys.MailUsuario] as? String
        MisProductos = userbean[UserKeys.MisProductos] as? NSMutableArray
        Foto = userbean[UserKeys.Foto] as? String
        SesionId = userbean[UserKeys.SesionId] as? String
        Sexo = userbean[UserKeys.Sexo] as? String
        EsPremium = userbean[UserKeys.EsPremium] as? String
        NroSerCT = userbean[UserKeys.NroSerCT] as? String
        //
        NroSerFl = userbean[UserKeys.NroSerFl] as? String
        NroDocRel = userbean[UserKeys.NroDocRel] as? String
        SemActRel = userbean[UserKeys.SemActRel] as? String
        //
        CodigoWS = userbean[UserKeys.CodigoWS] as? String
        codigoWS = userbean[UserKeys.codigoWS] as? String
        //--
        CCOVerId = userbean[UserKeys.CCOVerId] as? String
        //--
        ApePat = userbean[UserKeys.ApePat] as? String
        ApeMat = userbean[UserKeys.ApeMat] as? String
        //Nombres = userbean[UserKeys.Nombres] as? String
        Mail = userbean[UserKeys.Mail] as? String
        codigoValidacion = userbean[UserKeys.codigoValidacion] as? String
        Celular = userbean[UserKeys.Celular] as? String
        //
        MensajeRespuesta = userbean[UserKeys.MensajeRespuesta] as? String
        //
        ErrorDes = userbean[UserKeys.ErrorDes] as? String
        //
        Codigo = userbean[UserKeys.Codigo] as? String
        EsValido = userbean[UserKeys.EsValido] as? String
        Mensaje = userbean[UserKeys.Mensaje] as? String
        //
        InfoTitular = userbean[UserKeys.InfoTitular] as? [String:Any]
        InfoTitularEmpresas = userbean[UserKeys.InfoTitularEmpresas] as? [String:Any]
        //
        MisEmpresas = userbean[UserKeys.MisEmpresas] as? NSArray
        objMisEmpresas = userbean[UserKeys.objMisEmpresas] as? NSDictionary
        //
        VieronMiReporte = userbean[UserKeys.VieronMiReporte] as? NSArray
        //
        SDT_MSDeudaTitular = userbean[UserKeys.SDT_MSDeudaTitular] as? [String : Any]
        //
        SDTNConRap = userbean[UserKeys.SDTNConRap] as? NSArray
        //
        Indicadores = userbean[UserKeys.Indicadores] as? NSArray
        SDTConRap = userbean[UserKeys.SDTConRap] as? NSArray
        Cont = userbean[UserKeys.Cont] as? Int
        codigoWs = userbean[UserKeys.codigoWs] as? String
        SDTMensajes = userbean[UserKeys.SDTMensajes] as? NSArray
        //
        SDTListadoAlertasPersonales = userbean[UserKeys.SDTListadoAlertasPersonales] as? NSArray
        
        CodigoValidacion = userbean[UserKeys.CodigoValidacion] as? String
        MsjActivacion = userbean[UserKeys.MsjActivacion] as? String
        
        urlPDF = userbean["URLPDF"] as? String
    }
}
