//
//  NotificationClass.swift
//  Sentinel
//
//  Created by Daniel on 28/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import Foundation

class NotificationClass {
    
    var codigoWS : String?
    var CodigoWS : String?
    //--
    var cantidadPersonal : Int?
    var cantidadTercero : Int?
    var cantidadMensaje : Int?
    
    var contAleCG : Int?
    var contAleMisEmp : Int?
    var contAleConTer : Int?
    var contAlertas : Int?
    var contMsj : Int?
    
    struct UserKeys {
        static let codigoWS = "codigoWS"
        static let CodigoWS = "CodigoWS"
    }
    
    init(notificationbean : [String:Any]) {
        codigoWS = notificationbean[UserKeys.codigoWS] as? String
        CodigoWS = notificationbean[UserKeys.CodigoWS] as? String
    }
    
}
