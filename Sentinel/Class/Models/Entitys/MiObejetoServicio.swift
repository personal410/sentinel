//
//  MiObejetoServicio.swift
//  Sentinel
//
//  Created by Daniel on 3/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class MiObejetoServicio: NSObject {
    var NroServicio : String = ""
    var DscServicio : String = ""
    var FlgCoop : String = ""
    var TipServ : String = ""
    var FlgSabio : String = ""
    
    override var description: String {
        return "NroServicio: \(NroServicio), DscServicio: \(DscServicio)"
    }
}
