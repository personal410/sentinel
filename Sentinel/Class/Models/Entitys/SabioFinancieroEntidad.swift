//
//  SabioFinancieroEntidad.swift
//  Sentinel
//
//  Created by Daniel on 5/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class SabioFinancieroEntidad {
    
    var CodigoWS : String?
    var codigoWs : String?
    //Obtiene Sabio
    var TieneSabio : String?
    var TipoSabio : String?
    
    var SDT_InOCSabioFinanciero: NSArray?
    var TDocEnt : String?
    var NDocEnt : String?
    var TCred : String?
    var Saldo : String?
    /*CronogramaCreditos */
    var porCanc : String?
    var NSaldo : String?
    var MonCanc : String?
    var NombreEntidad : String?
    var DscTCred : String?
    //Lista productos
    var Productos: NSArray?
    var IdProducto : String?
    var omProducto : String?
    var ImgProducto : String?

    //SABIO FINANCIERO
    
    var TDoc : String?
    var NroDoc : String?
    var Producto : String?
    var TipoDeudor : String?
    var Ingresos : String?
    var MontoPropuesto : String?
    var CuotaMontoPropuesto : String?
    var DeudaEntidad : String?
    var CuotaEntidad : String?
    var TDocCony : String?
    var NDocCony : String?
    var CancelaOC : String?
    var EsValido : String?
    var MensajeRespuesta : String?
    var Recomendacion: NSArray?
    var Imagen : String?
    var ProductoBean: SabioFinancieroEntidad?
    var TextoRecomendacion : String?
    var Termometro: NSArray?
    var Rangos: NSArray?
    
    var Inicio : String?
    var Fin : String?
    var Color : String?
    var ColorScore : String?
    var Score : String?
    var Minimo : String?
    var Maximo : String?
    var Riesgo : String?
    var SESeguimiento: NSArray?
    var NivelRiesgo : String?
    var ColorRiesgo : String?
    var Resultado: [AnyHashable : Any]?
    var Indicador : String?
    var ValorEsperado : String?
    var ValorReal : String?
    var Calificacion : String?
    var ProductosColeccion: [AnyHashable : Any]?
    //@property (nonatomic,strong) NSString*Nombre;
    var EvaluacionCredicitia: [AnyHashable]?
    var Dictamen : String?
    var ColorDictamen : String?
    var DetalleEvaluacion: [AnyHashable : Any]?
    var Item : String?
    var SEOriginacion: NSArray?
    //@property (nonatomic,strong) NSString*NivelRiesgo;
    //@property (nonatomic,strong) NSString*ColorRiesgo;
    var DscRiesgo : String?
    
    // ES EL MISMO :NDocCony -> NroDocCony
    //@property (nonatomic,strong) NSString*ProdId;
    
    var SDT_OCSabioFinanciero : NSArray?
    
    var SDTOpcionCredito: NSArray?
    var Monto : String?
    var Plazo : String?
    var Cuota : String?
    var CodProp : String?
    var CodOper : String?
    //CRONOGRAMA CREDITOS
    var SDTCronogramaCredito: NSArray?
    var Nro : String?
    //@property (nonatomic,strong) NSString*Saldo;
    var Amortizacion : String?
    var Interes : String?
    //CONSULTA CREDITICIA -> CLIENTE, CONYUGE
    var ClienteConyuge: [AnyHashable : Any]?
    var ConsultaCrediticia: [AnyHashable : Any]?
    var ApeMat : String?
    var ApePat : String?
    var Nombre : String?
    var NombreRazonSocial : String?
    var FechaNac : String?
    var DeudaTotal : String?
    var VarCod : String?
    var SemAct : String?
    var SemPre : String?
    var Sem12m : String?
    var TipoContribuyente : String?
    var EstContribuyente : String?
    var Dependencia : String?
    var InicioAct : String?
    var Condicion : String?
    var Foto : String?
    
    var CodigoCelda : String?
    //Campos CronogramaV2
    var CuotaC : String?
    var FechaCuotaInicial : String?
    var MontoPrestamo : String?
    var NumCuotas : String?
    var PeriodoPago : String?
    var Tea : String?
    var Tem : String?
    //Hora y Fecha limite de WS
    var ParFlg : String?
    var ParCon : String?
    var Error : String?
    var Fecha : String?
    var Hora : String?
    
    //Datos de la entidad
    var SDTDatosservicio : NSDictionary?
    
    //SabioFinanciero
    var Usuario = ""
    var SesionId = ""
    var Servicio = ""
    var SDTSabioFinanciero: [AnyHashable : Any]?
    
    //SabioComercial
    var SDTSabioComercial : [AnyHashable : Any]?
    
    //Cronograma de un credito
    
    struct UserKeys {
        static let codigoWs = "codigoWs"
        static let CodigoWS = "CodigoWS"
        static let TieneSabio = "TieneSabio"
        static let TipoSabio = "TipoSabio"
        
        static let SDT_InOCSabioFinanciero = "SDT_InOCSabioFinanciero"
        static let TDocEnt = "TDocEnt"
        static let NDocEnt = "NDocEnt"
        static let TCred = "TCred"
        static let Saldo = "Saldo"
        static let porCanc = "porCanc"
        static let NSaldo = "NSaldo"
        static let MonCanc = "MonCanc"
        static let NombreEntidad = "NombreEntidad"
        static let DscTCred = "DscTCred"
        
        static let Productos = "Productos"
        static let IdProducto = "IdProducto"
        static let omProducto = "omProducto"
        static let ImgProducto = "ImgProducto"
        
        //Sabio Financiero
        static let TDoc = "TDoc"
        static let NroDoc = "NroDoc"
        static let Producto = "Producto"
        static let TipoDeudor = "TipoDeudor"
        static let Ingresos = "Ingresos"
        static let MontoPropuesto = "MontoPropuesto"
        static let CuotaMontoPropuesto = "CuotaMontoPropuesto"
        static let DeudaEntidad = "DeudaEntidad"
        static let CuotaEntidad = "CuotaEntidad"
        static let TDocCony = "TDocCony"
        static let NDocCony = "NDocCony"
        static let CancelaOC = "CancelaOC"
        static let EsValido = "EsValido"
        static let MensajeRespuesta = "MensajeRespuesta"
        
        static let Recomendacion = "Recomendacion"
        static let Imagen = "Imagen"
        
        static let ProductoBean = "ProductoBean"
        static let TextoRecomendacion = "TextoRecomendacion"
        static let Termometro = "Termometro"
        static let Rangos = "Rangos"
        
        static let Inicio = "Inicio"
        static let Fin = "Fin"
        static let Color = "Color"
        static let ColorScore = "ColorScore"
        static let Score = "Score"
        static let Minimo = "Minimo"
        static let Maximo = "Maximo"
        static let Riesgo = "Riesgo"
        
        static let SESeguimiento = "SESeguimiento"
        static let NivelRiesgo = "NivelRiesgo"
        static let ColorRiesgo = "ColorRiesgo"
        static let Resultado = "Resultado"
        static let Indicador = "Indicador"
        static let ValorEsperado = "ValorEsperado"
        static let ValorReal = "ValorReal"
        static let Calificacion = "Calificacion"
        static let ProductosColeccion = "ProductosColeccion"
        static let EvaluacionCredicitia = "EvaluacionCredicitia"
        static let Dictamen = "Dictamen"
        static let ColorDictamen = "ColorDictamen"
        static let DetalleEvaluacion = "DetalleEvaluacion"
        static let Item = "Item"
        static let SEOriginacion = "SEOriginacion"
        static let DscRiesgo = "DscRiesgo"
        
        static let SDTOpcionCredito = "SDTOpcionCredito"
        static let Monto = "Monto"
        static let Plazo = "Plazo"
        static let Cuota = "Cuota"
        static let CodProp = "CodProp"
        static let CodOper = "CodOper"
        
        static let SDTCronogramaCredito = "SDTCronogramaCredito"
        static let Nro = "Nro"
        static let Amortizacion = "Amortizacion"
        static let Interes = "Interes"
        
        static let ClienteConyuge = "ClienteConyuge"
        static let ConsultaCrediticia = "ConsultaCrediticia"
        static let ApeMat = "ApeMat"
        static let Nombre = "Nombre"
        static let NombreRazonSocial = "NombreRazonSocial"
        static let FechaNac = "FechaNac"
        static let DeudaTotal = "DeudaTotalc"
        static let VarCod = "VarCod"
        static let SemAct = "SemAct"
        static let SemPre = "SemPre"
        static let Sem12m = "Sem12m"
        static let TipoContribuyente = "TipoContribuyente"
        static let EstContribuyente = "EstContribuyente"
        static let Dependencia = "Dependencia"
        static let InicioAct = "InicioAct"
        static let Condicion = "Condicion"
        static let Foto = "Foto"
        
        static let CodigoCelda = "CodigoCelda"
        static let CuotaC = "CuotaC"
        static let FechaCuotaInicial = "FechaCuotaInicial"
        static let MontoPrestamo = "MontoPrestamo"
        static let NumCuotas = "NumCuotas"
        static let PeriodoPago = "PeriodoPago"
        static let Tea = "Tea"
        static let Tem = "Tem"
        
        static let ParFlg = "ParFlg"
        static let ParCon = "ParCon"
        static let Error = "Error"
        static let Fecha = "Fecha"
        static let Hora = "Hora"
        
        static let SDT_OCSabioFinanciero = "SDT_OCSabioFinanciero"
        static let SDTDatosservicio = "SDTDatosservicio"
        static let SDTSabioFinanciero = "SDTSabioFinanciero"
        
        static let SDTSabioComercial = "SDTSabioComercial"
    }
    
    init(userbean : [String:Any]) {
        
        codigoWs = userbean[UserKeys.codigoWs] as? String
        CodigoWS = userbean[UserKeys.CodigoWS] as? String
        TieneSabio = userbean[UserKeys.TieneSabio] as? String
        TipoSabio = userbean[UserKeys.TipoSabio] as? String
        
        SDT_InOCSabioFinanciero = userbean[UserKeys.SDT_InOCSabioFinanciero] as? NSArray
        TDocEnt = userbean[UserKeys.TDocEnt] as? String
        NDocEnt = userbean[UserKeys.NDocEnt] as? String
        TCred = userbean[UserKeys.TCred] as? String
        Saldo = userbean[UserKeys.Saldo] as? String
        porCanc = userbean[UserKeys.porCanc] as? String
        NSaldo = userbean[UserKeys.NSaldo] as? String
        MonCanc = userbean[UserKeys.MonCanc] as? String
        NombreEntidad = userbean[UserKeys.NombreEntidad] as? String
        DscTCred = userbean[UserKeys.DscTCred] as? String
        
        Productos = userbean[UserKeys.Productos] as? NSArray
        IdProducto = userbean[UserKeys.IdProducto] as? String
        omProducto = userbean[UserKeys.omProducto] as? String
        ImgProducto = userbean[UserKeys.ImgProducto] as? String
        
        TDoc = userbean[UserKeys.TDoc] as? String
        NroDoc = userbean[UserKeys.NroDoc] as? String
        Producto = userbean[UserKeys.Producto] as? String
        TipoDeudor = userbean[UserKeys.TipoDeudor] as? String
        Ingresos = userbean[UserKeys.Ingresos] as? String
        MontoPropuesto = userbean[UserKeys.MontoPropuesto] as? String
        CuotaMontoPropuesto = userbean[UserKeys.CuotaMontoPropuesto] as? String
        DeudaEntidad = userbean[UserKeys.DeudaEntidad] as? String
        CuotaEntidad = userbean[UserKeys.CuotaEntidad] as? String
        TDocCony = userbean[UserKeys.TDocCony] as? String
        NDocCony = userbean[UserKeys.NDocCony] as? String
        CancelaOC = userbean[UserKeys.CancelaOC] as? String
        EsValido = userbean[UserKeys.EsValido] as? String
        MensajeRespuesta = userbean[UserKeys.MensajeRespuesta] as? String
        
        Recomendacion = userbean[UserKeys.Recomendacion] as? NSArray
        Imagen = userbean[UserKeys.Imagen] as? String
        ProductoBean = userbean[UserKeys.ProductoBean] as? SabioFinancieroEntidad
        TextoRecomendacion = userbean[UserKeys.TextoRecomendacion] as? String
        Termometro = userbean[UserKeys.Termometro] as? NSArray
        Rangos = userbean[UserKeys.Rangos] as? NSArray
        
        Inicio = userbean[UserKeys.Inicio] as? String
        Fin = userbean[UserKeys.Fin] as? String
        Color = userbean[UserKeys.Color] as? String
        ColorScore = userbean[UserKeys.ColorScore] as? String
        Score = userbean[UserKeys.Score] as? String
        Minimo = userbean[UserKeys.Minimo] as? String
        Maximo = userbean[UserKeys.Maximo] as? String
        Riesgo = userbean[UserKeys.Riesgo] as? String
        
        SESeguimiento = userbean[UserKeys.SESeguimiento] as? NSArray
        NivelRiesgo = userbean[UserKeys.NivelRiesgo] as? String
        ColorRiesgo = userbean[UserKeys.ColorRiesgo] as? String
         Resultado = userbean[UserKeys.Resultado] as? [AnyHashable : Any]
        Indicador = userbean[UserKeys.Indicador] as? String
        ValorEsperado = userbean[UserKeys.ValorEsperado] as? String
        ValorReal = userbean[UserKeys.ValorReal] as? String
        Calificacion = userbean[UserKeys.Calificacion] as? String
        ProductosColeccion = userbean[UserKeys.ProductosColeccion] as? [AnyHashable : Any]
        EvaluacionCredicitia = userbean[UserKeys.EvaluacionCredicitia] as? [AnyHashable]
        Dictamen = userbean[UserKeys.Dictamen] as? String
        ColorDictamen = userbean[UserKeys.ColorDictamen] as? String
        DetalleEvaluacion = userbean[UserKeys.DetalleEvaluacion] as? [AnyHashable : Any]
        Item = userbean[UserKeys.Item] as? String
        SEOriginacion = userbean[UserKeys.SEOriginacion] as? NSArray
        DscRiesgo = userbean[UserKeys.DscRiesgo] as? String
        
        SDTOpcionCredito = userbean[UserKeys.SDTOpcionCredito] as? NSArray
        Monto = userbean[UserKeys.Monto] as? String
        Plazo = userbean[UserKeys.Plazo] as? String
        Cuota = userbean[UserKeys.Cuota] as? String
        CodProp = userbean[UserKeys.CodProp] as? String
        CodOper = userbean[UserKeys.CodOper] as? String
        
         SDTCronogramaCredito = userbean[UserKeys.SDTCronogramaCredito] as? NSArray
        Nro = userbean[UserKeys.Nro] as? String
        Amortizacion = userbean[UserKeys.Amortizacion] as? String
        Interes = userbean[UserKeys.Interes] as? String
        
        ClienteConyuge = userbean[UserKeys.ClienteConyuge] as? [AnyHashable : Any]
        ConsultaCrediticia = userbean[UserKeys.ConsultaCrediticia] as? [AnyHashable : Any]
        ApeMat = userbean[UserKeys.ApeMat] as? String
        Nombre = userbean[UserKeys.Nombre] as? String
        NombreRazonSocial = userbean[UserKeys.NombreRazonSocial] as? String
        FechaNac = userbean[UserKeys.FechaNac] as? String
        DeudaTotal = userbean[UserKeys.DeudaTotal] as? String
        VarCod = userbean[UserKeys.VarCod] as? String
        SemAct = userbean[UserKeys.SemAct] as? String
        SemPre = userbean[UserKeys.SemPre] as? String
        Sem12m = userbean[UserKeys.Sem12m] as? String
        TipoContribuyente = userbean[UserKeys.TipoContribuyente] as? String
        EstContribuyente = userbean[UserKeys.EstContribuyente] as? String
        Dependencia = userbean[UserKeys.Dependencia] as? String
        InicioAct = userbean[UserKeys.InicioAct] as? String
        Condicion = userbean[UserKeys.Condicion] as? String
        Foto = userbean[UserKeys.Foto] as? String
        
        CodigoCelda = userbean[UserKeys.CodigoCelda] as? String
        CuotaC = userbean[UserKeys.CuotaC] as? String
        FechaCuotaInicial = userbean[UserKeys.FechaCuotaInicial] as? String
        MontoPrestamo = userbean[UserKeys.MontoPrestamo] as? String
        NumCuotas = userbean[UserKeys.NumCuotas] as? String
        PeriodoPago = userbean[UserKeys.PeriodoPago] as? String
        Tea = userbean[UserKeys.Tea] as? String
        Tem = userbean[UserKeys.Tem] as? String
        
        ParFlg = userbean[UserKeys.ParFlg] as? String
        ParCon = userbean[UserKeys.ParCon] as? String
        Error = userbean[UserKeys.Error] as? String
        Fecha = userbean[UserKeys.Fecha] as? String
        Hora = userbean[UserKeys.Hora] as? String
        
        SDT_OCSabioFinanciero = userbean[UserKeys.SDT_OCSabioFinanciero] as? NSArray
        SDTDatosservicio = userbean[UserKeys.SDTDatosservicio] as? NSDictionary
        SDTSabioFinanciero = userbean[UserKeys.SDTSabioFinanciero] as? [AnyHashable : Any]
        
        SDTSabioComercial = userbean[UserKeys.SDTSabioComercial] as? [AnyHashable : Any]
    }
    
}
