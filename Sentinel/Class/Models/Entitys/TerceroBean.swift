//
//  TerceroBean.swift
//  Sentinel
//
//  Created by Daniel on 10/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit
import Foundation

struct TerceroBean: Codable {
    
    let servicioValido: String
    let codigoServicio: String
    let codigoWS: String
    let CodigoValidacion: String
    
    let servicioValidoAlerta: String
    let codigoServicioAlerta: String
    let CodigoValidacionAlerta: String
    
    let teceroConsultado: String
    let teceroBusqueda: String
    let tipoConsulta: String
    
    let AFSerCod: String
    let AFSerTip: String
    let AFSerCPTDis: Int
    let AFSerCPTDisAlerta: Int
    
    let AFSerGraDis: Int
    let AFSerGraDisAlerta: Int
    
    let AFSerGraTCon: Int
    let AFSerGraCon: Int
    
    let cantidadDispo: Int
    let AFSerCPTNDisp: String
    
    let afserSBSTipDoc: String
    let afserSBSNumDoc: String
    let afserSBSNomAbr: String
    
    /*
     @property (nonatomic,strong) NSString*servicioValido;
     @property (nonatomic,strong) NSString*codigoServicio;
     @property (nonatomic,strong) NSString*codigoWS;
     @property (nonatomic,strong) NSString*CodigoValidacion;
     
     
     @property (nonatomic,strong) NSString*servicioValidoAlerta;
     @property (nonatomic,strong) NSString*codigoServicioAlerta;
     @property (nonatomic,strong) NSString*CodigoValidacionAlerta;
     
     
     @property (nonatomic,strong) NSString*teceroConsultado;//S:si - N:no;
     @property (nonatomic,strong) NSString*teceroBusqueda;//S:si - N:no;
     @property (nonatomic,strong) NSString*tipoConsulta; //G gratuita, X Tercero
     
     
     @property (nonatomic,strong) NSString*AFSerCod;
     @property (nonatomic,strong) NSString*AFSerTip;
     @property (nonatomic,strong) NSNumber*AFSerCPTDis;
     @property (nonatomic,strong) NSNumber*AFSerCPTDisAlerta;
     
     @property (nonatomic,strong) NSNumber*AFSerGraDis;
     @property (nonatomic,strong) NSNumber*AFSerGraDisAlerta;
     
     
     
     @property (nonatomic,strong) NSNumber*AFSerGraTCon;
     @property (nonatomic,strong) NSNumber*AFSerGraCon;
     
     @property (nonatomic,strong) NSNumber*cantidadDispo;
     @property (nonatomic,strong) NSString*AFSerCPTNDisp;
     
     
     @property (nonatomic,strong) NSString*afserSBSTipDoc;
     @property (nonatomic,strong) NSString*afserSBSNumDoc;
     @property (nonatomic,strong) NSString*afserSBSNomAbr;
    */
}
