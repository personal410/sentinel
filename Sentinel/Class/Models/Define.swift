//
//  Define.swift
//  Luhho
//
//  Created by Juan Alberto Carlos Vera on 1/22/18.
//  Copyright © 2018 Daniel Montoya. All rights reserved.
//
struct PATHS {
    static let APP_ID = 11
    static var DEPENDENCIA = 0
    static var RECUPERA = 0
    static let SENTINEL = "SENTINEL"
    static let PATH_PAYU = "https://api.payulatam.com/payments-api/4.0/service.cgi"
    
    static let PATH_PAYU_SENTINEL = "https://www.sentinelconsultagratuita.com/qawsapp/rest/RWS_PayuPaso1"//QAS
    static let PATH_PAYU_SENTINEL_RESPUESTA = "https://www.sentinelconsultagratuita.com/qawsapp/rest/RWS_RespuestaTransaccion"//QAS
    
//    static let PATH_PAYU_SENTINEL = "https://www.sentinelconsultagratuita.com/wsappv2/rest/RWS_PayuPaso1"//PROD
//    static let PATH_PAYU_SENTINEL_RESPUESTA = "https://www.sentinelconsultagratuita.com/wsappv2/rest/RWS_RespuestaTransaccion"//PROD
    
    static let PATHSENTINEL = "https://misentinel.sentinelperu.com/misentinelws/rest/"
    static let PATH = "https://www.sentinelconsultagratuita.com/wsappv2/rest/"
    static let PATHWS = "https://www.sentinelconsultagratuita.com/wsappv2/rest/"
    static let PATH_HOMONIMOS = "https://misentinel.sentinelperu.com/wsappv2/rest/"
    static let PATH_CONSULTA = "https://www.sentinelperu.com/qa/logeoMS.aspx"
    
    static var sesionActive = true
    
    struct TIENDA {
        static let ALER_PERSONAL = 11;
        static let CONS_TERCEROS = 10;
        static let ALER_TERCEROS = 13;
        static let CONS_FLASH = 20;
        static let RECUPERA = 48;
        static let PYME = 31
        static let CONS_TERCEROS_PREMIUM = 55
        static let SENTINEL_PREMIUM = 56
        static let CONS_TERCEROS_RESUMIDAS = 17
        
        static let SENTINEL_PREMIUM_MENSUAL = 60
        static let CONS_TERCEROS_UNO = 61;
        static let CONS_TERCEROS_CINCO = 62;
        static let CONS_TERCEROS_DIEZ = 63;
        static let CONS_TERCEROS_RESUMIDAS_UNO = 64
        static let CONS_TERCEROS_RESUMIDAS_CINCO = 65
        static let CONS_TERCEROS_RESUMIDAS_DIEZ = 66
        
        static let CONS_TERCEROS_UNO_PREMIUM = 67;
        static let CONS_TERCEROS_CINCO_PREMIUM = 68;
        static let CONS_TERCEROS_DIEZ_PREMIUM = 69;
        static let CONS_TERCEROS_RESUMIDAS_UNO_PREMIUM = 70
        static let CONS_TERCEROS_RESUMIDAS_CINCO_PREMIUM = 71
        static let CONS_TERCEROS_RESUMIDAS_DIEZ_PREMIUM = 72
        
        static let SENTINEL_PREMIUM_ANUAL = 73
    }
}

struct WEBSERVICES {
    
    struct REGISTER {
        static let RWSLoginMiSentinel = "RWSLoginMiSentinel"
        static let RWS_MSLogin = "RWS_MSLogin"
        static let RWS_MSConsultaVersionApp = "RWS_MSConsultaVersionApp"
        static let RWS_MSDatosUsuario = "RWS_MSDatosUsuario"
        static let RWS_MSPaso1App = "RWS_MSPaso1App"
        static let RWS_MSPaso2App = "RWS_MSPaso2App"
        static let RWS_MSPaso3App = "RWS_MSPaso3App"
        static let RWS_MSPaso4App = "RWS_MSPaso4App"
        static let RWS_MSActualizaContratoCG = "RWS_MSActualizaContratoCG"
        static let RWS_ValidarDNI = "RWS_ValidarDNI"
        static let RWS_MSCambiarClave = "RWS_MSCambiarClave"
        static let RWS_AFPValMail10 = "RWS_AFPValMail10"
        static let RWS_ValidaDigitoVerif = "RWS_ValidaDigitoVerif"
        static let RWS_ValidacionCelular = "RWS_ValidacionCelular"
        static let RWS_GeneraClaveSMS = "RWS_GeneraClaveSMS"
        static let RWS_AFPInsInvA = "RWS_AFPInsInvA"
        static let RWS_EnviaSMS = "RWS_EnviaSMS"
        static let RWS_AFPEnvCorreoSMS = "RWS_AFPEnvCorreoSMS"
        static let RWS_VerCelSMSEnv = "RWS_VerCelSMSEnv"
        static let RWS_GenPassword = "RWS_GenPassword"
        static let RWS_AceptaContrato = "RWS_AceptaContrato" //RWS_AceptaContrato
        static let RWS_ContratoRegistroMS = "RWS_ContratoRegistroMS"
        static let RWS_AFPActRegYa2 = "RWS_AFPActRegYa2"
        static let RWS_AFPEnvCorreoClave = "RWS_AFPEnvCorreoClave"
        static let RWS_MSOlvideClave = "RWS_MSOlvideClave"
        static let RWSCambiarClave = "RWSCambiarClave"
        static let RWS_ActDatosMS = "RWS_ActDatosMS"
        static let RWS_MSServicioxTipoProd = "RWS_MSServicioxTipoProd"
    }
    //Error
    static let RWS_MSErrorWS = "RWS_MSErrorWS"
    
    struct CONSULTA {
        static let RWS_MSConsultaCPT = "RWS_MSConsultaCPT"
        static let RWS_BUSQUEDACPT_SERVICIO = "RWS_BusquedaCPT_Servicio" 
        static let RWS_MSConsultaCPTDeudas = "RWS_MSConsultaCPTDeudas"
        static let RSentinelWSCG = "RSentinelWSCG"
        static let RWSVencidosMes = "RWSVencidosMes"
        static let RWSGeneraPDFMiSentinel2 = "RWSGeneraPDFMiSentinel2"
        static let RWS_AFPEnvCorreoPdf = "RWS_AFPEnvCorreoPdf"
        static let RWSQuienMeMira = "RWSQuienMeMira"
        static let RWS_MSVieronMiReporte = "RWS_MSVieronMiReporte"
        static let RWS_EvaCred = "RWS_EvaCred"
        static let RWS_MSListadoAlePer = "RWS_MSListadoAlePer"
        static let RWS_NotificacionAlertasPersonales = "RWS_NotificacionAlertasPersonales"
        static let RWS_NotificacionMisAlertas = "RWS_NotificacionMisAlertas"
        static let RWS_MSCargaCombo = "RWS_MSCargaCombo"
        static let Rws_usualeatorio = "Rws_usualeatorio"
        static let RWS_ListadoIndicadores = "RWS_ListadoIndicadores"
        static let RSentinelWSCG24 = "RSentinelWSCG24"
        static let RWS_PromAler_ConTer = "RWS_PromAler_ConTer"
        static let RWS_SET_Promo = "RWS_SET_Promo"
        static let RWS_MSMisConsultas = "RWS_MSMisConsultas"
        //Generar pdf
        static let RWS_MSPDFMiSentinel = "RWS_MSPDFMiSentinel"
        static let RWS_MSListadoIndicadores = "RWS_MSListadoIndicadores"
        //Obtener nombres
        static let RWS_MSTitulares = "RWS_MSTitulares"
        static let RWS_MSSTListadoAlertaPago = "RWS_MSSTListadoAlertaPago"
        static let RWS_MSSTRegistroAlertaPago = "RWS_MSSTRegistroAlertaPago"
        static let RWS_MSSTModificarAlertaPago = "RWS_MSSTModificarAlertaPago"
        static let RWS_MSSTActivaPagado = "RWS_MSSTActivaPagado"
        static let RWS_MSSTListadoLugaresCercanos = "RWS_MSSTListadoLugaresCercanos"
        static let RWS_MSListadoMensajes = "RWS_MSListadoMensajesOrigen" //RWS_MSListadoMensajes
        static let RWS_MSGenPDFFlash = "RWS_MSGenPDFFlash"
        static let RWS_MSModificaEstMensajes = "RWS_MSModificaEstMensajes"
        static let RWS_MSSet_Promo = "RWS_MSSet_Promo"
        static let RWS_MSCTAvisoCobranza = "RWS_MSCTAvisoCobranza"
        static let RWS_MSCTConsultaNotificacion = "RWS_MSCTConsultaNotificacion"
    }
    struct TERCEROS {
        static let RWS_ServUsuario = "RWS_ServUsuario"
        static let RWS_MSDatosServicio = "RWS_MSDatosServicio"
        static let RWS_AFPMAETIT = "RWS_AFPMAETIT"
        static let RWSBusXNomCT = "RWSBusXNomCT"
        static let RWS_BusquedaCPT_Servicio = "RWS_BusquedaCPT_Servicio"
        static let Rws_AsignarAlerta = "Rws_AsignarAlerta"
        static let RWS_NHistoConsul = "RWS_NHistoConsul"
        static let RWS_CNPCargaHistoria = "RWS_CNPCargaHistoria"
        static let RWS_ListadoMisAlertas = "RWS_ListadoMisAlertas"
        static let RWS_ValidaEmail = "RWS_ValidaEmail"
        static let RWS_CTAvisoCobranza = "RWS_CTAvisoCobranza"
        static let RWS_CTRegistroDeudor = "RWS_CTRegistroDeudor"
        static let RWS_CTListadoConDeu = "RWS_CTListadoConDeu"
        static let RWS_CTListadoDocProb = "RWS_CTListadoDocProb"
        static let RWS_CTListadoNotificaciones = "RWS_CTListadoNotificaciones"
        static let RWS_CTConsultaNotificacion = "RWS_CTConsultaNotificacion"
        static let RWS_CTReportarMoroso = "RWS_CTReportarMoroso"
        //static let RWS_ObtErrorWS = "RWS_ObtErrorWS"
        static let RWS_CTActEstRegistro = "RWS_CTActEstRegistro"
        static let RWS_CTRegDiscrep = "RWS_CTRegDiscrep"
        
        //Recupera
        static let RWS_MSCTRegistroDeudor = "RWS_MSCTRegistroDeudor"
        static let RWS_MSCTListadoConDeu = "RWS_MSCTListadoConDeu"
        static let RWS_MSCTListadoDocProb = "RWS_MSCTListadoDocProb"
        static let RWS_ObtieneFechaHora = "RWS_ObtieneFechaHora"
    }
    struct MENSAJERIA {
        static let RWS_ListadoMensajes = "RWS_ListadoMensajes"
        static let RWS_ContadorMensajes = "RWS_ContadorMensajes"
        static let RWS_ModificaEstMensajes = "RWS_ModificaEstMensajes"
        static let RWS_ElimMasivaMensajes = "RWS_ElimMasivaMensajes"
    }
    
    struct PASARELA {
        static let RWS_ObtKeyLinkApp = "RWS_ObtKeyLinkApp"
        static let RWS_FATTAR30 = "RWS_FATTAR30"
        static let RWS_FATTARIOS30 = "RWS_FATTARIOS30"
        static let RWS_PayuPaso1 = "RWS_PayuPaso1"
        static let RWS_PATH_PAYU = PATHS.PATH_PAYU
        static let RWS_RespuestaTransaccion = "RWS_RespuestaTransaccion"
    }

    struct NOTIFICACION {
        static let RWS_RegistraToken = "RWS_RegistraToken"
        static let RWS_ActTokenUsu = "RWS_ActTokenUsu"
        static let RWS_MSActTokenUsuario = "RWS_MSActTokenUsuario"
        static let RWS_ContaNotif = "RWS_ContaNotif"
        static let rws_notifhomesent = "rws_notifhomesent"
    }
   
    struct MISEMPRESAS {
        static let RWS_MSMisEmpresas = "RWS_MSMisEmpresas"
        static let RWS_AnadirEmpresas = "RWS_AnadirEmpresas"
        static let RWSBusXNomMisEmp = "RWSBusXNomMisEmp"
    }
    
    struct PROMOCIONES {
        static let RWS_PRecPar2 = "RWS_PRecPar2"
        static let RWS_GuardarSPTD = "RWS_GuardarSPTD"
    }
    
    struct FLASH {
        static let RWS_ConsultaBonifSentinelFlash = "RWS_ConsultaBonifSentinelFlash"
        static let RWS_RegTipoConsulta = "RWS_RegTipoConsulta"
    }
    
    struct SABIO {
        static let RWS_ObtieneSabio = "RWS_ObtieneSabio"
        static let RWS_SabioComercial = "RWS_SabioComercial"
        static let RWS_DeudasSabioFinanciero = "RWS_DeudasSabioFinanciero"
        static let RWS_SabioFinancieroV2 = "RWS_SabioFinancieroV2"
        static let RWS_OpcionCredito = "RWS_OpcionCredito"
        static let RWS_CronogramaCreditoV2 = "RWS_CronogramaCreditoV2"
        static let RWS_SabioFinancieroCronograma = "RWS_SabioFinancieroCronograma"
        static let RWS_SFAceptoCredito = "RWS_SFAceptoCredito"
        static let RWS_ACProductosSabio = "RWS_ACProductosSabio"
        static let RWS_ACConsultaCrediticia = "RWS_ACConsultaCrediticia"
        static let RWSDatosServicio = "RWSDatosServicio"
    }
    
    struct RECUPERA {
        static let RWS_MSCTListadoNotificaciones = "RWS_MSCTListadoNotificaciones"
        static let RWS_MSCTActEstRegistro = "RWS_MSCTActEstRegistro"
    }
}
struct K {
    struct ProductionServer {
        static let baseURL = "https://api.medium.com/v1"
    }
    
    struct APIParameterKey {
        static let password = "password"
        static let email = "email"
    }
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/json"
}
