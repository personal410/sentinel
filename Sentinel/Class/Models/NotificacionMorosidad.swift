//
//  NotificacionMorosidad.swift
//  Sentinel
//
//  Created by Victor Salazar on 5/30/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import UIKit
class NotificacionMorosidad{
    var coincidencia = Coincidencia(with: [:])
    var tipoDocRemitente = ""
    var nroDocRemitente = ""
    var nombreRemitente = ""
    var direccion = ""
    var correo = ""
    var celular = ""
    var correlativo = 0
    var conceptoDeudaId = ""
    var conceptoDeudaDesc = ""
    var monto = ""
    var moneda = ""
    var dateFecEmision = Date()
    var fechaEmision = ""
    var tipoDocumento = ""
    var documentoAdjunto = ""
    var dateFecVencimiento = Date()
    var fechaVencimiento = ""
    var fechaLimite = ""
    var comentarios = ""
}
