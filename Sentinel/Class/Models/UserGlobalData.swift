//
//  UserGlobalData.swift
//  Sentinel
//
//  Created by Daniel on 9/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class UserGlobalData: NSObject {
    internal var userGlobal: UserClass!
    internal var alertaGlobal: AlertaClass!
    internal var terceroGlobal: TerceroClass!
    internal var misServiciosGlobal: MisServiciosClass!
    
    internal var infoFinanciera: Financiera!
    internal var infoFinancieraGratuita: Financiera!
    internal var infoTercero: TerceroBean!
    internal var infoNotificacion: NotificacionBean!
    internal var infoMisEmpresas: MisEmpresasBean!
    internal var infoMisEmpresasBusqueda: MisEmpresasBusquedaBean!
    internal var infoEmpresaSeleccionada: MisEmpresasBusquedaBean!
    internal var infoTerceroBusquedaBean: TerceroBusquedaBean!
    internal var infoEnviarCorreoBean: EnviarCorreoBean!
    internal var infoMisServiciosBean: MisServiciosBean!
    internal var infoSabioBean: SabioFinancieroEntidad!
    
    class var sharedInstance: UserGlobalData {
        struct Static {
            static let instance = UserGlobalData()
        }
        return Static.instance
    }   
}
