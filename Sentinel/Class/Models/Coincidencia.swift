//
//  Coincidencia.swift
//  Sentinel
//
//  Created by Victor Salazar on 5/30/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import UIKit
class Coincidencia{
    var descripcion = ""
    var tipoDocAbr = ""
    var tipoDoc = ""
    var numDoc = ""
    var adicional = ""
    var nroRev = 0
    init(with dic:[String: Any]){
        if let desc = dic["descripcion"] as? String {
            descripcion = desc
        }
        if let tDocAbr = dic["TipoDocAbr"] as? String {
            tipoDocAbr = tDocAbr
        }
        if let tipodoc = dic["tipodoc"] as? String {
            tipoDoc = tipodoc
        }
        if let numdoc = dic["numdoc"] as? String {
            numDoc = numdoc
        }
        if let adi = dic["adicional"] as? String {
            adicional = adi
        }
        if let nRev = dic["NroRev"] as? Int {
            nroRev = nRev
        }
    }
    
    class func transformList(_ arrDics:[[String: Any]]) -> [Coincidencia]{
        var arrCoincidencias:[Coincidencia] = []
        for dic in arrDics {
            arrCoincidencias.append(Coincidencia(with: dic))
        }
        return arrCoincidencias
    }
}
