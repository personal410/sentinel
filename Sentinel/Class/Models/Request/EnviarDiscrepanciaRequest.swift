//
//  EnviarDiscrepanciaRequest.swift
//  Sentinel
//
//  Created by Victor Salazar on 22/6/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
struct EnviarDiscrepanciaRequest:Encodable {
    var useCod:String
    var tipoDoc:String
    var nroDoc:String
    var correlativo:Int
    var sesionId:String
    var usuario:String
    var archivo:String
    var discrepancia:String
    
    enum CodingKeys:String, CodingKey {
        case useCod = "CTUseCod"
        case tipoDoc = "CTTDocCPT"
        case nroDoc = "CTNDocCPT"
        case correlativo = "CTNCorr"
        case sesionId = "SesionId"
        case usuario = "Usuario"
        case archivo = "CTDocProbDiscrep"
        case discrepancia = "CTDiscrep"
    }
}
