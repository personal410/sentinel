//
//  ServicioTipoProdResponse.swift
//  Sentinel
//
//  Created by Victor Salazar on 1/29/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import Foundation
class ServicioTipoProdResponse {
    var servicio:String?
    var codigoWs:String?
    
    init(_ dic:[String:Any]) {
        servicio = dic["Servicio"] as? String
        codigoWs = dic["CodigoWS"] as? String
    }
}
