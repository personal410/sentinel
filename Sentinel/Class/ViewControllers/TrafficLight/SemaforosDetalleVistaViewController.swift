//
//  SemaforosDetalleVistaViewController.swift
//  Sentinel
//
//  Created by Daniel on 12/11/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class SemaforosDetalleVistaViewController: ParentViewController {

    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var primerIMG: UIImageView!
    @IBOutlet weak var segunIMG: UIImageView!
    @IBOutlet weak var tercerIMG: UIImageView!
    @IBOutlet weak var cuarIMG: UIImageView!
    @IBOutlet weak var quinIMG: UIImageView!
    @IBOutlet weak var sexIMG: UIImageView!
    
    @IBOutlet weak var primerMESA: UILabel!
    @IBOutlet weak var segunMESA: UILabel!
    @IBOutlet weak var tercerMESA: UILabel!
    @IBOutlet weak var cuarMESA: UILabel!
    @IBOutlet weak var quinMESA: UILabel!
    @IBOutlet weak var sexMESA: UILabel!
    
    var esPremium : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.estilos()
        self.validarPremium()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func estilos(){
        self.toolbar.clipsToBounds = true
    }
    
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            self.esPremium = true
            self.changeStatusBar(cod: 3)
            self.setGradient(uiView: self.toolbar)
            
        }
        else{
            self.esPremium = false
        }
    }

    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
