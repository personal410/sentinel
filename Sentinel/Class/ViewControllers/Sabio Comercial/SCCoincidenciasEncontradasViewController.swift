//
//  SCCoincidenciasEncontradasViewController.swift
//  Sentinel
//
//  Created by Daniel on 12/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//SCCoincidenciasEncontradasViewController

import UIKit

class SCCoincidenciasEncontradasViewController: ParentViewController {

    @IBOutlet weak var coincidenciasLB: UILabel!
    @IBOutlet weak var seleccionaTextoLB: UILabel!
    @IBOutlet weak var personasTable: UITableView!
    
    var apePaterno : String = ""
    var apeMaterno : String = ""
    var nombre : String = ""
    var personaJuridica: String = ""
    var otros: String = ""
    var tipoPersona: String!
    var arregloCoincidencias : NSArray = NSArray()
    
    var tercero : TerceroClass?
    var userData : UserClass?
    
    var esPremium : Bool = false
    var esEmpresarial : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializarVariables()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.iniCoincidenciasEncontradas()
        
        self.personasTable.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func inicializarVariables(){
        
        self.tercero = UserGlobalData.sharedInstance.terceroGlobal
        self.userData = UserGlobalData.sharedInstance.userGlobal
        self.personasTable.dataSource = self
        self.personasTable.delegate = self
    }
    
    func cargarDatos(){

    }
    
    func iniCoincidenciasEncontradas(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endCoincidenciasEncontradas(_:)), name: NSNotification.Name("endCoincidenciasEncontradas"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        let userData = UserGlobalData.sharedInstance.userGlobal
        let tercero = UserGlobalData.sharedInstance.terceroGlobal
        let randomNumber : String = String(self.generateRandomNumber(numDigits: 7))
        var tipoPersonaTercero : String = ""
        let usuario : String = (userData?.user)!
        let sesion : String = (userData?.SesionId)!
        
        if self.tipoPersona == "1" {
            tipoPersonaTercero = "N"
            let parametros = [
                "UserId":usuario,
                "IdSession":randomNumber,
                "TipoPersona":tipoPersonaTercero,
                "Paterno":self.apePaterno,
                "Materno":self.apeMaterno,
                "nombres":self.nombre,
                "UseSeIDSession":sesion
                ] as [String : Any]
            print(parametros)
            
            OriginData.sharedInstance.getConsultaNombre(notification: "endCoincidenciasEncontradas", parametros: parametros)
        }
        else if self.tipoPersona == "2" {
            tipoPersonaTercero = "J"
            let parametros = [
                "UserId":usuario,
                "IdSession":randomNumber,
                "TipoPersona":tipoPersonaTercero,
                "Paterno":self.personaJuridica,
                "Materno":"",
                "nombres":"",
                "UseSeIDSession":sesion
                ] as [String : Any]
            print(parametros)
            
            OriginData.sharedInstance.getConsultaNombre(notification: "endCoincidenciasEncontradas", parametros: parametros)
        }
        else if self.tipoPersona == "3" {
            tipoPersonaTercero = "O"
            let parametros = [
                "UserId":usuario,
                "IdSession":randomNumber,
                "TipoPersona":tipoPersonaTercero,
                "Paterno":self.otros,
                "Materno":"",
                "nombres":"",
                "UseSeIDSession":sesion
                ] as [String : Any]
            print(parametros)
            
            OriginData.sharedInstance.getConsultaNombre(notification: "endCoincidenciasEncontradas", parametros: parametros)
        }
        else{
            let parametros = [
                "UserId":usuario,
                "IdSession":randomNumber,
                "TipoPersona":tipoPersonaTercero,
                "Paterno":self.apePaterno,
                "Materno":self.apeMaterno,
                "nombres":self.nombre,
                "UseSeIDSession":sesion
                ] as [String : Any]
            print(parametros)
            
            OriginData.sharedInstance.getConsultaNombre(notification: "endCoincidenciasEncontradas", parametros: parametros)
        }
    }
    
    @objc func endCoincidenciasEncontradas(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as? TerceroClass
            if data?.CodigoWS == "0"{
                UserGlobalData.sharedInstance.terceroGlobal.SDTBusAlf = data?.SDTBusAlf
                self.arregloCoincidencias = (data?.SDTBusAlf)!
                if self.arregloCoincidencias.count == 0 {
                    self.coincidenciasLB.text = "No hay coincidencias encontradas."
                }else if self.arregloCoincidencias.count == 1 {
                    self.coincidenciasLB.text = "Hay \(self.arregloCoincidencias.count) coincidencia encontrada."
                }
                else if self.arregloCoincidencias.count > 1 {
                    self.coincidenciasLB.text = "Hay \(self.arregloCoincidencias.count) coincidencias encontradas."
                }
                self.cargarDatos()
                self.personasTable.reloadData()
            }
            else
            {
                if data?.CodigoWS != nil {
                    if data?.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "En estos momentos no se puede realizar la consulta. Intente luego.", preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                        //iniMensajeError(codigo: (data?.CodigoWS!)!)
                    }
                }
            }
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension SCCoincidenciasEncontradasViewController : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arregloCoincidencias.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusquedaPersonasTVC", for: indexPath) as! BusquedaPersonasTableViewCell
        let persona : NSDictionary = self.arregloCoincidencias[indexPath.row] as! NSDictionary
        let dni : String = persona.object(forKey: "numdoc") as! String
        let tipo: String = persona.object(forKey: "tipodoc") as! String
        
        if tipo == "D"{
            cell.dniLB.text = "DNI \(dni)"
            let fechaSinFormato : String = persona.object(forKey: "adicional") as! String
            
            let first : String = String(fechaSinFormato[...3])
            let second : String = String(fechaSinFormato[4..<6])
            let third : String = String(fechaSinFormato[NSRange(location: 6, length: 2)])
            
            let fechaConFormato : String = "\(third)/\(second)/\(first)"
            
            cell.fechaLB.text = fechaConFormato
            
        }
        else if tipo == "R"{
            cell.dniLB.text = "RUC \(dni)"
            cell.fechaLB.text = ""
        }
        else{
            cell.dniLB.text = "CE \(dni)"
        }
        
        let nombrePersona : String = persona.object(forKey: "descripcion") as! String
        cell.nombreLB.text = nombrePersona
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let productos : NSArray = UserGlobalData.sharedInstance.terceroGlobal.Productos!
        if productos.count == 1 {
            
        }
        
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "PopupVC") as! PopupViewController
        let persona : NSDictionary = self.arregloCoincidencias[indexPath.row] as! NSDictionary
        let dni : String = persona.object(forKey: "numdoc") as! String
        let tipo: String = persona.object(forKey: "tipodoc") as! String
        nav.tipoDocTercero = tipo
        nav.documentoTercero = dni
        
        self.navigationController?.pushViewController(nav, animated: true)
        
    }
}



