//
//  SCBuscarTipoViewController.swift
//  Sentinel
//
//  Created by Daniel on 12/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class SCBuscarTipoViewController: ParentViewController {

    @IBOutlet weak var numeroDocBT: UIButton!
    @IBOutlet weak var nombreDocBT: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializar()
        self.estilos()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func inicializar(){
        
    }
    
    func estilos(){
        self.numeroDocBT.layer.cornerRadius = 10
        self.nombreDocBT.layer.cornerRadius = 10
    }
    
    @IBAction func accionAtras(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
    }
    
}
