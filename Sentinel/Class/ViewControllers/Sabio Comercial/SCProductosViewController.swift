//
//  SCProductosViewController.swift
//  Sentinel
//
//  Created by Daniel on 12/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class SCProductosViewController: ParentViewController {

    @IBOutlet weak var productosTV: UITableView!
    
    var array : NSArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.estilos()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.cargarDatos()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func estilos(){
        
    }
    
    func cargarDatos(){
        let productos : NSArray = UserGlobalData.sharedInstance.terceroGlobal.Productos!
        array = productos
        self.productosTV.reloadData()
    }

}

extension SCProductosViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
    return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.array.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SFProductosTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SFProductosTVC", for: indexPath) as! SFProductosTableViewCell
        
        let objProducto : NSDictionary = self.array[indexPath.row] as! NSDictionary
        cell.tituloLB.text = objProducto.object(forKey: "NomProducto") as? String
        let idProducto : String = String(objProducto.object(forKey: "IdProducto") as! Int)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let objProducto : NSDictionary = self.array[indexPath.row] as! NSDictionary
        let idProducto : String = String(objProducto.object(forKey: "IdProducto") as! Int)
        let NomProducto : String = objProducto.object(forKey: "NomProducto") as! String
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "SCConsultaSabioVC") as! SCConsultaSabioViewController
        nav.idProducto = idProducto
        nav.NomProducto = NomProducto
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
}











