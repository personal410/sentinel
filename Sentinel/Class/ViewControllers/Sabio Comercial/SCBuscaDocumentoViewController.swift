//
//  SCBuscaDocumentoViewController.swift
//  Sentinel
//
//  Created by Daniel on 12/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//  SCBuscaDocumentoViewController

import UIKit
import Alamofire
import AlamofireImage

class SCBuscaDocumentoViewController: ParentViewController,MiPickerComboDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var numDocTXT: UITextField!
    @IBOutlet weak var comboDocumento: CTComboPicker!
    
    var tercero : TerceroClass?
    var userData : UserClass?
    
    var codCombo = "1"
    var desCombo = "Automático"
    var combosArray: NSMutableArray = NSMutableArray()
    var combosIDArray: NSMutableArray = NSMutableArray()
    
    var tipoDocTercero : String!
    
    var esPremium : Bool = false
    var esEmpresarial : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializarVariables()
        self.gestureScreen()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.iniConsultaDatosServicioParent()
        self.cargarDatos()
    }
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func inicializarVariables(){
        
        self.numDocTXT.delegate = self
        self.comboDocumento.delegate = self
        
        self.combosArray.add("Automático")
        self.combosArray.add("DNI")
        self.combosArray.add("RUC")
        self.combosArray.add("Carnet de extranjería")
        
        self.combosIDArray.add("1")
        self.combosIDArray.add("2")
        self.combosIDArray.add("3")
        self.combosIDArray.add("4")
    }
    
    func cargarDatos(){
        
        self.tercero = UserGlobalData.sharedInstance.terceroGlobal
        self.userData = UserGlobalData.sharedInstance.userGlobal
    }
    
    func validar() -> Bool{
        
        let numeroDocumento : String = self.numDocTXT.text!
        let cantidad = numeroDocumento.count
        if numeroDocumento == ""{
            self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar el número de documento a consultar.")
            return false
        }
        if codCombo == "1"{
            if (cantidad == 8 || cantidad == 11) {
                
            }
            else
            {
                self.showAlert(PATHS.SENTINEL, mensaje: "Número de documento inválido")
                return false
            }
        }
        if codCombo == "2" && cantidad != 8 {
            self.showAlert(PATHS.SENTINEL, mensaje: "El número de DNI debe contener 8 dígitos")
            return false
        }
        if codCombo == "3" && cantidad != 11 {
            self.showAlert(PATHS.SENTINEL, mensaje: "El número de RUC debe contener 11 dígitos")
            return false
        }
        if numeroDocumento == userData?.NumDocumento{
            self.showAlert(PATHS.SENTINEL, mensaje: "No se puede consultar a usted mismo.")
            self.numDocTXT.text = ""
            return false
        }
        return true
    }
    
    func iniSabioComercial(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endSabioComercial(_:)), name: NSNotification.Name("endSabioComercial"), object: nil)
        
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        
        var tipoDoc : String = ""
        if codCombo == "2" {
            tipoDoc = "D"
            self.tipoDocTercero = "D"
        }
        else if codCombo == "3"{
            tipoDoc = "R"
            self.tipoDocTercero = "R"
        }
        else if codCombo == "1"{
            if self.numDocTXT.text?.count == 8 {
                tipoDoc = "D"
                self.tipoDocTercero = "D"
            }
            else if self.numDocTXT.text?.count == 11 {
                tipoDoc = "R"
                self.tipoDocTercero = "R"
            }
        }
        else if codCombo == "4"{
            tipoDoc = "4"
            self.tipoDocTercero = "4"
        }
        let servicio : String = UserGlobalData.sharedInstance.misServiciosGlobal.NroServicio!
        let nroDoc : String = self.numDocTXT.text!
        let sesion : String = userData!.SesionId!
        let usuario : String = (userData?.user!)!
        //let servicio : String = (userData?.NroSerCT!)!
        
        let parametros = [
            "Usuario" : usuario,
            "Servicio" : servicio,
            "NroDoc" :  nroDoc,
            "TDoc" : tipoDoc,
            "SesionId" : sesion
            ] as [String : Any]
        print(parametros)
        OriginData.sharedInstance.getSabioComercial(notification: "endSabioComercial", parametros: parametros)
    }
    
    @objc func endSabioComercial(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as! SabioFinancieroEntidad
            if data.CodigoWS == "0"{
                
                UserGlobalData.sharedInstance.terceroGlobal.SabioComercial = data.SDTSabioComercial as! NSDictionary
                UserGlobalData.sharedInstance.terceroGlobal.documentoTercero = self.numDocTXT.text!
                
                UserGlobalData.sharedInstance.terceroGlobal.tipoDocumentoTercero = self.tipoDocTercero
                
                print(data.SDTSabioComercial)
                let nav = UIStoryboard.init(name: "StoryboardSabioComercial", bundle: nil).instantiateViewController(withIdentifier: "SCConsultaSabioVC") as!  SCConsultaSabioViewController
                
                self.navigationController?.pushViewController(nav, animated: true)
                
            }
            else
            {
                if data.CodigoWS != nil {
                    if data.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        iniMensajeError(codigo: (data.CodigoWS!))
                    }
                }
                else
                {
                    let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                    alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                    self.present(alerta, animated: true, completion: nil)
                }
            }
        }
    }
    
    func iniListarProductos(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endListarProductos(_:)), name: NSNotification.Name("endListarProductos"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        
        let user : String = UserGlobalData.sharedInstance.userGlobal.user!
        let sesion : String = UserGlobalData.sharedInstance.userGlobal.SesionId!
        let servicio : String = UserGlobalData.sharedInstance.misServiciosGlobal.NroServicio!
        
        let parametros = [
            "Usuario" : user,
            "SesionId" : sesion,
            "Servicio" : servicio
            ] as [String : Any]
        
        print(parametros)
        
        OriginData.sharedInstance.getProductosSabio(notification: "endListarProductos", parametros: parametros)
    }
    
    @objc func endListarProductos(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as? SabioFinancieroEntidad
            if data?.CodigoWS == "0"{
                let productos : NSArray = (data?.Productos)!
                if productos.count == 0 {
                    self.showAlert(PATHS.SENTINEL, mensaje: "Sin productos disponibles para el cliente.")
                }
                else if productos.count == 1 {
                    UserGlobalData.sharedInstance.terceroGlobal.Productos = productos
                    let nav = self.storyboard?.instantiateViewController(withIdentifier: "SCConsultaSabioVC") as! SCConsultaSabioViewController
                    self.navigationController?.pushViewController(nav, animated: true)
                }
                else if productos.count > 1 {
                    UserGlobalData.sharedInstance.terceroGlobal.Productos = productos
                    let nav = self.storyboard?.instantiateViewController(withIdentifier: "SCProductosVC") as! SCProductosViewController
                    
                    self.navigationController?.pushViewController(nav, animated: true)
                }
            }
            else
            {
                if data?.CodigoWS != nil {
                    if data?.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: data?.MensajeRespuesta, preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                        //iniMensajeError(codigo: (data?.CodigoWS!)!)
                    }
                }
            }
        }
    }
    
    @objc func endCargarFotoTercero2(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "ReporteEmpresarialVC") as! ReporteEmpresarialViewController
        //nav.documentoTercero = self.numDocTXT.text
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if codCombo == "1" {
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 10
        }
        if codCombo == "2"{
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 7
        }
        if codCombo == "3"{
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 10
        }
        if codCombo == "4" {
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 19
        }
        return true
    }
    
    @IBAction func accionCombo(_ sender: Any) {
        
        self.comboDocumento.setConfig(combosArray as! [Any])
        self.comboDocumento.open()
        self.comboDocumento.setId(1)
    }
    @IBAction func accionComboDos(_ sender: Any) {
        
        self.comboDocumento.setConfig(combosArray as! [Any])
        self.comboDocumento.open()
        self.comboDocumento.setId(1)
    }
    @IBAction func accionBuscar(_ sender: Any) {
        if self.validar() {
            self.iniSabioComercial()
        }
        
    }
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func miPickerCombo(_ pickerid: Int, didSelectRow row: Int) {
        switch pickerid {
        case 1:
            codCombo = combosIDArray[row] as! String
            desCombo = combosArray[row] as! String
            
            break
        case 2:
            break
        default:
            break
        }
    }
    
    func miPickerCombo(_ MiPickerView: CTComboPicker!, pickerView picker: UIPickerView!, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
}
