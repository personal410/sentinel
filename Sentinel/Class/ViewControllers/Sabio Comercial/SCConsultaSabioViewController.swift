//
//  SCConsultaSabioViewController.swift
//  Sentinel
//
//  Created by Daniel on 12/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class SCConsultaSabioViewController: ParentViewController {

    var idProducto : String!
    var NomProducto : String!
    
    @IBOutlet weak var apellidosCompletosLB: UILabel!
    @IBOutlet weak var nombreCompletoTX: UILabel!
    @IBOutlet weak var tipoDocTX: UILabel!
    @IBOutlet weak var numeroDocTX: UILabel!
    
    @IBOutlet weak var unoView: UIView!
    @IBOutlet weak var primerView: UIView!
    
    @IBOutlet weak var dosView: UIView!
    @IBOutlet weak var tipoCreditoLB: UILabel!
    @IBOutlet weak var aprobarLB: UILabel!
    @IBOutlet weak var detalleSabioLB: UILabel!
    @IBOutlet weak var aprobarIMG: UIImageView!
    
    @IBOutlet weak var cuatroView: UIView!
    @IBOutlet weak var tituloCuotaEstimada: UILabel!
    @IBOutlet weak var cuotaEstimada: UILabel!
    
    @IBOutlet weak var tresView: SFControlTermometro!
    @IBOutlet weak var recomendacionLB: UILabel!
    
    @IBOutlet weak var cuotaIMG: UIImageView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var alturaPrimerView: NSLayoutConstraint!
    @IBOutlet weak var alturaDosView: NSLayoutConstraint!
    
    @IBOutlet weak var topTipoDoc: NSLayoutConstraint!
    @IBOutlet weak var altoApellidos: NSLayoutConstraint!
    
    var userData : UserClass!
    var tercero : TerceroClass!
    var tipoDocComercial : String!
    var numDocComercial : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializar()
        self.estilos()
        self.limpiarPantalla()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.iniTitulares(numDoc: numDocComercial, tipoDoc: tipoDocComercial)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func cargarDatos(){
        tercero = UserGlobalData.sharedInstance.terceroGlobal
        let SabioComercial = tercero.SabioComercial
        
        let Termometro : NSDictionary = SabioComercial?.object(forKey: "Termometro") as! NSDictionary
        let TermometroMutable : NSMutableDictionary = Termometro.mutableCopy() as! NSMutableDictionary
        let DescRiesgo : String = SabioComercial?.object(forKey: "DescRiesgo") as! String
        let Evaluacion : NSDictionary = SabioComercial?.object(forKey: "Evaluacion") as! NSDictionary
        let Productos : NSArray = Evaluacion.object(forKey: "Productos") as! NSArray
        let CapacidadPago : NSDictionary = SabioComercial?.object(forKey: "CapacidadPago") as! NSDictionary
        
        let ProductosOBJ : NSDictionary = Productos[0] as! NSDictionary
        let NombreProducto : String = ProductosOBJ.object(forKey: "NombreProducto") as! String
        let Resultado : String = ProductosOBJ.object(forKey: "Resultado") as! String
        let ImgEvaluacion : String = ProductosOBJ.object(forKey: "ImgEvaluacion") as! String
        let ImgProducto : String = ProductosOBJ.object(forKey: "ImgProducto") as! String
        let ColorEvaluacion : String = ProductosOBJ.object(forKey: "ColorEvaluacion") as! String
        let DetalleEvaluacion : NSArray = ProductosOBJ.object(forKey: "DetalleEvaluacion") as! NSArray
        let Titulo : String = CapacidadPago.object(forKey: "Titulo") as! String
        let Descripcion : String = CapacidadPago.object(forKey: "Descripcion") as! String
        
        
        if tipoDocComercial == "D"{
            self.altoApellidos.constant = 22
            self.alturaPrimerView.constant = 85
            //Termometro
            self.tresView.formatoTermometro(TermometroMutable)
            self.recomendacionLB.text = DescRiesgo
            if DescRiesgo == ""{
                self.recomendacionLB.text = "Sin detalle de recomendación."
            }
            self.tipoCreditoLB.text = NombreProducto
            self.aprobarLB.text = Resultado
            self.aprobarLB.textColor = self.devuelveColorRGB(colorAprobacion: ColorEvaluacion)
            var detalleMotivosCadena : String = ""
            for i in 0 ... DetalleEvaluacion.count - 1 {
                let objDetalle : String = DetalleEvaluacion[i] as! String
                if i == 0 {
                    detalleMotivosCadena = "\(objDetalle)"
                }
                else
                {
                    detalleMotivosCadena = "\(detalleMotivosCadena) \n\(objDetalle)"
                }
            }
            self.detalleSabioLB.text = detalleMotivosCadena
            self.tituloCuotaEstimada.text = Titulo
            self.cuotaEstimada.text = Descripcion
            let nombres : String = tercero.Nombres!
            self.nombreCompletoTX.text = nombres
            let apellidos : String = "\(tercero.ApePat!) \(tercero.ApeMat!)"
            self.apellidosCompletosLB.text = apellidos
            self.tipoDocTX.text = "DNI"
            self.numeroDocTX.text = self.numDocComercial
            //Image
            let consultado: String = ImgEvaluacion
            let url : URL = NSURL(string: consultado)! as URL
            NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarFotoSabioC(_:)), name: NSNotification.Name("endCargarFotoSabio"), object: nil)
            
            self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
            Alamofire.request(url).responseImage { response in
                debugPrint(response)
                
                switch response.result {
                case .success:
                    if let image = response.result.value {
                        print("image downloaded: \(image)")
                        self.aprobarIMG.image = image
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoSabio"), object: ImgProducto)
                    }
                    break
                    
                case .failure(let error):
                    //CNIGenImaHom
                    //FALTA POR DEFECTO
                    //let image : UIImage = UIImage.init(named: "CNIGenImaHom")!
                    //self.imageIMG.image =
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoSabio"), object: ImgProducto)
                    break
                }
            }
            
        }
        else if tipoDocComercial == "R"{
            self.alturaPrimerView.constant = 63
            self.altoApellidos.constant = 0
            //Termometro
            self.tresView.formatoTermometro(TermometroMutable)
            self.recomendacionLB.text = DescRiesgo
            if DescRiesgo == ""{
                self.recomendacionLB.text = "Sin detalle de recomendación."
            }
            self.tipoCreditoLB.text = NombreProducto
            self.aprobarLB.text = Resultado
            self.aprobarLB.textColor = self.devuelveColorRGB(colorAprobacion: ColorEvaluacion)
            var detalleMotivosCadena : String = ""
            for i in 0 ... DetalleEvaluacion.count - 1 {
                let objDetalle : String = DetalleEvaluacion[i] as! String
                if i == 0 {
                    detalleMotivosCadena = "\(objDetalle)"
                }
                else
                {
                    detalleMotivosCadena = "\(detalleMotivosCadena) \n\(objDetalle)"
                }
            }
            self.detalleSabioLB.text = detalleMotivosCadena
            self.tituloCuotaEstimada.text = Titulo
            self.cuotaEstimada.text = Descripcion
            let nombres : String = tercero.NombreRazSoc!
            self.nombreCompletoTX.text = nombres
            //let apellidos : String = "\(tercero.ApePat!) \(tercero.ApeMat!)"
            //self.apellidosCompletosLB.text = apellidos
            self.tipoDocTX.text = "RUC"
            self.numeroDocTX.text = self.numDocComercial
            //Image
            let consultado: String = ImgEvaluacion
            let url : URL = NSURL(string: consultado)! as URL
            NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarFotoSabioC(_:)), name: NSNotification.Name("endCargarFotoSabio"), object: nil)
            
            self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
            Alamofire.request(url).responseImage { response in
                debugPrint(response)
                
                switch response.result {
                case .success:
                    if let image = response.result.value {
                        print("image downloaded: \(image)")
                        self.aprobarIMG.image = image
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoSabio"), object: ImgProducto)
                    }
                    break
                    
                case .failure(let error):
                    //CNIGenImaHom
                    //FALTA POR DEFECTO
                    //let image : UIImage = UIImage.init(named: "CNIGenImaHom")!
                    //self.imageIMG.image =
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoSabio"), object: ImgProducto)
                    break
                }
            }
        }
        else
        {
            self.alturaPrimerView.constant = 85
            self.altoApellidos.constant = 22
            //Termometro
            self.tresView.formatoTermometro(TermometroMutable)
            self.recomendacionLB.text = DescRiesgo
            if DescRiesgo == ""{
                self.recomendacionLB.text = "Sin detalle de recomendación."
            }
            self.tipoCreditoLB.text = NombreProducto
            self.aprobarLB.text = Resultado
            self.aprobarLB.textColor = self.devuelveColorRGB(colorAprobacion: ColorEvaluacion)
            var detalleMotivosCadena : String = ""
            for i in 0 ... DetalleEvaluacion.count - 1 {
                let objDetalle : String = DetalleEvaluacion[i] as! String
                if i == 0 {
                    detalleMotivosCadena = "\(objDetalle)"
                }
                else
                {
                    detalleMotivosCadena = "\(detalleMotivosCadena) \n\(objDetalle)"
                }
            }
            self.detalleSabioLB.text = detalleMotivosCadena
            self.tituloCuotaEstimada.text = Titulo
            self.cuotaEstimada.text = Descripcion
            let nombres : String = tercero.Nombres!
            self.nombreCompletoTX.text = nombres
            let apellidos : String = "\(tercero.ApePat!) \(tercero.ApeMat!)"
            self.apellidosCompletosLB.text = apellidos
            self.tipoDocTX.text = "DNI"
            self.numeroDocTX.text = self.numDocComercial
            //Image
            let consultado: String = ImgEvaluacion
            let url : URL = NSURL(string: consultado)! as URL
            NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarFotoSabioC(_:)), name: NSNotification.Name("endCargarFotoSabio"), object: nil)
            
            self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
            Alamofire.request(url).responseImage { response in
                debugPrint(response)
                
                switch response.result {
                case .success:
                    if let image = response.result.value {
                        print("image downloaded: \(image)")
                        self.aprobarIMG.image = image
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoSabio"), object: ImgProducto)
                    }
                    break
                    
                case .failure(let error):
                    //CNIGenImaHom
                    //FALTA POR DEFECTO
                    //let image : UIImage = UIImage.init(named: "CNIGenImaHom")!
                    //self.imageIMG.image =
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoSabio"), object: ImgProducto)
                    break
                }
            }
        }
        
    }
    
    @objc func endCargarFotoSabioC(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        let ImgProducto : String = notification.object as! String
        let consultado: String = ImgProducto
        let url : URL = NSURL(string: consultado)! as URL
        NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarFotoSabioCC(_:)), name: NSNotification.Name("endCargarFotoSabioCC"), object: nil)
        
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        Alamofire.request(url).responseImage { response in
            debugPrint(response)
            
            switch response.result {
            case .success:
                if let image = response.result.value {
                    print("image downloaded: \(image)")
                    self.cuotaIMG.image = image
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoSabioCC"), object: nil)
                }
                break
                
            case .failure(let error):
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoSabioCC"), object: nil)
                break
            }
        }
    }
    
    @objc func endCargarFotoSabioCC(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
    }
    
    func limpiarPantalla(){
        self.nombreCompletoTX.text = ""
        self.apellidosCompletosLB.text = ""
        self.numeroDocTX.text = ""
        self.tipoDocTX.text = "Doc:"
        self.detalleSabioLB.text = ""
        self.cuotaEstimada.text = ""
    }
    
    func inicializar(){
        userData = UserGlobalData.sharedInstance.userGlobal
        tercero = UserGlobalData.sharedInstance.terceroGlobal
        self.tipoDocComercial = tercero.tipoDocumentoTercero!
        self.numDocComercial = tercero.documentoTercero!
    }
    
    func estilos(){
        self.primerView.layer.cornerRadius = 10;
        self.dosView.layer.cornerRadius = 10;
        self.tresView.layer.cornerRadius = 10;
        self.cuatroView.layer.cornerRadius = 10;
    }
    
    func iniTitulares(numDoc : String, tipoDoc : String){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endTitulares(_:)), name: NSNotification.Name("endTitulares"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        
        let user : String = UserGlobalData.sharedInstance.userGlobal.user!
        let sesion : String = UserGlobalData.sharedInstance.userGlobal.SesionId!
        
        let parametros = [
            "Usuario" : user,
            "TDoc" : tipoDoc,
            "NDoc" : numDoc,
            "SesionId" : sesion
            ] as [String : Any]
        
        print(parametros)
        
        OriginData.sharedInstance.nombreTitular(notification: "endTitulares", parametros: parametros)
    }
    
    @objc func endTitulares(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as? TerceroClass
            if data?.CodigoWS == "0"{
                
                if tipoDocComercial == "D"{
                    UserGlobalData.sharedInstance.terceroGlobal.ApeMat = data?.ApeMat!
                    UserGlobalData.sharedInstance.terceroGlobal.ApePat =
                        data?.ApePat!
                    UserGlobalData.sharedInstance.terceroGlobal.Nombres = data?.Nombres!
                    self.cargarDatos()
                }
                else if tipoDocComercial == "R"{
            
                    UserGlobalData.sharedInstance.terceroGlobal.NombreRazSoc = data?.NombreRazSoc!
                    self.cargarDatos()
                }
               else
                {
                    UserGlobalData.sharedInstance.terceroGlobal.ApeMat = data?.ApeMat!
                    UserGlobalData.sharedInstance.terceroGlobal.ApePat =
                        data?.ApePat!
                    UserGlobalData.sharedInstance.terceroGlobal.Nombres = data?.Nombres!
                UserGlobalData.sharedInstance.terceroGlobal.NombreRazSoc = data?.NombreRazSoc!
                    self.cargarDatos()
                }
                
            }
            else
            {
                if data?.CodigoWS != nil {
                    if data?.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "En estos momentos no se puede realizar la consulta.", preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                        //iniMensajeError(codigo: (data?.CodigoWS!)!)
                    }
                }
            }
        }
    }
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
