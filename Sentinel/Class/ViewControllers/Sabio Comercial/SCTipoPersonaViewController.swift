//
//  SCTipoPersonaViewController.swift
//  Sentinel
//
//  Created by Daniel on 12/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//SCTipoPersonaViewController

import UIKit

class SCTipoPersonaViewController: ParentViewController,MiPickerComboDelegate {
    
    @IBOutlet weak var comboPersona: CTComboPicker!
    @IBOutlet weak var personaJuridicaTXT: UITextField!
    @IBOutlet weak var otrosTXT: UITextField!
    @IBOutlet weak var personaNaturalView: UIView!
    
    @IBOutlet weak var apePaternoTXT: UITextField!
    @IBOutlet weak var apeMaternoTXT: UITextField!
    @IBOutlet weak var nombresTXT: UITextField!
    
    @IBOutlet weak var topScroll: NSLayoutConstraint!
    
    var tercero : TerceroClass?
    var userData : UserClass?
    
    var esPremium : Bool = false
    var esEmpresarial : Bool = false
    
    var codCombo = "1"
    var desCombo = "Persona Natural"
    var combosArray: NSMutableArray = NSMutableArray()
    var combosIDArray: NSMutableArray = NSMutableArray()
    
    var isKeyboardAppear = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializarVariables()
        self.gestureScreen()
        self.cargarDatos()
    }
    
    override func viewWillAppear(_ animated: Bool) {

        self.iniConsultaDatosServicioParent()
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func inicializarVariables(){
        
        
        
        self.tercero = UserGlobalData.sharedInstance.terceroGlobal
        self.userData = UserGlobalData.sharedInstance.userGlobal
        
        self.comboPersona.delegate = self
        
        self.combosArray.add("Persona Natural")
        self.combosArray.add("Persona Jurídica")
        self.combosArray.add("Otros")
        
        self.combosIDArray.add("1")
        self.combosIDArray.add("2")
        self.combosIDArray.add("3")
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if codCombo == "1"{
            if !isKeyboardAppear {
                if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                    if self.topScroll.constant == 0{
                        self.topScroll.constant -= (keyboardSize.height + 20)
                        UIView.animate(withDuration: 0.5) {
                            self.view.layoutIfNeeded()
                        }
                    }
                }
                isKeyboardAppear = true
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if codCombo == "1"{
            if isKeyboardAppear {
                if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                    if self.topScroll.constant != 0{
                        self.topScroll.constant += (keyboardSize.height + 20)
                        UIView.animate(withDuration: 0.5) {
                            self.view.layoutIfNeeded()
                        }
                    }
                }
                isKeyboardAppear = false
            }
        }
    }
    
    func cargarDatos(){
        
    }
    
    @IBAction func accionCombo(_ sender: Any) {
        
        self.comboPersona.setConfig(combosArray as! [Any])
        self.comboPersona.open()
        self.comboPersona.setId(1)
    }
    @IBAction func accionComboDos(_ sender: Any) {
        
        self.comboPersona.setConfig(combosArray as! [Any])
        self.comboPersona.open()
        self.comboPersona.setId(1)
    }
    @IBAction func accionBuscar(_ sender: Any) {
        
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "ConsultaResultadoPersonasVC") as! ConsultaResultadoPersonasViewController
        
        nav.apePaterno = self.apePaternoTXT.text!
        nav.apeMaterno = self.apeMaternoTXT.text!
        nav.nombre = self.nombresTXT.text!
        nav.tipoPersona = self.codCombo
        nav.otros = self.otrosTXT.text!
        nav.personaJuridica = self.personaJuridicaTXT.text!
        
        self.navigationController?.pushViewController(nav, animated: true)
        
    }
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func miPickerCombo(_ pickerid: Int, didSelectRow row: Int) {
        switch pickerid {
        case 1:
            codCombo = combosIDArray[row] as! String
            desCombo = combosArray[row] as! String
            if codCombo == "1"{
                self.personaNaturalView.isHidden = false
                self.personaJuridicaTXT.isHidden = true
                self.otrosTXT.isHidden = true
            }
            else if codCombo == "2"{
                self.personaNaturalView.isHidden = true
                self.personaJuridicaTXT.isHidden = false
                self.otrosTXT.isHidden = true
            }
            else if codCombo == "3"{
                self.personaNaturalView.isHidden = true
                self.personaJuridicaTXT.isHidden = true
                self.otrosTXT.isHidden = false
            }
            break
        case 2:
            break
        default:
            break
        }
    }
    
    func miPickerCombo(_ MiPickerView: CTComboPicker!, pickerView picker: UIPickerView!, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
}
