//
//  MensajesViewController.swift
//  Sentinel
//
//  Created by Daniel on 12/11/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class MensajesViewController:ParentViewController {
    //MARK: - Outlets
    @IBOutlet weak var mensajesTable:UITableView!
    @IBOutlet weak var toolbar:UIToolbar!
    @IBOutlet weak var deslizaLB:UILabel!
    //MARK: - Props
    var arregloMensajes = NSMutableArray()
    let userData = UserGlobalData.sharedInstance.userGlobal!
    //MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
        if userData.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(uiView: self.toolbar)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.iniListarMensajes()
    }
    //MARK: - Auxiliar
    func iniListarMensajes(){
        self.showActivityIndicator()
        let usuario = userData.user!
        let sesion = userData.SesionId!
        let tDoc = userData.TDocusu!
        let parametros:[String:Any] = ["Usuario": usuario, "SesionId": sesion, "CntMsjUseTipDoc": tDoc, "CntMsjUseCod": usuario, "CntMsjEst": "L"]
        NotificationCenter.default.addObserver(self, selector: #selector(self.endListarMensajes(_:)), name: NSNotification.Name("endListarMensajes"), object: nil)
        OriginData.sharedInstance.listaMensajesMenu(notification: "endListarMensajes", parametros: parametros)
    }
    @objc func endListarMensajes(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        guard let data = notification.object as? UserClass else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if let codigoWs = data.CodigoWS {
            if codigoWs == "0" {
                if (data.SDTMensajes?.count)! > 0 {
                    UserGlobalData.sharedInstance.userGlobal.SDTMensajes = data.SDTMensajes!
                    self.arregloMensajes = NSMutableArray(array: data.SDTMensajes!)
                    self.mensajesTable.reloadData()
                }else{
                    self.deslizaLB.text = "Por el momento, no cuenta con mensajes en su bandeja de entrada."
                }
            }else if codigoWs == "99" {
                showError99()
            }else{
                iniMensajeError(codigo: codigoWs)
            }
        }else{
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: .alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
        }
    }
    func iniEliminarMensaje(correlativo:String){
        showActivityIndicator()
        let usuario = userData.user!
        let sesion = userData.SesionId!
        let tDoc = userData.TDocusu!
        let params:[String: Any] = ["Usuario" : usuario, "SesionId": sesion, "CntMsjUseTipDoc": tDoc, "CntMsjUseCod": usuario, "CntMsjNroCor": correlativo, "CntMsjEst" : "P", "origenAplicacion" : PATHS.APP_ID]
        ServiceConnector.connectToUrl("\(PATHS.PATHSENTINEL)RWS_MSModificaEstMensajes", params: params) { (r, e) in
            self.hideActivityIndicator()
            if let dic = r as? [String: Any], let codigoWs = dic["CodigoWS"] as? String {
                if codigoWs == "0" {
                    for i in 0 ..< self.arregloMensajes.count {
                        if let dicMessage = self.arregloMensajes[i] as? NSDictionary, let CntMsjNroCor = dicMessage["CntMsjNroCor"] as? String, CntMsjNroCor == correlativo {
                            self.arregloMensajes.removeObject(at: i)
                            self.mensajesTable.deleteRows(at: [IndexPath(row: i, section: 0)], with: .top)
                            break
                        }
                    }
                    //self.iniListarMensajes()
                }else if codigoWs == "99" {
                    self.showError99()
                }else{
                    self.iniMensajeError(codigo: codigoWs)
                }
            }else{
                let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                self.present(alerta, animated: true, completion: nil)
            }
        }
    }
    @IBAction func accionAtras(_ sender:Any){
        navigationController?.popViewController(animated: true)
    }
}
extension MensajesViewController:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arregloMensajes.count
    }
    func tableView(_ tableView:UITableView, heightForRowAt indexPath:IndexPath) -> CGFloat {
        return 65
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MensajesMenuTVC", for: indexPath) as! MensajesMenuTableViewCell
        let objMensaje = arregloMensajes[indexPath.row] as! NSDictionary
        if let CntMsjAsunto = objMensaje.object(forKey: "CntMsjAsunto") as? String {
            cell.tituloLB.text = CntMsjAsunto
        }else{
            cell.tituloLB.text = ""
        }
        if let CntMsjFchLec = objMensaje.object(forKey: "CntMsjFchEnv") as? String {
            cell.fechaLB.text = self.devuelveFechaCortaLetras(fecha: CntMsjFchLec)
        }else{
            cell.fechaLB.text = ""
        }
        if let CntMsjEst = objMensaje.object(forKey: "CntMsjEst") as? String, CntMsjEst == "N" {
            cell.lblIsNew.isHidden = false
        }else{
            cell.lblIsNew.isHidden = true
        }
        return cell
    }
    func tableView(_ tableView:UITableView, editActionsForRowAt indexPath:IndexPath) -> [UITableViewRowAction]? {
        let objMensaje = self.arregloMensajes[indexPath.row] as! NSDictionary
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Eliminar") { (action , indexPath) -> Void in
            self.isEditing = false
            let correlativo = objMensaje.object(forKey: "CntMsjNroCor") as! String
            self.iniEliminarMensaje(correlativo: correlativo)
        }
        deleteAction.backgroundColor = .red
        return [deleteAction]
    }
    func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath){
        let nav = storyboard!.instantiateViewController(withIdentifier: "DetalleMensajesVC") as! DetalleMensajesViewController
        nav.objMensaje = self.arregloMensajes[indexPath.row] as! NSDictionary
        navigationController?.pushViewController(nav, animated: true)
    }
}
