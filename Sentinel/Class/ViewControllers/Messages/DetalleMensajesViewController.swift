//
//  DetalleMensajesViewController.swift
//  Sentinel
//
//  Created by Daniel on 12/11/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class DetalleMensajesViewController:ParentViewController, UITextViewDelegate {
    //MARK: - Outlets
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var mensajeLB: UILabel!
    @IBOutlet weak var tituloLB: UILabel!
    @IBOutlet weak var fechaLB: UILabel!
    @IBOutlet weak var lblDetalle:UILabel!
    @IBOutlet weak var ivMensaje:UIImageView!
    //MARK: - Props
    var objMensaje = NSDictionary()
    var esPremium = false
    //MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
        self.validarPremium()
    }
    override func viewWillAppear(_ animated: Bool) {
        cargarDatos()
    }
    //MARK: - Actions
    @IBAction func accionAtras(_ sender:Any){
        navigationController?.popViewController(animated: true)
    }
    //MARK: - Auxiliar
    func cargarDatos(){
        fechaLB.text = (objMensaje.object(forKey: "CntMsjFchEnv") as! String).split(separator: "-").reversed().joined(separator: "/")
        lblDetalle.text = objMensaje.object(forKey: "CntMsjBody") as? String
        tituloLB.text = objMensaje.object(forKey: "CntMsjAsunto") as? String
        showActivityIndicator()
        DispatchQueue.main.async {
            if let strUrl = self.objMensaje.object(forKey: "CntMsjUrlImagen") as? String, let url = URL(string: strUrl), let data = try? Data(contentsOf: url) {
                self.ivMensaje.image = UIImage(data: data)
            }
            self.hideActivityIndicator()
        }
        if let CntMsjEst = objMensaje.object(forKey: "CntMsjEst") as? String, CntMsjEst == "N", let CntMsjNroCor = objMensaje.object(forKey: "CntMsjNroCor") as? String {
            let user = UserGlobalData.sharedInstance.userGlobal!
            let usuario = user.user!
            let sesion = user.SesionId!
            let tDoc = user.TDocusu!
            let params:[String: Any] = ["Usuario" : usuario, "SesionId": sesion, "CntMsjUseTipDoc": tDoc, "CntMsjUseCod": usuario, "CntMsjNroCor": CntMsjNroCor, "CntMsjEst" : "L", "origenAplicacion" : PATHS.APP_ID]
            ServiceConnector.connectToUrl("\(PATHS.PATHSENTINEL)RWS_MSModificaEstMensajes", params: params) { (r, e) in
                if let dic = r as? [String: Any] {
                    print("dic: \(dic)")
                }
            }
        }
    }
    func validarPremium(){
        if UserGlobalData.sharedInstance.userGlobal?.EsPremium == "S" {
            self.esPremium = true
            self.changeStatusBar(cod: 3)
            self.setGradient(uiView: self.toolbar)
            self.mensajeLB.textColor = UIColor.fromHex(0xD18E18)
        }else{
            self.esPremium = false
        }
    }
}
