//
//  SFOpcionesCreditosViewController.swift
//  Sentinel
//
//  Created by Daniel on 4/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class SFOpcionesCreditosViewController: ParentViewController {

    @IBOutlet weak var titulosView: UIView!
    @IBOutlet weak var opcionesTV: UITableView!
    @IBOutlet weak var textoSeleccion: UILabel!
    
    // MARK: - Variables
    var TextoInformativo : String!
    var CodOperacion : String!
    var codigoProp : String!
    var plazoCronograma : String!
    var Alternativas : NSArray!
    var idProducto : String!
    var userData : UserClass!
    var tercero : TerceroClass!
    
    // MARK: - Métodos del ciclo de vida
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializar()
        self.estilos()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.llenarDatos()
        self.opcionesTV.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // MARK: - Métodos generales
    func llenarDatos(){
        self.textoSeleccion.text = self.TextoInformativo
    }
    func inicializar(){
        self.userData = UserGlobalData.sharedInstance.userGlobal!
        self.tercero = UserGlobalData.sharedInstance.terceroGlobal!
    }
    func estilos (){
        self.titulosView.layer.cornerRadius = 10
    }
    
    // MARK: - Métodos WS
    func iniCronograma(codigo : String){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endCronograma(_:)), name: NSNotification.Name("endCronograma"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        let userData = UserGlobalData.sharedInstance.userGlobal
        let servicios = UserGlobalData.sharedInstance.misServiciosGlobal
        let sesion : String = (userData?.SesionId!)!
        let servicio : String = (servicios?.NroServicio)!
        let usuario : String = (userData?.user)!
        let productoCodigo : String = self.idProducto!
        let codigoOperacionS : String = self.CodOperacion!
        let parametros = [
            "Usuario" : usuario,
            "SesionId" : sesion,
            "ProdId" : productoCodigo,
            "CodOper" : codigoOperacionS,
            "CodProp" : codigo,
            "Servicio" : servicio
            ] as [String : Any]
        OriginData.sharedInstance.getCronograma(notification: "endCronograma", parametros: parametros)
    }
    
    @objc func endCronograma(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data : SabioFinancieroEntidad = (notification.object as? SabioFinancieroEntidad)!
            if data.CodigoWS == "0"{
                if data.SDTCronogramaCredito?.count == 0 {
                    self.showAlert(PATHS.SENTINEL, mensaje: "En estos momentos no existen créditos disponibles.")
                }
                else
                {
                    let nav = self.storyboard?.instantiateViewController(withIdentifier: "SFCronogramaVC") as! SFCronogramaViewController
                    
                    nav.SDTCronogramaCredito = data.SDTCronogramaCredito
                    nav.idProducto = self.idProducto
                    nav.CodOper = self.CodOperacion
                    nav.codigoProp = self.codigoProp
                    nav.creditoGeneral = data
                    nav.plazoCronograma = self.plazoCronograma
                    
                    self.navigationController?.pushViewController(nav, animated: true)
                }
                
            }
            else
            {
                if data.CodigoWS != "" {
                    if data.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "En estos momentos no se puede realizar la operación.", preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                        //iniMensajeError(codigo: (data?.CodigoWS!)!)
                    }
                }
            }
        }
    }
    
    // MARK: - Acciones
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SFOpcionesCreditosViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Alternativas.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SFOpcionesCreditosTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SFOpcionesCreditosTVC", for: indexPath) as! SFOpcionesCreditosTableViewCell
        if let objAlternativa : NSDictionary = Alternativas[indexPath.row] as? NSDictionary{
            if let monto : String = objAlternativa.object(forKey: "Monto") as? String {
                cell.montoLB.text = "S/ \(monto)"
            }
            if let cuota : String = objAlternativa.object(forKey: "Cuota") as? String{
                cell.cuotaLB.text = "S/ \(cuota)"
            }
            if let plazo : String = String(objAlternativa.object(forKey: "Plazo") as! Int){
                cell.plazoLB.text = "\(plazo) meses"
            }
            if let FlgMiAlternativa : String = objAlternativa.object(forKey: "FlgMiAlternativa") as? String{
                if FlgMiAlternativa == "S"{
                    cell.opcionesView.backgroundColor = UIColor.fromHex(0xD9EDF7)
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let objAlternativa : NSDictionary = Alternativas[indexPath.row] as? NSDictionary{
            
            self.CodOperacion = objAlternativa.object(forKey: "CodOperacion") as! String
            self.codigoProp = String(Int(objAlternativa.object(forKey: "CodPropuesto") as! Int))
            self.plazoCronograma = String(Int(objAlternativa.object(forKey: "Plazo") as! Int))
            self.iniCronograma(codigo: codigoProp)
        }
    }
    
}





















