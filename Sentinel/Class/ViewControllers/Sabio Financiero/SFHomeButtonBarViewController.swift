//
//  SFHomeButtonBarViewController.swift
//  Sentinel
//
//  Created by Daniel on 4/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class SFHomeButtonBarViewController: ButtonBarPagerTabStripViewController {

    // MARK: - Declaración de variables
    var ParentVC: UIViewController = UIViewController()
    
    var primerViewController : SFClienteViewController!
    var segundoViewController : SFConyugeViewController!
    
    var isReload = false
    
    let graySpotifyColor = UIColor.fromHex(0x1A1A1A)
    var tabActivo = 1
    
    // MARK: - Métodos de ciclo de vida
    override func viewDidLoad() {
        settings.style.buttonBarHeight = 44.0
        settings.style.buttonBarBackgroundColor = UIColor.fromHex(0x363636)
        settings.style.buttonBarItemBackgroundColor = UIColor.clear
        settings.style.selectedBarBackgroundColor =  UIColor.fromHex(0x00A900)
        settings.style.buttonBarItemFont = UIFont(name: "HelveticaNeue-Bold", size:12) ??
            UIFont.systemFont(ofSize: 12)
        
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor.clear
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        
        settings.style.buttonBarLeftContentInset = 5
        settings.style.buttonBarRightContentInset = 5
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.fromHex(0x808080)
            newCell?.label.textColor = UIColor.fromHex(968266)
            
            oldCell?.imageView.tintColor = UIColor.fromHex(0x808080)
            newCell?.imageView.tintColor = UIColor.fromHex(968266)
        }
        
        super.viewDidLoad()
        
        self.reloadPagerTabStripView()
    }
    
    // MARK: - PagerTabStripDataSource
    
    override func viewControllers(for pagerTabStripController:PagerTabStripViewController) -> [UIViewController] {
        let storyboardMain = UIStoryboard(name: "SabioFinanciero", bundle: nil)
        
        primerViewController = storyboardMain.instantiateViewController(withIdentifier: "SFClienteVC") as? SFClienteViewController
        segundoViewController = UIStoryboard.init(name: "SabioFinanciero", bundle: nil).instantiateViewController(withIdentifier: "SFConyugeVC") as? SFConyugeViewController
        
        guard isReload else {
            if UserGlobalData.sharedInstance.terceroGlobal.hayConyuge == "SI" {
                return [primerViewController, segundoViewController]
            }else{
                return [primerViewController]
            }
        }
        var childViewControllers : [Any]!
        if UserGlobalData.sharedInstance.terceroGlobal.hayConyuge == "SI" {
            childViewControllers = [primerViewController, segundoViewController] as [Any]
        }else{
             childViewControllers = [primerViewController] as [Any]
        }
        for (index, _) in childViewControllers.enumerated(){
            let nElements = childViewControllers.count - index
            let n = (Int(arc4random()) % nElements) + index
            if n != index{
                childViewControllers.swapAt(index, n)
            }
        }
        let nItems = 2
        return Array(childViewControllers.prefix(Int(nItems))) as! [UIViewController]
    }
    
    func actualizarTabContent(){
    }
    
    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool) {
        
        super.updateIndicator(for: viewController, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: progressPercentage, indexWasChanged: indexWasChanged)
        
        self.tabActivo = self.currentIndex + 1
    }
}
