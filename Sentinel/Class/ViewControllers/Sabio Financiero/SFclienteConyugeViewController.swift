//
//  SFclienteConyugeViewController.swift
//  Sentinel
//
//  Created by Daniel on 4/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class SFclienteConyugeViewController: ParentViewController, MiPickerComboDelegate, UITextFieldDelegate {

    @IBOutlet weak var cliconView: UIView!
    @IBOutlet weak var ClienteView: UIView!
    @IBOutlet weak var comboCliView: CTComboPicker!
    @IBOutlet weak var tipoDocBT: UIButton!
    @IBOutlet weak var tipoDocFlechaTop: UIButton!
    @IBOutlet weak var numeroClienteTX: UITextField!
    
    @IBOutlet weak var siguienteBT: UIButton!
    
    @IBOutlet weak var ConyugeView: UIView!
    @IBOutlet weak var comboConyView: CTComboPicker!
    @IBOutlet weak var tipoDocSegBT: UIButton!
    
    @IBOutlet weak var numeroConyugeTX: UITextField!
    
    var tipoDocBancoArray: NSMutableArray = NSMutableArray()
    var tipoDocBancoConyugeArray: NSMutableArray = NSMutableArray()
    var numDocBancoArray: NSMutableArray = NSMutableArray()
    var numDocBancoSegundoArray: NSMutableArray = NSMutableArray()
    
    var nomTipDocCon : String = ""
    var nomTipDocCli : String = ""
    var nombreCompletoConsultado : String = ""
    var nombreCompletoConyuge : String = ""
    
    var numeroDocCliente : String = ""
    var numeroDocConyuge : String = ""
    
    var tDoc : String = ""
    var tDocCony : String = ""
    
    var codMensajeError : String = ""
    
    var cliente : NSDictionary = NSDictionary()
    var conyuge : NSDictionary = NSDictionary()
    
    var clienteSabio : SabioFinancieroEntidad!
    var conyugeSabio : SabioFinancieroEntidad!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gestureScreen()
        self.inicializarVariable()
        self.estilos()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.clipsToBounds = false
        self.tabBarController?.tabBar.layer.borderWidth = 0.1
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func inicializarVariable(){
        
        self.numeroClienteTX.delegate = self
        self.numeroConyugeTX.delegate = self
        
        nomTipDocCli = "D"
        nomTipDocCon = "D"
        
        tipoDocBancoArray.add("DNI")
        tipoDocBancoArray.add("RUC")
        tipoDocBancoArray.add("CE")
        tipoDocBancoArray.add("PASAPORTE")
        
        tipoDocBancoConyugeArray.add("DNI")
        tipoDocBancoConyugeArray.add("CE")
        tipoDocBancoConyugeArray.add("PASAPORTE")
        
        numDocBancoArray.add("D")
        numDocBancoArray.add("R")
        numDocBancoArray.add("4")
        numDocBancoArray.add("5")
        
        numDocBancoSegundoArray.add("D")
        numDocBancoSegundoArray.add("4")
        numDocBancoSegundoArray.add("5")
    }
    
    func estilos(){
        self.ClienteView.layer.cornerRadius = 5.0
        self.ClienteView.layer.shadowColor = UIColor.lightGray.cgColor
        self.ClienteView.layer.shadowOffset = CGSize.zero
        self.ClienteView.layer.shadowRadius = 2.5
        self.ClienteView.layer.shadowOpacity = 0.5
        self.comboCliView.delegate = self;
        
        self.ConyugeView.layer.cornerRadius = 5.0
        self.ConyugeView.layer.shadowColor = UIColor.lightGray.cgColor
        self.ConyugeView.layer.shadowOffset = CGSize.zero
        self.ConyugeView.layer.shadowRadius = 2.5
        self.ConyugeView.layer.shadowOpacity = 0.5
        self.comboConyView.delegate = self;
        
        self.tipoDocBT.layer.cornerRadius = 5;
        self.tipoDocSegBT.layer.cornerRadius = 5;
        
        self.siguienteBT.layer.cornerRadius = 18;
    }
    
    func validar() -> Bool{
        
        let numeroDocumentoCli : String = self.numeroClienteTX.text!
        let numeroDocumentoCon : String = self.numeroConyugeTX.text!
        
        let cantidadCli = self.numeroClienteTX.text?.count
        let cantidadCon = self.numeroConyugeTX.text?.count
        
        if numeroDocumentoCli == ""{
            self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar el número de documento del cliente a consultar.")
            return false
        }
        
        if nomTipDocCli == "D"{
            if (cantidadCli == 8) {
                
            }
            else
            {
                self.showAlert(PATHS.SENTINEL, mensaje: "Número de documento debe contener 8 dígitos.")
                return false
            }
        }
        
        if nomTipDocCli == "R"{
            if (cantidadCli == 11) {
                
            }
            else
            {
                self.showAlert(PATHS.SENTINEL, mensaje: "Número de documento debe contener 11 dígitos.")
                return false
            }
        }
        
        if numeroDocumentoCon != ""{
            if nomTipDocCon == "D"{
                if (cantidadCon == 8) {
                    
                }
                else
                {
                    self.showAlert(PATHS.SENTINEL, mensaje: "Número de documento debe contener 8 dígitos.")
                    return false
                }
            }
            
            if nomTipDocCon == "R"{
                if (cantidadCon == 11) {
                    
                }
                else
                {
                    self.showAlert(PATHS.SENTINEL, mensaje: "Número de documento debe contener 11 dígitos.")
                    return false
                }
            }
        }
        return true
    }
    
    func iniConsultaSabioFCli(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endConsultaSabioFCli(_:)), name: NSNotification.Name("endConsultaSabioFCli"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        let userData = UserGlobalData.sharedInstance.userGlobal
        let servicios = UserGlobalData.sharedInstance.misServiciosGlobal
        let sesion : String = (userData?.SesionId!)!
        let servicio : String = (servicios?.NroServicio)!
        let usuario : String = (userData?.user)!
        self.numeroDocCliente = self.numeroClienteTX.text!
        let parametros = [
            "Usuario" : usuario,
            "SesionId" : sesion,
            "Servicio" : servicio,
            "TDoc" : self.nomTipDocCli,
            "NroDoc" : self.numeroDocCliente
            ] as [String : Any]
        OriginData.sharedInstance.getClienteConyugeSabio(notification: "endConsultaSabioFCli", parametros: parametros)
    }
    
    @objc func endConsultaSabioFCli(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data : SabioFinancieroEntidad = (notification.object as? SabioFinancieroEntidad)!
            if data.CodigoWS == "0"{
                UserGlobalData.sharedInstance.terceroGlobal.objSabioEntidad = data
                UserGlobalData.sharedInstance.terceroGlobal.tipoDocClienteSabio = nomTipDocCli
                UserGlobalData.sharedInstance.terceroGlobal.numClienteSabio = numeroDocCliente
                if self.numeroConyugeTX.text?.count != 0 {
                    self.iniConsultaSabioFConyuge()
                }
                else
                {
                     UserGlobalData.sharedInstance.terceroGlobal.hayConyuge = "NO"
                    let nav = self.storyboard?.instantiateViewController(withIdentifier: "SFHomeVC") as! SFHomeViewController
                    
                    self.navigationController?.pushViewController(nav, animated: true)
                }
                
            }
            else
            {
                if data.CodigoWS != "" {
                    if data.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "En estos momentos no se puede realizar la operación.", preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                        //iniMensajeError(codigo: (data?.CodigoWS!)!)
                    }
                }
            }
        }
    }
    
    func iniConsultaSabioFConyuge(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endConsultaSabioFConyuge(_:)), name: NSNotification.Name("endConsultaSabioFConyuge"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        let userData = UserGlobalData.sharedInstance.userGlobal
        let servicios = UserGlobalData.sharedInstance.misServiciosGlobal
        let sesion : String = (userData?.SesionId!)!
        let servicio : String = (servicios?.NroServicio)!
        let usuario : String = (userData?.user)!
        self.numeroDocConyuge = self.numeroConyugeTX.text!
        let parametros = [
            "Usuario" : usuario,
            "SesionId" : sesion,
            "Servicio" : servicio,
            "TDoc" : self.nomTipDocCon,
            "NroDoc" : self.numeroDocConyuge
            ] as [String : Any]
        OriginData.sharedInstance.getClienteConyugeSabio(notification: "endConsultaSabioFConyuge", parametros: parametros)
    }
    
    @objc func endConsultaSabioFConyuge(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data : SabioFinancieroEntidad = (notification.object as? SabioFinancieroEntidad)!
            if data.CodigoWS == "0"{
                UserGlobalData.sharedInstance.terceroGlobal.hayConyuge = "SI"
               UserGlobalData.sharedInstance.terceroGlobal.tipoDocConyugeSabio = nomTipDocCon
                UserGlobalData.sharedInstance.terceroGlobal.numConyugeSabio = numeroDocConyuge
                 UserGlobalData.sharedInstance.terceroGlobal.objSabioEntidadConyuge = data
                let nav = self.storyboard?.instantiateViewController(withIdentifier: "SFHomeVC") as! SFHomeViewController
                
                self.navigationController?.pushViewController(nav, animated: true)
            }
            else
            {
                if data.CodigoWS != "" {
                    if data.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "En estos momentos no se puede realizar la operación.", preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                        //iniMensajeError(codigo: (data?.CodigoWS!)!)
                    }
                }
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.numeroClienteTX{
        if nomTipDocCli == "R" {
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 10
        }
        if nomTipDocCli == "D"{
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 7
        }
        if nomTipDocCli == "4"{
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 19
        }
        }
        
        if textField == self.numeroConyugeTX{
            if nomTipDocCon == "R" {
                let char = string.cString(using: String.Encoding.utf8)
                let isBackSpace = strcmp(char, "\\b")
                if isBackSpace == -92 {
                    return true
                }
                return textField.text!.count <= 10
            }
            if nomTipDocCon == "D"{
                let char = string.cString(using: String.Encoding.utf8)
                let isBackSpace = strcmp(char, "\\b")
                if isBackSpace == -92 {
                    return true
                }
                return textField.text!.count <= 7
            }
            if nomTipDocCon == "4"{
                let char = string.cString(using: String.Encoding.utf8)
                let isBackSpace = strcmp(char, "\\b")
                if isBackSpace == -92 {
                    return true
                }
                return textField.text!.count <= 19
            }
        }
        
        return true
    }
    
    func miPickerCombo(_ pickerid: Int, didSelectRow row: Int) {
        switch pickerid {
        case 1:
            nomTipDocCli = self.numDocBancoArray[row] as! String
            if nomTipDocCli == "R"{
                self.ConyugeView.isHidden = true
                numeroDocConyuge = ""
                self.numeroConyugeTX.text = ""
            }
            else
            {
                self.ConyugeView.isHidden = false
                
            }
            
            break
        case 2:
            nomTipDocCon = self.numDocBancoSegundoArray[row] as! String
            
            break
        default:
            break
        }
    }
    
    func miPickerCombo(_ MiPickerView: CTComboPicker!, pickerView picker: UIPickerView!, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
    @IBAction func accionCombo(_ sender: Any) {
        
        self.comboCliView.setConfig(tipoDocBancoArray as! [Any])
        self.comboCliView.open()
        self.comboCliView.setId(1)
    }
    @IBAction func accionComboDos(_ sender: Any) {
        self.comboCliView.setConfig(tipoDocBancoArray as! [Any])
        self.comboCliView.open()
        self.comboCliView.setId(1)
    }
    
    @IBAction func accionComboTres(_ sender: Any) {
        
        self.comboConyView.setConfig(tipoDocBancoConyugeArray as! [Any])
        self.comboConyView.open()
        self.comboConyView.setId(2)
    }
    @IBAction func accionComboCuatro(_ sender: Any) {
        self.comboConyView.setConfig(tipoDocBancoConyugeArray as! [Any])
        self.comboConyView.open()
        self.comboConyView.setId(2)
    }
    @IBAction func accionSiguiente(_ sender: Any) {
        if validar(){
            self.iniConsultaSabioFCli()
        }
    }
    
    @IBAction func accionAtras(_ sender: Any) {
        let validacionProviene : String = UserGlobalData.sharedInstance.userGlobal.vieneDelUltimoSF!
        if validacionProviene == "SI" {
            self.navigationController?.popToRootViewController(animated: true)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }
}
