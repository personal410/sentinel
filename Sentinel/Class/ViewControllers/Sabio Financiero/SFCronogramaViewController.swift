//
//  SFCronogramaViewController.swift
//  Sentinel
//
//  Created by Daniel on 4/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class SFCronogramaViewController: ParentViewController{

    @IBOutlet weak var montoLB: UILabel!
    @IBOutlet weak var teaLB: UILabel!
    @IBOutlet weak var temLB: UILabel!
    @IBOutlet weak var plazoLB: UILabel!
    @IBOutlet weak var periodoPagoLB: UILabel!
    @IBOutlet weak var cuotaMensualLB: UILabel!
    @IBOutlet weak var fechaIniciadaLB: UILabel!
    @IBOutlet weak var desembolso: UILabel!
    @IBOutlet weak var cronogramaTV: UITableView!
    
    @IBOutlet weak var enviarBT: UIButton!
    @IBOutlet weak var aceptarBT: UIButton!
    
    var SDTCronogramaCredito : NSArray!
    var idProducto : String!
    var CodOper : String!
    var codigoProp : String!
    var creditoGeneral : SabioFinancieroEntidad!
    var plazoCronograma : String!
    
    var userData : UserClass!
    var tercero : TerceroClass!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.inicializar()
        self.estilos()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.cargarData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func inicializar(){
        self.userData = UserGlobalData.sharedInstance.userGlobal!
        self.tercero = UserGlobalData.sharedInstance.terceroGlobal!
    }
    func estilos(){
        self.enviarBT.layer.cornerRadius = 5
        self.aceptarBT.layer.cornerRadius = 5
    }
    func cargarData(){
        if let MontoPrestamo : String = creditoGeneral.MontoPrestamo {
            self.montoLB.text = "S/ \(MontoPrestamo)"
        }
        if let TEA : String = creditoGeneral.Tea {
            self.teaLB.text = "\(TEA)"
        }
        if let TEM :String = creditoGeneral.Tem{
            self.temLB.text = "\(TEM)"
        }
        if let Plazo : String = plazoCronograma{
            self.plazoLB.text = "\(Plazo) meses"
        }
        if let periodo : String = creditoGeneral.PeriodoPago{
            self.periodoPagoLB.text = "\(periodo) días"
        }
        if let cuotaMensual : String = creditoGeneral.Cuota{
            self.cuotaMensualLB.text = "S/ \(cuotaMensual)"
        }
        if let fecha : String = self.devuelveFecha(fecha: creditoGeneral.FechaCuotaInicial!){
            self.fechaIniciadaLB.text = fecha
        }
        
        self.cronogramaTV.reloadData()
    }
    
    @IBAction func accionEnviar(_ sender: Any) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "SFAlertaCorreoVC") as! SFAlertaCorreoViewController
        
        nav.SDTCronogramaCredito = self.SDTCronogramaCredito
        nav.codigoProp = self.codigoProp
        nav.CodOper = self.CodOper
        nav.idProducto = self.idProducto
        
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    @IBAction func accionAceptar(_ sender: Any) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "SFAceptaCreditoVC") as! SFAceptaCreditoViewController
        
        nav.idProducto = self.idProducto
        nav.codigoProp = self.codigoProp
        nav.CodOper = self.CodOper
        
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    @IBAction func accionAtras(_ sender: Any) {
        //self.navigationController?.popToRootViewController(animated: true)
        self.navigationController?.popViewController(animated: true)
    }
}

extension SFCronogramaViewController : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var arreglo : NSArray = NSArray()
        if let SDTCronogramaCredito : NSArray = self.creditoGeneral.SDTCronogramaCredito{
            arreglo = SDTCronogramaCredito
        }
        return arreglo.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SFCronogramaTVC") as! SFCronogramaTableViewCell
        if let objSDTCronogramaCredito : NSDictionary = self.creditoGeneral.SDTCronogramaCredito![indexPath.row] as? NSDictionary{
            if let nro : Int = objSDTCronogramaCredito.object(forKey: "Nro") as? Int{
                cell.nroCronoLB.text = String(nro)
            }
//            if let fechaP : String = objSDTCronogramaCredito.object(forKey: "Nro") as? String{
//                cell.nroCronoLB.text = fechaP
//            }
//            if let nro : String = objSDTCronogramaCredito.object(forKey: "Nro") as? String{
//                cell.nroCronoLB.text = nro
//            }
            if let nro : String = objSDTCronogramaCredito.object(forKey: "Saldo") as? String{
                cell.saldoCronoLB.text = nro
            }
            if let nro : String = objSDTCronogramaCredito.object(forKey: "Amortizacion") as? String{
                cell.amortizacionLB.text = nro
            }
            if let nro : String = objSDTCronogramaCredito.object(forKey: "Interes") as? String{
                cell.interesLB.text = nro
            }
            if let nro : String = objSDTCronogramaCredito.object(forKey: "Cuota") as? String{
                cell.cuotaLB.text = nro
            }
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}







