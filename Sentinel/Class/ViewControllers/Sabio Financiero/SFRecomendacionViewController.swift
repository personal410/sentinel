//
//  SFRecomendacionViewController.swift
//  Sentinel
//
//  Created by Daniel on 4/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class SFRecomendacionViewController: ParentViewController {

    @IBOutlet weak var nombreLB: UILabel!
    @IBOutlet weak var apellidosLB: UILabel!
    @IBOutlet weak var tipoDocumentoLB: UILabel!
    @IBOutlet weak var numeroDocLB: UILabel!
    @IBOutlet weak var recomendacionLB: UILabel!
    @IBOutlet weak var aprobacionLB: UILabel!
    @IBOutlet weak var imageIMG: UIImageView!
    
    @IBOutlet weak var nivelRiesgoLB: UILabel!
    
    @IBOutlet weak var dictamenLB: UILabel!
    @IBOutlet weak var detalleMotivosLB: UILabel!
    
    @IBOutlet weak var endeudamientoLB: UILabel!
    
    @IBOutlet weak var originacionLB: UILabel!
    
    @IBOutlet weak var verOpcionBT: UIButton!
    
    @IBOutlet weak var primerView: UIView!
    @IBOutlet weak var segundoView: UIView!
    @IBOutlet weak var tercerView: UIView!
    
    @IBOutlet weak var segundoInterno: UIView!
    
    
    @IBOutlet weak var controTermometroVW: SFControlTermometro!
    
    @IBOutlet weak var topSobreEndeudamiento: NSLayoutConstraint!
    
    
    var tercero : TerceroClass!
    var userData : UserClass!
    var idProducto : String!
    var validaCreditos : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializar()
        self.estilos()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.cargarDatos()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func inicializar(){
        tercero = UserGlobalData.sharedInstance.terceroGlobal!
        userData = UserGlobalData.sharedInstance.userGlobal!
    }
    
    func estilos(){
        primerView.layer.cornerRadius = 5.0
        primerView.layer.shadowColor = UIColor.black.cgColor
        primerView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        primerView.layer.shadowRadius = 2.5
        primerView.layer.shadowOpacity = 0.2
        
        segundoView.layer.cornerRadius = 5.0
        segundoView.layer.shadowColor = UIColor.black.cgColor
        segundoView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        segundoView.layer.shadowRadius = 2.5
        segundoView.layer.shadowOpacity = 0.2
        
        tercerView.layer.cornerRadius = 5.0
        tercerView.layer.shadowColor = UIColor.black.cgColor
        tercerView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        tercerView.layer.shadowRadius = 2.5
        tercerView.layer.shadowOpacity = 0.2
        
        segundoInterno.layer.cornerRadius = 5
        self.controTermometroVW.layer.cornerRadius = 5
        self.verOpcionBT.layer.cornerRadius = 5
    }
    
    func cargarDatos(){
        let SDTSabioFinanciero : NSDictionary = (tercero.SDTSabioFinanciero)!
        let Recomendacion  : NSArray = SDTSabioFinanciero.object(forKey: "Recomendacion") as! NSArray
        //let SESeguimiento : NSDictionary = SDTSabioFinanciero.object(forKey: "SESeguimiento") as! NSDictionary
        let Productos : NSArray = SDTSabioFinanciero.object(forKey: "Productos") as! NSArray
        //
        let datosSabio = UserGlobalData.sharedInstance.terceroGlobal.objSabioEntidad
        let ConsultaCrediticia = datosSabio?.ConsultaCrediticia
        let apePaterno : String = ConsultaCrediticia!["ApePat"] as! String
        let ApeMat : String = ConsultaCrediticia!["ApeMat"] as! String
        let Nombre : String = ConsultaCrediticia!["Nombre"] as! String
        let NombreRazonSocial : String = ConsultaCrediticia!["NombreRazonSocial"] as! String
        let FechaNac : String = ConsultaCrediticia!["FechaNac"] as! String
        let tipoDocCliente : String = UserGlobalData.sharedInstance.terceroGlobal.tipoDocClienteSabio!
        let numDocumento : String = UserGlobalData.sharedInstance.terceroGlobal.numClienteSabio!
        if tipoDocCliente == "D" || tipoDocCliente == "5" {
            self.nombreLB.text = "\(Nombre)"
            self.apellidosLB.text = "\(apePaterno) \(ApeMat)"
            self.tipoDocumentoLB.text = "\(tipoDocCliente == "D" ? "DNI" : "Pasaporte"): \(numDocumento)"
            self.numeroDocLB.text = numDocumento
            let recomencacionDict : NSDictionary = Recomendacion[0] as! NSDictionary
            self.recomendacionLB.text = recomencacionDict.object(forKey: "Producto") as? String
            let recomendacionString : String = (recomencacionDict.object(forKey: "TextoRecomendacion") as? String)!
            
            if recomendacionString == "APROBAR" || recomendacionString == "Aprobar"{
                self.validaCreditos = "SI"
            }
            else if recomendacionString == "OBSERVAR" || recomendacionString == "Observar"{
                self.validaCreditos = "NO"
            }
            else if recomendacionString == "RECHAZAR" || recomendacionString == "Rechazar"{
                self.validaCreditos = "NO"
            }
            
            self.aprobacionLB.text = recomendacionString
            let colorAprobacion : String = (recomencacionDict.object(forKey: "ColorRecomendacion") as? String)!
            
            self.aprobacionLB.textColor = self.devuelveColorRGB(colorAprobacion: colorAprobacion)
            let objProductos : NSDictionary = Productos[0] as! NSDictionary
            let EvaluacionCrediticia : NSDictionary = objProductos.object(forKey: "EvaluacionCrediticia") as! NSDictionary
            let dictamen : String = EvaluacionCrediticia.object(forKey: "Dictamen") as! String
            let dictamenColor : String = EvaluacionCrediticia.object(forKey: "ColorDictamen") as! String
            self.dictamenLB.text = dictamen
            self.dictamenLB.textColor = self.devuelveColorRGB(colorAprobacion: dictamenColor)
            let DetalleEvaluacion : NSArray = EvaluacionCrediticia.object(forKey: "DetalleEvaluacion") as! NSArray
            var detalleMotivosCadena : String = ""
            for i in 0 ... DetalleEvaluacion.count - 1 {
                let objDetalle : String = DetalleEvaluacion[i] as! String
                if i == 0 {
                    detalleMotivosCadena = "\(objDetalle)"
                }
                else
                {
                    detalleMotivosCadena = "\(detalleMotivosCadena) \n\(objDetalle)"
                }
            }
            self.detalleMotivosLB.text = detalleMotivosCadena
            let SESeguimiento : NSDictionary = SDTSabioFinanciero.object(forKey: "SESeguimiento") as! NSDictionary
            let NivelRiesgoEndeudamiento : String = SESeguimiento.object(forKey: "NivelRiesgo") as! String
            let colorEndeudamiento : String = SESeguimiento.object(forKey: "ColorRiesgo") as! String
            self.endeudamientoLB.text = NivelRiesgoEndeudamiento
            self.endeudamientoLB.textColor = self.devuelveColorRGB(colorAprobacion: colorEndeudamiento)
            let SEOriginacion : NSDictionary = objProductos.object(forKey: "SEOriginacion") as! NSDictionary
            let riesgoOriginacion : String = SEOriginacion.object(forKey: "NivelRiesgo") as! String
            let colorRiesgoOriginacion : String = SEOriginacion.object(forKey: "ColorRiesgo") as! String
            self.originacionLB.text = riesgoOriginacion
            self.originacionLB.textColor = self.devuelveColorRGB(colorAprobacion: colorRiesgoOriginacion)
           
            let Termometro : NSDictionary = SDTSabioFinanciero.object(forKey: "Termometro") as! NSDictionary
            let MutableTermometro : NSMutableDictionary = Termometro.mutableCopy() as! NSMutableDictionary
            self.controTermometroVW.formatoTermometro(MutableTermometro)
            //Image
            let consultado: String = recomencacionDict.object(forKey: "Imagen") as! String
            let url : URL = NSURL(string: consultado)! as URL
            NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarFotoSabio(_:)), name: NSNotification.Name("endCargarFotoSabio"), object: nil)
            
            self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
            Alamofire.request(url).responseImage { response in
                debugPrint(response)
                
                switch response.result {
                case .success:
                    if let image = response.result.value {
                        self.imageIMG.image = image
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoSabio"), object: nil)
                    }
                    break
                    
                case .failure(let error):
                    //CNIGenImaHom
                    //FALTA POR DEFECTO
                    let image : UIImage = UIImage.init(named: "CNIGenImaHom")!
                   //self.imageIMG.image =
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoSabio"), object: nil)
                    break
                }
            }
        }
        else if tipoDocCliente == "R" {
            self.nombreLB.text = "\(NombreRazonSocial)"
            self.apellidosLB.text = ""
            self.tipoDocumentoLB.text = "RUC"
            self.numeroDocLB.text = numDocumento
            let recomencacionDict : NSDictionary = Recomendacion[0] as! NSDictionary
            self.recomendacionLB.text = recomencacionDict.object(forKey: "Producto") as? String
            let recomendacionString : String = (recomencacionDict.object(forKey: "TextoRecomendacion") as? String)!
            
            if recomendacionString == "APROBAR" || recomendacionString == "Aprobar"{
                self.validaCreditos = "SI"
            }
            else if recomendacionString == "OBSERVAR" || recomendacionString == "Observar"{
                self.validaCreditos = "NO"
            }
            else if recomendacionString == "RECHAZAR" || recomendacionString == "Rechazar"{
                self.validaCreditos = "NO"
            }
            
            self.aprobacionLB.text = recomendacionString
            let colorAprobacion : String = (recomencacionDict.object(forKey: "ColorRecomendacion") as? String)!
            
            self.aprobacionLB.textColor = self.devuelveColorRGB(colorAprobacion: colorAprobacion)
            let objProductos : NSDictionary = Productos[0] as! NSDictionary
            let EvaluacionCrediticia : NSDictionary = objProductos.object(forKey: "EvaluacionCrediticia") as! NSDictionary
            let dictamen : String = EvaluacionCrediticia.object(forKey: "Dictamen") as! String
            let dictamenColor : String = EvaluacionCrediticia.object(forKey: "ColorDictamen") as! String
            self.dictamenLB.text = dictamen
            self.dictamenLB.textColor = self.devuelveColorRGB(colorAprobacion: dictamenColor)
            let DetalleEvaluacion : NSArray = EvaluacionCrediticia.object(forKey: "DetalleEvaluacion") as! NSArray
            var detalleMotivosCadena : String = ""
            for i in 0 ... DetalleEvaluacion.count - 1 {
                let objDetalle : String = DetalleEvaluacion[i] as! String
                if i == 0 {
                    detalleMotivosCadena = "\(objDetalle)"
                }
                else
                {
                    detalleMotivosCadena = "\(detalleMotivosCadena) \n\(objDetalle)"
                }
            }
            self.detalleMotivosLB.text = detalleMotivosCadena
            let SESeguimiento : NSDictionary = SDTSabioFinanciero.object(forKey: "SESeguimiento") as! NSDictionary
            let NivelRiesgoEndeudamiento : String = SESeguimiento.object(forKey: "NivelRiesgo") as! String
            let colorEndeudamiento : String = SESeguimiento.object(forKey: "ColorRiesgo") as! String
            self.endeudamientoLB.text = NivelRiesgoEndeudamiento
            self.endeudamientoLB.textColor = self.devuelveColorRGB(colorAprobacion: colorEndeudamiento)
            let SEOriginacion : NSDictionary = objProductos.object(forKey: "SEOriginacion") as! NSDictionary
            let riesgoOriginacion : String = SEOriginacion.object(forKey: "NivelRiesgo") as! String
            let colorRiesgoOriginacion : String = SEOriginacion.object(forKey: "ColorRiesgo") as! String
            self.originacionLB.text = riesgoOriginacion
            self.originacionLB.textColor = self.devuelveColorRGB(colorAprobacion: colorRiesgoOriginacion)
            
            let Termometro : NSDictionary = SDTSabioFinanciero.object(forKey: "Termometro") as! NSDictionary
            let MutableTermometro : NSMutableDictionary = Termometro.mutableCopy() as! NSMutableDictionary
            self.controTermometroVW.formatoTermometro(MutableTermometro)
            //Image
            let consultado: String = recomencacionDict.object(forKey: "Imagen") as! String
            let url : URL = NSURL(string: consultado)! as URL
            NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarFotoSabio(_:)), name: NSNotification.Name("endCargarFotoSabio"), object: nil)
            
            self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
            Alamofire.request(url).responseImage { response in
                debugPrint(response)
                
                switch response.result {
                case .success:
                    if let image = response.result.value {
                        self.imageIMG.image = image
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoSabio"), object: nil)
                    }
                    break
                    
                case .failure(let error):
                    //CNIGenImaHom
                    //FALTA POR DEFECTO
                    let image : UIImage = UIImage.init(named: "CNIGenImaHom")!
                    //self.imageIMG.image =
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoSabio"), object: nil)
                    break
                }
            }
        }
    }
    
    @objc func endCargarFotoSabio(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
    }
    
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func accionVerOpcionesCredito(_ sender: Any) {
        if self.validaCreditos == "SI"{
            let SDTSabioFinanciero : NSDictionary = (tercero.SDTSabioFinanciero)!
            let OpcionPrestamo : NSDictionary = SDTSabioFinanciero.object(forKey: "OpcionPrestamo") as! NSDictionary
            let TextoInformativo : String = OpcionPrestamo.object(forKey: "TextoInformativo") as! String
            //let CodOperacion : String = OpcionPrestamo.object(forKey: "CodOperacion") as! String
            let Alternativas : NSArray = OpcionPrestamo.object(forKey: "Alternativas") as! NSArray
            if Alternativas.count == 0 {
                self.showAlert(PATHS.SENTINEL, mensaje: "En estos momentos no existen créditos disponibles.")
            }
            else
            {
                let nav = self.storyboard?.instantiateViewController(withIdentifier: "SFOpcionesCreditosVC") as! SFOpcionesCreditosViewController
                
                nav.TextoInformativo = TextoInformativo
                //nav.CodOperacion = CodOperacion
                nav.Alternativas = Alternativas
                nav.idProducto = self.idProducto
                
                self.navigationController?.pushViewController(nav, animated: true)
            }
            
        }
        else
        {
            self.showAlert(PATHS.SENTINEL, mensaje: "No califica para opciones de préstamos.")
        }
    }
}
