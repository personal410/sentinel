//
//  SFAceptaCreditoViewController.swift
//  Sentinel
//
//  Created by Daniel on 4/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class SFAceptaCreditoViewController: ParentViewController {

    @IBOutlet weak var aceptaView: UIView!
    
    @IBOutlet weak var SIbt: UIButton!
    @IBOutlet weak var NObt: UIButton!
    
    @IBOutlet weak var enviarBT: UIButton!
    
    var idProducto : String!
    var CodOper : String!
    var codigoProp : String!
    var estadoSI : Int!
    var estadoNO : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializacion()
        self.estilos()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.cargarData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func inicializacion(){
        
    }
    
    func estilos(){
        self.enviarBT.layer.cornerRadius = 5
        aceptaView.layer.cornerRadius = 10.0
        
        aceptaView.layer.shadowColor = UIColor.black.cgColor
        aceptaView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        aceptaView.layer.shadowRadius = 2.0
        aceptaView.layer.shadowOpacity = 0.2
    }
    
    func cargarData(){
        self.estadoSI = 1
        self.estadoNO = 0
        
        self.SIbt.setImage(UIImage.init(named: "ic_sem_verde"), for: .normal)
        self.NObt.setImage(UIImage.init(named: "circulo1"), for: .normal)
    }
    
    func iniAceptarCredito(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endAceptarCredito(_:)), name: NSNotification.Name("endAceptarCredito"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        
        let userData = UserGlobalData.sharedInstance.userGlobal
        let servicios = UserGlobalData.sharedInstance.misServiciosGlobal
        let sesion : String = (userData?.SesionId!)!
        let servicio : String = (servicios?.NroServicio)!
        let usuario : String = (userData?.user)!
        let producto : String = self.idProducto!
        let cOpe : String = self.CodOper!
        var acepta : String = ""
        if estadoSI == 1{
            acepta = "S"
        }
        else
        {
            acepta = "N"
        }
        
        let parametros = [
            "Usuario" : usuario,
            "SesionId" : sesion,
            "SBTSOCodSer" : servicio,
            "SBTSOCodProd" : producto,
            "SBTSOCodOpe" : cOpe,
            "AceptaCredito" : acepta
            ] as [String : Any]
        print(parametros)
        
        OriginData.sharedInstance.aceptaCreditoSF(notification: "endAceptarCredito", parametros: parametros)
    }
    
    @objc func endAceptarCredito(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data : SabioFinancieroEntidad = (notification.object as? SabioFinancieroEntidad)!
            if data.CodigoWS == "0"{
                let alerta = UIAlertController(title: PATHS.SENTINEL, message: "Proceso finalizado satisfactoriamente.", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.cancel, handler: { (action: UIAlertAction!) in
                    let nav = self.storyboard?.instantiateViewController(withIdentifier: "SFclienteConyugeVC") as! SFclienteConyugeViewController
                    UserGlobalData.sharedInstance.userGlobal.vieneDelUltimoSF = "SI"
                    self.navigationController?.pushViewController(nav, animated: false)
                }))
                
                self.present(alerta, animated: true, completion:nil)
            }
            else
            {
                if data.CodigoWS != "" {
                    if data.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "En estos momentos no se puede realizar la operación.", preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                        //iniMensajeError(codigo: (data?.CodigoWS!)!)
                    }
                }
            }
        }
    }
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func accionSI(_ sender: Any) {
        if estadoSI == 1 {
            self.estadoSI = 0
            self.estadoNO = 1
            
            self.SIbt.setImage(UIImage.init(named: "circulo1"), for: .normal)
            self.NObt.setImage(UIImage.init(named: "ic_sem_verde"), for: .normal)
        }
        else
        {
            self.estadoSI = 1
            self.estadoNO = 0
            
            self.SIbt.setImage(UIImage.init(named: "ic_sem_verde"), for: .normal)
            self.NObt.setImage(UIImage.init(named: "circulo1"), for: .normal)
        }
    }
    
    @IBAction func accionNO(_ sender: Any) {
        if estadoNO == 1 {
            self.estadoSI = 1
            self.estadoNO = 0
            
            self.SIbt.setImage(UIImage.init(named: "ic_sem_verde"), for: .normal)
            self.NObt.setImage(UIImage.init(named: "circulo1"), for: .normal)
        }
        else
        {
            self.estadoSI = 0
            self.estadoNO = 1
            
            self.SIbt.setImage(UIImage.init(named: "circulo1"), for: .normal)
            self.NObt.setImage(UIImage.init(named: "ic_sem_verde"), for: .normal)
        }
    }
    
    @IBAction func accionEnviar(_ sender: Any) {
        self.iniAceptarCredito()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
