//
//  SFAlertaCorreoViewController.swift
//  Sentinel
//
//  Created by Daniel on 4/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class SFAlertaCorreoViewController: ParentViewController {

    @IBOutlet weak var envioVW: UIView!
    @IBOutlet weak var correoTX: UITextField!
    @IBOutlet weak var cancelarBT: UIButton!
    @IBOutlet weak var aceptarBT: UIButton!
    
    var SDTCronogramaCredito : NSArray!
    var idProducto : String!
    var CodOper : String!
    var codigoProp : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.inicializar()
        self.estilos()
        self.gestureScreen()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.cargarData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func inicializar(){
        
    }
    
    func estilos(){
        self.aceptarBT.layer.cornerRadius = 5
        self.cancelarBT.layer.cornerRadius = 5
        
        envioVW.layer.cornerRadius = 10.0
        envioVW.layer.shadowColor = UIColor.black.cgColor
        envioVW.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        envioVW.layer.shadowRadius = 2.0
        envioVW.layer.shadowOpacity = 0.2
    }
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func cargarData(){
        
    }
    
    func validar() -> Bool{
        
        let correo : String = (self.correoTX.text!).trimmingCharacters(in: CharacterSet.whitespaces)
        
        if correo == "" {
            self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar el correo.")
            return false
        }
        if correo.isValidEmail() == false {
            self.showAlert(PATHS.SENTINEL, mensaje: "Formato de correo inválido. Intente nuevamente")
            self.correoTX.text = ""
            return false
        }
        
        return true
    }
    
    func iniCorreoCronograma(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endCorreoCronograma(_:)), name: NSNotification.Name("endCorreoCronograma"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        
        let userData = UserGlobalData.sharedInstance.userGlobal
        let servicios = UserGlobalData.sharedInstance.misServiciosGlobal
        let sesion : String = (userData?.SesionId!)!
        let servicio : String = (servicios?.NroServicio)!
        let usuario : String = (userData?.user)!
        let tipoDoc : String = (userData?.TDocusu)!
        let correo : String = (self.correoTX.text!).trimmingCharacters(in: CharacterSet.whitespaces)
        let producto : String = self.idProducto!
        let cOpe : String = self.CodOper!
        let cPro : String = self.codigoProp!
        
        let parametros = [
            "AFUsuTDoc" : tipoDoc,
            "AFUsuNroDoc" : usuario,
            "Sesion" : sesion,
            "SBTSOCodSer" : servicio,
            "SBTSOCodProd" : producto,
            "SBTSOCodOpe" : cOpe,
            "SBTSOCodPro" : cPro,
            "MailPara" : correo
            ] as [String : Any]
        print(parametros)
        
        OriginData.sharedInstance.enviarCorreo(notification: "endCorreoCronograma", parametros: parametros)
    }
    
    @objc func endCorreoCronograma(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data : SabioFinancieroEntidad = (notification.object as? SabioFinancieroEntidad)!
            if data.CodigoWS == "0"{
                let alerta = UIAlertController(title: PATHS.SENTINEL, message: "Se ha enviado el cronograma satisfactoriamente.", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.cancel, handler: { (action: UIAlertAction!) in
                    self.navigationController?.popViewController(animated: true)
                }))
                
                self.present(alerta, animated: true, completion:nil)
                
            }
            else
            {
                if data.CodigoWS != "" {
                    if data.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "En estos momentos no se puede realizar la operación.", preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                        //iniMensajeError(codigo: (data?.CodigoWS!)!)
                    }
                }
            }
        }
    }
    
    @IBAction func accionAceptar(_ sender: Any) {
        if validar(){
            iniCorreoCronograma()
        }
    }
    
    @IBAction func accionCancelar(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
