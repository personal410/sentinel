//
//  SFClienteViewController.swift
//  Sentinel
//
//  Created by Daniel on 4/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import XLPagerTabStrip
import Alamofire
import AlamofireImage
class SFClienteViewController: ParentViewController, IndicatorInfoProvider {
    @IBOutlet weak var rostroIMG: UIImageView!
    @IBOutlet weak var nombreCompletoLB: UILabel!
    @IBOutlet weak var apellidosLB: UILabel!
    @IBOutlet weak var dniLB: UILabel!
    @IBOutlet weak var tituloFecNacLB:UILabel!
    @IBOutlet weak var fecNacLB: UILabel!
    @IBOutlet weak var deudaTotalLB: UILabel!
    @IBOutlet weak var varIMG: UIImageView!
    @IBOutlet weak var actualIMG: UIImageView!
    @IBOutlet weak var previoIMG: UIImageView!
    @IBOutlet weak var mIMG: UIImageView!
    @IBOutlet weak var informacionGeneralLB: UILabel!
    @IBOutlet weak var tipoContribuyenteLB: UILabel!
    @IBOutlet weak var estadoConLB: UILabel!
    @IBOutlet weak var dependenciaLB: UILabel!
    @IBOutlet weak var inicioActividadesLB: UILabel!
    @IBOutlet weak var condicionLB: UILabel!
    @IBOutlet weak var primerCuadroView: UIView!
    @IBOutlet weak var asegundoCuadroVW: UIView!
    @IBOutlet weak var productoBT: UIButton!
    @IBOutlet weak var constraintDistanciaTopCuadro: NSLayoutConstraint!
    @IBOutlet weak var constraintTopNombre: NSLayoutConstraint!
    @IBOutlet weak var constraintAlturaIMG: NSLayoutConstraint!
    @IBOutlet weak var constraintAnchoIMG: NSLayoutConstraint!
    @IBOutlet weak var alturaApellidos: NSLayoutConstraint!
    @IBOutlet weak var alturaFecha: NSLayoutConstraint!
    @IBOutlet weak var topNombreCompleto: NSLayoutConstraint!
    @IBOutlet weak var alturaViewDatos: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.estilos()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.cargarData()
    }
    
    func cargarData(){
        let datosSabio = UserGlobalData.sharedInstance.terceroGlobal.objSabioEntidad
        let ConsultaCrediticia = datosSabio?.ConsultaCrediticia
        if let varCod = ConsultaCrediticia!["VarCod"] as? Int {
            var nombreImagenVariacion = ""
            if varCod >= 2 && varCod <= 6 {
                nombreImagenVariacion = "5"
            }else if varCod == 7 {
                nombreImagenVariacion = "6"
            }else if varCod == 8 {
                nombreImagenVariacion = "1"
            }else if varCod == 12 {
                nombreImagenVariacion = "4"
            }else if varCod == 11 {
                nombreImagenVariacion = "7"
            }else if varCod == 9 || varCod == 13 || varCod == 15 || varCod == 17 || varCod == 19 {
                nombreImagenVariacion = "2"
            }else if varCod == 10 || varCod == 14 || varCod == 16 || varCod == 18 || varCod == 20 {
                nombreImagenVariacion = "3"
            }
            if !nombreImagenVariacion.isEmpty {
                varIMG.image = UIImage(named: "var\(nombreImagenVariacion)")
            }
        }
        let apePaterno : String = ConsultaCrediticia!["ApePat"] as! String
        let ApeMat : String = ConsultaCrediticia!["ApeMat"] as! String
        let Nombre : String = ConsultaCrediticia!["Nombre"] as! String
        let NombreRazonSocial : String = ConsultaCrediticia!["NombreRazonSocial"] as! String
        let FechaNac : String = ConsultaCrediticia!["FechaNac"] as! String
        let DeudaTotal : String = ConsultaCrediticia!["DeudaTotal"] as! String
        let SemAct : String = ConsultaCrediticia!["SemAct"] as! String
        let SemPre : String = ConsultaCrediticia!["SemPre"] as! String
        let Sem12m : String = ConsultaCrediticia!["Sem12m"] as! String
        let TipoContribuyente : String = ConsultaCrediticia!["TipoContribuyente"] as! String
        let EstContribuyente : String = ConsultaCrediticia!["EstContribuyente"] as! String
        let Dependencia : String = ConsultaCrediticia!["Dependencia"] as! String
        let InicioAct : String = ConsultaCrediticia!["InicioAct"] as! String
        let Condicion : String = ConsultaCrediticia!["Condicion"] as! String
        let urlImage : String = ConsultaCrediticia!["Foto"] as! String
        
        let tipoDocCliente : String = UserGlobalData.sharedInstance.terceroGlobal.tipoDocClienteSabio!
        let numDocumento : String = UserGlobalData.sharedInstance.terceroGlobal.numClienteSabio!
        if tipoDocCliente == "D" || tipoDocCliente == "5" {
            self.apellidosLB.text = "\(apePaterno) \(ApeMat)"
            self.nombreCompletoLB.text = Nombre
            self.dniLB.text = "\(tipoDocCliente == "D" ? "DNI" : "Pasaporte"): \(numDocumento)"
            
            if FechaNac.isEmpty || FechaNac == "//" {
                self.tituloFecNacLB.isHidden = true
                self.fecNacLB.isHidden = true
            }else{
                self.fecNacLB.text = FechaNac
            }
            
            let deuda = (DeudaTotal as NSString).doubleValue
            let numberFormatter = Toolbox.getNumberFormmatter()
            numberFormatter.usesGroupingSeparator = true
            numberFormatter.groupingSeparator = ","
            self.deudaTotalLB.text = numberFormatter.string(from: NSNumber(value: deuda))
            
            self.actualIMG.image = self.devuelveSemaforo(SemAct: SemAct)
            self.previoIMG.image = self.devuelveSemaforo(SemAct: SemPre)
            self.mIMG.image = self.devuelveSemaforo(SemAct: Sem12m)
            self.informacionGeneralLB.text = "Datos principales RUC"
            self.tipoContribuyenteLB.text = TipoContribuyente
            self.estadoConLB.text = EstContribuyente
            self.dependenciaLB.text = Dependencia
            self.inicioActividadesLB.text = InicioAct
            self.condicionLB.text = Condicion
            
            let url : URL = NSURL(string: urlImage)! as URL
            NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarFotoSabio(_:)), name: NSNotification.Name("endCargarFotoSabio"), object: nil)
            
            self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
            Alamofire.request(url).responseImage { response in
                debugPrint(response)
                
                switch response.result {
                case .success:
                    if let image = response.result.value {
                      self.rostroIMG.image = image
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoSabio"), object: nil)
                    }
                    break
                case .failure(let error):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFoto"), object: nil)
                    break
                }
            }
        }
        else if tipoDocCliente == "R"{
           self.nombreCompletoLB.text = NombreRazonSocial
            self.dniLB.text = "RUC: \(numDocumento)"
            self.alturaApellidos.constant = 0
            self.alturaFecha.constant = 0
            self.alturaViewDatos.constant = 40
            
            if FechaNac.isEmpty || FechaNac == "//" {
                self.tituloFecNacLB.isHidden = true
                self.fecNacLB.isHidden = true
            }else{
                self.fecNacLB.text = FechaNac
            }
            
            let deuda = (DeudaTotal as NSString).doubleValue
            let numberFormatter = Toolbox.getNumberFormmatter()
            numberFormatter.usesGroupingSeparator = true
            numberFormatter.groupingSeparator = ","
            self.deudaTotalLB.text = numberFormatter.string(from: NSNumber(value: deuda))
            self.actualIMG.image = self.devuelveSemaforo(SemAct: SemAct)
            self.previoIMG.image = self.devuelveSemaforo(SemAct: SemPre)
            self.mIMG.image = self.devuelveSemaforo(SemAct: Sem12m)
            self.informacionGeneralLB.text = "Datos principales RUC"
            self.tipoContribuyenteLB.text = TipoContribuyente
            self.estadoConLB.text = EstContribuyente
            self.dependenciaLB.text = Dependencia
            self.inicioActividadesLB.text = InicioAct
            self.condicionLB.text = Condicion
            
            let url : URL = NSURL(string: urlImage)! as URL
            NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarFotoSabio(_:)), name: NSNotification.Name("endCargarFotoSabio"), object: nil)
            
            self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
            Alamofire.request(url).responseImage { response in
                debugPrint(response)
                
                switch response.result {
                case .success:
                    if let image = response.result.value {
                        self.rostroIMG.image = image
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoSabio"), object: nil)
                    }
                    
                    break
                    
                case .failure(let error):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFoto"), object: nil)
                    break
                }
            }
        }
    }

    func estilos(){
        self.primerCuadroView.layer.cornerRadius = 5
        self.asegundoCuadroVW.layer.cornerRadius = 5
        
        primerCuadroView.layer.shadowColor = UIColor.lightGray.cgColor
        primerCuadroView.layer.shadowOpacity = 0.5
        primerCuadroView.layer.shadowOffset = CGSize.zero
        
        asegundoCuadroVW.layer.shadowColor = UIColor.lightGray.cgColor
        asegundoCuadroVW.layer.shadowOpacity = 0.5
        asegundoCuadroVW.layer.shadowOffset = CGSize.zero
        
        self.productoBT.layer.cornerRadius = 5
        rostroIMG.layer.cornerRadius = rostroIMG.frame.size.width / 2
        rostroIMG.clipsToBounds = true
    }
    
    func iniListarProductos(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endListarProductos(_:)), name: NSNotification.Name("endListarProductos"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        
        let user : String = UserGlobalData.sharedInstance.userGlobal.user!
        let sesion : String = UserGlobalData.sharedInstance.userGlobal.SesionId!
        let servicio : String = UserGlobalData.sharedInstance.misServiciosGlobal.NroServicio!
        
        let parametros = [
            "Usuario" : user,
            "SesionId" : sesion,
            "Servicio" : servicio
            ] as [String : Any]
        OriginData.sharedInstance.getProductosSabio(notification: "endListarProductos", parametros: parametros)
    }
    
    @objc func endListarProductos(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as? SabioFinancieroEntidad
            if data?.CodigoWS == "0"{
                let productos : NSArray = (data?.Productos)!
                if productos.count == 0 {
                    self.showAlert(PATHS.SENTINEL, mensaje: "Sin productos disponibles para el cliente.")
                }
                else if productos.count == 1 {
                    UserGlobalData.sharedInstance.terceroGlobal.Productos = productos
                    let nav = self.storyboard?.instantiateViewController(withIdentifier: "SFCantidadPropuestaVC") as! SFCantidadPropuestaViewController
                    self.navigationController?.pushViewController(nav, animated: true)
                }
                else if productos.count > 1 {
                    UserGlobalData.sharedInstance.terceroGlobal.Productos = productos
                    let nav = self.storyboard?.instantiateViewController(withIdentifier: "SFProductosVC") as! SFProductosViewController
                    self.navigationController?.pushViewController(nav, animated: true)
                }
            }
            else
            {
                if data?.CodigoWS != nil {
                    if data?.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: data?.MensajeRespuesta, preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    @objc func endCargarFotoSabio(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
    }
    @IBAction func accionSeleccion(_ sender: Any) {
        self.iniListarProductos()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Cliente", image: nil)
    }
}
