//
//  SFCantidadPropuestaViewController.swift
//  Sentinel
//
//  Created by Daniel on 4/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class SFCantidadPropuestaViewController: ParentViewController, MiPickerComboDelegate, UITextFieldDelegate, UIAlertViewDelegate {

    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var internalView: UIView!
    
    @IBOutlet weak var primerView: UIView!
    @IBOutlet weak var segundoView: UIView!
    @IBOutlet weak var tercerView: UIView!
    
    @IBOutlet weak var calcularBT: UIButton!
    
    //SegundoView
    @IBOutlet weak var nombreCompletoLB: UILabel!
    @IBOutlet weak var documentoLB: UILabel!
    @IBOutlet weak var productoLB: UILabel!
    @IBOutlet weak var recirrenteBT: UIButton!
    @IBOutlet weak var nuevoBT: UIButton!
    @IBOutlet weak var ingresosDlienteBT: UITextField!
    @IBOutlet weak var montoPrestamoBT: UITextField!
    @IBOutlet weak var comboSeleccion: CTComboPicker!
    @IBOutlet weak var seleccioneBT: UIButton!
    
    @IBOutlet weak var switchSW: UISwitch!
    
    //TercerView
    @IBOutlet weak var opcionNuevoSaldoVW: UIView!
    @IBOutlet weak var datosOpcionNuevoVW: UIView!
    @IBOutlet weak var actualizarsw: UISwitch!
    @IBOutlet weak var deudaTX: UITextField!
    @IBOutlet weak var cuotaTX: UITextField!
    
    @IBOutlet weak var segundoCombo: CTComboPicker!
    @IBOutlet weak var segundoSeleccioneBT: UIButton!
    @IBOutlet weak var aplicarBT: UIButton!
    
    @IBOutlet weak var sinInformacionPrimeroLB: UILabel!
    @IBOutlet weak var creditosSistemaVW: UIView!
    
    @IBOutlet weak var sistemaFinancieroTV: UITableView!
    
    @IBOutlet weak var noreguladosVW: UIView!
    @IBOutlet weak var sinInformacionSegundoLB: UILabel!
    @IBOutlet weak var noReguladoTV: UITableView!
    
    @IBOutlet weak var inferiorHeight: NSLayoutConstraint!//ALTURA DE TERCER VIEW : 715
    @IBOutlet weak var formHeight: NSLayoutConstraint! // ALTURA DE INTERNAL VIEW :1249
    @IBOutlet weak var topCreditosSistemaFinanciero: NSLayoutConstraint!
    @IBOutlet weak var nuevoSaldoHeight: NSLayoutConstraint!//ALTURA NUEVO SALDO 270 ACTIVO
    @IBOutlet var sinInformacionView: UILabel!
    
    @IBOutlet weak var oTituloDeuda: NSLayoutConstraint!
    @IBOutlet weak var oDeuda: NSLayoutConstraint!
    @IBOutlet weak var oTituloCuota: NSLayoutConstraint!
    @IBOutlet weak var oCuota: NSLayoutConstraint!
    @IBOutlet weak var oTituloCredito: NSLayoutConstraint!
    @IBOutlet weak var oCredito: NSLayoutConstraint!
    
    @IBOutlet weak var topTablaSistemasFinancieros: NSLayoutConstraint!
    //@IBOutlet var tipoCreditoBT: UIButton!
    
    @IBOutlet weak var alturaNoRegulados: NSLayoutConstraint!
    
    @IBOutlet weak var constraintAlturaMensaje: NSLayoutConstraint!
    
    @IBOutlet weak var topScroll: NSLayoutConstraint!
    
    @IBOutlet weak var tituloNuevoSaldoEntidadLB: UILabel!
    
    // MARK: - Variables
    
    var idProducto : String = ""
    var NomProducto : String = ""
    
    var tDoc = ""
    var numeroDoc = ""
    var creditosArray: [SabioFinancieroEntidad] = []
    var creditosSFinancieroArray: [SabioFinancieroEntidad] = []
    var creditosSNoReguladosArray: [SabioFinancieroEntidad] = []
    var estadoRecurrente: Int?
    var estadoNuevo: Int?
    var estadoSW: Int?
    var estadoActualizarSW: Int?
    var otrosCreditosArray: [SabioFinancieroEntidad] = []
    var arregloCreditosSinCancelar: [SabioFinancieroEntidad] = []
    var ingresos = ""
    var montoPropuesto = ""
    var cuotaMontoPropuesto = ""
    var deudaEntidad = ""
    var cuotaEntidad = ""
    
    var cliente: SabioFinancieroEntidad?
    var conyuge: SabioFinancieroEntidad?
    var montoDosDecimal = ""
    var nuevoSaldoDosDecimal = ""
    var porcentajeTotal = ""
    var misServicios: MisServiciosClass?
    var porcentajeCancelado = ""
    var codMensajeError = ""
    //Valores globales constraint
    var valorAlturaForm: Int = 0
    var valorAlturaInferior: Int = 0
    //Arreglos del picker
    var plazoArray: [String] = []
    var tipoCreditoArray: NSMutableArray = NSMutableArray()
    var valorCreditoArray: NSMutableArray = NSMutableArray()
    var campoPlazo = ""
    var campoTipoCredito = ""
    var campoValorCredito = ""
    var afserSBSNumDoc = ""
    var afserSBSNomAbr = ""
    var afserSBSTipDoc = ""
    
    var isKeyboardAppear = false
    
    // MARK: - Métodos del ciclo de vida
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.inicializar()
        self.estilos()
        self.gestureScreen()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.llenarCombos()
        self.llenarDatos()
        
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.mostrarOtrosCreditos(mostrar: false)
        creditosArray = [SabioFinancieroEntidad]()
        otrosCreditosArray = [SabioFinancieroEntidad]()
        creditosSFinancieroArray = [SabioFinancieroEntidad]()
        creditosSNoReguladosArray = [SabioFinancieroEntidad]()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Métodos generales
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func inicializar(){
        segundoCombo.delegate = self
        comboSeleccion.delegate = self
        
        estadoSW = 0
        estadoActualizarSW = 0
        estadoNuevo = 0
        estadoRecurrente = 1
        sistemaFinancieroTV.dataSource = self
        sistemaFinancieroTV.delegate = self
        
        noReguladoTV.dataSource = self
        noReguladoTV.delegate = self
        
        deudaTX.delegate = self
        self.calcularBT.addTarget(self, action: #selector(playTapped), for: UIControlEvents.touchUpInside)

    }
    
    @objc func playTapped() {
        if validar(){
            if switchSW.isOn {
                UserGlobalData.sharedInstance.terceroGlobal.CancelaOC = "SI"
                
            }
            else
            {
                UserGlobalData.sharedInstance.terceroGlobal.CancelaOC = "NO"
            }
            
            if estadoSW == 1 {
                self.recorrer()
            }
            else
            {
                self.cargarOtrosCreditosSinCancelar()
            }
            
            self.iniSabioFinanciero()
        }
    }
    
    func estilos(){
        segundoView.layer.cornerRadius = 5.0
        segundoView.layer.shadowColor = UIColor.black.cgColor
        segundoView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        segundoView.layer.shadowRadius = 2.5
        segundoView.layer.shadowOpacity = 0.2
        
        datosOpcionNuevoVW.layer.cornerRadius = 5.0
        datosOpcionNuevoVW.layer.shadowColor = UIColor.black.cgColor
        datosOpcionNuevoVW.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        datosOpcionNuevoVW.layer.shadowRadius = 2.5
        datosOpcionNuevoVW.layer.shadowOpacity = 0.2
        
        opcionNuevoSaldoVW.layer.cornerRadius = 10.0
        creditosSistemaVW.layer.cornerRadius = 10.0
        noreguladosVW.layer.cornerRadius = 10.0
        
        seleccioneBT.layer.cornerRadius = 5
        aplicarBT.layer.cornerRadius = 5
        segundoSeleccioneBT.layer.cornerRadius = 5
        calcularBT.layer.cornerRadius = 18
    }
    
    func llenarCombos() {
        plazoArray = ["Seleccione", "3", "6", "12", "18", "24", "36"]
        tipoCreditoArray = ["Seleccione", "Corporativo", "Grandes Empresas", "Medianas Empresas", "Pequeñas Empresas", "Micro Empresas", "Consumo", "Hipotecario"]
        valorCreditoArray = ["Seleccione", "06", "07", "08", "09", "10", "11", "13"]
    }

    @objc func keyboardWillShow(notification: NSNotification) {
            if !isKeyboardAppear {
                if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                    if self.topScroll.constant == 0{
                        self.topScroll.constant -= (keyboardSize.height + 20)
                        UIView.animate(withDuration: 0.5) {
                            self.view.layoutIfNeeded()
                        }
                    }
                }
                isKeyboardAppear = true
            }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
            if isKeyboardAppear {
                if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                    if self.topScroll.constant != 0{
                        self.topScroll.constant += (keyboardSize.height + 20)
                        UIView.animate(withDuration: 0.5) {
                            self.view.layoutIfNeeded()
                        }
                    }
                }
                isKeyboardAppear = false
            }
    }

    func mostrarOtrosCreditos(mostrar : Bool){
        var alturaViewNuevoSaldo: Int = 95
        
        if estadoActualizarSW == 0 {
            alturaViewNuevoSaldo = 95
            oTituloDeuda.constant = 0
            oDeuda.constant = 0
            oTituloCuota.constant = 0
            oCuota.constant = 0
            oTituloCredito.constant = 0
            oCredito.constant = 0
            segundoSeleccioneBT.isHidden = true
            aplicarBT.isHidden = true
            
            nuevoSaldoHeight.constant = 95
        } else if estadoActualizarSW == 1 {
            alturaViewNuevoSaldo = 270
        }
        
        var altoForm: Int = 1215 //985; //1060 //1010
        var altoInferior: Int = 675 //350; //528 //478
        var constante: Int = 200
        
        altoForm = 900 + alturaViewNuevoSaldo //985
        altoInferior = 240 + alturaViewNuevoSaldo //440 - > 240
        if mostrar {
            inferiorHeight.constant = CGFloat(altoInferior   + 35)
            tercerView.isHidden = false
            formHeight.constant = CGFloat(altoForm)
            valorAlturaForm =  1050 + 35
            valorAlturaInferior = 510 + 35
            
            if (creditosSFinancieroArray.count == 1) {
                valorAlturaForm =  1300 + 35//35
                valorAlturaInferior = 710 //+ 35
                self.formHeight.constant = CGFloat(altoForm)//altoForm;//1215;//1000 // 1310 -110 por deuda, cuota
                self.inferiorHeight.constant = CGFloat(altoInferior   + 35)//altoInferior;//675;//350;
                
                
                if (creditosSNoReguladosArray.count > 0) {
                    self.modificarConstraintsSegundaTabla(valorAlturaForm + 35, inferiorHeight: valorAlturaInferior + 35   + 35, cantidad: creditosSNoReguladosArray.count, formXCredito: altoForm, inferiorXCredito: altoInferior)
                    
                }
                else
                {
                    self.sinInformacionView.isHidden = false;
                    self.noReguladoTV.isHidden = true;
                }
                if (estadoActualizarSW == 1) {
                    self.inferiorHeight.constant = CGFloat(valorAlturaInferior   + 35)
                    self.formHeight.constant = CGFloat(valorAlturaForm)
                }
                else if (estadoActualizarSW == 0) {
                    
                    self.formHeight.constant = CGFloat(valorAlturaForm - 220)
                    self.inferiorHeight.constant = CGFloat(valorAlturaInferior   + 35 - 220)
                }
                
            }
            else if creditosSFinancieroArray.count == 2 {
                valorAlturaForm = 1500 + 35
                valorAlturaInferior = 900 + 35
                formHeight.constant = 1500 + 35 - 220 //1510;//1200;
                inferiorHeight.constant = 900   + 35 + 35 - 220 //550;
                
                
                if creditosSNoReguladosArray.count > 0 {
                    modificarConstraintsSegundaTabla(valorAlturaForm + 35, inferiorHeight: valorAlturaInferior   + 35 + 35, cantidad: creditosSNoReguladosArray.count, formXCredito: 1220 + 35, inferiorXCredito: 680 + 35)
                } else {
                    sinInformacionView.isHidden = false
                }
                if estadoActualizarSW == 1 {
                    inferiorHeight.constant = CGFloat(valorAlturaInferior   + 35)
                    formHeight.constant = CGFloat(valorAlturaForm)
                } else if estadoActualizarSW == 0 {
                    
                    formHeight.constant = CGFloat(valorAlturaForm - 220)
                    inferiorHeight.constant = CGFloat(valorAlturaInferior   + 35 - 220)
                }
            }
            else if creditosSFinancieroArray.count == 3 {
                valorAlturaForm = 1700 + 35
                valorAlturaInferior = 1100 + 35
                formHeight.constant = 1700 + 35 - 220 //1710;//1400
                inferiorHeight.constant = 1100 + 35 - 220   + 35 //750
                
                
                if creditosSNoReguladosArray.count > 0 {
                    modificarConstraintsSegundaTabla(valorAlturaForm + 35, inferiorHeight: valorAlturaInferior + 35   + 35, cantidad: creditosSNoReguladosArray.count, formXCredito: (1640 - 220) + 35, inferiorXCredito: (1100 - 220) + 35)
                } else {
                    sinInformacionView.isHidden = false
                }
                if estadoActualizarSW == 1 {
                    inferiorHeight.constant = CGFloat(valorAlturaInferior   + 35)
                    formHeight.constant = CGFloat(valorAlturaForm   + 35)
                } else if estadoActualizarSW == 0 {
                    
                    formHeight.constant = CGFloat(valorAlturaForm - 220)
                    inferiorHeight.constant = CGFloat(valorAlturaInferior   + 35 - 220)
                }
            }
            else if creditosSFinancieroArray.count == 4 {
                valorAlturaForm = 1900 + 35
                valorAlturaInferior = 1300 + 35
                formHeight.constant = 1900 + 35 - 220 //1910;//1600
                inferiorHeight.constant = 1300 + 35 - 220   + 35 //950
                
                
                if creditosSNoReguladosArray.count > 0 {
                    modificarConstraintsSegundaTabla(valorAlturaForm + 35, inferiorHeight: valorAlturaInferior   + 35 + 35, cantidad: creditosSNoReguladosArray.count, formXCredito: (1840 - 220 + 35), inferiorXCredito: (1300 - 220) + 35)
                } else {
                    sinInformacionView.isHidden = false
                }
                if estadoActualizarSW == 1 {
                    inferiorHeight.constant = CGFloat(valorAlturaInferior   + 35)
                    formHeight.constant = CGFloat(valorAlturaForm)
                } else if estadoActualizarSW == 0 {
                    
                    formHeight.constant = CGFloat(valorAlturaForm - 220)
                    inferiorHeight.constant = CGFloat(valorAlturaInferior - 220   + 35)
                }
            } else if creditosSFinancieroArray.count == 5 {
                valorAlturaForm = 2040 + 35
                valorAlturaInferior = 1500 + 35
                formHeight.constant = 2040 + 35 - 220 //2110;//1800
                inferiorHeight.constant = 1500 + 35 - 220 + 35  //1150
                
                
                if creditosSNoReguladosArray.count > 0 {
                    modificarConstraintsSegundaTabla(valorAlturaForm + 35, inferiorHeight: valorAlturaInferior + 35 + 35 , cantidad: creditosSNoReguladosArray.count, formXCredito: (2040 - 220) + 35, inferiorXCredito: (1500 - 220) + 35)
                } else {
                    sinInformacionView.isHidden = false
                }
                if estadoActualizarSW == 1 {
                    inferiorHeight.constant = CGFloat(valorAlturaInferior)
                    formHeight.constant = CGFloat(valorAlturaForm)
                } else if estadoActualizarSW == 0 {
                    
                    formHeight.constant = CGFloat(valorAlturaForm - 220)
                    inferiorHeight.constant = CGFloat(valorAlturaInferior - 220)
                }
            }
            else if creditosSFinancieroArray.count == 6 {
                valorAlturaForm = 2240 + 35
                valorAlturaInferior = 1700 + 35
                formHeight.constant = 2240 + 35 - 220 + 35 //2310;//2000
                inferiorHeight.constant = 1700 + 35 - 220 + 35 //1350
                
                
                if creditosSNoReguladosArray.count > 0 {
                    modificarConstraintsSegundaTabla(valorAlturaForm + 35, inferiorHeight: valorAlturaInferior + 35 + 35, cantidad: creditosSNoReguladosArray.count, formXCredito: (2240 - 220) + 35, inferiorXCredito: (1700 - 220) + 35)
                } else {
                    sinInformacionView.isHidden = false
                }
                if estadoActualizarSW == 1 {
                    inferiorHeight.constant = CGFloat(valorAlturaInferior)
                    formHeight.constant = CGFloat(valorAlturaForm)
                } else if estadoActualizarSW == 0 {
                    
                    formHeight.constant = CGFloat(valorAlturaForm - 220)
                    inferiorHeight.constant = CGFloat(valorAlturaInferior - 220)
                }
            } else if creditosSFinancieroArray.count == 7 {
                //CORREGIR DESDE AQUI
                valorAlturaForm = 2440 + 35
                valorAlturaInferior = 1900 + 35
                formHeight.constant = 2440 - 220 + 35 + 35 //2200
                inferiorHeight.constant = 1900 + 35 - 220 + 35//1550;
                
                
                if creditosSNoReguladosArray.count > 0 {
                    modificarConstraintsSegundaTabla(valorAlturaForm + 35, inferiorHeight: valorAlturaInferior + 35 + 35, cantidad: creditosSNoReguladosArray.count, formXCredito: (2240 - 220) + 35 , inferiorXCredito: (1700 - 220) + 35)
                } else {
                    sinInformacionView.isHidden = false
                }
                if estadoActualizarSW == 1 {
                    inferiorHeight.constant = CGFloat(valorAlturaInferior)
                    formHeight.constant = CGFloat(valorAlturaForm)
                } else if estadoActualizarSW == 0 {
                    
                    formHeight.constant = CGFloat(valorAlturaForm - 220)
                    inferiorHeight.constant = CGFloat(valorAlturaInferior - 220)
                }
            }
            else if creditosSFinancieroArray.count == 8 {
                valorAlturaForm = 2640 + 35
                valorAlturaInferior = 2100 + 35
                formHeight.constant = 2640 - 220 + 35 + 35 //2400;
                inferiorHeight.constant = 2100 - 220 + 35 + 35 //1750;
                
                
                if creditosSNoReguladosArray.count > 0 {
                    modificarConstraintsSegundaTabla(valorAlturaForm + 35, inferiorHeight: valorAlturaInferior + 35 + 35, cantidad: creditosSNoReguladosArray.count, formXCredito: (2240 - 220) + 35, inferiorXCredito: (1700 - 220) + 35)
                } else {
                    sinInformacionView.isHidden = false
                }
                if estadoActualizarSW == 1 {
                    inferiorHeight.constant = CGFloat(valorAlturaInferior)
                    formHeight.constant = CGFloat(valorAlturaForm)
                } else if estadoActualizarSW == 0 {
                    
                    formHeight.constant = CGFloat(valorAlturaForm - 220)
                    inferiorHeight.constant = CGFloat(valorAlturaInferior - 220)
                }
            } else if creditosSFinancieroArray.count == 9 {
                valorAlturaForm = 2840 + 35
                valorAlturaInferior = 2300 + 35
                formHeight.constant = 2840 - 220 + 35 //2600;
                inferiorHeight.constant = 2300 - 220 + 35 + 35 //1950;
                
                
                if creditosSNoReguladosArray.count > 0 {
                    modificarConstraintsSegundaTabla(valorAlturaForm, inferiorHeight: valorAlturaInferior, cantidad: creditosSNoReguladosArray.count, formXCredito: (2240 - 220 + 35), inferiorXCredito: (1700 - 220 + 35))
                } else {
                    sinInformacionView.isHidden = false
                }
            }
            else if creditosSFinancieroArray.count == 10 {
                valorAlturaForm = 3040 + 35
                valorAlturaInferior = 2500 + 35
                formHeight.constant = 3040 - 220 + 35
                inferiorHeight.constant = 2500 - 220 + 35 + 35
                
                
                if creditosSNoReguladosArray.count > 0 {
                    modificarConstraintsSegundaTabla(valorAlturaForm + 35, inferiorHeight: valorAlturaInferior + 35 + 35, cantidad: creditosSNoReguladosArray.count, formXCredito: (2240 - 220) + 35, inferiorXCredito: (1700 - 220) + 35)
                } else {
                    sinInformacionView.isHidden = false
                }
                if estadoActualizarSW == 1 {
                    inferiorHeight.constant = CGFloat(valorAlturaInferior)
                    formHeight.constant = CGFloat(valorAlturaForm)
                } else if estadoActualizarSW == 0 {
                    
                    formHeight.constant = CGFloat(valorAlturaForm - 220)
                    inferiorHeight.constant = CGFloat(valorAlturaInferior - 220)
                }
            } else if creditosSFinancieroArray.count == 11 {
                valorAlturaForm = 3240 + 35
                valorAlturaInferior = 2700 + 35
                formHeight.constant = 3240 + 35 - 220
                inferiorHeight.constant = 2700 + 35 - 220 + 35
                
                
                if creditosSNoReguladosArray.count > 0 {
                    modificarConstraintsSegundaTabla(valorAlturaForm + 35, inferiorHeight: valorAlturaInferior + 35 + 35, cantidad: creditosSNoReguladosArray.count, formXCredito: (2240 - 220) + 35, inferiorXCredito: (1700 - 220) + 35)
                } else {
                    sinInformacionView.isHidden = false
                }
                if estadoActualizarSW == 1 {
                    inferiorHeight.constant = CGFloat(valorAlturaInferior)
                    formHeight.constant = CGFloat(valorAlturaForm)
                } else if estadoActualizarSW == 0 {
                    
                    formHeight.constant = CGFloat(valorAlturaForm - 220)
                    inferiorHeight.constant = CGFloat(valorAlturaInferior - 220)
                }
            }
            else if creditosSFinancieroArray.count == 12 {
                valorAlturaForm = 3440 + 35
                valorAlturaInferior = 2900 + 35
                formHeight.constant = 3440 - 220 + 35 + 35
                inferiorHeight.constant = 2900 - 220 + 35 + 35
                
                
                if creditosSNoReguladosArray.count > 0 {
                    modificarConstraintsSegundaTabla(valorAlturaForm + 35, inferiorHeight: valorAlturaInferior + 35, cantidad: creditosSNoReguladosArray.count, formXCredito: (2240 - 220) + 35, inferiorXCredito: (1700 - 220) + 35)
                } else {
                    sinInformacionView.isHidden = false
                }
                if estadoActualizarSW == 1 {
                    inferiorHeight.constant = CGFloat(valorAlturaInferior)
                    formHeight.constant = CGFloat(valorAlturaForm)
                } else if estadoActualizarSW == 0 {
                    
                    formHeight.constant = CGFloat(valorAlturaForm - 220)
                    inferiorHeight.constant = CGFloat(valorAlturaInferior - 220)
                }
            } else if creditosSFinancieroArray.count == 13 {
                valorAlturaForm = 3640 + 35
                valorAlturaInferior = 3100 + 35
                formHeight.constant = 3640 - 220 + 35
                inferiorHeight.constant = 3100 - 220 + 35
                
                
                if creditosSNoReguladosArray.count > 0 {
                    modificarConstraintsSegundaTabla(valorAlturaForm + 35 + 35, inferiorHeight: valorAlturaInferior, cantidad: creditosSNoReguladosArray.count, formXCredito: (2240 - 220) + 35, inferiorXCredito: (1700 - 220) + 35)
                } else {
                    sinInformacionView.isHidden = false
                }
                if estadoActualizarSW == 1 {
                    inferiorHeight.constant = CGFloat(valorAlturaInferior)
                    formHeight.constant = CGFloat(valorAlturaForm)
                } else if estadoActualizarSW == 0 {
                    
                    formHeight.constant = CGFloat(valorAlturaForm - 220)
                    inferiorHeight.constant = CGFloat(valorAlturaInferior - 220)
                }
            }
            else if creditosSFinancieroArray.count == 14 {
                valorAlturaForm = 3840 + 35
                valorAlturaInferior = 3300 + 35
                formHeight.constant = 3840 - 220 + 35
                inferiorHeight.constant = 3300 - 220 + 35
                
                
                if creditosSNoReguladosArray.count > 0 {
                    modificarConstraintsSegundaTabla(valorAlturaForm + 35, inferiorHeight: valorAlturaInferior + 35, cantidad: creditosSNoReguladosArray.count, formXCredito: (2240 - 220) + 35, inferiorXCredito: (1700 - 220) + 35)
                } else {
                    sinInformacionView.isHidden = false
                }
                if estadoActualizarSW == 1 {
                    inferiorHeight.constant = CGFloat(valorAlturaInferior)
                    formHeight.constant = CGFloat(valorAlturaForm)
                } else if estadoActualizarSW == 0 {
                    
                    formHeight.constant = CGFloat(valorAlturaForm - 220)
                    inferiorHeight.constant = CGFloat(valorAlturaInferior - 220)
                }
            } else if creditosSFinancieroArray.count == 15 {
                valorAlturaForm = 4040 + 35
                valorAlturaInferior = 3500 + 35
                formHeight.constant = 4040 + 35 - 220
                inferiorHeight.constant = 3500 + 35 - 220
                
                
                if creditosSNoReguladosArray.count > 0 {
                    modificarConstraintsSegundaTabla(valorAlturaForm + 35, inferiorHeight: valorAlturaInferior + 35, cantidad: creditosSNoReguladosArray.count, formXCredito: (2240 - 220) + 35, inferiorXCredito: (1700 - 220) + 35)
                } else {
                    sinInformacionView.isHidden = false
                }
                if estadoActualizarSW == 1 {
                    inferiorHeight.constant = CGFloat(valorAlturaInferior)
                    formHeight.constant = CGFloat(valorAlturaForm)
                } else if estadoActualizarSW == 0 {
                    
                    formHeight.constant = CGFloat(valorAlturaForm - 220)
                    inferiorHeight.constant = CGFloat(valorAlturaInferior - 220)
                }
            }
            else if creditosSFinancieroArray.count == 16 {
                valorAlturaForm = 4240 + 35
                valorAlturaInferior = 3700 + 35
                formHeight.constant = 4240 + 35 - 220
                inferiorHeight.constant = 3700 + 35 - 220
                
                
                if creditosSNoReguladosArray.count > 0 {
                    modificarConstraintsSegundaTabla(valorAlturaForm + 35, inferiorHeight: valorAlturaInferior + 35, cantidad: creditosSNoReguladosArray.count, formXCredito: (2240 - 220) + 35, inferiorXCredito: (1700 - 220) + 35)
                } else {
                    sinInformacionView.isHidden = false
                }
                if estadoActualizarSW == 1 {
                    inferiorHeight.constant = CGFloat(valorAlturaInferior)
                    formHeight.constant = CGFloat(valorAlturaForm)
                } else if estadoActualizarSW == 0 {
                    
                    formHeight.constant = CGFloat(valorAlturaForm - 220)
                    inferiorHeight.constant = CGFloat(valorAlturaInferior - 220)
                }
            } else if creditosSFinancieroArray.count == 17 {
                valorAlturaForm = 4440 + 35
                valorAlturaInferior = 3900 + 35
                formHeight.constant = 4440 - 220 + 35
                inferiorHeight.constant = 3900 - 220 + 35
                
                
                if creditosSNoReguladosArray.count > 0 {
                    modificarConstraintsSegundaTabla(valorAlturaForm + 35, inferiorHeight: valorAlturaInferior + 35, cantidad: creditosSNoReguladosArray.count, formXCredito: (2240 - 220) + 35, inferiorXCredito: (1700 - 220) + 35)
                } else {
                    sinInformacionView.isHidden = false
                }
                if estadoActualizarSW == 1 {
                    inferiorHeight.constant = CGFloat(valorAlturaInferior)
                    formHeight.constant = CGFloat(valorAlturaForm)
                } else if estadoActualizarSW == 0 {
                    
                    formHeight.constant = CGFloat(valorAlturaForm - 220)
                    inferiorHeight.constant = CGFloat(valorAlturaInferior - 220)
                }
            }
            else if creditosSFinancieroArray.count == 18 {
                valorAlturaForm = 4640 + 35
                valorAlturaInferior = 4100 + 35
                formHeight.constant = 4640 - 220 + 35
                inferiorHeight.constant = 4100 - 220 + 35
                
                
                if creditosSNoReguladosArray.count > 0 {
                    modificarConstraintsSegundaTabla(valorAlturaForm + 35, inferiorHeight: valorAlturaInferior + 35, cantidad: creditosSNoReguladosArray.count, formXCredito: (2240 - 220) + 35, inferiorXCredito: (1700 - 220) + 35)
                } else {
                    sinInformacionView.isHidden = false
                }
                if estadoActualizarSW == 1 {
                    inferiorHeight.constant = CGFloat(valorAlturaInferior)
                    formHeight.constant = CGFloat(valorAlturaForm)
                } else if estadoActualizarSW == 0 {
                    
                    formHeight.constant = CGFloat(valorAlturaForm - 220)
                    inferiorHeight.constant = CGFloat(valorAlturaInferior - 220)
                }
            } else if creditosSFinancieroArray.count == 19 {
                valorAlturaForm = 4840 + 35
                valorAlturaInferior = 4300 + 35
                formHeight.constant = 4840 - 220 + 35
                inferiorHeight.constant = 4300 - 220 + 35
                
                
                if creditosSNoReguladosArray.count > 0 {
                    modificarConstraintsSegundaTabla(valorAlturaForm + 35, inferiorHeight: valorAlturaInferior + 35, cantidad: creditosSNoReguladosArray.count, formXCredito: (2240 - 220) + 35, inferiorXCredito: (1700 - 220) + 35)
                } else {
                    sinInformacionView.isHidden = false
                }
                if estadoActualizarSW == 1 {
                    inferiorHeight.constant = CGFloat(valorAlturaInferior)
                    formHeight.constant = CGFloat(valorAlturaForm)
                } else if estadoActualizarSW == 0 {
                    
                    formHeight.constant = CGFloat(valorAlturaForm - 220)
                    inferiorHeight.constant = CGFloat(valorAlturaInferior - 220)
                }
            }
            else if creditosSFinancieroArray.count == 20 {
                valorAlturaForm = 5040 + 35
                valorAlturaInferior = 4500 + 35
                formHeight.constant = 5040 + 35 - 220
                inferiorHeight.constant = 4500 + 35 - 220
                
                
                if creditosSNoReguladosArray.count > 0 {
                    modificarConstraintsSegundaTabla(valorAlturaForm + 35, inferiorHeight: valorAlturaInferior + 35, cantidad: creditosSNoReguladosArray.count, formXCredito: (2240 - 220) + 35, inferiorXCredito: (1700 - 220) + 35)
                } else {
                    sinInformacionView.isHidden = false
                }
                if estadoActualizarSW == 1 {
                    inferiorHeight.constant = CGFloat(valorAlturaInferior)
                    formHeight.constant = CGFloat(valorAlturaForm)
                } else if estadoActualizarSW == 0 {
                    
                    formHeight.constant = CGFloat(valorAlturaForm - 220)
                    inferiorHeight.constant = CGFloat(valorAlturaInferior - 220)
                }
            }
    
        }
        else
        {
            self.inferiorHeight.constant = 0
            self.tercerView.isHidden = true
            self.formHeight.constant = 650//CGFloat(altoForm - altoInferior)
        }
        
    }
    
    func modificarConstraintsSegundaTabla(_ formHeight: Int, inferiorHeight: Int, cantidad: Int, formXCredito: Int, inferiorXCredito: Int) {
        
        let valorxCantidad: Int = (223 - 40) * cantidad
        
        let valorxCantidad2: Int = 194 * cantidad
        
        switch cantidad {
        case 0:
            sinInformacionView.isHidden = false
            noReguladoTV.isHidden = true
        case 1:
            valorAlturaForm = formHeight + valorxCantidad
            valorAlturaInferior = inferiorHeight + valorxCantidad
            self.formHeight.constant = CGFloat(formXCredito + valorxCantidad)
            self.inferiorHeight.constant = CGFloat(inferiorXCredito + valorxCantidad)
            alturaNoRegulados.constant = 210
            sinInformacionView.isHidden = true
            noReguladoTV.isHidden = false
        default:
            valorAlturaForm = formHeight + valorxCantidad
            valorAlturaInferior = inferiorHeight + valorxCantidad

            self.formHeight.constant  = CGFloat(formXCredito + valorxCantidad)
            self.inferiorHeight.constant = CGFloat(inferiorXCredito + valorxCantidad)
            self.alturaNoRegulados.constant = CGFloat(valorxCantidad2)
            sinInformacionView.isHidden = true
            noReguladoTV.isHidden = false
            break
        }
    }
    
    func limpiarNuevoSaldo() {
        
        estadoActualizarSW = 0
        deudaTX.text = ""
        cuotaTX.text = ""
        
        self.actualizarsw.isOn = false
        //tipoCreditoBT.setTitle("Seleccione", for: .normal)
        
        //Falta limpiar variable del picker
        self.segundoSeleccioneBT.titleLabel?.text = "Seleccione"
        self.segundoCombo.generalBT.titleLabel?.text = "Seleccione"
        campoTipoCredito = ""
        campoValorCredito = ""
    }

    func llenarDatos() {
        
        switchSW.isOn = false
        estadoSW = 0
        otrosCreditosArray = [SabioFinancieroEntidad]()
        mostrarOtrosCreditos(mostrar: false)
        
        let productos : NSArray = UserGlobalData.sharedInstance.terceroGlobal.Productos!
        if productos.count == 1{
            let objProducto : NSDictionary = productos[0] as! NSDictionary
            let idProductoL : String = String(objProducto.object(forKey: "IdProducto") as! Int)
            let NomProductoL : String = objProducto.object(forKey: "NomProducto") as! String
            self.idProducto = idProductoL
            self.NomProducto = NomProductoL
        }
        
        let tercero = UserGlobalData.sharedInstance.terceroGlobal//objSabioEntidad
        let datosSabio = UserGlobalData.sharedInstance.terceroGlobal.objSabioEntidad
        let ConsultaCrediticia = datosSabio?.ConsultaCrediticia
        let apePaterno : String = ConsultaCrediticia!["ApePat"] as! String
        let ApeMat : String = ConsultaCrediticia!["ApeMat"] as! String
        let Nombre : String = ConsultaCrediticia!["Nombre"] as! String
        let NombreRazonSocial : String = ConsultaCrediticia!["NombreRazonSocial"] as! String
        let FechaNac : String = ConsultaCrediticia!["FechaNac"] as! String
        let DeudaTotal : String = ConsultaCrediticia!["DeudaTotal"] as! String
        let VarCod : String = String(ConsultaCrediticia!["VarCod"] as! Int)
        let SemAct : String = ConsultaCrediticia!["SemAct"] as! String
        let SemPre : String = ConsultaCrediticia!["SemPre"] as! String
        let Sem12m : String = ConsultaCrediticia!["Sem12m"] as! String
        let TipoContribuyente : String = ConsultaCrediticia!["TipoContribuyente"] as! String
        let EstContribuyente : String = ConsultaCrediticia!["EstContribuyente"] as! String
        let Dependencia : String = ConsultaCrediticia!["Dependencia"] as! String
        let InicioAct : String = ConsultaCrediticia!["InicioAct"] as! String
        let Condicion : String = ConsultaCrediticia!["Condicion"] as! String
        let urlImage : String = ConsultaCrediticia!["Foto"] as! String
        
        let tipoDocCliente : String = UserGlobalData.sharedInstance.terceroGlobal.tipoDocClienteSabio!
        let numDocumento : String = UserGlobalData.sharedInstance.terceroGlobal.numClienteSabio!
        
        if (tipoDocCliente == "RUC") || (tipoDocCliente == "R") {
            var consultado: String? = nil
            if let aSocial : String = NombreRazonSocial {
                consultado = "\(aSocial)"
            }
            let nombreCompleto2 = consultado?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            nombreCompletoLB.text = nombreCompleto2
        } else {
            nombreCompletoLB.text = "\(Nombre) \(apePaterno) \(ApeMat)"
        }
        var documentoUpper: String = ""
        if (tipoDocCliente == "R") {
            documentoUpper = "RUC"
        } else if (tipoDocCliente == "RUC") {
            documentoUpper = "RUC"
        } else if (tipoDocCliente == "D") {
            documentoUpper = "DNI"
        } else if (tipoDocCliente == "DNI") {
            documentoUpper = "DNI"
        } else if (tipoDocCliente == "CE") {
            documentoUpper = "CE"
        } else if (tipoDocCliente == "4") {
            documentoUpper = "CE"
        }
        
        var documentoConsultado: String? = nil
        if let aConsultado : String = numDocumento {
            documentoConsultado = "\(documentoUpper) \(aConsultado)"
        }
        self.documentoLB.text = documentoConsultado
        numeroDoc = numDocumento
        
        if (tipoDocCliente == "DNI") || (tipoDocCliente == "D") {
            tDoc = "D"
            iniCargarDeudasSabio()
        } else if (tipoDocCliente == "RUC") || (tipoDocCliente == "R") {
            tDoc = "R"
            iniCargarDeudasSabio()
        } else {
            tDoc = "4"
            iniCargarDeudasSabio()
        }
        
        self.productoLB.text = NomProducto
    }
    
    func validar() -> Bool {
        
        ingresos = (ingresosDlienteBT.text?.trimmingCharacters(in: CharacterSet.whitespaces))!
        montoPropuesto = (montoPrestamoBT.text?.trimmingCharacters(in: CharacterSet.whitespaces))!
        cuotaMontoPropuesto = (cuotaTX.text?.trimmingCharacters(in: CharacterSet.whitespaces))!
        
        
        if (ingresos == "") {
            
            self.showAlert(PATHS.SENTINEL, mensaje: "Debe colocar los ingresos.")
            return false
        }
        if (montoPropuesto == "") {
            
            self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar el monto propuesto.")
            return false
        }
        
        if (campoPlazo == "") {
            
            self.showAlert(PATHS.SENTINEL, mensaje: "Debe seleccionar el plazo.")
            return false
        }
        return true
    }
    
    func validarEntidad() -> Bool {
        
        deudaEntidad = (deudaTX.text?.trimmingCharacters(in: CharacterSet.whitespaces))!
        cuotaEntidad = (cuotaTX.text?.trimmingCharacters(in: CharacterSet.whitespaces))!
        
        if deudaEntidad == nil || (deudaEntidad == "") {
            
            self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar la deuda.")
            return false
        }
        if cuotaEntidad == nil || (cuotaEntidad == "") {
            
            self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar la cuota.")
            return false
        }
        if campoTipoCredito == nil || (campoTipoCredito == "") {
            
            self.showAlert(PATHS.SENTINEL, mensaje: "Debe seleccionar el tipo de crédito.")
            return false
        }
        
        return true
    }

    func recorrer(){
        otrosCreditosArray = [SabioFinancieroEntidad]()

        for i in 0 ... creditosSFinancieroArray.count - 1 {
            
        //for subviews in self.sistemaFinancieroTV.subviews
        //{
            let subviews = self.sistemaFinancieroTV.subviews[i]
            if subviews is SFCantidadPropuestaTableViewCellDos
            {
                var cell = subviews as? SFCantidadPropuestaTableViewCellDos
                var credito : SabioFinancieroEntidad = SabioFinancieroEntidad(userbean: [:])
                
                if let aText = cell?.tipoDocEntidadTX.text {
                    credito.TDocEnt = "\(aText)"
                }
                if let aText = cell?.numeroDocEntidadTX.text {
                    credito.NDocEnt = "\(aText)"
                }
                if let aText = cell?.tipoCreditoTX.text {
                    credito.TCred = "\(aText)"
                }
                if let aText = cell?.saldoLB.text {
                    credito.Saldo = "\(aText)"
                }
                if let aText = cell?.porcentajeTX.text {
                    credito.porCanc = "\(aText)"
                }
                if let aText = cell?.nuevoSaldoLB.text {
                    credito.NSaldo = "\(aText)"
                }
                if let aText = cell?.montoLB.text {
                    credito.MonCanc = "\(aText)"
                }
                
                if (cell?.porcentajeTX.text == "") {
                    
                    credito.porCanc = "0"
                    credito.MonCanc = "0"
                }
                otrosCreditosArray.append(credito)
            }
        }
        if creditosSNoReguladosArray.isEmpty {
            return
        }
        for i in 0 ... creditosSNoReguladosArray.count - 1 {
            
            //for subviews in self.sistemaFinancieroTV.subviews
            //{
            let subviews = self.noReguladoTV.subviews[i]
            if subviews is SFCantidadPropuestaTableViewCellDos
            {
                var cell = subviews as? SFCantidadPropuestaTableViewCellDos
                
                let credito : SabioFinancieroEntidad = SabioFinancieroEntidad(userbean: [:])
                
                if let aText = cell?.tipoDocEntidadTX.text {
                    credito.TDocEnt = "\(aText)"
                }
                if let aText = cell?.numeroDocEntidadTX.text {
                    credito.NDocEnt = "\(aText)"
                }
                if let aText = cell?.tipoCreditoTX.text {
                    credito.TCred = "\(aText)"
                }
                if let aText = cell?.saldoLB.text {
                    credito.Saldo = "\(aText)"
                }
                if let aText = cell?.porcentajeTX.text {
                    credito.porCanc = "\(aText)"
                }
                if let aText = cell?.nuevoSaldoLB.text {
                    credito.NSaldo = "\(aText)"
                }
                if let aText = cell?.montoLB.text {
                    credito.MonCanc = "\(aText)"
                }
                
                if (cell?.porcentajeTX.text == "") {
                    
                    credito.porCanc = "0"
                    credito.MonCanc = "0"
                }
                otrosCreditosArray.append(credito)
            }
        }
    }
    
    func recorrerTabla(){
        otrosCreditosArray = [SabioFinancieroEntidad]()
        for view: UIView? in sistemaFinancieroTV.subviews {
            for subview: Any? in (view?.subviews)! {
                if (subview is SFCantidadPropuestaTableViewCellDos) {
                    var cell = subview as? SFCantidadPropuestaTableViewCell
                    var credito : SabioFinancieroEntidad?
                    
                    if let aText = cell?.tipoDocEntidadTX.text {
                        credito?.TDocEnt = "\(aText)"
                    }
                    if let aText = cell?.numeroDocEntidadTX.text {
                        credito?.NDocEnt = "\(aText)"
                    }
                    if let aText = cell?.tipoCreditoTX.text {
                        credito?.TCred = "\(aText)"
                    }
                    if let aText = cell?.saldoLB.text {
                        credito?.Saldo = "\(aText)"
                    }
                    if let aText = cell?.porcentajeTX.text {
                        credito?.porCanc = "\(aText)"
                    }
                    if let aText = cell?.nuevoSaldoLB.text {
                        credito?.NSaldo = "\(aText)"
                    }
                    if let aText = cell?.montoLB.text {
                        credito?.MonCanc = "\(aText)"
                    }
                    
                    if (cell?.porcentajeTX.text == "") {
                        
                        credito?.porCanc = "0"
                        credito?.MonCanc = "0"
                    }
                    otrosCreditosArray.append(credito!)
                    
                    print("\(otrosCreditosArray)")
                }
            }
        }
        for view: UIView? in noReguladoTV.subviews {
            for subview: Any? in (view?.subviews)! {
                if (subview is SFCantidadPropuestaTableViewCellDos) {
                    var cell = subview as? SFCantidadPropuestaTableViewCell
                    // do something with your cell
                    
                    if let aText = cell?.tipoDocEntidadTX.text {
                        print("Valor: \(aText)")
                    }
                    if let aText = cell?.tipoCreditoTX.text {
                        print("Valor: \(aText)")
                    }
                    
                    var credito : SabioFinancieroEntidad?
                    
                    if let aText = cell?.tipoDocEntidadTX.text {
                        credito?.TDocEnt = "\(aText)"
                    }
                    if let aText = cell?.numeroDocEntidadTX.text {
                        credito?.NDocEnt = "\(aText)"
                    }
                    if let aText = cell?.tipoCreditoTX.text {
                        credito?.TCred = "\(aText)"
                    }
                    if let aText = cell?.saldoLB.text {
                        credito?.Saldo = "\(aText)"
                    }
                    if let aText = cell?.porcentajeTX.text {
                        credito?.porCanc = "\(aText)"
                    }
                    if let aText = cell?.nuevoSaldoLB.text {
                        credito?.NSaldo = "\(aText)"
                    }
                    if let aText = cell?.montoLB.text {
                        credito?.MonCanc = "\(aText)"
                    }
                    
                    if (cell?.porcentajeTX.text == "") {
                        
                        credito?.porCanc = "0"
                        credito?.MonCanc = "0"
                    }
                    otrosCreditosArray.append(credito!)
                }
            }
        }
    }
    
    func cargarOtrosCreditosSinCancelar() {
        
        arregloCreditosSinCancelar = [SabioFinancieroEntidad]()
        
        for i in 0..<creditosArray.count {
            let objCredito: SabioFinancieroEntidad? = creditosArray[i]
            let objCreditoDos : SabioFinancieroEntidad = SabioFinancieroEntidad(userbean: [:])
            //
            if objCredito?.TDocEnt == nil {
                objCreditoDos.TDocEnt = "0"
            } else {
                objCreditoDos.TDocEnt = objCredito?.TDocEnt
            }
            //
            if objCredito?.NDocEnt == nil {
                objCreditoDos.NDocEnt = "0"
            } else {
                objCreditoDos.NDocEnt = objCredito?.NDocEnt
            }
            //
            if objCredito?.TCred == nil {
                objCreditoDos.TCred = "0"
            } else {
                objCreditoDos.TCred = objCredito?.TCred
            }
            //
            if objCredito?.Saldo == nil {
                objCreditoDos.Saldo = "0"
            } else {
                objCreditoDos.Saldo = objCredito?.Saldo
            }
            //
            if objCredito?.porCanc == nil {
                objCreditoDos.porCanc = "0"
            } else {
                objCreditoDos.porCanc = objCredito?.porCanc
            }
            //
            if objCredito?.NSaldo == nil {
                objCreditoDos.NSaldo = objCredito?.Saldo
            } else {
                objCreditoDos.NSaldo = objCredito?.NSaldo
            }
            //
            if objCredito?.MonCanc == nil {
                objCreditoDos.MonCanc = "0"
            } else {
                objCreditoDos.MonCanc = objCredito?.MonCanc
            }
            
            arregloCreditosSinCancelar.append(objCreditoDos)
        }
    }

    // MARK: - Métodos WS
    
    func iniCargarDeudasSabio(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarDeudasSabio(_:)), name: NSNotification.Name("endCargarDeudasSabio"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        
        let user : String = UserGlobalData.sharedInstance.userGlobal.user!
        let sesion : String = UserGlobalData.sharedInstance.userGlobal.SesionId!
        let servicio : String = UserGlobalData.sharedInstance.misServiciosGlobal.NroServicio!
        let tdocumento : String = self.tDoc
        let nDocumento : String = self.numeroDoc
        
        let parametros = [
            "Usuario" : user,
            "SesionId" : sesion,
            "Servicio" : servicio,
            "TDoc" : tdocumento,
            "NroDoc" : nDocumento
            ] as [String : Any]
        OriginData.sharedInstance.cargarDeudasSabio(notification: "endCargarDeudasSabio", parametros: parametros)
    }
    
    @objc func endCargarDeudasSabio(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            //let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            //alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            //self.present(alerta, animated: true, completion: nil)
            self.switchSW.isEnabled = false
            let sabio : SabioFinancieroEntidad = SabioFinancieroEntidad(userbean: [:])
            self.creditosSFinancieroArray.append(sabio)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as? SabioFinancieroEntidad
            if data?.CodigoWS == "0"{
                if data?.SDT_OCSabioFinanciero?.count == 0 {
                    
                    self.switchSW.isEnabled = false
                } else {
                    for i in 0 ... (data?.SDT_OCSabioFinanciero?.count)! - 1 {
                        let prod: NSDictionary = data?.SDT_OCSabioFinanciero![i] as! NSDictionary
                        let creditoPro : SabioFinancieroEntidad = SabioFinancieroEntidad(userbean: ["TDocEnt" : prod.object(forKey: "TDocEnt") as Any, "NDocEnt" : prod.object(forKey: "NDocEnt") as Any,"NombreEntidad" : prod.object(forKey: "NombreEntidad") as Any,"TCred" : prod.object(forKey: "TCred") as Any,"DscTCred" : prod.object(forKey: "DscTCred") as Any,"Saldo" : prod.object(forKey: "Saldo") as Any])
                        
                        if let aProd : SabioFinancieroEntidad = creditoPro 
                        {
                            creditosArray.append(aProd)
                        }
                    }
                    
                    for i in 0..<creditosArray.count {
                        
                        var credito: SabioFinancieroEntidad? = creditosArray[i]
                        if (credito?.TDocEnt == "S") {
                            if let aCredito = credito {
                                creditosSFinancieroArray.append(aCredito)
                            }
                            //[creditosSNoReguladosArray addObject:credito];
                        } else {
                            if let aCredito = credito {
                                creditosSNoReguladosArray.append(aCredito)
                            }
                        }
                    }
                    iniCargarDatosEntidad()
                }
            }
            else
            {
                if data?.CodigoWS != nil {
                    if data?.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: data?.MensajeRespuesta, preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                        //iniMensajeError(codigo: (data?.CodigoWS!)!)
                    }
                }
            }
        }
    }

    func iniCargarDatosEntidad(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarDatosEntidad(_:)), name: NSNotification.Name("endCargarDatosEntidad"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        
        let user : String = UserGlobalData.sharedInstance.userGlobal.user!
        let sesion : String = UserGlobalData.sharedInstance.userGlobal.SesionId!
        let servicio : String = UserGlobalData.sharedInstance.misServiciosGlobal.NroServicio!
        let Tservicio : String = UserGlobalData.sharedInstance.misServiciosGlobal.TipServ!
       
        
        let parametros = [
            "UserID" : user,
            "UseSeIDSession" : sesion,
            "AFSerCod" : servicio,
            "FatProCod" : Tservicio
            ] as [String : Any]
        OriginData.sharedInstance.cargarDatosEntidadSabio(notification: "endCargarDatosEntidad", parametros: parametros)
    }
    
    @objc func endCargarDatosEntidad(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data : SabioFinancieroEntidad = (notification.object as? SabioFinancieroEntidad)!
            if data.codigoWs == "0"{
                let SDTDatosservicio : NSDictionary = (data.SDTDatosservicio as? NSDictionary)!
                let AfserSBSNomAbr : String = SDTDatosservicio.object(forKey: "afserSBSNomAbr") as! String
                let AfserSBSNumDoc : String = SDTDatosservicio.object(forKey: "afserSBSNumDoc") as! String
                let AfserSBSTipDoc : String = SDTDatosservicio.object(forKey: "afserSBSTipDoc") as! String
                
                tituloNuevoSaldoEntidadLB.text = "Opción para ingresar el nuevo saldo de la entidad \(AfserSBSNomAbr)"
                afserSBSNomAbr = AfserSBSNomAbr
                afserSBSNumDoc = AfserSBSNumDoc
                afserSBSTipDoc = AfserSBSTipDoc
                
                mostrarOtrosCreditos(mostrar: false)
                
                if creditosSFinancieroArray.count > 0 {
                    sistemaFinancieroTV.reloadData()
                }
                if creditosSNoReguladosArray.count > 0 {
                    noReguladoTV.reloadData()
                }
            }
            else
            {
                sistemaFinancieroTV.reloadData()
                if data.CodigoWS != nil {
                    if data.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: data.MensajeRespuesta, preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                        //iniMensajeError(codigo: (data?.CodigoWS!)!)
                    }
                }
            }
        }
    }

    func iniSabioFinanciero(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endSabioFinanciero(_:)), name: NSNotification.Name("endSabioFinanciero"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        
        let user : String = UserGlobalData.sharedInstance.userGlobal.user!
        let sesion : String = UserGlobalData.sharedInstance.userGlobal.SesionId!
        let servicio : String = UserGlobalData.sharedInstance.misServiciosGlobal.NroServicio!
        let Tservicio : String = UserGlobalData.sharedInstance.misServiciosGlobal.TipServ!
        let tercero = UserGlobalData.sharedInstance.terceroGlobal//objSabioEntidad
        let datosSabio = UserGlobalData.sharedInstance.terceroGlobal.objSabioEntidad
        
        let tipoDocCliente : String = UserGlobalData.sharedInstance.terceroGlobal.tipoDocClienteSabio!
        let numDocumento : String = UserGlobalData.sharedInstance.terceroGlobal.numClienteSabio!
        
        var tipoDeudor : String = ""
        //self.estadoRecurrente = 0
        if estadoRecurrente == 0 {
            tipoDeudor = "N"
        }
        else
        {
            tipoDeudor = "R"
        }
        var TDocCony : String = ""
        var NDocCony : String = ""
        
        let hayConyuge : String = UserGlobalData.sharedInstance.terceroGlobal.hayConyuge!
        
        if hayConyuge == "SI"{
           TDocCony = UserGlobalData.sharedInstance.terceroGlobal.tipoDocConyugeSabio!
           NDocCony = UserGlobalData.sharedInstance.terceroGlobal.numConyugeSabio!
        }
        else
        {
          TDocCony = ""
          NDocCony = ""
        }
        
        let CancelaOC : String = UserGlobalData.sharedInstance.terceroGlobal.CancelaOC!
        let otrosArray : NSMutableArray = NSMutableArray()
        if CancelaOC == "SI"{
            for i in 0..<otrosCreditosArray.count {
                
                var dictCreditos: NSMutableDictionary = NSMutableDictionary()
                var cred: SabioFinancieroEntidad? = otrosCreditosArray[i]
                
                dictCreditos.setValue(cred?.TDocEnt, forKey: "TDocEnt")
                dictCreditos.setValue(cred?.NDocEnt, forKey: "NDocEnt")
                dictCreditos.setValue(cred?.TCred, forKey: "TCred")
                dictCreditos.setValue(cred?.Saldo, forKey: "Saldo")
                dictCreditos.setValue(cred?.porCanc, forKey: "PorCanc")
                dictCreditos.setValue(cred?.NSaldo, forKey: "NSaldo")
                dictCreditos.setValue(cred?.MonCanc, forKey: "MonCanc")
                dictCreditos.setValue(cred?.Cuota, forKey: "Cuota")
                
                
                otrosArray.add(dictCreditos)
            }
        }
        else
        {
            for i in 0..<arregloCreditosSinCancelar.count {
                
                var dictCreditos: NSMutableDictionary = NSMutableDictionary()
                var cred: SabioFinancieroEntidad? = arregloCreditosSinCancelar[i]
                
                dictCreditos.setValue(cred?.TDocEnt, forKey: "TDocEnt")
                dictCreditos.setValue(cred?.NDocEnt, forKey: "NDocEnt")
                dictCreditos.setValue(cred?.TCred, forKey: "TCred")
                dictCreditos.setValue(cred?.Saldo, forKey: "Saldo")
                dictCreditos.setValue(cred?.porCanc, forKey: "PorCanc")
                dictCreditos.setValue(cred?.NSaldo, forKey: "NSaldo")
                dictCreditos.setValue(cred?.MonCanc, forKey: "MonCanc")
                dictCreditos.setValue(cred?.Cuota, forKey: "Cuota")
                
                otrosArray.add(dictCreditos)
            }
        }
        
        //09676304 -- 46972023
        let parametros = [
            "Usuario" : user,
            "SesionId" : sesion,
            "Servicio" : servicio,
            "TDoc" : tipoDocCliente,
            "NroDoc" : numDocumento,
            "Producto" : self.idProducto,
            "TipoDeudor" : tipoDeudor,
            "Ingresos" : ingresos,
            "MontoPropuesto" : montoPropuesto,
            "Plazo" : campoPlazo,
            "TDocCony" : TDocCony,
            "NDocCony" : NDocCony,
            "CancelaOC" : CancelaOC,
            "SDT_InOCSabioFinanciero" : otrosArray
            ] as [String : Any]
        OriginData.sharedInstance.iniSabioFinanciero(notification: "endSabioFinanciero", parametros: parametros)
    }
    
    @objc func endSabioFinanciero(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data : SabioFinancieroEntidad = (notification.object as? SabioFinancieroEntidad)!
            if data.CodigoWS == "0"{
                if data.EsValido == "N"{
                    self.showAlert(PATHS.SENTINEL, mensaje: data.MensajeRespuesta!)
                }
                else{
                    if data.SDTSabioFinanciero?.count == 0 {
                        self.showAlert(PATHS.SENTINEL, mensaje: "En estos momentos no existe un análisis disponible.")
                    }
                    else
                    {
                        UserGlobalData.sharedInstance.terceroGlobal.SDTSabioFinanciero = data.SDTSabioFinanciero! as NSDictionary
                        let nav = self.storyboard?.instantiateViewController(withIdentifier: "SFRecomendacionVC") as? SFRecomendacionViewController
                        nav?.idProducto = self.idProducto
                        self.navigationController?.pushViewController(nav!, animated: true)
                    }
                }
            }
            else
            {
                if data.CodigoWS != nil {
                    if data.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: data.MensajeRespuesta, preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                        //iniMensajeError(codigo: (data?.CodigoWS!)!)
                    }
                }
            }
        }
    }
    
    // MARK: - Métodos Delegados
    
    func miPickerCombo(_ pickerid: Int, didSelectRow row: Int) {
        switch pickerid {
        case 1:
            if !(plazoArray[row] == "Seleccione") {
                campoPlazo = plazoArray[row]
            }
            
            break
        case 2:
            if !(tipoCreditoArray[row] as! String == "Seleccione") {
                campoTipoCredito = tipoCreditoArray[row] as! String
                campoValorCredito = valorCreditoArray[row] as! String
            }
            
            break
        default:
            break
        }
    }
    
    func miPickerCombo(_ MiPickerView: CTComboPicker!, pickerView picker: UIPickerView!, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
    // MARK: - Acciones
    
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func accionRecurrente(_ sender: Any) {
        if estadoRecurrente == 0 {
            estadoRecurrente = 1
            estadoNuevo = 0
            self.recirrenteBT.setImage(UIImage(named: "ic_sem_verde"), for: .normal)
            self.nuevoBT.setImage(UIImage(named: "circulo1"), for: .normal)
            
        } else {
            estadoRecurrente = 0
            estadoNuevo = 1
            self.recirrenteBT.setImage(UIImage(named: "circulo1"), for: .normal)
            self.nuevoBT.setImage(UIImage(named: "ic_sem_verde"), for: .normal)
        }
    }
    
    @IBAction func accionNuevo(_ sender: Any) {
        if estadoNuevo == 0 {
            estadoRecurrente = 0
            estadoNuevo = 1
            self.recirrenteBT.setImage(UIImage(named: "circulo1"), for: .normal)
            self.nuevoBT.setImage(UIImage(named: "ic_sem_verde"), for: .normal)
        } else {
            estadoRecurrente = 1
            estadoNuevo = 0
            self.recirrenteBT.setImage(UIImage(named: "ic_sem_verde"), for: .normal)
            self.nuevoBT.setImage(UIImage(named: "circulo1"), for: .normal)
        }

    }
    
    @IBAction func accionActualizar(_ sender: UISwitch) {
        
        if sender.isOn{
            oTituloDeuda.constant = 21
            oDeuda.constant = 30
            oTituloCuota.constant = 21
            oCuota.constant = 30
            oTituloCredito.constant = 21
            oCredito.constant = 30
            //tipoCreditoBT.isHidden = false
            aplicarBT.isHidden = false
            segundoSeleccioneBT.isHidden = false
            nuevoSaldoHeight.constant = 270
            constraintAlturaMensaje.constant = 0
            inferiorHeight.constant = CGFloat(valorAlturaInferior )
            formHeight.constant = CGFloat(valorAlturaForm + 35)
        }
        else
        {
            oTituloDeuda.constant = 0
            oDeuda.constant = 0
            oTituloCuota.constant = 0
            oCuota.constant = 0
            oTituloCredito.constant = 0
            oCredito.constant = 0
            //tipoCreditoBT.isHidden = true
            aplicarBT.isHidden = true
            segundoSeleccioneBT.isHidden = true
            nuevoSaldoHeight.constant = 95
            estadoActualizarSW = 0
            formHeight.constant = CGFloat(valorAlturaForm - 140)
            inferiorHeight.constant = CGFloat(valorAlturaInferior - 175)
            
            constraintAlturaMensaje.constant = 38
        }
    }
    
    @IBAction func accionAplicarCambios(_ sender: Any) {
        
        if validarEntidad() {
            self.showAlert(PATHS.SENTINEL, mensaje: "Se agregó el saldo de la entidad a la lista de créditos.")
            if self.afserSBSTipDoc == "S"{
                var credito = SabioFinancieroEntidad(userbean: [:])
                credito.TDocEnt = afserSBSTipDoc
                credito.NDocEnt = afserSBSNumDoc
                credito.NombreEntidad = afserSBSNomAbr
                credito.DscTCred = campoTipoCredito
                credito.TCred = campoValorCredito
                credito.Saldo = deudaTX.text
                credito.CuotaEntidad = self.cuotaTX.text
                
                creditosSFinancieroArray.insert(credito, at: 0)

                tipoCreditoArray.remove(campoTipoCredito)
                valorCreditoArray.remove(campoValorCredito)
                
                self.mostrarOtrosCreditos(mostrar: true)
                
//                if estadoActualizarSW == 1 {
//                    inferiorHeight.constant = CGFloat(valorAlturaInferior)
//                    formHeight.constant = CGFloat(valorAlturaForm)
//                } else if estadoActualizarSW == 0 {
//
//                    formHeight.constant = CGFloat(valorAlturaForm - 220)
//                    inferiorHeight.constant = CGFloat(valorAlturaInferior - 220)
//                }
                
                limpiarNuevoSaldo()
                estadoActualizarSW = 0
                actualizarsw.isOn = false
                self.constraintAlturaMensaje.constant = 38
                campoValorCredito = ""
                campoTipoCredito = ""
                
                sistemaFinancieroTV.reloadData()
                
            }
        }
    }
    
    @IBAction func accionCancelarOtrosCreditos(_ sender: UISwitch) {
        
        if sender.isOn {
            estadoSW = 1
            
            mostrarOtrosCreditos(mostrar: true)
            
            actualizarsw.isOn = false
            self.constraintAlturaMensaje.constant = 38
            
            if  case self.creditosSFinancieroArray.count = 0 {
                topTablaSistemasFinancieros.constant = 80
            }
            else
            {
                topTablaSistemasFinancieros.constant = 8
            }
            
        } else {
            estadoSW = 0
            mostrarOtrosCreditos(mostrar: false)
        }

        
        
    }
    @IBAction func accionPrimerCombo(_ sender: Any) {
        self.comboSeleccion.setConfig(plazoArray as [Any])
        self.comboSeleccion.open()
        self.comboSeleccion.setId(1)
    }
    
    @IBAction func accionSegundoCombo(_ sender: Any) {
        self.segundoCombo.setConfig(tipoCreditoArray as! [Any])
        self.segundoCombo.open()
        self.segundoCombo.setId(2)
    }
    
    @IBAction func accionCalcular(_ sender: Any) {
        
        
    }
}

extension SFCantidadPropuestaViewController : UITableViewDataSource, UITableViewDelegate{
    
    // MARK:  Métodos TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView == self.sistemaFinancieroTV) {
            return creditosSFinancieroArray.count;
        }
        else if (tableView == self.noReguladoTV){
            return creditosSNoReguladosArray.count;
        }
        
        return creditosArray.count;
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SFCantidadPropuestaTVC") as! SFCantidadPropuestaTableViewCellDos
        if (tableView == self.sistemaFinancieroTV) {
            let cellS = tableView.dequeueReusableCell(withIdentifier: "SFCantidadPropuestaTVC") as! SFCantidadPropuestaTableViewCellDos
            cellS.porcentajeTX.delegate = self
            let credito : SabioFinancieroEntidad = creditosSFinancieroArray[indexPath.row]
            cellS.tipoCreditoTX.text = credito.TCred
            cellS.tipoDocEntidadTX.text = credito.TDocEnt
            cellS.numeroDocEntidadTX.text = credito.NDocEnt
            cellS.entidadlb.text = credito.NombreEntidad
            cellS.creditoLB.text = credito.DscTCred
            cellS.saldoLB.text = credito.Saldo
            cellS.nuevoSaldoLB.text = credito.Saldo
            cellS.montoLB.text = "0"
            cellS.porcentajeTX.text = ""
            
            
            return cellS
        }
        else if (tableView == self.noReguladoTV){
            let cellR = tableView.dequeueReusableCell(withIdentifier: "SFCantidadPropuestaTVC2") as! SFCantidadPropuestaTableViewCellDos
            cellR.porcentajeTX.delegate = self
            let credito : SabioFinancieroEntidad = creditosSNoReguladosArray[indexPath.row]
            
            cellR.tipoCreditoTX.text = credito.TCred
            cellR.tipoDocEntidadTX.text = credito.TDocEnt
            cellR.numeroDocEntidadTX.text = credito.NDocEnt
            cellR.entidadlb.text = credito.NombreEntidad
            cellR.creditoLB.text = credito.DscTCred
            cellR.saldoLB.text = credito.Saldo
            cellR.nuevoSaldoLB.text = credito.Saldo
            cellR.montoLB.text = "0"
            cellR.porcentajeTX.text = ""
            
            return cellR;
        }
        
        return cell
    }
    
}












