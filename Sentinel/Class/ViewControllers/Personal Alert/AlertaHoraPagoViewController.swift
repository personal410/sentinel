//
//  AlertaHoraPagoViewController.swift
//  Sentinel
//
//  Created by Daniel on 24/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class AlertaHoraPagoViewController:ParentViewController {
    //MARK: - Outlet
    @IBOutlet weak var fechaLB:UILabel!
    @IBOutlet weak var datePickerDP:UIDatePicker!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        accionDetalle()
    }
    //MARK: - Actions
    @IBAction func accionDetalle(){
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "HH:mm"
        fechaLB.text = dateFormat.string(from: datePickerDP.date)
    }
    @IBAction func accionEstablecer(){
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "HH:mm"
        UserGlobalData.sharedInstance.userGlobal.horaPago = dateFormat.string(from: datePickerDP.date)
        accionCancelar()
    }
    @IBAction func accionCancelar(){
        self.navigationController?.popViewController(animated: true)
    }
}
