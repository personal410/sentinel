//
//  AlertaPersonalViewController.swift
//  Sentinel
//
//  Created by Daniel on 14/11/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class AlertaPersonalViewController: ParentViewController {

    @IBOutlet weak var mensajeSinAlertasLB: UILabel!
    @IBOutlet weak var laertasTV: UITableView!
    @IBOutlet weak var toolbar: UIToolbar!
    
    
    var esPremium : Bool = false
    var arregloAlertas: NSArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.estilos()
        self.inicializar()
        self.validarPremium()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.iniListarAlertas()
    }
    
    func inicializar(){
        
    }
    
    func estilos(){
        self.toolbar.clipsToBounds = true
    }
    
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            self.esPremium = true
            self.changeStatusBar(cod: 3)
            self.setGradient(uiView: self.toolbar)
            
        }
        else{
            self.esPremium = false
        }
    }
    
    func iniListarAlertas(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endListarAlertas(_:)), name: NSNotification.Name("endListarAlertas"), object: nil)
        
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        
        let userData = UserGlobalData.sharedInstance.userGlobal!
        
        let usuario : String = userData.user!
        let sesion : String = userData.SesionId!
        let tDoc : String = userData.TDocusu!
        
        let parametros = [
            "TDocUsuario" : tDoc,
            "Usuario" : usuario,
            "SesionId" :  sesion
            ] as [String : Any]
        print(parametros)
        OriginData.sharedInstance.listaAlertasMenu(notification: "endListarAlertas", parametros: parametros)
    }
    
    @objc func endListarAlertas(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as! UserClass
            if data.CodigoWS == "0"{
                if data.ServicioActivo == "S"{
                    if (data.SDTListadoAlertasPersonales?.count)! > 0 {
                        UserGlobalData.sharedInstance.userGlobal.SDTListadoAlertasPersonales = data.SDTListadoAlertasPersonales!
                        self.arregloAlertas = data.SDTListadoAlertasPersonales!
                        self.mensajeSinAlertasLB.isHidden = true
                        self.laertasTV.reloadData()
                    }
                    else
                    {
                        self.mensajeSinAlertasLB.isHidden = false
                        self.mensajeSinAlertasLB.text = "Por el momento, no cuenta con mensajes en su bandeja de entrada."
                    }
                }
                else
                {
                    self.mensajeSinAlertasLB.isHidden = false
                    self.mensajeSinAlertasLB.text = "Por el momento, no cuenta con el servicio activo de las alertas personales."
                }
            }
            else
            {
                if data.codigoWS != nil {
                    if data.codigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        iniMensajeError(codigo: (data.codigoWS!))
                    }
                }
                else
                {
                    let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                    alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                    self.present(alerta, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension AlertaPersonalViewController : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arregloAlertas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AlertaPersonalMenuTVC", for: indexPath) as! AlertaPersonalMenuTableViewCell
        
        let objMensaje : NSDictionary = self.arregloAlertas[indexPath.row] as! NSDictionary
        if let CnhDesCor : String = objMensaje.object(forKey: "CnhDesCor") as? String {
            cell.tituloLB.text = CnhDesCor
        }
        
        if let CnhFchPro : String = objMensaje.object(forKey: "CnhFchPro") as? String {
            cell.fechaLB.text = self.devuelveFechaCortaLetras(fecha: CnhFchPro)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            //tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let objMensaje : NSDictionary = self.arregloAlertas[indexPath.row] as! NSDictionary
        
        let nav = UIStoryboard.init(name: "OpcionesMenu", bundle: nil).instantiateViewController(withIdentifier: "DetalleMensajesVC") as! DetalleMensajesViewController
        nav.objMensaje = objMensaje
        self.navigationController?.pushViewController(nav, animated: true)
    }
}

