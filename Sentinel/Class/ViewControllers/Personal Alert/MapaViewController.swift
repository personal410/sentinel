//
//  MapaViewController.swift
//  Sentinel
//
//  Created by Daniel on 19/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
import MapKit
import CoreLocation
import GoogleMaps
class MapaViewController:ParentViewController {
    //MARK: - Outlet
    @IBOutlet weak var mapview:GMSMapView!
    @IBOutlet weak var toolbar:UIToolbar!
    //MARK: - Attributes
    private let locationManager = CLLocationManager()
    var arrLugares:[NSDictionary]!
    //MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        if UserGlobalData.sharedInstance.userGlobal.EsPremium == "S" {
            self.changeStatusBar(cod: 3)
            setGradient(uiView: toolbar)
        }
        print("arrLugares: \(arrLugares.description)")
        for dic in arrLugares {
            print("lugar: \(dic.description)")
            if let lat = dic.object(forKey: "STELCLat") as? NSString, let lon = dic.object(forKey: "STELCLong") as? NSString {
                if lat.length > 0 && lon.length > 0 {
                    let vlat = lat.doubleValue
                    let vlon = lon.doubleValue
                    let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: vlat, longitude: vlon))
                    marker.map = mapview
                    continue
                }
            }
            print("**aa**")
            if let direccion = dic.object(forKey: "STELCDirecc") as? String {
                print("direccion")
                var components = URLComponents(string: "https://maps.googleapis.com/maps/api/geocode/json")!
                //AIzaSyBk1HkDKTV69ugA1io5WZwzTVknRQhyL3I
                //AIzaSyDbminkhw13hx0EAh159OWR-hMJlqg3jtE
                let key = URLQueryItem(name: "key", value: "AIzaSyBk1HkDKTV69ugA1io5WZwzTVknRQhyL3I")
                let address = URLQueryItem(name: "address", value: direccion)
                components.queryItems = [key, address]
                
                let task = URLSession.shared.dataTask(with: components.url!){(d, response, error) in
                    if error == nil {
                        print("error nil")
                        if let data = d {
                            if let j = try? JSONSerialization.jsonObject(with: data) as? [String: Any], let json = j {
                                print("json: \(json)")
                                if let dicGeo = json["geometry"] as? [String: Any] {
                                    print("dicGeo: \(dicGeo)")
                                }
                            }
                        }
                    }else{
                        print("error: \(error!)")
                    }
                }
                task.resume()
            }
        }
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? OtrosCentroPagoViewController {
            viewCont.arrLugares = arrLugares
        }
    }
    //MARK: - Action
    @IBAction func atras(){
        navigationController?.popViewController(animated: true)
    }
}
// MARK: - CLLocationManagerDelegate
extension MapaViewController:CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        locationManager.startUpdatingLocation()
        mapview.isMyLocationEnabled = true
        mapview.settings.myLocationButton = true
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        mapview.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        locationManager.stopUpdatingLocation()
    }
}
// MARK: - GMSmapviewDelegate
extension MapaViewController:GMSMapViewDelegate {
    func mapView(_ mapview: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
        guard let placeMarker = marker as? PlaceMarker else {
            return nil
        }
        guard let infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView else {
            return nil
        }
        infoView.nameLabel.text = placeMarker.place.name
        if let photo = placeMarker.place.photo {
            infoView.placePhoto.image = photo
        } else {
            infoView.placePhoto.image = UIImage(named: "generic")
        }
        return infoView
    }
    func didTapMyLocationButton(for mapview: GMSMapView) -> Bool {
        return false
    }
}
