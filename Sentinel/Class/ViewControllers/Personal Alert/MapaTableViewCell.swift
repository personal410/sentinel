//
//  MapaTableViewCell.swift
//  Sentinel
//
//  Created by Richy on 19/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class MapaTableViewCell:UITableViewCell {
    @IBOutlet weak var iconoIV:UIImageView!
    @IBOutlet weak var descripcionLB:UILabel!
}
