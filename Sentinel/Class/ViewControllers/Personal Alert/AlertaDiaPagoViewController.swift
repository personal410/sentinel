//
//  AlertaDiaPagoViewController.swift
//  Sentinel
//
//  Created by Daniel on 24/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class AlertaDiaPagoViewController:ParentViewController {
    //MARK: - Outlets
    @IBOutlet var fechaLB:UILabel!
    @IBOutlet var fechaDP:UIDatePicker!
    //MARK: - ViewCont Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        fechaDP.minimumDate = Date()
        if UserGlobalData.sharedInstance.userGlobal.fechaPago != "" {
            let fechaPago = UserGlobalData.sharedInstance.userGlobal.fechaPago
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            fechaDP.maximumDate = formatter.date(from: fechaPago)!
        }
        accionDetalle(0)
    }
    //MARK: - Actions
    @IBAction func accionDetalle(_ sender:Any){
        let date = fechaDP.date
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es_ES")
        dateFormatter.dateFormat = "EEEE"
        let weekday = dateFormatter.string(from: date).capitalized
        dateFormatter.dateFormat = "MMMM"
        let month = dateFormatter.string(from: date).capitalized
        dateFormatter.dateFormat = "'\(weekday)', dd 'de \(month) de' yyyy"
        let strDate = dateFormatter.string(from: fechaDP.date)
        print("strDate: \(strDate)")
        fechaLB.text = dateFormatter.string(from: date)
    }
    @IBAction func accionEstablecer(_ sender:Any){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        UserGlobalData.sharedInstance.userGlobal.fechaDia = dateFormatter.string(from: fechaDP.date)
        accionCancelar(0)
    }
    @IBAction func accionCancelar(_ sender:Any){
        self.navigationController?.popViewController(animated: false)
    }
}
