//
//  CrearAlertaPagoViewController.swift
//  Sentinel
//
//  Created by Richy on 19/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class CrearAlertaPagoViewController:ParentViewController {
    //MARK: - Outlets
    @IBOutlet weak var toolbar:UIToolbar!
    @IBOutlet weak var nombreTF:UITextField!
    @IBOutlet weak var montoTF:UITextField!
    @IBOutlet weak var fechaTF:UITextField!
    @IBOutlet weak var diasTF:UITextField!
    @IBOutlet weak var horaTF:UITextField!
    @IBOutlet weak var btnFechapago:UIButton!
    @IBOutlet weak var diaBT:UIButton!
    @IBOutlet weak var horaBT:UIButton!
    @IBOutlet weak var btnGuardar:UIButton!
    //MARK: - Attributes
    var userData:UserClass!
    var alertaPago:NSDictionary?
    var esEditable = true
    var primeraCarga = true
    //MARK: - ViewCont Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        userData = UserGlobalData.sharedInstance.userGlobal!
        if userData.EsPremium == "S" {
            self.changeStatusBar(cod: 3)
            setGradient(uiView: toolbar)
        }
        if let dic = alertaPago {
            userData.fechaPago = (dic.object(forKey: "STAPFecPago") as! String).split(separator: "-").reversed().joined(separator: "/")
            userData.fechaDia = (dic.object(forKey: "STAPFecAle") as! String).split(separator: "-").reversed().joined(separator: "/")
            userData.horaPago = dic.object(forKey: "STAPHorAle") as! String
            fechaTF.text = userData.fechaPago
            diasTF.text = userData.fechaDia
            horaTF.text = userData.horaPago
            nombreTF.text = dic.object(forKey: "STEntNom") as? String
            montoTF.text = dic.object(forKey: "STAPMonto") as? String
            esEditable = (dic.object(forKey: "STAPEstPago") as! String) != "P"
            if !esEditable {
                nombreTF.isEnabled = false
                montoTF.isEnabled = false
                fechaTF.isEnabled = false
                diasTF.isEnabled = false
                horaTF.isEnabled = false
                btnFechapago.isEnabled = false
                diaBT.isEnabled = false
                horaBT.isEnabled = false
                btnGuardar.isHidden = true
            }
        }else{
            userData.fechaPago = ""
            userData.fechaDia = ""
            userData.horaPago = ""
        }
    }
    override func viewWillAppear(_ animated:Bool){
        if primeraCarga {
            primeraCarga = false
        }else{
            userData = UserGlobalData.sharedInstance.userGlobal!
        }
        fechaTF.text = userData.fechaPago
        diasTF.text = userData.fechaDia
        horaTF.text = userData.horaPago
    }
    //MARK: - Actions
    @IBAction func accionAtras(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func accionGuardar(_ sender: Any) {
        if validar(){
            iniCrearAlertaPagos()
        }
    }
    @IBAction func hideKeyboard(){
        view.endEditing(true)
    }
    //MARK: - Methods
    func validar() -> Bool{
        let nombre = self.nombreTF.text!
        if nombre == "" {
            self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar el nombre del titular a quien va dirigida la alerta.")
            return false
        }
        let monto = self.montoTF.text!
        if monto == ""{
            self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar el monto de la alerta.")
            return false
        }
        let fechaP = UserGlobalData.sharedInstance.userGlobal.fechaPago
        if fechaP == ""{
            self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar la fecha de pago de la alerta.")
            return false
        }
        let diaP = UserGlobalData.sharedInstance.userGlobal.fechaDia
        if diaP == ""{
            self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar el día de alerta.")
            return false
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let dateFechaPago = dateFormatter.date(from: fechaP)!
        let dateDiaAlerta = dateFormatter.date(from: diaP)!
        if dateFechaPago < dateDiaAlerta {
            self.showAlert(PATHS.SENTINEL, mensaje: "La fecha de alerta no debe ser mayor a la fecha de pago.")
            return false
        }
        let horaP = UserGlobalData.sharedInstance.userGlobal.horaPago
        if horaP == "" {
            self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar la hora de alerta.")
            return false
        }
        return true
    }
    func iniCrearAlertaPagos(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endCrearAlertaPagos(_:)), name: NSNotification.Name("endCrearAlertaPagos"), object: nil)
        let userData = UserGlobalData.sharedInstance.userGlobal!
        let parametros = [
            "UseTipDoc" : userData.tipoDocumento!,
            "UserId" : userData.user!,
            "STEntTDoc" : "O",
            "STEntNDoc" : "",
            "STAPFecAle" : userData.fechaDia.split(separator: "/").reversed().joined(separator: "-"),
            "STAPHorAle" : userData.horaPago,
            "STAPMonto" : montoTF.text!,
            "STAPFecPago" : userData.fechaPago.split(separator: "/").reversed().joined(separator: "-"),
            "STEntNom" : nombreTF.text!,
            "SesionId" : userData.SesionId!
        ] as [String : Any]
        print(parametros)
        if alertaPago == nil {
            OriginData.sharedInstance.crearAlertasPagos(notification: "endCrearAlertaPagos", parametros: parametros)
        }else{
            OriginData.sharedInstance.modificarAlertaPago(notification: "endCrearAlertaPagos", parametros: parametros)
        }
    }
    @objc func endCrearAlertaPagos(_ notification:NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        guard let data = notification.object as? AlertaClass else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if let codigoWs = data.CodigoWS {
            if codigoWs == "0" {
                navigationController?.popViewController(animated: true)
                return
            }else if codigoWs == "99" {
                showError99()
                return
            }
        }
        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "En estos momentos no se puede realizar la operación.", preferredStyle: .alert)
        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
        alertaVersion.addAction(actionVersion)
        self.present(alertaVersion, animated: true, completion: nil)
    }
}
