//
//  OtrosCentroPagoViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 2/17/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import UIKit
class OtrosCentroPagoViewController:ParentViewController{
    //MARK: - Outlets
    @IBOutlet weak var toolbar:UIToolbar!
    @IBOutlet weak var lblLocales:UILabel!
    @IBOutlet weak var lblBancos:UILabel!
    @IBOutlet weak var lblOtros:UILabel!
    //MARK: - Attributtes
    var arrLugares:[NSDictionary]!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        if UserGlobalData.sharedInstance.userGlobal.EsPremium == "S" {
            self.changeStatusBar(cod: 3)
            setGradient(uiView: toolbar)
        }
        var arrLocales:[String] = []
        var arrBancos:[String] = []
        var arrOtros:[String] = []
        for dic in arrLugares {
            if let tipo = dic.object(forKey: "STELCTip") as? String, let desc = dic.object(forKey: "STELCDsc") as? String {
                if tipo == "L" {
                    arrLocales.append(desc)
                }else if tipo == "B" {
                    arrBancos.append(desc)
                }else if tipo == "O" {
                    arrOtros.append(desc)
                }
            }
        }
        lblLocales.text = arrLocales.joined(separator: "\n")
        lblBancos.text = arrBancos.joined(separator: "\n")
        lblOtros.text = arrOtros.joined(separator: "\n")
    }
    //MARK: - Actions
    @IBAction func atras(){
        navigationController?.popViewController(animated: true)
    }
}
