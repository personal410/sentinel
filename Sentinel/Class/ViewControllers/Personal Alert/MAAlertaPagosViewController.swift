//
//  MAAlertaPagosViewController.swift
//  Sentinel
//
//  Created by Daniel on 23/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
import Alamofire
import AlamofireImage

enum TipoAlertaPago{
    case todos, fecha
}

class MAAlertaPagosViewController:ParentViewController {
    //MARK: - IBOutlet
    @IBOutlet weak var toolbar:UIToolbar!
    @IBOutlet weak var alertasTV:UITableView!
    @IBOutlet weak var crearBT:UIButton!
    @IBOutlet weak var lcAlturaResumen:NSLayoutConstraint!
    @IBOutlet weak var lblTotalDia:UILabel!
    @IBOutlet weak var lblTotalSemana:UILabel!
    //MARK: - Variables
    var userData:UserClass!
    var arrPagos:[[String: Any]] = []
    var selectedIndex = -1
    var tipo:TipoAlertaPago = .todos
    var fechaSeleccionada = ""
    var totalSemana = ""
    var totalDia = ""
    //MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
        userData = UserGlobalData.sharedInstance.userGlobal!
        self.estilos()
        if tipo == .fecha {
            crearBT.isHidden = true
            var items = toolbar.items!
            items.removeLast()
            items[2].title = fechaSeleccionada
            toolbar.setItems(items, animated: false)
            lcAlturaResumen.constant = 74
            lblTotalDia.text = "S/ \(totalDia)"
            lblTotalSemana.text = "S/ \(totalSemana)"
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        validarPremium()
        UserGlobalData.sharedInstance.userGlobal.fechaPago = ""
        UserGlobalData.sharedInstance.userGlobal.fechaDia = ""
        UserGlobalData.sharedInstance.userGlobal.horaPago = ""
        if tipo == .todos {
            self.iniListarAlertaPagos()
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender:Any?) {
        if let viewCont = segue.destination as? CentroDePagoViewController {
            viewCont.dict = sender as? NSDictionary
        }else if let viewCont = segue.destination as? CrearAlertaPagoViewController {
            if let dic = sender as? NSDictionary {
                viewCont.alertaPago = dic
            }
        }else if let viewCont = segue.destination as? CalendarioViewController {
            viewCont.arrPagos = arrPagos
        }
    }
    //MARK: - IBActions
    @IBAction func accionCancelar(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func checkedChange(_ sender:UISwitch){
        selectedIndex = sender.tag
        if sender.isOn {
            let alertCont = UIAlertController(title: "Alerta", message: "¿Está seguro que desea marcar esta alerta como pagada?", preferredStyle: .alert)
            alertCont.addAction(UIAlertAction(title: "Aceptar", style: .default, handler:{(action) in
                self.showActivityIndicator(view: UIApplication.shared.keyWindow!)
                let dic = self.arrPagos[self.selectedIndex]
                let userData = UserGlobalData.sharedInstance.userGlobal!
                let params = ["STEntTDoc": dic["STEntTDoc"] as! String, "STEntNDoc": dic["STEntNDoc"] as! String, "STAPFecAle": dic["STAPFecAle"] as! String, "STAPEstPago": dic["STAPEstPago"] as! String, "UseTipDoc" : userData.tipoDocumento!, "UserId" : userData.user!, "SesionId" : userData.SesionId!]
                print("params: \(params)")
                NotificationCenter.default.addObserver(self, selector: #selector(self.endMarcarPagadoAlertaPago(_:)), name: NSNotification.Name("endMarcarPagadoAlertaPago"), object: nil)
                OriginData.sharedInstance.marcarPagadoAlertaPago(notification: "endMarcarPagadoAlertaPago", parametros: params)
            }))
            alertCont.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            present(alertCont, animated: true, completion: nil)
        }
    }
    //MARK: - Methods
    func inicializacion(){
        userData = UserGlobalData.sharedInstance.userGlobal!
    }
    func validarPremium(){
        if userData.EsPremium == "S" {
            self.changeStatusBar(cod: 3)
            setGradient(uiView: toolbar)
        }
    }
    func estilos(){
        crearBT.layer.shadowColor = UIColor.lightGray.cgColor
        crearBT.layer.shadowOffset = CGSize(width: 0, height: 3)
        alertasTV.estimatedRowHeight = 120.0
        alertasTV.rowHeight = UITableViewAutomaticDimension
    }
    func iniListarAlertaPagos(){
        self.showActivityIndicator(view: UIApplication.shared.keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endListarAlertaPagos(_:)), name: NSNotification.Name("endListarAlertaPagos"), object: nil)
        let parametros = ["UseTipDoc" : userData.tipoDocumento!, "UserId" : userData.user!, "SesionId" : userData.SesionId!]
        print(parametros)
        OriginData.sharedInstance.listarAlertasPagos(notification: "endListarAlertaPagos", parametros: parametros)
    }
    @objc func endListarAlertaPagos(_ notification:NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        guard let data = notification.object as? AlertaClass else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if let codigoWS = data.CodigoWS {
            if codigoWS == "0" {
                UserGlobalData.sharedInstance.alertaGlobal.SDTListadoAlertaPagos = data.SDTListadoAlertaPagos!
                if let arr = data.SDTListadoAlertaPagos as? [[String: Any]] {
                    arrPagos = arr
                }
                self.alertasTV.reloadData()
                return
            }else if codigoWS == "99" {
                showError99()
                return
            }
        }
        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "En estos momentos no se puede realizar la operación.", preferredStyle: .alert)
        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
        alertaVersion.addAction(actionVersion)
        self.present(alertaVersion, animated: true, completion: nil)
    }
    @objc func endMarcarPagadoAlertaPago(_ notification:NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard let data = notification.object as? AlertaClass else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if let codigoWS = data.CodigoWS {
            if codigoWS == "0" {
                arrPagos[selectedIndex]["STAPEstPago"] = "P"
                alertasTV.reloadRows(at: [IndexPath(row: selectedIndex, section: 0)], with: .none)
                return
            }else if codigoWS == "99" {
                showError99()
                return
            }
        }
        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "En estos momentos no se puede realizar la operación.", preferredStyle: .alert)
        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
        alertaVersion.addAction(actionVersion)
        self.present(alertaVersion, animated: true, completion: nil)
    }
}

extension MAAlertaPagosViewController : UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arrPagos.count
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AlertaDePagosTableViewCell
        let dict = arrPagos[indexPath.row]
        cell.montoLB.text = dict["STAPMonto"] as? String
        cell.fechaLB.text = self.devuelveFechaCorta(fecha: dict["STAPFecPago"] as! String)
        let pagado = dict["STAPEstPago"] as! String
        var color = UIColor(hex: "0272BE")
        if pagado == "R" {
            color = UIColor(hex: "EC2A3D")
        }else if pagado == "P" {
            color = UIColor(hex: "B3B3B3")
        }else if pagado == "A" {
            color = UIColor(hex: "F0C915")
        }
        cell.pagadoSW.tintColor = color
        cell.pagadoSW.tag = indexPath.row
        cell.pagadoSW.backgroundColor = color
        cell.pagadoSW.isOn = pagado == "P"
        cell.pagadoSW.isUserInteractionEnabled = pagado != "P"
        let foto = dict["STEntLogo"] as! String
        if foto != ""{
            cell.nombreLBL.text = ""
            cell.imagen.image = nil
            let url = NSURL(string: foto)! as URL
            Alamofire.request(url).responseImage { response in
                switch response.result {
                    case .success:
                        if let image = response.result.value {
                            print("image downloaded: \(image)")
                            cell.imagen.image = image
                        }
                        break
                    case .failure(let error):
                        print("error: \(error)")
                        break
                }
            }
        }else{
            cell.imagen.image = nil
            cell.nombreLBL.text = dict["STEntNom"] as? String
        }
        return cell
    }
    func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath){
        let dict = arrPagos[indexPath.row]
        let entTDoc = dict["STEntTDoc"] as! String
        performSegue(withIdentifier: entTDoc == "R" ? "showAlertaCentral" : "showAlertaPersonalizada", sender: dict)
    }
}
