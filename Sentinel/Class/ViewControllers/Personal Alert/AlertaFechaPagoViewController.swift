//
//  AlertaFechaPagoViewController.swift
//  Sentinel
//
//  Created by Daniel on 24/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class AlertaFechaPagoViewController:ParentViewController {
    //MARK: - Outlets
    @IBOutlet weak var fechaLB:UILabel!
    @IBOutlet weak var fechaDP:UIDatePicker!
    //MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
        fechaDP.minimumDate = Date()
        accionDetalle(0)
    }
    //MARK: - Actions
    @IBAction func accionDetalle(_ sender:Any){
        let date = fechaDP.date
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es_ES")
        dateFormatter.dateFormat = "EEEE"
        let weekday = dateFormatter.string(from: date).capitalized
        dateFormatter.dateFormat = "MMMM"
        let month = dateFormatter.string(from: date).capitalized
        dateFormatter.dateFormat = "'\(weekday)', dd 'de \(month) de' yyyy"
        let strDate = dateFormatter.string(from: fechaDP.date)
        print("strDate: \(strDate)")
        fechaLB.text = dateFormatter.string(from: date)
    }
    @IBAction func accionEstablecer(_ sender:Any){
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es_ES")
        dateFormatter.dateFormat = "dd/MM/yyyy"
        UserGlobalData.sharedInstance.userGlobal.fechaPago = dateFormatter.string(from: fechaDP.date)
        accionCancelar(0)
    }
    @IBAction func accionCancelar(_ sender:Any){
        self.navigationController?.popViewController(animated: false)
    }
}
