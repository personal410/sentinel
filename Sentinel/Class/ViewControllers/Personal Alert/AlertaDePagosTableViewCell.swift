//
//  DetalleDeDeudaTableViewCell.swift
//  Sentinel
//
//  Created by Richy on 18/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class AlertaDePagosTableViewCell: UITableViewCell {
    @IBOutlet weak var montoLB:UILabel!
    @IBOutlet weak var fechaLB:UILabel!
    @IBOutlet weak var pagadoSW:UISwitch!
    @IBOutlet weak var imagen:UIImageView!
    @IBOutlet weak var nombreLBL:UILabel!
    override func awakeFromNib(){
        super.awakeFromNib()
        pagadoSW.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
    }
}
