//
//  CalendarioViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 2/18/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import UIKit
class CalendarioViewController:ParentViewController, UIScrollViewDelegate{
    //MARK: - Attributes
    var daySize:CGFloat = 0
    var dayHeight:CGFloat = 0
    var arrInfoMonths:[MonthYear] = []
    var arrPagos:[[String:Any]] = []
    var arrMonths:Array<(monthYear:MonthYear, monthView:UIView)> = []
    var info:(String, String, String) = ("0", "0", "0")
    //MARK: - Outlets
    @IBOutlet weak var vCalendario:UIView!
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var lblTotalMes:UILabel!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        if UserGlobalData.sharedInstance.userGlobal.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(inNavBar: navBar)
            setGradient(uiView: vCalendario)
        }
        daySize = (view.frame.width - 16) / 7
        dayHeight = scrollView.frame.height / 6
    }
    override func viewWillAppear(_ animated:Bool){
        super.viewWillAppear(animated)
        for view in self.scrollView.subviews {
            view.removeFromSuperview()
        }
        let currentMonth = Toolbox.normalizeDate(date: Date())
        let previousMonth = Toolbox.getMonthWithDate(date: currentMonth, withDirection: -1)
        let nextMonth = Toolbox.getMonthWithDate(date: currentMonth, withDirection: 1)
        let previousMonthYearView = self.createMonthYearViewWithDate(date: previousMonth, withIndex: 0)
        let currentMonthYearView = self.createMonthYearViewWithDate(date: currentMonth, withIndex: 1)
        let nextMonthYearView = self.createMonthYearViewWithDate(date: nextMonth, withIndex: 2)
        arrMonths = [previousMonthYearView, currentMonthYearView, nextMonthYearView]
        actualizarTitulo()
        scrollView.contentSize = CGSize(width: self.view.frame.width * 3, height: currentMonthYearView.monthView.frame.height)
        scrollView.contentOffset = CGPoint(x: self.view.frame.width, y: 0)
        scrollView.addSubview(previousMonthYearView.monthView)
        scrollView.addSubview(currentMonthYearView.monthView)
        scrollView.addSubview(nextMonthYearView.monthView)
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? MAAlertaPagosViewController {
            viewCont.tipo = .fecha
            viewCont.arrPagos = sender as! [[String: Any]]
            viewCont.fechaSeleccionada = info.0
            viewCont.totalSemana = info.1
            viewCont.totalDia = info.2
        }
    }
    //MARK: - ScrollView
    func scrollViewDidEndDecelerating(_ sv:UIScrollView){
        if sv == self.scrollView {
            let contentOffsetX = scrollView.contentOffset.x
            if contentOffsetX == 0 {
                let nextMonthYearView = self.arrMonths[2]
                nextMonthYearView.monthView.removeFromSuperview()
                arrMonths.remove(at: 2)
                let prevMonthYearView = self.arrMonths[0]
                let currentDate = prevMonthYearView.monthYear.dateTemp()
                let prevDate = Toolbox.getMonthWithDate(date: currentDate, withDirection: -1)
                for monthYearView in arrMonths {
                    var frameTemp = monthYearView.monthView.frame
                    frameTemp.origin.x = frameTemp.origin.x + self.view.frame.width
                    monthYearView.monthView.frame = frameTemp
                }
                let newPrevMonthYearView = self.createMonthYearViewWithDate(date: prevDate, withIndex: 0)
                arrMonths.insert(newPrevMonthYearView, at: 0)
                self.scrollView.addSubview(newPrevMonthYearView.monthView)
                self.scrollView.contentOffset = CGPoint(x: self.view.frame.width, y: 0)
                self.scrollView.contentSize = CGSize(width: self.view.frame.width * 3, height: self.daySize * CGFloat(prevMonthYearView.monthYear.numberLines))
                actualizarTitulo()
            }else if contentOffsetX == self.view.frame.width * CGFloat(2) {
                let prevMonthYearView = self.arrMonths[0]
                prevMonthYearView.monthView.removeFromSuperview()
                arrMonths.removeFirst()
                let nextMonthYearView = self.arrMonths[1]
                let currentDate = nextMonthYearView.monthYear.dateTemp()
                let nextDate = Toolbox.getMonthWithDate(date: currentDate, withDirection: 1)
                for monthYearView in arrMonths {
                    var frameTemp = monthYearView.monthView.frame
                    frameTemp.origin.x = frameTemp.origin.x - self.view.frame.width
                    monthYearView.monthView.frame = frameTemp
                }
                let newNextMonthYearView = self.createMonthYearViewWithDate(date: nextDate, withIndex: 2)
                arrMonths.append(newNextMonthYearView)
                self.scrollView.addSubview(newNextMonthYearView.monthView)
                self.scrollView.contentOffset = CGPoint(x: self.view.frame.width, y: 0)
                self.scrollView.contentSize = CGSize(width: self.view.frame.width * 3, height: self.daySize * CGFloat(nextMonthYearView.monthYear.numberLines))
                actualizarTitulo()
            }
        }
    }
    //MARK: - Actions
    @IBAction func atras(){
        navigationController?.popViewController(animated: true)
    }
    //MARK: - Methods
    func createMonthYearViewWithDate(date:Date, withIndex index:Int) -> (monthYear:MonthYear, monthView:UIView) {
        let currentMonthYear = self.getMonthYearWithDate(date: date)
        let previousDate = Toolbox.getMonthWithDate(date: date, withDirection: -1)
        let previousMonthYear = self.getMonthYearWithDate(date: previousDate)
        let nextDate = Toolbox.getMonthWithDate(date: date, withDirection: 1)
        let nextMonthYear = self.getMonthYearWithDate(date: nextDate)
        let monthView = UIView(frame: CGRect(x: CGFloat(index) * self.view.frame.width + 8, y: 0, width: self.view.frame.width, height: dayHeight * 6))
        for i in 0 ..< (currentMonthYear.numberLines * 7) {
            let ind = i + 1
            let dayCtrl = UIControl(frame: CGRect(x: CGFloat(i%7) * daySize, y: floor(CGFloat(i) / 7.0) * dayHeight, width: daySize, height: dayHeight))
            dayCtrl.tag = ind
            dayCtrl.addTarget(self, action: #selector(didSelectDay(_:)), for: .touchUpInside)
            let dayLbl = UILabel(frame: CGRect(x: 0, y: 0, width: daySize, height: dayHeight / 3))
            dayLbl.textAlignment = .center
            dayLbl.numberOfLines = 1
            dayLbl.backgroundColor = UIColor.clear
            var dayNumber = 0
            if ind < currentMonthYear.firstDay {
                dayNumber = previousMonthYear.numberDays + 1 - currentMonthYear.firstDay + ind
                dayLbl.textColor = UIColor.lightGray
                var montoDia = 0.0
                var cantPagos = 0
                for dic in arrPagos {
                    if let fechaPago = dic["STAPFecAle"] as? String, let m = dic["STAPMonto"] as? String, let monto = Double(m) {
                        let arrFechaPago = fechaPago.split(separator: "-")
                        let anioFechaPago = Int(arrFechaPago[0])!
                        let mesFechaPago = Int(arrFechaPago[1])!
                        let diaFechaPago = Int(arrFechaPago[2])!
                        if anioFechaPago == previousMonthYear.year && mesFechaPago == previousMonthYear.month && diaFechaPago == dayNumber {
                            montoDia += monto
                            cantPagos += 1
                        }
                    }
                }
                if cantPagos > 0 {
                    let lblCantPagos = UILabel(frame: CGRect(x: 0, y: dayHeight / 3.0, width: daySize, height: dayHeight / 3))
                    lblCantPagos.textAlignment = .center
                    lblCantPagos.numberOfLines = 1
                    lblCantPagos.font = UIFont.systemFont(ofSize: 12)
                    lblCantPagos.backgroundColor = UIColor.clear
                    lblCantPagos.text = "\(cantPagos) pago\(cantPagos == 1 ? "" : "s")"
                    dayCtrl.addSubview(lblCantPagos)
                    let lblMonto = UILabel(frame: CGRect(x: 0, y: dayHeight * 2.0 / 3.0, width: daySize, height: dayHeight / 3))
                    lblMonto.textAlignment = .center
                    lblMonto.numberOfLines = 1
                    lblMonto.font = UIFont.systemFont(ofSize: 12)
                    lblMonto.textColor = UIColor.white
                    lblMonto.layer.cornerRadius = 5
                    lblMonto.layer.borderWidth = 1
                    lblMonto.layer.backgroundColor = UIColor(hex: "0072BD").cgColor
                    lblMonto.layer.borderColor = UIColor(hex: "B3B3B3").cgColor
                    lblMonto.text = Toolbox.getNumberFormmatter().string(from: NSNumber(value: montoDia))
                    dayCtrl.addSubview(lblMonto)
                }
            }else if ind < currentMonthYear.firstDay + currentMonthYear.numberDays {
                dayNumber = ind + 1 - currentMonthYear.firstDay
                dayLbl.textColor = UIColor.black
                let todayDayComps = Toolbox.getCalendar().dateComponents([.day, .month, .year], from: Date())
                if todayDayComps.month == currentMonthYear.month && todayDayComps.year == currentMonthYear.year {
                    if todayDayComps.day == dayNumber {
                        dayLbl.textColor = UIColor.red
                    }
                }
                var montoDia = 0.0
                var cantPagos = 0
                for dic in arrPagos {
                    if let fechaPago = dic["STAPFecAle"] as? String, let m = dic["STAPMonto"] as? String, let monto = Double(m) {
                        let arrFechaPago = fechaPago.split(separator: "-")
                        let anioFechaPago = Int(arrFechaPago[0])!
                        let mesFechaPago = Int(arrFechaPago[1])!
                        let diaFechaPago = Int(arrFechaPago[2])!
                        if anioFechaPago == currentMonthYear.year && mesFechaPago == currentMonthYear.month && diaFechaPago == dayNumber {
                            montoDia += monto
                            cantPagos += 1
                        }
                    }
                }
                if cantPagos > 0 {
                    let lblCantPagos = UILabel(frame: CGRect(x: 0, y: dayHeight / 3.0, width: daySize, height: dayHeight / 3))
                    lblCantPagos.textAlignment = .center
                    lblCantPagos.numberOfLines = 1
                    lblCantPagos.font = UIFont.systemFont(ofSize: 12)
                    lblCantPagos.backgroundColor = UIColor.clear
                    lblCantPagos.text = "\(cantPagos) pago\(cantPagos == 1 ? "" : "s")"
                    dayCtrl.addSubview(lblCantPagos)
                    let lblMonto = UILabel(frame: CGRect(x: 0, y: dayHeight * 2.0 / 3.0, width: daySize, height: dayHeight / 3))
                    lblMonto.textAlignment = .center
                    lblMonto.numberOfLines = 1
                    lblMonto.font = UIFont.systemFont(ofSize: 12)
                    lblMonto.textColor = UIColor.white
                    lblMonto.layer.cornerRadius = 5
                    lblMonto.layer.borderWidth = 1
                    lblMonto.layer.backgroundColor = UIColor(hex: "0072BD").cgColor
                    lblMonto.layer.borderColor = UIColor(hex: "B3B3B3").cgColor
                    lblMonto.text = Toolbox.getNumberFormmatter().string(from: NSNumber(value: montoDia))
                    dayCtrl.addSubview(lblMonto)
                }
            }else{
                dayNumber = ind + 1 - currentMonthYear.firstDay - currentMonthYear.numberDays
                dayLbl.textColor = UIColor.lightGray
                var montoDia = 0.0
                var cantPagos = 0
                for dic in arrPagos {
                    if let fechaPago = dic["STAPFecAle"] as? String, let m = dic["STAPMonto"] as? String, let monto = Double(m) {
                        let arrFechaPago = fechaPago.split(separator: "-")
                        let anioFechaPago = Int(arrFechaPago[0])!
                        let mesFechaPago = Int(arrFechaPago[1])!
                        let diaFechaPago = Int(arrFechaPago[2])!
                        if anioFechaPago == nextMonthYear.year && mesFechaPago == nextMonthYear.month && diaFechaPago == dayNumber {
                            montoDia += monto
                            cantPagos += 1
                        }
                    }
                }
                if cantPagos > 0 {
                    let lblCantPagos = UILabel(frame: CGRect(x: 0, y: dayHeight / 3.0, width: daySize, height: dayHeight / 3))
                    lblCantPagos.textAlignment = .center
                    lblCantPagos.numberOfLines = 1
                    lblCantPagos.font = UIFont.systemFont(ofSize: 12)
                    lblCantPagos.backgroundColor = UIColor.clear
                    lblCantPagos.text = "\(cantPagos) pago\(cantPagos == 1 ? "" : "s")"
                    dayCtrl.addSubview(lblCantPagos)
                    let lblMonto = UILabel(frame: CGRect(x: 0, y: dayHeight * 2.0 / 3.0, width: daySize, height: dayHeight / 3))
                    lblMonto.textAlignment = .center
                    lblMonto.numberOfLines = 1
                    lblMonto.font = UIFont.systemFont(ofSize: 12)
                    lblMonto.textColor = UIColor.white
                    lblMonto.layer.cornerRadius = 5
                    lblMonto.layer.borderWidth = 1
                    lblMonto.layer.backgroundColor = UIColor(hex: "0072BD").cgColor
                    lblMonto.layer.borderColor = UIColor(hex: "B3B3B3").cgColor
                    lblMonto.text = Toolbox.getNumberFormmatter().string(from: NSNumber(value: montoDia))
                    dayCtrl.addSubview(lblMonto)
                }
            }
            let finalAttributeText = NSMutableAttributedString(string: "")
            finalAttributeText.append(NSAttributedString(string: "\(dayNumber)\n", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15)]))
            dayLbl.attributedText = finalAttributeText
            let vLinea = UIView(frame: CGRect(x: 0, y: dayCtrl.frame.height - 1, width: dayCtrl.frame.width, height: 1))
            vLinea.backgroundColor = UIColor.white
            dayCtrl.addSubview(vLinea)
            dayCtrl.addSubview(dayLbl)
            monthView.addSubview(dayCtrl)
        }
        return (currentMonthYear, monthView)
    }
    func getMonthYearWithDate(date:Date) -> MonthYear {
        let monthYear:MonthYear
        let arrInfoMonthsTemp = arrInfoMonths.filter(){$0.dateTemp() == date}
        if arrInfoMonthsTemp.count > 0 {
            monthYear = arrInfoMonthsTemp.first!
        }else{
            monthYear = MonthYear(date: date)
            arrInfoMonths.append(monthYear)
        }
        return monthYear
    }
    @objc func didSelectDay(_ ctrl:UIControl){
        if ctrl.subviews.count == 1 {
            let alertCont = UIAlertController(title: "Alerta", message: "No tiene alertas en esta fecha", preferredStyle: .alert)
            alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alertCont, animated: true, completion: nil)
        }else{
            let ind = ctrl.tag
            let currentMonthYear = arrMonths[1].monthYear
            var anio = 0, mes = 0, dia = 0
            if ind < currentMonthYear.firstDay {
                let previousMonthYear = arrMonths[0].monthYear
                anio = previousMonthYear.year
                mes = previousMonthYear.month
                dia = previousMonthYear.numberDays + ind - currentMonthYear.firstDay
            }else if ind < currentMonthYear.firstDay + currentMonthYear.numberDays {
                anio = currentMonthYear.year
                mes = currentMonthYear.month
                dia = ind - currentMonthYear.firstDay
            }else{
                let nextMonthYear = arrMonths[2].monthYear
                anio = nextMonthYear.year
                mes = nextMonthYear.month
                dia = ind - currentMonthYear.firstDay - currentMonthYear.numberDays
            }
            dia += 1
            var dateComps = DateComponents()
            dateComps.day = dia
            dateComps.month = mes
            dateComps.year = anio
            let fechaSeleccionada = Toolbox.getCalendar().date(from: dateComps)!
            let weekday = (Toolbox.getCalendar().component(.weekday, from: fechaSeleccionada) + 5) % 7 + 1
            var dateCompsAgregar = DateComponents()
            dateCompsAgregar.day = 1 - weekday
            let primerDia = Toolbox.getCalendar().date(byAdding: dateCompsAgregar, to: fechaSeleccionada)!
            dateCompsAgregar.day = 7 - weekday
            let ultimoDia = Toolbox.getCalendar().date(byAdding: dateCompsAgregar, to: fechaSeleccionada)!
            var arrPagosTemp:[[String: Any]] = []
            var montoSemana = 0.0
            var montoDia = 0.0
            for dic in arrPagos {
                if let fechaPago = dic["STAPFecAle"] as? String, let m = dic["STAPMonto"] as? String, let monto = Double(m) {
                    let arrFechaPago = fechaPago.split(separator: "-")
                    var fechaPagoDateComps = DateComponents()
                    fechaPagoDateComps.year = Int(arrFechaPago[0])!
                    fechaPagoDateComps.month = Int(arrFechaPago[1])!
                    fechaPagoDateComps.day = Int(arrFechaPago[2])!
                    let dFechaPago = Toolbox.getCalendar().date(from: fechaPagoDateComps)!
                    if dFechaPago >= primerDia && dFechaPago <= ultimoDia {
                        montoSemana += monto
                        if dFechaPago == fechaSeleccionada {
                            arrPagosTemp.append(dic)
                            montoDia += monto
                        }
                    }
                }
            }
            let dateFormatter = Toolbox.getDateFormatter()
            dateFormatter.dateFormat = "EEEE"
            let diaSemana = dateFormatter.string(from: fechaSeleccionada).uppercased()
            dateFormatter.dateFormat = "MMMM"
            let nombreMes = dateFormatter.string(from: fechaSeleccionada).uppercased()
            dateFormatter.dateFormat = "'\(diaSemana)' d 'de \(nombreMes)'"
            let numberFormatter = Toolbox.getNumberFormmatter()
            info = (dateFormatter.string(from: fechaSeleccionada), numberFormatter.string(from: NSNumber(value: montoSemana))!, numberFormatter.string(from: NSNumber(value: montoDia))!)
            performSegue(withIdentifier: "showAlertas", sender: arrPagosTemp)
        }
    }
    func actualizarTitulo(){
        let currentMonthYear = self.arrMonths[1].monthYear
        let currentMonth = currentMonthYear.dateTemp()
        navBar.topItem?.title = Toolbox.getDate(date: currentMonth, withFormat: "MMMM yyyy").capitalized
        var montoMes = 0.0
        for dic in arrPagos {
            if let fechaPago = dic["STAPFecAle"] as? String, let m = dic["STAPMonto"] as? String, let monto = Double(m) {
                let arrFechaPago = fechaPago.split(separator: "-")
                let anioFechaPago = Int(arrFechaPago[0])!
                let mesFechaPago = Int(arrFechaPago[1])!
                if anioFechaPago == currentMonthYear.year && mesFechaPago == currentMonthYear.month {
                    montoMes += monto
                }
            }
        }
        lblTotalMes.text = "S/ \(Toolbox.getNumberFormmatter().string(from: NSNumber(value: montoMes))!)"
    }
}
