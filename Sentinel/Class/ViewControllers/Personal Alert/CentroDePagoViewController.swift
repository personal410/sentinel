//
//  CentroDePagoViewController.swift
//  Sentinel
//
//  Created by Richy on 19/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
import Alamofire
import AlamofireImage
class CentroDePagoViewController:ParentViewController, UITableViewDelegate, UITableViewDataSource {
    //MARK: - Outlets
    @IBOutlet weak var vTop:UIView!
    @IBOutlet weak var fechaTF:UITextField!
    @IBOutlet weak var horaTF:UITextField!
    @IBOutlet weak var nombreEntidad:UIBarButtonItem!
    @IBOutlet weak var montoLB:UILabel!
    @IBOutlet weak var fechaPagoLB:UILabel!
    @IBOutlet weak var nombreIdentificador:UILabel!
    @IBOutlet weak var codigoIdentificador:UILabel!
    @IBOutlet weak var entidadIMG:UIImageView!
    @IBOutlet weak var tabla:UITableView!
    //MARK: - Attributes
    let grayBorderColor = UIColor.fromHex(0xE6E6E6).cgColor
    var arregloLugares = [NSDictionary]()
    var dict:NSDictionary!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.estilos()
        self.cargarData()
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?) {
        if let viewCont = segue.destination as? MapaViewController {
            viewCont.arrLugares = arregloLugares
        }
    }
    //MARK: - Methods
    func estilos(){
        if UserGlobalData.sharedInstance.userGlobal.EsPremium == "S" {
            self.changeStatusBar(cod: 3)
            setGradient(uiView: vTop)
        }
    }
    func cargarData(){
        nombreEntidad.title = dict.object(forKey: "STEntNom") as? String
        if let logo = dict.object(forKey: "STEntLogo") as? String {
            if let url = URL(string: logo) {
                Alamofire.request(url).responseImage{ response in
                    switch response.result {
                    case .success:
                        if let image = response.result.value {
                            self.entidadIMG.image = image
                        }
                        break
                    case .failure(let error):
                        print("error: \(error)")
                        break
                    }
                }
            }
        }
        let monto = dict.object(forKey: "STAPMonto") as! String
        montoLB.text = "S/ \(monto)"
        let STAPFecPago = dict.object(forKey: "STAPFecPago") as! String
        fechaPagoLB.text = self.devuelveFechaCorta(fecha: STAPFecPago)
        let STEUNomIdPago = dict.object(forKey: "STEUNomIdPago") as! String
        print("STEUNomIdPago: \(STEUNomIdPago)")
        if STEUNomIdPago != "" {
            nombreIdentificador.text = "\(STEUNomIdPago):"
        }else{
            nombreIdentificador.text = ""
        }
        codigoIdentificador.text = dict.object(forKey: "STEUCodIdPago") as? String
        let STAPFecAle = dict.object(forKey: "STAPFecAle") as! String
        fechaTF.text = self.devuelveFecha(fecha: STAPFecAle)
        horaTF.text = dict.object(forKey: "STAPHorAle") as? String
        iniListarLugaresCercanos()
    }
    func iniListarLugaresCercanos(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endListarLugaresCercanos(_:)), name: NSNotification.Name("endListarLugaresCercanos"), object: nil)
        let userData = UserGlobalData.sharedInstance.userGlobal!
        let parametros = [
            "STEntNDoc":dict.object(forKey: "STEntNDoc") as! String,
            "STEntTDoc":dict.object(forKey: "STEntTDoc") as! String,
            "SesionId":userData.SesionId!,
            "UseTipDoc":userData.tipoDocumento!,
            "UserId":userData.user!
            ] as [String : Any]
        print(parametros)
        OriginData.sharedInstance.listarLugares(notification: "endListarLugaresCercanos", parametros: parametros)
    }
    @objc func endListarLugaresCercanos(_ notification:NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        guard let data = notification.object as? AlertaClass, let codigoWS = data.CodigoWS else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if codigoWS == "0" {
            arregloLugares = data.SDTLugaresCercanosST as! [NSDictionary]
            if arregloLugares.isEmpty {
                let alertCont = UIAlertController(title: "Alerta", message: "No tiene direcciones", preferredStyle: .alert)
                alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                present(alertCont, animated: true, completion: nil)
            }else{
                tabla.reloadData()
            }
            return
        }else if codigoWS == "99" {
            showError99()
            return
        }
    }
    //MARK: - Actions
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buscar(){
        if arregloLugares.isEmpty {
            let alertCont = UIAlertController(title: "Alerta", message: "No tiene direcciones", preferredStyle: .alert)
            alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alertCont, animated: true, completion: nil)
        }else{
            performSegue(withIdentifier: "showMapa", sender: nil)
        }
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arregloLugares.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MapaTableViewCell
        let objDict = arregloLugares[indexPath.row]
        cell.descripcionLB.text = objDict.object(forKey: "STELCDirecc") as? String
        return cell
    }
}
