//
//  XLEmpresarialMainTabsViewController.swift
//  Sentinel
//
//  Created by Daniel Montoya on 1/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class XLEmpresarialMainTabsViewController: ButtonBarPagerTabStripViewController {

    var isReload = false
    let graySpotifyColor = UIColor.fromHex(rgbValue: 0x1A1A1A)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // change selected bar color
        settings.style.buttonBarHeight = 44.0
        settings.style.buttonBarBackgroundColor = graySpotifyColor
        settings.style.buttonBarItemBackgroundColor = UIColor.clear
        settings.style.selectedBarBackgroundColor =  UIColor.fromHex(rgbValue: 0xffffff)
        settings.style.buttonBarItemFont = UIFont(name: "Roboto", size:12) ??
            UIFont.systemFont(ofSize: 12)
        
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor.clear
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        
        settings.style.buttonBarLeftContentInset = 5
        settings.style.buttonBarRightContentInset = 5
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.fromHex(rgbValue: 0x999999)
            newCell?.label.textColor = UIColor.fromHex(rgbValue: 0xffffff)
            
            oldCell?.imageView.tintColor = UIColor.fromHex(rgbValue: 0xffffff)
            newCell?.imageView.tintColor = UIColor.fromHex(rgbValue: 0x999999)
            
        }
        
        super.viewDidLoad()
        
        self.reloadPagerTabStripView()
        
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let storyboardMain = UIStoryboard(name: "Consultas", bundle: nil)
        
        let BusquedaSabioVC = storyboardMain.instantiateViewController(withIdentifier: "BusquedaTipoEmpresarialVC") as! BusquedaTipoEmpresarialViewController
        
        let HistorialSabioVC = storyboardMain.instantiateViewController(withIdentifier: "HistorialEmpresarialVC") as! HistorialEmpresarialViewController
        
        guard isReload else {
            return [BusquedaSabioVC, HistorialSabioVC]
        }
        
        var childViewControllers = [BusquedaSabioVC, HistorialSabioVC] as [Any]
        
        for (index, _) in childViewControllers.enumerated(){
            let nElements = childViewControllers.count - index
            let n = (Int(arc4random()) % nElements) + index
            if n != index{
                childViewControllers.swapAt(index, n)
            }
        }
        
        let nItems = 2
        return Array(childViewControllers.prefix(Int(nItems))) as! [UIViewController]
        
    }
    
    override func reloadPagerTabStripView() {
        moveToViewController(at: 3, animated: true)
        super.reloadPagerTabStripView()
    }

}
