//
//  SegundoTabEmpresarialViewController.swift
//  Sentinel
//
//  Created by Daniel Montoya on 1/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class SegundoTabEmpresarialViewController: ParentViewController, IndicatorInfoProvider {

    var userData : UserClass!
    var tercero : TerceroClass!
    
    var esPremium = ""
    var creditosArray : NSArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        
        return IndicatorInfo(title: "CRÉDITOS E INDICADORES", image: UIImage(named: ""))
        
    }

}
