//
//  BusquedaPersonaEmpresaEmpresarialViewController.swift
//  Sentinel
//
//  Created by Daniel on 27/01/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class BusquedaPersonaEmpresaEmpresarialViewController: ParentViewController, UITextFieldDelegate {

    @IBOutlet weak var disponiblesLB: UILabel!
    @IBOutlet weak var nombreDisponibleLB: UILabel!
    
    @IBOutlet weak var consultaPersonaToolbar: UIToolbar!
    
    @IBOutlet weak var apePaternoTXT: UITextField!
    
    var tercero : TerceroClass?
    var userData : UserClass?
    
    var checkCPT : String = "NO"
    var checkHisto : String = "NO"
    
    var arregloCoincidenciasCPT : NSArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializarVariables()
        self.gestureScreen()
        self.cargarDatos()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.iniConsultaDatosServicioParent()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func inicializarVariables(){
        self.consultaPersonaToolbar.clipsToBounds = true
        
        self.tercero = UserGlobalData.sharedInstance.terceroGlobal
        self.userData = UserGlobalData.sharedInstance.userGlobal
    }
    
    func cargarDatos(){
        
        //CARGA DE DISPONIBLES X SERV TERCERO
        let disponbles : String = UserGlobalData.sharedInstance.userGlobal.AFSerCPTNDisp
        if disponbles == ""{
            self.disponiblesLB.text = "0"
        }
        else
        {
            let colorD : Int = Int(disponbles)!
            if colorD < 0 {
                self.nombreDisponibleLB.text = "Sobregirado"
                self.nombreDisponibleLB.textColor = UIColor.red
                self.disponiblesLB.textColor = UIColor.red
                self.disponiblesLB.text = disponbles
            }
            else
            {
                self.nombreDisponibleLB.text = "Disponibles"
                self.nombreDisponibleLB.textColor = UIColor.white
                self.disponiblesLB.textColor = UIColor.white
                self.disponiblesLB.text = disponbles
            }
        }
    }
    
    func iniBusquedaPorCPT(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endBusquedaPorCPT(_:)), name: NSNotification.Name("endBusquedaPorCPT"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        let userData = UserGlobalData.sharedInstance.userGlobal

        let sesion : String = (userData?.SesionId)!
        let servicio = UserGlobalData.sharedInstance.misServiciosGlobal!
        let nombre : String = self.apePaternoTXT.text!
        var tipocheck : String = ""
        if checkCPT == "SI"{
            tipocheck = "NOM"
        }
        else if checkHisto == "SI"{
            tipocheck = "CON"
        }

        let parametros = [
            "UserId" : userData?.user as! String,
            "AFSerCod" : servicio.NroServicio!,
            "UseSeIDSession" : sesion,
            "CnpNomRaz" : nombre,
            "TipoBus" : tipocheck
            ] as [String : Any]
        OriginData.sharedInstance.miReporteDNITerceroSERV6ESPECIAL(notification: "endBusquedaPorCPT", parametros: parametros)
    }
    
    @objc func endBusquedaPorCPT(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as! TerceroClass
            if data.codigoWS == "0"{
                
                let nav = self.storyboard?.instantiateViewController(withIdentifier: "ResultadoPersonaEmpresarialVC") as! ResultadoPersonaEmpresarialViewController
                nav.arregloCoincidenciasCPT = data.SDTBusAlf!
                //nav.apePaterno = self.apePaternoTXT.text!
                nav.checkCPT = self.checkCPT
                nav.checkHisto = self.checkHisto
                
                self.navigationController?.pushViewController(nav, animated: true)
                
            }
            else
            {
                if data.codigoWS != nil {
                    if data.codigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        iniMensajeError(codigo: (data.codigoWS!))
                    }
                }
                else
                {
                    let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                    alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                    self.present(alerta, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func accionBuscar(_ sender: Any) {
        if self.apePaternoTXT.text != ""{
            self.iniBusquedaPorCPT()
        }
        else
        {
            let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "Debe llenar el campo requerido.", preferredStyle: .alert)
            
            let actionCancel = UIAlertAction.init(title: "ACEPTAR", style: .cancel, handler: nil)
            alertaVersion.addAction(actionCancel)
            
            self.present(alertaVersion, animated: true, completion: nil)
        }
        
    }
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func accionComprar(_ sender: Any) {
        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "Las compras de Sentinel Empresarial se realiza de forma personalizada con la visita de un asesor, por favor contáctanos al teléfono: 514-9000 Anexo 132 o desde nuestra web: www.sentinelperu.com", preferredStyle: .alert)
        
        let actionCancel = UIAlertAction.init(title: "ACEPTAR", style: .cancel, handler: nil)
        alertaVersion.addAction(actionCancel)
        
        self.present(alertaVersion, animated: true, completion: nil)
    }
}
