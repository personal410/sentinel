//
//  BusquedaTipoEmpresarialViewController.swift
//  Sentinel
//
//  Created by Daniel Montoya on 1/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//BusquedaTipoEmpresarialViewController
import XLPagerTabStrip
class BusquedaTipoEmpresarialViewController: ParentViewController, IndicatorInfoProvider {
    
    @IBOutlet weak var primerView: UIView!
    @IBOutlet weak var segundoView: UIView!
    @IBOutlet weak var cptBT: UIButton!
    @IBOutlet weak var historialBT: UIButton!
    @IBOutlet weak var disponiblesLB: UILabel!
    @IBOutlet weak var nombreDisponibleLB: UILabel!
    
    var checkCPT : String = "NO"
    var checkHisto : String = "NO"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.estilos()
    }
    override func viewDidAppear(_ animated: Bool) {
        cargarData()
    }
    
    func estilos(){
        self.primerView.layer.cornerRadius = 5
        
        primerView.layer.shadowColor = UIColor.lightGray.cgColor
        primerView.layer.shadowOpacity = 0.5
        primerView.layer.shadowOffset = CGSize.zero
        
        self.segundoView.layer.cornerRadius = 5
        
        segundoView.layer.shadowColor = UIColor.lightGray.cgColor
        segundoView.layer.shadowOpacity = 0.5
        segundoView.layer.shadowOffset = CGSize.zero
    }
    
    func cargarData(){
        let disponbles : String = UserGlobalData.sharedInstance.userGlobal.AFSerCPTNDisp
        if disponbles == ""{
            self.disponiblesLB.text = "0"
        }
        else
        {
            let colorD : Int = Int(disponbles)!
            if colorD < 0 {
                self.nombreDisponibleLB.text = "Sobregirado: "
                self.nombreDisponibleLB.textColor = UIColor.red
                self.disponiblesLB.textColor = UIColor.red
                self.disponiblesLB.text = disponbles
            }
            else
            {
                self.nombreDisponibleLB.text = "Disponibles: "
                self.nombreDisponibleLB.textColor = UIColor.white
                self.disponiblesLB.textColor = UIColor.white
                self.disponiblesLB.text = disponbles
            }
        }
    }
    
    @IBAction func accionDocumento(_ sender: Any) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "BusquedaxDocumentoSabioVC") as! DNIEmpresarialViewController
        nav.checkCPT = self.checkCPT
        nav.checkHisto = self.checkHisto
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    @IBAction func accionNombreMpresa(_ sender: Any) {
        if self.checkCPT == "SI" || self.checkHisto == "SI" {
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "BusquedaPersonaEmpresaEmpresarialVC") as! BusquedaPersonaEmpresaEmpresarialViewController
            nav.checkCPT = self.checkCPT
            nav.checkHisto = self.checkHisto
            self.navigationController?.pushViewController(nav, animated: true)
        }
        else
        {
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "PersonaEmpresarialVC") as! PersonaEmpresarialViewController
            self.navigationController?.pushViewController(nav, animated: true)
        }
        
    }
    
    @IBAction func accionCheckCPT(_ sender: Any) {
        if checkHisto == "NO"{
            if checkCPT == "NO"{
                checkCPT = "SI"
                
                self.cptBT.setImage(UIImage.init(named: "check.verde"), for: .normal)
            }
            else
            {
                checkCPT = "NO"
                
                self.cptBT.setImage(UIImage.init(named: "circulo1"), for: .normal)
            }
        }
        if checkHisto == "SI" {
            if checkCPT == "NO"{
                checkCPT = "SI"
                checkHisto = "NO"
                self.cptBT.setImage(UIImage.init(named: "check.verde"), for: .normal)
                self.historialBT.setImage(UIImage.init(named: "circulo1"), for: .normal)
            }
        }
    }
    
    @IBAction func accionCheckHistorial(_ sender: Any) {
        if checkCPT == "NO"{
            if checkHisto == "NO"{
                checkHisto = "SI"
                
                self.historialBT.setImage(UIImage.init(named: "check.verde"), for: .normal)
            }
            else
            {
                checkHisto = "NO"
                
                self.historialBT.setImage(UIImage.init(named: "circulo1"), for: .normal)
            }
        }
        if checkCPT == "SI" {
            if checkHisto == "NO"{
                checkHisto = "SI"
                checkCPT = "NO"
                self.historialBT.setImage(UIImage.init(named: "check.verde"), for: .normal)
                self.cptBT.setImage(UIImage.init(named: "circulo1"), for: .normal)
            }
        }
    }
    @IBAction func accionCompra(_ sender: Any) {
        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "Las compras de Sentinel Empresarial se realiza de forma personalizada con la visita de un asesor, por favor contáctanos al teléfono: 514-9000 Anexo 132 o desde nuestra web: www.sentinelperu.com", preferredStyle: .alert)
        
        let actionCancel = UIAlertAction.init(title: "ACEPTAR", style: .cancel, handler: nil)
        alertaVersion.addAction(actionCancel)
        
        self.present(alertaVersion, animated: true, completion: nil)
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "", image: UIImage(named: "busqueda.on"))
    }
    
}
