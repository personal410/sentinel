//
//  PersonaEmpresarialViewController.swift
//  Sentinel
//
//  Created by Daniel Montoya on 1/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class PersonaEmpresarialViewController: ParentViewController ,MiPickerComboDelegate, UITextFieldDelegate {

    @IBOutlet weak var disponiblesLB: UILabel!
    @IBOutlet weak var nombreDisponibleLB: UILabel!
    
    @IBOutlet weak var consultaPersonaToolbar: UIToolbar!
    @IBOutlet weak var comboPersona: CTComboPicker!
    @IBOutlet weak var personaJuridicaTXT: UITextField!
    @IBOutlet weak var otrosTXT: UITextField!
    @IBOutlet weak var personaNaturalView: UIView!
    
    @IBOutlet weak var apePaternoTXT: UITextField!
    @IBOutlet weak var apeMaternoTXT: UITextField!
    @IBOutlet weak var nombresTXT: UITextField!
    
    var tercero : TerceroClass?
    var userData : UserClass?
    
    var checkCPT : String = "NO"
    var checkHisto : String = "NO"
    
    var codCombo = "1"
    var desCombo = "Persona Natural"
    var combosArray: NSMutableArray = NSMutableArray()
    var combosIDArray: NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializarVariables()
        self.gestureScreen()
        self.cargarDatos()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.iniConsultaDatosServicioParent()
    }
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func inicializarVariables(){
        self.consultaPersonaToolbar.clipsToBounds = true
        
        self.tercero = UserGlobalData.sharedInstance.terceroGlobal
        self.userData = UserGlobalData.sharedInstance.userGlobal
        
        self.comboPersona.delegate = self
        
        self.combosArray.add("Persona Natural")
        self.combosArray.add("Persona Jurídica")
        self.combosArray.add("Otros")
        
        self.combosIDArray.add("1")
        self.combosIDArray.add("2")
        self.combosIDArray.add("3")
    }
    
    func cargarDatos(){
        
        //CARGA DE DISPONIBLES X SERV TERCERO
        let disponbles : String = UserGlobalData.sharedInstance.userGlobal.AFSerCPTNDisp
        if disponbles == ""{
            self.disponiblesLB.text = "0"
        }
        else
        {
            let colorD : Int = Int(disponbles)!
            if colorD < 0 {
                self.nombreDisponibleLB.text = "Sobregirado"
                self.nombreDisponibleLB.textColor = UIColor.red
                self.disponiblesLB.textColor = UIColor.red
                self.disponiblesLB.text = disponbles
            }
            else
            {
                self.nombreDisponibleLB.text = "Disponibles"
                self.nombreDisponibleLB.textColor = UIColor.white
                self.disponiblesLB.textColor = UIColor.white
                self.disponiblesLB.text = disponbles
            }
        }
    }
    
    @IBAction func accionCombo(_ sender: Any) {
        
        self.comboPersona.setConfig(combosArray as! [Any])
        self.comboPersona.open()
        self.comboPersona.setId(1)
    }
    @IBAction func accionComboDos(_ sender: Any) {
        
        self.comboPersona.setConfig(combosArray as! [Any])
        self.comboPersona.open()
        self.comboPersona.setId(1)
    }
    @IBAction func accionBuscar(_ sender: Any) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "ResultadoPersonaEmpresarialVC") as! ResultadoPersonaEmpresarialViewController
        nav.apePaterno = self.apePaternoTXT.text!
        nav.apeMaterno = self.apeMaternoTXT.text!
        nav.nombre = self.nombresTXT.text!
        nav.tipoPersona = self.codCombo
        nav.otros = self.otrosTXT.text!
        nav.personaJuridica = self.personaJuridicaTXT.text!
        nav.checkCPT = self.checkCPT
        nav.checkHisto = self.checkHisto
        self.navigationController?.pushViewController(nav, animated: true)
    }
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func accionComprar(_ sender: Any) {
        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "Las compras de Sentinel Empresarial se realiza de forma personalizada con la visita de un asesor, por favor contáctanos al teléfono: 514-9000 Anexo 132 o desde nuestra web: www.sentinelperu.com", preferredStyle: .alert)
        
        let actionCancel = UIAlertAction.init(title: "ACEPTAR", style: .cancel, handler: nil)
        alertaVersion.addAction(actionCancel)
        
        self.present(alertaVersion, animated: true, completion: nil)
    }
    
    func miPickerCombo(_ pickerid: Int, didSelectRow row: Int) {
        switch pickerid {
        case 1:
            codCombo = combosIDArray[row] as! String
            desCombo = combosArray[row] as! String
            if codCombo == "1"{
                self.personaNaturalView.isHidden = false
                self.personaJuridicaTXT.isHidden = true
                self.otrosTXT.isHidden = true
            }
            else if codCombo == "2"{
                self.personaNaturalView.isHidden = true
                self.personaJuridicaTXT.isHidden = false
                self.otrosTXT.isHidden = true
            }
            else if codCombo == "3"{
                self.personaNaturalView.isHidden = true
                self.personaJuridicaTXT.isHidden = true
                self.otrosTXT.isHidden = false
            }
            break
        case 2:
            break
        default:
            break
        }
    }
    
    func miPickerCombo(_ MiPickerView: CTComboPicker!, pickerView picker: UIPickerView!, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
}
