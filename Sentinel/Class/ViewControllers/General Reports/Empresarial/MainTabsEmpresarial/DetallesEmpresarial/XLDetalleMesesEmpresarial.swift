//
//  XLDetalleMesesEmpresarial.swift
//  Sentinel
//
//  Created by Daniel on 22/11/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import XLPagerTabStrip
class XLDetalleMesesEmpresarial: ButtonBarPagerTabStripViewController, LoaderVC {
    var cargandoMesSeleccionado = true
    var mesDefault:Int = UserDefaults.standard.object(forKey: "mesSemaforo") as! Int
    
    @IBOutlet weak var barBB: ButtonBarView!
    
    var arrayMesesController = NSMutableArray()
    var arrayMeses = NSMutableArray()
    var tercero:TerceroClass!
    
    var EneroViewController : EneroEViewController!
    var FebreroViewController : FebreroEViewController!
    var MarzoViewController : MarzoEViewController!
    var AbrilViewController : AbrilEViewController!
    var MayoViewController : MayoEViewController!
    var JunioViewController : JunioEViewController!
    var JulioViewController : JulioEViewController!
    var AgostoViewController : AgostoEViewController!
    var SetiembreViewController : SetiembreEViewController!
    var OctubreViewController : OctubreEViewController!
    var NoviembreViewController : NoviembreEViewController!
    var DiciembreViewController : DiciembreEViewController!
    
    override func viewDidLoad(){
        self.tercero = UserGlobalData.sharedInstance.terceroGlobal
        self.estilos()
    }
    override func viewDidAppear(_ animated:Bool){
        buttonBarView.selectItem(at: IndexPath(item: mesDefault, section: 0), animated: true, scrollPosition: [])
        buttonBarView.moveTo(index: mesDefault, animated: true, swipeDirection: .right, pagerScroll: .yes)
        if mesDefault > 0 {
            moveToViewController(at: mesDefault, animated: true)
        }
    }
    override func viewDidLayoutSubviews(){
        print("viewDidLayoutSubviews")
    }
    func estilos(){
        settings.style.buttonBarBackgroundColor = UIColor.fromHex(0x1a1a1a)
        settings.style.buttonBarHeight = 44.0
        settings.style.buttonBarItemBackgroundColor = UIColor.clear
        settings.style.selectedBarBackgroundColor =  UIColor.fromHex(0xffffff)
        settings.style.buttonBarItemFont = UIFont(name: "HelveticaNeue-Bold", size:12) ?? UIFont.systemFont(ofSize: 12)
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor.clear
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 5
        settings.style.buttonBarRightContentInset = 5
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            //guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.fromHex(0xffffff)
            newCell?.label.textColor = UIColor.fromHex(0xffffff)
        }
        super.viewDidLoad()
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        self.arrayMeses = UserGlobalData.sharedInstance.userGlobal.mesesArreglo
        
        let storyboardMain = UIStoryboard(name: "Consultas", bundle: nil)
        
        EneroViewController = storyboardMain.instantiateViewController(withIdentifier: "EneroEVC") as! EneroEViewController
        EneroViewController.parentVC = self
        FebreroViewController = storyboardMain.instantiateViewController(withIdentifier: "FebreroEVC") as! FebreroEViewController
        FebreroViewController.parentVC = self
        MarzoViewController = storyboardMain.instantiateViewController(withIdentifier: "MarzoEVC") as! MarzoEViewController
        MarzoViewController.parentVC = self
        AbrilViewController = storyboardMain.instantiateViewController(withIdentifier: "AbrilEVC") as! AbrilEViewController
        AbrilViewController.parentVC = self
        MayoViewController = storyboardMain.instantiateViewController(withIdentifier: "MayoEVC") as! MayoEViewController
        MayoViewController.parentVC = self
        JunioViewController = storyboardMain.instantiateViewController(withIdentifier: "JunioEVC") as! JunioEViewController
        JunioViewController.parentVC = self
        JulioViewController = storyboardMain.instantiateViewController(withIdentifier: "JulioEVC") as! JulioEViewController
        JulioViewController.parentVC = self
        AgostoViewController = storyboardMain.instantiateViewController(withIdentifier: "AgostoEVC") as! AgostoEViewController
        AgostoViewController.parentVC = self
        SetiembreViewController = storyboardMain.instantiateViewController(withIdentifier: "SetiembreEVC") as! SetiembreEViewController
        SetiembreViewController.parentVC = self
        OctubreViewController = storyboardMain.instantiateViewController(withIdentifier: "OctubreEVC") as! OctubreEViewController
        OctubreViewController.parentVC = self
        NoviembreViewController = storyboardMain.instantiateViewController(withIdentifier: "NoviembreEVC") as! NoviembreEViewController
        NoviembreViewController.parentVC = self
        DiciembreViewController = storyboardMain.instantiateViewController(withIdentifier: "DiciembreEVC") as! DiciembreEViewController
        DiciembreViewController.parentVC = self
        for i in 0 ... arrayMeses.count - 1{
            let objMes = arrayMeses[i] as! NSDictionary
            let numMes = objMes.object(forKey: "mes") as! String
            if numMes  == "1"{
                arrayMesesController.add(EneroViewController)
            }else if numMes == "2"{
                arrayMesesController.add(FebreroViewController)
            }else if numMes == "3"{
                arrayMesesController.add(MarzoViewController)
            }else if numMes == "4"{
                arrayMesesController.add(AbrilViewController)
            }else if numMes == "5"{
                arrayMesesController.add(MayoViewController)
            }else if numMes == "6"{
                arrayMesesController.add(JunioViewController)
            }else if numMes == "7"{
                arrayMesesController.add(JulioViewController)
            }else if numMes == "8"{
                arrayMesesController.add(AgostoViewController)
            }else if numMes == "9"{
                arrayMesesController.add(SetiembreViewController)
            }else if numMes == "10"{
                arrayMesesController.add(OctubreViewController)
            }else if numMes == "11"{
                arrayMesesController.add(NoviembreViewController)
            }else if numMes == "12"{
                arrayMesesController.add(DiciembreViewController)
            }
        }
        let swiftArray = arrayMesesController as AnyObject as! [AnyObject]
        let objSwift0 = swiftArray[0]
        let objSwift1 = swiftArray[1]
        let objSwift2 = swiftArray[2]
        let objSwift3 = swiftArray[3]
        let objSwift4 = swiftArray[4]
        let objSwift5 = swiftArray[5]
        let objSwift6 = swiftArray[6]
        let objSwift7 = swiftArray[7]
        let objSwift8 = swiftArray[8]
        let objSwift9 = swiftArray[9]
        let objSwift10 = swiftArray[10]
        let objSwift11 = swiftArray[11]
        
        self.arrayMesesController = NSMutableArray()
        self.arrayMesesController.add(objSwift11)
        self.arrayMesesController.add(objSwift10)
        self.arrayMesesController.add(objSwift9)
        self.arrayMesesController.add(objSwift8)
        self.arrayMesesController.add(objSwift7)
        self.arrayMesesController.add(objSwift6)
        self.arrayMesesController.add(objSwift5)
        self.arrayMesesController.add(objSwift4)
        self.arrayMesesController.add(objSwift3)
        self.arrayMesesController.add(objSwift2)
        self.arrayMesesController.add(objSwift1)
        self.arrayMesesController.add(objSwift0)
        
        let nsArray = NSArray(array: self.arrayMesesController)
        return [nsArray[0] as! UIViewController, nsArray[1] as! UIViewController,nsArray[2] as! UIViewController,nsArray[3] as! UIViewController,nsArray[4] as! UIViewController,nsArray[5] as! UIViewController,nsArray[6] as! UIViewController,nsArray[7] as! UIViewController,nsArray[8] as! UIViewController,nsArray[9] as! UIViewController,nsArray[10] as! UIViewController,nsArray[11] as! UIViewController]
    }
    func setGradient(uiView:UIView){
        let colorBottom = UIColor(red: 239.0/255.0, green: 212.0/255.0, blue: 80.0/255.0, alpha: 1.0).cgColor
        let colorTop = UIColor(red: 209.0/255.0, green: 142.0/255.0, blue: 24.0/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.startPoint = CGPoint(x:0.0, y:0.5)
        gradientLayer.endPoint = CGPoint(x:1.0, y:0.5)
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = uiView.bounds
        uiView.layer.insertSublayer(gradientLayer, at: 0)
    }
}
