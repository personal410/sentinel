//
//  MayoEViewController.swift
//  Sentinel
//
//  Created by Daniel on 22/11/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//MayoEViewController
import XLPagerTabStrip
class MayoEViewController: ParentViewController, UITableViewDelegate, UITableViewDataSource, IndicatorInfoProvider, EnviarPDFHUDViewDelegate {
    @IBOutlet weak var montoDeudaMes: UILabel!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var deudasTV: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var alturaInfoView: NSLayoutConstraint!//176 -> 75 sin tabla //55 x celda
    @IBOutlet weak var alturaVencidosView: NSLayoutConstraint!
    
    var terceroBean:TerceroClass!
    var usuarioBean:UserClass!
    var terceroBeanMes:TerceroClass?
    var cantidadArray = NSArray()
    var vencidosArray = NSArray()
    var enviarPDF = false
    var parentVC:LoaderVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializarVariables()
    }
    override func viewWillAppear(_ animated:Bool){
        if let parentVC = parentVC {
            if parentVC.cargandoMesSeleccionado {
                let arrayMeses = UserGlobalData.sharedInstance.userGlobal.mesesArreglo
                let dicMesActual = arrayMeses.object(at: 11 - parentVC.mesDefault) as! NSDictionary
                let mes = dicMesActual.object(forKey: "mes") as! String
                if mes == "5" {
                    self.iniDetalleDeuda()
                }
            }else{
                self.iniDetalleDeuda()
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    func cargarDatos(){
        let InfoSBSMicrof : NSArray = terceroBeanMes?.SDT_MSDeudaTitular!["InfoSBSMicrof"] as! NSArray
        cantidadArray = terceroBeanMes?.SDT_MSDeudaTitular!["InfoSBSMicrof"] as! NSArray
        vencidosArray = terceroBeanMes?.SDT_MSDeudaTitular!["Vencidos"] as! NSArray
        self.montoDeudaMes.text = "\(0.00)"
        var valorASumar : Double = 0
        if cantidadArray.count != 0 {
            
            var deuda : Double = 0
            for i in 0 ... InfoSBSMicrof.count - 1{
                let obj :NSDictionary = InfoSBSMicrof[i] as! NSDictionary
                let deudaString = obj.object(forKey: "SaldoDeuda") as! String
                deuda = deuda + Double(deudaString)!
            }
            if "\(deuda)" == "0.00"{
                self.montoDeudaMes.text = "\(deuda)"
            }
            else
            {
                valorASumar = deuda
                let newDeuda: NSNumber = NSNumber.init( value: Double(deuda))
                let numberFormatter = NumberFormatter()
                numberFormatter.formatterBehavior = .behavior10_4
                numberFormatter.numberStyle = .decimal
                self.montoDeudaMes.text = numberFormatter.string(from: newDeuda)
            }
            
            if cantidadArray.count == 2 {
                self.alturaInfoView.constant = 176
            }
            else if cantidadArray.count == 1 {
                self.alturaInfoView.constant = 126
            }
            else if cantidadArray.count == 3 {
                self.alturaInfoView.constant = 226
            }
            else if cantidadArray.count == 4 {
                self.alturaInfoView.constant = 276
            }
            else if cantidadArray.count == 5 {
                self.alturaInfoView.constant = 326
            }
            else
            {
                let alturaTabla : Int = self.cantidadArray.count * 50
                self.alturaInfoView.constant = CGFloat(75 + alturaTabla + 15) // 15 del contraint de Bot
            }
        }
        else
        {
            self.alturaInfoView.constant = 75 + 15
        }
        
        if vencidosArray.count != 0 {
            var deuda : Double = 0
            for i in 0 ... vencidosArray.count - 1{
                let obj :NSDictionary = vencidosArray[i] as! NSDictionary
                let diasVenc = obj.object(forKey: "DiasVencidosVen") as! Int
                if diasVenc > 0 {
                    let deudaString = obj.object(forKey: "SaldoDeudaVen") as! String
                    deuda = deuda + Double(deudaString)!
                }
            }
            if "\(deuda)" == "0.00"{
                valorASumar = valorASumar + deuda
                self.montoDeudaMes.text = "\(deuda)"
            }
            else
            {
                valorASumar = valorASumar + deuda
                let newDeuda: NSNumber = NSNumber.init( value: Double(valorASumar))
                
                let numberFormatter = NumberFormatter()
                numberFormatter.formatterBehavior = .behavior10_4
                numberFormatter.numberStyle = .decimal
                let numberString = numberFormatter.string(from: newDeuda)
                self.montoDeudaMes.text = numberString
            }
            
            if vencidosArray.count == 2 {
                self.alturaVencidosView.constant = 176
            }
            else if vencidosArray.count == 1 {
                self.alturaVencidosView.constant = 126
            }
            else if vencidosArray.count == 3 {
                self.alturaVencidosView.constant = 226
            }
            else if vencidosArray.count == 4 {
                self.alturaVencidosView.constant = 276
            }
            else if vencidosArray.count == 5 {
                self.alturaVencidosView.constant = 326
            }
            else
            {
                let alturaTabla : Int = self.vencidosArray.count * 50
                self.alturaVencidosView.constant = CGFloat(75 + alturaTabla + 15) // 15 del contraint de Bot
            }
        }
        else
        {
            self.alturaVencidosView.constant = 75 + 15
        }
        if cantidadArray.count == 0 && vencidosArray.count == 0 {
            self.scrollView.isScrollEnabled = false
        }
        self.table.reloadData()
        self.deudasTV.reloadData()
    }
    func inicializarVariables(){
        terceroBean = UserGlobalData.sharedInstance.terceroGlobal
        usuarioBean = UserGlobalData.sharedInstance.userGlobal
        self.montoDeudaMes.text = "00.00"
    }
    
    func iniDetalleDeuda(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endDetalleDeuda(_:)), name: NSNotification.Name("endDetalleDeuda_mayo"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        
        let servicios = UserGlobalData.sharedInstance.misServiciosGlobal!
        
        let usuario : String = usuarioBean.user!
        let servicio : String = servicios.NroServicio!
        let tdocumento : String = terceroBean.tipoDocumentoTercero!
        let numDocumento : String = terceroBean.documentoTercero!
        let sesion : String = usuarioBean.SesionId!
        var fechaProceso : String = ""
        //fecha
        let meses = self.devuelveArregloUltimosMeses(fechaUltima: UserGlobalData.sharedInstance.terceroGlobal.FechaReporte)
        var anioMesActual : String = ""
        for i in 0 ... meses.count - 1 {
            let objMes : NSDictionary = meses[i] as! NSDictionary
            let anioNum : String = objMes.object(forKey: "anio") as! String
            let mesNum : String = objMes.object(forKey: "mes") as! String
            if mesNum == "5"{
                anioMesActual = anioNum
            }
        }
        let fechaReporte = UserGlobalData.sharedInstance.terceroGlobal.FechaReporte
        let arrFechaReporte = fechaReporte.split(separator: "-")
        let diaReporte = arrFechaReporte[1] == "05" ? arrFechaReporte[2] : "31"
        if anioMesActual != "" {
            fechaProceso = "\(anioMesActual)-05-\(diaReporte)"
            let parametros = [
                "Usuario" : usuario,
                "Servicio" : servicio,
                "TipoDocumento" : tdocumento,
                "NroDocumento" : numDocumento,
                "SesionId" : sesion,
                "FechaProceso" : fechaProceso,
                "origenAplicacion" : PATHS.APP_ID,
                "TipoConsulta" : "D"
                ] as [String : Any]
            OriginData.sharedInstance.mesDetalleTerceroSERV6(notification: "endDetalleDeuda_mayo", parametros: parametros)
        }
    }
    
    @objc func endDetalleDeuda(_ notification: NSNotification){
        parentVC?.cargandoMesSeleccionado = false
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as? TerceroClass
            if data?.codigoWS == "0"{
                terceroBeanMes = UserGlobalData.sharedInstance.terceroGlobal
                terceroBeanMes?.SDT_MSDeudaTitular = data?.SDT_MSDeudaTitular
                self.cargarDatos()
            }
            else
            {
                if data?.CodigoWS != nil {
                    if data?.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                        alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                        self.present(alerta, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    func iniGenerarPDF(){
        self.showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endGenerarPDF(_:)), name: NSNotification.Name("endGenerarPDF"), object: nil)
        
        let userData = UserGlobalData.sharedInstance.userGlobal!
        let tercero = UserGlobalData.sharedInstance.terceroGlobal!
        let usua : String = userData.user!
        let sesion : String = userData.SesionId!
        var numDoc : String = ""
        var tdoc : String = ""//userData.TDocusu!
        if (tercero.InfoTitular!["SDTCPTMS"] as! NSDictionary).object(forKey: "TipoDocumento") as! String == "D"{
            numDoc = (tercero.InfoTitular!["SDTCPTMS"] as! NSDictionary).object(forKey: "NroDocumento") as! String
            tdoc = "D"
        }
        else
        {
            numDoc = (tercero.InfoTitular!["SDTCPTMS"] as! NSDictionary).object(forKey: "NDocRel") as! String
            tdoc = "R"
        }
        
        let servicio : String = userData.NroSerCT!
        
        let parametros = [
            "TDocUsu" : tdoc,
            "Usuario" : usua,
            "SesionId" : sesion,
            "Servicio" : servicio,
            "TipoDocumento" : tdoc,
            "NroDocumento" : numDoc,
            "TipoReporte" : "D",
            "EnvioMail" : enviarPDF ? "S" : "N"
            ] as [String : Any]
        OriginData.sharedInstance.generarPDF(notification: "endGenerarPDF", parametros: parametros)
    }
    
    @objc func endGenerarPDF(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if let error = notification.object as? NSError {
            let message = error.code == -1001 ? "Tiempo de espera agotado." : "En estos momentos no es posible realizar la consulta."
            let alerta = UIAlertController(title: "ALERTA", message: message, preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as? UserClass
            if data?.CodigoWS == "0"{
                if enviarPDF {
                    showAlert(PATHS.SENTINEL, mensaje: "En breve le estará llegando a su correo el reporte en PDF.")
                }else{
                    showPDF(from: data?.urlPDF)
                }
            }
            else
            {
                if data?.CodigoWS != nil {
                    if data?.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: data?.MensajeRespuesta, preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    @IBAction func accionPDF(_ sender:Any){
        EnviarPDFHUDView.showEnviarPDFViewWithDelegate(self, with: "Reporte Detallado PDF")
    }
    //MARK: - EnviarPDF
    func enviarPDFHUDDidConfirmWith(option index: Int) {
        enviarPDF = index == 1
        iniGenerarPDF()
    }
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count : Int!
        
        if tableView == self.table {
            
            count = cantidadArray.count
            
        } else if tableView == self.deudasTV {
            
            count = vencidosArray.count
            
        }
        
        return count
        
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.table {
            
            let identifier = "Cell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! DetalleDeudaTableViewCell
            let array : NSArray = terceroBeanMes?.SDT_MSDeudaTitular!["InfoSBSMicrof"] as! NSArray
            let objArray : NSDictionary = array[indexPath.row] as!NSDictionary
            cell.descripcionLB.text = objArray.object(forKey: "NombreEntidad") as? String
            
            let deuda : String = objArray.object(forKey: "SaldoDeuda") as! String
            
            if "0.00" == objArray.object(forKey: "SaldoDeuda") as? String{
                cell.deudaLB.text = "\(deuda)"
            }
            else
            {
                //let deudaDouble = Double(deuda)
                let newDeuda: NSNumber = NSNumber.init( value: Double(deuda)!)
                
                let numberFormatter = Toolbox.getNumberFormmatter()
                let numberString = numberFormatter.string(from: newDeuda)
                
                cell.deudaLB.text = numberString
            }
            
            //cell.deudaLB.text = objArray.object(forKey: "SaldoDeuda") as? String
            let calificacion : Int = (objArray.object(forKey: "CalifEnt") as? Int)!
            let nombreCalificacion : String = (self.devuelveObjetoCalificacion(codigo: calificacion)["nombre"])!
            if nombreCalificacion == "CPP" || nombreCalificacion == "SCAL"{
                cell.califLB.textColor = UIColor.black
            }else{
                cell.califLB.textColor = UIColor.white
            }
            cell.califLB.text = nombreCalificacion
            let coloCalificacion : String = (self.devuelveObjetoCalificacion(codigo: calificacion)["color"])!
            let splitColor = coloCalificacion.split(separator: ",")
            let red :Double = Double(splitColor[0])!
            let green :Double = Double(splitColor[1])!
            let blue :Double = Double(splitColor[2])!
            
            cell.califLB.backgroundColor = UIColor.init(red: CGFloat(red/255.0), green: CGFloat(green/255.0), blue: CGFloat(blue/255.0), alpha: 1.0)
            let diasVencidos : Int64 = objArray.object(forKey: "DiasVencidos") as! Int64
            cell.diasVencidos.text = "\(diasVencidos)"
            let fecha : String = (objArray.object(forKey: "FechaReporte") as? String)!
            cell.fechaInf.text = self.devuelveFechaCorta(fecha: fecha)
            return cell
            
        }
        if tableView == self.deudasTV {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellD", for: indexPath) as! deudasTableViewCell
            let objArray : NSDictionary = vencidosArray[indexPath.row] as!NSDictionary
            let NombreEntidadVen: String = (objArray.object(forKey: "NombreEntidadVen") as? String)!
            cell.descripcionLB.text = NombreEntidadVen
            let DiasVencidosVen: Int = (objArray.object(forKey: "DiasVencidosVen") as? Int)!
            if DiasVencidosVen == 0 {
                cell.montoLB.text = ""
                cell.diasVencidos.text = ""
            }else{
                let SaldoDeudaVen: String = (objArray.object(forKey: "SaldoDeudaVen") as? String)!
                if "0.00" == objArray.object(forKey: "SaldoDeuda") as? String{
                    cell.montoLB.text = "\(SaldoDeudaVen)"
                }else{
                    let newDeuda: NSNumber = NSNumber(value: Double(SaldoDeudaVen)!)
                    cell.montoLB.text = Toolbox.getNumberFormmatter().string(from: newDeuda)
                }
                cell.diasVencidos.text = "\(DiasVencidosVen)"
                cell.montoLB.textColor = UIColor.black
                var colorMonto:UIColor!
                if DiasVencidosVen == 0 {
                    colorMonto = UIColor.clear
                }else if DiasVencidosVen <= 30 {
                    colorMonto = UIColor(red: 240.0 / 255.0, green: 201.0 / 255.0, blue: 21.0 / 2550.0, alpha: 1.0)
                }else{
                    cell.montoLB.textColor = UIColor.white
                    colorMonto = UIColor(red: 236.0 / 255.0, green: 42.0 / 255.0, blue: 63.0 / 2550.0, alpha: 1.0)
                }
                cell.montoLB.backgroundColor = colorMonto
            }
            return cell
        }
        
        let celda = UITableViewCell()
        return celda
        
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        
        var enero : NSDictionary = NSDictionary()
        let array : NSMutableArray = self.devuelveArregloUltimosMeses(fechaUltima: UserGlobalData.sharedInstance.terceroGlobal.FechaReporte)
        for i in 0 ... array.count - 1 {
            let objMes : NSDictionary = array[i] as! NSDictionary
            let numMes : String = objMes.object(forKey: "mes") as! String
            if numMes == "5"{
                enero = objMes
            }
        }
        let nombre : String = enero.object(forKey: "mes") as! String
        let anio : String = enero.object(forKey: "anio") as! String
        
        let nombreMes : String = self.devuelveNombreMesParticionado(mes: nombre)
        
        let titulo : String = "\(nombreMes).\(anio)"
        return IndicatorInfo(title: titulo, image: UIImage(named: ""))
        
    }
    
    
}
