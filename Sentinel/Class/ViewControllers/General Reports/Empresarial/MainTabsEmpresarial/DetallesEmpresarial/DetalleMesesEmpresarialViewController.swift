//
//  DetalleMesesEmpresarialViewController.swift
//  Sentinel
//
//  Created by Daniel on 22/11/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//DetalleMesesEmpresarialViewController
import UIKit
class DetalleMesesEmpresarialViewController:ParentViewController, EnviarPDFHUDViewDelegate {
    //MARK: - Outlets
    @IBOutlet weak var toolbarDetalle:UIToolbar!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblDocument:UILabel!
    //MARK: - Attributes
    let tercero = UserGlobalData.sharedInstance.terceroGlobal!
    var enviarPDF = false
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        let dic = tercero.InfoTitular!["SDTCPTMS"] as! NSDictionary
        let tipoDoc = dic.object(forKey: "TipoDocumento") as! String
        let nroDoc =  dic.object(forKey: "NroDocumento") as! String
        var nombreCompleto = ""
        var documento = ""
        if tipoDoc == "D" {
            let nombre = dic.object(forKey: "Nombre") as! String
            let apePat = dic.object(forKey: "ApePaterno") as! String
            let apeMat = dic.object(forKey: "ApeMaterno") as! String
            nombreCompleto = "\(apePat) \(apeMat) \(nombre)"
            documento = "DNI: \(nroDoc)"
        }else if tipoDoc == "R" {
            nombreCompleto = dic.object(forKey: "NomRazSoc") as! String
            documento = "RUC: \(nroDoc)"
        }
        lblName.text = nombreCompleto
        lblDocument.text = documento
    }
    func iniGenerarPDF(){
        showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endGenerarPDF(_:)), name: NSNotification.Name("endGenerarPDF"), object: nil)
        let userData = UserGlobalData.sharedInstance.userGlobal!
        let usua = userData.user!
        let sesion = userData.SesionId!
        var numDoc = ""
        var tdoc = ""
        if (tercero.InfoTitular!["SDTCPTMS"] as! NSDictionary).object(forKey: "TipoDocumento") as! String == "D"{
            numDoc = (tercero.InfoTitular!["SDTCPTMS"] as! NSDictionary).object(forKey: "NroDocumento") as! String
            tdoc = "D"
        }else{
            numDoc = (tercero.InfoTitular!["SDTCPTMS"] as! NSDictionary).object(forKey: "NDocRel") as! String
            tdoc = "R"
        }
        let parametros = [
            "TDocUsu" : tdoc,
            "Usuario" : usua,
            "SesionId" : sesion,
            "Servicio" : userData.NroSerCT!,
            "TipoDocumento" : tdoc,
            "NroDocumento" : numDoc,
            "TipoReporte" : "D",
            "EnvioMail" : enviarPDF ? "S" : "N"
            ] as [String : Any]
        OriginData.sharedInstance.generarPDF(notification: "endGenerarPDF", parametros: parametros)
    }
    
    @objc func endGenerarPDF(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if let error = notification.object as? NSError {
            let message = error.code == -1001 ? "Tiempo de espera agotado." : "En estos momentos no es posible realizar la consulta."
            let alerta = UIAlertController(title: "ALERTA", message: message, preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as? UserClass
            if data?.CodigoWS == "0"{
                if enviarPDF {
                    self.showAlert(PATHS.SENTINEL, mensaje: "En breve le estará llegando a su correo el reporte en PDF.")
                }else{
                    showPDF(from: data?.urlPDF)
                }
            }
            else
            {
                if data?.CodigoWS != nil {
                    if data?.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: data?.MensajeRespuesta, preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    @IBAction func accionPDF(_ sender: Any) {
        EnviarPDFHUDView.showEnviarPDFViewWithDelegate(self, with: "Reporte Empresarial PDF")
    }
    @IBAction func accionAtras(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    //MARK: - EnviarPDF
    func enviarPDFHUDDidConfirmWith(option index: Int){
        enviarPDF = index == 1
        iniGenerarPDF()
    }
}
