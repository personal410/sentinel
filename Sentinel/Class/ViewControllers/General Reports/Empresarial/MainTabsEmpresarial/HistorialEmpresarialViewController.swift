//
//  HistorialEmpresarialViewController.swift
//  Sentinel
//
//  Created by Daniel Montoya on 1/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//HistorialEmpresarialViewController

import UIKit
import XLPagerTabStrip
import Alamofire

class HistorialEmpresarialViewController:  ParentViewController, IndicatorInfoProvider, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var ListaTV: UITableView!
    
    @IBOutlet weak var mensajeHistorial: UILabel!
    
    var dataArr = NSArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ListaTV.rowHeight = 90.0
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.iniHistorial()
    }
    
    func iniHistorial(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endHistorial(_:)), name: NSNotification.Name("endHistorial"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        let userData = UserGlobalData.sharedInstance.userGlobal
        
        let tipoDoc: String = (userData?.TDocusu)!
        let usuario: String = (userData?.user)!
        let sesion : String = (userData?.SesionId)!
        
        let servicioTipo : String = UserGlobalData.sharedInstance.misServiciosGlobal.NroServicio!
        
        let parametros = [
            "AFSerCod" : servicioTipo,
            "AFUsuTDoc" : tipoDoc,
            "AFUsuNroDoc" : usuario,
            "AFSerCptTDoc" : usuario,
            "UseSeIDSession" : sesion
            ] as [String : Any]
        OriginData.sharedInstance.getHistorialEmpresasSERV6(notification: "endHistorial", parametros: parametros)
    }
    
    @objc func endHistorial(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as? UserClass
            if data?.codigoWs == "0"{
                if data?.Cont == 0{
                    self.mensajeHistorial.isHidden = false
                    self.ListaTV.isHidden = true
                }
                else
                {
                    self.mensajeHistorial.isHidden = true
                    self.ListaTV.isHidden = false
                    self.dataArr = (data?.SDTConRap)!
                    self.ListaTV.reloadData()
                }
                
            }
            else
            {
                if data?.codigoWs != nil {
                    if data?.codigoWs == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: data?.MensajeRespuesta, preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                        //iniMensajeError(codigo: (data?.CodigoWS!)!)
                    }
                }
            }
        }
    }
    
    func iniBusquedaDocumento(objeto : NSDictionary){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endBusquedaDocumento(_:)), name: NSNotification.Name("endBusquedaDocumento"), object: nil)
        let userData = UserGlobalData.sharedInstance.userGlobal!
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)

        var tipoDoc : String = objeto.object(forKey: "AFSerCptTDoc") as! String

        let nroDoc : String = objeto.object(forKey: "AFSerCptNroDoc") as! String
        let sesion : String = userData.SesionId!

        let servicio = UserGlobalData.sharedInstance.misServiciosGlobal!
        var  checkCPT = "NO"
        var checkHisto = "NO"
        if checkCPT == "SI" || checkHisto == "SI"{
        }
        else
        {
            let parametros = [
                "Usuario" : userData.user as! String,
                "Servicio" : servicio.NroServicio!,
                "TipoDocumento" :  tipoDoc,
                "NroDocumento" : nroDoc,
                "SesionId" : sesion,
                "origenAplicacion" : PATHS.APP_ID,
                "TipoConsulta" : "D",
                ] as [String : Any]
            OriginData.sharedInstance.miReporteDNITerceroSERV6(notification: "endBusquedaDocumento", parametros: parametros)
        }

    }

    @objc func endBusquedaDocumento(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as! TerceroClass
            if data.codigoWS == "0"{

                UserGlobalData.sharedInstance.terceroGlobal.InfoTitular = data.InfoTitular
                let consultado: String = data.InfoTitular!["URLFoto"] as! String
                let url : URL = NSURL(string: consultado)! as URL

                self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
                Alamofire.request(url).responseImage { response in
                    debugPrint(response)
                    switch response.result {
                    case .success:
                        if let image = response.result.value {
                            _ = self.saveImageTercero(image: image)
                            self.hideActivityIndicator()
                            let nav = UIStoryboard.init(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "ReporteEmpresarialVC") as! ReporteEmpresarialViewController
                            self.navigationController?.pushViewController(nav, animated: true)
                        }
                        break

                    case .failure(_):
                        //CNIGenImaHom
                        let image : UIImage = UIImage.init(named: "CNIGenImaHom")!
                        let ok = self.saveImageTercero(image: image)
                        self.hideActivityIndicator()
                        let nav = UIStoryboard.init(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "ReporteEmpresarialVC") as! ReporteEmpresarialViewController
                        self.navigationController?.pushViewController(nav, animated: true)
                        break
                    }
                }
            }
            else
            {
                if data.codigoWS != nil {
                    if data.codigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        iniMensajeError(codigo: (data.codigoWS!))
                    }
                }
                else
                {
                    let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                    alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                    self.present(alerta, animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc func selectedCell(sender: UIButton)  {
        let objArray: NSDictionary = dataArr[sender.tag] as! NSDictionary
        self.iniBusquedaDocumento(objeto: objArray)
        
        
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArr.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! HistorialEmpresarialTableViewCell
        //cell.nombreLB.text = dataArr[indexPath.row]
        let objArray: NSDictionary = dataArr[indexPath.row] as! NSDictionary
        let nombre : String = objArray.object(forKey: "AFSerCPTNomRaz") as! String
        cell.nombreLB.text = nombre
        let fecha = objArray.object(forKey: "CnpFchReg") as! String
        cell.fechaLB.text = self.devuelveFecha(fecha: fecha)
        let tipo = objArray.object(forKey: "AFSerCptTDoc") as! String
        cell.historialBT.tag = indexPath.row
        cell.historialBT.addTarget(self, action: #selector(self.selectedCell(sender:)), for: .touchUpInside)
        cell.tipoDetalleLB.text = ""
//        if tipo == "D"{
//            cell.tipoDetalleLB.text = "Reporte Detallado"
//            cell.tipoDetalleLB.textColor = UIColor.fromHex(0x06B150)
//        }
//        else if tipo == "R"{
//            cell.tipoDetalleLB.text = "Reporte Resumido"
//            cell.tipoDetalleLB.textColor = UIColor.fromHex(0x0072BE)
//        }
        let semaforo : String = objArray.object(forKey: "CnpSemAct") as! String
        cell.semaforoIMG.image = self.devuelveSemaforo(SemAct: semaforo)
        
        return cell
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "", image: UIImage(named: "historialS.on"))
    }
    
}
