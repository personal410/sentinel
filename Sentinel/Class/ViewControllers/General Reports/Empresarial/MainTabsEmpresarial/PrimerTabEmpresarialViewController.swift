//
//  PrimerTabEmpresarialViewController.swift
//  Sentinel
//
//  Created by Daniel Montoya on 1/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import XLPagerTabStrip
class PrimerTabEmpresarialViewController:ParentViewController, IndicatorInfoProvider {
    //MARK: - Outlets
    @IBOutlet weak var fechaNacimientoLB: UILabel!
    @IBOutlet weak var nroDocumento: UILabel!
    @IBOutlet weak var notaLB: UILabel!
    @IBOutlet weak var nombreLB: UILabel!
    @IBOutlet weak var creditosCL: UICollectionView!
    @IBOutlet weak var semaforoCL: UICollectionView!
    @IBOutlet weak var imaSemIMG: UIImageView!
    @IBOutlet weak var deudaLB: UILabel!
    @IBOutlet weak var sinCreditos: UILabel!
    @IBOutlet weak var sabioBT: UIButton!
    @IBOutlet weak var webBT: UIButton!
    @IBOutlet weak var fotoIMG: UIImageView!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var signoDeudaLB: UILabel!
    @IBOutlet weak var vistaWebBT: UIButton!
    @IBOutlet weak var topFechaNac: NSLayoutConstraint!
    @IBOutlet weak var alturaFechaNac: NSLayoutConstraint!
    //MARK: - Properties
    var userData:UserClass!
    var tercero:TerceroClass!
    var esPremium = ""
    var creditosArray = NSArray()
    var imgArray = [UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris")]
    //MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializar()
        self.estilos()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.cargarDatos()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.semaforoCL.scrollToItem(at: IndexPath(item: 11, section: 0), at: .right, animated: false)
    }
    //MARK: - Methods
    func cargarDatos(){
        if UserGlobalData.sharedInstance.misServiciosGlobal.FlgSabio == "F"{
            self.sabioBT.isHidden = false
            self.webBT.isHidden = false
            self.vistaWebBT.isHidden = true
        }else if UserGlobalData.sharedInstance.misServiciosGlobal.FlgSabio == "C"{
            self.sabioBT.isHidden = false
            self.webBT.isHidden = false
            self.vistaWebBT.isHidden = true
        }else{
            self.sabioBT.isHidden = true
            self.webBT.isHidden = true
            self.vistaWebBT.isHidden = false
        }
        
        let InfoTitular = tercero.InfoTitular! as NSDictionary
        let SDTCPTMS = InfoTitular.object(forKey: "SDTCPTMS") as! NSDictionary
        let tipoDoc = SDTCPTMS.object(forKey: "TipoDocumento") as! String
        if tipoDoc == "D" || tipoDoc == "5" {
            if let imageMain = getSavedImage(named: "fileNameTercero.png") {
                self.fotoIMG.image = imageMain
            }
            //Nombre Completo
            let nombre = SDTCPTMS.object(forKey: "Nombre") as! String
            let apePat = SDTCPTMS.object(forKey: "ApePaterno") as! String
            let apeMat = SDTCPTMS.object(forKey: "ApeMaterno") as! String
            let nombreCompleto : String = "\(apePat) \(apeMat)\n\(nombre)"
            nombreLB.text = nombreCompleto
            
            //FechaProceso
            UserGlobalData.sharedInstance.terceroGlobal.FechaReporte = SDTCPTMS.object(forKey: "FechaProceso") as! String
            //Fecha
            let fechaSinFormat = SDTCPTMS.object(forKey: "FechaNacimiento") as! String
            if fechaSinFormat.isEmpty || fechaSinFormat == "0000-00-00" {
                self.fechaNacimientoLB.text = ""
                self.fechaNacimientoLB.isHidden = true
                self.topFechaNac.constant = 0
                self.alturaFechaNac.constant = 0
            }else{
                self.fechaNacimientoLB.text = "Fecha Nac. \(fechaSinFormat)"
            }
            
            //Documento
            let dni =  SDTCPTMS.object(forKey: "NroDocumento") as! String
            UserGlobalData.sharedInstance.terceroGlobal.documentoTercero = dni
            if dni.count == 8 {
                self.nroDocumento.text = "DNI: \(dni)"
                UserGlobalData.sharedInstance.terceroGlobal.tipoDocumentoTercero = "D"
            }else if dni.count == 11 {
                self.nroDocumento.text = "RUC: \(dni)"
                UserGlobalData.sharedInstance.terceroGlobal.tipoDocumentoTercero = "R"
            }
            
            //Semaforo
            let semaforoActivo = SDTCPTMS.object(forKey: "SemActual") as! String
            self.imaSemIMG.image = self.devuelveSemaforo(SemAct: semaforoActivo)
            self.cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: self.deudaLB)
            self.cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: self.signoDeudaLB)
            
            //Deuda
            let deuda : String = SDTCPTMS.object(forKey: "DeudaTotal") as! String
            if deuda == "0.00"{
                self.deudaLB.text = deuda
            }else{
                let newDeuda: NSNumber = NSNumber.init( value: Double(deuda)!)
                let numberFormatter = NumberFormatter()
                numberFormatter.formatterBehavior = .behavior10_4
                numberFormatter.numberStyle = .decimal
                let numberString = numberFormatter.string(from: newDeuda)
                self.deudaLB.text = numberString
            }
            
            let indicador : [NSDictionary] = tercero.InfoTitular!["SDTIndicadoresCPT"] as! [NSDictionary]
            self.creditosArray = indicador as NSArray
            if self.creditosArray.count == 0 {
                self.sinCreditos.isHidden = false
            }
            self.creditosCL.reloadData()
            
            let sema : String = SDTCPTMS.object(forKey: "Semaforos") as! String
            if (sema != "" && sema.count == 12){
                imgArray = devuelveArrayIMGSemaforos(semaforos: sema) as! [UIImage?]
                self.semaforoCL.reloadData()
            }
            else
            {
                let semaDefault : String = "GGGGGGGGGGGG"
                imgArray = devuelveArrayIMGSemaforos(semaforos: semaDefault) as! [UIImage?]
                self.semaforoCL.reloadData()
            }
           
        }else if tipoDoc == "R"{
            //Cargar imagen
            fotoIMG.image = UIImage.init(named: "IconoEmpresaF")
            
            //Nombre Completo
            let nombreCompleto = SDTCPTMS.object(forKey: "NomRazSoc") as! String
            self.nombreLB.text = nombreCompleto
            
            //Fecha
            let fechaSinFormat = SDTCPTMS.object(forKey: "FechaNacimiento") as! String
            let fecha = "Fecha Nac. \(self.devuelveFecha(fecha: fechaSinFormat))"
            self.fechaNacimientoLB.text = fecha
            self.fechaNacimientoLB.isHidden = true
            self.topFechaNac.constant = 0
            self.alturaFechaNac.constant = 0
            
            //FechaProceso
            UserGlobalData.sharedInstance.terceroGlobal.FechaReporte = SDTCPTMS.object(forKey: "FechaProceso") as! String
            
            //DNI
            let dni =  SDTCPTMS.object(forKey: "NroDocumento") as! String
            UserGlobalData.sharedInstance.terceroGlobal.documentoTercero = dni
            
            if dni.count == 8 {
                self.nroDocumento.text = "DNI: \(dni)"
                UserGlobalData.sharedInstance.terceroGlobal.tipoDocumentoTercero = "D"
            }else if dni.count == 11 {
                self.nroDocumento.text = "RUC: \(dni)"
                UserGlobalData.sharedInstance.terceroGlobal.tipoDocumentoTercero = "R"
            }
            
            //Semaforo
            let semaforoActivo = SDTCPTMS.object(forKey: "SemActual") as! String
            self.imaSemIMG.image = self.devuelveSemaforo(SemAct: semaforoActivo)
            self.cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: self.deudaLB)
            self.cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: self.signoDeudaLB)
            
            //Deuda
            let deuda : String = SDTCPTMS.object(forKey: "DeudaTotal") as! String
            if deuda == "0.00"{
                self.deudaLB.text = deuda
            }else{
                let newDeuda: NSNumber = NSNumber.init( value: Double(deuda)!)
                let numberFormatter = NumberFormatter()
                numberFormatter.formatterBehavior = .behavior10_4
                numberFormatter.numberStyle = .decimal
                let numberString = numberFormatter.string(from: newDeuda)
                self.deudaLB.text = numberString
            }
            
            //indicadores
            let indicador : [NSDictionary] = tercero.InfoTitular!["SDTIndicadoresCPT"] as! [NSDictionary]
            self.creditosArray = indicador as NSArray
            if self.creditosArray.count == 0 {
                self.sinCreditos.isHidden = false
            }
            self.creditosCL.reloadData()
            
            //semaforo últimos meses
            let sema = SDTCPTMS.object(forKey: "Semaforos") as! String
            if(sema != "" && sema.count == 12){
                imgArray = devuelveArrayIMGSemaforos(semaforos: sema) as! [UIImage?]
                self.semaforoCL.reloadData()
            }else{
                let semaDefault = "GGGGGGGGGGGG"
                imgArray = devuelveArrayIMGSemaforos(semaforos: semaDefault) as! [UIImage?]
                self.semaforoCL.reloadData()
            }
        }
    }
    
    func inicializar(){
        self.userData = UserGlobalData.sharedInstance.userGlobal!
        self.tercero = UserGlobalData.sharedInstance.terceroGlobal!
    }
    
    func estilos(){
        viewImage.layer.cornerRadius = viewImage.frame.size.width / 2
        viewImage.clipsToBounds = true
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "REPORTE", image: UIImage(named: ""))
    }
    @IBAction func accionDabio(_ sender: Any) {
        //PRUEBA
        let servicios = UserGlobalData.sharedInstance.misServiciosGlobal!
        if servicios.FlgSabio == "F"{
            let nav = UIStoryboard.init(name: "SabioFinanciero", bundle: nil).instantiateViewController(withIdentifier: "SFclienteConyugeVC") as! SFclienteConyugeViewController
            UserGlobalData.sharedInstance.userGlobal.vieneDelUltimoSF = "NO"
            self.navigationController?.pushViewController(nav, animated: true)
        }else if servicios.FlgSabio == "C"{
            let navComercial = UIStoryboard(name: "StoryboardSabioComercial", bundle: nil).instantiateViewController(withIdentifier: "NuevaBusquedaVC")
            self.navigationController?.pushViewController(navComercial, animated: true)
        }
    }
    @IBAction func accionWeb(_ sender: Any) {
        accionVistaWeb(sender)
    }
    @IBAction func accionVistaWeb(_ sender: Any){
        let alerta = UIAlertController(title: PATHS.SENTINEL, message: "Será redireccionado a la Web.", preferredStyle: UIAlertControllerStyle.alert)
        alerta.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action: UIAlertAction!) in
            self.showActivityIndicator()
            let serviceUrl = "https://www.sentinelconsultagratuita.com/qawsapp/rest/RWS_MSObtieneLinkEmp"
            let InfoTitular = self.tercero.InfoTitular! as NSDictionary
            let SDTCPTMS = InfoTitular.object(forKey: "SDTCPTMS") as! NSDictionary
            let tipoDoc = SDTCPTMS.object(forKey: "TipoDocumento") as! String
            let dni =  SDTCPTMS.object(forKey: "NroDocumento") as! String
            let nroServicio = UserGlobalData.sharedInstance.misServiciosGlobal.NroServicio ?? ""
            let dicParams = ["Usuario": self.userData!.user!, "SesionId": self.userData!.SesionId!, "Servicio": nroServicio, "TipoConsulta": "CR", "TDocCPT": tipoDoc, "NDocCPT": dni]
            ServiceConnector.connectToUrl(serviceUrl, params: dicParams){(result, error) in
                self.hideActivityIndicator()
                if let dicResult = result as? [String: String], let codigoWS = dicResult["CodigoWS"], let url = dicResult["URL"] {
                    if codigoWS == "0" {
                        if !url.isEmpty {
                            UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
                            return
                        }
                    }else if codigoWS == "99" {
                        self.showError99()
                        return
                    }else{
                        self.iniMensajeError(codigo: codigoWS)
                        return
                    }
                }
                let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                self.present(alerta, animated: true, completion: nil)
            }
        }))
        alerta.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        present(alerta, animated: true, completion:nil)
    }
}

extension PrimerTabEmpresarialViewController:UICollectionViewDataSource , UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.semaforoCL {
            return imgArray.count
        }
        else if collectionView == self.creditosCL{
            return creditosArray.count
        }
        return imgArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.semaforoCL {
            let identifier = "CellEmpresa"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! imageCollectionViewCell
            cell.imgIV.image = imgArray[indexPath.row]

            let arregloMesDesc = self.devuelveArregloUltimosMeses(fechaUltima: UserGlobalData.sharedInstance.terceroGlobal.FechaReporte)
            var swiftArray = arregloMesDesc as AnyObject as! [AnyObject]
            let objSwift0 = swiftArray[0]
            let objSwift1 = swiftArray[1]
            let objSwift2 = swiftArray[2]
            let objSwift3 = swiftArray[3]
            let objSwift4 = swiftArray[4]
            let objSwift5 = swiftArray[5]
            let objSwift6 = swiftArray[6]
            let objSwift7 = swiftArray[7]
            let objSwift8 = swiftArray[8]
            let objSwift9 = swiftArray[9]
            let objSwift10 = swiftArray[10]
            let objSwift11 = swiftArray[11]
            
            let arrayMesesController = NSMutableArray()
            arrayMesesController.add(objSwift11)
            arrayMesesController.add(objSwift10)
            arrayMesesController.add(objSwift9)
            arrayMesesController.add(objSwift8)
            arrayMesesController.add(objSwift7)
            arrayMesesController.add(objSwift6)
            arrayMesesController.add(objSwift5)
            arrayMesesController.add(objSwift4)
            arrayMesesController.add(objSwift3)
            arrayMesesController.add(objSwift2)
            arrayMesesController.add(objSwift1)
            arrayMesesController.add(objSwift0)
            
            let objMes : NSDictionary = arrayMesesController[indexPath.row] as! NSDictionary
            let nombre : String = objMes.object(forKey: "mes") as! String
            let anio : String = objMes.object(forKey: "anio") as! String
            let nombreMes : String = self.devuelveNombreMesParticionado(mes: nombre)
            cell.mesLB.text = "\(nombreMes)\n\(anio)"
            
            return cell
        }
        else if collectionView == self.creditosCL {
            
            let identifier = "CreditosCVC"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! CreditosCollectionViewCell
            
            let objIndicador : NSDictionary = creditosArray[indexPath.row] as! NSDictionary
            let codigo : String = "\((objIndicador.object(forKey: "CICodInd"))!)"
            
            cell.aimagenCreditos.image =  self.devuelveCreditosIndicadores(cred: codigo)
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.semaforoCL {
            return 5
        }
        else if collectionView == self.creditosCL{
            return 5
        }
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var widthItem  = 30
        var heighItem = 30
        
        if collectionView == self.semaforoCL {
            widthItem = 45
            heighItem = 60
        }
        else if collectionView == self.creditosCL{
            widthItem  = 30
            heighItem = 40
        }
        
        return CGSize(width: widthItem, height: heighItem)
    }
    
    func indexPathForPreferredFocusedView(in collectionView: UICollectionView) -> IndexPath? {
        if collectionView == self.semaforoCL {
            let indexP = IndexPath(item: (self.imgArray.count - 1), section: 0)
            return indexP
        }else if collectionView == self.creditosCL{
            let indexP = IndexPath(item: (self.creditosArray.count - 1), section: 0)
            return indexP
        }
        
        return IndexPath()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.semaforoCL {
            UserGlobalData.sharedInstance.userGlobal.mesesArreglo = devuelveArregloUltimosMeses(fechaUltima: UserGlobalData.sharedInstance.terceroGlobal.FechaReporte)
            UserDefaults.standard.set(indexPath.row, forKey: "mesSemaforo")
            let nav = storyboard!.instantiateViewController(withIdentifier: "DetalleMesesEmpresarialVC")
            self.navigationController?.pushViewController(nav, animated: true)
        }else if collectionView == self.creditosCL {
            if let parentVC = self.parent as? XLReportesEmprearialViewController {
                parentVC.moveToViewController(at: 1)
            }
        }
    }
}
