//
//  ReporteEmpresarialViewController.swift
//  Sentinel
//
//  Created by Daniel Montoya on 1/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class ReporteEmpresarialViewController: ParentViewController {
    @IBOutlet weak var barVW:UIToolbar!
    var tipoCheck : String = ""
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.estilos()
    }
    func estilos(){
        self.barVW.clipsToBounds = true
    }
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
