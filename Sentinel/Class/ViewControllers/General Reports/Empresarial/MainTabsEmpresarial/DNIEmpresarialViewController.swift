//
//  DNIEmpresarialViewController.swift
//  Sentinel
//
//  Created by Daniel Montoya on 1/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//DNIEmpresarialViewController

import UIKit
import Alamofire
import AlamofireImage

class DNIEmpresarialViewController: ParentViewController,MiPickerComboDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var consultaDniToolbar: UIToolbar!
    @IBOutlet weak var comboDocumento: CTComboPicker!
    @IBOutlet weak var numeroLB: UITextField!
    @IBOutlet weak var disponiblesLB: UILabel!
    @IBOutlet weak var nombreDisponibleLB: UILabel!
    
    var checkHisto : String = "NO"
    var checkCPT : String = "NO"
    
    var validaBusquedaCPT : Bool = false
    
    var tercero : TerceroClass?
    var userData : UserClass?
    
    var tipoDocumento : String  = ""
    
    var codCombo = "1"
    var desCombo = "Automático"
    var combosArray: NSMutableArray = NSMutableArray()
    var combosIDArray: NSMutableArray = NSMutableArray()
    
    //variables para cpt externo
    var tipoDoc : String = ""
    var tipocheck : String = ""
    
    let grayBorderColor = UIColor.fromHex(rgbValue: 0xE6E6E6).cgColor
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializarVariables()
        self.gestureScreen()
    }
    override func viewWillAppear(_ animated:Bool){
        self.cargarDatos()
    }
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func inicializarVariables(){
        self.consultaDniToolbar.clipsToBounds = true
        
        self.comboDocumento.delegate = self
        self.numeroLB.delegate = self
        
        self.combosArray.add("Automático")
        self.combosArray.add("DNI")
        self.combosArray.add("RUC")
        self.combosArray.add("Carnet de extranjería")
        self.combosArray.add("Pasaporte")
        
        self.combosIDArray.add("1")
        self.combosIDArray.add("2")
        self.combosIDArray.add("3")
        self.combosIDArray.add("4")
        self.combosIDArray.add("5")
    }
    
    func cargarDatos(){
        self.tercero = UserGlobalData.sharedInstance.terceroGlobal
        self.userData = UserGlobalData.sharedInstance.userGlobal
        let disponbles : String = UserGlobalData.sharedInstance.userGlobal.AFSerCPTNDisp
        if disponbles == ""{
            self.disponiblesLB.text = "0"
        }else{
            let colorD : Int = Int(disponbles)!
            if colorD < 0 {
                self.nombreDisponibleLB.text = "Sobregirado"
                self.nombreDisponibleLB.textColor = UIColor.red
                self.disponiblesLB.textColor = UIColor.red
                self.disponiblesLB.text = disponbles
            }
            else
            {
                self.nombreDisponibleLB.text = "Disponibles"
                self.nombreDisponibleLB.textColor = UIColor.white
                self.disponiblesLB.textColor = UIColor.white
                self.disponiblesLB.text = disponbles
            }
        }
    }
    
    
    func iniTitulares(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endTitulares(_:)), name: NSNotification.Name("endTitulares"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        let userData = UserGlobalData.sharedInstance.userGlobal
        
        var tipoDoc: String = ""
        if codCombo == "1" {
            if numeroLB.text?.count == 8 {
                tipoDoc = "D"
            }
            else
            {
                tipoDoc = "R"
            }
        }
        else if codCombo == "2" {
            tipoDoc = "D"
        }
        else if codCombo == "3" {
            tipoDoc = "R"
        }
        else if codCombo == "4" {
            tipoDoc = "4"
        }
        else if codCombo == "5" {
            tipoDoc = "5"
        }
        
        self.tipoDocumento = tipoDoc
        
        let numero : String = self.numeroLB.text!
        
        let sesion : String = (userData?.SesionId!)!
        let usuario : String = (userData?.user)!
        
        let parametros = [
            "Usuario" : usuario,
            "TDoc" :   tipoDoc,
            "NDoc" : numero,
            "SesionId" : sesion
            ] as [String : Any]
        OriginData.sharedInstance.nombreTitular(notification: "endTitulares", parametros: parametros)
    }
    
    @objc func endTitulares(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data : TerceroClass = (notification.object as? TerceroClass)!
            if data.CodigoWS == "0"{
                
                if tipoDocumento == "D"{
                    let nombre : String = "\(data.Nombres!) \(data.ApePat!) \(data.ApeMat!)"
                    let alerta = UIAlertController(title: PATHS.SENTINEL, message: "¿Desea consultar a \(nombre) con DNI: \(self.numeroLB.text!)?", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alerta.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler: nil))
                        
                    alerta.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
                        UserGlobalData.sharedInstance.terceroGlobal.ApeMat = data.ApeMat!
                        UserGlobalData.sharedInstance.terceroGlobal.ApePat =
                            data.ApePat!
                        UserGlobalData.sharedInstance.terceroGlobal.Nombres = data.Nombres!
                        UserGlobalData.sharedInstance.terceroGlobal.tipoDocumentoTercero = self.tipoDocumento
                        UserGlobalData.sharedInstance.terceroGlobal.documentoTercero = self.numeroLB.text!

                        self.iniBusquedaDocumento()
                        }))
                    
                        self.present(alerta, animated: true, completion: nil)
                
                    
                }
                else if tipoDocumento == "R"{
                    
                    let nombre : String = "\(data.NombreRazSoc)"
                    let alerta = UIAlertController(title: PATHS.SENTINEL, message: "¿Desea consultar a \(nombre) con RUC: \(self.numeroLB.text!)?", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alerta.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler: nil))
                    
                    alerta.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
                        UserGlobalData.sharedInstance.terceroGlobal.NombreRazSoc = data.NombreRazSoc!
                        UserGlobalData.sharedInstance.terceroGlobal.tipoDocumentoTercero = self.tipoDocumento
                        UserGlobalData.sharedInstance.terceroGlobal.documentoTercero = self.numeroLB.text!
//                        let nav = UIStoryboard.init(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "ReporteEmpresarialVC") as! ReporteEmpresarialViewController
//                        self.navigationController?.pushViewController(nav, animated: true)
                        self.iniBusquedaDocumento()
                    }))
                    
                    self.present(alerta, animated: true, completion: nil)
                }
                else
                {
                    let nombre : String = "\(data.Nombres) \(data.ApePat) \(data.ApeMat)"
                    let alerta = UIAlertController(title: PATHS.SENTINEL, message: "¿Desea consultar a \(nombre) con Documento: \(self.numeroLB.text!)?", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alerta.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler: nil))
                    
                    alerta.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
                        UserGlobalData.sharedInstance.terceroGlobal.ApeMat = data.ApeMat!
                        UserGlobalData.sharedInstance.terceroGlobal.ApePat =
                            data.ApePat!
                        UserGlobalData.sharedInstance.terceroGlobal.Nombres = data.Nombres!
                        UserGlobalData.sharedInstance.terceroGlobal.NombreRazSoc = data.NombreRazSoc!
                        UserGlobalData.sharedInstance.terceroGlobal.tipoDocumentoTercero = self.tipoDocumento
                        UserGlobalData.sharedInstance.terceroGlobal.documentoTercero = self.numeroLB.text!
//                        let nav = UIStoryboard.init(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "ReporteEmpresarialVC") as! ReporteEmpresarialViewController
//                        self.navigationController?.pushViewController(nav, animated: true)
                        self.iniBusquedaDocumento()
                    }))
                    
                    self.present(alerta, animated: true, completion: nil)
                    
                }
            }
            else
            {
                if data.CodigoWS != "" {
                    if data.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "En estos momentos no se puede realizar la operación.", preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                        //iniMensajeError(codigo: (data?.CodigoWS!)!)
                    }
                }
            }
        }
    }
    
    func iniBusquedaEmpresarial(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endBusquedaEmpresarial(_:)), name: NSNotification.Name("endBusquedaEmpresarial"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        let userData = UserGlobalData.sharedInstance.userGlobal
        
        
        let sesion : String = (userData?.SesionId!)!
        let tdocumento : String = (userData?.tipoDocumento)!
        let usuario : String = (userData?.user)!
        
        let parametros = [
            "Usuario" : usuario,
            "TDoc" :   tdocumento,
            "NDoc" : "O",
            "SesionId" : sesion
            ] as [String : Any]
        OriginData.sharedInstance.crearAlertasPagos(notification: "endBusquedaEmpresarial", parametros: parametros)
    }
    
    @objc func endBusquedaEmpresarial(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data : AlertaClass = (notification.object as? AlertaClass)!
            if data.CodigoWS == "0"{
                
                self.navigationController?.popViewController(animated: true)
            }
            else
            {
                if data.CodigoWS != "" {
                    if data.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "En estos momentos no se puede realizar la operación.", preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                        //iniMensajeError(codigo: (data?.CodigoWS!)!)
                    }
                }
            }
        }
    }
    
    func validar() -> Bool{
        
        let numeroDocumento : String = self.numeroLB.text!
        let cantidad = numeroDocumento.count
        if numeroDocumento == ""{
            self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar el número de documento a consultar.")
            return false
        }
        if codCombo == "1"{
            if (cantidad == 8 || cantidad == 11) {
                
            }
            else
            {
                self.showAlert(PATHS.SENTINEL, mensaje: "Número de documento inválido")
                return false
            }
        }
        if codCombo == "2" && cantidad != 8 {
            self.showAlert(PATHS.SENTINEL, mensaje: "El número de DNI debe contener 8 dígitos")
            return false
        }
        if codCombo == "3" && cantidad != 11 {
            self.showAlert(PATHS.SENTINEL, mensaje: "El número de RUC debe contener 11 dígitos")
            return false
        }
        if numeroDocumento == userData?.NumDocumento{
            self.showAlert(PATHS.SENTINEL, mensaje: "No se puede consultar a usted mismo.")
            self.numeroLB.text = ""
            return false
        }
        return true
    }
    
    func iniBusquedaDocumento(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endBusquedaDocumento(_:)), name: NSNotification.Name("endBusquedaDocumento"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endBusquedaDocumento(_:)), name: NSNotification.Name("endBusquedaDocumentoSERV"), object: nil)
        
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        
        tipoDoc = ""
        if codCombo == "2" {
            tipoDoc = "D"
        }
        else if codCombo == "3"{
            tipoDoc = "R"
        }
        else if codCombo == "1"{
            if self.numeroLB.text?.count == 8 {
                tipoDoc = "D"
            }
            else if self.numeroLB.text?.count == 11 {
                tipoDoc = "R"
            }
        }
        else if codCombo == "4"{
            tipoDoc = "4"
        }
        else if codCombo == "5"{
            tipoDoc = "5"
        }
        
        let nroDoc : String = self.numeroLB.text!
        let sesion : String = userData!.SesionId!
        
        let servicio = UserGlobalData.sharedInstance.misServiciosGlobal!
//        checkCPT = "NO"
//        checkHisto = "NO"
        if checkCPT == "SI" || checkHisto == "SI"{
            tipocheck = ""
            
            if self.checkCPT == "SI"{
                tipocheck = "DOC"
            }
            else if self.checkHisto == "SI"{
                tipocheck = "CON"
            }
            
            let nroDoc : String = self.numeroLB.text!
            let sesion : String = userData!.SesionId!
            validaBusquedaCPT = true
            let parametros = [
                "UserId" : userData?.user as! String,
                "AFSerCod" : servicio.NroServicio!,
                "AFSerCPTTDoc" :  tipoDoc,
                "afserCPTNroDoc" : nroDoc,
                "UseSeIDSession" : sesion,
                "CnpNomRaz" : "",
                "TipoBus" : tipocheck
                ] as [String : Any]
            OriginData.sharedInstance.miReporteDNITerceroSERV6ESPECIAL(notification: "endBusquedaDocumentoSERV", parametros: parametros)
        }
        else
        {
            let parametros = [
                "Usuario" : userData?.user as! String,
                "Servicio" : servicio.NroServicio!,
                "TipoDocumento" :  tipoDoc,
                "NroDocumento" : nroDoc,
                "SesionId" : sesion,
                "origenAplicacion" : PATHS.APP_ID,
                "TipoConsulta" : "D",
                ] as [String : Any]
            OriginData.sharedInstance.miReporteDNITerceroSERV6(notification: "endBusquedaDocumento", parametros: parametros)
        }
        
    }
    
    @objc func endBusquedaDocumento(_ notification: NSNotification){
        if notification.name.rawValue == "endBusquedaDocumentoSERV" {
            NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
            self.hideActivityIndicator()
            var validaIndicador = 0
            guard notification.object != nil  else {
                validaIndicador = 1
                let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                self.present(alerta, animated: true, completion: nil)
                self.hideActivityIndicator()
                return
            }
            if validaIndicador == 0 {
                let data = notification.object as! TerceroClass
                if data.codigoWS == "0"{
                    if data.AFMVNom != ""{
                        
                    let nroDoc : String = self.numeroLB.text!
                    let sesion : String = userData!.SesionId!
                    
                    let servicio = UserGlobalData.sharedInstance.misServiciosGlobal!
                    let parametros = [
                        "Usuario" : userData?.user as! String,
                        "Servicio" : servicio.NroServicio!,
                        "TipoDocumento" :  tipoDoc,
                        "NroDocumento" : nroDoc,
                        "SesionId" : sesion,
                        "origenAplicacion" : PATHS.APP_ID,
                        "TipoConsulta" : "R",
                        ] as [String : Any]
                    OriginData.sharedInstance.miReporteDNITerceroSERV6(notification: "endBusquedaDocumento", parametros: parametros)
                    }
                    else
                    {
                        let alerta = UIAlertController(title: "ALERTA", message: "No existen consultas para esta persona según tipo de búsqueda.", preferredStyle: UIAlertControllerStyle.alert)
                        alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                        self.present(alerta, animated: true, completion: nil)
                    }
                }
                else
                {
                    if data.codigoWS != nil {
                        if data.codigoWS == "99"
                        {
                            showError99()
                        }
                        else
                        {
                            iniMensajeError(codigo: (data.codigoWS!))
                        }
                    }
                    else
                    {
                        let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                        alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                        self.present(alerta, animated: true, completion: nil)
                    }
                }
            }
        }
        else
        {
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as! TerceroClass
            if data.codigoWS == "0"{

                UserGlobalData.sharedInstance.terceroGlobal.InfoTitular = data.InfoTitular
                let consultado: String = data.InfoTitular!["URLFoto"] as! String
                let url : URL = NSURL(string: consultado)! as URL
                
                self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
                Alamofire.request(url).responseImage { response in
                    debugPrint(response)
                    switch response.result {
                    case .success:
                        if let image = response.result.value {
                            let ok = self.saveImageTercero(image: image)
                            self.hideActivityIndicator()
                            let nav = UIStoryboard.init(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "ReporteEmpresarialVC") as! ReporteEmpresarialViewController
                            self.navigationController?.pushViewController(nav, animated: true)
                        }
                        break
                        
                    case .failure(let error):
                        //CNIGenImaHom
                        let image : UIImage = UIImage.init(named: "CNIGenImaHom")!
                        let ok = self.saveImageTercero(image: image)
                        self.hideActivityIndicator()
                        let nav = UIStoryboard.init(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "ReporteEmpresarialVC") as! ReporteEmpresarialViewController
                        self.navigationController?.pushViewController(nav, animated: true)
                        break
                    }
                }
            }
            else
            {
                if data.codigoWS != nil {
                    if data.codigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        iniMensajeError(codigo: (data.codigoWS!))
                    }
                }
                else
                {
                    let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                    alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                    self.present(alerta, animated: true, completion: nil)
                }
            }
        }
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if codCombo == "1" {
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 10
        }
        if codCombo == "2"{
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 7
        }
        if codCombo == "3"{
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 10
        }
        if codCombo == "4" {
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 19
        }
        return true
    }
    
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func accionCombo(_ sender: Any) {
        
        self.comboDocumento.setConfig(combosArray as! [Any])
        self.comboDocumento.open()
        self.comboDocumento.setId(1)
    }
    @IBAction func accionComboDos(_ sender: Any) {
        
        self.comboDocumento.setConfig(combosArray as! [Any])
        self.comboDocumento.open()
        self.comboDocumento.setId(1)
    }
    @IBAction func accionBuscar(_ sender: Any) {
        if self.validar(){
            //self.iniTitulares()
            self.iniBusquedaDocumento()
        }
    }
    
    func miPickerCombo(_ pickerid: Int, didSelectRow row: Int) {
        switch pickerid {
        case 1:
            codCombo = combosIDArray[row] as! String
            desCombo = combosArray[row] as! String
            
            break
        case 2:
            break
        default:
            break
        }
    }
    
    @IBAction func accionComprar(_ sender: Any) {
        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "Las compras de Sentinel Empresarial se realiza de forma personalizada con la visita de un asesor, por favor contáctanos al teléfono: 514-9000 Anexo 132 o desde nuestra web: www.sentinelperu.com", preferredStyle: .alert)
        
        let actionCancel = UIAlertAction.init(title: "ACEPTAR", style: .cancel, handler: nil)
        alertaVersion.addAction(actionCancel)
        
        self.present(alertaVersion, animated: true, completion: nil)
    }
    func miPickerCombo(_ MiPickerView: CTComboPicker!, pickerView picker: UIPickerView!, didSelectRow row: Int, inComponent component: Int) {
        
    }
}
