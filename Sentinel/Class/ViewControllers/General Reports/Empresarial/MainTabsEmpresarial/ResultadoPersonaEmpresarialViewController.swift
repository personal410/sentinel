//
//  ResultadoPersonaEmpresarialViewController.swift
//  Sentinel
//
//  Created by Daniel Montoya on 1/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//ResultadoPersonaEmpresarialViewController
import Alamofire
import AlamofireImage
class ResultadoPersonaEmpresarialViewController: ParentViewController {
    @IBOutlet weak var coincidenciasLB: UILabel!
    @IBOutlet weak var seleccionaTextoLB: UILabel!
    @IBOutlet weak var personasTable: UITableView!
    @IBOutlet weak var resultadoPersonaToolbar: UIToolbar!
    @IBOutlet weak var disponiblesLB: UILabel!
    @IBOutlet weak var nombreDisponibleLB: UILabel!

    var apePaterno : String = ""
    var apeMaterno : String = ""
    var nombre : String = ""
    var personaJuridica: String = ""
    var otros: String = ""
    var tipoPersona: String!
    var arregloCoincidencias : NSArray = NSArray()
    var arregloCoincidenciasCPT : NSArray = NSArray()
    var tercero : TerceroClass?
    var userData : UserClass?
    var esPremium : Bool = false
    var esEmpresarial : Bool = false
    
    var checkCPT : String = "NO"
    var checkHisto : String = "NO"
    
    //Variables Busqueda CPT
    var dni : String = ""
    var tipo: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializarVariables()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if checkCPT == "SI" || checkHisto == "SI"{
            self.validaCoincidenciasCPT()
        }
        else
        {
            self.iniCoincidenciasEncontradas()
        }
        self.personasTable.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func inicializarVariables(){
        
        self.resultadoPersonaToolbar.clipsToBounds = true
        self.tercero = UserGlobalData.sharedInstance.terceroGlobal
        self.userData = UserGlobalData.sharedInstance.userGlobal
        self.personasTable.dataSource = self
        self.personasTable.delegate = self
    }
    
    func cargarDatos(){
        let disponbles : String = UserGlobalData.sharedInstance.userGlobal.AFSerCPTNDisp
        if disponbles == ""{
            self.disponiblesLB.text = "0"
        }
        else
        {
            let colorD : Int = Int(disponbles)!
            if colorD < 0 {
                self.nombreDisponibleLB.text = "Sobregirado"
                self.nombreDisponibleLB.textColor = UIColor.red
                self.disponiblesLB.textColor = UIColor.red
                self.disponiblesLB.text = disponbles
            }
            else
            {
                self.nombreDisponibleLB.text = "Disponibles"
                self.nombreDisponibleLB.textColor = UIColor.white
                self.disponiblesLB.textColor = UIColor.white
                self.disponiblesLB.text = disponbles
            }
        }
    }
    
    func validaCoincidenciasCPT (){
        if self.arregloCoincidenciasCPT.count == 0 {
            self.coincidenciasLB.text = "No hay coincidencias encontradas."
        }else if self.arregloCoincidenciasCPT.count == 1 {
            self.coincidenciasLB.text = "Hay \(self.arregloCoincidenciasCPT.count) coincidencia encontrada."
        }
        else if self.arregloCoincidenciasCPT.count > 1 {
            self.coincidenciasLB.text = "Hay \(self.arregloCoincidenciasCPT.count) coincidencias encontradas."
        }
        self.cargarDatos()
        self.personasTable.reloadData()
    }
    
    func iniCoincidenciasEncontradas(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endCoincidenciasEncontradas(_:)), name: NSNotification.Name("endCoincidenciasEncontradas"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        let userData = UserGlobalData.sharedInstance.userGlobal
        let randomNumber : String = String(self.generateRandomNumber(numDigits: 7))
        var tipoPersonaTercero : String = ""
        let usuario : String = (userData?.user)!
        let sesion : String = (userData?.SesionId)!
        
        if self.tipoPersona == "1" {
            tipoPersonaTercero = "N"
            let parametros = [
                "UserId":usuario,
                "IdSession":randomNumber,
                "TipoPersona":tipoPersonaTercero,
                "Paterno":self.apePaterno,
                "Materno":self.apeMaterno,
                "nombres":self.nombre,
                "UseSeIDSession":sesion
                ] as [String : Any]
            
            OriginData.sharedInstance.getConsultaNombre(notification: "endCoincidenciasEncontradas", parametros: parametros)
        }
        else if self.tipoPersona == "2" {
            tipoPersonaTercero = "J"
            let parametros = [
                "UserId":usuario,
                "IdSession":randomNumber,
                "TipoPersona":tipoPersonaTercero,
                "Paterno":self.personaJuridica,
                "Materno":"",
                "nombres":"",
                "UseSeIDSession":sesion
                ] as [String : Any]
            OriginData.sharedInstance.getConsultaNombre(notification: "endCoincidenciasEncontradas", parametros: parametros)
        }
        else if self.tipoPersona == "3" {
            tipoPersonaTercero = "O"
            let parametros = [
                "UserId":usuario,
                "IdSession":randomNumber,
                "TipoPersona":tipoPersonaTercero,
                "Paterno":self.otros,
                "Materno":"",
                "nombres":"",
                "UseSeIDSession":sesion
                ] as [String : Any]
            OriginData.sharedInstance.getConsultaNombre(notification: "endCoincidenciasEncontradas", parametros: parametros)
        }
        else{
            let parametros = [
                "UserId":usuario,
                "IdSession":randomNumber,
                "TipoPersona":tipoPersonaTercero,
                "Paterno":self.apePaterno,
                "Materno":self.apeMaterno,
                "nombres":self.nombre,
                "UseSeIDSession":sesion
                ] as [String : Any]
            OriginData.sharedInstance.getConsultaNombre(notification: "endCoincidenciasEncontradas", parametros: parametros)
        }
    }
    
    @objc func endCoincidenciasEncontradas(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as? TerceroClass
            if data?.CodigoWS == "0"{
                UserGlobalData.sharedInstance.terceroGlobal.SDTBusAlf = data?.SDTBusAlf
                self.arregloCoincidencias = (data?.SDTBusAlf)!
                if self.arregloCoincidencias.count == 0 {
                    self.coincidenciasLB.text = "No hay coincidencias encontradas."
                }else if self.arregloCoincidencias.count == 1 {
                    self.coincidenciasLB.text = "Hay \(self.arregloCoincidencias.count) coincidencia encontrada."
                }
                else if self.arregloCoincidencias.count > 1 {
                    self.coincidenciasLB.text = "Hay \(self.arregloCoincidencias.count) coincidencias encontradas."
                }
                self.cargarDatos()
                self.personasTable.reloadData()
            }
            else
            {
                if data?.CodigoWS != nil {
                    if data?.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "En estos momentos no se puede realizar la consulta. Intente luego.", preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                        //iniMensajeError(codigo: (data?.CodigoWS!)!)
                    }
                }
            }
        }
    }
    
    func iniBusquedaDocumento(tipo:String, dni: String){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endBusquedaDocumento(_:)), name: NSNotification.Name("endBusquedaDocumento"), object: nil)
        
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        
        let sesion : String = userData!.SesionId!
        
        let servicio = UserGlobalData.sharedInstance.misServiciosGlobal!

            let parametros = [
                "Usuario" : userData?.user as! String,
                "Servicio" : servicio.NroServicio!,
                "TipoDocumento" :  tipo,
                "NroDocumento" : dni,
                "SesionId" : sesion,
                "origenAplicacion" : PATHS.APP_ID,
                "TipoConsulta" : "D",
                ] as [String : Any]
            OriginData.sharedInstance.miReporteDNITerceroSERV6(notification: "endBusquedaDocumento", parametros: parametros)
    }
    
    @objc func endBusquedaDocumento(_ notification: NSNotification){
        
            NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
            self.hideActivityIndicator()
            var validaIndicador = 0
            guard notification.object != nil  else {
                validaIndicador = 1
                let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                self.present(alerta, animated: true, completion: nil)
                self.hideActivityIndicator()
                return
            }
            if validaIndicador == 0 {
                let data = notification.object as! TerceroClass
                if data.codigoWS == "0"{
                    
                    UserGlobalData.sharedInstance.terceroGlobal.InfoTitular = data.InfoTitular
                    let consultado: String = data.InfoTitular!["URLFoto"] as! String
                    let url : URL = NSURL(string: consultado)! as URL
                    
                    self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
                    Alamofire.request(url).responseImage { response in
                        debugPrint(response)
                        switch response.result {
                        case .success:
                            if let image = response.result.value {
                                let ok = self.saveImageTercero(image: image)
                                self.hideActivityIndicator()
                                let nav = UIStoryboard.init(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "ReporteEmpresarialVC") as! ReporteEmpresarialViewController
                                self.navigationController?.pushViewController(nav, animated: true)
                            }
                            break
                            
                        case .failure(let error):
                            //CNIGenImaHom
                            let image : UIImage = UIImage.init(named: "CNIGenImaHom")!
                            let ok = self.saveImageTercero(image: image)
                            self.hideActivityIndicator()
                            let nav = UIStoryboard.init(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "ReporteEmpresarialVC") as! ReporteEmpresarialViewController
                            self.navigationController?.pushViewController(nav, animated: true)
                            break
                        }
                    }
                }
                else
                {
                    if data.codigoWS != nil {
                        if data.codigoWS == "99"
                        {
                            showError99()
                        }
                        else
                        {
                            iniMensajeError(codigo: (data.codigoWS!))
                        }
                    }
                    else
                    {
                        let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                        alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                        self.present(alerta, animated: true, completion: nil)
                    }
                }
            }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    @objc func accionButton (_ sender: UIButton){
        if self.checkCPT == "SI" || self.checkHisto == "SI" {
            let persona : NSDictionary = self.arregloCoincidenciasCPT[sender.tag] as! NSDictionary
            self.dni = persona.object(forKey: "numdoc") as! String
            self.tipo = persona.object(forKey: "tipodoc") as! String
            
            let userD = UserGlobalData.sharedInstance.userGlobal!
            if userD.NumDocumento == dni {
                self.showAlert(PATHS.SENTINEL, mensaje: "No puede elegirse a usted mismo como consulta")
            }
            else{
                var tipoCompleto : String = ""
                if tipo == "D"{
                    tipoCompleto = "DNI"
                }
                else if tipo == "R"{
                    tipoCompleto = "RUC"
                }
                else
                {
                    tipoCompleto = "CE"
                }
                self.iniBusquedaDocumento(tipo: tipo,dni: dni)
                
            }
        }
        else
        {
            let persona : NSDictionary = self.arregloCoincidencias[sender.tag] as! NSDictionary
            self.dni = persona.object(forKey: "numdoc") as! String
            self.tipo = persona.object(forKey: "tipodoc") as! String
            
            let userD = UserGlobalData.sharedInstance.userGlobal!
            if userD.NumDocumento == dni {
                self.showAlert(PATHS.SENTINEL, mensaje: "No puede elegirse a usted mismo como consulta")
            }
            else{
                var tipoCompleto : String = ""
                if tipo == "D"{
                    tipoCompleto = "DNI"
                }
                else if tipo == "R"{
                    tipoCompleto = "RUC"
                }
                else
                {
                    tipoCompleto = "CE"
                }
                self.iniBusquedaDocumento(tipo: tipo,dni: dni)
                
            }
        }
    }
    
    @IBAction func accionComprar(_ sender: Any) {
        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "Las compras de Sentinel Empresarial se realiza de forma personalizada con la visita de un asesor, por favor contáctanos al teléfono: 514-9000 Anexo 132 o desde nuestra web: www.sentinelperu.com", preferredStyle: .alert)
        
        let actionCancel = UIAlertAction.init(title: "ACEPTAR", style: .cancel, handler: nil)
        alertaVersion.addAction(actionCancel)
        
        self.present(alertaVersion, animated: true, completion: nil)
    }
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension ResultadoPersonaEmpresarialViewController : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if checkCPT == "SI" || checkHisto == "SI"{
            return self.arregloCoincidenciasCPT.count
        }
        else
        {
            return self.arregloCoincidencias.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusquedaPersonasTVC", for: indexPath) as! BusquedaPersonasTableViewCell
        if checkCPT == "SI" || checkHisto == "SI"{
            let persona : NSDictionary = self.arregloCoincidenciasCPT[indexPath.row] as! NSDictionary
            let dni : String = persona.object(forKey: "numdoc") as! String
            let tipo: String = persona.object(forKey: "tipodoc") as! String
            
            if tipo == "D"{
                cell.dniLB.text = "DNI \(dni)"
                let fecha = persona.object(forKey: "adicional") as? String ?? ""
                if fecha.isEmpty || fecha == "00/00/0000" {
                    cell.fechaLB.text = ""
                    cell.guionLB.isHidden = true
                }else{
                    cell.fechaLB.text = fecha
                    cell.guionLB.isHidden = false
                }
            }else if tipo == "R"{
                cell.dniLB.text = "RUC \(dni)"
                cell.fechaLB.text = ""
                cell.guionLB.isHidden = true
            }else{
                cell.dniLB.text = "CE \(dni)"
                cell.fechaLB.text = ""
            }
            let nombrePersona : String = persona.object(forKey: "descripcion") as! String
            cell.nombreLB.text = nombrePersona
            cell.buttonBT.addTarget(self, action: #selector(self.accionButton(_:)), for: .touchUpInside)
            cell.buttonBT.tag = indexPath.row
        }
        else
        {
            let persona : NSDictionary = self.arregloCoincidencias[indexPath.row] as! NSDictionary
            let dni : String = persona.object(forKey: "numdoc") as! String
            let tipo: String = persona.object(forKey: "tipodoc") as! String
            
            if tipo == "D"{
                cell.dniLB.text = "DNI \(dni)"
                let fechaSinFormato : String = persona.object(forKey: "adicional") as! String
                if fechaSinFormato.isEmpty || fechaSinFormato == "00/00/0000"{
                    cell.fechaLB.text = ""
                    cell.guionLB.isHidden = true
                }else{
                    cell.fechaLB.text = fechaSinFormato
                    cell.guionLB.isHidden = false
                }
            }else if tipo == "R"{
                cell.dniLB.text = "RUC \(dni)"
                cell.fechaLB.text = ""
                cell.guionLB.isHidden = true
            }else{
                cell.dniLB.text = "CE \(dni)"
            }
            
            let nombrePersona : String = persona.object(forKey: "descripcion") as! String
            cell.nombreLB.text = nombrePersona
            cell.buttonBT.addTarget(self, action: #selector(self.accionButton(_:)), for: .touchUpInside)
            cell.buttonBT.tag = indexPath.row
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "PopupVC") as! PopupViewController
        let persona : NSDictionary = self.arregloCoincidencias[indexPath.row] as! NSDictionary
        let dni : String = persona.object(forKey: "numdoc") as! String
        let tipo: String = persona.object(forKey: "tipodoc") as! String
        
        let userD = UserGlobalData.sharedInstance.userGlobal!
        if userD.NumDocumento == dni {
            self.showAlert(PATHS.SENTINEL, mensaje: "No puede elegirse a usted mismo como consulta")
        }
        else{
            var tipoCompleto : String = ""
            if tipo == "D"{
                tipoCompleto = "DNI"
            }
            else if tipo == "R"{
                tipoCompleto = "RUC"
            }
            else
            {
                tipoCompleto = "CE"
            }
            
            let nombre : String = persona.object(forKey: "descripcion") as! String
            let alerta = UIAlertController(title: PATHS.SENTINEL, message: "¿Desea consultar a \(nombre) con \(tipoCompleto): \(dni)?", preferredStyle: UIAlertControllerStyle.alert)
            
            alerta.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler: nil))
            
            alerta.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
                nav.tipoDocTercero = tipo
                nav.documentoTercero = dni
                
                self.navigationController?.pushViewController(nav, animated: true)
            }))
            
            self.present(alerta, animated: true, completion: nil)
        }
    }
}



