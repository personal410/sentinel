//
//  ConsultasEmpresarialViewController.swift
//  Sentinel
//
//  Created by Daniel Montoya on 1/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class ConsultasEmpresarialViewController:ParentViewController {
    override func viewDidLoad(){
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated:Bool){
        self.validarPremium()
    }
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            let servicio = UserGlobalData.sharedInstance.misServiciosGlobal
            if servicio?.TipServ != "6" {
                navigationController?.popToRootViewController(animated: true)
            }
        }else{
            let servicio = UserGlobalData.sharedInstance.misServiciosGlobal
            if servicio?.TipServ != "6" {
                navigationController?.popToRootViewController(animated: true)
            }
        }
    }
}
