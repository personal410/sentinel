//
//  MiReporteRUCResumidoViewController.swift
//  Sentinel
//
//  Created by Daniel on 1/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//MiReporteRUCResumidoViewController
import XLPagerTabStrip
import Alamofire
import AlamofireImage
class MiReporteRUCResumidoViewController: ParentViewController,   IndicatorInfoProvider {
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var nombreLB: UILabel!
    @IBOutlet weak var fechaNacimientoLB: UILabel!
    @IBOutlet weak var nroDocumentoLB: UILabel!
    @IBOutlet weak var quieroBT: UIButton!
    @IBOutlet weak var semaforoIMG: UIImageView!
    @IBOutlet weak var deudaLB: UILabel!
    @IBOutlet weak var creditosCL: UICollectionView!
    @IBOutlet weak var semaforosCL: UICollectionView!
    @IBOutlet weak var verReporteBT: UIButton!
    
    var usuarioBean:UserClass = UserClass(userbean: [:])
    
    var creditosArray : NSArray = NSArray()
    var imgArray = [UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris")]
    var mesArray = ["OCT","NOV","DIC","ENE","FEB","MAR","ABR","MAY","JUN","JUL","AGO","SET"]
    var anioArray = ["2017","2017","2017","2018","2018","2018","2018","2018","2018","2018","2018","2018"]
    var docRelacionado : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.gestureScreen()
        self.estilos()
        self.inicializarVariables()
    }
    
    override func viewWillAppear(_ animated:Bool){
        self.iniBusquedaDocumentoRUC()
    }
    
    func estilos(){
        viewImage.layer.cornerRadius = viewImage.frame.size.width / 2
        viewImage.clipsToBounds = true
    }
    
    func inicializarVariables(){
        usuarioBean = UserGlobalData.sharedInstance.userGlobal
        UserGlobalData.sharedInstance.userGlobal.tabIndicador = 1
        self.semaforosCL.dataSource = self
        self.semaforosCL.delegate = self
        self.creditosCL.dataSource = self
        self.creditosCL.delegate = self
        
    }
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func cargarDatos(){
        let userData = UserGlobalData.sharedInstance.terceroGlobal
        let SDTCPTMS  = userData?.InfoTitular!["SDTCPTMS"] as! NSDictionary
        
        //Cargar imagen
        if let imageMain = getSavedImage(named: "fileNameTercero.png") {
            self.image.image = imageMain
        }
        //Nombre Completo
        
        let nombreCompleto : String = SDTCPTMS.object(forKey: "NomRazSoc") as! String
        self.nombreLB.text = nombreCompleto
        //Fecha
        
        let fechaSinFormat = SDTCPTMS.object(forKey: "FechaNacimiento") as! String
        self.fechaNacimientoLB.text = "Fecha Nac. \(fechaSinFormat)"
        
        //FechaProceso
        UserGlobalData.sharedInstance.terceroGlobal.FechaReporte = SDTCPTMS.object(forKey: "FechaProceso") as! String
        
        //DNI
        let dni : String =  SDTCPTMS.object(forKey: "NroDocumento") as! String
        self.nroDocumentoLB.text = "RUC: \(dni)"
        
        //Semaforo
        let semaforoActivo : String = SDTCPTMS.object(forKey: "SemActual") as! String
        self.semaforoIMG.image = self.devuelveSemaforo(SemAct: semaforoActivo)
        //Deuda
        let deuda : String = SDTCPTMS.object(forKey: "DeudaTotal") as! String
        self.deudaLB.text = deuda
        //indicadores
        let indicador : [NSDictionary] = userData?.InfoTitular!["SDTIndicadoresCPT"] as! [NSDictionary]
        self.creditosArray = indicador as NSArray
        
        
        //semaforo últimos meses
        let sema : String = SDTCPTMS.object(forKey: "Semaforos") as! String
        if (sema != "" && sema.count == 12){
            //Parse arreglo imgArray
            imgArray = devuelveArrayIMGSemaforos(semaforos: sema) as! [UIImage?]
        }
        self.semaforosCL.reloadData()
        self.creditosCL.reloadData()
    }
    
    func cargarDatosDNI(){
        let terceroBean = UserGlobalData.sharedInstance.terceroGlobal
        let SDTCPTMS = terceroBean?.InfoTitular!["SDTCPTMS"] as! NSDictionary
        
        //Nombre Completo
        let nombre : String = SDTCPTMS.object(forKey: "Nombre") as! String
        let apePat : String = SDTCPTMS.object(forKey: "ApePaterno") as! String
        let apeMat : String = SDTCPTMS.object(forKey: "ApeMaterno") as! String
        let nombreCompleto : String = "\(apePat) \(apeMat)\n\(nombre)"
        self.nombreLB.text = nombreCompleto
        //FechaProceso
        UserGlobalData.sharedInstance.terceroGlobal.FechaReporte = SDTCPTMS.object(forKey: "FechaProceso") as! String
        //Fecha
        let fechaSinFormat : String = SDTCPTMS.object(forKey: "FechaNacimiento") as! String
        let fecha : String = "Fecha Nac. \(self.devuelveFecha(fecha: fechaSinFormat))"
        self.fechaNacimientoLB.text = fecha
        
        //DNI
        let dni : String =  SDTCPTMS.object(forKey: "NroDocumento") as! String
        if dni.count == 8 {
            self.nroDocumentoLB.text = "DNI: \(dni)"
        }
        else if dni.count == 11 {
            self.nroDocumentoLB.text = "RUC: \(dni)"
        }
        
        //Semaforo
        let semaforoActivo : String = SDTCPTMS.object(forKey: "SemActual") as! String

        self.semaforoIMG.image = self.devuelveSemaforo(SemAct: semaforoActivo)
        
        //Deuda
        let deuda : String = SDTCPTMS.object(forKey: "DeudaTotal") as! String
        self.deudaLB.text = deuda
        
        //indicadores
        let indicador : [NSDictionary] = terceroBean?.InfoTitular!["SDTIndicadoresCPT"] as! [NSDictionary]
        self.creditosArray = indicador as NSArray
        self.creditosCL.reloadData()
        
        //semaforo últimos meses
        let sema : String = SDTCPTMS.object(forKey: "Semaforos") as! String
        if (sema != "" && sema.count == 12) {
            imgArray = devuelveArrayIMGSemaforos(semaforos: sema) as! [UIImage?]
            self.semaforosCL.reloadData()
        }
    }
    @objc func selectedCell(sender: UIButton){
        UserGlobalData.sharedInstance.userGlobal.mesesArreglo = self.devuelveArregloUltimosMeses(fechaUltima: UserGlobalData.sharedInstance.terceroGlobal.FechaReporte)
        UserDefaults.standard.set(sender.tag, forKey: "mesSemaforo")
        let nav = UIStoryboard.init(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "DetalleMesTerceroDetalladoVC") as! DetalleMesTerceroDetalladoViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    func iniBusquedaDocumentoRUC(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endBusquedaDocumento(_:)), name: NSNotification.Name("endBusquedaDocumento"), object: nil)
        
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        
        let tercero = UserGlobalData.sharedInstance.terceroGlobal
        let userData = UserGlobalData.sharedInstance.userGlobal
        
        let tipoDoc : NSDictionary = tercero!.InfoTitular! as NSDictionary
        let tipoDos : NSDictionary = tipoDoc.object(forKey: "SDTCPTMS") as! NSDictionary
        let NroDocumento : String = tipoDos.object(forKey: "NDocRel") as! String
        let TDocRel : String = tipoDos.object(forKey: "TDocRel") as! String
        self.docRelacionado = TDocRel
        
        let nroDoc : String = NroDocumento
        let sesion : String = userData!.SesionId!
        let parametros = [
            "Usuario" : userData!.user!,
            "Servicio" : userData!.NroSerCT!,
            "TipoDocumento" :  TDocRel,
            "NroDocumento" : nroDoc,
            "SesionId" : sesion,
            "origenAplicacion" : PATHS.APP_ID,
            "TipoConsulta" : "R",
            ] as [String : Any]
        OriginData.sharedInstance.miReporteDNITercero(notification: "endBusquedaDocumento", parametros: parametros)
    }
    
    @objc func endBusquedaDocumento(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as! TerceroClass
            if data.codigoWS == "0"{
                
                UserGlobalData.sharedInstance.terceroGlobal.InfoTitular = data.InfoTitular
                let consultado: String = data.InfoTitular!["URLFoto"] as! String
                let url : URL = NSURL(string: consultado)! as URL
                self.clearTempFolder()
                NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarFotoTercero5(_:)), name: NSNotification.Name("endCargarFotoTercero5"), object: nil)
                
                self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
                Alamofire.request(url).responseImage { response in
                    debugPrint(response)
                    
                    switch response.result {
                    case .success:
                        if let image = response.result.value {
                            _ = self.saveImageTercero(image: image)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoTercero5"), object: nil)
                        }
                        break
                    case .failure(_):
                        let image : UIImage = UIImage.init(named: "CNIGenImaHom")!
                        _ = self.saveImageTercero(image: image)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoTercero5"), object: nil)
                        break
                    }
                }
            }
            else
            {
                if data.codigoWS != nil {
                    if data.codigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        iniMensajeError(codigo: (data.codigoWS!))
                    }
                }
                else
                {
                    let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                    alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                    self.present(alerta, animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc func endCargarFotoTercero5(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        
        //Envío al view correspondiente al tipoConsultaTercero
        if self.docRelacionado == "D"{
            self.cargarDatosDNI()
            self.verReporteBT.setTitle("Ver Reporte RUC", for: .normal)
        }
        else if self.docRelacionado == "R"{
            self.cargarDatos()
            self.verReporteBT.setTitle("Ver Reporte DNI", for: .normal)
        }
    }
    @IBAction func accionOtroReporte(_ sender: Any) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "ConsultaReporteResumidoVC") as!ConsultaReporteResumidoViewController
        self.navigationController?.pushViewController(nav, animated: false)
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "REPORTE", image: UIImage(named: ""))
    }
}

extension MiReporteRUCResumidoViewController :UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.semaforosCL {
            return imgArray.count
        }
        else if collectionView == self.creditosCL{
            return creditosArray.count
        }
        return imgArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.semaforosCL {
            let identifier = "SemaforoCVC"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! SemaforoMiReporteCollectionViewCell
            cell.semaIMG.image = imgArray[indexPath.row]
            //cell.mesLB.text = "\(mesArray[indexPath.row])\n\(anioArray[indexPath.row])"
            let arregloMesDesc = self.devuelveArregloUltimosMeses(fechaUltima: UserGlobalData.sharedInstance.terceroGlobal.FechaReporte)
            var swiftArray = arregloMesDesc as AnyObject as! [AnyObject]
            let objSwift0 = swiftArray[0]
            let objSwift1 = swiftArray[1]
            let objSwift2 = swiftArray[2]
            let objSwift3 = swiftArray[3]
            let objSwift4 = swiftArray[4]
            let objSwift5 = swiftArray[5]
            let objSwift6 = swiftArray[6]
            let objSwift7 = swiftArray[7]
            let objSwift8 = swiftArray[8]
            let objSwift9 = swiftArray[9]
            let objSwift10 = swiftArray[10]
            let objSwift11 = swiftArray[11]
            
            let arrayMesesController = NSMutableArray()
            arrayMesesController.add(objSwift11)
            arrayMesesController.add(objSwift10)
            arrayMesesController.add(objSwift9)
            arrayMesesController.add(objSwift8)
            arrayMesesController.add(objSwift7)
            arrayMesesController.add(objSwift6)
            arrayMesesController.add(objSwift5)
            arrayMesesController.add(objSwift4)
            arrayMesesController.add(objSwift3)
            arrayMesesController.add(objSwift2)
            arrayMesesController.add(objSwift1)
            arrayMesesController.add(objSwift0)
            
            let objMes : NSDictionary = arrayMesesController[indexPath.row] as! NSDictionary
            let nombre : String = objMes.object(forKey: "mes") as! String
            let anio : String = objMes.object(forKey: "anio") as! String
            let nombreMes : String = self.devuelveNombreMesParticionado(mes: nombre)
            cell.mesLB.text = "\(nombreMes)\n\(anio)"
            cell.indexBT.tag = indexPath.row
            cell.indexBT.addTarget(self, action: #selector(self.selectedCell(sender:)), for: .touchUpInside)
            return cell
        }
        else if collectionView == self.creditosCL {
            
            let identifier = "CreditosCVC"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! CreditosCollectionViewCell
            
            let objIndicador : NSDictionary = creditosArray[indexPath.row] as! NSDictionary
            let codigo : String = "\((objIndicador.object(forKey: "CICodInd"))!)"
            
            cell.aimagenCreditos.image =  self.devuelveCreditosIndicadores(cred: codigo)
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var widthItem  = 30
        var heighItem = 30
        
        if collectionView == self.semaforosCL {
            widthItem = 45
            heighItem = 60
        }
        else if collectionView == self.creditosCL{
            widthItem  = 30
            heighItem = 40
        }
        return CGSize(width: widthItem, height: heighItem)
    }
    func collectionView(_ collectionView:UICollectionView, didSelectItemAt indexPath:IndexPath){}
}
