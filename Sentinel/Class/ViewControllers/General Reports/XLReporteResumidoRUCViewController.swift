//
//  XLReporteResumidoRUCViewController.swift
//  Sentinel
//
//  Created by Daniel on 1/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import XLPagerTabStrip
class XLReporteResumidoRUCViewController: ButtonBarPagerTabStripViewController {
    var ParentVC: UIViewController = UIViewController()
    
    var primerViewController : MiReporteRUCResumidoViewController!
    var segundoViewController : CreditosIndicadoresTerceroDetalladoViewController!
    
    var isReload = false
    
    let graySpotifyColor = UIColor.fromHex(0x1A1A1A)
    var tabActivo: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.estilos()
        self.reloadPagerTabStripView()
    }
    
    func estilos(){
        settings.style.buttonBarHeight = 44.0
        settings.style.buttonBarBackgroundColor = UIColor.fromHex(0x0072BE)
        settings.style.buttonBarItemBackgroundColor = UIColor.clear
        settings.style.selectedBarBackgroundColor =  UIColor.fromHex(0xffffff)
        settings.style.buttonBarItemFont = UIFont(name: "HelveticaNeue-Bold", size:12) ??
            UIFont.systemFont(ofSize: 12)
        
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor.clear
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        
        settings.style.buttonBarLeftContentInset = 5
        settings.style.buttonBarRightContentInset = 5
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.fromHex(0xe6e6e6)
            newCell?.label.textColor = UIColor.fromHex(0xffffff)
            
            //            oldCell?.imageView.tintColor = UIColor.fromHex(0x808080)
            //            newCell?.imageView.tintColor = UIColor.fromHex(968266)
        }
        
        super.viewDidLoad()
    }
    
    override func viewControllers(for pagerTabStripController:PagerTabStripViewController) -> [UIViewController] {
        let storyboardMain = UIStoryboard(name: "Consultas", bundle: nil)
        primerViewController = storyboardMain.instantiateViewController(withIdentifier: "MiReporteRUCResumidoVC") as! MiReporteRUCResumidoViewController
        segundoViewController = storyboardMain.instantiateViewController(withIdentifier: "CreditosIndicadoresTerceroDetalladoVC") as! CreditosIndicadoresTerceroDetalladoViewController
        guard isReload else {
            return [primerViewController, segundoViewController]
        }
        var childViewControllers = [primerViewController, segundoViewController] as [Any]
        
        for (index, _) in childViewControllers.enumerated(){
            let nElements = childViewControllers.count - index
            let n = (Int(arc4random()) % nElements) + index
            if n != index{
                childViewControllers.swapAt(index, n)
            }
        }
        let nItems = 2
        return Array(childViewControllers.prefix(Int(nItems))) as! [UIViewController]
    }
    
    func actualizarTabContent(){
    }
    
    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool) {
        
        super.updateIndicator(for: viewController, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: progressPercentage, indexWasChanged: indexWasChanged)
        
        self.tabActivo = self.currentIndex + 1
    }
    override func reloadPagerTabStripView() {
        super.reloadPagerTabStripView()
    }
}

