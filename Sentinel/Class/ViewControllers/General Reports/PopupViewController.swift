//
//  PopupViewController.swift
//  Sentinel
//
//  Created by Richy on 11/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import Alamofire
class PopupViewController:ParentViewController {
    @IBOutlet weak var resumidoBT: UIButton!
    @IBOutlet weak var detalladoBT: UIButton!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var alturaMainView: NSLayoutConstraint!
    @IBOutlet weak var spaceTopDetallado: NSLayoutConstraint!
    @IBOutlet weak var spaceTopResumido: NSLayoutConstraint!

    var tercero : TerceroClass?
    var userData : UserClass?
    var tipoConsultaTercero : String!
    var documentoTercero : String!
    var tipoDocTercero : String!
    static var atrasOption = false
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.tabBarItem.isEnabled = false
        PopupViewController.atrasOption = false
    }
    override func viewWillAppear(_ animated:Bool) {
        if PopupViewController.atrasOption == true {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func iniBusquedaDocumento(tipoConsulta : String){
        showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endBusquedaDocumento(_:)), name: NSNotification.Name("endBusquedaDocumento"), object: nil)
        
        self.tercero = UserGlobalData.sharedInstance.terceroGlobal
        self.userData = UserGlobalData.sharedInstance.userGlobal
        let servicios = UserGlobalData.sharedInstance.misServiciosGlobal!
        
        let tipoDoc = self.tipoDocTercero
        let nroDoc = self.documentoTercero
        let sesion = userData!.SesionId!
        
        let servicioUser = (userData?.NroSerCT)!
        var servicioCategorico : String = servicioUser
        if servicios.NroServicio != "0"{
            if servicios.NroServicio == servicioUser {
                servicioCategorico = servicioUser
            }
            else
            {
                servicioCategorico = servicios.NroServicio!
            }
        }
        
        let parametros = [
            "Usuario" : userData!.user!,
            "Servicio" : servicioCategorico,
            "TipoDocumento" : tipoDoc,
            "NroDocumento" : nroDoc,
            "SesionId" : sesion,
            "origenAplicacion" : PATHS.APP_ID,
            "TipoConsulta" : tipoConsulta,
            ] as [String : Any]
        OriginData.sharedInstance.miReporteDNITercero(notification: "endBusquedaDocumento", parametros: parametros)
    }
    
    @objc func endBusquedaDocumento(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as! TerceroClass
            if data.codigoWS == "0"{
                UserGlobalData.sharedInstance.terceroGlobal.documentoTercero = self.documentoTercero
                UserGlobalData.sharedInstance.terceroGlobal.tipoDocumentoTercero = self.tipoDocTercero
                UserGlobalData.sharedInstance.terceroGlobal.InfoTitular = data.InfoTitular
                UserGlobalData.sharedInstance.terceroGlobal.tipoReporte = tipoConsultaTercero
                
                let consultado: String = data.InfoTitular!["URLFoto"] as! String
                let url : URL = NSURL(string: consultado)! as URL
                self.clearTempFolder()
                NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarFotoTercero3(_:)), name: NSNotification.Name("endCargarFotoTercero3"), object: nil)
                
                self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
                Alamofire.request(url).responseImage { response in
                    debugPrint(response)
                    
                    switch response.result {
                    case .success:
                        if let image = response.result.value {
                            _ = self.saveImageTercero(image: image)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoTercero3"), object: nil)
                        }
                        break
                        
                    case .failure(_):
                        let image : UIImage = UIImage(named: "CNIGenImaHom")!
                        _ = self.saveImageTercero(image: image)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoTercero3"), object: nil)
                        break
                    }
                } 
            }
            else
            {
                if data.codigoWS != nil {
                    if data.codigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        iniMensajeError(codigo: (data.codigoWS!))
                    }
                }
                else
                {
                    let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                    alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                    self.present(alerta, animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc func endCargarFotoTercero3(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        
        if tipoConsultaTercero == "D"{
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "ConsultaReporteDetalladoVC") as! ConsultaReporteDetalladoViewController
            self.navigationController?.pushViewController(nav, animated: true)
        }
        else if tipoConsultaTercero == "R"{
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "ConsultaReporteResumidoVC") as! ConsultaReporteResumidoViewController
            self.navigationController?.pushViewController(nav, animated: true)
        }
    }
    
    @IBAction func accionDetallado(_ sender: Any) {
        let tercero = UserGlobalData.sharedInstance.terceroGlobal
        if tercero?.DispDet == "0"{
            self.irComprarConsultas()
        }else{
            tipoConsultaTercero = "D"
            self.iniBusquedaDocumento(tipoConsulta: "D")
        }
    }
    @IBAction func accionResumido(_ sender:Any){
        let tercero = UserGlobalData.sharedInstance.terceroGlobal
        if tercero?.DispRes == "0"{
            self.irComprarConsultas()
        }else{
            tipoConsultaTercero = "R"
            self.iniBusquedaDocumento(tipoConsulta: "R")
        }
    }
    @IBAction func cerrar(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
}
