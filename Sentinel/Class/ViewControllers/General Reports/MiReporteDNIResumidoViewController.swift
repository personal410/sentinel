//
//  MiReporteDNIResumidoViewController.swift
//  Sentinel
//
//  Created by Daniel on 1/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//MiReporteDNIResumidoViewController
import XLPagerTabStrip
class MiReporteDNIResumidoViewController:ParentViewController, IndicatorInfoProvider {
    @IBOutlet weak var topFechaNac: NSLayoutConstraint!
    @IBOutlet weak var alturaFechaNac: NSLayoutConstraint!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var nombreLB: UILabel!
    @IBOutlet weak var fechaNacimientoLB: UILabel!
    @IBOutlet weak var nroDocumentoLB: UILabel!
    @IBOutlet weak var quieroBT: UIButton!
    @IBOutlet weak var signoDeudaLB: UILabel!
    @IBOutlet weak var semaforoIMG: UIImageView!
    @IBOutlet weak var deudaLB: UILabel!
    @IBOutlet weak var creditosCL: UICollectionView!
    @IBOutlet weak var semaforosCL: UICollectionView!
    @IBOutlet weak var sinCreditos: UILabel!
    @IBOutlet weak var verReporteBT: UIButton!
    
    var usuarioBean:  UserClass = UserClass(userbean: [:])
    
    var creditosArray : NSArray = NSArray()
    var imgArray = [UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.estilos()
        self.inicializarVariables()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.cargarDatos()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.semaforosCL.scrollToItem(at: IndexPath(item: 11, section: 0), at: .right, animated: false)
    }
    
    func estilos(){
        viewImage.layer.cornerRadius = viewImage.frame.size.width / 2
        viewImage.clipsToBounds = true
    }
    
    func inicializarVariables(){
        usuarioBean = UserGlobalData.sharedInstance.userGlobal
        UserGlobalData.sharedInstance.userGlobal.tabIndicador = 1
        self.semaforosCL.dataSource = self
        self.semaforosCL.delegate = self
        self.creditosCL.dataSource = self
        self.creditosCL.delegate = self
        
    }
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func cargarDatos(){
        let userData = UserGlobalData.sharedInstance.terceroGlobal
        let SDTCPTMS  = userData?.InfoTitular!["SDTCPTMS"] as! NSDictionary
        let tipoDoc = SDTCPTMS.object(forKey: "TipoDocumento") as! String
        
        UserGlobalData.sharedInstance.terceroGlobal.tipoDocumentoTercero = tipoDoc
        if tipoDoc == "D" ||  tipoDoc == "5"{
        //Cargar imagen
        if let imageMain = getSavedImage(named: "fileNameTercero.png") {
            self.image.image = imageMain
        }
        //Nombre Completo
        let nombre = SDTCPTMS.object(forKey: "Nombre") as! String
        let apePat = SDTCPTMS.object(forKey: "ApePaterno") as! String
        let apeMat = SDTCPTMS.object(forKey: "ApeMaterno") as! String
        let nombreCompleto : String = "\(apePat) \(apeMat)\n\(nombre)"
        self.nombreLB.text = nombreCompleto
        //Fecha
        let fechaSinFormat = SDTCPTMS.object(forKey: "FechaNacimiento") as! String
        if fechaSinFormat.isEmpty || fechaSinFormat == "0000-00-00" {
            self.fechaNacimientoLB.text = ""
            self.fechaNacimientoLB.isHidden = true
            self.topFechaNac.constant = 0
            self.alturaFechaNac.constant = 0
        }else{
            self.fechaNacimientoLB.text = "Fecha Nac. \(fechaSinFormat)"
        }
        //DNI
        let dni =  SDTCPTMS.object(forKey: "NroDocumento") as! String
        if dni.count == 8 {
            self.nroDocumentoLB.text = "DNI: \(dni)"
            self.verReporteBT.setTitle("Ver Reporte RUC", for: .normal)
        }else if dni.count == 11 {
            self.nroDocumentoLB.text = "RUC: \(dni)"
            self.verReporteBT.setTitle("Ver Reporte DNI", for: .normal)
        }
        UserGlobalData.sharedInstance.terceroGlobal.documentoTercero = dni
            
        //FechaProceso
        UserGlobalData.sharedInstance.terceroGlobal.FechaReporte = SDTCPTMS.object(forKey: "FechaProceso") as! String
        //Semaforo
        let semaforoActivo : String = SDTCPTMS.object(forKey: "SemActual") as! String
        self.semaforoIMG.image = self.devuelveSemaforo(SemAct: semaforoActivo)
        self.cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: self.deudaLB)
        self.cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: self.signoDeudaLB)
        //Deuda
        let deuda : String = SDTCPTMS.object(forKey: "DeudaTotal") as! String
            
            var newDeuda: NSNumber = NSNumber.init( value: Double(deuda)!)
            
            if deuda == "0.00"{
                self.deudaLB.text = deuda
            }
            else
            {
                var newDeuda: NSNumber = NSNumber.init( value: Double(deuda)!)
                
                var numberFormatter = NumberFormatter()
                numberFormatter.formatterBehavior = .behavior10_4
                numberFormatter.numberStyle = .decimal
                var numberString = numberFormatter.string(from: newDeuda)
                //        let deudaNumber : String = String((Double(deuda)! as NSNumber).description(withLocale: Locale.current))
                self.deudaLB.text = numberString
            }
        
        //indicadores
        let indicador : [NSDictionary] = userData?.InfoTitular!["SDTIndicadoresCPT"] as! [NSDictionary]
        self.creditosArray = indicador as NSArray
            if self.creditosArray.count == 0 {
                self.sinCreditos.isHidden = false
            }
        
        //semaforo últimos meses
        let sema : String = SDTCPTMS.object(forKey: "Semaforos") as! String
        if (sema != "" && sema.count == 12){
            imgArray = devuelveArrayIMGSemaforos(semaforos: sema) as! [UIImage?]
        }
        else
        {
            let semaDefault : String = "GGGGGGGGGGGG"
            imgArray = devuelveArrayIMGSemaforos(semaforos: semaDefault) as! [UIImage?]
        }
        self.semaforosCL.reloadData()
        self.creditosCL.reloadData()
        }
        else if tipoDoc == "R"{
            //Cargar imagen
            if let imageMain = getSavedImage(named: "fileNameTercero.png") {
                self.image.image = UIImage.init(named: "IconoEmpresaF")
            }
            else
            {
                self.image.image = UIImage.init(named: "IconoEmpresaF")
            }
            let nombreCompleto : String = SDTCPTMS.object(forKey: "NomRazSoc") as! String
            self.nombreLB.text = nombreCompleto
            //Fecha
            
            self.fechaNacimientoLB.isHidden = true
            self.topFechaNac.constant = 0
            self.alturaFechaNac.constant = 0
            //DNI
            let dni : String =  SDTCPTMS.object(forKey: "NroDocumento") as! String
            if dni.count == 8 {
                self.nroDocumentoLB.text = "DNI: \(dni)"
                self.verReporteBT.setTitle("Ver Reporte RUC", for: .normal)
            }
            else if dni.count == 11 {
                self.nroDocumentoLB.text = "RUC: \(dni)"
                self.verReporteBT.setTitle("Ver Reporte DNI", for: .normal)
            }
            UserGlobalData.sharedInstance.terceroGlobal.documentoTercero = dni
            
            //FechaProceso
            UserGlobalData.sharedInstance.terceroGlobal.FechaReporte = SDTCPTMS.object(forKey: "FechaProceso") as! String
            //Semaforo
            var semaforoActivo : String = SDTCPTMS.object(forKey: "SemActual") as! String
            self.semaforoIMG.image = self.devuelveSemaforo(SemAct: semaforoActivo)
            self.cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: self.deudaLB)
            self.cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: self.signoDeudaLB)
            //Deuda
            let deuda : String = SDTCPTMS.object(forKey: "DeudaTotal") as! String
            if deuda == "0.00"{
                self.deudaLB.text = deuda
            }
            else
            {
                var newDeuda: NSNumber = NSNumber.init( value: Double(deuda)!)
                
                var numberFormatter = NumberFormatter()
                numberFormatter.formatterBehavior = .behavior10_4
                numberFormatter.numberStyle = .decimal
                var numberString = numberFormatter.string(from: newDeuda)
                //        let deudaNumber : String = String((Double(deuda)! as NSNumber).description(withLocale: Locale.current))
                self.deudaLB.text = numberString
            }
            //indicadores
            let indicador : [NSDictionary] = userData?.InfoTitular!["SDTIndicadoresCPT"] as! [NSDictionary]
            self.creditosArray = indicador as NSArray
            if self.creditosArray.count == 0 {
                self.sinCreditos.isHidden = false
            }
            
            //semaforo últimos meses
            let sema : String = SDTCPTMS.object(forKey: "Semaforos") as! String
            if (sema != "" && sema.count == 12){
                //Parse arreglo imgArray
                imgArray = devuelveArrayIMGSemaforos(semaforos: sema) as! [UIImage?]
            }
            else
            {
                let semaDefault : String = "GGGGGGGGGGGG"
                imgArray = devuelveArrayIMGSemaforos(semaforos  : semaDefault) as! [UIImage?]
                self.semaforosCL.reloadData()
            }
            self.semaforosCL.reloadData()
            self.creditosCL.reloadData()
        }
    }
    
    @objc func selectedCell(sender:UIButton){
        if sender.tag == 11 || sender.tag == 10 || sender.tag == 9 {
            UserGlobalData.sharedInstance.userGlobal.mesesArreglo = self.devuelveArregloUltimosTresMeses(fechaUltima: UserGlobalData.sharedInstance.terceroGlobal.FechaReporte)
            UserDefaults.standard.set(sender.tag, forKey: "mesSemaforo")
                let nav = self.storyboard?.instantiateViewController(withIdentifier: "DetalleMesTerceroResumidosVC") as! DetalleMesTerceroResumidosViewController
                self.navigationController?.pushViewController(nav, animated: true)
        }else{
            if let mainTabBarCont = navigationController?.parent as? MainTabBarViewController  {
                mainTabBarCont.cambiarPantallaCompra()
            }
        }
    }
    @IBAction func accionOtroReporte(_ sender: Any){}
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "REPORTE", image: UIImage(named: ""))
    }
}

extension MiReporteDNIResumidoViewController :UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.semaforosCL {
            return imgArray.count
        }
        else if collectionView == self.creditosCL{
            return creditosArray.count
        }
        return imgArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.semaforosCL {
            let identifier = "SemaforoCVC"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! SemaforoMiReporteCollectionViewCell
            cell.semaIMG.image = imgArray[indexPath.row]
            let arregloMesDesc = self.devuelveArregloUltimosMeses(fechaUltima: UserGlobalData.sharedInstance.terceroGlobal.FechaReporte)
            var swiftArray = arregloMesDesc as AnyObject as! [AnyObject]
            let objSwift0 = swiftArray[0]
            let objSwift1 = swiftArray[1]
            let objSwift2 = swiftArray[2]
            let objSwift3 = swiftArray[3]
            let objSwift4 = swiftArray[4]
            let objSwift5 = swiftArray[5]
            let objSwift6 = swiftArray[6]
            let objSwift7 = swiftArray[7]
            let objSwift8 = swiftArray[8]
            let objSwift9 = swiftArray[9]
            let objSwift10 = swiftArray[10]
            let objSwift11 = swiftArray[11]
            
            let arrayMesesController = NSMutableArray()
            arrayMesesController.add(objSwift11)
            arrayMesesController.add(objSwift10)
            arrayMesesController.add(objSwift9)
            arrayMesesController.add(objSwift8)
            arrayMesesController.add(objSwift7)
            arrayMesesController.add(objSwift6)
            arrayMesesController.add(objSwift5)
            arrayMesesController.add(objSwift4)
            arrayMesesController.add(objSwift3)
            arrayMesesController.add(objSwift2)
            arrayMesesController.add(objSwift1)
            arrayMesesController.add(objSwift0)
            
            let objMes : NSDictionary = arrayMesesController[indexPath.row] as! NSDictionary
            let nombre : String = objMes.object(forKey: "mes") as! String
            let anio : String = objMes.object(forKey: "anio") as! String
            let nombreMes : String = self.devuelveNombreMesParticionado(mes: nombre)
            cell.mesLB.text = "\(nombreMes)\n\(anio)"
            cell.indexBT.tag = indexPath.row
            cell.indexBT.addTarget(self, action: #selector(self.selectedCell(sender:)), for: .touchUpInside)
            return cell
        }
        else if collectionView == self.creditosCL {
            
            let identifier = "CreditosCVC"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! CreditosCollectionViewCell
            
            let objIndicador : NSDictionary = creditosArray[indexPath.row] as! NSDictionary
            let codigo : String = "\((objIndicador.object(forKey: "CICodInd"))!)"
            
            cell.aimagenCreditos.image =  self.devuelveCreditosIndicadores(cred: codigo)
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.semaforosCL {
            return 5
        }
        else if collectionView == self.creditosCL{
            return 5
        }
        return 5
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var widthItem  = 30
        var heighItem = 30
        if collectionView == self.semaforosCL {
            widthItem = 45
            heighItem = 60
        }else if collectionView == self.creditosCL{
            heighItem = 40
        }
        return CGSize(width: widthItem, height: heighItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.creditosCL {
            if let parenteVC = self.parent as? XLReporteResumidoViewController {
                parenteVC.moveToViewController(at: 2)
            }
        }
    }
}
