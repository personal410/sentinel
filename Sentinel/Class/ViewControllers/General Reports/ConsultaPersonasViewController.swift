//
//  ConsultaPersonasViewController.swift
//  Sentinel
//
//  Created by Daniel on 18/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import Alamofire
import AlamofireImage
class ConsultaPersonasViewController:ParentViewController, MiPickerComboDelegate {
    @IBOutlet weak var scrollVista:UIScrollView!
    @IBOutlet weak var resumidoLB:UILabel!
    @IBOutlet weak var detalladoLB: UILabel!
    @IBOutlet weak var consultaPersonaToolbar:UIToolbar!
    @IBOutlet weak var comboPersona:CTComboPicker!
    @IBOutlet weak var personaJuridicaTXT: UITextField!
    @IBOutlet weak var otrosTXT: UITextField!
    @IBOutlet weak var personaNaturalView:UIView!
    @IBOutlet weak var apePaternoTXT:UITextField!
    @IBOutlet weak var apeMaternoTXT:UITextField!
    @IBOutlet weak var nombresTXT:UITextField!
    
    var tercero:TerceroClass?
    var userData:UserClass?
    var esPremium = false
    var esEmpresarial = false
    var isKeyboardAppear = false
    var codCombo = "1"
    var desCombo = "Persona Natural"
    var combosArray = NSMutableArray()
    var combosIDArray = NSMutableArray()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.inicializarVariables()
        self.gestureScreen()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.validarPremium()
        iniConsultaDatosServicio()
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        let servicios = UserGlobalData.sharedInstance.misServiciosGlobal
        
        if userData?.EsPremium == "S" {
            self.esPremium = true
            
            if servicios?.TipServ == "6"
            {
                self.esEmpresarial = true
                self.changeStatusBar(cod: 2)
            }
            else
            {
                self.esEmpresarial = false
                self.changeStatusBar(cod: 3)
                self.setGradient(uiView: self.consultaPersonaToolbar)
            }
        }
        else
        {
            if servicios?.TipServ == "6"{
                self.esEmpresarial = true
                self.changeStatusBar(cod: 2)
                self.consultaPersonaToolbar.barTintColor = UIColor.fromHex(0x363636)
            }
            else{
                self.esEmpresarial = false
                self.esPremium = false
            }
        }
    }
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    func validar() -> Bool{
        let personaJuridica = self.personaJuridicaTXT.text!
        let otros = self.otrosTXT.text!
        let apePaterno = self.apePaternoTXT.text!
        let apeMaterno = self.apeMaternoTXT.text!
        let nombres = self.nombresTXT.text!
        if codCombo == "1" {
            if apePaterno.isEmpty {
                self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar Apellido Paterno")
                return false
            }
            if apeMaterno.isEmpty && nombres.isEmpty {
                self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar Apellido Materno o Nombres")
                return false
            }
        }else if codCombo == "2" {
            if personaJuridica == ""{
                self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar la persona jurídica.")
                return false
            }
        }else if codCombo == "3" {
            if otros == ""{
                self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar el nombre.")
                return false
            }
        }
        return true
    }
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func inicializarVariables(){
        self.consultaPersonaToolbar.clipsToBounds = true
        
        self.tercero = UserGlobalData.sharedInstance.terceroGlobal
        self.userData = UserGlobalData.sharedInstance.userGlobal
        
        self.comboPersona.delegate = self
        
        self.combosArray.add("Persona Natural")
        self.combosArray.add("Persona Jurídica")
        self.combosArray.add("Otros")
        
        self.combosIDArray.add("1")
        self.combosIDArray.add("2")
        self.combosIDArray.add("3")
    }

    @objc func keyboardWillShow(notification:NSNotification){
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let content = UIEdgeInsetsMake(0, 0, keyboardSize.height - 50, 0)
            scrollVista.contentInset = content
            scrollVista.scrollIndicatorInsets = content
        }
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        scrollVista.contentInset = .zero
        scrollVista.scrollIndicatorInsets = .zero
    }
    
    func randomInt(min: Int, max: Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
    
    func iniBusquedaDocumento(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.endBusquedaDocumento(_:)), name: NSNotification.Name("endBusquedaDocumento"), object: nil)
        showActivityIndicator()
        
        var tipoDoc = ""
        var apePat = ""
        var apeMat = ""
        var nomCPT = ""
        if codCombo == "1" {
            tipoDoc = "N"
            apePat = self.apePaternoTXT.text!
            apeMat = self.apeMaternoTXT.text!
            nomCPT = self.nombresTXT.text!
        }else if codCombo == "2"{
            tipoDoc = "J"
            apePat = self.personaJuridicaTXT.text!
        }else if codCombo == "3"{
            tipoDoc = "O"
            apePat = self.otrosTXT.text!
        }
        let numberR = self.randomInt(min: 100000, max: 999999)
        let parametros = [
            "UserId" : userData!.user!,
            "IdSession" : numberR,
            "TipoPersona" :  tipoDoc,
            "Paterno" : apePat,
            "Materno" : apeMat,
            "nombres" : nomCPT,
            "UseSeIDSession" : userData!.SesionId!,
            ] as [String : Any]
        OriginData.sharedInstance.busquedaTerceroCoincidencias(notification: "endBusquedaDocumento", parametros: parametros)
    }
    
    @objc func endBusquedaDocumento(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as! TerceroClass
            if data.codigoWS == "0"{
                UserGlobalData.sharedInstance.terceroGlobal.InfoTitular = data.InfoTitular
                let consultado: String = data.InfoTitular!["URLFoto"] as! String
                let url : URL = NSURL(string: consultado)! as URL
                self.clearTempFolder()
                NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarFotoTercero(_:)), name: NSNotification.Name("endCargarFotoTercero"), object: nil)
                
                self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
                Alamofire.request(url).responseImage { response in
                    debugPrint(response)
                    switch response.result {
                    case .success:
                        if let image = response.result.value {
                            _ = self.saveImageTercero(image: image)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoTercero"), object: nil)
                        }
                        break
                    case .failure( _):
                        let image : UIImage = UIImage.init(named: "CNIGenImaHom")!
                        _ = self.saveImageTercero(image: image)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoTercero"), object: nil)
                        break
                    }
                }
            }else{
                if data.codigoWS != nil {
                    if data.codigoWS == "99"{
                        showError99()
                    }else{
                        iniMensajeError(codigo: (data.codigoWS!))
                    }
                }else{
                    Toolbox.showAlert(with: "ALERTA", and: "En estos momentos no es posible realizar la consulta.", in: self)
                }
            }
        }
    }
    
    @objc func endCargarFotoTercero(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        
        let nav = UIStoryboard.init(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "MiReporteFlashVC") as! MiReporteFlashViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    public func iniConsultaDatosServicio(){
        showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endConsultaDatosServicio(_:)), name: NSNotification.Name("endConsultaDatosServicio"), object: nil)
        usuarioParent = UserGlobalData.sharedInstance.userGlobal
        let usua : String = (usuarioParent?.NumDocumento)!
        let sesion : String = (usuarioParent?.SesionId)!
        let servicios = UserGlobalData.sharedInstance.misServiciosGlobal!
        let servicio : String = servicios.NroServicio!
        
        let parametros = [
            "Usuario": usua,
            "SesionId": sesion,
            "Servicio": servicio
            ] as [String : Any]
        OriginData.sharedInstance.consultaDatosServicio(notification: "endConsultaDatosServicio", parametros: parametros)
    }
    
    @objc func endConsultaDatosServicio(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let terceroLocal = notification.object as! TerceroClass
            UserGlobalData.sharedInstance.terceroGlobal = terceroLocal
            terceroParent = UserGlobalData.sharedInstance.terceroGlobal
            
            UserGlobalData.sharedInstance.terceroGlobal.DispDet = terceroLocal.SaldoServicio?.object(forKey: "DispDet") as? String
            UserGlobalData.sharedInstance.terceroGlobal.DispRes = terceroLocal.SaldoServicio?.object(forKey: "DispRes") as? String
            UserGlobalData.sharedInstance.terceroGlobal.SalFlash = terceroLocal.SaldoServicio?.object(forKey: "SalFlash") as? String
            self.resumidoLB.text = terceroLocal.SaldoServicio?.object(forKey: "DispRes") as? String
            self.detalladoLB.text = terceroLocal.SaldoServicio?.object(forKey: "DispDet") as? String
        }
    }
    
    @IBAction func accionCombo(_ sender: Any) {
        self.comboPersona.setConfig(combosArray as! [Any])
        self.comboPersona.open()
        self.comboPersona.setId(1)
    }
    @IBAction func accionComboDos(_ sender: Any) {
        self.comboPersona.setConfig(combosArray as! [Any])
        self.comboPersona.open()
        self.comboPersona.setId(1)
    }
    @IBAction func accionBuscar(_ sender: Any) {
        if validar(){
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "ConsultaResultadoPersonasVC") as! ConsultaResultadoPersonasViewController
            nav.apePaterno = self.apePaternoTXT.text!
            nav.apeMaterno = self.apeMaternoTXT.text!
            nav.nombre = self.nombresTXT.text!
            nav.tipoPersona = self.codCombo
            nav.otros = self.otrosTXT.text!
            nav.personaJuridica = self.personaJuridicaTXT.text!
            self.navigationController?.pushViewController(nav, animated: true)
        }
    }
    @IBAction func accionComprar(_ sender: Any) {
        let nav = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReporteDeudasPersonasEmpresasVC") as! ReporteDeudasPersonasEmpresasViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func miPickerCombo(_ pickerid: Int, didSelectRow row: Int) {
        switch pickerid {
        case 1:
            codCombo = combosIDArray[row] as! String
            desCombo = combosArray[row] as! String
            if codCombo == "1"{
                self.personaNaturalView.isHidden = false
                self.personaJuridicaTXT.isHidden = true
                self.otrosTXT.isHidden = true
            }
            else if codCombo == "2"{
                self.personaNaturalView.isHidden = true
                self.personaJuridicaTXT.isHidden = false
                self.otrosTXT.isHidden = true
            }
            else if codCombo == "3"{
                self.personaNaturalView.isHidden = true
                self.personaJuridicaTXT.isHidden = true
                self.otrosTXT.isHidden = false
            }
            break
        case 2:
            break
        default:
            break
        }
    }
    func miPickerCombo(_ MiPickerView:CTComboPicker!, pickerView picker:UIPickerView!, didSelectRow row:Int, inComponent component:Int){}
}
