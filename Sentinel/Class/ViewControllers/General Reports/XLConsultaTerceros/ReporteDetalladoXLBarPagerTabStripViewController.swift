//
//  ReporteDetalladoXLBarPagerTabStripViewController.swift
//  Sentinel
//
//  Created by Daniel on 19/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
import XLPagerTabStrip
class ReporteDetalladoXLBarPagerTabStripViewController:ButtonBarPagerTabStripViewController {
    @IBOutlet weak var barrabb: ButtonBarView!
    var ParentVC:UIViewController = UIViewController()
    var userData:UserClass!
    var primerViewController:MiReporteTerceroDetalladoViewController!
    var lineasCreditoViewCont:LineasCreditoViewController!
    var segundoViewController:CreditosIndicadoresTerceroDetalladoViewController!
    var isReload = false
    let graySpotifyColor = UIColor.fromHex(0x1A1A1A)
    var tabActivo: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.estilos()
        self.reloadPagerTabStripView()
    }
    func estilos(){
        userData = UserGlobalData.sharedInstance.userGlobal
        if userData.EsPremium == "S"{
            setGradient(uiView: barrabb)
            settings.style.buttonBarBackgroundColor = UIColor.fromHex(0xEFD450)
        }else{
           settings.style.buttonBarBackgroundColor = UIColor.fromHex(0x06B150)
        }
        settings.style.buttonBarHeight = 44.0
        settings.style.buttonBarItemBackgroundColor = UIColor.clear
        settings.style.selectedBarBackgroundColor =  UIColor.fromHex(0xffffff)
        settings.style.buttonBarItemFont = UIFont(name: "HelveticaNeue-Bold", size:12) ??
            UIFont.systemFont(ofSize: 12)
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 10
        settings.style.buttonBarItemTitleColor = UIColor.clear
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 1
        settings.style.buttonBarRightContentInset = 1
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            
            if self.userData.EsPremium == "S" {
                oldCell?.label.textColor = UIColor(white: 1, alpha: 0.5)
                newCell?.label.textColor = UIColor.fromHex(rgbValue: 0xffffff)
                
                oldCell?.imageView.tintColor = UIColor(white: 1, alpha: 0.5)
                newCell?.imageView.tintColor = UIColor.fromHex(rgbValue: 0xffffff)
            }else{
                oldCell?.label.textColor = UIColor.fromHex(0x137d3d)
                newCell?.label.textColor = UIColor.fromHex(0xffffff)
                
                oldCell?.imageView.tintColor = UIColor.fromHex(0x137d3d)
                newCell?.imageView.tintColor = UIColor.fromHex(rgbValue: 0xffffff)
            }
        }
        
        super.viewDidLoad()
    }
    
    override func viewControllers(for pagerTabStripController:PagerTabStripViewController) -> [UIViewController] {
        let storyboardMain = UIStoryboard(name: "Consultas", bundle: nil)
        primerViewController = storyboardMain.instantiateViewController(withIdentifier: "MiReporteTerceroDetalladoVC") as? MiReporteTerceroDetalladoViewController
        lineasCreditoViewCont = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LineasCreditoVC") as? LineasCreditoViewController
        let usuarioBean = UserGlobalData.sharedInstance.userGlobal!
        let terceroBean = UserGlobalData.sharedInstance.terceroGlobal!
        lineasCreditoViewCont.servicio = usuarioBean.NroSerCT!
        lineasCreditoViewCont.tipoDocumento = terceroBean.tipoDocumentoTercero!
        lineasCreditoViewCont.nroDocumento = terceroBean.documentoTercero!
        lineasCreditoViewCont.fechaProceso = terceroBean.FechaReporte
        
        segundoViewController = storyboardMain.instantiateViewController(withIdentifier: "CreditosIndicadoresTerceroDetalladoVC") as? CreditosIndicadoresTerceroDetalladoViewController
        
        guard isReload else {
            return [primerViewController, lineasCreditoViewCont, segundoViewController]
        }
        
        var childViewControllers = [primerViewController, lineasCreditoViewCont, segundoViewController] as [Any]
        
        for (index, _) in childViewControllers.enumerated(){
            let nElements = childViewControllers.count - index
            let n = (Int(arc4random()) % nElements) + index
            if n != index{
                childViewControllers.swapAt(index, n)
            }
        }
        let nItems = 2
        return Array(childViewControllers.prefix(Int(nItems))) as! [UIViewController]
    }
    
    // MARK: - Métodos generales
    func actualizarTabContent(){}
    
    // MARK: - Métodos PagerTabStripViewController
    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged:Bool) {
        super.updateIndicator(for: viewController, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: progressPercentage, indexWasChanged: indexWasChanged)
        self.tabActivo = self.currentIndex + 1
    }

    override func reloadPagerTabStripView() {
        super.reloadPagerTabStripView()
    }
    
    func setGradient(uiView:UIView){
        let colorBottom =  UIColor(red: 239.0/255.0, green: 212.0/255.0, blue: 80.0/255.0, alpha: 1.0).cgColor
        let colorTop = UIColor(red: 209.0/255.0, green: 142.0/255.0, blue: 24.0/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.startPoint = CGPoint(x:0.0, y:0.5)
        gradientLayer.endPoint = CGPoint(x:1.0, y:0.5)
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = uiView.bounds
        uiView.layer.insertSublayer(gradientLayer, at: 0)
    }
}
