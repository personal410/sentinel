//
//  DetalleMesTerceroDetalladoViewController.swift
//  Sentinel
//
//  Created by Daniel on 20/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class DetalleMesTerceroDetalladoViewController:ParentViewController, EnviarPDFHUDViewDelegate {
    //MARK: - Outlets
    @IBOutlet weak var toolbarDetalle:UIToolbar!
    @IBOutlet weak var vInfo:UIView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblDocument:UILabel!
    //MARK: - Variables
    var enviarPDF = false
    let tercero = UserGlobalData.sharedInstance.terceroGlobal!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        UserDefaults.standard.set(11, forKey: "check")
        let dic = tercero.InfoTitular!["SDTCPTMS"] as! NSDictionary
        let tipoDoc = dic.object(forKey: "TipoDocumento") as! String
        let nroDoc =  dic.object(forKey: "NroDocumento") as! String
        var nombreCompleto = ""
        var documento = ""
        if tipoDoc == "D" {
            let nombre = dic.object(forKey: "Nombre") as! String
            let apePat = dic.object(forKey: "ApePaterno") as! String
            let apeMat = dic.object(forKey: "ApeMaterno") as! String
            nombreCompleto = "\(apePat) \(apeMat) \(nombre)"
            documento = "DNI: \(nroDoc)"
        }else if tipoDoc == "R" {
            nombreCompleto = dic.object(forKey: "NomRazSoc") as! String
            documento = "RUC: \(nroDoc)"
        }
        lblName.text = nombreCompleto
        lblDocument.text = documento
    }
    override func viewDidLayoutSubviews() {
        validarPremium()
    }
    //MARK: - Auxiliar
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(uiView: toolbarDetalle)
            setGradient(uiView: vInfo)
        }
    }
    func iniGenerarPDF(){
        self.showActivityIndicator()
        let userData = UserGlobalData.sharedInstance.userGlobal!
        let usua = userData.user!
        let sesion = userData.SesionId!
        var numDoc = ""
        var tdoc : String = ""//userData.TDocusu!
        if (tercero.InfoTitular!["SDTCPTMS"] as! NSDictionary).object(forKey: "TipoDocumento") as! String == "D"{
            numDoc = (tercero.InfoTitular!["SDTCPTMS"] as! NSDictionary).object(forKey: "NroDocumento") as! String
            tdoc = "D"
        }else{
            numDoc = (tercero.InfoTitular!["SDTCPTMS"] as! NSDictionary).object(forKey: "NDocRel") as! String
            tdoc = "R"
        }
        let parametros = ["TDocUsu" : tdoc, "Usuario" : usua, "SesionId" : sesion, "Servicio" : userData.NroSerCT!, "TipoDocumento" : tdoc, "NroDocumento" : numDoc, "TipoReporte" : "D", "EnvioMail" : enviarPDF ? "S" : "N"] as [String : Any]
        NotificationCenter.default.addObserver(self, selector: #selector(self.endGenerarPDF(_:)), name: NSNotification.Name("endGenerarPDF"), object: nil)
        OriginData.sharedInstance.generarPDF(notification: "endGenerarPDF", parametros: parametros)
    }
    
    @objc func endGenerarPDF(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        guard notification.object != nil else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if let error = notification.object as? NSError {
            let message = error.code == -1001 ? "Tiempo de espera agotado." : "En estos momentos no es posible realizar la consulta."
            let alerta = UIAlertController(title: "ALERTA", message: message, preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        let data = notification.object as? UserClass
        if data?.CodigoWS == "0"{
            if enviarPDF {
                self.showAlert(PATHS.SENTINEL, mensaje: "En breve le estará llegando a su correo el reporte en PDF.")
            }else{
                showPDF(from: data?.urlPDF)
            }
        }else{
            if data?.CodigoWS != nil {
                if data?.CodigoWS == "99"
                {
                    showError99()
                }
                else
                {
                    let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: data?.MensajeRespuesta, preferredStyle: .alert)
                    let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                    alertaVersion.addAction(actionVersion)
                    self.present(alertaVersion, animated: true, completion: nil)
                }
            }
        }
    }
    //MARK: - Actions
    @IBAction func accionPDF(_ sender: Any){
        EnviarPDFHUDView.showEnviarPDFViewWithDelegate(self, with: "Reporte Completo PDF")
    }
    @IBAction func accionAtras(_ sender:Any){
        navigationController?.popViewController(animated: true)
    }
    //MARK: - EnviarPDF
    func enviarPDFHUDDidConfirmWith(option index: Int) {
        enviarPDF = index == 1
        iniGenerarPDF()
    }
}
