//
//  DetalleXLTerceroDetalle.swift
//  Sentinel
//
//  Created by Daniel on 20/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import XLPagerTabStrip
class DetalleXLTerceroDetalle: ButtonBarPagerTabStripViewController, LoaderVC {
    var cargandoMesSeleccionado = true
    var mesDefault:Int = UserDefaults.standard.object(forKey: "mesSemaforo") as! Int
    
    @IBOutlet weak var barBB: ButtonBarView!
    var arrayMesesController = NSMutableArray()
    var arrayMeses = NSMutableArray()
    var esPremium = false
    var EneroViewController : Enero2018ViewController!
    var FebreroViewController : Febrero2018ViewController!
    var MarzoViewController : Marzo2018ViewController!
    var AbrilViewController : Abril2018ViewController!
    var MayoViewController : Mayo2018ViewController!
    var JunioViewController : Junio2018ViewController!
    var JulioViewController : Julio2018ViewController!
    var AgostoViewController : Agosto2018ViewController!
    var SetiembreViewController : Setiembre2018ViewController!
    var OctubreViewController : Octubre2018ViewController!
    var NoviembreViewController : Noviembre2018ViewController!
    var DiciembreViewController : Diciembre2018ViewController!
    
    override func viewDidLoad(){
        esPremium = UserGlobalData.sharedInstance.userGlobal.EsPremium == "S"
        self.estilos()
    }
    override func viewDidAppear(_ animated:Bool){
        buttonBarView.selectItem(at: IndexPath(item: mesDefault, section: 0), animated: true, scrollPosition: [])
        buttonBarView.moveTo(index: mesDefault, animated: true, swipeDirection: .right, pagerScroll: .yes)
        if mesDefault > 0 {
            moveToViewController(at: mesDefault, animated: true)
        }
    }
    override func viewDidLayoutSubviews(){
        print("viewDidLayoutSubviews")
    }
    func estilos(){
        if esPremium {
            setGradient(uiView: barBB)
        }
        settings.style.buttonBarHeight = 44.0
        settings.style.buttonBarBackgroundColor = !esPremium ? UIColor.fromHex(0x06B150) : UIColor.clear
        settings.style.buttonBarItemBackgroundColor = UIColor.clear
        settings.style.selectedBarBackgroundColor =  UIColor.fromHex(0xffffff)
        settings.style.buttonBarItemTitleColor = esPremium ? UIColor(white: 1, alpha: 0.5) : UIColor.fromHex(0x137d3d)
        settings.style.buttonBarItemFont = UIFont(name: "HelveticaNeue-Bold", size:12) ?? UIFont.systemFont(ofSize: 12)
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 5
        settings.style.buttonBarRightContentInset = 5
        pagerBehaviour = PagerTabStripBehaviour.common(skipIntermediateViewControllers: true)
        changeCurrentIndex = {(oldCell:ButtonBarViewCell?, newCell:ButtonBarViewCell?, animated:Bool) -> Void in
            oldCell?.label.textColor = self.esPremium ? UIColor(white: 1, alpha: 0.5) : UIColor.fromHex(0x137d3d)
            newCell?.label.textColor = UIColor.fromHex(0xffffff)
        }
        changeCurrentIndexProgressive = {(oldCell:ButtonBarViewCell?, newCell:ButtonBarViewCell?, progressPercentage:CGFloat, changeCurrentIndex: Bool, animated:Bool) -> Void in
            oldCell?.label.textColor = self.esPremium ? UIColor(white: 1, alpha: 0.5) : UIColor.fromHex(0x137d3d)
            newCell?.label.textColor = UIColor.fromHex(0xffffff)
        }
        super.viewDidLoad()
    }
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        self.arrayMeses = UserGlobalData.sharedInstance.userGlobal.mesesArreglo
        let storyboardMain = UIStoryboard(name: "Consultas", bundle: nil)
        EneroViewController = storyboardMain.instantiateViewController(withIdentifier: "EneroVC") as? Enero2018ViewController
        EneroViewController.parentVC = self
        FebreroViewController = storyboardMain.instantiateViewController(withIdentifier: "FebreroVC") as? Febrero2018ViewController
        FebreroViewController.parentVC = self
        MarzoViewController = storyboardMain.instantiateViewController(withIdentifier: "MarzoVC") as? Marzo2018ViewController
        MarzoViewController.parentVC = self
        AbrilViewController = storyboardMain.instantiateViewController(withIdentifier: "AbrilVC") as? Abril2018ViewController
        AbrilViewController.parentVC = self
        MayoViewController = storyboardMain.instantiateViewController(withIdentifier: "MayoVC") as? Mayo2018ViewController
        MayoViewController.parentVC = self
        JunioViewController = storyboardMain.instantiateViewController(withIdentifier: "JunioVC") as? Junio2018ViewController
        JunioViewController.parentVC = self
        JulioViewController = storyboardMain.instantiateViewController(withIdentifier: "JulioVC") as? Julio2018ViewController
        JulioViewController.parentVC = self
        AgostoViewController = storyboardMain.instantiateViewController(withIdentifier: "AgostoVC") as? Agosto2018ViewController
        AgostoViewController.parentVC = self
        SetiembreViewController = storyboardMain.instantiateViewController(withIdentifier: "SetiembreVC") as? Setiembre2018ViewController
        SetiembreViewController.parentVC = self
        OctubreViewController = storyboardMain.instantiateViewController(withIdentifier: "OctubreVC") as? Octubre2018ViewController
        OctubreViewController.parentVC = self
        NoviembreViewController = storyboardMain.instantiateViewController(withIdentifier: "NoviembreVC") as? Noviembre2018ViewController
        NoviembreViewController.parentVC = self
        DiciembreViewController = storyboardMain.instantiateViewController(withIdentifier: "DiciembreVC") as? Diciembre2018ViewController
        DiciembreViewController.parentVC = self
        for i in 0 ... arrayMeses.count - 1{
            let objMes = arrayMeses[i] as! NSDictionary
            let numMes = objMes.object(forKey: "mes") as! String
            if numMes == "1"{
                arrayMesesController.add(EneroViewController)
            }else if numMes == "2"{
                arrayMesesController.add(FebreroViewController)
            }
            else if numMes == "3"{
                arrayMesesController.add(MarzoViewController)
            }
            else if numMes == "4"{
                arrayMesesController.add(AbrilViewController)
            }
            else if numMes == "5"{
                arrayMesesController.add(MayoViewController)
            }
            else if numMes == "6"{
                arrayMesesController.add(JunioViewController)
            }
            else if numMes == "7"{
                arrayMesesController.add(JulioViewController)
            }
            else if numMes == "8"{
                arrayMesesController.add(AgostoViewController)
            }
            else if numMes == "9"{
                arrayMesesController.add(SetiembreViewController)
            }else if numMes == "10"{
                arrayMesesController.add(OctubreViewController)
            }else if numMes == "11"{
                arrayMesesController.add(NoviembreViewController)
            }else if numMes == "12"{
                arrayMesesController.add(DiciembreViewController)
            }
        }
        let swiftArray = arrayMesesController as AnyObject as! [AnyObject]
        let objSwift0 = swiftArray[0]
        let objSwift1 = swiftArray[1]
        let objSwift2 = swiftArray[2]
        let objSwift3 = swiftArray[3]
        let objSwift4 = swiftArray[4]
        let objSwift5 = swiftArray[5]
        let objSwift6 = swiftArray[6]
        let objSwift7 = swiftArray[7]
        let objSwift8 = swiftArray[8]
        let objSwift9 = swiftArray[9]
        let objSwift10 = swiftArray[10]
        let objSwift11 = swiftArray[11]
        self.arrayMesesController = NSMutableArray()
        self.arrayMesesController.add(objSwift11)
        self.arrayMesesController.add(objSwift10)
        self.arrayMesesController.add(objSwift9)
        self.arrayMesesController.add(objSwift8)
        self.arrayMesesController.add(objSwift7)
        self.arrayMesesController.add(objSwift6)
        self.arrayMesesController.add(objSwift5)
        self.arrayMesesController.add(objSwift4)
        self.arrayMesesController.add(objSwift3)
        self.arrayMesesController.add(objSwift2)
        self.arrayMesesController.add(objSwift1)
        self.arrayMesesController.add(objSwift0)
        return arrayMesesController as! [UIViewController]
    }
    func setGradient(uiView: UIView) {
        let colorBottom =  UIColor(red: 239.0/255.0, green: 212.0/255.0, blue: 80.0/255.0, alpha: 1.0).cgColor
        let colorTop = UIColor(red: 209.0/255.0, green: 142.0/255.0, blue: 24.0/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.startPoint = CGPoint(x:0.0, y:0.5)
        gradientLayer.endPoint = CGPoint(x:1.0, y:0.5)
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = uiView.bounds
        uiView.layer.insertSublayer(gradientLayer, at: 0)
    }
}
