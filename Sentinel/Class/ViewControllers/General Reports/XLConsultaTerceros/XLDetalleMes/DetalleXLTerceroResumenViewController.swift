//
//  DetalleXLTerceroResumenViewController.swift
//  Sentinel
//
//  Created by Daniel on 4/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//DetalleXLTerceroResumenViewController
import XLPagerTabStrip
class DetalleXLTerceroResumenViewController: ButtonBarPagerTabStripViewController, LoaderVC {
    var cargandoMesSeleccionado = true
    var mesDefault:Int = UserDefaults.standard.object(forKey: "mesSemaforo") as! Int
    
    @IBOutlet weak var barBB: ButtonBarView!
    
    var arrayMesesController: NSMutableArray = NSMutableArray()
    var arrayMeses: NSMutableArray = NSMutableArray()
    var tercero : TerceroClass!
    var ParentVC: UIViewController = UIViewController()
    
    var EneroViewController : Enero2018ViewController!
    var FebreroViewController : Febrero2018ViewController!
    var MarzoViewController : Marzo2018ViewController!
    var AbrilViewController : Abril2018ViewController!
    var MayoViewController : Mayo2018ViewController!
    var JunioViewController : Junio2018ViewController!
    var JulioViewController : Julio2018ViewController!
    var AgostoViewController : Agosto2018ViewController!
    var SetiembreViewController : Setiembre2018ViewController!
    var OctubreViewController : Octubre2018ViewController!
    var NoviembreViewController : Noviembre2018ViewController!
    var DiciembreViewController : Diciembre2018ViewController!
    
    var isReload = false
    var userData : UserClass!
    let graySpotifyColor = UIColor.fromHex(0x1A1A1A)
    var tabActivo: Int = 1
    
    override func viewDidLoad(){
        self.inicializarVariables()
        self.estilos()
    }
    override func viewDidAppear(_ animated: Bool) {
        if mesDefault - 9 > -1 {
            moveToViewController(at: mesDefault - 9, animated: false)
        }
    }
    func inicializarVariables(){
        self.tercero = UserGlobalData.sharedInstance.terceroGlobal
    }
    func estilos(){
        userData = UserGlobalData.sharedInstance.userGlobal
        
        // change selected bar color
        settings.style.buttonBarBackgroundColor = UIColor.fromHex(0x0072BE)
        settings.style.buttonBarHeight = 44.0
        settings.style.buttonBarItemBackgroundColor = UIColor.clear
        settings.style.selectedBarBackgroundColor =  UIColor.fromHex(0xffffff)
        settings.style.buttonBarItemFont = UIFont(name: "HelveticaNeue-Bold", size:12) ??
            UIFont.systemFont(ofSize: 12)
        
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor.clear
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        
        settings.style.buttonBarLeftContentInset = 5
        settings.style.buttonBarRightContentInset = 5
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.fromHex(0xffffff)
            newCell?.label.textColor = UIColor.fromHex(0xffffff)
        }
        
        super.viewDidLoad()
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        self.arrayMeses = UserGlobalData.sharedInstance.userGlobal.mesesArreglo
        let storyboardMain = UIStoryboard(name: "Consultas", bundle: nil)
        
        EneroViewController = (storyboardMain.instantiateViewController(withIdentifier: "EneroVC") as! Enero2018ViewController)
        EneroViewController.parentVC = self
        FebreroViewController = (storyboardMain.instantiateViewController(withIdentifier: "FebreroVC") as! Febrero2018ViewController)
        FebreroViewController.parentVC = self
        MarzoViewController = (storyboardMain.instantiateViewController(withIdentifier: "MarzoVC") as! Marzo2018ViewController)
        MarzoViewController.parentVC = self
        AbrilViewController = (storyboardMain.instantiateViewController(withIdentifier: "AbrilVC") as! Abril2018ViewController)
        AbrilViewController.parentVC = self
        MayoViewController = (storyboardMain.instantiateViewController(withIdentifier: "MayoVC") as! Mayo2018ViewController)
        MayoViewController.parentVC = self
        JunioViewController = (storyboardMain.instantiateViewController(withIdentifier: "JunioVC") as! Junio2018ViewController)
        JunioViewController.parentVC = self
        JulioViewController = (storyboardMain.instantiateViewController(withIdentifier: "JulioVC") as! Julio2018ViewController)
        JulioViewController.parentVC = self
        AgostoViewController = (storyboardMain.instantiateViewController(withIdentifier: "AgostoVC") as! Agosto2018ViewController)
        AgostoViewController.parentVC = self
        SetiembreViewController = (storyboardMain.instantiateViewController(withIdentifier: "SetiembreVC") as! Setiembre2018ViewController)
        SetiembreViewController.parentVC = self
        OctubreViewController = (storyboardMain.instantiateViewController(withIdentifier: "OctubreVC") as! Octubre2018ViewController)
        OctubreViewController.parentVC = self
        NoviembreViewController = (storyboardMain.instantiateViewController(withIdentifier: "NoviembreVC") as! Noviembre2018ViewController)
        NoviembreViewController.parentVC = self
        DiciembreViewController = (storyboardMain.instantiateViewController(withIdentifier: "DiciembreVC") as! Diciembre2018ViewController)
        DiciembreViewController.parentVC = self
        for i in 0 ... arrayMeses.count - 1{
            let objMes : NSDictionary = arrayMeses[i] as! NSDictionary
            let numMes : String = objMes.object(forKey: "mes") as! String
            if numMes  == "1"{
                arrayMesesController.add(EneroViewController)
            }
            else if numMes == "2"{
                arrayMesesController.add(FebreroViewController)
            }
            else if numMes == "3"{
                arrayMesesController.add(MarzoViewController)
            }
            else if numMes == "4"{
                arrayMesesController.add(AbrilViewController)
            }
            else if numMes == "5"{
                arrayMesesController.add(MayoViewController)
            }
            else if numMes == "6"{
                arrayMesesController.add(JunioViewController)
            }
            else if numMes == "7"{
                arrayMesesController.add(JulioViewController)
            }
            else if numMes == "8"{
                arrayMesesController.add(AgostoViewController)
            }
            else if numMes == "9"{
                arrayMesesController.add(SetiembreViewController)
            }
            else if numMes == "10"{
                arrayMesesController.add(OctubreViewController)
            }
            else if numMes == "11"{
                arrayMesesController.add(NoviembreViewController)
            }
            else if numMes == "12"{
                arrayMesesController.add(DiciembreViewController)
            }
        }
        
        //var nsArray = NSMutableArray()
        let swiftArray = arrayMesesController as AnyObject as! [AnyObject]
        let objSwift0 = swiftArray[0]
        let objSwift1 = swiftArray[1]
        let objSwift2 = swiftArray[2]
        
        self.arrayMesesController = NSMutableArray()
        self.arrayMesesController.add(objSwift2)
        self.arrayMesesController.add(objSwift1)
        self.arrayMesesController.add(objSwift0)
        
        let nsArray = NSArray.init(array: self.arrayMesesController)
        
        guard isReload else {
            return [nsArray[0] as! UIViewController, nsArray[1] as! UIViewController,nsArray[2] as! UIViewController]
        }
        let childViewControllers = [(nsArray[0] as! UIViewController), (nsArray[1] as! UIViewController),(nsArray[2] as! UIViewController)] as [UIViewController]
        let nItems = 3 + (arc4random() % 8)
        return Array(childViewControllers.prefix(Int(nItems)))
    }
    func setGradient(uiView: UIView) {
        
        let colorBottom =  UIColor(red: 239.0/255.0, green: 212.0/255.0, blue: 80.0/255.0, alpha: 1.0).cgColor
        let colorTop = UIColor(red: 209.0/255.0, green: 142.0/255.0, blue: 24.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.startPoint = CGPoint(x:0.0, y:0.5)
        gradientLayer.endPoint = CGPoint(x:1.0, y:0.5)
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = uiView.bounds
        
        uiView.layer.insertSublayer(gradientLayer, at: 0)
    }
}
