//
//  MiReporteTerceroDetalladoViewController.swift
//  Sentinel
//
//  Created by Daniel on 20/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import XLPagerTabStrip
class MiReporteTerceroDetalladoViewController:ParentViewController,  IndicatorInfoProvider {
    @IBOutlet weak var topFechaNac: NSLayoutConstraint!
    @IBOutlet weak var alturaFechaNac: NSLayoutConstraint!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var nombreLB: UILabel!
    @IBOutlet weak var fechaNacimientoLB: UILabel!
    @IBOutlet weak var nroDocumentoLB: UILabel!
    @IBOutlet weak var quieroBT: UIButton!
    @IBOutlet weak var reporteBT: UIButton!
    @IBOutlet weak var signoDeudaLB: UILabel!
    @IBOutlet weak var semaforoActual: UIImageView!
    @IBOutlet weak var deudaTotalLB: UILabel!
    @IBOutlet weak var indicadoresCollection: UICollectionView!
    @IBOutlet weak var semaforosCollection: UICollectionView!
    @IBOutlet weak var controlTermometro: SFControlTermometro!
    @IBOutlet weak var sinCreditos: UILabel!
    
    @IBOutlet weak var vSinScore:UIView!
    @IBOutlet weak var vConScore:UIView!
    @IBOutlet weak var ivSemaforoActualSinScore:UIImageView!
    @IBOutlet weak var lblMonedaSinScore:UILabel!
    @IBOutlet weak var lblDeudaSinScore:UILabel!
    
    var usuarioBean = UserClass(userbean: [:])
    var terceroBean:TerceroClass!
    var creditosArray = NSArray()
    var imgArray = [UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializarVariables()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.semaforosCollection.scrollToItem(at: IndexPath(item: 11, section: 0), at: .right, animated: false)
        self.cargarDatos()
    }
    
    func inicializarVariables(){
        viewImage.layer.cornerRadius = image.frame.size.width / 2
        viewImage.clipsToBounds = true
        usuarioBean = UserGlobalData.sharedInstance.userGlobal
        terceroBean = UserGlobalData.sharedInstance.terceroGlobal
    }
    func cargarDatos(){
        let SDTCPTMS = terceroBean?.InfoTitular!["SDTCPTMS"] as! NSDictionary
        let tipoDoc = SDTCPTMS.object(forKey: "TipoDocumento") as! String
        if tipoDoc == "D" || tipoDoc == "5" {
            //Cargar imagen
            if let imageMain = getSavedImage(named: "fileNameTercero.png") {
                self.image.image = imageMain
            }
            //Nombre Completo
            let nombre : String = SDTCPTMS.object(forKey: "Nombre") as! String
            let apePat : String = SDTCPTMS.object(forKey: "ApePaterno") as! String
            let apeMat : String = SDTCPTMS.object(forKey: "ApeMaterno") as! String
            let nombreCompleto : String = "\(apePat) \(apeMat)\n\(nombre)"
            self.nombreLB.text = nombreCompleto
            
            //Fecha
            let fechaSinFormat = SDTCPTMS.object(forKey: "FechaNacimiento") as! String
            if fechaSinFormat.isEmpty || fechaSinFormat == "0000-00-00" {
                self.fechaNacimientoLB.text = ""
                self.fechaNacimientoLB.isHidden = true
                self.topFechaNac.constant = 0
                self.alturaFechaNac.constant = 0
            }else{
                self.fechaNacimientoLB.text = "Fecha Nac. \(fechaSinFormat)"
            }
            
            //FechaProceso
            UserGlobalData.sharedInstance.terceroGlobal.FechaReporte = SDTCPTMS.object(forKey: "FechaProceso") as! String
            
            //DNI
            let dni : String =  SDTCPTMS.object(forKey: "NroDocumento") as! String
            UserGlobalData.sharedInstance.terceroGlobal.documentoTercero = dni
            
            if dni.count == 8 {
                self.nroDocumentoLB.text = "DNI: \(dni)"
                UserGlobalData.sharedInstance.terceroGlobal.tipoDocumentoTercero = "D"
            }
            else if dni.count == 11 {
                self.nroDocumentoLB.text = "RUC: \(dni)"
                UserGlobalData.sharedInstance.terceroGlobal.tipoDocumentoTercero = "R"
            }
            
            //Semaforo
            let semaforoActivo = SDTCPTMS.object(forKey: "SemActual") as! String
            
            
            var mostrarScore = "S"
            if let infoTit = terceroBean.InfoTitular, let mostScore = infoTit["MostrarScore"] as? String {
                mostrarScore = mostScore
            }
            if mostrarScore == "S" {
                vConScore.isHidden = false
                vSinScore.isHidden = true
                self.semaforoActual.image = devuelveSemaforo(SemAct: semaforoActivo)
                self.cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: self.deudaTotalLB)
                self.cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: self.signoDeudaLB)
                //Deuda
                let deuda : String = SDTCPTMS.object(forKey: "DeudaTotal") as! String
                if deuda == "0.00"{
                    self.deudaTotalLB.text = deuda
                }else{
                    let newDeuda = NSNumber( value: Double(deuda)!)
                    let numberFormatter = NumberFormatter()
                    numberFormatter.formatterBehavior = .behavior10_4
                    numberFormatter.numberStyle = .decimal
                    let numberString = numberFormatter.string(from: newDeuda)
                    self.deudaTotalLB.text = numberString
                }
                let dictSentinel = terceroBean?.InfoTitular!["ScoreSentinel"] as! NSDictionary
                let ScoreSentinel = dictSentinel.mutableCopy() as! NSMutableDictionary
                self.controlTermometro.formatoTermometro(ScoreSentinel)
            }else{
                vConScore.isHidden = true
                vSinScore.isHidden = false
                ivSemaforoActualSinScore.image = devuelveSemaforo(SemAct: semaforoActivo)
                cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: lblMonedaSinScore)
                cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: lblDeudaSinScore)
                let deuda : String = SDTCPTMS.object(forKey: "DeudaTotal") as! String
                if deuda == "0.00"{
                    self.lblDeudaSinScore.text = deuda
                }else{
                    let newDeuda = NSNumber( value: Double(deuda)!)
                    let numberFormatter = NumberFormatter()
                    numberFormatter.formatterBehavior = .behavior10_4
                    numberFormatter.numberStyle = .decimal
                    let numberString = numberFormatter.string(from: newDeuda)
                    self.lblDeudaSinScore.text = numberString
                }
            }
            
            //indicadores
            let indicador : [NSDictionary] = terceroBean?.InfoTitular!["SDTIndicadoresCPT"] as! [NSDictionary]
            self.creditosArray = indicador as NSArray
            if self.creditosArray.count == 0 {
                self.sinCreditos.isHidden = false
            }
            self.indicadoresCollection.reloadData()
            
            let sema = SDTCPTMS.object(forKey: "Semaforos") as! String
            if (sema != "" && sema.count == 12){
                imgArray = devuelveArrayIMGSemaforos(semaforos: sema) as! [UIImage?]
            }else{
                let semaDefault = "GGGGGGGGGGGG"
                imgArray = devuelveArrayIMGSemaforos(semaforos: semaDefault) as! [UIImage?]
            }
            self.semaforosCollection.reloadData()
        }else if tipoDoc == "R" {
            self.image.image = UIImage.init(named: "IconoEmpresaF")
            
            //Nombre Completo
            let nombreCompleto = SDTCPTMS.object(forKey: "NomRazSoc") as! String
            self.nombreLB.text = nombreCompleto
            
            //Fecha
            self.fechaNacimientoLB.isHidden = true
            self.topFechaNac.constant = 0
            self.alturaFechaNac.constant = 0
            
            //FechaProceso
            UserGlobalData.sharedInstance.terceroGlobal.FechaReporte = SDTCPTMS.object(forKey: "FechaProceso") as! String
            
            //DNI
            let dni : String =  SDTCPTMS.object(forKey: "NroDocumento") as! String
            UserGlobalData.sharedInstance.terceroGlobal.documentoTercero = dni
            
            if dni.count == 8 {
                self.nroDocumentoLB.text = "DNI: \(dni)"
                UserGlobalData.sharedInstance.terceroGlobal.tipoDocumentoTercero = "D"
            }
            else if dni.count == 11 {
                self.nroDocumentoLB.text = "RUC: \(dni)"
                UserGlobalData.sharedInstance.terceroGlobal.tipoDocumentoTercero = "R"
            }
            
            let semaforoActivo = SDTCPTMS.object(forKey: "SemActual") as! String
            var mostrarScore = "S"
            if let infoTit = terceroBean.InfoTitular, let mostScore = infoTit["MostrarScore"] as? String {
                mostrarScore = mostScore
            }
            if mostrarScore == "S" {
                vConScore.isHidden = false
                vSinScore.isHidden = true
                self.semaforoActual.image = devuelveSemaforo(SemAct: semaforoActivo)
                self.cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: self.deudaTotalLB)
                self.cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: self.signoDeudaLB)
                //Deuda
                let deuda : String = SDTCPTMS.object(forKey: "DeudaTotal") as! String
                if deuda == "0.00"{
                    self.deudaTotalLB.text = deuda
                }else{
                    let newDeuda = NSNumber( value: Double(deuda)!)
                    let numberFormatter = NumberFormatter()
                    numberFormatter.formatterBehavior = .behavior10_4
                    numberFormatter.numberStyle = .decimal
                    let numberString = numberFormatter.string(from: newDeuda)
                    self.deudaTotalLB.text = numberString
                }
                let dictSentinel = terceroBean?.InfoTitular!["ScoreSentinel"] as! NSDictionary
                let ScoreSentinel = dictSentinel.mutableCopy() as! NSMutableDictionary
                self.controlTermometro.formatoTermometro(ScoreSentinel)
            }else{
                vConScore.isHidden = true
                vSinScore.isHidden = false
                ivSemaforoActualSinScore.image = devuelveSemaforo(SemAct: semaforoActivo)
                cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: lblMonedaSinScore)
                cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: lblDeudaSinScore)
                let deuda : String = SDTCPTMS.object(forKey: "DeudaTotal") as! String
                if deuda == "0.00"{
                    self.lblDeudaSinScore.text = deuda
                }else{
                    let newDeuda = NSNumber( value: Double(deuda)!)
                    let numberFormatter = NumberFormatter()
                    numberFormatter.formatterBehavior = .behavior10_4
                    numberFormatter.numberStyle = .decimal
                    let numberString = numberFormatter.string(from: newDeuda)
                    self.lblDeudaSinScore.text = numberString
                }
            }
            
            //indicadores
            let indicador : [NSDictionary] = terceroBean?.InfoTitular!["SDTIndicadoresCPT"] as! [NSDictionary]
            self.creditosArray = indicador as NSArray
            if self.creditosArray.count == 0 {
                self.sinCreditos.isHidden = false
            }
            self.indicadoresCollection.reloadData()
            
            //semaforo últimos meses
            let sema : String = SDTCPTMS.object(forKey: "Semaforos") as! String
            if (sema != "" && sema.count == 12){
                //Parse arreglo imgArray
                imgArray = devuelveArrayIMGSemaforos(semaforos: sema) as! [UIImage?]
                self.semaforosCollection.reloadData()
            }
            else
            {
                let semaDefault : String = "GGGGGGGGGGGG"
                imgArray = devuelveArrayIMGSemaforos(semaforos: semaDefault) as! [UIImage?]
                self.semaforosCollection.reloadData()
            }
        }
    }
    
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        
        return IndicatorInfo(title: "REPORTE", image: UIImage(named: ""))
    }
}

extension MiReporteTerceroDetalladoViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.semaforosCollection {
            return imgArray.count
        }
        else if collectionView == self.indicadoresCollection{
            return creditosArray.count
        }
        return imgArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.semaforosCollection {
            let identifier = "Cell"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! imageCollectionViewCell
            cell.imgIV.image = imgArray[indexPath.row]
            let arregloMesDesc = self.devuelveArregloUltimosMeses(fechaUltima: UserGlobalData.sharedInstance.terceroGlobal.FechaReporte)
            var swiftArray = arregloMesDesc as AnyObject as! [AnyObject]
            let objSwift0 = swiftArray[0]
            let objSwift1 = swiftArray[1]
            let objSwift2 = swiftArray[2]
            let objSwift3 = swiftArray[3]
            let objSwift4 = swiftArray[4]
            let objSwift5 = swiftArray[5]
            let objSwift6 = swiftArray[6]
            let objSwift7 = swiftArray[7]
            let objSwift8 = swiftArray[8]
            let objSwift9 = swiftArray[9]
            let objSwift10 = swiftArray[10]
            let objSwift11 = swiftArray[11]
            
            let arrayMesesController = NSMutableArray()
            arrayMesesController.add(objSwift11)
            arrayMesesController.add(objSwift10)
            arrayMesesController.add(objSwift9)
            arrayMesesController.add(objSwift8)
            arrayMesesController.add(objSwift7)
            arrayMesesController.add(objSwift6)
            arrayMesesController.add(objSwift5)
            arrayMesesController.add(objSwift4)
            arrayMesesController.add(objSwift3)
            arrayMesesController.add(objSwift2)
            arrayMesesController.add(objSwift1)
            arrayMesesController.add(objSwift0)
            
            let objMes : NSDictionary = arrayMesesController[indexPath.row] as! NSDictionary
            let nombre : String = objMes.object(forKey: "mes") as! String
            let anio : String = objMes.object(forKey: "anio") as! String
            let nombreMes : String = self.devuelveNombreMesParticionado(mes: nombre)
            cell.mesLB.text = "\(nombreMes)\n\(anio)"
            
            return cell
        }
        else if collectionView == self.indicadoresCollection {
            
            let identifier = "CreditosCVC"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! CreditosCollectionViewCell
            
            let objIndicador : NSDictionary = creditosArray[indexPath.row] as! NSDictionary
            let codigo : String = "\((objIndicador.object(forKey: "CICodInd"))!)"
            
            cell.aimagenCreditos.image =  self.devuelveCreditosIndicadores(cred: codigo)
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var widthItem  = 30
        var heighItem = 30
        
        if collectionView == self.semaforosCollection {
            widthItem = 45
            heighItem = 60
        }
        else if collectionView == self.indicadoresCollection{
            widthItem  = 30
            heighItem = 40
        }
        return CGSize(width: widthItem, height: heighItem)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.semaforosCollection {
            UserGlobalData.sharedInstance.userGlobal.mesesArreglo = self.devuelveArregloUltimosMeses(fechaUltima: UserGlobalData.sharedInstance.terceroGlobal.FechaReporte)
            UserDefaults.standard.set(indexPath.row, forKey: "mesSemaforo")
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "DetalleMesTerceroDetalladoVC") as! DetalleMesTerceroDetalladoViewController
            self.navigationController?.pushViewController(nav, animated: true)
        }else if collectionView == indicadoresCollection {
            if let parentVc = self.parent as? ReporteDetalladoXLBarPagerTabStripViewController {
                parentVc.moveToViewController(at: 2)
            }
        }
    } 
}
