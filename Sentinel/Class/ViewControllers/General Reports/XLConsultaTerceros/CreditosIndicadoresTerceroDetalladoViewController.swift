//
//  CreditosIndicadoresTerceroDetalladoViewController.swift
//  Sentinel
//
//  Created by Daniel on 20/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
import XLPagerTabStrip

class CreditosIndicadoresTerceroDetalladoViewController:ParentViewController, IndicatorInfoProvider {
    @IBOutlet weak var creditosLB: UILabel!
    @IBOutlet weak var tableTV: UITableView!
    @IBOutlet weak var viewTable: UIView!
    
    var dataArray:  [NSDictionary] = [NSDictionary]()
    var dataNewArray : [NSDictionary] = [NSDictionary]()
    var ordenTopArray : [NSDictionary] = [NSDictionary]()
    var ordenBotArray : [NSDictionary] = [NSDictionary]()
    
    override func viewWillAppear(_ animated: Bool) {
        self.cargarDatos()
    }
    
    func cargarDatos(){
        self.ordenBotArray = [NSDictionary]()
        self.ordenTopArray = [NSDictionary]()
        if let Indicadires : NSArray = UserGlobalData.sharedInstance.userGlobal.Indicadores {
            self.dataArray = Indicadires as! [NSDictionary]
        }
        var indicador : [NSDictionary] = UserGlobalData.sharedInstance.terceroGlobal!.InfoTitular!["SDTIndicadoresCPT"] as! [NSDictionary]
        if indicador.count == 0 {
            self.tableTV.isHidden = true
            self.creditosLB.isHidden = false
            self.viewTable.isHidden = true
        }
        for i in 0 ... dataArray.count - 1 {
            let obj : NSDictionary = self.dataArray[i]
            if indicador.count > 0 {
                for y in 0 ... indicador.count - 1 {
                    let obj2 : NSDictionary = indicador[y]
                    let primer : String = String(obj.object(forKey: "CICodInd") as! Int)
                    let segundo : String = String(obj2.object(forKey: "CICodInd") as! Int)
                    if primer == segundo{
                        self.dataArray = self.rearrange(array: self.dataArray, fromIndex: i, toIndex: 0)
                    }
                }
            }
        }
        self.tableTV.reloadData()
    }
    
    func rearrange<T>(array: Array<T>, fromIndex: Int, toIndex: Int) -> Array<T>{
        var arr = array
        let element = arr.remove(at: fromIndex)
        arr.insert(element, at: toIndex)
        
        return arr
    }


    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        
        return IndicatorInfo(title: "CRÉDITOS E INDICADORES", image: UIImage(named: ""))
        
    }

}

extension CreditosIndicadoresTerceroDetalladoViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let tercero = UserGlobalData.sharedInstance.terceroGlobal!
        let identifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! CreditosIndicadoresTableViewCell
        //cell.descripcionLB.text = dataArray[indexPath.row]
        let objArray : NSDictionary = self.dataArray[indexPath.row]
        
        var indicador : [NSDictionary] = [NSDictionary]()
        
        indicador = tercero.InfoTitular!["SDTIndicadoresCPT"] as! [NSDictionary]
        
        cell.checkIV.image = nil
        let CICodInd : String = String(Int(objArray.object(forKey: "CICodInd") as! Int))
        let CICodIndDes : String = objArray.object(forKey: "CICodIndDes") as! String
        if indicador.count != 0 {
            let obj : NSDictionary = self.dataArray[indexPath.row]
            for y in 0 ... indicador.count - 1 {
                let obj2 : NSDictionary = indicador[y]
                let primer : String = String(obj.object(forKey: "CICodInd") as! Int)
                let segundo : String = String(obj2.object(forKey: "CICodInd") as! Int)
                if primer == segundo{
                    cell.checkIV.image = UIImage.init(named: "check.azul")
                }
            }
        }
        cell.iconoIV.image = self.devuelveCreditosIndicadores(cred: CICodInd)
        cell.descripcionLB.text = CICodIndDes
        return cell
    }
}
