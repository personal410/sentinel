//
//  ConsultasPremiumViewController.swift
//  Sentinel
//
//  Created by Daniel on 27/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//ConsultasPremiumViewController
import XLPagerTabStrip
class ConsultasPremiumViewController:ParentViewController {
    @IBOutlet weak var toolbarConsultas:UIToolbar!
    var cargarConsultasFlash = false
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.estilos()
    }
    override func viewWillAppear(_ animated:Bool){
        self.validarPremium()
    }
    override func viewDidAppear(_ animated: Bool) {
        if let viewCont = childViewControllers.first as? XLConsultasPremiumViewController, cargarConsultasFlash {
            viewCont.cargarConsultasFlash = true
        }
        cargarConsultasFlash = false
    }
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            let servicio = UserGlobalData.sharedInstance.misServiciosGlobal
            if servicio?.TipServ == "6" {
                cargarConsultasFlash = false
                navigationController?.popToRootViewController(animated: true)
            }
        }else{
            cargarConsultasFlash = false
            navigationController?.popToRootViewController(animated: true)
        }
    }
    func estilos(){
        self.setGradient(uiView: self.toolbarConsultas)
        self.changeStatusBar(cod: 3)
    }
}
