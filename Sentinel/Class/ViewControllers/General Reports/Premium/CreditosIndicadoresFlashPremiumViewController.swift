//
//  CreditosIndicadoresFlashPremiumViewController.swift
//  Sentinel
//
//  Created by Daniel on 28/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
import XLPagerTabStrip
class CreditosIndicadoresFlashPremiumViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, IndicatorInfoProvider {
    
    let dataArray = ["Crédito Vehicular", "Tarjeta Crédito", "Crédito Hipotecario", "No Hábido", "Es Aval", "Está Avalado", "Mancomunado", "Representante Legal", "REDAM"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CreditosIndicadoresTableViewCell
        cell.descripcionLB.text = dataArray[indexPath.row]
        return cell
    }
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "CRÉDITOS E INDICADORES", image: UIImage(named: ""))
    }
}
