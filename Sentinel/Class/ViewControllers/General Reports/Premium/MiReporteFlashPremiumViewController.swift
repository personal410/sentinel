//
//  MiReporteFlashPremiumViewController.swift
//  Sentinel
//
//  Created by Daniel on 28/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import XLPagerTabStrip
import Alamofire
class MiReporteFlashPremiumViewController:ParentViewController, IndicatorInfoProvider {
    //MARK: - Outlets
    @IBOutlet weak var topFechaNac: NSLayoutConstraint!
    @IBOutlet weak var alturaFechaNac: NSLayoutConstraint!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var semMedia: UIImageView!
    @IBOutlet weak var nroDocumentoLB: UILabel!
    @IBOutlet weak var fechaNacimientoLB: UILabel!
    @IBOutlet weak var nombreLB: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var semaforosCL: UICollectionView!
    @IBOutlet weak var creditosCL: UICollectionView!
    @IBOutlet weak var semaforoIMG: UIImageView!
    @IBOutlet weak var deudaLB: UILabel!
    @IBOutlet weak var sinCreditos: UILabel!
    @IBOutlet weak var btnDetallado:UIButton!
    @IBOutlet weak var lblDetallado:UILabel!
    @IBOutlet weak var btnResumido:UIButton!
    @IBOutlet weak var lblResumido:UILabel!
    //MARK: - Props
    let userGlobal = UserGlobalData.sharedInstance.userGlobal!
    var cantDetallados = 0, cantResumidos = 0
    var usuarioBean = UserClass(userbean: [:])
    var creditosArray = NSArray()
    var tipoDoc = "", nroDoc = "", tipoReporte = ""
    var imgArray = [UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris")]
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        viewImage.layer.cornerRadius = viewImage.frame.size.width / 2
        viewImage.clipsToBounds = true
        inicializarVariables()
        btnDetallado.titleLabel?.textAlignment = .center
        btnResumido.titleLabel?.textAlignment = .center
        cargarDatos()
    }
    override func viewWillAppear(_ animated:Bool){
        showActivityIndicator()
        let servicio = UserGlobalData.sharedInstance.misServiciosGlobal.NroServicio!
        let dicParams = ["Usuario": userGlobal.user!, "SesionId": userGlobal.SesionId!, "Servicio": servicio]
        ServiceConnector.connectToUrl("\(PATHS.PATHSENTINEL)RWS_MSDatosServicio", params: dicParams){(r, e) in
            self.hideActivityIndicator()
            if let dic = r as? [String: Any] {
                if let saldoServicio = dic["SaldoServicio"] as? [String:Any] {
                    self.cantDetallados = Int(saldoServicio["DispDet"] as? String ?? "0") ?? 0
                    self.cantResumidos = Int(saldoServicio["DispRes"] as? String ?? "0") ?? 0
                    self.lblDetallado.text = "\(self.cantDetallados) Disponible\(self.cantDetallados == 1 ? "" : "s")"
                    self.lblResumido.text = " \(self.cantResumidos) Disponible\(self.cantResumidos == 1 ? "" : "s")"
                    self.btnDetallado.setTitle(self.cantDetallados == 0 ? "Adquirir Reporte\nDetallado" : "Ver Reporte\nDetallado", for: .normal)
                    self.btnResumido.setTitle(self.cantResumidos == 0 ? "Adquirir Reporte\nResumido" : "Ver Reporte\nResumido", for: .normal)
                }
            }
        }
    }
    //MARK: - Actions
    @IBAction func verReporteDetallado(){
        if cantDetallados == 0 {
            navigationController?.pushViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ConfiguraProductoUnoVC"), animated: true)
        }else{
            let alertCont = UIAlertController(title: PATHS.SENTINEL, message: "Al consultar se le descontará un disponible.", preferredStyle: .alert)
            alertCont.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            alertCont.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action) in
                self.iniBusquedaDocumento(tipoConsulta: "D")
            }))
            present(alertCont, animated: true, completion: nil)
        }
    }
    @IBAction func verReporteResumido(){
        if cantResumidos == 0 {
            navigationController?.pushViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ConfiguraProductoDosVC"), animated: true)
        }else{
            let alertCont = UIAlertController(title: PATHS.SENTINEL, message: "Al consultar se le descontará un disponible.", preferredStyle: .alert)
            alertCont.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            alertCont.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action) in
                self.iniBusquedaDocumento(tipoConsulta: "R")
            }))
            present(alertCont, animated: true, completion: nil)
        }
    }
    //MARK: - Auxiliar
    func inicializarVariables(){
        usuarioBean = UserGlobalData.sharedInstance.userGlobal
        UserGlobalData.sharedInstance.userGlobal.tabIndicador = 1
        self.semaforosCL.dataSource = self
        self.semaforosCL.delegate = self
        self.creditosCL.dataSource = self
        self.creditosCL.delegate = self
    }
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    func cargarDatos(){
        let userData = UserGlobalData.sharedInstance.terceroGlobal
        let SDTCPTMS = userData?.InfoTitular!["SDTCPTMS"] as! NSDictionary
        tipoDoc = SDTCPTMS.object(forKey: "TipoDocumento") as! String
        nroDoc = SDTCPTMS.object(forKey: "NroDocumento") as! String
        if tipoDoc == "D" || tipoDoc == "5" {
            //Cargar imagen
            if let imageMain = getSavedImage(named: "fileNameTercero.png") {
                self.image.image = imageMain
            }
            //Nombre Completo
            let nombre : String = SDTCPTMS.object(forKey: "Nombre") as! String
            let apePat : String = SDTCPTMS.object(forKey: "ApePaterno") as! String
            let apeMat : String = SDTCPTMS.object(forKey: "ApeMaterno") as! String
            let nombreCompleto : String = "\(apePat) \(apeMat)\n\(nombre)"
            self.nombreLB.text = nombreCompleto
            
            //Fecha
            let fechaSinFormat = SDTCPTMS.object(forKey: "FechaNacimiento") as! String
            if fechaSinFormat.isEmpty || fechaSinFormat == "0000-00-00" {
                self.fechaNacimientoLB.text = ""
                self.topFechaNac.constant = 0
                self.alturaFechaNac.constant = 0
            }else{
                self.fechaNacimientoLB.text = "Fecha Nac. \(fechaSinFormat)"
            }
            //DNI
            self.nroDocumentoLB.text = "DNI: \(nroDoc)"
            
            //FechaProceso
            UserGlobalData.sharedInstance.terceroGlobal.FechaReporte = SDTCPTMS.object(forKey: "FechaProceso") as! String
            
            //Semaforo
            let semaforoActivo = SDTCPTMS.object(forKey: "SemActual") as! String

            self.semaforoIMG.image = self.devuelveSemaforo(SemAct: semaforoActivo)
            self.cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: self.deudaLB)

            //semaforo Media
            let semaforoActivoMedia : String = SDTCPTMS.object(forKey: "SemMit") as! String
            self.semMedia.image = self.devuelveSemaforo(SemAct: semaforoActivoMedia)
            
            //Deuda
            let deuda : String = SDTCPTMS.object(forKey: "DscDeuda") as! String

            
            self.deudaLB.text = deuda
            //indicadores
            let indicador : [NSDictionary] = userData?.InfoTitular!["SDTIndicadoresCPT"] as! [NSDictionary]
            self.creditosArray = indicador as NSArray
            if self.creditosArray.count == 0 {
                self.sinCreditos.isHidden = false
            }
            
            //semaforo últimos meses
            let sema = SDTCPTMS.object(forKey: "Semaforos") as! String
            if sema == "" || sema.count != 3 {
                imgArray = devuelveArrayIMGSemaforos(semaforos: "GGG") as! [UIImage?]
            }else{
                imgArray = devuelveArrayIMGSemaforos(semaforos: sema) as! [UIImage?]
            }
        }else if tipoDoc == "R" {
            _ = getSavedImage(named: "fileNameTercero.png")
            self.image.image = UIImage(named: "IconoEmpresaF")
            let nombre : String = SDTCPTMS.object(forKey: "NomRazSoc") as! String
            
            self.nombreLB.text = nombre
            
            //Fecha
            self.fechaNacimientoLB.text = ""
            self.topFechaNac.constant = 0
            self.alturaFechaNac.constant = 0
            self.nroDocumentoLB.text = "RUC: \(nroDoc)"
            //FechaProceso
            UserGlobalData.sharedInstance.terceroGlobal.FechaReporte = SDTCPTMS.object(forKey: "FechaProceso") as! String
            
            //Semaforo
            let semaforoActivo : String = SDTCPTMS.object(forKey: "SemActual") as! String
            self.semaforoIMG.image = self.devuelveSemaforo(SemAct: semaforoActivo)
            self.cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: self.deudaLB)
         
            //semaforo Media
            let semaforoActivoMedia : String = SDTCPTMS.object(forKey: "SemMit") as! String
            self.semMedia.image = self.devuelveSemaforo(SemAct: semaforoActivoMedia)
            
            //Deuda
            let deuda : String = SDTCPTMS.object(forKey: "DscDeuda") as! String
            if deuda == "0.00"{
                self.deudaLB.text = deuda
            }
            else
            {
                self.deudaLB.text = deuda 
            }
            //indicadores
            let indicador : [NSDictionary] = userData?.InfoTitular!["SDTIndicadoresCPT"] as! [NSDictionary]
            self.creditosArray = indicador as NSArray
            if self.creditosArray.count == 0 {
                self.sinCreditos.isHidden = false
            }
            
            //semaforo últimos meses
            let sema : String = SDTCPTMS.object(forKey: "Semaforos") as! String
            //Parse arreglo imgArray
            imgArray = devuelveArrayIMGSemaforos(semaforos: sema) as! [UIImage?]
            if sema == "" || sema.count != 3
            {
                let semaDefault : String = "GGG"
                imgArray = devuelveArrayIMGSemaforos(semaforos: semaDefault) as! [UIImage?]
            }
        }
        
        
        self.semaforosCL.reloadData()
        self.creditosCL.reloadData()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "REPORTE", image: UIImage(named: ""))
    }
    
    func iniBusquedaDocumento(tipoConsulta:String){
        tipoReporte = tipoConsulta
        showActivityIndicator()
        let servicios = UserGlobalData.sharedInstance.misServiciosGlobal!
        var servicio = userGlobal.NroSerCT ?? ""
        let nroServicio = servicios.NroServicio ?? ""
        if nroServicio != "0" {
            servicio = nroServicio
        }
        let parametros = ["Usuario": userGlobal.user!,
            "Servicio": servicio,
            "TipoDocumento": tipoDoc,
            "NroDocumento": nroDoc,
            "SesionId": userGlobal.SesionId ?? "",
            "origenAplicacion": PATHS.APP_ID,
            "TipoConsulta": tipoReporte] as [String: Any]
        NotificationCenter.default.addObserver(self, selector: #selector(self.endBusquedaDocumento(_:)), name: NSNotification.Name("endBusquedaDocumento"), object: nil)
        OriginData.sharedInstance.miReporteDNITercero(notification: "endBusquedaDocumento", parametros: parametros)
    }
    @objc func endBusquedaDocumento(_ notification:NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        guard notification.object != nil, let data = notification.object as? TerceroClass, let codigoWs = data.codigoWS else {
            hideActivityIndicator()
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if codigoWs == "0"{
            UserGlobalData.sharedInstance.terceroGlobal.documentoTercero = nroDoc
            UserGlobalData.sharedInstance.terceroGlobal.tipoDocumentoTercero = tipoDoc
            UserGlobalData.sharedInstance.terceroGlobal.InfoTitular = data.InfoTitular
            UserGlobalData.sharedInstance.terceroGlobal.tipoReporte = tipoReporte
            let consultado = data.InfoTitular!["URLFoto"] as! String
            let url = URL(string: consultado)! as URL
            self.clearTempFolder()
            NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarFotoTercero3(_:)), name: NSNotification.Name("endCargarFotoTercero3"), object: nil)
            Alamofire.request(url).responseImage { response in
                switch response.result {
                case .success:
                    if let image = response.result.value {
                        _ = self.saveImageTercero(image: image)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoTercero3"), object: nil)
                    }
                    break
                case .failure( _):
                    let image = UIImage(named: "CNIGenImaHom")!
                    _ = self.saveImageTercero(image: image)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoTercero3"), object: nil)
                    break
                }
            }
        }else{
            if codigoWs == "99" {
                showError99()
            }else{
                iniMensajeError(codigo: (data.codigoWS!))
            }
        }
    }
    
    @objc func endCargarFotoTercero3(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        let viewCont = storyboard!.instantiateViewController(withIdentifier: tipoReporte == "D" ? "ConsultaReporteDetalladoVC" : "ConsultaReporteResumidoVC")
        navigationController?.pushViewController(viewCont, animated: true)
    }
}

extension MiReporteFlashPremiumViewController:UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.semaforosCL {
            return imgArray.count
        }
        else if collectionView == self.creditosCL{
            return creditosArray.count
        }
        return imgArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.semaforosCL {
            let identifier = "SemaforoCVC"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! SemaforoMiReporteCollectionViewCell
            cell.semaIMG.image = imgArray[indexPath.row]
            let arregloMesDesc = self.devuelveArregloUltimosTresMeses(fechaUltima: UserGlobalData.sharedInstance.terceroGlobal.FechaReporte)
            var swiftArray = arregloMesDesc as AnyObject as! [AnyObject]
            let objSwift0 : NSDictionary = swiftArray[0] as! NSDictionary
            let objSwift1 : NSDictionary = swiftArray[1] as! NSDictionary
            let objSwift2 : NSDictionary = swiftArray[2] as! NSDictionary
            
            let arrayMesesController = NSMutableArray()
            arrayMesesController.add(objSwift2)
            arrayMesesController.add(objSwift1)
            arrayMesesController.add(objSwift0)
            
            let objMes : NSDictionary = arrayMesesController.object(at: indexPath.row) as! NSDictionary
            let nombre : String = objMes.object(forKey: "mes") as! String
            let anio : String = objMes.object(forKey: "anio") as! String
            let nombreMes : String = self.devuelveNombreMesParticionado(mes: nombre)
            cell.mesLB.text = "\(nombreMes)\n\(anio)"
            
            return cell
        }
        else if collectionView == self.creditosCL {
            
            let identifier = "CreditosCVC"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! CreditosCollectionViewCell
            
            let objIndicador : NSDictionary = creditosArray[indexPath.row] as! NSDictionary
            let codigo : String = "\((objIndicador.object(forKey: "CICodInd"))!)"
            
            cell.aimagenCreditos.image =  self.devuelveCreditosIndicadores(cred: codigo)
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == self.semaforosCL {
            return 40
        }
        else if collectionView == self.creditosCL{
            return 5
        }
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if collectionView == self.semaforosCL {
            let totalCellWidth = 45 * imgArray.count
            let totalSpacingWidth = 30 * (imgArray.count - 1)
            let leftInset = (semaforosCL.layer.bounds.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
            let rightInset = leftInset
            return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
        }
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    func collectionView(_ collectionView:UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var widthItem  = 30
        var heighItem = 30
        
        if collectionView == self.semaforosCL {
            widthItem = 45
            heighItem = 60
        }
        else if collectionView == self.creditosCL{
            widthItem  = 30
            heighItem = 40
        }
        return CGSize(width: widthItem, height: heighItem)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.creditosCL {
            if let parentVC = self.parent as? ReportesPremiumFlashXLBarPagerTabStripViewController {
                parentVC.moveToViewController(at: 1)
            }
        }
    }
}
