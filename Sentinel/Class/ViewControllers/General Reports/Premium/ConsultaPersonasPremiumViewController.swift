//
//  ConsultaPersonasPremiumViewController.swift
//  Sentinel
//
//  Created by Daniel on 27/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class ConsultaPersonasPremiumViewController: ParentViewController,MiPickerComboDelegate, UITextFieldDelegate {

    @IBOutlet weak var saldoLB: UILabel!
    @IBOutlet weak var consultaPersonaToolbar: UIToolbar!
    @IBOutlet weak var comboPersona: CTComboPicker!
    @IBOutlet weak var personaJuridicaTXT: UITextField!
    @IBOutlet weak var otrosTXT: UITextField!
    @IBOutlet weak var personaNaturalView: UIView!
    
    @IBOutlet weak var apePaternoTXT: UITextField!
    @IBOutlet weak var apeMaternoTXT: UITextField!
    @IBOutlet weak var nombresTXT: UITextField!
    
    var tercero : TerceroClass?
    var userData : UserClass?
    
    var codCombo = "1"
    var desCombo = "Persona Natural"
    var combosArray: NSMutableArray = NSMutableArray()
    var combosIDArray: NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializarVariables()
        self.gestureScreen()
        self.cargarDatos()
    }
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func inicializarVariables(){
        self.consultaPersonaToolbar.clipsToBounds = true
        
        self.tercero = UserGlobalData.sharedInstance.terceroGlobal
        self.userData = UserGlobalData.sharedInstance.userGlobal
        
        self.comboPersona.delegate = self
        
        self.combosArray.add("Persona Natural")
        self.combosArray.add("Persona Jurídica")
        self.combosArray.add("Otros")
        
        self.combosIDArray.add("1")
        self.combosIDArray.add("2")
        self.combosIDArray.add("3")
    }
    
    func cargarDatos(){
        let saldo : String = UserGlobalData.sharedInstance.userGlobal.NroSerFl!
        self.saldoLB.text = "Saldo: S/ \(saldo)"
        
    }
    
    @IBAction func accionCombo(_ sender: Any) {
        
        self.comboPersona.setConfig((combosArray as! [Any]))
        self.comboPersona.open()
        self.comboPersona.setId(1)
    }
    @IBAction func accionComboDos(_ sender: Any) {
        
        self.comboPersona.setConfig((combosArray as! [Any]))
        self.comboPersona.open()
        self.comboPersona.setId(1)
    }
    @IBAction func accionBuscar(_ sender: Any) {
        
        
    }
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func miPickerCombo(_ pickerid: Int, didSelectRow row: Int) {
        switch pickerid {
        case 1:
            codCombo = combosIDArray[row] as! String
            desCombo = combosArray[row] as! String
            if codCombo == "1"{
                self.personaNaturalView.isHidden = false
                self.personaJuridicaTXT.isHidden = true
                self.otrosTXT.isHidden = true
            }
            else if codCombo == "2"{
                self.personaNaturalView.isHidden = true
                self.personaJuridicaTXT.isHidden = false
                self.otrosTXT.isHidden = true
            }
            else if codCombo == "3"{
                self.personaNaturalView.isHidden = true
                self.personaJuridicaTXT.isHidden = true
                self.otrosTXT.isHidden = false
            }
            break
        case 2:
            break
        default:
            break
        }
    }
    
    func miPickerCombo(_ MiPickerView: CTComboPicker!, pickerView picker: UIPickerView!, didSelectRow row: Int, inComponent component: Int) {
        
    }

}
