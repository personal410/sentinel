//
//  XLConsultasPremiumViewController.swift
//  Sentinel
//
//  Created by Daniel on 27/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import XLPagerTabStrip
class XLConsultasPremiumViewController:ButtonBarPagerTabStripViewController {
    //MARK: - Outlets
    @IBOutlet weak var barrabb:ButtonBarView!
    //MARK: - Props
    var primerViewController:UIViewController!
    var segundoViewController:UIViewController!
    var tercerViewController:UIViewController!
    var isReload = false
    var cargarConsultasFlash = false
    //MARK: - ViewCont
    override func viewDidLoad(){
        settings.style.buttonBarHeight = 44.0
        settings.style.buttonBarBackgroundColor = UIColor.clear
        settings.style.buttonBarItemBackgroundColor = UIColor.clear
        settings.style.selectedBarBackgroundColor = UIColor(hex: "491B02")
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 5
        settings.style.buttonBarRightContentInset = 5
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        pagerBehaviour = PagerTabStripBehaviour.common(skipIntermediateViewControllers: true)
        changeCurrentIndex = {(oldCell:ButtonBarViewCell?, newCell:ButtonBarViewCell?, animated:Bool) -> Void in
            oldCell?.isSelected = false
            newCell?.isSelected = true
        }
        //reloadPagerTabStripView()
        super.viewDidLoad()
    }
    override func viewDidLayoutSubviews(){
        setGradient(uiView: barrabb)
        if !isReload {
            reloadPagerTabStripView()
            isReload = true
            if cargarConsultasFlash {
                cargarConsultasFlash = false
                moveToViewController(at: 2)
            }
        }
    }
    override func viewDidAppear(_ animated:Bool){
        if isReload && cargarConsultasFlash {
            cargarConsultasFlash = false
            moveToViewController(at: 2)
        }
    }
    //MARK: - Pager
    override func viewControllers(for pagerTabStripController:PagerTabStripViewController) -> [UIViewController] {
        primerViewController = storyboard!.instantiateViewController(withIdentifier: "PrimerTabConsultaPremiumVC")
        segundoViewController = storyboard!.instantiateViewController(withIdentifier: "HistorialVC")
        tercerViewController = storyboard!.instantiateViewController(withIdentifier: "TercerTabConsultaPremiumVC")
        return [primerViewController, segundoViewController, tercerViewController]
    }
    //MARK: - Auxiliar
    func setGradient(uiView:UIView){
        let colorBottom =  UIColor(red: 239.0/255.0, green: 212.0/255.0, blue: 80.0/255.0, alpha: 1.0).cgColor
        let colorTop = UIColor(red: 209.0/255.0, green: 142.0/255.0, blue: 24.0/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.startPoint = CGPoint(x:0.0, y:0.5)
        gradientLayer.endPoint = CGPoint(x:1.0, y:0.5)
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = uiView.bounds
        uiView.layer.insertSublayer(gradientLayer, at: 0)
    }
}
