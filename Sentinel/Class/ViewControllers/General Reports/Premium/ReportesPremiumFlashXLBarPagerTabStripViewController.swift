//
//  ReportesPremiumFlashXLBarPagerTabStripViewController.swift
//  Sentinel
//
//  Created by Daniel on 28/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import XLPagerTabStrip
class ReportesPremiumFlashXLBarPagerTabStripViewController: ButtonBarPagerTabStripViewController {
    var isReload = false
    let graySpotifyColor = UIColor.fromHex(rgbValue: 0x0072BE)
    var MiReporteFlashPremiumVC:UIViewController!
    var CreditosIndicadoresFlashPremiumVC:UIViewController!
    override func viewDidLoad(){
        super.viewDidLoad()
        
        settings.style.buttonBarHeight = 0
        settings.style.buttonBarBackgroundColor = graySpotifyColor
        settings.style.buttonBarItemBackgroundColor = UIColor.clear
        settings.style.selectedBarBackgroundColor =  UIColor.fromHex(rgbValue: 0xffffff)
        settings.style.buttonBarItemFont = UIFont(name: "HelveticaNeue-Bold", size:12) ??
            UIFont.systemFont(ofSize: 12)
        
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor.clear
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        
        settings.style.buttonBarLeftContentInset = 5
        settings.style.buttonBarRightContentInset = 5
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor(white: 1, alpha: 0.5)
            oldCell?.imageView.tintColor = UIColor(white: 1, alpha: 0.5)
            newCell?.label.textColor = UIColor.fromHex(rgbValue: 0xffffff)
            newCell?.imageView.tintColor = UIColor.fromHex(rgbValue: 0xffffff)
        }
        super.viewDidLoad()
        self.reloadPagerTabStripView()
    }
    override func viewControllers(for pagerTabStripController:PagerTabStripViewController) -> [UIViewController] {
        if !isReload {
            isReload = true
            MiReporteFlashPremiumVC = storyboard!.instantiateViewController(withIdentifier: "MiReporteFlashPremiumVC")
            CreditosIndicadoresFlashPremiumVC = storyboard!.instantiateViewController(withIdentifier: "CreditosIndicadoresTerceroDetalladoVC")
        }
        return [MiReporteFlashPremiumVC, CreditosIndicadoresFlashPremiumVC]
    }
}
