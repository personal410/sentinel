//
//  TercerTabConsultaPremiumViewController.swift
//  Sentinel
//
//  Created by Daniel on 27/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit
import XLPagerTabStrip
class TercerTabConsultaPremiumViewController: ParentViewController,IndicatorInfoProvider {
    @IBOutlet weak var saldoLB: UILabel!
    @IBOutlet weak var primerView: UIView!
    @IBOutlet weak var segundoView: UIView!
    @IBOutlet weak var resumidoLB: UILabel!
    @IBOutlet weak var detalladoLB: UILabel!
    @IBOutlet weak var viewDos: UIView!
    @IBOutlet weak var viewUno: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.estilos()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.iniConsultaDatosServicio()
    }
    func estilos(){
        self.primerView.layer.cornerRadius = 5
        
        primerView.layer.shadowColor = UIColor.lightGray.cgColor
        primerView.layer.shadowOpacity = 0.5
        primerView.layer.shadowOffset = CGSize.zero
        
        self.segundoView.layer.cornerRadius = 5
        
        segundoView.layer.shadowColor = UIColor.lightGray.cgColor
        segundoView.layer.shadowOpacity = 0.5
        segundoView.layer.shadowOffset = CGSize.zero
    }
    
    func indicatorInfo(for pagerTabStripController:PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "", image: UIImage(named: "con_pre_flash_off"), highlightedImage: UIImage(named: "con_pre_flash_on"))
    }
    
    public func iniConsultaDatosServicio(){
        showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endConsultaDatosServicio(_:)), name: NSNotification.Name("endConsultaDatosServicio"), object: nil)
        usuarioParent = UserGlobalData.sharedInstance.userGlobal
        let usua : String = (usuarioParent?.NumDocumento)!
        let sesion : String = (usuarioParent?.SesionId)!
        let servicios = UserGlobalData.sharedInstance.misServiciosGlobal!
        let servicio : String = servicios.NroServicio!
        
        let parametros = [
            "Usuario": usua,
            "SesionId": sesion,
            "Servicio": servicio
            ] as [String : Any]
        OriginData.sharedInstance.consultaDatosServicio(notification: "endConsultaDatosServicio", parametros: parametros)
    }
    
    @objc func endConsultaDatosServicio(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if validaIndicador == 0 {
            let terceroLocal = notification.object as! TerceroClass
            UserGlobalData.sharedInstance.terceroGlobal = terceroLocal
            terceroParent = UserGlobalData.sharedInstance.terceroGlobal
            
            UserGlobalData.sharedInstance.terceroGlobal.DispDet = terceroLocal.SaldoServicio?.object(forKey: "DispDet") as? String
            UserGlobalData.sharedInstance.terceroGlobal.DispRes = terceroLocal.SaldoServicio?.object(forKey: "DispRes") as? String
            UserGlobalData.sharedInstance.terceroGlobal.SalFlash = terceroLocal.SaldoServicio?.object(forKey: "SalFlash") as? String
            self.resumidoLB.text = terceroLocal.SaldoServicio?.object(forKey: "DispRes") as? String
            self.detalladoLB.text = terceroLocal.SaldoServicio?.object(forKey: "DispDet") as? String
        }
    }
    
    @IBAction func accionComprar(_ sender: Any) {
        let nav = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReporteDeudasPersonasEmpresasVC") as! ReporteDeudasPersonasEmpresasViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    @IBAction func accionBusquedaNumero(_ sender: Any) {
        let nav = UIStoryboard.init(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "BusquedaxDocumentoPremiumVC") as! BusquedaxDocumentoPremiumViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
    @IBAction func accionBusquedaNombre(_ sender: Any) {
        let nav = UIStoryboard.init(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "ConsultaxPersonaNombrePremiumVC") as! ConsultaxPersonaNombrePremiumViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    
}
