//
//  BusquedaxDocumentoPremiumViewController.swift
//  Sentinel
//
//  Created by Daniel on 27/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import Alamofire
import AlamofireImage
class BusquedaxDocumentoPremiumViewController: ParentViewController,MiPickerComboDelegate, UITextFieldDelegate {
    //MARK: - Outlets
    @IBOutlet weak var consultaDniToolbar: UIToolbar!
    @IBOutlet weak var numDocTXT: UITextField!
    @IBOutlet weak var comboDocumento: CTComboPicker!
    @IBOutlet weak var saldoLB: UILabel!
    @IBOutlet weak var resumidosLB: UILabel!
    @IBOutlet weak var detalladosLB: UILabel!
    //MARK: - Props
    var saldoFlash = ""
    var tercero:TerceroClass?
    var userData : UserClass?
    var tipoDocumento = ""
    var codCombo = "1"
    var desCombo = "Automático"
    var combosArray = NSMutableArray()
    var combosIDArray = NSMutableArray()
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        inicializarVariables()
        gestureScreen()
    }
    override func viewWillAppear(_ animated:Bool){
        iniConsultaDatosServicioParent()
        cargarDatos()
    }
    //MARK: - Auxiliar
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        view.addGestureRecognizer(tapGesture)
    }
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    func inicializarVariables(){
        self.consultaDniToolbar.clipsToBounds = true
        
        self.comboDocumento.delegate = self
        self.numDocTXT.delegate = self
        
        self.combosArray.add("Automático")
        self.combosArray.add("DNI")
        self.combosArray.add("RUC")
        self.combosArray.add("Carnet de extranjería")
        self.combosArray.add("Pasaporte")
        
        self.combosIDArray.add("1")
        self.combosIDArray.add("2")
        self.combosIDArray.add("3")
        self.combosIDArray.add("4")
        self.combosIDArray.add("5")
    }
    func cargarDatos(){
        tercero = UserGlobalData.sharedInstance.terceroGlobal
        self.userData = UserGlobalData.sharedInstance.userGlobal
        let saldo : String = UserGlobalData.sharedInstance.terceroGlobal.SalFlash!
        saldoFlash = saldo
        self.numDocTXT.text = ""
        self.resumidosLB.text = tercero?.DispRes
        self.detalladosLB.text = tercero?.DispDet
    }
    func validar() -> Bool{
        let numeroDocumento : String = self.numDocTXT.text!
        let cantidad = numeroDocumento.count
        if numeroDocumento == ""{
            self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar el número de documento a consultar.")
            return false
        }
        if codCombo == "1"{
            if (cantidad == 8 || cantidad == 11) {
                
            }
            else
            {
                self.showAlert(PATHS.SENTINEL, mensaje: "Número de documento inválido")
                return false
            }
        }
        if codCombo == "2" && cantidad != 8 {
            self.showAlert(PATHS.SENTINEL, mensaje: "El número de DNI debe contener 8 dígitos")
            return false
        }
        if codCombo == "3" && cantidad != 11 {
            self.showAlert(PATHS.SENTINEL, mensaje: "El número de RUC debe contener 11 dígitos")
            return false
        }
        if numeroDocumento == userData?.NumDocumento{
            self.showAlert(PATHS.SENTINEL, mensaje: "No se puede consultar a usted mismo.")
            self.numDocTXT.text = ""
            return false
        }
        return true
    }
    func iniTitulares(){
        showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endTitulares(_:)), name: NSNotification.Name("endTitulares"), object: nil)
        let userData = UserGlobalData.sharedInstance.userGlobal
        
        var tipoDoc: String = ""
        if codCombo == "1" {
            if numDocTXT.text?.count == 8 {
                tipoDoc = "D"
            }else{
                tipoDoc = "R"
            }
        }else if codCombo == "2" {
            tipoDoc = "D"
        }else if codCombo == "3" {
            tipoDoc = "R"
        }else if codCombo == "4" {
            tipoDoc = "4"
        }
        self.tipoDocumento = tipoDoc
        let numero : String = self.numDocTXT.text!
        let sesion : String = (userData?.SesionId!)!
        let usuario : String = (userData?.user)!
        let parametros = ["Usuario": usuario, "TDoc": tipoDoc, "NDoc": numero, "SesionId": sesion] as [String : Any]
        OriginData.sharedInstance.nombreTitular(notification: "endTitulares", parametros: parametros)
    }
    
    @objc func endTitulares(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data : TerceroClass = (notification.object as? TerceroClass)!
            if data.CodigoWS == "0"{
                if tipoDocumento == "D"{
                    let nombre : String = "\(data.Nombres!) \(data.ApePat!) \(data.ApeMat!)"
                    let alerta = UIAlertController(title: PATHS.SENTINEL, message: "¿Desea consultar a \(nombre) con DNI: \(self.numDocTXT.text!)", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alerta.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler: nil))
                    
                    alerta.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
                        let nav = UIStoryboard.init(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "MiReporteFlashVC") as! MiReporteFlashViewController
                        self.navigationController?.pushViewController(nav, animated: true)
                    }))
                    self.present(alerta, animated: true, completion: nil)
                }else if tipoDocumento == "R"{
                    let nombre : String = "\(data.NombreRazSoc!)"
                    let alerta = UIAlertController(title: PATHS.SENTINEL, message: "¿Desea consultar a \(nombre) con RUC: \(self.numDocTXT.text!)", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alerta.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler: nil))
                    
                    alerta.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
                        let nav = UIStoryboard.init(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "MiReporteFlashVC") as! MiReporteFlashViewController
                        self.navigationController?.pushViewController(nav, animated: true)
                    }))
                    
                    self.present(alerta, animated: true, completion: nil)
                }else{
                    let nombre : String = "\(data.Nombres ?? "") \(data.ApePat ?? "") \(data.ApeMat ?? "")"
                    let alerta = UIAlertController(title: PATHS.SENTINEL, message: "¿Desea consultar a \(nombre) con Documento: \(self.numDocTXT.text!)?", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alerta.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler: nil))
                    
                    alerta.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
                        let nav = UIStoryboard.init(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "MiReporteFlashVC") as! MiReporteFlashViewController
                        self.navigationController?.pushViewController(nav, animated: true)
                    }))
                    self.present(alerta, animated: true, completion: nil)
                    
                }
            }else{
                if data.CodigoWS != "" {
                    if data.CodigoWS == "99" {
                        showError99()
                    }else{
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "En estos momentos no se puede realizar la operación.", preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func iniBusquedaDocumento(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endBusquedaDocumento(_:)), name: NSNotification.Name("endBusquedaDocumento"), object: nil)
        
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        
        var tipoDoc : String = ""
        if codCombo == "2" {
            tipoDoc = "D"
        }
        else if codCombo == "3"{
            tipoDoc = "R"
        }
        else if codCombo == "1"{
            if self.numDocTXT.text?.count == 8 {
                tipoDoc = "D"
            }
            else if self.numDocTXT.text?.count == 11 {
                tipoDoc = "R"
            }
        }
        else if codCombo == "4"{
            tipoDoc = "4"
        }
        
        let nroDoc : String = self.numDocTXT.text!
        let sesion : String = userData!.SesionId!
        
        let parametros = [
            "Usuario" : userData?.user as! String,
            "Servicio" : userData?.NroSerFl as! String,
            "TipoDocumento" :  tipoDoc,
            "NroDocumento" : nroDoc,
            "SesionId" : sesion,
            "origenAplicacion" : PATHS.APP_ID,
            "TipoConsulta" : "",
            ] as [String : Any]
        OriginData.sharedInstance.miReporteDNITercero(notification: "endBusquedaDocumento", parametros: parametros)
    }
    
    @objc func endBusquedaDocumento(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as! TerceroClass
            if data.codigoWS == "0"{
                UserGlobalData.sharedInstance.terceroGlobal.InfoTitular = data.InfoTitular
                let consultado: String = data.InfoTitular!["URLFoto"] as! String
                let url : URL = NSURL(string: consultado)! as URL
                self.clearTempFolder()
                NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarFotoTercero(_:)), name: NSNotification.Name("endCargarFotoTercero"), object: nil)
                
                self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
                Alamofire.request(url).responseImage { response in
                    debugPrint(response)
                    switch response.result {
                    case .success:
                        if let image = response.result.value {
                            _ = self.saveImageTercero(image: image)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoTercero"), object: nil)
                        }
                        break
                        
                    case .failure(_):
                        //CNIGenImaHom
                        let image : UIImage = UIImage.init(named: "CNIGenImaHom")!
                        _ = self.saveImageTercero(image: image)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoTercero"), object: nil)
                        break
                    }
                }   
            }
            else
            {
                if data.codigoWS != nil {
                    if data.codigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        iniMensajeError(codigo: (data.codigoWS!))
                    }
                }
                else
                {
                    let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                    alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                    self.present(alerta, animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc func endCargarFotoTercero(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        
        self.iniTitulares()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if codCombo == "1" {
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 10
        }
        if codCombo == "2"{
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 7
        }
        if codCombo == "3"{
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 10
        }
        if codCombo == "4" {
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 19
        }
        return true
    }
    
    @IBAction func accionCombo(_ sender: Any) {
        
        self.comboDocumento.setConfig(combosArray as! [Any])
        self.comboDocumento.open()
        self.comboDocumento.setId(1)
    }
    @IBAction func accionComboDos(_ sender: Any) {
        
        self.comboDocumento.setConfig(combosArray as! [Any])
        self.comboDocumento.open()
        self.comboDocumento.setId(1)
    }
    @IBAction func accionBuscar(_ sender: Any) {
        if self.validar() {
            self.iniBusquedaDocumento()
        }
        
    }
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func accionComprar(_ sender: Any) {
        self.irComprarConsultas()
    }
    
    func miPickerCombo(_ pickerid: Int, didSelectRow row: Int) {
        switch pickerid {
        case 1:
            codCombo = combosIDArray[row] as! String
            desCombo = combosArray[row] as! String
            
            break
        case 2:
            break
        default:
            break
        }
    }
    
    func miPickerCombo(_ MiPickerView:CTComboPicker!, pickerView picker: UIPickerView!, didSelectRow row: Int, inComponent component: Int){}
}
