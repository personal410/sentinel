//
//  PrimerTabConsultaPremiumViewController.swift
//  Sentinel
//
//  Created by Daniel on 27/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import XLPagerTabStrip
class PrimerTabConsultaPremiumViewController:ParentViewController, IndicatorInfoProvider {
    //MARK: - Outlet
    @IBOutlet weak var detalladosLB: UILabel!
    @IBOutlet weak var resumidosLB: UILabel!
    @IBOutlet weak var primerView: UIView!
    @IBOutlet weak var segundoView: UIView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.estilos()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.iniConsultaDatosServicio()
    }
    func cargarDatos(){
        self.detalladosLB.text = UserGlobalData.sharedInstance.terceroGlobal.DispDet
        self.resumidosLB.text = UserGlobalData.sharedInstance.terceroGlobal.DispRes
    }
    
    func estilos(){
        self.primerView.layer.cornerRadius = 5
        primerView.layer.shadowColor = UIColor.lightGray.cgColor
        primerView.layer.shadowOpacity = 0.5
        primerView.layer.shadowOffset = CGSize.zero
        self.segundoView.layer.cornerRadius = 5
        segundoView.layer.shadowColor = UIColor.lightGray.cgColor
        segundoView.layer.shadowOpacity = 0.5
        segundoView.layer.shadowOffset = CGSize.zero
    }
    
    public func iniConsultaDatosServicio(){
        self.showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endConsultaDatosServicio(_:)), name: NSNotification.Name("endConsultaDatosServicio"), object: nil)
        usuarioParent = UserGlobalData.sharedInstance.userGlobal
        let servicios = UserGlobalData.sharedInstance.misServiciosGlobal!
        let usua : String = (usuarioParent?.NumDocumento)!
        let sesion : String = (usuarioParent?.SesionId)!
        let servicio : String = servicios.NroServicio!//(usuarioParent?.NroSerCT)!
        
        let parametros = [
            "Usuario": usua,
            "SesionId": sesion,
            "Servicio": servicio
            ] as [String : Any]
        OriginData.sharedInstance.consultaDatosServicio(notification: "endConsultaDatosServicio", parametros: parametros)
    }
    
    @objc func endConsultaDatosServicio(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let terceroLocal = notification.object as! TerceroClass
            UserGlobalData.sharedInstance.terceroGlobal = terceroLocal
            terceroParent = UserGlobalData.sharedInstance.terceroGlobal
            
            UserGlobalData.sharedInstance.terceroGlobal.DispDet = terceroLocal.SaldoServicio?.object(forKey: "DispDet") as? String
            UserGlobalData.sharedInstance.terceroGlobal.DispRes = terceroLocal.SaldoServicio?.object(forKey: "DispRes") as? String
            UserGlobalData.sharedInstance.terceroGlobal.SalFlash = terceroLocal.SaldoServicio?.object(forKey: "SalFlash") as? String
            self.cargarDatos()
        }
    }
    @IBAction func accionNumeroDocumento(_ sender:Any){
        if UserGlobalData.sharedInstance.terceroGlobal.DispDet == "0" && UserGlobalData.sharedInstance.terceroGlobal.DispRes == "0"{
            irComprarConsultas()
        }else{
            let nav = UIStoryboard(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "ConsultasDNIVC")
            navigationController?.pushViewController(nav, animated: true)
        }
    }
    @IBAction func accionNombreEmpresa(_ sender: Any) {
        if UserGlobalData.sharedInstance.terceroGlobal.DispDet == "0" && UserGlobalData.sharedInstance.terceroGlobal.DispRes == "0"{
            irComprarConsultas()
        }else{
            let nav = UIStoryboard(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "ConsultaPersonasVC") as! ConsultaPersonasViewController
            navigationController?.pushViewController(nav, animated: true)
        }
    }
    @IBAction func accionComprar(_ sender: Any) {
        let nav = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReporteDeudasPersonasEmpresasVC") as! ReporteDeudasPersonasEmpresasViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "", image: UIImage(named: "con_pre_busq_off"), highlightedImage: UIImage(named: "con_pre_busq_on"))
    }
}
