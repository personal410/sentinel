//
//  ConsultaResultadoPersonasPremiumViewController.swift
//  Sentinel
//
//  Created by Daniel on 27/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//ConsultaResultadoPersonasPremiumViewController
import Alamofire
import AlamofireImage
class ConsultaResultadoPersonasPremiumViewController:ParentViewController {
    @IBOutlet weak var coincidenciasLB: UILabel!
    @IBOutlet weak var seleccionaTextoLB: UILabel!
    @IBOutlet weak var personasTable: UITableView!
    @IBOutlet weak var resultadoPersonaToolbar: UIToolbar!
    @IBOutlet weak var saldoLB: UILabel!
    @IBOutlet weak var resumidoLB: UILabel!
    @IBOutlet weak var detalladoLB: UILabel!
    
    var apePaterno : String = ""
    var apeMaterno : String = ""
    var nombre : String = ""
    var personaJuridica: String = ""
    var otros: String = ""
    var tipoPersona: String!
    var arregloCoincidencias : NSArray = NSArray()
    var tDocTercero : String = ""
    var nDocTercero : String = ""
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.inicializarVariables()
        self.iniCoincidenciasEncontradas()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.personasTable.reloadData()
        self.iniConsultaDatosServicioParent()
        self.cargarDatos()
    }
    
    func cargarDatos(){
        let saldo : String = UserGlobalData.sharedInstance.terceroGlobal.SalFlash!
        self.saldoLB.text = "Saldo: S/ \(saldo)"
        let tercero = UserGlobalData.sharedInstance.terceroGlobal!
        self.resumidoLB.text = tercero.DispRes
        self.detalladoLB.text = tercero.DispDet
    }
    
    func inicializarVariables(){
        self.resultadoPersonaToolbar.clipsToBounds = true
        self.personasTable.dataSource = self
        self.personasTable.delegate = self
    }
    
    func iniCoincidenciasEncontradas(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endCoincidenciasEncontradas(_:)), name: NSNotification.Name("endCoincidenciasEncontradas"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        let userData = UserGlobalData.sharedInstance.userGlobal
        let randomNumber = String(self.generateRandomNumber(numDigits: 7))
        var tipoPersonaTercero = ""
        let usuario = (userData?.user)!
        let sesion = (userData?.SesionId)!
        
        if self.tipoPersona == "1" {
            tipoPersonaTercero = "N"
            let parametros = [
                "UserId":usuario,
                "IdSession":randomNumber,
                "TipoPersona":tipoPersonaTercero,
                "Paterno":self.apePaterno,
                "Materno":self.apeMaterno,
                "nombres":self.nombre,
                "UseSeIDSession":sesion
                ] as [String : Any]
            OriginData.sharedInstance.getConsultaNombre(notification: "endCoincidenciasEncontradas", parametros: parametros)
        }
        else if self.tipoPersona == "2" {
            tipoPersonaTercero = "J"
            let parametros = [
                "UserId":usuario,
                "IdSession":randomNumber,
                "TipoPersona":tipoPersonaTercero,
                "Paterno":self.personaJuridica,
                "Materno":"",
                "nombres":"",
                "UseSeIDSession":sesion
                ] as [String : Any]
            OriginData.sharedInstance.getConsultaNombre(notification: "endCoincidenciasEncontradas", parametros: parametros)
        }
        else if self.tipoPersona == "3" {
            tipoPersonaTercero = "O"
            let parametros = [
                "UserId":usuario,
                "IdSession":randomNumber,
                "TipoPersona":tipoPersonaTercero,
                "Paterno":self.otros,
                "Materno":"",
                "nombres":"",
                "UseSeIDSession":sesion
                ] as [String : Any]
            OriginData.sharedInstance.getConsultaNombre(notification: "endCoincidenciasEncontradas", parametros: parametros)
        }
        else{
            let parametros = [
                "UserId":usuario,
                "IdSession":randomNumber,
                "TipoPersona":tipoPersonaTercero,
                "Paterno":self.apePaterno,
                "Materno":self.apeMaterno,
                "nombres":self.nombre,
                "UseSeIDSession":sesion
                ] as [String : Any]
            OriginData.sharedInstance.getConsultaNombre(notification: "endCoincidenciasEncontradas", parametros: parametros)
        }
    }
    
    @objc func endCoincidenciasEncontradas(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as? TerceroClass
            if data?.CodigoWS == "0"{
                UserGlobalData.sharedInstance.terceroGlobal.SDTBusAlf = data?.SDTBusAlf
                self.arregloCoincidencias = (data?.SDTBusAlf)!
                if self.arregloCoincidencias.count == 0 {
                    self.coincidenciasLB.text = "No hay coincidencias encontradas."
                }else if self.arregloCoincidencias.count == 1 {
                    self.coincidenciasLB.text = "Hay \(self.arregloCoincidencias.count) coincidencia encontrada."
                }
                else if self.arregloCoincidencias.count > 1 {
                    self.coincidenciasLB.text = "Hay \(self.arregloCoincidencias.count) coincidencias encontradas."
                }
                
                self.personasTable.reloadData()
            }
            else
            {
                if data?.CodigoWS != nil {
                    if data?.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "En estos momentos no se puede realizar la consulta. Intente luego.", preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                        //iniMensajeError(codigo: (data?.CodigoWS!)!)
                    }
                }
            }
        }
    }
    
    func iniBusquedaDocumento(tipoDoc : String, numeroDoc : String){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endBusquedaDocumento(_:)), name: NSNotification.Name("endBusquedaDocumento"), object: nil)
        
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        let userData = UserGlobalData.sharedInstance.userGlobal!
        
        let usuario : String = userData.user!
        let servicio : String = userData.NroSerFl!
        let tdoc : String = tipoDoc
        let ndoc : String = numeroDoc
        let sesion : String = userData.SesionId!
        
        let parametros = [
            "Usuario" : usuario,
            "Servicio" : servicio,
            "TipoDocumento" :  tdoc,
            "NroDocumento" : ndoc,
            "SesionId" : sesion,
            "origenAplicacion" : PATHS.APP_ID,
            "TipoConsulta" : "",
            ] as [String : Any]
        OriginData.sharedInstance.miReporteDNITercero(notification: "endBusquedaDocumento", parametros: parametros)
    }
    
    @objc func endBusquedaDocumento(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as! TerceroClass
            if data.codigoWS == "0"{
                UserGlobalData.sharedInstance.terceroGlobal.InfoTitular = data.InfoTitular
                UserGlobalData.sharedInstance.terceroGlobal.documentoTercero = nDocTercero
                UserGlobalData.sharedInstance.terceroGlobal.tipoDocumentoTercero = tDocTercero
                
                let consultado: String = data.InfoTitular!["URLFoto"] as! String
                let url : URL = NSURL(string: consultado)! as URL
                self.clearTempFolder()
                NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarFotoTercero(_:)), name: NSNotification.Name("endCargarFotoTercero"), object: nil)
                
                self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
                Alamofire.request(url).responseImage { response in
                    debugPrint(response)
                    
                    switch response.result {
                    case .success:
                        if let image = response.result.value {
                            _ = self.saveImageTercero(image: image)
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoTercero"), object: nil)
                        }
                        break
                        
                    case .failure(_):
                        //CNIGenImaHom
                        let image : UIImage = UIImage.init(named: "CNIGenImaHom")!
                        _ = self.saveImageTercero(image: image)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoTercero"), object: nil)
                        break
                    }
                }
            }
            else
            {
                if data.codigoWS != nil {
                    if data.codigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        iniMensajeError(codigo: (data.codigoWS!))
                    }
                }
                else
                {
                    let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                    alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                    self.present(alerta, animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc func endCargarFotoTercero(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        
        let nav = UIStoryboard.init(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "MiReporteFlashVC") as! MiReporteFlashViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }

    @IBAction func accionComprar(_ sender: Any) {
        self.irComprarConsultas()
    }
    
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        self.navigationController?.popViewController(animated: true)
    }
}

extension ConsultaResultadoPersonasPremiumViewController : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arregloCoincidencias.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusquedaPersonasTVC", for: indexPath) as! BusquedaPersonasTableViewCell
        let persona : NSDictionary = self.arregloCoincidencias[indexPath.row] as! NSDictionary
        let dni : String = persona.object(forKey: "numdoc") as! String
        let tipo: String = persona.object(forKey: "tipodoc") as! String
        
        if tipo == "D" {
            cell.dniLB.text = "DNI \(dni)"
            let fecha = persona.object(forKey: "adicional") as? String ?? ""
            if fecha.isEmpty || fecha == "00/00/0000" {
                cell.fechaLB.text = ""
            }else{
                cell.fechaLB.text = fecha
            }
        }else if tipo == "R" {
            cell.dniLB.text = "RUC \(dni)"
            cell.fechaLB.text = ""
        }else{
            cell.dniLB.text = "CE \(dni)"
            cell.fechaLB.text = ""
        }
        cell.nombreLB.text = persona.object(forKey: "descripcion") as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let persona : NSDictionary = self.arregloCoincidencias[indexPath.row] as! NSDictionary
        let dni : String = persona.object(forKey: "numdoc") as! String
        let tipo: String = persona.object(forKey: "tipodoc") as! String
        
        let userD = UserGlobalData.sharedInstance.userGlobal!
        if userD.NumDocumento == dni {
            self.showAlert(PATHS.SENTINEL, mensaje: "No puede elegirse a usted mismo como consulta")
        }
        else{
            var tipoCompleto : String = ""
            if tipo == "D"{
                tipoCompleto = "DNI"
            }
            else if tipo == "R"{
                tipoCompleto = "RUC"
            }
            else
            {
                tipoCompleto = "CE"
            }
            
            let nombre : String = persona.object(forKey: "descripcion") as! String
            let alerta = UIAlertController(title: PATHS.SENTINEL, message: "¿Desea consultar a \(nombre) con \(tipoCompleto): \(dni)?", preferredStyle: UIAlertControllerStyle.alert)
            
            alerta.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.cancel, handler: nil))
            
            alerta.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: { (action: UIAlertAction!) in
                self.nDocTercero = dni
                self.tDocTercero = tipo
                self.iniBusquedaDocumento(tipoDoc: tipo, numeroDoc: dni)
            }))
            
            self.present(alerta, animated: true, completion: nil)
        }
    }
}



