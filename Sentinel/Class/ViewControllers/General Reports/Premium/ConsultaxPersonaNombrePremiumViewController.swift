//
//  ConsultaxPersonaNombrePremiumViewController.swift
//  Sentinel
//
//  Created by Daniel on 27/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class ConsultaxPersonaNombrePremiumViewController:ParentViewController, MiPickerComboDelegate, UITextFieldDelegate{
    @IBOutlet weak var scrollVista: UIScrollView!
    @IBOutlet weak var saldoLB: UILabel!
    @IBOutlet weak var consultaPersonaToolbar: UIToolbar!
    @IBOutlet weak var comboPersona: CTComboPicker!
    @IBOutlet weak var personaJuridicaTXT: UITextField!
    @IBOutlet weak var otrosTXT: UITextField!
    @IBOutlet weak var personaNaturalView: UIView!
    @IBOutlet weak var apePaternoTXT: UITextField!
    @IBOutlet weak var apeMaternoTXT: UITextField!
    @IBOutlet weak var nombresTXT: UITextField!
    @IBOutlet weak var resumidoLB: UILabel!
    @IBOutlet weak var detalladoLB: UILabel!
    
    var tercero : TerceroClass?
    var userData : UserClass?
    
    var esPremium = false
    var esEmpresarial = false
    var codCombo = "1"
    var desCombo = "Persona Natural"
    var combosArray = NSMutableArray()
    var combosIDArray = NSMutableArray()
    
    var isKeyboardAppear = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializarVariables()
        self.gestureScreen()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.iniConsultaDatosServicioParent()
        self.cargarDatos()
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let content = UIEdgeInsetsMake(0, 0, keyboardSize.height - 50, 0)
            scrollVista.contentInset = content
            scrollVista.scrollIndicatorInsets = content
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        scrollVista.contentInset = .zero
        scrollVista.scrollIndicatorInsets = .zero
    }

    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func inicializarVariables(){
        self.consultaPersonaToolbar.clipsToBounds = true
        self.tercero = UserGlobalData.sharedInstance.terceroGlobal
        self.userData = UserGlobalData.sharedInstance.userGlobal
        self.comboPersona.delegate = self
        self.combosArray.add("Persona Natural")
        self.combosArray.add("Persona Jurídica")
        self.combosArray.add("Otros")
        self.combosIDArray.add("1")
        self.combosIDArray.add("2")
        self.combosIDArray.add("3")
    }
    
    func cargarDatos(){
        let saldo : String = UserGlobalData.sharedInstance.terceroGlobal.SalFlash!
        self.saldoLB.text = "Saldo: S/ \(saldo)"
        self.resumidoLB.text = tercero?.DispRes
        self.detalladoLB.text = tercero?.DispDet
    }
    
    func validar() -> Bool{
        let personaJuridica = self.personaJuridicaTXT.text!
        let otros = self.otrosTXT.text!
        let apePaterno = self.apePaternoTXT.text!
        let apeMaterno = self.apeMaternoTXT.text!
        let nombres = self.nombresTXT.text!
        if codCombo == "1" {
            if apePaterno.isEmpty {
                self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar Apellido Paterno")
                return false
            }
            if apeMaterno.isEmpty && nombres.isEmpty {
                self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar Apellido Materno o Nombres")
                return false
            }
        }else if codCombo == "2"{
            if personaJuridica == ""{
                self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar la persona jurídica.")
                return false
            }
        }else if codCombo == "3"{
            if otros == ""{
                self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar el nombre.")
                return false
            }
        }
        return true
    }
    
    @IBAction func accionCombo(_ sender: Any) {
        self.comboPersona.setConfig(combosArray as! [Any])
        self.comboPersona.open()
        self.comboPersona.setId(1)
    }
    @IBAction func accionComboDos(_ sender: Any) {
        self.comboPersona.setConfig(combosArray as! [Any])
        self.comboPersona.open()
        self.comboPersona.setId(1)
    }
    @IBAction func accionBuscar(_ sender: Any) {
        if validar(){
            let navControl : ConsultaResultadoPersonasPremiumViewController = self.storyboard?.instantiateViewController(withIdentifier: "ConsultaResultadoPersonasPremiumVCVC") as! ConsultaResultadoPersonasPremiumViewController
            
            navControl.apePaterno = self.apePaternoTXT.text!
            navControl.apeMaterno = self.apeMaternoTXT.text!
            navControl.nombre = self.nombresTXT.text!
            navControl.tipoPersona = self.codCombo
            navControl.otros = self.otrosTXT.text!
            navControl.personaJuridica = self.personaJuridicaTXT.text!
            
            self.navigationController?.pushViewController(navControl, animated: true)
        } 
    }
    
    @IBAction func accionComprar(_ sender: Any) {
        self.irComprarConsultas()
    }
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func miPickerCombo(_ pickerid: Int, didSelectRow row: Int) {
        switch pickerid {
        case 1:
            codCombo = combosIDArray[row] as! String
            desCombo = combosArray[row] as! String
            if codCombo == "1"{
                self.personaNaturalView.isHidden = false
                self.personaJuridicaTXT.isHidden = true
                self.otrosTXT.isHidden = true
            }
            else if codCombo == "2"{
                self.personaNaturalView.isHidden = true
                self.personaJuridicaTXT.isHidden = false
                self.otrosTXT.isHidden = true
            }
            else if codCombo == "3"{
                self.personaNaturalView.isHidden = true
                self.personaJuridicaTXT.isHidden = true
                self.otrosTXT.isHidden = false
            }
            break
        case 2:
            break
        default:
            break
        }
    }
    
    func miPickerCombo(_ MiPickerView: CTComboPicker!, pickerView picker: UIPickerView!, didSelectRow row: Int, inComponent component: Int) {
        
    }

}
