//
//  MiReporteFlashViewController.swift
//  Sentinel
//
//  Created by Daniel on 28/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class MiReporteFlashViewController:ParentViewController {
    @IBAction func accionAtras(_ sender: Any){
        let index = UserGlobalData.sharedInstance.userGlobal!.EsPremium == "S" ? 1 : 0
        navigationController?.popToViewController(navigationController!.viewControllers[index], animated: true)
    }
}
