//
//  HistorialViewController.swift
//  Sentinel
//
//  Created by Daniel on 1/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//HistorialViewController
import XLPagerTabStrip
import Alamofire
import AlamofireImage
class HistorialViewController:ParentViewController, IndicatorInfoProvider, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var ListaTV: UITableView!
    var checkCPT = "NO"
    var checkHisto = "NO"
    
    var dataArr = NSArray()
    var tipoConsulta = ""
    var dicSeleccionado:NSDictionary?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ListaTV.rowHeight = 90.0  
    }
    override func viewWillAppear(_ animated: Bool) {
        self.iniHistorial()
    }
    func iniHistorial(){
        showActivityIndicator()
        let userData = UserGlobalData.sharedInstance.userGlobal!
        let tipoDoc = (userData.TDocusu)!
        let usuario = (userData.user)!
        let sesion = (userData.SesionId)!
        let parametros = ["TDocUsu": tipoDoc, "Usuario": usuario, "SesionId": sesion] as [String : Any]
        NotificationCenter.default.addObserver(self, selector: #selector(self.endHistorial(_:)), name: NSNotification.Name("endHistorial"), object: nil)
        OriginData.sharedInstance.getHistorialEmpresas(notification: "endHistorial", parametros: parametros)
    }
    @objc func endHistorial(_ notification:NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard notification.object != nil else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: .alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil));
            present(alerta, animated: true, completion: nil)
            return
        }
        let data = notification.object as? UserClass
        if data?.CodigoWS == "0"{
            self.dataArr = (data?.SDTNConRap)!
            self.ListaTV.reloadData()
        }else{
            if data?.CodigoWS != nil {
                if data?.CodigoWS == "99" {
                    showError99()
                }else{
                    let alertaVersion = UIAlertController(title: PATHS.SENTINEL, message: data?.MensajeRespuesta, preferredStyle: .alert)
                    alertaVersion.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    present(alertaVersion, animated: true, completion: nil)
                }
            }
        }
    }
    func iniBusquedaDocumento(objHistorial:NSDictionary){
        dicSeleccionado = objHistorial
        showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endBusquedaDocumento(_:)), name: NSNotification.Name("endBusquedaDocumento"), object: nil)
        let userData = UserGlobalData.sharedInstance.userGlobal!
        var tipoDoc = ""
        let nroDoc = objHistorial.object(forKey: "AFSerCptNroDoc") as! String
        if nroDoc.count == 8 {
            tipoDoc = "D"
        }else if nroDoc.count == 11 {
            tipoDoc = "R"
        }else{
            tipoDoc = "4"
        }
        
        let consulta = objHistorial.object(forKey: "TipoConsulta") as! String
        let sesion = userData.SesionId!
        let parametros = [
            "Usuario": userData.user!,
            "Servicio": objHistorial.object(forKey: "AFSerCod") ?? "",
            "TipoDocumento": tipoDoc,
            "NroDocumento": nroDoc,
            "SesionId": sesion,
            "origenAplicacion": PATHS.APP_ID,
            "TipoConsulta": consulta,
            ] as [String : Any]
        OriginData.sharedInstance.miReporteDNITercero(notification: "endBusquedaDocumento", parametros: parametros)
    }
    
    @objc func endBusquedaDocumento(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard notification.object != nil  else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: .alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        let data = notification.object as! TerceroClass
        if data.codigoWS == "0" {
            UserGlobalData.sharedInstance.terceroGlobal.InfoTitular = data.InfoTitular
            UserGlobalData.sharedInstance.terceroGlobal.tipoDocumentoTercero = dicSeleccionado!.object(forKey: "AFSerCptTDoc") as? String
            UserGlobalData.sharedInstance.terceroGlobal.documentoTercero = dicSeleccionado!.object(forKey: "AFSerCptNroDoc") as? String
            UserGlobalData.sharedInstance.terceroGlobal.FechaReporte = dicSeleccionado!.object(forKey: "CnpFchReg") as! String
            let consultado = data.InfoTitular!["URLFoto"] as! String
            let url = NSURL(string: consultado)! as URL
            self.clearTempFolder()
            NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarFotoTercero4(_:)), name: NSNotification.Name("endCargarFotoTercero4"), object: nil)
            showActivityIndicator()
            Alamofire.request(url).responseImage { response in
                debugPrint(response)
                switch response.result {
                    case .success:
                        if let image = response.result.value {
                            _ = self.saveImageTercero(image: image)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoTercero4"), object: nil)
                        }
                        break
                    case .failure(_):
                        let image = UIImage(named: "CNIGenImaHom")!
                        _ = self.saveImageTercero(image: image)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoTercero4"), object: nil)
                        break
                }
            }
        }else{
            if data.codigoWS != nil {
                if data.codigoWS == "99" {
                    showError99()
                }else{
                    iniMensajeError(codigo: (data.codigoWS!))
                }
            }else{
                let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: .alert)
                alerta.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil));
                self.present(alerta, animated: true, completion: nil)
            }
        }
    }
    @objc func endCargarFotoTercero4(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        if self.tipoConsulta == "D"{
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "ConsultaReporteDetalladoVC") as! ConsultaReporteDetalladoViewController
            self.navigationController?.pushViewController(nav, animated: true)
        }
        else if self.tipoConsulta == "R"
        {
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "ConsultaReporteResumidoVC") as! ConsultaReporteResumidoViewController
            self.navigationController?.pushViewController(nav, animated: true)
        }
        else{
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "ConsultaReporteDetalladoVC") as! ConsultaReporteDetalladoViewController
            self.navigationController?.pushViewController(nav, animated: true)
        }
    }
    
    func iniConsulta( tipoDoc : String){
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.endConsulta(_:)), name: NSNotification.Name("endConsulta"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        let usuarioBean : UserClass = UserGlobalData.sharedInstance.userGlobal
        
        //let tipoDoc : String = usuarioBean.TDocusu!
        var nroDoc : String = usuarioBean.NumDocumento!
        if tipoDoc == "D"{
            nroDoc = usuarioBean.NumDocumento!
        }
        else
        {
            nroDoc = usuarioBean.NroDocRel!
        }
        
        let sesion : String = usuarioBean.SesionId!
        
        let parametros = [
            "Usuario" : usuarioBean.user! as String,
            "Servicio" : "0",
            "TipoDocumento" :  tipoDoc,
            "NroDocumento" : nroDoc,
            "SesionId" : sesion,
            "origenAplicacion" : PATHS.APP_ID,
            "TipoConsulta" : "D",
            ] as [String : Any]
        OriginData.sharedInstance.miReporteDNI(notification: "endConsulta", parametros: parametros)
    }
    
    @objc func endConsulta(_ notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0//45789285 - juan316zack
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        
        if validaIndicador == 0 {
            
            let data = notification.object as! UserClass
            if data.codigoWS == "0" {
                UserGlobalData.sharedInstance.userGlobal.InfoTitular = data.InfoTitular
                
                let nav = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MiReporteDniVC") as! MiReporteDniViewController
                
                self.navigationController?.pushViewController(nav, animated: true)
            }
            else
            {
                if data.codigoWS != nil {
                    if data.codigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        iniMensajeError(codigo: (data.codigoWS!))
                    }
                }
                else
                {
                    let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                    alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                    self.present(alerta, animated: true, completion: nil)
                }
            }
        }
    }
    
    func iniBusquedaDocumentoFlash(tipoDoc : String, nroDoc : String){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endBusquedaDocumentoFlash(_:)), name: NSNotification.Name("endBusquedaDocumentoFlash"), object: nil)
        
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        let userData = UserGlobalData.sharedInstance.userGlobal!
        let sesion : String = userData.SesionId!
        
        let parametros = [
            "Usuario" : userData.user!,
            "Servicio" : userData.NroSerFl!,
            "TipoDocumento" :  tipoDoc,
            "NroDocumento" : nroDoc,
            "SesionId" : sesion,
            "origenAplicacion" : PATHS.APP_ID,
            "TipoConsulta" : "",
            ] as [String : Any]
        OriginData.sharedInstance.miReporteDNITercero(notification: "endBusquedaDocumentoFlash", parametros: parametros)
    }
    
    @objc func endBusquedaDocumentoFlash(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as! TerceroClass
            if data.codigoWS == "0"{
                UserGlobalData.sharedInstance.terceroGlobal.InfoTitular = data.InfoTitular
                let consultado: String = data.InfoTitular!["URLFoto"] as! String
                let url : URL = NSURL(string: consultado)! as URL
                self.clearTempFolder()
                NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarFotoTercero(_:)), name: NSNotification.Name("endCargarFotoTercero"), object: nil)
                
                self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
                Alamofire.request(url).responseImage { response in
                    debugPrint(response)
                    switch response.result {
                    case .success:
                        if let image = response.result.value {
                            _ = self.saveImageTercero(image: image)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoTercero"), object: nil)
                        }
                        break
                        
                    case .failure(_):
                        //CNIGenImaHom
                        let image : UIImage = UIImage.init(named: "CNIGenImaHom")!
                        _ = self.saveImageTercero(image: image)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFotoTercero"), object: nil)
                        break
                    }
                }
            }
            else
            {
                if data.codigoWS != nil {
                    if data.codigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        iniMensajeError(codigo: (data.codigoWS!))
                    }
                }
                else
                {
                    let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                    alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                    self.present(alerta, animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc func endCargarFotoTercero(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        let nav = UIStoryboard.init(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "MiReporteFlashVC") as! MiReporteFlashViewController
        self.navigationController?.pushViewController(nav, animated: true)
        
    }
    
    @objc func selectedCell(sender: UIButton)  {
        let objArray: NSDictionary = dataArr[sender.tag] as! NSDictionary
        let tipo = objArray.object(forKey: "TipoConsulta") as! String
        if tipo == "D"{
            self.tipoConsulta = "D"
            self.iniBusquedaDocumento(objHistorial: objArray)
        }
        else if tipo == "R"{
            self.tipoConsulta = "R"
            self.iniBusquedaDocumento(objHistorial: objArray)
        }
        else
        {
            let ndocumentoH : String = objArray.object(forKey: "AFSerCptNroDoc") as! String
            if ndocumentoH == UserGlobalData.sharedInstance.userGlobal.user! {
                self.tipoConsulta = "D"
                self.iniConsulta(tipoDoc: "D")
            }
            else
            {
                let tipoDoc : String = objArray.object(forKey: "AFSerCptTDoc") as! String
                let nroDoc : String = objArray.object(forKey: "AFSerCptNroDoc") as! String
                self.iniBusquedaDocumentoFlash(tipoDoc: tipoDoc, nroDoc: nroDoc)
            }
        }
    }
    // number of rows in table view
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return dataArr.count == 0 ? 1 : dataArr.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        if dataArr.count == 0 {
            return tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath)
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! HistorialEmpresarialTableViewCell
        let objArray: NSDictionary = dataArr[indexPath.row] as! NSDictionary
        let nombre : String = objArray.object(forKey: "AFSerCPTNomRaz") as! String
        cell.nombreLB.text = nombre
        let fechaDos = objArray.object(forKey: "CnpFchReg")! as! String
        let fechaTres = self.devuelveFecha(fecha: fechaDos)
        cell.fechaLB.text = fechaTres
        let tipo = objArray.object(forKey: "TipoConsulta") as! String
        if tipo == "D"{
            cell.tipoDetalleLB.text = "Reporte Detallado"
            cell.tipoDetalleLB.textColor = UIColor.fromHex(0x06B150)
        }
        else if tipo == "R"{
            cell.tipoDetalleLB.text = "Reporte Resumido"
            cell.tipoDetalleLB.textColor = UIColor.fromHex(0x0072BE)
        }
        else if tipo == ""{
            let ndocumentoH : String = objArray.object(forKey: "AFSerCptNroDoc") as! String
            if ndocumentoH == UserGlobalData.sharedInstance.userGlobal.user! {
                cell.tipoDetalleLB.text = "Consulta Gratuita"
                cell.tipoDetalleLB.textColor = UIColor.fromHex(0x06B150)
            }
            else
            {
                cell.tipoDetalleLB.text = "Reporte Flash"
                cell.tipoDetalleLB.textColor = UIColor.fromHex(0x0072BE)
            }
        }
        let semaforo : String = objArray.object(forKey: "CnpSemAct") as! String
        cell.semaforoIMG.image = self.devuelveSemaforo(SemAct: semaforo)
        cell.historialBT.tag = indexPath.row
        cell.historialBT.addTarget(self, action: #selector(self.selectedCell(sender:)), for: .touchUpInside)
        return cell
    }
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        let modo = UserGlobalData.sharedInstance.userGlobal!.EsPremium == "S" ? "pre" : "free"
        return IndicatorInfo(title: "", image: UIImage(named: String(format: "con_%@_hist_off", modo)), highlightedImage: UIImage(named: String(format: "con_%@_hist_on", modo)))
    }
}
