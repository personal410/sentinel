//
//  XLReporteResumidoViewController.swift
//  Sentinel
//
//  Created by Daniel on 1/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//XLReporteResumidoViewController
import UIKit
import XLPagerTabStrip
class XLReporteResumidoViewController: ButtonBarPagerTabStripViewController {
    ///
    // MARK: - Declaración de variables
    var ParentVC: UIViewController = UIViewController()
    
    var primerViewController:UIViewController!
    var lineasCreditoViewCont:LineasCreditoViewController!
    var segundoViewController:UIViewController!
    
    var isReload = false
    
    let graySpotifyColor = UIColor.fromHex(0x1A1A1A)
    var tabActivo: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.estilos()
        self.reloadPagerTabStripView()
    }
    
    func estilos(){
        // change selected bar color
        settings.style.buttonBarHeight = 44.0
        settings.style.buttonBarBackgroundColor = UIColor.fromHex(0x0072BE)
        settings.style.buttonBarItemBackgroundColor = UIColor.clear
        settings.style.selectedBarBackgroundColor =  UIColor.fromHex(0xffffff)
        settings.style.buttonBarItemFont = UIFont(name: "HelveticaNeue-Bold", size:12) ??
            UIFont.systemFont(ofSize: 12)
        
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor.clear
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        
        settings.style.buttonBarLeftContentInset = 5
        settings.style.buttonBarRightContentInset = 5
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
                oldCell?.label.textColor = UIColor(white: 1, alpha: 0.5)
                newCell?.label.textColor = UIColor.fromHex(rgbValue: 0xffffff)
                oldCell?.imageView.tintColor = UIColor(white: 1, alpha: 0.5)
                newCell?.imageView.tintColor = UIColor.fromHex(rgbValue: 0xffffff)
        }
        
        super.viewDidLoad()
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let storyboardMain = UIStoryboard(name: "Consultas", bundle: nil)
        
        primerViewController = storyboardMain.instantiateViewController(withIdentifier: "MiReporteDNIResumidoVC")
        
        lineasCreditoViewCont = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LineasCreditoVC") as! LineasCreditoViewController
        let usuarioBean = UserGlobalData.sharedInstance.userGlobal!
        let terceroBean = UserGlobalData.sharedInstance.terceroGlobal!
        lineasCreditoViewCont.servicio = usuarioBean.NroSerCT!
        lineasCreditoViewCont.tipoDocumento = terceroBean.tipoDocumentoTercero!
        lineasCreditoViewCont.nroDocumento = terceroBean.documentoTercero!
        lineasCreditoViewCont.fechaProceso = terceroBean.FechaReporte
        
        segundoViewController = storyboardMain.instantiateViewController(withIdentifier: "CreditosIndicadoresTerceroDetalladoVC")
        
        guard isReload else {
            return [primerViewController, lineasCreditoViewCont, segundoViewController]
        }
        
        var childViewControllers = [primerViewController, lineasCreditoViewCont, segundoViewController] as [Any]
        
        for (index, _) in childViewControllers.enumerated(){
            let nElements = childViewControllers.count - index
            let n = (Int(arc4random()) % nElements) + index
            if n != index{
                childViewControllers.swapAt(index, n)
            }
        }
        let nItems = 2
        return Array(childViewControllers.prefix(Int(nItems))) as! [UIViewController]
    }
    
    // MARK: - Métodos generales
    func actualizarTabContent(){
        //        self.homeViewController.arregloHome = homeLlenar
        //        self.homeViewController.cargarBanner()
    }
    
    // MARK: - Métodos PagerTabStripViewController
    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool) {
        super.updateIndicator(for: viewController, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: progressPercentage, indexWasChanged: indexWasChanged)
        self.tabActivo = self.currentIndex + 1
    }
    
    override func reloadPagerTabStripView() {
        super.reloadPagerTabStripView()
    }
}

