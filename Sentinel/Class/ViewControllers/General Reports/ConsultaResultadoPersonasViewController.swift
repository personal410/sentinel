//
//  ConsultaResultadoPersonasViewController.swift
//  Sentinel
//
//  Created by Daniel on 18/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class ConsultaResultadoPersonasViewController:ParentViewController {
    @IBOutlet weak var resumidoLB:UILabel!
    @IBOutlet weak var detalladoLB:UILabel!
    @IBOutlet weak var coincidenciasLB:UILabel!
    @IBOutlet weak var seleccionaTextoLB:UILabel!
    @IBOutlet weak var personasTable:UITableView!
    @IBOutlet weak var resultadoPersonaToolbar: UIToolbar!
    
    var apePaterno = "", apeMaterno = "", nombre = "", personaJuridica = "", otros = ""
    var tipoPersona:String!
    var arregloCoincidencias = NSArray()
    var tercero:TerceroClass?
    var userData:UserClass?
    var esPremium = false, esEmpresarial = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializarVariables()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.validarPremium()
        self.iniCoincidenciasEncontradas()
        self.iniConsultaDatosServicio()
        self.personasTable.reloadData()
    }
    func inicializarVariables(){
        self.resultadoPersonaToolbar.clipsToBounds = true
        self.tercero = UserGlobalData.sharedInstance.terceroGlobal
        self.userData = UserGlobalData.sharedInstance.userGlobal
        self.personasTable.dataSource = self
        self.personasTable.delegate = self
    }
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        let servicios = UserGlobalData.sharedInstance.misServiciosGlobal
        
        if userData?.EsPremium == "S" {
            self.esPremium = true
            
            if servicios?.TipServ == "6"
            {
                self.esEmpresarial = true
                self.changeStatusBar(cod: 2)
            }
            else
            {
                self.esEmpresarial = false
                self.changeStatusBar(cod: 3)
                self.setGradient(uiView: self.resultadoPersonaToolbar)
            }
        }
        else
        {
            if servicios?.TipServ == "6"{
                self.esEmpresarial = true
                self.changeStatusBar(cod: 2)
                self.resultadoPersonaToolbar.barTintColor = UIColor.fromHex(0x363636)
            }
            else
            {
                self.esEmpresarial = false
                self.esPremium = false
            }
        }
    }

    func iniCoincidenciasEncontradas(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endCoincidenciasEncontradas(_:)), name: NSNotification.Name("endCoincidenciasEncontradas"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        let userData = UserGlobalData.sharedInstance.userGlobal
        let tercero = UserGlobalData.sharedInstance.terceroGlobal
        let randomNumber : String = String(self.generateRandomNumber(numDigits: 7))
        var tipoPersonaTercero : String = ""
        let usuario : String = (userData?.user)!
        let sesion : String = (userData?.SesionId)!

        if self.tipoPersona == "1" {
            tipoPersonaTercero = "N"
            let parametros = [
                "UserId":usuario,
                "IdSession":randomNumber,
                "TipoPersona":tipoPersonaTercero,
                "Paterno":self.apePaterno,
                "Materno":self.apeMaterno,
                "nombres":self.nombre,
                "UseSeIDSession":sesion
                ] as [String : Any]
            OriginData.sharedInstance.getConsultaNombre(notification: "endCoincidenciasEncontradas", parametros: parametros)
        }
        else if self.tipoPersona == "2" {
            tipoPersonaTercero = "J"
            let parametros = [
                "UserId":usuario,
                "IdSession":randomNumber,
                "TipoPersona":tipoPersonaTercero,
                "Paterno":self.personaJuridica,
                "Materno":"",
                "nombres":"",
                "UseSeIDSession":sesion
                ] as [String : Any]
            OriginData.sharedInstance.getConsultaNombre(notification: "endCoincidenciasEncontradas", parametros: parametros)
        }
        else if self.tipoPersona == "3" {
            tipoPersonaTercero = "O"
            let parametros = [
                "UserId":usuario,
                "IdSession":randomNumber,
                "TipoPersona":tipoPersonaTercero,
                "Paterno":self.otros,
                "Materno":"",
                "nombres":"",
                "UseSeIDSession":sesion
                ] as [String : Any]
            OriginData.sharedInstance.getConsultaNombre(notification: "endCoincidenciasEncontradas", parametros: parametros)
        }
        else{
            let parametros = [
                "UserId":usuario,
                "IdSession":randomNumber,
                "TipoPersona":tipoPersonaTercero,
                "Paterno":self.apePaterno,
                "Materno":self.apeMaterno,
                "nombres":self.nombre,
                "UseSeIDSession":sesion
                ] as [String : Any]
            OriginData.sharedInstance.getConsultaNombre(notification: "endCoincidenciasEncontradas", parametros: parametros)
        }
    }
    
    @objc func endCoincidenciasEncontradas(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as? TerceroClass
            if data?.CodigoWS == "0"{
                UserGlobalData.sharedInstance.terceroGlobal.SDTBusAlf = data?.SDTBusAlf
                self.arregloCoincidencias = (data?.SDTBusAlf)!
                if self.arregloCoincidencias.count == 0 {
                     self.coincidenciasLB.text = "No hay coincidencias encontradas."
                }else if self.arregloCoincidencias.count == 1 {
                    self.coincidenciasLB.text = "Hay \(self.arregloCoincidencias.count) coincidencia encontrada."
                }
                else if self.arregloCoincidencias.count > 1 {
                    self.coincidenciasLB.text = "Hay \(self.arregloCoincidencias.count) coincidencias encontradas."
                }
                
                self.personasTable.reloadData()
            }
            else
            {
                if data?.CodigoWS != nil {
                    if data?.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "En estos momentos no se puede realizar la consulta. Intente luego.", preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                        //iniMensajeError(codigo: (data?.CodigoWS!)!)
                    }
                }
            }
        }
    }
    
    public func iniConsultaDatosServicio(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endConsultaDatosServicio(_:)), name: NSNotification.Name("endConsultaDatosServicio"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        usuarioParent = UserGlobalData.sharedInstance.userGlobal
        let usua : String = (usuarioParent?.NumDocumento)!
        let sesion : String = (usuarioParent?.SesionId)!
        let servicios = UserGlobalData.sharedInstance.misServiciosGlobal!
        let servicio : String = servicios.NroServicio!//(usuarioParent?.NroSerCT)!
        
        let parametros = [
            "Usuario": usua,
            "SesionId": sesion,
            "Servicio": servicio
            ] as [String : Any]
        OriginData.sharedInstance.consultaDatosServicio(notification: "endConsultaDatosServicio", parametros: parametros)
    }
    
    @objc func endConsultaDatosServicio(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let terceroLocal = notification.object as! TerceroClass
            UserGlobalData.sharedInstance.terceroGlobal = terceroLocal
            terceroParent = UserGlobalData.sharedInstance.terceroGlobal
            
            UserGlobalData.sharedInstance.terceroGlobal.DispDet = terceroLocal.SaldoServicio?.object(forKey: "DispDet") as? String
            UserGlobalData.sharedInstance.terceroGlobal.DispRes = terceroLocal.SaldoServicio?.object(forKey: "DispRes") as? String
            UserGlobalData.sharedInstance.terceroGlobal.SalFlash = terceroLocal.SaldoServicio?.object(forKey: "SalFlash") as? String
            self.resumidoLB.text = terceroLocal.SaldoServicio?.object(forKey: "DispRes") as? String
            self.detalladoLB.text = terceroLocal.SaldoServicio?.object(forKey: "DispDet") as? String
        }
    }

    @IBAction func accionComprar(_ sender: Any) {
        self.irComprarConsultas()
    }
    
    
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension ConsultaResultadoPersonasViewController:UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arregloCoincidencias.count
    }
    func tableView(_ tableView:UITableView, heightForRowAt indexPath:IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusquedaPersonasTVC", for: indexPath) as! BusquedaPersonasTableViewCell
        let persona = self.arregloCoincidencias[indexPath.row] as! NSDictionary
        let dni = persona.object(forKey: "numdoc") as! String
        let tipo = persona.object(forKey: "tipodoc") as! String
        if tipo == "D" {
            cell.dniLB.text = "DNI \(dni)"
            let fecha = persona.object(forKey: "adicional") as? String ?? ""
            if fecha.isEmpty || fecha == "00/00/0000" {
                cell.fechaLB.text = ""
            }else{
                cell.fechaLB.text = fecha
            }
        }else if tipo == "R" {
            cell.dniLB.text = "RUC \(dni)"
            cell.fechaLB.text = ""
        }else{
            cell.dniLB.text = "CE \(dni)"
            cell.fechaLB.text = ""
        }
        cell.nombreLB.text = persona.object(forKey: "descripcion") as? String
        return cell
    }
    func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath){
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "PopupVC") as! PopupViewController
        let persona : NSDictionary = self.arregloCoincidencias[indexPath.row] as! NSDictionary
        let dni = persona.object(forKey: "numdoc") as! String
        let tipo = persona.object(forKey: "tipodoc") as! String
        let userD = UserGlobalData.sharedInstance.userGlobal!
        if userD.NumDocumento == dni {
            self.showAlert(PATHS.SENTINEL, mensaje: "No puede elegirse a usted mismo como consulta")
        }else{
            var tipoCompleto : String = ""
            if tipo == "D" {
                tipoCompleto = "DNI"
            }else if tipo == "R" {
                tipoCompleto = "RUC"
            }else{
                tipoCompleto = "CE"
            }
            let nombre = persona.object(forKey: "descripcion") as! String
            let alerta = UIAlertController(title: PATHS.SENTINEL, message: "¿Desea consultar a \(nombre) con \(tipoCompleto): \(dni)?", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            alerta.addAction(UIAlertAction(title: "Aceptar", style: .default, handler:{(action) in
                nav.tipoDocTercero = tipo
                nav.documentoTercero = dni
                self.navigationController?.pushViewController(nav, animated: true)
            }))
            self.present(alerta, animated: true, completion: nil)
        }
    }
}
