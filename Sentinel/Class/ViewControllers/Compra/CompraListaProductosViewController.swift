//
//  CompraListaProductosViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 15/6/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import StoreKit
class CompraListaProductosViewController:ParentViewController, SKProductsRequestDelegate {
    //MARK: - Outlets
    @IBOutlet weak var toolbar:UIToolbar!
    @IBOutlet weak var lblTitulo:UILabel!
    @IBOutlet weak var lblSubtitulo:UILabel!
    @IBOutlet weak var lblDescripcion:UILabel!
    @IBOutlet weak var vUno:UIView!
    @IBOutlet weak var lblDescUno:UILabel!
    @IBOutlet weak var lblPrecioUno:UILabel!
    @IBOutlet weak var vDos:UIView!
    @IBOutlet weak var lblDescDos:UILabel!
    @IBOutlet weak var lblPrecioDos:UILabel!
    @IBOutlet weak var vTres:UIView!
    @IBOutlet weak var lblDescTres:UILabel!
    @IBOutlet weak var lblPrecioTres:UILabel!
    @IBOutlet weak var lblCantidad:UILabel!
    @IBOutlet weak var lblTotal:UILabel!
    //MARK: - Props
    var arrProductosIds:[String] = []
    var subtitulo = "", descripcion = "", cantidadDesc = "", precio1 = "", precio2 = "", precio3 = "", total = "", productoId = ""
    var cantidadReportes = 0
    //MARK: - ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        lblSubtitulo.text = subtitulo
        lblDescripcion.text = descripcion
        configurarSobra(a: vUno)
        configurarSobra(a: vDos)
        configurarSobra(a: vTres)
        loadPrices()
    }
    override func viewDidLayoutSubviews(){
        let userData = UserGlobalData.sharedInstance.userGlobal!
        if userData.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(uiView: toolbar)
        }
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? CompraResumenViewController {
            viewCont.precio = total
            viewCont.cantReportes = cantidadReportes
            viewCont.consultas = cantidadDesc
            viewCont.producto = subtitulo
            viewCont.vigencia = "1 año"
            viewCont.productoId = productoId
        }
    }
    //MARK: - Actions
    @IBAction func atras(){
        navigationController?.popViewController(animated: true)
    }
    @IBAction func seleccionoProducto(_ btn:UIButton){
        let tag = btn.tag
        productoId = arrProductosIds[tag]
        if tag == 0 {
            total = precio1
            cantidadReportes = 1
        }else if tag == 1 {
            total = precio2
            cantidadReportes = 5
        }else if tag == 2 {
            total = precio3
            cantidadReportes = 10
        }
        lblTotal.text = "S/ \(total)"
        cantidadDesc = "\(cantidadReportes) Reporte\(cantidadReportes == 1 ? "" : "s")"
        lblCantidad.text = cantidadDesc
    }
    @IBAction func accionAdquirir(_ sender: Any) {
        if total != "" {
            performSegue(withIdentifier: "showResumen", sender: nil)
        }else{
            self.showAlert(PATHS.SENTINEL, mensaje: "Debe seleccionar un producto para seguir con la compra.")
        }
    }
    //MARK: - Auxiliar
    func configurarSobra(a v:UIView){
        v.layer.shadowColor = UIColor.lightGray.cgColor
        v.layer.shadowOpacity = 0.5
        v.layer.shadowOffset = CGSize.zero
    }
    func loadPrices(){
        showActivityIndicator()
        if(SKPaymentQueue.canMakePayments()){
            let productsRequest = SKProductsRequest(productIdentifiers: Set<String>(arrProductosIds))
            productsRequest.delegate = self
            productsRequest.start()
        }else{
            let alertCont = UIAlertController(title: "Alerta", message: "No se puede realizar compras", preferredStyle: .alert)
            alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                self.hideActivityIndicator()
                self.navigationController?.popViewController(animated: true)
            }))
            present(alertCont, animated: true, completion: nil)
        }
    }
    //MARK: - StoreKit
    func productsRequest(_ request:SKProductsRequest, didReceive response:SKProductsResponse){
        DispatchQueue.main.async {
            self.hideActivityIndicator()
            let products = response.products
            if products.count == 3 {
                for product in products {
                    let precio = Toolbox.getNumberFormmatter().string(from: product.price)!
                    if product.productIdentifier == self.arrProductosIds[0] {
                        self.precio1 = precio
                        self.lblPrecioUno.text = "S/ \(precio)"
                    }else if product.productIdentifier == self.arrProductosIds[1] {
                        self.precio2 = precio
                        self.lblPrecioDos.text = "S/ \(precio)"
                    }else if product.productIdentifier == self.arrProductosIds[2] {
                        self.precio3 = precio
                        self.lblPrecioTres.text = "S/ \(precio)"
                    }
                }
            }else{
                let alertCont = UIAlertController(title: "Alerta", message: "Hubo un error. Intentelo más tarde", preferredStyle: .alert)
                alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                    self.hideActivityIndicator()
                    self.navigationController?.popViewController(animated: true)
                }))
                self.present(alertCont, animated: true, completion: nil)
            }
        }
    }
}
