//
//  CompraContratoViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 16/6/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import StoreKit
class CompraContratoViewController:ParentViewController, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    //MARK: - Outlet
    @IBOutlet weak var toolbar:UIToolbar!
    @IBOutlet weak var btnAcepto:UIButton!
    //MARK: - props
    let userData = UserGlobalData.sharedInstance.userGlobal!
    var esAceptado = false, validaError = false
    var precio = "", productoId = "", correoElectronico = "", comprobantePago = "", ruc = "", codigoProducto = "", codigoTarifa = "", detalleContrato = "", referenceCode = ""
    var cantReportes = 0
    //MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
        inicializar()
        SKPaymentQueue.default().add(self)
    }
    override func viewDidLayoutSubviews(){
        if userData.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(uiView: toolbar)
        }
    }
    //MARK: - Actions
    @IBAction func atras(){
        navigationController?.popViewController(animated: true)
    }
    @IBAction func acepto(){
        esAceptado = !esAceptado
        btnAcepto.setImage(UIImage(named: esAceptado ? "check_on" : "check_off"), for: .normal)
    }
    @IBAction func finalizarCompra(){
        if esAceptado {
            iniPAYU()
        }else{
            let alertCont = UIAlertController(title: "ALERTA", message: "Debe aceptar los términos y condiciones.", preferredStyle: .alert)
            alertCont.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alertCont, animated: true, completion: nil)
        }
    }
    //MARK: - Auxiliar
    func inicializar(){
        if productoId == "com.sentinel.misentinel.recupera1" {
            codigoProducto = "35"
            codigoTarifa = "440"
        }else if productoId == "com.sentinel.misentinel.recupera5" {
            codigoProducto = "35"
            codigoTarifa = "441"
        }else if productoId == "com.sentinel.misentinel.recupera10" {
            codigoProducto = "35"
            codigoTarifa = "442"
        }else if productoId == "com.sentinel.misentinel.recupera.premium1" {
            codigoProducto = "35"
            codigoTarifa = "443"
        }else if productoId == "com.sentinel.misentinel.recupera.premium5" {
            codigoProducto = "35"
            codigoTarifa = "444"
        }else if productoId == "com.sentinel.misentinel.recupera.premium10" {
            codigoProducto = "35"
            codigoTarifa = "445"
        }
    }
    func iniPAYU(){
        print("iniPAYU")
        showActivityIndicator()
        let usuario = userData.user!
        let parametros = [
            "CNTPAYUCodProd": "\(codigoProducto)\(codigoTarifa)",
            "CNTPAYUCorreoFact": correoElectronico,
            "CNTPAYUNDoc": usuario,
            "CanCPT": String(cantReportes),
            "CodProd": codigoTarifa,
            "ForPag": "4",
            "NroRucPago": ruc,
            "TX_VALUE": precio,
            "TipDocPago": comprobantePago,
            "TipTar" : "4",
            "TipoProd" : codigoProducto,
            "UseSeIDSession" : userData.SesionId!,
            "buyerEmail" : userData.MailUsuario!,
            "description" : detalleContrato,
            "origenAplicacion": PATHS.APP_ID,
            "Usuario": usuario
            ] as [String : Any]
        NotificationCenter.default.addObserver(self, selector: #selector(self.endPAYU(_:)), name: NSNotification.Name("endPAYU"), object: nil)
        OriginData.sharedInstance.iniPAYU(notification: "endPAYU", parametros: parametros)
    }
    @objc func endPAYU(_ notification: NSNotification){
        print("endPAYU")
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        guard let data = notification.object as? MisServiciosClass, let codigoWS = data.CodigoWS else {
            hideActivityIndicator()
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if codigoWS == "0" {
            referenceCode = data.referenceCode!
            iniRegistroCuatro()
        }else{
            if codigoWS == "99" {
                showError99()
            }else{
                iniMensajeError(codigo: (data.codigoWS!))
            }
        }
    }
    func iniRegistroCuatro(){
        print("iniRegistroCuatro")
        if(SKPaymentQueue.canMakePayments()){
            let productsRequest = SKProductsRequest(productIdentifiers: Set<String>([productoId]))
            productsRequest.delegate = self
            productsRequest.start()
        }else{
            hideActivityIndicator()
            let alertCont = UIAlertController(title: "Alerta", message: "No se puede realizar compras", preferredStyle: .alert)
            alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alertCont, animated: true, completion: nil)
        }
    }
    func buyProduct(product:SKProduct){
        print("buyProduct")
        SKPaymentQueue.default().add(SKPayment(product: product))
    }
    func iniPAYUAPPSTORE(transaction:SKPaymentTransaction){
        print("iniPAYUAPPSTORE")
        showActivityIndicator()
        let transStateId = transaction.transactionState == .purchased ? 2 : 3
        var erroriOS = ""
        if let error = transaction.error {
            erroriOS = error.localizedDescription
        }
        var transactionDateiOS = ""
        if let transDate = transaction.transactionDate {
            transactionDateiOS = "\(transDate)"
        }
        let parametros = [
            "Usuario": userData.user!,
            "SesionId": userData.SesionId!,
            "referenceCode": referenceCode,
            "CodTrniOS": String(format: "%ld", transStateId),
            "CodErriOS": "0",
            "erroriOS": erroriOS,
            "paymentiOS": transaction.payment.productIdentifier,
            "transactionStateiOS" : transStateId,
            "transactionIdentifieriOS" : transaction.transactionIdentifier!,
            "transactionReceiptiOS" : "",
            "transactionDateiOS" : transactionDateiOS,
            ] as [String : Any]
        NotificationCenter.default.addObserver(self, selector: #selector(self.endPAYUAPPSTORE(_:)), name: NSNotification.Name("endPAYUAPPSTORE"), object: nil)
        OriginData.sharedInstance.iniPAYUAS(notification: "endPAYUAPPSTORE", parametros: parametros)
    }
    @objc func endPAYUAPPSTORE(_ notification:NSNotification){
        print("endPAYUAPPSTORE")
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard let data = notification.object as? MisServiciosClass else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            present(alerta, animated: true, completion: nil)
            return
        }
        if data.CodigoError == "0" {
            if self.validaError {
                let alertaVersion = UIAlertController(title: PATHS.SENTINEL, message: "Se produjo un error al realizar la compra. Compra no efectuada.", preferredStyle: .alert)
                alertaVersion.addAction(UIAlertAction(title: "Terminar", style: .default, handler: { (action: UIAlertAction!) in
                    self.navigationController?.popToRootViewController(animated: true)
                }))
                alertaVersion.addAction(UIAlertAction(title: "Volver a intentar", style: .cancel, handler: nil))
                present(alertaVersion, animated: true, completion: nil)
            }else{
                let dictionary:[String:Any] = ["TipoProd": codigoProducto, "SesionId" : userData.SesionId!, "Usuario" : userData.user!, "UsuTipDoc" : userData.TDocusu!]
                showActivityIndicator()
                NotificationCenter.default.addObserver(self, selector: #selector(self.endServicioTipoProd(_:)), name: NSNotification.Name("endServicioTipoProd"), object: nil)
                OriginData.sharedInstance.servicioTipoProd(notification: "endServicioTipoProd", parametros: dictionary)
            }
        }else{
            let alertaVersion = UIAlertController(title: PATHS.SENTINEL, message: data.ErrDescription, preferredStyle: .alert)
            alertaVersion.addAction(UIAlertAction(title: "Cancelar", style: .default, handler:{(action) in
                self.navigationController?.popToRootViewController(animated: true)
            }))
            alertaVersion.addAction(UIAlertAction(title: "Volver a intentar", style: .cancel, handler: nil))
            present(alertaVersion, animated: true, completion: nil)
        }
    }
    @objc func endServicioTipoProd(_ notification:Notification){
        print("endServicioTipoProd")
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard let data = notification.object as? ServicioTipoProdResponse, let codigoWs = data.codigoWs else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if codigoWs == "0" {
            UserGlobalData.sharedInstance.userGlobal.NroSerCT = data.servicio
            let navCont = navigationController!
            let indicePantalla = navCont.viewControllers.firstIndex(of: self)!
            navCont.popToViewController(navCont.viewControllers[indicePantalla - 5], animated: true)
        }else if codigoWs == "99" {
            showError99()
        }else{
            iniMensajeError(codigo: codigoWs)
        }
    }
    //MARK: - StoreKit
    func productsRequest(_ request:SKProductsRequest, didReceive response:SKProductsResponse){
        if let validProduct = response.products.first {
            if validProduct.productIdentifier == productoId {
                buyProduct(product: validProduct)
                return
            }
        }
        hideActivityIndicator()
        let alerta = UIAlertController(title: "ALERTA", message: "Hubo un error al reconocer el producto", preferredStyle: .alert)
        alerta.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alerta, animated: true, completion: nil)
    }
    func paymentQueue(_ queue:SKPaymentQueue, updatedTransactions transactions:[SKPaymentTransaction]){
        print("paymentQueue")
        for transaction in transactions {
            switch transaction.transactionState {
                case .purchased:
                    SKPaymentQueue.default().finishTransaction(transaction)
                    validaError = false
                    iniPAYUAPPSTORE(transaction: transaction)
                    break
                case .failed:
                    validaError = true
                    iniPAYUAPPSTORE(transaction: transaction)
                    break
                case .restored:
                    hideActivityIndicator()
                    SKPaymentQueue.default().finishTransaction(transaction)
                case .purchasing:
                    showActivityIndicator()
                    break
                default:
                    break
            }
        }
    }
    func paymentQueue(_ queue:SKPaymentQueue, restoreCompletedTransactionsFailedWithError error:Error){}
}
