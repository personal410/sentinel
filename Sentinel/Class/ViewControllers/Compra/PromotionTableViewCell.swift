//
//  PromotionTableViewCell.swift
//  Sentinel
//
//  Created by Victor Salazar on 4/10/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
class PromotionTableViewCell:UITableViewCell {
    //MARK: - Outlets
    @IBOutlet weak var vBack:UIView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblPreviousPrice:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
}
