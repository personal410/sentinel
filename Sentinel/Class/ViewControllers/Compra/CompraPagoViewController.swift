//
//  CompraPagoViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 16/6/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
class CompraPagoViewController:ParentViewController, MiPickerComboDelegate, UITextFieldDelegate {
    //MARK: - Outlets
    @IBOutlet weak var toolbar:UIToolbar!
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var lblProducto:UILabel!
    @IBOutlet weak var lblConsultas:UILabel!
    @IBOutlet weak var lblVigencia:UILabel!
    @IBOutlet weak var cpComprobante:CTComboPicker!
    @IBOutlet weak var tfComprobante:UITextField!
    @IBOutlet weak var tfCorreoElectronico:UITextField!
    @IBOutlet weak var tfRuc:UITextField!
    //MARK: - Props
    var producto = "", consultas = "", precio = "", vigencia = "", productoId = ""
    var cantReportes = 0, indiceComprobante = 0
    let arrComprobanteId = ["1", "2"]
    let arrComprobanteNombre = ["BOLETA", "FACTURA"]
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        let user = UserGlobalData.sharedInstance.userGlobal!
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        view.addGestureRecognizer(tapGesture)
        cpComprobante.layer.shadowColor = UIColor.lightGray.cgColor
        cpComprobante.layer.shadowOpacity = 0.5
        cpComprobante.layer.shadowOffset = .zero
        cpComprobante.setConfig(arrComprobanteNombre)
        cpComprobante.delegate = self
        lblProducto.text = producto
        lblConsultas.text = "Consultas: \(consultas)"
        lblVigencia.text = "Vigencia: \(vigencia)"
        tfCorreoElectronico.text = user.MailUsuario
    }
    override func viewDidLayoutSubviews() {
        let user = UserGlobalData.sharedInstance.userGlobal!
        if user.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(uiView: toolbar)
        }
    }
    override func viewWillAppear(_ animated:Bool){
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboard(_:)), name: .UIKeyboardWillHide, object: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? CompraContratoViewController {
            viewCont.precio = precio
            viewCont.productoId = productoId
            viewCont.cantReportes = cantReportes
            viewCont.correoElectronico = tfCorreoElectronico.text!
            viewCont.comprobantePago = indiceComprobante == 0 ? "B" : "F"
            viewCont.ruc = indiceComprobante == 0 ? "" : tfRuc.text!
            viewCont.detalleContrato = "\(producto) \(lblConsultas.text!)"
        }
    }
    //MARK: - Actions
    @IBAction func atras(){
        navigationController?.popViewController(animated: true)
    }
    @IBAction func mostrarComprobantes(){
        cpComprobante.open()
    }
    @IBAction func adquirir(){
        if tfCorreoElectronico.text!.isEmpty {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe agregar el correo destinatario.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if !Toolbox.validateEmail(tfCorreoElectronico.text!) {
            let alerta = UIAlertController(title: "ALERTA", message: "El correo ingresado es inválido.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if indiceComprobante == 1 {
            let ruc = tfRuc.text!
            if ruc.isEmpty {
                let alerta = UIAlertController(title: "ALERTA", message: "Debe agregar el número de RUC.", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                present(alerta, animated: true, completion: nil)
                return
            }else{
                if ruc.count != 11 {
                    let alerta = UIAlertController(title: "ALERTA", message: "El número de RUC debe contener 11 caracteres.", preferredStyle: UIAlertControllerStyle.alert)
                    alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                    present(alerta, animated: true, completion: nil)
                    return
                }
            }
            showActivityIndicator()
            let user = UserGlobalData.sharedInstance.userGlobal!
            let dicParams = ["NDoc": ruc, "Usuario": user.user!, "SesionId": user.SesionId!, "TDoc": "R"]
            ServiceConnector.connectToUrl("\(PATHS.PATHSENTINEL)RWS_MSTitulares", params: dicParams){(r, e) in
                self.hideActivityIndicator()
                if e == nil, let dicResult = r as? [String: Any], let codigoWs = dicResult["CodigoWS"] as? String, codigoWs == "0" {
                    if let nombreRazSoc = dicResult["NombreRazSoc"] as? String, !nombreRazSoc.isEmpty {
                        self.continuar()
                    }else{
                        Toolbox.showAlert(with: "Alerta", and: "RUC no válido", in: self)
                    }
                }else{
                    Toolbox.showAlert(with: "Alerta", and: "Hubo un error inesperado", in: self)
                }
            }
        }else{
            continuar()
        }
    }
    //MARK: - TextField
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfCorreoElectronico {
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 200
        }else if textField == tfRuc {
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 10
        }
        return true
    }
    //MARK: - ComboPicker
    func miPickerCombo(_ pickerid:Int, didSelectRow row:Int){
        indiceComprobante = row
        tfRuc.isHidden = indiceComprobante == 0
    }
    func miPickerCombo(_ MiPickerView:CTComboPicker!, pickerView picker:UIPickerView!, didSelectRow row: Int, inComponent component:Int){}
    //MARK: - Auxiliar
    @objc func myviewTapped(_ sender:UITapGestureRecognizer){
        self.view.endEditing(true)
    }
    @objc func showKeyboard(_ notification:NSNotification){
        let newContentInset = UIEdgeInsets(top: 0, left: 0, bottom: (notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue.height - 121, right: 0)
        scrollView.contentInset = newContentInset
        scrollView.scrollIndicatorInsets = newContentInset
    }
    @objc func hideKeyboard(_ notification:NSNotification){
        scrollView.contentInset = .zero
        scrollView.scrollIndicatorInsets = .zero
    }
    func continuar(){
        performSegue(withIdentifier: "showContrato", sender: nil)
    }
}
