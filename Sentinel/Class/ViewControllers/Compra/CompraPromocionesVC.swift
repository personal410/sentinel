//
//  CompraPromocionesVC.swift
//  Sentinel
//
//  Created by Victor Salazar on 20/9/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
class CompraPromocionesVC:ParentViewController, UITableViewDataSource, UITableViewDelegate {
    //MARK: - Props
    var arrProductos:[[String: Any]] = []
    //MARK: - Outlets
    @IBOutlet weak var lblDescripcion:UILabel!
    @IBOutlet weak var tvProducts:UITableView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        let attributedString = NSMutableAttributedString(attributedString: lblDescripcion.attributedText!)
        let fontRobotoBold = UIFont(name: "Roboto-Bold", size: 15)!
        attributedString.addAttribute(.font, value: fontRobotoBold, range: NSRange(location: 0, length: 37))
        lblDescripcion.attributedText = attributedString
        showActivityIndicator()
        let user = UserGlobalData.sharedInstance.userGlobal!
        let dicParams = ["Usuario": user.user!, "SesionId": user.SesionId!, "CodTarifa": "65"]
        ServiceConnector.connectToUrl("\(PATHS.PATHSENTINEL)RWS_MSTarifario", params: dicParams){(r, e) in
            self.hideActivityIndicator()
            if e == nil, let dicResult = r as? [String: Any], let codigoWs = dicResult["CodigoWS"] as? String, codigoWs == "0", let arrPromos = dicResult["Tarifario"] as? [[String: Any]], !arrPromos.isEmpty {
                self.arrProductos = arrPromos
                self.tvProducts.reloadData()
            }
        }
    }
    //MARK: - Actions
    @IBAction func actionAtras(){
        navigationController?.popViewController(animated: true)
    }
    //MARK: - Auxiliar
    func addBorder(to view:UIView){
        view.layer.cornerRadius = 5
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize.zero
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arrProductos.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "promotionCell", for: indexPath) as! PromotionTableViewCell
        addBorder(to: cell.vBack)
        let attributedString = NSMutableAttributedString(string: cell.lblPreviousPrice.text!, attributes: [NSAttributedStringKey.strikethroughStyle : 2])
        cell.lblPreviousPrice.attributedText = attributedString
        let product = arrProductos[indexPath.row]
        cell.lblName.text = product["FaTarDes"] as? String
        cell.lblPrice.text = "S/ \(product["FATarMonTotal"] as? String ?? "0.00")"
        return cell
    }
}
