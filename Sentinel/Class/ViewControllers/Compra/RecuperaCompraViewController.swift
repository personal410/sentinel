//
//  RecuperaCompraViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 10/6/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
class RecuperaCompraViewController:ParentViewController {
    //MARK: - Outlets
    @IBOutlet weak var lblDescripcion:UILabel!
    @IBOutlet weak var lblDesde:UILabel!
    @IBOutlet weak var vAquirir:UIView!
    //MARK: - Props
    var esPremium = UserGlobalData.sharedInstance.userGlobal!.EsPremium == "S"
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        vAquirir.layer.cornerRadius = 5
        vAquirir.layer.shadowColor = UIColor.lightGray.cgColor
        vAquirir.layer.shadowOpacity = 0.5
        vAquirir.layer.shadowOffset = CGSize.zero
        var texto = ""
        if esPremium {
            topBarView.backgroundColor = UIColor(hex: "D18E18")
            texto = "Recupera desde S/ 9.90"
        }else{
            texto = "Recupera desde S/ 19.90"
        }
        lblDesde.text = texto
        let attributedString = NSMutableAttributedString(attributedString: lblDescripcion.attributedText!)
        let fontRobotoBold = UIFont(name: "Roboto-Bold", size: 15)!
        attributedString.addAttribute(.font, value: fontRobotoBold, range: NSRange(location: 0, length: 79))
        attributedString.addAttribute(.font, value: fontRobotoBold, range: NSRange(location: 167, length: 15))
        attributedString.addAttribute(.font, value: fontRobotoBold, range: NSRange(location: 381, length: 25))
        lblDescripcion.attributedText = attributedString
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewCont = segue.destination as? CompraListaProductosViewController {
            viewCont.subtitulo = "Recupera"
            viewCont.descripcion = "Indica la cantidad de Notificaciones y calcula el precio de compra."
            viewCont.arrProductosIds = esPremium ? ["com.sentinel.misentinel.recupera.premium1", "com.sentinel.misentinel.recupera.premium5", "com.sentinel.misentinel.recupera.premium10"] : ["com.sentinel.misentinel.recupera1", "com.sentinel.misentinel.recupera5", "com.sentinel.misentinel.recupera10"]
        }
    }
    //MARK: - Actions
    @IBAction func atras(){
        navigationController?.popViewController(animated: true)
    }
}
