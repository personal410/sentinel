//
//  CompraResumenViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 16/6/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
class CompraResumenViewController:ParentViewController {
    //MARK: - Outlets
    @IBOutlet weak var toolbar:UIToolbar!
    @IBOutlet weak var lblProducto:UILabel!
    @IBOutlet weak var lblConsultas:UILabel!
    @IBOutlet weak var lblVigencia:UILabel!
    @IBOutlet weak var lblTotal:UILabel!
    //MARK: - Props
    var producto = "", consultas = "", precio = "", vigencia = "", productoId = ""
    var cantReportes = 0
    //MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
        lblProducto.text = producto
        lblConsultas.text = "Consultas: \(consultas)"
        lblVigencia.text = "Vigencia: \(vigencia)"
        lblTotal.text = "S/ \(precio)"
    }
    override func viewDidLayoutSubviews(){
        if UserGlobalData.sharedInstance.userGlobal!.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(uiView: toolbar)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewCont = segue.destination as? CompraPagoViewController {
            viewCont.precio = precio
            viewCont.cantReportes = cantReportes
            viewCont.consultas = consultas
            viewCont.producto = producto
            viewCont.vigencia = "1 año"
            viewCont.productoId = productoId
        }
    }
    //MARK: - Actions
    @IBAction func atras(){
        navigationController?.popViewController(animated: true)
    }
}
