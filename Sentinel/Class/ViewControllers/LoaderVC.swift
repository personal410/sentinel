//
//  LoaderVC.swift
//  Sentinel
//
//  Created by Victor Salazar on 3/11/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
protocol LoaderVC {
    var cargandoMesSeleccionado:Bool { get set }
    var mesDefault:Int { get set }
}
