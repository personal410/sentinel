//
//  ConfiguraProductoUnoViewController.swift
//  Sentinel
//
//  Created by Daniel on 22/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
import StoreKit
class ConfiguraProductoUnoViewController:ParentViewController, SKProductsRequestDelegate {
    //MARK: - IBOutlets
    @IBOutlet weak var unReporteLB: UILabel!
    @IBOutlet weak var cincoReportesLB: UILabel!
    @IBOutlet weak var diezReportesLB: UILabel!
    @IBOutlet weak var totalLB: UILabel!
    @IBOutlet weak var adquirirBT: UIButton!
    @IBOutlet weak var productosToolBar: UIToolbar!
    @IBOutlet weak var primerView: UIView!
    @IBOutlet weak var segundoView: UIView!
    @IBOutlet weak var tercerView: UIView!
    @IBOutlet weak var numeroReportesLB: UILabel!
    @IBOutlet weak var lblOneUnitPrice:UILabel!
    @IBOutlet weak var lblFiveUnitsPrice:UILabel!
    @IBOutlet weak var lblTenUnitsPrice:UILabel!
    //MARK: - Variables
    var esPremium = false
    var cantidadReportes = "0"
    var total = "0"
    var oneUnitPrice = "", fiveUnitsPrice = "", tenUnitsPrice = ""
    //MARK: - ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        estilos()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.total = "0"
        validarPremium()
        loadPrices()
    }
    //MARK: - IBActions
    @IBAction func accion1Reporte(_ sender:Any){
        self.totalLB.text = "S/ \(oneUnitPrice)"
        self.total = oneUnitPrice
        self.cantidadReportes = "1"
        self.numeroReportesLB.text = "1 Reporte"
    }
    @IBAction func accion5reportes(_ sender:Any){
        self.totalLB.text = "S/ \(fiveUnitsPrice)"
        self.total = fiveUnitsPrice
        self.cantidadReportes = "5"
        self.numeroReportesLB.text = "5 Reportes"
    }
    @IBAction func accion10Reportes(_ sender:Any){
        self.totalLB.text = "S/ \(tenUnitsPrice)"
        self.total = tenUnitsPrice
        self.cantidadReportes = "10"
        self.numeroReportesLB.text = "10 Reportes"
    }
    @IBAction func accionSalir(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func accionAdquirir(_ sender: Any) {
        if self.total != "0" {
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "ResumenCompraVC") as! ResumenCompraViewController
            nav.total = self.total
            nav.cantidad = self.cantidadReportes
            nav.tipoReporte = "Detallado"
            self.navigationController?.pushViewController(nav, animated: true)
        }else{
            self.showAlert(PATHS.SENTINEL, mensaje: "Debe seleccionar un producto para seguir con la compra.")
        }
    }
    //MARK: - Methods
    func estilos(){
        self.primerView.layer.cornerRadius = 5
        self.segundoView.layer.cornerRadius = 5
        self.tercerView.layer.cornerRadius = 5
        self.adquirirBT.layer.cornerRadius = 5
        
        primerView.layer.shadowColor = UIColor.lightGray.cgColor
        primerView.layer.shadowOpacity = 0.5
        primerView.layer.shadowOffset = CGSize.zero
        segundoView.layer.shadowColor = UIColor.lightGray.cgColor
        segundoView.layer.shadowOpacity = 0.5
        segundoView.layer.shadowOffset = CGSize.zero
        tercerView.layer.shadowColor = UIColor.lightGray.cgColor
        tercerView.layer.shadowOpacity = 0.5
        tercerView.layer.shadowOffset = CGSize.zero
    }
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            self.esPremium = true
            self.changeStatusBar(cod: 3)
            self.setGradient(uiView: productosToolBar)
        }
    }
    func loadPrices(){
        showActivityIndicator(view: UIApplication.shared.keyWindow!)
        if(SKPaymentQueue.canMakePayments()){
            let productID = esPremium ? NSSet(array: ["com.sentinel.misentinel.consulta.premium.detallado.uno" as NSString, "com.sentinel.misentinel.consulta.premium.detallado.cinco" as NSString, "com.sentinel.misentinel.consulta.premium.detallado.diez" as NSString]) : NSSet(array: ["com.sentinel.misentinel.consulta.detallado.uno" as NSString, "com.sentinel.misentinel.consulta.detallado.cinco" as NSString, "com.sentinel.misentinel.consulta.detallado.diez" as NSString])
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>);
            productsRequest.delegate = self;
            productsRequest.start();
        }else{
            let alertCont = UIAlertController(title: "Alerta", message: "No se puede realizar compras", preferredStyle: .alert)
            alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                self.hideActivityIndicator()
                self.navigationController?.popViewController(animated: true)
            }))
            present(alertCont, animated: true, completion: nil)
        }
    }
    //MARK: - StoreKit
    func productsRequest(_ request:SKProductsRequest, didReceive response:SKProductsResponse) {
        DispatchQueue.main.async {
            self.hideActivityIndicator()
            let products = response.products
            if products.count == 3 {
                for product in products {
                    if product.productIdentifier == "com.sentinel.misentinel.consulta.detallado.uno" {
                        self.oneUnitPrice = Toolbox.getNumberFormmatter().string(from: product.price)!
                        self.lblOneUnitPrice.text = "S/ \(self.oneUnitPrice)"
                    }else if product.productIdentifier == "com.sentinel.misentinel.consulta.detallado.cinco" {
                        self.fiveUnitsPrice = Toolbox.getNumberFormmatter().string(from: product.price)!
                        self.lblFiveUnitsPrice.text = "S/ \(self.fiveUnitsPrice)"
                    }else if product.productIdentifier == "com.sentinel.misentinel.consulta.detallado.diez" {
                        self.tenUnitsPrice = Toolbox.getNumberFormmatter().string(from: product.price)!
                        self.lblTenUnitsPrice.text = "S/ \(self.tenUnitsPrice)"
                    }else if product.productIdentifier == "com.sentinel.misentinel.consulta.premium.detallado.uno" {
                        self.oneUnitPrice = Toolbox.getNumberFormmatter().string(from: product.price)!
                        self.lblOneUnitPrice.text = "S/ \(self.oneUnitPrice)"
                    }else if product.productIdentifier == "com.sentinel.misentinel.consulta.premium.detallado.cinco" {
                        self.fiveUnitsPrice = Toolbox.getNumberFormmatter().string(from: product.price)!
                        self.lblFiveUnitsPrice.text = "S/ \(self.fiveUnitsPrice)"
                    }else if product.productIdentifier == "com.sentinel.misentinel.consulta.premium.detallado.diez" {
                        self.tenUnitsPrice = Toolbox.getNumberFormmatter().string(from: product.price)!
                        self.lblTenUnitsPrice.text = "S/ \(self.tenUnitsPrice)"
                    }
                }
            }else{
                let alertCont = UIAlertController(title: "Alerta", message: "Hubo un error. Intentelo más tarde", preferredStyle: .alert)
                alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                    self.hideActivityIndicator()
                    self.navigationController?.popViewController(animated: true)
                }))
                self.present(alertCont, animated: true, completion: nil)
            }
        }
    }
}
