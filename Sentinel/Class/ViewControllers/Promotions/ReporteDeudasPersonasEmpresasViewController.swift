//
//  ReporteDeudasPersonasEmpresasViewController.swift
//  Sentinel
//
//  Created by Daniel on 22/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class ReporteDeudasPersonasEmpresasViewController:ParentViewController {
    @IBOutlet weak var productosToolBar:UIView!
    @IBOutlet weak var resumidoLB:UILabel!
    @IBOutlet weak var detalleLB:UILabel!
    @IBOutlet weak var primerView:UIView!
    @IBOutlet weak var segundoView:UIView!
    @IBOutlet weak var tercerView:UIView!
    @IBOutlet weak var cuadroUno:UIView!
    @IBOutlet weak var cuadroDos:UIView!
    @IBOutlet weak var cuadroTres:UIView!
    @IBOutlet weak var cuadroCuatro:UIView!
    
    var esPremium : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.estilos()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.validarPremium()
    }

    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            self.esPremium = true
            self.changeStatusBar(cod: 3)
            self.setGradient(uiView: self.productosToolBar)
            self.resumidoLB.text = "Resumido desde S/ 9.90"
            self.detalleLB.text = "Detallado desde S/ 12.90"
        }
    }
    func estilos(){
        primerView.layer.cornerRadius = 5
        primerView.layer.shadowColor = UIColor.lightGray.cgColor
        primerView.layer.shadowOpacity = 0.5
        primerView.layer.shadowOffset = CGSize.zero
        
        segundoView.layer.cornerRadius = 5
        segundoView.layer.shadowColor = UIColor.lightGray.cgColor
        segundoView.layer.shadowOpacity = 0.5
        segundoView.layer.shadowOffset = CGSize.zero
        
        tercerView.layer.cornerRadius = 5
        tercerView.layer.shadowColor = UIColor.lightGray.cgColor
        tercerView.layer.shadowOpacity = 0.5
        tercerView.layer.shadowOffset = CGSize.zero
        
        cuadroUno.layer.cornerRadius = self.cuadroUno.layer.bounds.size.width / 2
        cuadroDos.layer.cornerRadius = self.cuadroDos.layer.bounds.size.width / 2
        cuadroTres.layer.cornerRadius = self.cuadroTres.layer.bounds.size.width / 2
        cuadroCuatro.layer.cornerRadius = self.cuadroCuatro.layer.bounds.size.width / 2
    }
    
    @IBAction func accionResumido(_ sender: Any) {
        let nav = (storyboard?.instantiateViewController(withIdentifier: "ConfiguraProductoDosVC"))!
        self.navigationController?.pushViewController(nav, animated: true)
    }
    @IBAction func accionDetallado(_ sender: Any) {
        let nav = (storyboard?.instantiateViewController(withIdentifier: "ConfiguraProductoUnoVC"))!
        self.navigationController?.pushViewController(nav, animated: true)
    }
    @IBAction func accionComparar(_ sender: Any) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "CompararResumidosDetalladosVC") as! CompararResumidosDetalladosViewController
        nav.comparativo = "0"
        self.navigationController?.pushViewController(nav, animated: true)
    }
    @IBAction func accionSalir(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
