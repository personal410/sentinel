//
//  ResumenCompraPasoDosViewController.swift
//  Sentinel
//
//  Created by Daniel Montoya on 17/12/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
// ResumenCompraPasoDosViewController
class ResumenCompraPasoDosViewController: ParentViewController, MiPickerComboDelegate, UITextFieldDelegate {
    @IBOutlet weak var tipoReporteLB: UILabel!
    @IBOutlet weak var consultasReporteLB: UILabel!
    @IBOutlet weak var vigenciaLB: UILabel!
    @IBOutlet weak var adquirirBT: UIButton!
    @IBOutlet weak var productosToolBar: UIToolbar!
    @IBOutlet weak var correoTXT: UITextField!
    @IBOutlet weak var tipoComprobanteTXT: UITextField!
    @IBOutlet weak var comboComprobante: CTComboPicker!
    @IBOutlet weak var topScroll: NSLayoutConstraint!
    @IBOutlet weak var svContent:UIScrollView!
    
    var esPremium = false
    var total = "0"
    var cantidad = "0"
    var tipoReporte = ""
    var productoNumero : String = ""
    var TipDocPago : String = "B"
    var codComprobante = "1"
    var nomComprobante = "BOLETA"
    var isKeyboardAppear = false
    
    var combosArray = NSMutableArray()
    var combosIDArray = NSMutableArray()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.validarPremium()
        self.gestureScreen()
        self.estilos()
        self.cargarData()
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboard(_:)), name: .UIKeyboardWillHide, object: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            self.esPremium = true
            self.changeStatusBar(cod: 3)
            self.setGradient(uiView: self.productosToolBar)
        }
    }
    func estilos(){
        self.adquirirBT.layer.cornerRadius = 5
        self.comboComprobante.layer.cornerRadius = 5
        comboComprobante.layer.shadowColor = UIColor.lightGray.cgColor
        comboComprobante.layer.shadowOpacity = 0.5
        comboComprobante.layer.shadowOffset = CGSize.zero
    }
    func cargarData(){
        if tipoReporte == "PremiumM"{
            self.tipoReporteLB.text = "Sentinel Premium Mensual"
            self.consultasReporteLB.text = "Consultas: Flash ilimitadas"
            self.vigenciaLB.text = "Vigencia: 1 mes"
        }else if tipoReporte == "PremiumA"{
            self.tipoReporteLB.text = "Sentinel Premium Anual"
            self.consultasReporteLB.text = "Consultas: Flash ilimitadas"
        }else{
            if tipoReporte == "Detallado"{
                self.tipoReporteLB.text = "Reporte de Deudas Detallado"
            }
            else if tipoReporte == "Resumido"{
                self.tipoReporteLB.text = "Reporte de Deudas Resumido"
            }
            if cantidad == "1"{
                self.consultasReporteLB.text = "Consultas: \(cantidad) Reporte"
            }
            else if cantidad > "0"{
                self.consultasReporteLB.text = "Consultas: \(cantidad) Reportes"
            }
        }
        self.comboComprobante.delegate = self
        self.correoTXT.delegate = self
        self.tipoComprobanteTXT.delegate = self
        self.combosArray.add("BOLETA")
        self.combosArray.add("FACTURA")
        self.combosIDArray.add("1")
        self.combosIDArray.add("2")
        
        if UserGlobalData.sharedInstance.userGlobal.MailUsuario != nil{
            self.correoTXT.text = UserGlobalData.sharedInstance.userGlobal.MailUsuario!
        }
    }
    
    func validar() -> Bool{
        if self.correoTXT.text == "" {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe agregar el correo destinatario.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        let correo = self.correoTXT.text!
        
        if correo.isValidEmail() == false {
            let alerta = UIAlertController(title: "ALERTA", message: "El correo ingresado es inválido.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        if self.tipoComprobanteTXT.text == ""  && self.codComprobante == "2"{
            let alerta = UIAlertController(title: "ALERTA", message: "Debe agregar el número de RUC.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        if self.codComprobante == "2" {
            if self.tipoComprobanteTXT.text?.count != 11 {
                let alerta = UIAlertController(title: "ALERTA", message: "El número de RUC debe contener 11 caracteres.", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                self.present(alerta, animated: true, completion: nil)
                return false
            }
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.correoTXT {
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 200
        }
        if textField == self.tipoComprobanteTXT{
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 10
        }
        return true
    }
    func miPickerCombo(_ pickerid:Int, didSelectRow row:Int){
        switch pickerid {
        case 1:
            codComprobante = combosIDArray[row] as! String
            nomComprobante = combosArray[row] as! String
            if codComprobante == "2"{
                self.tipoComprobanteTXT.isHidden = false
                self.tipoComprobanteTXT.text = ""
                self.TipDocPago = "F"
            }else{
                self.tipoComprobanteTXT.isHidden = true
                self.tipoComprobanteTXT.text = ""
                self.TipDocPago = "B"
            }
            break
        default:
            break
        }
    }
    func miPickerCombo(_ MiPickerView:CTComboPicker!, pickerView picker:UIPickerView!, didSelectRow row:Int, inComponent component:Int){}
    
    @objc func showKeyboard(_ notification:NSNotification){
        let info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardHeight = keyboardSize.height
        let newContentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
        svContent.contentInset = newContentInset
        svContent.scrollIndicatorInsets = newContentInset
    }
    @objc func hideKeyboard(_ notification:NSNotification){
        let newContentInset = UIEdgeInsets.zero
        svContent.contentInset = newContentInset
        svContent.scrollIndicatorInsets = newContentInset
    }
    
    //MARK: - Actions
    @IBAction func accionCombo(_ sender: Any) {
        self.comboComprobante.setConfig(combosArray as? [Any])
        self.comboComprobante.open()
        self.comboComprobante.setId(1)
    }
    @IBAction func accionComboDos(_ sender: Any) {
        self.comboComprobante.setConfig(combosArray as? [Any])
        self.comboComprobante.open()
        self.comboComprobante.setId(1)
    }
    @IBAction func accionSalir(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func accionFinalizarCompra(_ sender: Any) {
        if self.validar() {
            if self.codComprobante == "2" {
                showActivityIndicator()
                let user = UserGlobalData.sharedInstance.userGlobal!
                let dicParams = ["NDoc": tipoComprobanteTXT.text!, "Usuario": user.user!, "SesionId": user.SesionId!, "TDoc": "R"]
                ServiceConnector.connectToUrl("\(PATHS.PATHSENTINEL)RWS_MSTitulares", params: dicParams){(r, e) in
                    self.hideActivityIndicator()
                    if e == nil, let dicResult = r as? [String: Any], let codigoWs = dicResult["CodigoWS"] as? String, codigoWs == "0" {
                        if let nombreRazSoc = dicResult["NombreRazSoc"] as? String, !nombreRazSoc.isEmpty {
                            self.continuar()
                        }else{
                            Toolbox.showAlert(with: "Alerta", and: "RUC no válido", in: self)
                        }
                    }else{
                        Toolbox.showAlert(with: "Alerta", and: "Hubo un error inesperado", in: self)
                    }
                }
            }else{
                continuar()
            }
        }
    }
    
    func continuar(){
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "ContratoCompraVC") as! ContratoCompraViewController
        let detalleContrato = "\(self.tipoReporteLB.text!) \(self.consultasReporteLB.text!)"
        if self.esPremium == true {
            if self.tipoReporte == "Detallado"{
                if self.cantidad == "1"{
                    nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_UNO_PREMIUM)
                    nav.total = self.total
                    nav.TipDocPago = self.TipDocPago
                    nav.correo = self.correoTXT.text!
                    nav.numRUC = self.tipoComprobanteTXT.text!
                    nav.detalleContrato = detalleContrato
                }
                else if self.cantidad == "5"{
                    nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_CINCO_PREMIUM)
                    nav.total = self.total
                    nav.TipDocPago = self.TipDocPago
                    nav.correo = self.correoTXT.text!
                    nav.numRUC = self.tipoComprobanteTXT.text!
                    nav.detalleContrato = detalleContrato
                }
                else if self.cantidad == "10"{
                    nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_DIEZ_PREMIUM)
                    nav.total = self.total
                    nav.TipDocPago = self.TipDocPago
                    nav.correo = self.correoTXT.text!
                    nav.numRUC = self.tipoComprobanteTXT.text!
                    nav.detalleContrato = detalleContrato
                }
            }
            if self.tipoReporte == "Resumido"{
                if self.cantidad == "1"{
                    nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_RESUMIDAS_UNO_PREMIUM)
                    nav.total = self.total
                    nav.TipDocPago = self.TipDocPago
                    nav.correo = self.correoTXT.text!
                    nav.numRUC = self.tipoComprobanteTXT.text!
                    nav.detalleContrato = detalleContrato
                }
                else if self.cantidad == "5"{
                    nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_RESUMIDAS_CINCO_PREMIUM)
                    nav.total = self.total
                    nav.TipDocPago = self.TipDocPago
                    nav.correo = self.correoTXT.text!
                    nav.numRUC = self.tipoComprobanteTXT.text!
                    nav.detalleContrato = detalleContrato
                }
                else if self.cantidad == "10"{
                    nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_RESUMIDAS_DIEZ_PREMIUM)
                    nav.total = self.total
                    nav.TipDocPago = self.TipDocPago
                    nav.correo = self.correoTXT.text!
                    nav.numRUC = self.tipoComprobanteTXT.text!
                    nav.detalleContrato = detalleContrato
                }
            }
        }else{
            if self.tipoReporte == "PremiumM"{
                nav.productoNumero = String(PATHS.TIENDA.SENTINEL_PREMIUM_MENSUAL)
                nav.total = self.total
                nav.TipDocPago = self.TipDocPago
                nav.correo = self.correoTXT.text!
                nav.numRUC = self.tipoComprobanteTXT.text!
                nav.detalleContrato = detalleContrato
            }
            else if self.tipoReporte == "PremiumA"{
                nav.productoNumero = String(PATHS.TIENDA.SENTINEL_PREMIUM_ANUAL)
                nav.total = self.total
                nav.TipDocPago = self.TipDocPago
                nav.correo = self.correoTXT.text!
                nav.numRUC = self.tipoComprobanteTXT.text!
                nav.detalleContrato = detalleContrato
            }else if self.tipoReporte == "Detallado"{
                if self.cantidad == "1"{
                    nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_UNO)
                    nav.total = self.total
                    nav.TipDocPago = self.TipDocPago
                    nav.correo = self.correoTXT.text!
                    nav.numRUC = self.tipoComprobanteTXT.text!
                    nav.detalleContrato = detalleContrato
                }
                else if self.cantidad == "5"{
                    nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_CINCO)
                    nav.total = self.total
                    nav.TipDocPago = self.TipDocPago
                    nav.correo = self.correoTXT.text!
                    nav.numRUC = self.tipoComprobanteTXT.text!
                    nav.detalleContrato = detalleContrato
                }
                else if self.cantidad == "10"{
                    nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_DIEZ)
                    nav.total = self.total
                    nav.TipDocPago = self.TipDocPago
                    nav.correo = self.correoTXT.text!
                    nav.numRUC = self.tipoComprobanteTXT.text!
                    nav.detalleContrato = detalleContrato
                }
            }else if self.tipoReporte == "Resumido"{
                if self.cantidad == "1"{
                    nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_RESUMIDAS_UNO)
                    nav.total = self.total
                    nav.TipDocPago = self.TipDocPago
                    nav.correo = self.correoTXT.text!
                    nav.numRUC = self.tipoComprobanteTXT.text!
                    nav.detalleContrato = detalleContrato
                }else if self.cantidad == "5"{
                    nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_RESUMIDAS_CINCO)
                    nav.total = self.total
                    nav.TipDocPago = self.TipDocPago
                    nav.correo = self.correoTXT.text!
                    nav.numRUC = self.tipoComprobanteTXT.text!
                    nav.detalleContrato = detalleContrato
                }else if self.cantidad == "10"{
                    nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_RESUMIDAS_DIEZ)
                    nav.total = self.total
                    nav.TipDocPago = self.TipDocPago
                    nav.correo = self.correoTXT.text!
                    nav.numRUC = self.tipoComprobanteTXT.text!
                    nav.detalleContrato = detalleContrato
                }
            }
        }
        self.navigationController?.pushViewController(nav, animated: true)
    }
}
