//
//  ProductosViewController.swift
//  Sentinel
//
//  Created by Daniel on 4/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class ProductosViewController:ParentViewController {
    //MARK: - Outlets
    @IBOutlet weak var productosToolBar:UIToolbar!
    @IBOutlet weak var productosTV:UITableView!
    //MARK: - Props
    var arrImagenes:[String] = []
    var arrTitulos:[String] = []
    var arrSubtitulos:[String] = []
    var arrOpciones:[Int] = []
    var mostrarCompraDetalladoResumen = false
    //MARK: - ViewCont
    override func viewWillAppear(_ animated:Bool){
        let esPremium = UserGlobalData.sharedInstance.userGlobal!.EsPremium == "S"
        arrImagenes = ["terceros3", "fondo_compra_recupera", "pyme3"]
        arrTitulos = ["Reporte de Deudas\nPersonas/Empresas", "Recupera", "Sentinel \nPyME"]
        arrSubtitulos = ["Aprovecha todo el poder de la App Sentinel", "Toma las mejores decisiones", "Recupera tu platita", "Tu negocio crece en tus manos."]
        arrOpciones = [1, 3, 2]
        if esPremium {
            changeStatusBar(cod: 3)
            setGradient(uiView: productosToolBar)
        }else{
            let position = 0
            arrImagenes.insert("imagen2", at: position)
            arrTitulos.insert("Sentinel\nPremium", at: position)
            arrSubtitulos.insert("Todo el poder de Sentinel a tu alcance.", at: position)
            arrOpciones.insert(0, at: position)
        }
        productosTV.reloadData()
        if mostrarCompraDetalladoResumen {
            mostrarCompraDetalladoResumen = false
            navigationController?.pushViewController(storyboard!.instantiateViewController(withIdentifier: "ReporteDeudasPersonasEmpresasVC"), animated: true)
        }
//        showActivityIndicator()
//        let user = UserGlobalData.sharedInstance.userGlobal!
//        let dicParams = ["UsuTDoc": user.TDocusu!, "Usuario": user.user!, "SesionId": user.SesionId!, "CampId": "6", "SegmId": "0"] as [String : Any]
//        ServiceConnector.connectToUrl("\(PATHS.PATHSENTINEL)RWS_MSObtienePromoUsuario", params: dicParams){(r, e) in
//            self.hideActivityIndicator()
//            if e == nil, let dicResult = r as? [String: Any], let codigoWs = dicResult["CodigoWS"] as? String, codigoWs == "0", let arrPromos = dicResult["RWS_MSPromoUsuario"] as? [Any], !arrPromos.isEmpty {
//                self.arrImagenes.insert("iPromociones", at: 0)
//                self.arrTitulos.insert("Promociones", at: 0)
//                self.arrSubtitulos.insert("Aprovecha todo el poder de la App Sentinel", at: 0)
//                self.arrOpciones.insert(4, at: 0)
//                self.productosTV.reloadData()
//            }
//        }
    }
    //MARK: - Actions
    @IBAction func accionMas(_ sender:UIButton){
        let nav = navigationController
        switch sender.tag {
            case 0:
                nav?.pushViewController(storyboard!.instantiateViewController(withIdentifier: "SentinelPremiumVC"), animated: true)
                break
            case 1:
                nav?.pushViewController(storyboard!.instantiateViewController(withIdentifier: "ReporteDeudasPersonasEmpresasVC"), animated: true)
                break
            case 2:
                nav?.pushViewController(storyboard!.instantiateViewController(withIdentifier: "SentinelPyMEVC"), animated: true)
                break
            case 3:
                nav?.pushViewController(UIStoryboard(name: "Compra", bundle: nil).instantiateViewController(withIdentifier: "RecuperaCompraVC"), animated: true)
                break
            case 4:
                nav?.pushViewController(UIStoryboard(name: "Compra", bundle: nil).instantiateViewController(withIdentifier: "CompraPromocionesVC"), animated: true)
                break
            default:
                break
        }
    }
}
extension ProductosViewController:UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView:UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrImagenes.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductoTVC", for: indexPath) as! ProductoTableViewCell
        cell.bannerIMG.image = UIImage(named: arrImagenes[indexPath.row])
        cell.tituloLB.text = arrTitulos[indexPath.row]
        cell.subTituloLB.text = arrSubtitulos[indexPath.row]
        cell.verMasBT.tag = arrOpciones[indexPath.row]
        return cell
    }
}
