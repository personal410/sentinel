//
//  ContratoCompraViewController.swift
//  Sentinel
//
//  Created by Daniel on 19/11/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
import StoreKit
class ContratoCompraViewController:ParentViewController, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    @IBOutlet weak var checkUnoBT: UIButton!
    @IBOutlet weak var productosToolBar: UIToolbar!
    @IBOutlet weak var aceptoBT: UIButton!

    var esPremium = false
    var validaError = false
    var valorUno = 0
    var dniUsuario = ""
    var claveUsuario = ""
    var productoNumero = ""
    var tipoProducto = ""
    var correlativo = ""
    var total = ""
    var detalle = ""
    var TipDocPago = ""
    var correo = ""
    var numRUC = ""
    var detalleContrato = ""
    var referenceCode = ""
    var cantProductos = ""
    
    var product_id: NSString = "com.sentinel.misentinel"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gestureScreen()
        self.estilos()
        self.validarPremium()
        self.inicializar()
        SKPaymentQueue.default().add(self)
    }
    func inicializar(){
        print("productoNumero: \(productoNumero)")
        if productoNumero == "60"{
            product_id = "com.sentinel.misentinel.premium"
            correlativo = "401"
            tipoProducto = "56"
            cantProductos = "1"
        }else if productoNumero == "73"{
            product_id = "com.sentinel.misentinel.premium2"
            correlativo = "402"
            tipoProducto = "56"
            cantProductos = "1"
        }else if productoNumero == "61"{
            product_id = "com.sentinel.misentinel.consulta.detallado.uno"
            correlativo = "403"
            tipoProducto = "10"
            cantProductos = "1"
        }else if productoNumero == "62"{
            product_id = "com.sentinel.misentinel.consulta.detallado.cinco"
            correlativo = "404"
            tipoProducto = "10"
            cantProductos = "5"
        }else if productoNumero == "63"{
            product_id = "com.sentinel.misentinel.consulta.detallado.diez"
            correlativo = "405"
            tipoProducto = "10"
            cantProductos = "10"
        }else if productoNumero == "64"{
            product_id = "com.sentinel.misentinel.consulta.resumido.uno"
            correlativo = "406"
            tipoProducto = "57"
            cantProductos = "1"
        }else if productoNumero == "65"{
            product_id = "com.sentinel.misentinel.consulta.resumido.cinco"
            correlativo = "407"
            tipoProducto = "57"
            cantProductos = "5"
        }else if productoNumero == "66"{
            product_id = "com.sentinel.misentinel.consulta.resumido.diez"
            correlativo = "408"
            tipoProducto = "57"
            cantProductos = "10"
        }else if productoNumero == "67"{
            product_id = "com.sentinel.misentinel.consulta.premium.detallado.uno"
            correlativo = "409"
            tipoProducto = "55"
            cantProductos = "1"
        }else if productoNumero == "68"{
            product_id = "com.sentinel.misentinel.consulta.premium.detallado.cinco"
            correlativo = "410"
            tipoProducto = "55"
            cantProductos = "5"
        }else if productoNumero == "69"{
            product_id = "com.sentinel.misentinel.consulta.premium.detallado.diez"
            correlativo = "411"
            tipoProducto = "55"
            cantProductos = "10"
        }else if productoNumero == "70"{
           product_id = "com.sentinel.misentinel.consulta.premium.resumido.uno"
            correlativo = "412"
            tipoProducto = "55"
            cantProductos = "1"
        }else if productoNumero == "71"{
            product_id = "com.sentinel.misentinel.consulta.premium.resumido.cinco"
            correlativo = "413"
            tipoProducto = "55"
            cantProductos = "5"
        }else if productoNumero == "72"{
            product_id = "com.sentinel.misentinel.consulta.premium.resumido.diez"
            correlativo = "414"
            tipoProducto = "55"
            cantProductos = "10"
        }
    }
    
    func estilos(){
        self.aceptoBT.layer.cornerRadius = 5
    }
    
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            self.esPremium = true
            self.changeStatusBar(cod: 3)
            self.setGradient(uiView: self.productosToolBar)
        }
    }
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func validar() -> Bool{
        if self.valorUno == 0 {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe aceptar los términos y condiciones.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func iniRegistroCuatro(){
        if (SKPaymentQueue.canMakePayments()) {
            let productID:NSSet = NSSet(array: [self.product_id as NSString]);
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>);
            productsRequest.delegate = self;
            productsRequest.start();
        } else {
            let alertCont = UIAlertController(title: "Alerta", message: "No se puede realizar compras", preferredStyle: .alert)
            alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alertCont, animated: true, completion: nil)
        }
    }
    
    func retornarTransaccionId(_ estado: Int) -> Int {
        var iden: Int = 0
        
        switch estado {
        case SKPaymentTransactionState.purchasing.rawValue:
            iden = 1
        case SKPaymentTransactionState.purchased.rawValue:
            iden = 2
        case SKPaymentTransactionState.failed.rawValue:
            iden = 3
        case SKPaymentTransactionState.restored.rawValue:
            iden = 4
        case SKPaymentTransactionState.deferred.rawValue:
            iden = 5
        default:
            break
        }
        
        return iden
    }
    
    func iniPAYU(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.endPAYU(_:)), name: NSNotification.Name("endPAYU"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        let usuarioBean = UserGlobalData.sharedInstance.userGlobal!
        let Usuario = usuarioBean.user!
        let SesionId = usuarioBean.SesionId!
        let CNTPAYUCodProd = "\(tipoProducto)\(correlativo)"
        let parametros = [
            "CNTPAYUCodProd" : CNTPAYUCodProd,
            "CNTPAYUCorreoFact" : self.correo,
            "CNTPAYUNDoc" : Usuario,
            "CanCPT" : cantProductos,
            "CodProd" : correlativo,
            "ForPag" : "4",
            "NroRucPago" : numRUC,
            "TX_VALUE" : self.total,
            "TipDocPago" : self.TipDocPago,
            "TipTar" : "4",
            "TipoProd" : self.tipoProducto,
            "UseSeIDSession" : SesionId,
            "buyerEmail" : UserGlobalData.sharedInstance.userGlobal.MailUsuario!,
            "description" : self.detalleContrato,
            "origenAplicacion" : PATHS.APP_ID,
            "Usuario" :  Usuario
            ] as [String : Any]
        print("iniPAYU-params: \(parametros)")
        OriginData.sharedInstance.iniPAYU(notification: "endPAYU", parametros: parametros)
    }
    @objc func endPAYU(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        guard notification.object != nil else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        let data = notification.object as! MisServiciosClass
        if data.CodigoWS == "0" {
            self.referenceCode = data.referenceCode!
            self.iniRegistroCuatro()
        }else{
            if data.codigoWS != nil {
                if data.codigoWS == "99"
                {
                    showError99()
                }else{
                    iniMensajeError(codigo: (data.codigoWS!))
                }
            }else{
                let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                self.present(alerta, animated: true, completion: nil)
            }
        }
    }
    func iniPAYUAPPSTORE(transaction : SKPaymentTransaction){
        let transactionId = retornarTransaccionId(transaction.transactionState.rawValue)
        let transactionStateiOS = transactionId
        let errorId = 0
        let CodTrniOS = String(format: "%ld", transactionId)
        let CodErriOS = String(format: "%ld", errorId)
        var erroriOS = ""
        let paymentiOS = transaction.payment.productIdentifier
        let transactionIdentifieriOS : String = transaction.transactionIdentifier!
        let transactionReceiptiOS = "" //solo compra ok
        var transactionDateiOS = ""
        if transaction.error != nil {
            erroriOS = (transaction.error?.localizedDescription)!
        }
        if transaction.transactionDate != nil {
            if let aDate = transaction.transactionDate {
                transactionDateiOS = "\(aDate)"
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.endPAYUAPPSTORE(_:)), name: NSNotification.Name("endPAYUAPPSTORE"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        let usuarioBean = UserGlobalData.sharedInstance.userGlobal!
        let Usuario = usuarioBean.user!
        let SesionId = usuarioBean.SesionId!
        let parametros = [
            "Usuario" : Usuario,
            "SesionId" : SesionId,
            "referenceCode" : self.referenceCode,
            "CodTrniOS" : CodTrniOS,
            "CodErriOS" : CodErriOS,
            "erroriOS" : erroriOS,
            "paymentiOS" : paymentiOS,
            "transactionStateiOS" : transactionStateiOS,
            "transactionIdentifieriOS" : transactionIdentifieriOS,
            "transactionReceiptiOS" : transactionReceiptiOS,
            "transactionDateiOS" : transactionDateiOS,
            ] as [String : Any]
        print("iniPAYUAS-params: \(parametros)")
        OriginData.sharedInstance.iniPAYUAS(notification: "endPAYUAPPSTORE", parametros: parametros)
    }
    
    @objc func endPAYUAPPSTORE(_ notification:NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        guard notification.object != nil  else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        let data = notification.object as! MisServiciosClass
        if data.CodigoError == "0" {
            if self.validaError{
                let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "Se produjo un error al realizar la compra. Compra no efectuada.", preferredStyle: .alert)
                let actionVersionGO = UIAlertAction.init(title: "Terminar", style: .default, handler: { (action: UIAlertAction!) in
                    self.navigationController?.popToRootViewController(animated: true)
                })
                let actionCancel = UIAlertAction.init(title: "Volver a intentar", style: .cancel, handler: nil)
                alertaVersion.addAction(actionVersionGO)
                alertaVersion.addAction(actionCancel)
                self.present(alertaVersion, animated: true, completion: nil)
            }else{
                let usuario = UserGlobalData.sharedInstance.userGlobal!
                let dictionary:[String:Any] = ["TipoProd": (self.productoNumero == "60" || self.productoNumero == "73") ? "20" : "14", "SesionId" : usuario.SesionId!, "Usuario" : usuario.user!, "UsuTipDoc" : usuario.TDocusu!]
                print("servicioTipoProd-params: \(dictionary)")
                NotificationCenter.default.addObserver(self, selector: #selector(self.endServicioTipoProd(_:)), name: NSNotification.Name("endServicioTipoProd"), object: nil)
                self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
                OriginData.sharedInstance.servicioTipoProd(notification: "endServicioTipoProd", parametros: dictionary)
            }
        }else{
            let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: data.ErrDescription, preferredStyle: .alert)
            let actionVersionGO = UIAlertAction.init(title: "Cancelar", style: .default, handler: { (action: UIAlertAction!) in
                self.navigationController?.popToRootViewController(animated: true)
            })
            let actionCancel = UIAlertAction.init(title: "Volver a intentar", style: .cancel, handler: nil)
            alertaVersion.addAction(actionVersionGO)
            alertaVersion.addAction(actionCancel)
            self.present(alertaVersion, animated: true, completion: nil)
        }
    }
    @objc func endServicioTipoProd(_ notification:Notification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        guard let data = notification.object as? ServicioTipoProdResponse else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if let codigoWs = data.codigoWs {
            if codigoWs == "0" {
                if self.productoNumero == "60" || self.productoNumero == "73" {
                    UserGlobalData.sharedInstance.userGlobal.NroSerFl = data.servicio
                    UserGlobalData.sharedInstance.userGlobal.EsPremium = "S"
                }else{
                    UserGlobalData.sharedInstance.userGlobal.NroSerCT = data.servicio
                }
                self.navigationController?.popToRootViewController(animated: true)
            }else if codigoWs == "99" {
                showError99()
            }else{
                iniMensajeError(codigo: codigoWs)
            }
        }else{
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
        }
    }
    func buyProduct(product: SKProduct) {
        showActivityIndicator()
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment);
    }
    func productsRequest(_ request:SKProductsRequest, didReceive response:SKProductsResponse){
        if let validProduct = response.products.first, validProduct.productIdentifier == self.product_id as String {
            DispatchQueue.main.async {
                self.buyProduct(product: validProduct)
            }
        }
    }
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction:AnyObject in transactions {
            if let trans:SKPaymentTransaction = transaction as? SKPaymentTransaction{
                switch trans.transactionState {
                case .purchased:
                    hideActivityIndicator()
                    print("Product Purchased")
                    self.validaError = false
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    self.iniPAYUAPPSTORE(transaction: transaction as! SKPaymentTransaction)
                    break;
                case .failed:
                    hideActivityIndicator()
                    print("Purchased Failed");
                    self.validaError = true
                    self.iniPAYUAPPSTORE(transaction: transaction as! SKPaymentTransaction)
                    break;
                case .restored:
                    hideActivityIndicator()
                    print("Already Purchased")
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                case .purchasing:
                    showActivityIndicator(view: UIApplication.shared.keyWindow!)
                    break
                default:
                    break;
                }
            }
        }
    }
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        // Show some alert
    }
    @IBAction func accionCheckPrimer(_ sender: Any) {
        if valorUno == 0 {
            valorUno = 1
            self.checkUnoBT.setImage(UIImage.init(named: "check_on"), for: .normal)
        }else{
            valorUno = 0
            self.checkUnoBT.setImage(UIImage.init(named: "check_off"), for: .normal)
        }
    }
    @IBAction func accionAcepto(_ sender: Any) {
        if validar() {
            self.iniPAYU()
        }
    }
    @IBAction func atrasAccion(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
