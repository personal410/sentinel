//
//  SentinelPyMEViewController.swift
//  Sentinel
//
//  Created by Daniel on 19/11/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class SentinelPyMEViewController: ParentViewController {

    @IBOutlet weak var primerView: UIView!
    @IBOutlet weak var tercerView: UIView!
    
    var esPremium : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.estilos()
        self.validarPremium()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            self.esPremium = true
            self.changeStatusBar(cod: 3)
            //self.setGradient(uiView: self.productosToolBar)
        }
    }
    
    func estilos(){
        self.primerView.layer.cornerRadius = 5
        self.tercerView.layer.cornerRadius = 5

        primerView.layer.shadowColor = UIColor.lightGray.cgColor
        primerView.layer.shadowOpacity = 0.5
        primerView.layer.shadowOffset = CGSize.zero
        
        tercerView.layer.shadowColor = UIColor.lightGray.cgColor
        tercerView.layer.shadowOpacity = 0.5
        tercerView.layer.shadowOffset = CGSize.zero
    }
    
    @IBAction func accionComparar(_ sender: Any) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "CompararPyMEVC") as! CompararPyMEViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    @IBAction func accionDescargar(_ sender: Any) {
        var urlString : String = "https://itunes.apple.com/pe/app/sentinel-pyme/id1077785278?mt=8"
        print(urlString)
        UIApplication.shared.open(URL(string: urlString)!, options: [:], completionHandler: nil)
        //http://www.sentinelperu.com/pol%C3%ADtica_de_privacidad.pdf
    }
    
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
