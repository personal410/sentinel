//
//  SentinelPremiumViewController.swift
//  Sentinel
//
//  Created by Daniel on 19/11/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
import StoreKit
class SentinelPremiumViewController:ParentViewController, SKProductsRequestDelegate {
    //MARK: - IBOutlets
    @IBOutlet weak var primerView:UIView!
    @IBOutlet weak var segundoView:UIView!
    @IBOutlet weak var tercerView:UIView!
    @IBOutlet weak var promocionView:UIView!
    @IBOutlet weak var promocionText:UILabel!
    @IBOutlet weak var lblMonthlyPrice:UILabel!
    @IBOutlet weak var lblAnnualPrice:UILabel!
    //MARK: - Variables
    var monthlyPrice = "", annualPrice = ""
    //MARK: - ViewController
    override func viewDidLoad(){
        super.viewDidLoad()
        self.estilos()
        loadPrices()
    }
    //MARK: - IBActions
    @IBAction func accionSalir(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func accionComparar(_ sender: Any) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "CompararResumidosDetalladosVC") as! CompararResumidosDetalladosViewController
        nav.comparativo = "1"
        self.navigationController?.pushViewController(nav, animated: true)
    }
    @IBAction func accionPlanMensual(_ sender: Any) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "ResumenCompraVC") as! ResumenCompraViewController
        nav.tipoReporte = "PremiumM"
        nav.total = monthlyPrice
        nav.cantidad = "1"
        self.navigationController?.pushViewController(nav, animated: true)
    }
    @IBAction func accionplanAnual(_ sender: Any) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "ResumenCompraVC") as! ResumenCompraViewController
        nav.tipoReporte = "PremiumA"
        nav.total = annualPrice
        nav.cantidad = "1"
        self.navigationController?.pushViewController(nav, animated: true)
    }
    //MARK: - Others
    func estilos(){
        primerView.layer.cornerRadius = 5
        primerView.layer.shadowColor = UIColor.lightGray.cgColor
        primerView.layer.shadowOpacity = 0.5
        primerView.layer.shadowOffset = CGSize.zero
        
        segundoView.layer.cornerRadius = 5
        segundoView.layer.shadowColor = UIColor.lightGray.cgColor
        segundoView.layer.shadowOpacity = 0.5
        segundoView.layer.shadowOffset = CGSize.zero
        
        tercerView.layer.cornerRadius = 5
        tercerView.layer.shadowColor = UIColor.lightGray.cgColor
        tercerView.layer.shadowOpacity = 0.5
        tercerView.layer.shadowOffset = CGSize.zero
        
        promocionView.layer.cornerRadius = 10
        promocionText.layer.cornerRadius = 10
        promocionText.clipsToBounds = true
    }
    func loadPrices(){
        showActivityIndicator(view: UIApplication.shared.keyWindow!)
        if(SKPaymentQueue.canMakePayments()){
            let productID:NSSet = NSSet(array: ["com.sentinel.misentinel.premium" as NSString, "com.sentinel.misentinel.premium2" as NSString])
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>);
            productsRequest.delegate = self;
            productsRequest.start();
        }else{
            let alertCont = UIAlertController(title: "Alerta", message: "No se puede realizar compras", preferredStyle: .alert)
            alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                self.hideActivityIndicator()
                self.navigationController?.popViewController(animated: true)
            }))
            present(alertCont, animated: true, completion: nil)
        }
    }
    //MARK: - StoreKit
    func productsRequest(_ request:SKProductsRequest, didReceive response:SKProductsResponse){
        DispatchQueue.main.async {
            self.hideActivityIndicator()
            let products = response.products
            if products.count == 2 {
                for product in products {
                    if product.productIdentifier == "com.sentinel.misentinel.premium" {
                        self.monthlyPrice = Toolbox.getNumberFormmatter().string(from: product.price)!
                        self.lblMonthlyPrice.text = "Plan Mensual S/ \(self.monthlyPrice)"
                    }else{
                        self.annualPrice = Toolbox.getNumberFormmatter().string(from: product.price)!
                        self.lblAnnualPrice.text = "Plan Anual S/ \(self.annualPrice)"
                    }
                }
            }else{
                let alertCont = UIAlertController(title: "Alerta", message: "Hubo un error. Intentelo más tarde", preferredStyle: .alert)
                alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                    self.hideActivityIndicator()
                    self.navigationController?.popViewController(animated: true)
                }))
                self.present(alertCont, animated: true, completion: nil)
            }
        }
    }
}
