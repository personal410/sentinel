//
//  ResumenCompraViewController.swift
//  Sentinel
//
//  Created by Daniel on 22/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class ResumenCompraViewController:ParentViewController {
    @IBOutlet weak var tipoReporteLB: UILabel!
    @IBOutlet weak var consultasReporteLB: UILabel!
    @IBOutlet weak var vigenciaLB: UILabel!
    @IBOutlet weak var codigoTXT: UITextField!
    @IBOutlet weak var totalLB: UILabel!
    @IBOutlet weak var adquirirBT: UIButton!
    @IBOutlet weak var productosToolBar: UIToolbar!
    
    var esPremium = false
    var total = "0"
    var cantidad = "0"
    var tipoReporte = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.validarPremium()
        self.gestureScreen()
        self.estilos()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.cargarData()
    }
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            self.esPremium = true
            self.changeStatusBar(cod: 3)
            self.setGradient(uiView: self.productosToolBar)
        }
    }
    
    func estilos(){
        self.adquirirBT.layer.cornerRadius = 5
    }
    
    func cargarData(){
        if tipoReporte == "PremiumM"{
            self.totalLB.text = "S/ \(total)"
            self.tipoReporteLB.text = "Sentinel Premium Mensual"
            self.consultasReporteLB.text = "Consultas: Flash ilimitadas"
            self.vigenciaLB.text = "Vigencia: 1 mes"
        }
        else if tipoReporte == "PremiumA"{
            self.totalLB.text = "S/ \(total)"
            self.tipoReporteLB.text = "Sentinel Premium Anual"
            self.consultasReporteLB.text = "Consultas: Flash ilimitadas"
        }
        else
        {
            self.totalLB.text = "S/ \(total)"
            if tipoReporte == "Detallado"{
                self.tipoReporteLB.text = "Reporte de Deudas Detallado"
            }
            else if tipoReporte == "Resumido"{
                self.tipoReporteLB.text = "Reporte de Deudas Resumido"
            }
            if cantidad == "1"{
                self.consultasReporteLB.text = "Consultas: \(cantidad) Reporte"
            }
            else if cantidad > "0"{
                self.consultasReporteLB.text = "Consultas: \(cantidad) Reportes"
            }
        }
    }
    
    @IBAction func accionSalir(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func accionFinalizarCompra(_ sender: Any) {
        
        let codigoPromocional : String = self.codigoTXT.text!
        
        if codigoPromocional != ""
        {
            
        }
        else
        {
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "ResumenCompraPasoDosVC") as! ResumenCompraPasoDosViewController
            if esPremium == true
            {
                if tipoReporte == "Detallado"{
                    if self.cantidad == "1"{
                        nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_UNO_PREMIUM)
                        nav.total = self.total
                        nav.cantidad = self.cantidad
                        nav.tipoReporte = self.tipoReporte
                    }
                    else if self.cantidad == "5"{
                        nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_CINCO_PREMIUM)
                        nav.total = self.total
                        nav.cantidad = self.cantidad
                        nav.tipoReporte = self.tipoReporte
                    }
                    else if self.cantidad == "10"{
                        nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_DIEZ_PREMIUM)
                        nav.total = self.total
                        nav.cantidad = self.cantidad
                        nav.tipoReporte = self.tipoReporte
                    }
                }
                
                if tipoReporte == "Resumido"{
                    if self.cantidad == "1"{
                        nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_RESUMIDAS_UNO_PREMIUM)
                        nav.total = self.total
                        nav.cantidad = self.cantidad
                        nav.tipoReporte = self.tipoReporte
                    }
                    else if self.cantidad == "5"{
                        nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_RESUMIDAS_CINCO_PREMIUM)
                        nav.total = self.total
                        nav.cantidad = self.cantidad
                        nav.tipoReporte = self.tipoReporte
                    }
                    else if self.cantidad == "10"{
                        nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_RESUMIDAS_DIEZ_PREMIUM)
                        nav.total = self.total
                        nav.cantidad = self.cantidad
                        nav.tipoReporte = self.tipoReporte
                    }
                }
            }
            else
            {
                if tipoReporte == "PremiumM"{
                    nav.productoNumero = String(PATHS.TIENDA.SENTINEL_PREMIUM_MENSUAL)
                    nav.total = self.total
                    nav.tipoReporte = self.tipoReporte
                }
                else if tipoReporte == "PremiumA"{
                    nav.productoNumero = String(PATHS.TIENDA.SENTINEL_PREMIUM_ANUAL)
                    nav.total = self.total
                    nav.tipoReporte = self.tipoReporte
                }
                else if tipoReporte == "Detallado"{
                    if self.cantidad == "1"{
                        nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_UNO)
                        nav.total = self.total
                        nav.cantidad = self.cantidad
                        nav.tipoReporte = self.tipoReporte
                    }
                    else if self.cantidad == "5"{
                        nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_CINCO)
                        nav.total = self.total
                        nav.cantidad = self.cantidad
                        nav.tipoReporte = self.tipoReporte
                    }
                    else if self.cantidad == "10"{
                        nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_DIEZ)
                        nav.total = self.total
                        nav.cantidad = self.cantidad
                        nav.tipoReporte = self.tipoReporte
                    }
                }
                else if tipoReporte == "Resumido"{
                    if self.cantidad == "1"{
                        nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_RESUMIDAS_UNO)
                        nav.total = self.total
                        nav.cantidad = self.cantidad
                        nav.tipoReporte = self.tipoReporte
                    }
                    else if self.cantidad == "5"{
                        nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_RESUMIDAS_CINCO)
                        nav.total = self.total
                        nav.cantidad = self.cantidad
                        nav.tipoReporte = self.tipoReporte
                    }
                    else if self.cantidad == "10"{
                        nav.productoNumero = String(PATHS.TIENDA.CONS_TERCEROS_RESUMIDAS_DIEZ)
                        nav.total = self.total
                        nav.cantidad = self.cantidad
                        nav.tipoReporte = self.tipoReporte
                    }
                }
            }
            self.navigationController?.pushViewController(nav, animated: true)
        }
    }
}
