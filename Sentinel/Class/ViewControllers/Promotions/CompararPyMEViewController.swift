//
//  CompararPyMEViewController.swift
//  Sentinel
//
//  Created by Daniel on 20/11/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//CompararPyMEViewController
import UIKit
class CompararPyMEViewController: ParentViewController {
    @IBOutlet weak var topViewCuadro: UIView!
    @IBOutlet weak var tablaCompara: UITableView!
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var comparativo = "2"
    var arregloInfo = NSArray()
    var esPremium = false
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.topViewCuadro.layer.cornerRadius = 5
        self.validarPremium()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.cargarDatos()
    }
    
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            self.esPremium = true
            self.changeStatusBar(cod: 3)
            self.setGradient(uiView: self.toolbar)
            self.setGradient(uiView: self.scrollView, at: 0)
            self.scrollView.backgroundColor = UIColor.fromHex(0xD18E18)
        }else{
            self.esPremium = false
        }
    }
    func cargarDatos(){
        self.arregloInfo = (self.devuelveArregloDictComparar(vista: self.comparativo)).copy() as! NSArray
        self.tablaCompara.reloadData()
    }
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension CompararPyMEViewController:UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arregloInfo.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InfoComparacionTVC", for: indexPath) as! InfoComparacionTableViewCell
        let objArray = self.arregloInfo[indexPath.row] as! NSMutableDictionary
        if let detalle : String = objArray.object(forKey: "detalle") as? String {
            cell.infoGeneralLB.text = detalle
        }
        if let detallado : Int = objArray.object(forKey: "detallado") as? Int {
            if detallado == 0 {
                cell.detalleTercerIcono.isHidden = true
                cell.tercerIcono.image = UIImage.init(named: "")
            }else if detallado == 1 {
                cell.detalleTercerIcono.isHidden = true
                cell.tercerIcono.image = UIImage.init(named: "check.azul")
            }else if detallado == 2{
                cell.detalleTercerIcono.isHidden = false
                cell.detalleTercerIcono.text = "DIARIO"
                cell.tercerIcono.image = UIImage.init(named: "")
            }else if detallado == 3{
                cell.detalleTercerIcono.isHidden = false
                cell.detalleTercerIcono.text = "ONLINE"
                cell.tercerIcono.image = UIImage.init(named: "")
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
