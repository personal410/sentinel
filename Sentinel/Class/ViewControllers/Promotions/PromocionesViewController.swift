//
//  PromocionesViewController.swift
//  Sentinel
//
//  Created by Daniel on 20/11/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class PromocionesViewController: ParentViewController {

    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var codigoTXT: UITextField!
    
    var esPremium : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.validarPremium()
        self.gestureScreen()
        self.estilos()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func estilos(){
       self.toolbar.clipsToBounds = true
    }
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            self.esPremium = true
            self.changeStatusBar(cod: 3)
            self.setGradient(uiView: self.toolbar)
        }
        else{
            self.esPremium = false
        }
    }

    func iniAgregarCodigo(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endAgregarCodigo(_:)), name: NSNotification.Name("endAgregarCodigo"), object: nil)
        
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        
        let userData = UserGlobalData.sharedInstance.userGlobal!
        
        let usuario : String = userData.user!
        let sesion : String = userData.SesionId!
        let tDoc : String = userData.TDocusu!
        let codigo : String = self.codigoTXT.text!
        
        let parametros = [
            "tipoDocumento" : tDoc,
            "numeroDocumento" : usuario,
            "SesionId" : sesion,
            "origenAplicacion" : PATHS.APP_ID,
            "campania" : "5",
            "CodPromocion" :  codigo
            ] as [String : Any]
        print(parametros)
        OriginData.sharedInstance.codigoPromociones(notification: "endAgregarCodigo", parametros: parametros)
    }
    
    @objc func endAgregarCodigo(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as! UserClass
            if data.MsjActivacion != ""{
                self.showAlert(PATHS.SENTINEL, mensaje: data.MsjActivacion!)
            }
            else
            {
                if data.codigoWS != nil {
                    if data.codigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        iniMensajeError(codigo: (data.codigoWS!))
                    }
                }
                else
                {
                    let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                    alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                    self.present(alerta, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func accionAplicar(_ sender: Any) {
        let codigo : String = self.codigoTXT.text!
        if codigo != "" {
            self.iniAgregarCodigo()
        }
        else
        {
            self.showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar el código de promoción.")
        }
    }
    
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}
