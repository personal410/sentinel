//
//  CompararResumidosDetalladosViewController.swift
//  Sentinel
//
//  Created by Daniel on 22/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class CompararResumidosDetalladosViewController: ParentViewController {

    @IBOutlet weak var topViewCuadro: UIView!
    @IBOutlet weak var tablaCompara: UITableView!
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var primerBeneficioLB: UILabel!
    @IBOutlet weak var segundoBeneficioLB: UILabel!
    
    var comparativo : String = "0"
    var arregloInfo : NSArray = NSArray()
    var esPremium : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializar()
        self.estilos()
        self.validarPremium()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.cargarDatos()
    }
    
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            self.esPremium = true
            self.changeStatusBar(cod: 3)
            self.setGradient(uiView: self.toolbar)
            self.setGradient(uiView: self.scrollView, at: 0)
            self.scrollView.backgroundColor = UIColor.fromHex(0xD18E18)
        }
        else{
            self.esPremium = false
        }
    }
    
    func inicializar(){
        
    }
    
    func estilos(){
        self.topViewCuadro.layer.cornerRadius = 5
    }
    
    func cargarDatos(){
        self.arregloInfo = (self.devuelveArregloDictComparar(vista: self.comparativo)).copy() as! NSArray
        if self.comparativo == "1"{
            self.primerBeneficioLB.text = "GRATUITO"
            self.primerBeneficioLB.textColor = UIColor.fromHex(0x06B150)
            self.segundoBeneficioLB.textColor = UIColor.fromHex(0xD18E18)
            self.segundoBeneficioLB.text = "PREMIUM"
        }
        self.tablaCompara.reloadData()
    }
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension CompararResumidosDetalladosViewController : UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arregloInfo.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InfoComparacionTVC", for: indexPath) as! InfoComparacionTableViewCell
        let objArray : NSMutableDictionary = self.arregloInfo[indexPath.row] as! NSMutableDictionary
        if self.comparativo == "0"{
            if let detalle : String = objArray.object(forKey: "detalle") as? String {
                cell.infoGeneralLB.text = detalle
            }
            if let resumido : Int = objArray.object(forKey: "resumido") as? Int {
                if resumido == 0 {
                    cell.detalleSegundoIcono.isHidden = true
                    cell.segundoIcono.image = UIImage.init(named: "")
                }
                else if resumido == 1 {
                    cell.detalleSegundoIcono.isHidden = true
                    cell.segundoIcono.image = UIImage.init(named: "check.azul")
                }
                else if resumido == 2{
                    cell.detalleSegundoIcono.isHidden = false
                    cell.segundoIcono.image = UIImage.init(named: "")
                }
            }
            
            if let detallado : Int = objArray.object(forKey: "detallado") as? Int {
                if detallado == 0 {
                    cell.detalleTercerIcono.isHidden = true
                    cell.tercerIcono.image = UIImage.init(named: "")
                }
                else if detallado == 1 {
                    cell.detalleTercerIcono.isHidden = true
                    cell.tercerIcono.image = UIImage.init(named: "check.verde")
                }
                else if detallado == 2{
                    cell.detalleTercerIcono.isHidden = false
                    cell.detalleTercerIcono.text = "DETALLADO 12 MESES"
                    cell.tercerIcono.image = UIImage.init(named: "")
                }
                else if detallado == 3{
                    cell.detalleTercerIcono.isHidden = false
                    cell.detalleTercerIcono.text = "POR 3 MESES"
                    cell.tercerIcono.image = UIImage.init(named: "")
                }
            }
        }
        else if self.comparativo == "1"{
            if let detalle : String = objArray.object(forKey: "detalle") as? String {
                cell.infoGeneralLB.text = detalle
            }
            if let resumido : Int = objArray.object(forKey: "resumido") as? Int {
                cell.detalleSegundoIcono.textColor = UIColor.fromHex(0x06B150)
                if resumido == 0 {
                    cell.detalleSegundoIcono.isHidden = true
                    cell.segundoIcono.image = UIImage.init(named: "")
                }
                else if resumido == 1 {
                    cell.detalleSegundoIcono.isHidden = true
                    cell.segundoIcono.image = UIImage.init(named: "check.verde")
                }
                else if resumido == 2{
                    cell.detalleSegundoIcono.isHidden = false
                    cell.detalleSegundoIcono.text = "UNA VEZ CADA 3 MESES"
                    cell.segundoIcono.image = UIImage.init(named: "")
                }
                else if resumido == 4{
                    cell.detalleSegundoIcono.isHidden = false
                    cell.detalleSegundoIcono.text = "RESUMIDO"
                    cell.segundoIcono.image = UIImage.init(named: "")
                }
            }
            
            if let detallado : Int = objArray.object(forKey: "detallado") as? Int {
                cell.detalleTercerIcono.textColor = UIColor.fromHex(0xD18E18)
                if detallado == 0 {
                    cell.detalleTercerIcono.isHidden = true
                    cell.tercerIcono.image = UIImage.init(named: "")
                }
                else if detallado == 1 {
                    cell.detalleTercerIcono.isHidden = true
                    cell.tercerIcono.image = UIImage.init(named: "check.dorado")
                }
                else if detallado == 3{
                    cell.detalleTercerIcono.isHidden = false
                    cell.detalleTercerIcono.text = "ILIMITADO"
                    cell.tercerIcono.image = UIImage.init(named: "")
                }
                else if detallado == 5{
                    cell.detalleTercerIcono.isHidden = false
                    cell.detalleTercerIcono.text = "COMPLETO"
                    cell.tercerIcono.image = UIImage.init(named: "")
                }
                else if detallado == 6{
                    cell.detalleTercerIcono.isHidden = false
                    cell.detalleTercerIcono.text = "50% DESCUENTO"
                    cell.tercerIcono.image = UIImage.init(named: "")
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
}







