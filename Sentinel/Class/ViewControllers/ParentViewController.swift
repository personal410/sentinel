//
//  ParentViewController.swift
//  SentinelTuit
//
//  Created by Juan Alberto Carlos Vera on 3/21/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//
import CoreLocation
import Alamofire
import AlamofireImage
import SafariServices
class ParentViewController:UIViewController {
    @IBOutlet weak var topBarView:UIView!
    @IBOutlet weak var navBar:UINavigationBar!
    var usuarioParent : UserClass?
    var terceroParent : TerceroClass?
    var actualLocation = CLLocation()
    let MENSAJE_OK = "OK"
    var isPlayFirstTime = false
    var objetoDict : Dictionary = [String: String]()
    var objetoArray:NSMutableArray = []
    //MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(ParentViewController.endInternetConnection(_:)),  name:NSNotification.Name(rawValue: "endInternetConnection"), object: nil)
    }
    override var preferredStatusBarStyle:UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    @objc func endInternetConnection(_ notification:Notification){
        self.hideActivityIndicator()
        self.showAlertConnectivity()
    }
    public func devuelveArregloDictComparar(vista : String) ->  NSMutableArray{
        let arreglo : NSMutableArray = NSMutableArray()
        if vista == "0"{
            let objArray1 : NSMutableDictionary = NSMutableDictionary()
            objArray1.setValue("Informacion General", forKey: "detalle")
            objArray1.setValue(1, forKey: "resumido")
            objArray1.setValue(1, forKey: "detallado")
            arreglo.add(objArray1)
            
            let objArray2 : NSMutableDictionary = NSMutableDictionary()
            objArray2.setValue("Consulta Resumida", forKey: "detalle")
            objArray2.setValue(1, forKey: "resumido")
            objArray2.setValue(1, forKey: "detallado")
            arreglo.add(objArray2)
            
            let objArray3 : NSMutableDictionary = NSMutableDictionary()
            objArray3.setValue("Deuda Total", forKey: "detalle")
            objArray3.setValue(1, forKey: "resumido")
            objArray3.setValue(1, forKey: "detallado")
            arreglo.add(objArray3)
            
            let objArray4 : NSMutableDictionary = NSMutableDictionary()
            objArray4.setValue("Semáforo de los últimos 12 meses", forKey: "detalle")
            objArray4.setValue(1, forKey: "resumido")
            objArray4.setValue(1, forKey: "detallado")
            arreglo.add(objArray4)
            
            let objArray5 : NSMutableDictionary = NSMutableDictionary()
            objArray5.setValue("Principales Acreedores", forKey: "detalle")
            objArray5.setValue(0, forKey: "resumido")
            objArray5.setValue(2, forKey: "detallado")
            arreglo.add(objArray5)
            
            let objArray6 : NSMutableDictionary = NSMutableDictionary()
            objArray6.setValue("Alertas por Variaciones en la Información", forKey: "detalle")
            objArray6.setValue(0, forKey: "resumido")
            objArray6.setValue(3, forKey: "detallado")
            arreglo.add(objArray6)
            
            let objArray7 : NSMutableDictionary = NSMutableDictionary()
            objArray7.setValue("Detalle de Variaciones", forKey: "detalle")
            objArray7.setValue(0, forKey: "resumido")
            objArray7.setValue(1, forKey: "detallado")
            arreglo.add(objArray7)
            
            let objArray8 : NSMutableDictionary = NSMutableDictionary()
            objArray8.setValue("Posición Histórica", forKey: "detalle")
            objArray8.setValue(0, forKey: "resumido")
            objArray8.setValue(1, forKey: "detallado")
            arreglo.add(objArray8)
            
            let objArray9 : NSMutableDictionary = NSMutableDictionary()
            objArray9.setValue("Gráficos de Deudas", forKey: "detalle")
            objArray9.setValue(0, forKey: "resumido")
            objArray9.setValue(1, forKey: "detallado")
            arreglo.add(objArray9)
            
            let objArray10 : NSMutableDictionary = NSMutableDictionary()
            objArray10.setValue("Avales/Avalistas", forKey: "detalle")
            objArray10.setValue(0, forKey: "resumido")
            objArray10.setValue(1, forKey: "detallado")
            arreglo.add(objArray10)
            
            let objArray11 : NSMutableDictionary = NSMutableDictionary()
            objArray11.setValue("Reporte de Vencidos", forKey: "detalle")
            objArray11.setValue(0, forKey: "resumido")
            objArray11.setValue(1, forKey: "detallado")
            arreglo.add(objArray11)
            
            let objArray12 : NSMutableDictionary = NSMutableDictionary()
            objArray12.setValue("Detalle de deuda SBS y Microfinanzas", forKey: "detalle")
            objArray12.setValue(0, forKey: "resumido")
            objArray12.setValue(1, forKey: "detallado")
            arreglo.add(objArray12)
        }else if vista == "1"{
            let objArray1 : NSMutableDictionary = NSMutableDictionary()
            objArray1.setValue("Mi Reporte de Deudas Resumido (DNI/RUC)", forKey: "detalle")
            objArray1.setValue(1, forKey: "resumido")
            objArray1.setValue(1, forKey: "detallado")
            arreglo.add(objArray1)
            
            let objArray2 : NSMutableDictionary = NSMutableDictionary()
            objArray2.setValue("Mi Reporte Detallado de todas mis deudas", forKey: "detalle")
            objArray2.setValue(2, forKey: "resumido")
            objArray2.setValue(3, forKey: "detallado")
            arreglo.add(objArray2)
            
            let objArray3 : NSMutableDictionary = NSMutableDictionary()
            objArray3.setValue("Mis Líneas de crédito", forKey: "detalle")
            objArray3.setValue(1, forKey: "resumido")
            objArray3.setValue(1, forKey: "detallado")
            arreglo.add(objArray3)
            
            let objArray4 : NSMutableDictionary = NSMutableDictionary()
            objArray4.setValue("Principales indicadores de deudas y riesgo", forKey: "detalle")
            objArray4.setValue(1, forKey: "resumido")
            objArray4.setValue(1, forKey: "detallado")
            arreglo.add(objArray4)
            
            let objArray5 : NSMutableDictionary = NSMutableDictionary()
            objArray5.setValue("Descarga de Reporte PDF", forKey: "detalle")
            objArray5.setValue(4, forKey: "resumido")
            objArray5.setValue(5, forKey: "detallado")
            arreglo.add(objArray5)
            
            let objArray6 : NSMutableDictionary = NSMutableDictionary()
            objArray6.setValue("Reporte de Deudas Resumido de mis Empresas", forKey: "detalle")
            objArray6.setValue(1, forKey: "resumido")
            objArray6.setValue(1, forKey: "detallado")
            arreglo.add(objArray6)
            
            let objArray7 : NSMutableDictionary = NSMutableDictionary()
            objArray7.setValue("Reporte de Deudas Detallado de mis Empresas", forKey: "detalle")
            objArray7.setValue(0, forKey: "resumido")
            objArray7.setValue(1, forKey: "detallado")
            arreglo.add(objArray7)
            
            let objArray8 : NSMutableDictionary = NSMutableDictionary()
            objArray8.setValue("Mi Score Sentinel", forKey: "detalle")
            objArray8.setValue(0, forKey: "resumido")
            objArray8.setValue(1, forKey: "detallado")
            arreglo.add(objArray8)
            
            let objArray9 : NSMutableDictionary = NSMutableDictionary()
            objArray9.setValue("Mis Alertas por Email y App por variaciones", forKey: "detalle")
            objArray9.setValue(0, forKey: "resumido")
            objArray9.setValue(1, forKey: "detallado")
            arreglo.add(objArray9)
            
            let objArray10 : NSMutableDictionary = NSMutableDictionary()
            objArray10.setValue("REPORTES FLASH de Personas y Empresas", forKey: "detalle")
            objArray10.setValue(0, forKey: "resumido")
            objArray10.setValue(1, forKey: "detallado")
            arreglo.add(objArray10)
            
            let objArray11 : NSMutableDictionary = NSMutableDictionary()
            objArray11.setValue("Reporte de Deudas", forKey: "detalle")
            objArray11.setValue(0, forKey: "resumido")
            objArray11.setValue(6, forKey: "detallado")
            arreglo.add(objArray11)
            
        }else if vista == "2"{
            let objArray1 = NSMutableDictionary()
            objArray1.setValue("Monitoreo de Variaciones de Deuda", forKey: "detalle")
            objArray1.setValue(2, forKey: "detallado")
            arreglo.add(objArray1)
            
            let objArray2 = NSMutableDictionary()
            objArray2.setValue("Alertas por Variaciones en le información", forKey: "detalle")
            objArray2.setValue(3, forKey: "detallado")
            arreglo.add(objArray2)
            
            let objArray3 = NSMutableDictionary()
            objArray3.setValue("Información General", forKey: "detalle")
            objArray3.setValue(1, forKey: "detallado")
            arreglo.add(objArray3)
            
            let objArrayz = NSMutableDictionary()
            objArrayz.setValue("Consulta Resumida", forKey: "detalle")
            objArrayz.setValue(1, forKey: "detallado")
            arreglo.add(objArrayz)
            
            let objArrayx = NSMutableDictionary()
            objArrayx.setValue("Deuda Total", forKey: "detalle")
            objArrayx.setValue(1, forKey: "detallado")
            arreglo.add(objArrayx)
            
            let objArray4 = NSMutableDictionary()
            objArray4.setValue("Semáforo de los últimos 12 meses", forKey: "detalle")
            objArray4.setValue(1, forKey: "detallado")
            arreglo.add(objArray4)
            
            let objArray5 = NSMutableDictionary()
            objArray5.setValue("Principales Acreedores", forKey: "detalle")
            objArray5.setValue(1, forKey: "detallado")
            arreglo.add(objArray5)
            
            let objArray6 = NSMutableDictionary()
            objArray6.setValue("Alertas por Variaciones en la Información", forKey: "detalle")
            objArray6.setValue(1, forKey: "detallado")
            arreglo.add(objArray6)
            
            let objArray7 = NSMutableDictionary()
            objArray7.setValue("Detalle de Variaciones", forKey: "detalle")
            objArray7.setValue(1, forKey: "detallado")
            arreglo.add(objArray7)
            
            let objArray8 = NSMutableDictionary()
            objArray8.setValue("Posición Histórica", forKey: "detalle")
            objArray8.setValue(1, forKey: "detallado")
            arreglo.add(objArray8)
            
            let objArray9 = NSMutableDictionary()
            objArray9.setValue("Gráficos de Deudas", forKey: "detalle")
            objArray9.setValue(1, forKey: "detallado")
            arreglo.add(objArray9)
            
            let objArray10 = NSMutableDictionary()
            objArray10.setValue("Avales/Avalistas", forKey: "detalle")
            objArray10.setValue(1, forKey: "detallado")
            arreglo.add(objArray10)
            
            let objArray11 = NSMutableDictionary()
            objArray11.setValue("Reporte de Vencidos", forKey: "detalle")
            objArray11.setValue(1, forKey: "detallado")
            arreglo.add(objArray11)
            let objArray12 = NSMutableDictionary()
            objArray12.setValue("Detalle de Deuda SBS y Microfinancieras", forKey: "detalle")
            objArray12.setValue(1, forKey: "detallado")
            arreglo.add(objArray12)
        }
        return arreglo
    }
    
    public func iniMensajeError(codigo : String){
        let parametros = ["CodigoWS" : codigo] as [String : Any]
        NotificationCenter.default.addObserver(self, selector: #selector(self.endMensajeError(_:)), name: NSNotification.Name("endMensajeError"), object: nil)
        OriginData.sharedInstance.getError(notification: "endMensajeError", parametros: parametros)
    }
    
    @objc func endMensajeError(_ notification:NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        let data = notification.object as! String
        Toolbox.showAlert(with: PATHS.SENTINEL, and: data, in: self)
    }

    public func iniConsultaDatosServicioParent(){
        self.showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endConsultaDatosServicioParent(_:)), name: NSNotification.Name("endConsultaDatosServicioParent"), object: nil)
        usuarioParent = UserGlobalData.sharedInstance.userGlobal
        let usua : String = (usuarioParent?.NumDocumento)!
        let sesion : String = (usuarioParent?.SesionId)!
        let servicios = UserGlobalData.sharedInstance.misServiciosGlobal!
        let servicio : String = servicios.NroServicio!
        let parametros = [
            "Usuario": usua,
            "SesionId": sesion,
            "Servicio": servicio
            ] as [String : Any]
        OriginData.sharedInstance.consultaDatosServicio(notification: "endConsultaDatosServicioParent", parametros: parametros)
    }
    
    @objc func endConsultaDatosServicioParent(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if validaIndicador == 0 {
            let terceroLocal = notification.object as! TerceroClass
            UserGlobalData.sharedInstance.terceroGlobal = terceroLocal
            terceroParent = UserGlobalData.sharedInstance.terceroGlobal
            UserGlobalData.sharedInstance.terceroGlobal.DispDet = terceroLocal.SaldoServicio?.object(forKey: "DispDet") as? String
            UserGlobalData.sharedInstance.terceroGlobal.DispRes = terceroLocal.SaldoServicio?.object(forKey: "DispRes") as? String
            UserGlobalData.sharedInstance.terceroGlobal.SalFlash = terceroLocal.SaldoServicio?.object(forKey: "SalFlash") as? String
        }
    }
    
    public func changeStatusBar(cod : Int){
        switch cod {
        case 1:
            self.topBarView.backgroundColor = UIColor.fromHex(0x06B150)
            break;
        case 2:
            //black
            //if (self.topBarView.layer.sublayers?.count)! > 0{
            self.topBarView.layer.sublayers?.removeAll()
            //}
            //self.topBarView.layer.sublayers?.remove(at: num - 1)
            self.topBarView.backgroundColor = UIColor.fromHex(0x363636)
            
            break;
        case 3:
            //gold
            //self.topBarView.backgroundColor = UIColor.fromHex(0xD18E18)
            setGradient(uiView: self.topBarView)
            break;
        default:
            self.topBarView.backgroundColor = UIColor.fromHex(0x06B150)
        }
    }
    
    func automaticView(){
        let newView = UIView()
        newView.backgroundColor = UIColor.red
        view.addSubview(newView)
        
        newView.translatesAutoresizingMaskIntoConstraints = false
        let views = ["view": view!, "newView": newView]
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:[view]-(<=0)-[newView(100)]", options: NSLayoutFormatOptions.alignAllCenterY, metrics: nil, views: views)
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:[view]-(<=0)-[newView(100)]", options: NSLayoutFormatOptions.alignAllCenterX, metrics: nil, views: views)
        NSLayoutConstraint.activate(horizontalConstraints)
        NSLayoutConstraint.activate(verticalConstraints)
    }
    
    public func irComprar(){
        let nav = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopupVC1") as! PopViewController
        self.navigationController?.pushViewController(nav, animated: true)
        
    }
    
    public func irComprarConsultas(){
        
        let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopViewConsultasTercerosVC") as! PopViewConsultasTercerosViewController
        self.navigationController?.pushViewController(nav, animated: true)
        
    }
    
    public func devuelveObjetoCalificacion(codigo : Int) -> [String:String]{
        var dict : [String:String]!
        
        if codigo == 1 {
            dict = ["nombre" : "NOR", "color": "0,139,0"]
        }
        else if codigo == 2 {
            dict = ["nombre" : "CPP", "color": "255,255,0"]
        }
        else if codigo == 3 {
            dict = ["nombre" : "DEF", "color": "255,128,0"]
        }
        else if codigo == 4 {
            dict = ["nombre" : "DUD", "color": "255,0,0"]
        }
        else if codigo == 5 {
            dict = ["nombre" : "PER", "color": "0,1,0"]
        }else {
            dict = ["nombre" : "SCAL", "color": "240,240,240"]
        }
        return dict
    }
    public func devuelveUnMesAdicional(fecha : String) -> String{
        let cal = Calendar.current
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let fechaDato : String = fecha
        var myStringafd : String = ""
        var dateCalendar : Date = dateFormatter.date(from: fechaDato)!
        
        dateCalendar = cal.date(byAdding: .month, value: +1, to: dateCalendar)!
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        myStringafd = formatter.string(from: dateCalendar)
        
        return myStringafd
    }
    
    public func devuelveArregloUltimosMeses(fechaUltima : String) -> NSMutableArray{
        let cal = Calendar.current
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let fechaDato : String = fechaUltima
        var dateCalendar : Date = dateFormatter.date(from: fechaDato) ?? Date()
        
        //var date = cal.startOfDay(for: Date())
        var months = [Int]()
        var years = [Int]()
        let dicts : NSMutableArray  = NSMutableArray()
        let primerDay = cal.component(.month, from: dateCalendar)
        let primerYear = cal.component(.year, from: dateCalendar)
        months.append(primerDay)
        years.append(primerYear)
        let dict = ["mes": "\(primerDay)", "anio": "\(primerYear)"]
        dicts.add(dict)
        
        for _ in 1 ... 11 {
            dateCalendar = cal.date(byAdding: .month, value: -1, to: dateCalendar)!
            let day = cal.component(.month, from: dateCalendar)
            let year = cal.component(.year, from: dateCalendar)
            months.append(day)
            years.append(year)
            let dict = ["mes": "\(day)", "anio": "\(year)"]
            dicts.add(dict)
            
        }
        
        return dicts
    }
    
    public func devuelveArregloUltimosTresMeses(fechaUltima : String) -> NSMutableArray{
        let cal = Calendar.current
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let fechaDato : String = fechaUltima
        var dateCalendar : Date = dateFormatter.date(from: fechaDato)!
        
        var months = [Int]()
        var years = [Int]()
        let dicts : NSMutableArray  = NSMutableArray()
        
        let primerDay = cal.component(.month, from: dateCalendar)
        let primerYear = cal.component(.year, from: dateCalendar)
        months.append(primerDay)
        years.append(primerYear)
        let dict = ["mes": "\(primerDay)", "anio": "\(primerYear)"]
        dicts.add(dict)
        
        for _ in 1 ... 2 {
            dateCalendar = cal.date(byAdding: .month, value: -1, to: dateCalendar)!
            let day = cal.component(.month, from: dateCalendar)
            let year = cal.component(.year, from: dateCalendar)
            months.append(day)
            years.append(year)
            let dict = ["mes": "\(day)", "anio": "\(year)"]
            dicts.add(dict)
            
        }
        return dicts
    }
    
    public func devuelveSemaforo(SemAct : String) -> UIImage{
        
        let trimSem : String = SemAct.trimmingCharacters(in: NSCharacterSet.whitespaces)
        var imagen : UIImage!
        if trimSem == "" {
            imagen = UIImage.init(named: "gris.s")
        }
        else if trimSem == "1" || trimSem == "R" {
            imagen = UIImage.init(named: "rojo.s")
        }
        else if trimSem == "2" || trimSem == "A" {
            imagen = UIImage.init(named: "amarillo.s")
        }
        else if trimSem == "3" || trimSem == "G"{
            imagen = UIImage.init(named: "gris.s")
        }
        else if trimSem == "4" || trimSem == "V" {
            imagen = UIImage.init(named: "verde.s")
        }
        else if trimSem == "5" {
            imagen = UIImage.init(named: "azul.s")
        }
        else if trimSem == "6" {
            imagen = UIImage.init(named: "rojo.amarillo.s")
        }
        else if trimSem == "7" {
            imagen = UIImage.init(named: "rojo.verde.s")
        }
        else if trimSem == "8" {
            imagen = UIImage.init(named: "amarillo.rojo.s")
        }
        else if trimSem == "9" {
            imagen = UIImage.init(named: "amarillo.verde.s")
        }
        else if trimSem == "10" {
            imagen = UIImage.init(named: "verde.rojo.s")
        }
        else if trimSem == "11" {
            imagen = UIImage.init(named: "verde.amarillo.s")
        }
        else if trimSem == "12" {
            imagen = UIImage.init(named: "gris.rojo.s")
        }
        else if trimSem == "13" {
            imagen = UIImage.init(named: "gris.amarillo.s")
        }
        else if trimSem == "14" {
            imagen = UIImage.init(named: "gris.verde.s")
        }
        else if trimSem == "0" {
            imagen = UIImage.init(named: "semaforoON")
        }
        else if trimSem == "15" {
            imagen = UIImage.init(named: "option_off")
        }
        else if trimSem == "16" {
            imagen = UIImage.init(named: "rojo.gris.s")
        }
        else if trimSem == "17" {
            imagen = UIImage.init(named: "amarillo.gris.s")
        }
        else if trimSem == "18" || trimSem == "M"{
            imagen = UIImage.init(named: "24amarilloB")
        }
        else if trimSem == "19" || trimSem == "O" {
            imagen = UIImage.init(named: "24rojoB")
        }
        else{
            imagen = UIImage.init(named: "gris.s")
        }
        
        return imagen!
    }
    
    public func cambiaColorObjetoConLetra(colorActivo : String, objeto : UILabel){
        if colorActivo == "G" || colorActivo == "3"{
            objeto.textColor = UIColor.fromHex(0x949494)
        }else if colorActivo == "V" || colorActivo == "4"{
            objeto.textColor = UIColor.fromHex(0x00a900)
        }else if colorActivo == "A" || colorActivo == "2"{
            objeto.textColor = UIColor.fromHex(0xf0c915)
        }else if colorActivo == "R" || colorActivo == "1"{
            objeto.textColor = UIColor.fromHex(0xec2a3d)
        }else if colorActivo == "M"{
            objeto.textColor = UIColor.fromHex(0xf0c915)
        }else if colorActivo == "O"{
            objeto.textColor = UIColor.fromHex(0xec2a3d)
        }
    }
    
    public func devuelveCreditosIndicadores(cred : String) -> UIImage{
        let trimSem : String = cred.trimmingCharacters(in: NSCharacterSet.whitespaces)
        var imagen : UIImage!
        if trimSem == "1"  {
            imagen = UIImage.init(named: "creditohipotecario")
        }
        else if trimSem == "2"  {
            imagen = UIImage.init(named: "creditovehicular")
        }
        else if trimSem == "3" {
            imagen = UIImage.init(named: "tarjetadecredito")
        }
        else if trimSem == "4" {
            imagen = UIImage.init(named: "esaval-1")
        }
        else if trimSem == "5" {
            imagen = UIImage.init(named: "estaavalado72px")
        }
        else if trimSem == "6" {
            imagen = UIImage.init(named: "mancomunado")
        }
        else if trimSem == "7" {
            imagen = UIImage.init(named: "representantelegal")
        }
        else if trimSem == "8" {
            imagen = UIImage.init(named: "jn")
        }
        else if trimSem == "9" {
            imagen = UIImage.init(named: "nohabido")
        }
        else if trimSem == "10" {
            imagen = UIImage.init(named: "i")
        }
        else if trimSem == "11" {
            imagen = UIImage.init(named: "canitobronce72px")
        }
        else if trimSem == "12" {
            imagen = UIImage.init(named: "canitoplata72px")
        }
        else if trimSem == "13" {
            imagen = UIImage.init(named: "canitooro72px")
        }
        else if trimSem == "14" {
            imagen = UIImage.init(named: "buencontribuyenteSunat72px")
        }
        else if trimSem == "0" {
            imagen = UIImage.init(named: "semaforoON")
        }
        else if trimSem == "15" {
            imagen = UIImage.init(named: "buencontribuyenteMunicipal72px")
        }
        else if trimSem == "16" {
            imagen = UIImage.init(named: "rojo.gris.s")
        }
        else if trimSem == "17" {
            imagen = UIImage.init(named: "amarillo.gris.s")
        }
        else
        {
            imagen = UIImage.init(named: "nohabido")
        }
        return imagen!
    }
    
    public func devuelveArrayIMGSemaforos(semaforos : String) -> NSMutableArray{
        let trimSem : String = semaforos.trimmingCharacters(in: NSCharacterSet.whitespaces)
        var arraySemaforos = Array(trimSem)
        let array : NSMutableArray = NSMutableArray()
        for i in 0 ... arraySemaforos.count - 1{
            let objSem = arraySemaforos[i]
            array.add(self.devuelveSemaforo(SemAct: String(objSem)))
        }
        return array
    }
    
    public func devuelveFecha(fecha : String) -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        //dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale?
        let fechaDato : String = fecha
        var myStringafd : String = ""
        if fechaDato == "0000-00-00"{
            return ""
        }
        else
        {
            let date : Date = dateFormatter.date(from: fechaDato)!
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            myStringafd = formatter.string(from: date)
        }
        return myStringafd
    }
    
    public func devuelveFechaCorta(fecha : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let fechaDato : String = fecha
        var myStringafd : String = ""
        if fechaDato == "0000-00-00"{
            return ""
        }
        else
        {
            let date : Date = dateFormatter.date(from: fechaDato)!
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yy"
            myStringafd = formatter.string(from: date)
        }
        return myStringafd
    }
    
    public func devuelveFechaCortaLetras(fecha : String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let fechaDato : String = fecha
        var myStringafd : String = ""
        if fechaDato == "0000-00-00"{
            return ""
        }else{
            let date : Date = dateFormatter.date(from: fechaDato)!
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MMM"
            myStringafd = formatter.string(from: date)
        }
        return myStringafd
    }
    
    public func devuelveColorRGB (colorAprobacion : String) -> UIColor {
        
        let subColor : String = String(colorAprobacion[4 ... colorAprobacion.count - 2])
        let arrayColors : NSArray = subColor.split(separator: ",") as NSArray
        let colorRed : Double = Double(String(arrayColors[0] as! String))!
        let colorGreen : Double = Double(String(arrayColors[1] as! String))!
        let colorBlue : Double = Double(String(arrayColors[2] as! String))!
        
        return UIColor.init(red: CGFloat(colorRed/256.0), green: CGFloat(colorGreen/256.0), blue: CGFloat(colorBlue/256.0), alpha: 1.0)
        
    }
    
    func showAlertConnectivity(){
        
        let alerta = UIAlertController(title: "ALERTA", message: "Verifique su conexión a internet e inténtelo nuevamente.", preferredStyle: UIAlertControllerStyle.alert)
        alerta.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.cancel, handler: { (action: UIAlertAction!) in
            
        }))
        
        self.present(alerta, animated: true, completion: nil)
    }
    
    func showAlertDefault(title: String, message: String, titleButton: String) {
        
        // create the alert
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: titleButton, style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func actualizarServicios(numServ : String, tipServ : String){

        let servicios = UserGlobalData.sharedInstance.misServiciosGlobal!
        let arrayServicios: [MiObejetoServicio] = (servicios.ServiciosTipo!)
        
        //let servicioObj : MiObejetoServicio = arrayServicios[0]
        for i in 0 ... arrayServicios.count - 1 {
            let servicioObj : MiObejetoServicio = arrayServicios[i]
            if servicioObj.NroServicio == numServ &&  servicioObj.TipServ == tipServ {
                UserGlobalData.sharedInstance.misServiciosGlobal.NroServicio = servicioObj.NroServicio
                UserGlobalData.sharedInstance.misServiciosGlobal.TipServ = servicioObj.TipServ
                UserGlobalData.sharedInstance.misServiciosGlobal.DscServicio = servicioObj.DscServicio
                UserGlobalData.sharedInstance.misServiciosGlobal.FlgCoop = servicioObj.FlgCoop
                UserGlobalData.sharedInstance.misServiciosGlobal.FlgSabio = servicioObj.FlgSabio
            }
        }
    }
    
    func setGradient(uiView: UIView) {
        var count = 0
        if let arr = uiView.layer.sublayers {
            count = arr.count - 1
        }
        
        let colorBottom =  UIColor(red: 239.0/255.0, green: 212.0/255.0, blue: 80.0/255.0, alpha: 1.0).cgColor
        let colorTop = UIColor(red: 209.0/255.0, green: 142.0/255.0, blue: 24.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.startPoint = CGPoint(x:0.0, y:0.5)
        gradientLayer.endPoint = CGPoint(x:1.0, y:0.5)
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = uiView.bounds
        
        uiView.layer.insertSublayer(gradientLayer, at: UInt32(count))
    }
    
    func setGradient(uiView: UIView, at index:Int) {
        let colorBottom =  UIColor(red: 239.0/255.0, green: 212.0/255.0, blue: 80.0/255.0, alpha: 1.0).cgColor
        let colorTop = UIColor(red: 209.0/255.0, green: 142.0/255.0, blue: 24.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.startPoint = CGPoint(x:0.0, y:0.5)
        gradientLayer.endPoint = CGPoint(x:1.0, y:0.5)
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = uiView.bounds
        
        uiView.layer.insertSublayer(gradientLayer, at: UInt32(index))
        
    }
    func setGradient(inNavBar navBar:UINavigationBar) {
        let colorTop = UIColor(red: 209.0/255.0, green: 142.0/255.0, blue: 24.0/255.0, alpha: 1.0).cgColor
        let colorBottom =  UIColor(red: 239.0/255.0, green: 212.0/255.0, blue: 80.0/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.startPoint = CGPoint(x:0.0, y:0.5)
        gradientLayer.endPoint = CGPoint(x:1.0, y:0.5)
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = navBar.bounds
        
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        navBar.setBackgroundImage(image, for: .default)
    }
    
    public func devuelveSoloImagen(nombre:String) -> UIImage {
        self.showActivityIndicator()
        var imagenDevuelta : UIImage = UIImage()
        let url : URL = NSURL(string: nombre)! as URL
        Alamofire.request(url).responseImage { response in
            debugPrint(response)
            
            switch response.result {
                case .success:
                    if let image = response.result.value {
                        imagenDevuelta = image
                        self.hideActivityIndicator()
                    }
                    break
                case .failure(_):
                    self.hideActivityIndicator()
                    break
            }
        }
        return imagenDevuelta
        
    }
    
    public func saveImageMain(image: UIImage) -> Bool {
        guard let data = UIImageJPEGRepresentation(image, 1) ?? UIImagePNGRepresentation(image) else {
            return false
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return false
        }
        do {
            try data.write(to: directory.appendingPathComponent("fileName.png")!)
            return true
        } catch {
            return false
        }
    }
    
    public func saveImageTercero(image: UIImage) -> Bool {
        guard let data = UIImageJPEGRepresentation(image, 1) ?? UIImagePNGRepresentation(image) else {
            return false
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return false
        }
        do {
            try data.write(to: directory.appendingPathComponent("fileNameTercero.png")!)
            return true
        } catch {
            return false
        }
    }
    
    public func getSavedImage(named: String) -> UIImage? {
        if let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
            return UIImage(contentsOfFile: URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(named).path)
        }
        return nil
    }
    
    func clearTempFolder() {
        let fileManager = FileManager.default
        let tempFolderPath = NSTemporaryDirectory()
        do {
            let filePaths = try fileManager.contentsOfDirectory(atPath: tempFolderPath)
            for filePath in filePaths {
                try fileManager.removeItem(atPath: tempFolderPath + filePath)
            }
        } catch {}
    }
    
    public func generateRandomNumber(numDigits : Int) -> Int{
        var place = 1
        var finalNumber = 0;
        for _ in 0 ... numDigits{
            place *= 10
            let randomNumber = Int(arc4random_uniform(10))
            finalNumber += randomNumber * place
        }
        return finalNumber
    }
    func showPDF(from strUrl:String?){
        if let strUrlFinal = strUrl, !strUrlFinal.isEmpty, let url = URL(string: strUrlFinal.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!) {
            let safCont = SFSafariViewController(url: url)
            present(safCont, animated: true, completion: nil)
        }else{
            showAlert(PATHS.SENTINEL, mensaje: "No se pudo obtener la URL.")
        }
    }
    func showError99(){
        let alertCont = UIAlertController(title: PATHS.SENTINEL, message: "Su sesión se ha iniciado en otro dispositivo. Esta sesión se cerrará.", preferredStyle: .alert)
        alertCont.addAction(UIAlertAction(title: "Terminar", style: .default, handler: { (action) in
            self.goLogin()
        }))
        present(alertCont, animated: true, completion: nil)
    }
    func goLogin(){
        self.navigationController?.navigationController?.popViewController(animated: true)
    }
}
extension UIColor {
    static func fromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
}
