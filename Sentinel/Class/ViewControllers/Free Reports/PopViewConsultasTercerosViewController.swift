//
//  PopViewConsultasTercerosViewController.swift
//  Sentinel
//
//  Created by Daniel on 20/11/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class PopViewConsultasTercerosViewController:ParentViewController{
    @IBAction func cancel(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func accin(_ sender:Any){
        if let viewCont = self.navigationController?.parent as? MainTabBarViewController {
            viewCont.cambiarPantallaCompra()
        }
    }
}
