//
//  DetalleDeudaViewController.swift
//  App_Seninel
//
//  Created by Richy on 31/08/18.
//  Copyright © 2018 rmdesign. All rights reserved.
//

import UIKit

class DetalleDeudaViewController: ParentViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var montoDeuda: UILabel!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var deudasTV: UITableView!
    
    var FechaProceso : String = ""
    var montoDeudaDetalle : String = ""
    var dataArray = ["Bashi Corp.", "Interbank"]
    var dataArray2 = ["SUNAT", "ESSALUD"]
    
    var InfoSBSMicrof : NSArray = NSArray()
    var LineasCredito : NSArray = NSArray()
    var Vencidos : NSArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func cargarData(){
        self.montoDeuda.text = self.montoDeudaDetalle
        self.deudasTV.reloadData()
    }
    
    func iniConsultaDetalle(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.endConsultaDetalle(_:)), name: NSNotification.Name("endConsultaDetalle"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        let userData = UserGlobalData.sharedInstance.userGlobal
        let tipoDoc : String = userData!.TDocusu!
        let nroDoc : String = userData!.NumDocumento!
        let sesion : String = userData!.SesionId!
        let servicio : String = userData!.NroSerCT!
        
        let parametros = [
            "Usuario" : nroDoc,
            "Servicio" : servicio,
            "TipoDocumento" :  tipoDoc,
            "NroDocumento" : nroDoc,
            "SesionId" : sesion,
            "FechaProceso" : self.FechaProceso,
            "origenAplicacion" : PATHS.APP_ID,
            "TipoConsulta" : "D",
            ] as [String : Any]
        OriginData.sharedInstance.mesDetalle(notification: "endConsultaDetalle", parametros: parametros)
    }
    
    @objc func endConsultaDetalle(_ notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        
        if validaIndicador == 0 {
            
            let data = notification.object as! UserClass
            if data.codigoWS == "0" {
              
                let SDT_MSDeudaTitular : NSDictionary = data.SDT_MSDeudaTitular! as NSDictionary
                self.LineasCredito = NSArray(array: SDT_MSDeudaTitular.object(forKey: "LineasCredito") as! NSArray)
                self.Vencidos = NSArray(array: SDT_MSDeudaTitular.object(forKey: "Vencidos") as! NSArray)
                self.InfoSBSMicrof = NSArray(array: SDT_MSDeudaTitular.object(forKey: "InfoSBSMicrof") as! NSArray)
                self.cargarData()
            }
            else
            {
                if data.codigoWS != nil {
                    if data.codigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        iniMensajeError(codigo: (data.codigoWS!))
                    }
                }
                else
                {
                    let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                    alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                    self.present(alerta, animated: true, completion: nil)
                }
            }
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count : Int!
        
        if tableView == self.table {
            
            count = dataArray.count
            
        } else if tableView == self.deudasTV {
            
            count = dataArray2.count
            
        }
        
        return count
        
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.table {
            
            let identifier = "Cell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! DetalleDeudaTableViewCell
            cell.descripcionLB.text = dataArray[indexPath.row]
            return cell
            
        } else if tableView == self.deudasTV {
            
            let identifier = "CellD"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! deudasTableViewCell
            cell.descripcionLB.text = dataArray2[indexPath.row]
            return cell
            
        }
        
        let celda = UITableViewCell()
        return celda
        
    }
    
    

}
