//
//  LineasCreditoViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 5/16/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import UIKit
import XLPagerTabStrip
class LineasCreditoViewController:ParentViewController, IndicatorInfoProvider, UITableViewDataSource, UITableViewDelegate {
    //MARK: - Outlets
    @IBOutlet weak var lblLineaCredito:UILabel!
    @IBOutlet weak var tvLineasCreditos:UITableView!
    //MARK: - Attrs
    let userClass = UserGlobalData.sharedInstance.userGlobal!
    var arrLineasCredito:[[String: Any]] = []
    var fechaProceso = ""
    var servicio = ""
    var tipoDocumento = ""
    var nroDocumento = ""
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        showActivityIndicator()
        let parametros = ["Usuario": userClass.user!, "Servicio": servicio, "TipoDocumento": tipoDocumento, "NroDocumento": nroDocumento, "SesionId": userClass.SesionId!, "FechaProceso": fechaProceso, "origenAplicacion" : PATHS.APP_ID, "TipoConsulta" : "D"
            ] as [String : Any]
        NotificationCenter.default.addObserver(self, selector: #selector(self.endLineasCreditos(_:)), name: NSNotification.Name("endLineasCreditos"), object: nil)
        OriginData.sharedInstance.mesDetalle(notification: "endLineasCreditos", parametros: parametros)
    }
    //MARK: - IndicatorInfo
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "LÍNEAS DE CRÉDITO", image: UIImage(named: ""))
    }
    //MARK: - TableViewDataSource
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arrLineasCredito.isEmpty ? 0 : arrLineasCredito.count + 1
    }
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "lineaCreditoCell", for: indexPath) as! LineaCreditoTableViewCell
        let row = indexPath.row
        var dicLineaCredito:[String:Any]
        if row == 0 {
            dicLineaCredito = ["NombreEntidad": "Entidad", "LinCred": "L. Aprobada", "LinUtil": "L. Utilizada", "PorcLinUti": "%L. Uti."]
        }else{
            dicLineaCredito = arrLineasCredito[indexPath.row - 1]
        }
        cell.bind(with: dicLineaCredito, and: row == 0 ? TipoLineaCreditoCell.Cabecera : (row == arrLineasCredito.count ? TipoLineaCreditoCell.Total : TipoLineaCreditoCell.Linea))
        return cell
    }
    //MARK: - Auxiliar
    @objc func endLineasCreditos(_ notification:Notification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard let object = notification.object, let data = object as? UserClass, let codigoWs = data.codigoWS else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if codigoWs == "0" {
            arrLineasCredito = data.SDT_MSDeudaTitular!["LineasCredito"] as! [[String: Any]]
            lblLineaCredito.text = "Utilización de línea\nde crédito: \((arrLineasCredito.last!["PorcLinUti"] as! String).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))"
            tvLineasCreditos.reloadData()
        }else{
            if codigoWs == "99"{
                showError99()
            }else{
                iniMensajeError(codigo: codigoWs)
            }
        }
    }
}
