//
//  CreditosIndicadoresViewController.swift
//  Sentinel
//
//  Created by Richy on 10/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
import XLPagerTabStrip

class CreditosIndicadoresViewController: ParentViewController, UITableViewDelegate, UITableViewDataSource, IndicatorInfoProvider {
    @IBOutlet weak var tableTV: UITableView!
    @IBOutlet weak var viewTable: UIView!
    @IBOutlet weak var creditosLB: UILabel!
    
    var dataArray: [NSDictionary] = [NSDictionary]()
    
    override func viewWillAppear(_ animated: Bool) {
        self.cargarDatos()
    }
    
    func cargarDatos(){
        let Indicadires:NSArray = UserGlobalData.sharedInstance.userGlobal.Indicadores!
        self.dataArray = Indicadires as! [NSDictionary]
        var indicador : [NSDictionary] = [NSDictionary]()
        let userD = UserGlobalData.sharedInstance.userGlobal!
        if userD.haciaEmpresas == true {
            indicador = UserGlobalData.sharedInstance.userGlobal.InfoTitularEmpresas!["SDTIndicadoresCPT"] as! [NSDictionary]
        }else{
            indicador = userD.InfoTitular!["SDTIndicadoresCPT"] as! [NSDictionary]
        }
        
        if indicador.count == 0 {
            self.tableTV.isHidden = true
            self.creditosLB.isHidden = false
            self.viewTable.isHidden = true
        }
        for i in 0 ... dataArray.count - 1 {
            let obj : NSDictionary = self.dataArray[i]
            if indicador.count > 0 {
                for y in 0 ... indicador.count - 1 {
                    let obj2 : NSDictionary = indicador[y]
                    let primer : String = String(obj.object(forKey: "CICodInd") as! Int)
                    let segundo : String = String(obj2.object(forKey: "CICodInd") as! Int)
                    if primer == segundo{
                        self.dataArray = self.rearrange(array: self.dataArray, fromIndex: i, toIndex: 0)
                    }
                }
            }
        }
        
        self.tableTV.reloadData()
    }
    
    func rearrange<T>(array: Array<T>, fromIndex: Int, toIndex: Int) -> Array<T>{
        var arr = array
        let element = arr.remove(at: fromIndex)
        arr.insert(element, at: toIndex)
        
        return arr
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userData = UserGlobalData.sharedInstance.userGlobal!
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CreditosIndicadoresTableViewCell
        let objArray : NSDictionary = self.dataArray[indexPath.row]
        var indicador : [NSDictionary] = [NSDictionary]()
        if UserGlobalData.sharedInstance.userGlobal.haciaEmpresas == true {
            indicador = userData.InfoTitularEmpresas!["SDTIndicadoresCPT"] as! [NSDictionary]
        }else{
            indicador = userData.InfoTitular!["SDTIndicadoresCPT"] as! [NSDictionary]
        }
        cell.checkIV.image = nil
        let CICodInd : String = String(Int(objArray.object(forKey: "CICodInd") as! Int))
        let CICodIndDes : String = objArray.object(forKey: "CICodIndDes") as! String
        if indicador.count != 0 {
            let obj : NSDictionary = self.dataArray[indexPath.row]
            for y in 0 ... indicador.count - 1 {
                let obj2 : NSDictionary = indicador[y]
                let primer : String = String(obj.object(forKey: "CICodInd") as! Int)
                let segundo : String = String(obj2.object(forKey: "CICodInd") as! Int)
                if primer == segundo{
                    cell.checkIV.image = UIImage.init(named: "check.azul")
                    
                }
            }
            
        }
        cell.iconoIV.image = self.devuelveCreditosIndicadores(cred: CICodInd)
        cell.descripcionLB.text = CICodIndDes
        
        
        return cell
    }

    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        
        return IndicatorInfo(title: "CRÉDITOS E INDICADORES", image: UIImage(named: ""))
        
    }

}
