//
//  LineaCreditoTableViewCell.swift
//  Sentinel
//
//  Created by Victor Salazar on 5/17/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import UIKit

enum TipoLineaCreditoCell {
    case Cabecera, Linea, Total
}
class LineaCreditoTableViewCell:UITableViewCell {
    let FONT_SIZE = CGFloat(12)
    
    @IBOutlet weak var lblEntidad:UILabel!
    @IBOutlet weak var lblLineaAprobada:UILabel!
    @IBOutlet weak var lblLineaUtilizada:UILabel!
    @IBOutlet weak var lblPorcUtilizado:UILabel!
    @IBOutlet weak var vLinea:UIView!
    
    func bind(with dicLineaCredito:[String: Any], and tipo:TipoLineaCreditoCell){
        if tipo == .Cabecera {
            lblEntidad.font = UIFont.boldSystemFont(ofSize: FONT_SIZE)
            lblEntidad.textColor = UIColor.black
            lblLineaAprobada.font = UIFont.boldSystemFont(ofSize: FONT_SIZE)
            lblLineaAprobada.textColor = UIColor.black
            lblLineaUtilizada.font = UIFont.boldSystemFont(ofSize: FONT_SIZE)
            lblLineaUtilizada.textColor = UIColor.black
            lblPorcUtilizado.font = UIFont.boldSystemFont(ofSize: FONT_SIZE)
            lblPorcUtilizado.textColor = UIColor.black
            contentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            contentView.layer.cornerRadius = 5
            vLinea.isHidden = false
        }else if tipo == .Linea {
            lblEntidad.font = UIFont.systemFont(ofSize: FONT_SIZE)
            lblEntidad.textColor = UIColor.black
            lblLineaAprobada.font = UIFont.systemFont(ofSize: FONT_SIZE)
            lblLineaAprobada.textColor = UIColor.black
            lblLineaUtilizada.font = UIFont.systemFont(ofSize: FONT_SIZE)
            lblLineaUtilizada.textColor = UIColor.black
            lblPorcUtilizado.font = UIFont.systemFont(ofSize: FONT_SIZE)
            lblPorcUtilizado.textColor = UIColor.black
            contentView.layer.maskedCorners = []
            contentView.layer.cornerRadius = 0
            vLinea.isHidden = false
        }else{
            lblEntidad.font = UIFont.systemFont(ofSize: FONT_SIZE)
            lblEntidad.textColor = UIColor(hex: "06B150")
            lblLineaAprobada.font = UIFont.systemFont(ofSize: FONT_SIZE)
            lblLineaAprobada.textColor = UIColor(hex: "06B150")
            lblLineaUtilizada.font = UIFont.systemFont(ofSize: FONT_SIZE)
            lblLineaUtilizada.textColor = UIColor(hex: "06B150")
            lblPorcUtilizado.font = UIFont.systemFont(ofSize: FONT_SIZE)
            lblPorcUtilizado.textColor = UIColor(hex: "06B150")
            contentView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            contentView.layer.cornerRadius = 5
            vLinea.isHidden = true
            
        }
        lblEntidad.text = dicLineaCredito["NombreEntidad"] as? String
        lblLineaAprobada.text = dicLineaCredito["LinCred"] as? String
        lblLineaUtilizada.text = dicLineaCredito["LinUtil"] as? String
        lblPorcUtilizado.text = dicLineaCredito["PorcLinUti"] as? String
    }
}
