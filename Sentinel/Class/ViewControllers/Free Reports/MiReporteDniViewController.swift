//
//  MiReporteDniViewController.swift
//  Sentinel
//
//  Created by Richy on 10/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
import XLPagerTabStrip
class MiReporteDniViewController:ParentViewController, EnviarPDFHUDViewDelegate {
    @IBOutlet weak var tittleLB: UILabel!
    @IBOutlet weak var misreportesToolBar: UIToolbar!
    var esPremium = false
    var enviarPDF = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializarVariables()  
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.clipsToBounds = false
        self.tabBarController?.tabBar.layer.borderWidth = 0.1
        self.iniCreditos()
        self.validarPremium()
    }
    
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            self.esPremium = true
            self.changeStatusBar(cod: 3)
            self.setGradient(uiView: self.misreportesToolBar)
            self.tittleLB.text = "Mi Reporte DNI Premium"
        }
    }
    func inicializarVariables(){
        self.misreportesToolBar.clipsToBounds = true
    }
    public func iniGenerarPDF(){
        showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endGenerarPDF(_:)), name: NSNotification.Name("endGenerarPDF"), object: nil)
        
        let userData = UserGlobalData.sharedInstance.userGlobal!
        let usua = userData.user!
        let sesion = userData.SesionId!
        var numDoc = ""
        var tdoc = ""
        if UserGlobalData.sharedInstance.userGlobal.documentoTipoXPDF == "D"{
            numDoc = userData.user!
            tdoc = "D"
        }else{
            numDoc = userData.NroDocRel!
            tdoc = "R"
        }
        let servicio : String = userData.NroSerCT!
        var tipoReporte : String = ""
        if esPremium {
            tipoReporte = "D"
        }else{
            tipoReporte = "R"
        }
        
        let parametros = [
            "TDocUsu" : tdoc,
            "Usuario" : usua,
            "SesionId" : sesion,
            "Servicio" : servicio,
            "TipoDocumento" : tdoc,
            "NroDocumento" : numDoc,
            "EnvioMail" : enviarPDF ? "S" : "N",
            "TipoReporte" : tipoReporte
            ] as [String : Any]
        OriginData.sharedInstance.generarPDF(notification: "endGenerarPDF", parametros: parametros)
    }
    
    @objc func endGenerarPDF(_ notification:NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard notification.object != nil else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: .alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if let error = notification.object as? NSError {
            let message = error.code == -1001 ? "Tiempo de espera agotado." : "En estos momentos no es posible realizar la consulta."
            let alerta = UIAlertController(title: "ALERTA", message: message, preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        let data = notification.object as? UserClass
        if data?.CodigoWS == "0" {
            if enviarPDF {
                showAlert(PATHS.SENTINEL, mensaje: "En breve le estará llegando a su correo el reporte en PDF.")
            }else{
                showPDF(from: data?.urlPDF)
            }
        }else{
            if data?.CodigoWS != nil {
                if data?.CodigoWS == "99" {
                    showError99()
                }else{
                    let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: data?.MensajeRespuesta, preferredStyle: .alert)
                    let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                    alertaVersion.addAction(actionVersion)
                    self.present(alertaVersion, animated: true, completion: nil)
                }
            }
        }
    }
    
    func iniCreditos(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endCreditos(_:)), name: NSNotification.Name("endCreditos"), object: nil)
        let userData = UserGlobalData.sharedInstance.userGlobal!
        let usua = userData.user!
        let sesion = userData.SesionId!
        let parametros = ["Usuario" : usua, "SesionId" : sesion] as [String : Any]
        OriginData.sharedInstance.listadoIndicadores(notification: "endCreditos", parametros: parametros)
    }
    
    @objc func endCreditos(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as? UserClass
            if data?.CodigoWS == "0"{
                UserGlobalData.sharedInstance.userGlobal.Indicadores = data?.Indicadores
            }else{
                if data?.CodigoWS != nil {
                    if data?.CodigoWS == "99" {
                        showError99()
                    }else{
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: data?.MensajeRespuesta, preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    @IBAction func regresar(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func accionSend(_ sender: Any) {
        EnviarPDFHUDView.showEnviarPDFViewWithDelegate(self, with: "Reporte \(esPremium ? "Detallado" : "Resumido") PDF")
    }
    //MARK: - EnviarPDF
    func enviarPDFHUDDidConfirmWith(option index: Int) {
        enviarPDF = index == 1
        iniGenerarPDF()
    }
}
