//
//  ReportesXLBarPagerTabStripViewController.swift
//  Sentinel
//
//  Created by Daniel on 10/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class ReportesXLBarPagerTabStripViewController: ButtonBarPagerTabStripViewController {

    @IBOutlet weak var barBT: ButtonBarView!
    var isReload = false
    let graySpotifyColor = UIColor.fromHex(0x06B150)
    var esPremium : Bool = false
    
    override func viewDidLoad() {
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            self.esPremium = true
            self.setGradient(uiView: barBT)
        }

        super.viewDidLoad()
        settings.style.buttonBarHeight = 44.0
        if esPremium == false {
            settings.style.buttonBarBackgroundColor = graySpotifyColor
        }
        else
        {
            settings.style.buttonBarBackgroundColor = UIColor.clear
        }
        
        settings.style.buttonBarItemBackgroundColor = UIColor.clear
        settings.style.selectedBarBackgroundColor =  UIColor.fromHex(0xffffff)
        settings.style.buttonBarItemFont = UIFont(name: "HelveticaNeue-Bold", size:12) ??
            UIFont.systemFont(ofSize: 12)
        
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor.clear
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        
        settings.style.buttonBarLeftContentInset = 5
        settings.style.buttonBarRightContentInset = 5
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            if userData?.EsPremium == "S" {
                oldCell?.label.textColor = UIColor(white: 1, alpha: 0.5)
                newCell?.label.textColor = UIColor.fromHex(0xffffff)
            }
            else
            {
                oldCell?.label.textColor = UIColor.fromHex(0x127D3C)
                newCell?.label.textColor = UIColor.fromHex(0xffffff)
            }
        }
        super.viewDidLoad()
        
        self.reloadPagerTabStripView()
        
    }
    
    override func viewControllers(for pagerTabStripController:PagerTabStripViewController) -> [UIViewController] {
        let storyboardMain = UIStoryboard(name: "Main", bundle: nil)
        let MiReporteVC = storyboardMain.instantiateViewController(withIdentifier: "MiReporteVC")
        let lineasCreditosVC = storyboardMain.instantiateViewController(withIdentifier: "LineasCreditoVC") as! LineasCreditoViewController
        let SDTCPTMS = UserGlobalData.sharedInstance.userGlobal!.InfoTitular!["SDTCPTMS"] as! [String: Any]
        lineasCreditosVC.fechaProceso = SDTCPTMS["FechaProceso"] as! String
        lineasCreditosVC.servicio = "0"
        lineasCreditosVC.tipoDocumento = SDTCPTMS["TipoDocumento"] as! String
        lineasCreditosVC.nroDocumento = SDTCPTMS["NroDocumento"] as! String
        let VieronMiReporteVC = storyboardMain.instantiateViewController(withIdentifier: "VieronMiReporteVC")
        let CreditosIndicadoresVC = storyboardMain.instantiateViewController(withIdentifier: "CreditosIndicadoresVC")
        guard isReload else {
            return [MiReporteVC, lineasCreditosVC, VieronMiReporteVC, CreditosIndicadoresVC]
        }
        
        var childViewControllers = [MiReporteVC, lineasCreditosVC, VieronMiReporteVC, CreditosIndicadoresVC]
        
        for (index, _) in childViewControllers.enumerated() {
            let nElements = childViewControllers.count - index
            let n = (Int(arc4random()) % nElements) + index
            if n != index{
                childViewControllers.swapAt(index, n)
            }
        }
        let nItems = 2
        return Array(childViewControllers.prefix(Int(nItems))) 
    }
    
    func validarPremium(){
        
    }
    
    func setGradient(uiView: UIView) {
        
        let colorBottom =  UIColor(red: 239.0/255.0, green: 212.0/255.0, blue: 80.0/255.0, alpha: 1.0).cgColor
        let colorTop = UIColor(red: 209.0/255.0, green: 142.0/255.0, blue: 24.0/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.startPoint = CGPoint(x:0.0, y:0.5)
        gradientLayer.endPoint = CGPoint(x:1.0, y:0.5)
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = uiView.bounds
        
        uiView.layer.insertSublayer(gradientLayer, at: 0)
        
    }
    override func reloadPagerTabStripView() {
        moveToViewController(at: 3, animated: true)
        super.reloadPagerTabStripView()
    }
}
