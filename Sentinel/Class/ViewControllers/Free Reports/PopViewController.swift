//
//  PopViewController.swift
//  Sentinel
//
//  Created by Daniel on 19/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class PopViewController: ParentViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancel(_ sender: Any) {
        //dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func accin(_ sender: Any) {
        
        let nav = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SentinelPremiumVC") as! SentinelPremiumViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
