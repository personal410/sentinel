//
//  ResumidoMesMiReporteViewController.swift
//  Sentinel
//
//  Created by Daniel Montoya on 12/31/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class ResumidoMesMiReporteViewController:ParentViewController {
    @IBOutlet weak var toolbarDetalle:UIToolbar!
    @IBOutlet weak var vInfo:UIView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblDocument:UILabel!
    
    var esPremium = false
    let userData = UserGlobalData.sharedInstance.userGlobal!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(22, forKey: "check")
        let dic = userData.InfoTitular!["SDTCPTMS"] as! NSDictionary
        let tipoDoc = dic.object(forKey: "TipoDocumento") as! String
        let nroDoc =  dic.object(forKey: "NroDocumento") as! String
        var nombreCompleto = ""
        var documento = ""
        if tipoDoc == "D" {
            let nombre = dic.object(forKey: "Nombre") as! String
            let apePat = dic.object(forKey: "ApePaterno") as! String
            let apeMat = dic.object(forKey: "ApeMaterno") as! String
            nombreCompleto = "\(apePat) \(apeMat) \(nombre)"
            documento = "DNI: \(nroDoc)"
        }else if tipoDoc == "R" {
            nombreCompleto = dic.object(forKey: "NomRazSoc") as! String
            documento = "RUC: \(nroDoc)"
        }
        if userData.vieneEmpresas {
            let SDTCPTMS = userData.InfoTitularEmpresas!["SDTCPTMS"] as! NSDictionary
            let dni = SDTCPTMS.object(forKey: "NroDocumento") as! String
            if dni.count == 8 {
                documento = "DNI: \(dni)"
            }else if dni.count == 11 {
                documento = "RUC: \(dni)"
            }
            nombreCompleto = SDTCPTMS.object(forKey: "NomRazSoc") as? String ?? ""
        }
        lblName.text = nombreCompleto
        lblDocument.text = documento
    }
    override func viewWillAppear(_ animated: Bool) {
        self.validarPremium()
    }
    
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal!
        if userData.EsPremium == "S" {
            self.esPremium = true
            changeStatusBar(cod: 3)
            setGradient(uiView: toolbarDetalle)
            setGradient(uiView: vInfo)
        }
    }
    func iniGenerarPDF(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endGenerarPDF(_:)), name: NSNotification.Name("endGenerarPDF"), object: nil)
        let userData = UserGlobalData.sharedInstance.userGlobal!
        let usua : String = userData.user!
        let sesion : String = userData.SesionId!
        var numDoc : String = ""
        var tdoc : String = ""//userData.TDocusu!
        if UserGlobalData.sharedInstance.userGlobal.documentoTipoXPDF == "D"{
            numDoc = userData.user!
            tdoc = "D"
        }else{
            numDoc = userData.NroDocRel!
            tdoc = "R"
        }
        let servicio : String = userData.NroSerCT!
        var tipoReporte : String = ""
        if userData.EsPremium == "S" {
            tipoReporte = "D"
        }else{
            tipoReporte = "R"
        }
        let parametros:[String:Any] = ["TDocUsu" : tdoc, "Usuario" : usua, "SesionId" : sesion, "Servicio" : servicio, "TipoDocumento" : tdoc, "EnvioMail" : "S", "NroDocumento" : numDoc, "TipoReporte" : tipoReporte]
        OriginData.sharedInstance.generarPDF(notification: "endGenerarPDF", parametros: parametros)
    }
    
    @objc func endGenerarPDF(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if let error = notification.object as? NSError {
            let message = error.code == -1001 ? "Tiempo de espera agotado." : "En estos momentos no es posible realizar la consulta."
            let alerta = UIAlertController(title: "ALERTA", message: message, preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as? UserClass
            if data?.CodigoWS == "0"{
                self.showAlert(PATHS.SENTINEL, mensaje: "En breve le estará llegando a su correo el reporte en PDF.")
            }else{
                if data?.CodigoWS != nil {
                    if data?.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: data?.MensajeRespuesta, preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func accionPDF(_ sender: Any) {
        self.iniGenerarPDF()
    }
}
