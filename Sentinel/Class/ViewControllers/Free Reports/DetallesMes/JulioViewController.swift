//
//  JulioViewController.swift
//  Sentinel
//
//  Created by Daniel on 24/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
import XLPagerTabStrip
class JulioViewController:ParentViewController, IndicatorInfoProvider, EnviarPDFHUDViewDelegate {

    @IBOutlet weak var montoDeudaMesLB: UILabel!
    @IBOutlet weak var detalleDeudasTV: UITableView!
    @IBOutlet weak var detalleVencidosTV: UITableView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var alturaInfoView: NSLayoutConstraint!//176 -> 75 sin tabla //55 x celda
    @IBOutlet weak var alturaVencidosView: NSLayoutConstraint!
    @IBOutlet weak var btnVerPDF:UIButton!
    
    var esPremium = false, enviarPDF = false
    
    var usuarioBean: UserClass!
    var usuarioBeanMes : UserClass?
    var cantidadArray : NSArray = NSArray()
    var vencidosArray : NSArray = NSArray()
    var parentVC:LoaderVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializarVariables()
        self.limpiarDatos()
        self.showActivityIndicator()
    }
    override func viewWillAppear(_ animated:Bool){
        if let parentVC = parentVC {
            if parentVC.cargandoMesSeleccionado {
                let arrayMeses = UserGlobalData.sharedInstance.userGlobal.mesesArreglo
                let dicMesActual = arrayMeses.object(at: 11 - parentVC.mesDefault) as! NSDictionary
                let mes = dicMesActual.object(forKey: "mes") as! String
                if mes == "7" {
                    self.iniDetalleDeudaConsultas()
                }
            }else{
                self.iniDetalleDeudaConsultas()
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func limpiarDatos(){
        self.montoDeudaMesLB.text = ""
        
    }
    func cargarDatos(){
        let InfoSBSMicrof : NSArray = usuarioBean?.SDT_MSDeudaTitular!["InfoSBSMicrof"] as! NSArray
        cantidadArray = usuarioBean?.SDT_MSDeudaTitular!["InfoSBSMicrof"] as! NSArray
        vencidosArray = usuarioBean?.SDT_MSDeudaTitular!["Vencidos"] as! NSArray
        self.montoDeudaMesLB.text = "\(0.00)"
        var valorASumar : Double = 0
        if cantidadArray.count != 0 {
            
            var deuda : Double = 0
            for i in 0 ... InfoSBSMicrof.count - 1{
                let obj :NSDictionary = InfoSBSMicrof[i] as! NSDictionary
                let deudaString = obj.object(forKey: "SaldoDeuda") as! String
                deuda = deuda + Double(deudaString)!
            }
            if "\(deuda)" == "0.00"{
                self.montoDeudaMesLB.text = "\(deuda)"
                valorASumar = deuda
            }
            else
            {
                let newDeuda: NSNumber = NSNumber.init( value: Double(deuda))
                
                let numberFormatter = NumberFormatter()
                numberFormatter.formatterBehavior = .behavior10_4
                numberFormatter.numberStyle = .decimal
                let numberString = numberFormatter.string(from: newDeuda)
                valorASumar = deuda
                self.montoDeudaMesLB.text = numberString
            }
            
            if cantidadArray.count == 2 {
                self.alturaInfoView.constant = 176
            }
            else if cantidadArray.count == 1 {
                self.alturaInfoView.constant = 126
            }
            else if cantidadArray.count == 3 {
                self.alturaInfoView.constant = 226
            }
            else if cantidadArray.count == 4 {
                self.alturaInfoView.constant = 276
            }
            else if cantidadArray.count == 5 {
                self.alturaInfoView.constant = 326
            }
            else
            {
                let alturaTabla : Int = self.cantidadArray.count * 50
                self.alturaInfoView.constant = CGFloat(75 + alturaTabla + 15) // 15 del contraint de Bot
            }
        }else{
            self.alturaInfoView.constant = 90
        }
        if vencidosArray.count != 0 {
            var deuda : Double = 0
            for i in 0 ... vencidosArray.count - 1{
                let obj :NSDictionary = vencidosArray[i] as! NSDictionary
                let diasVenc = obj.object(forKey: "DiasVencidosVen") as! Int
                if diasVenc > 0 {
                    let deudaString = obj.object(forKey: "SaldoDeudaVen") as! String
                    deuda = deuda + Double(deudaString)!
                }
            }
            if "\(deuda)" == "0.00"{
                valorASumar = valorASumar + deuda
                self.montoDeudaMesLB.text = "\(deuda)"
            }
            else
            {
                valorASumar = valorASumar + deuda
                let newDeuda: NSNumber = NSNumber.init( value: Double(valorASumar))
                
                let numberFormatter = NumberFormatter()
                numberFormatter.formatterBehavior = .behavior10_4
                numberFormatter.numberStyle = .decimal
                let numberString = numberFormatter.string(from: newDeuda)
                
                self.montoDeudaMesLB.text = numberString
            }
            if vencidosArray.count == 2 {
                self.alturaVencidosView.constant = 176
            }
            else if vencidosArray.count == 1 {
                self.alturaVencidosView.constant = 126
            }
            else if vencidosArray.count == 3 {
                self.alturaVencidosView.constant = 226
            }
            else if vencidosArray.count == 4 {
                self.alturaVencidosView.constant = 276
            }
            else if vencidosArray.count == 5 {
                self.alturaVencidosView.constant = 326
            }
            else
            {
                let alturaTabla : Int = self.vencidosArray.count * 50
                self.alturaVencidosView.constant = CGFloat(75 + alturaTabla + 15) // 15 del contraint de Bot
            }
        }
        else
        {
            self.alturaVencidosView.constant = 75 + 15
        }
        if cantidadArray.count == 0 && vencidosArray.count == 0 {
            self.scrollView.isScrollEnabled = false
        }
        self.detalleDeudasTV.reloadData()
        self.detalleVencidosTV.reloadData()
    }
    
    func inicializarVariables(){
        usuarioBean = UserGlobalData.sharedInstance.userGlobal
        esPremium = usuarioBean.EsPremium == "S"
        btnVerPDF.setTitle("Ver Reporte \(esPremium ? "Detallado" : "Resumido") PDF", for: .normal)
        self.detalleDeudasTV.delegate = self
        self.detalleVencidosTV.delegate = self
    }
    
    func iniGenerarPDF(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endGenerarPDF(_:)), name: NSNotification.Name("endGenerarPDF"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        
        let userData = UserGlobalData.sharedInstance.userGlobal!
        let usua : String = userData.user!
        let sesion : String = userData.SesionId!
        var numDoc : String = ""
        var tdoc : String = ""//userData.TDocusu!
        if UserGlobalData.sharedInstance.userGlobal.documentoTipoXPDF == "D"{
            numDoc = userData.user!
            tdoc = "D"
        }
        else
        {
            numDoc = userData.NroDocRel!
            tdoc = "R"
        }
        let servicio : String = userData.NroSerCT!
        var tipoReporte : String = ""
        if userData.EsPremium == "S" {
            tipoReporte = "D"
        }
        else
        {
            tipoReporte = "R"
        }
        
        let parametros = [
            "TDocUsu" : tdoc,
            "Usuario" : usua,
            "SesionId" : sesion,
            "Servicio" : servicio,
            "EnvioMail" : enviarPDF ? "S" : "N",
            "TipoDocumento" : tdoc,
            "NroDocumento" : numDoc,
            "TipoReporte" : tipoReporte
            ] as [String : Any]
        OriginData.sharedInstance.generarPDF(notification: "endGenerarPDF", parametros: parametros)
    }
    
    @objc func endGenerarPDF(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if let error = notification.object as? NSError {
            let message = error.code == -1001 ? "Tiempo de espera agotado." : "En estos momentos no es posible realizar la consulta."
            let alerta = UIAlertController(title: "ALERTA", message: message, preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as? UserClass
            if data?.CodigoWS == "0"{
                if enviarPDF {
                    self.showAlert(PATHS.SENTINEL, mensaje: "En breve le estará llegando a su correo el reporte en PDF.")
                }else{
                    showPDF(from: data?.urlPDF)
                }
            }
            else
            {
                if data?.CodigoWS != nil {
                    if data?.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: data?.MensajeRespuesta, preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                        //iniMensajeError(codigo: (data?.CodigoWS!)!)
                    }
                }
            }
        }
    }
    
    func iniDetalleDeudaConsultas(){
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        NotificationCenter.default.addObserver(self, selector: #selector(self.endDetalleDeudaConsultas(_:)), name: NSNotification.Name("endDetalleDeudaConsultas_julio"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        
        let usuario : String = usuarioBean.user!
        var tdocumento : String = usuarioBean.TDocusu!
        var numDocumento : String = usuarioBean.NumDocumento!
        var tipoConsulta = "D"
        if usuarioBean.vieneEmpresas == true
        {
            tdocumento = usuarioBean.tEmpresa
            numDocumento = usuarioBean.nEmpresa
            tipoConsulta = "L"
        }
        let sesion : String = usuarioBean.SesionId!
        var fechaProceso : String = ""
        //fecha
        let meses = self.devuelveArregloUltimosMeses(fechaUltima: UserGlobalData.sharedInstance.userGlobal.FechaReporte)
        var anioMesActual : String = ""
        for i in 0 ... meses.count - 1 {
            let objMes : NSDictionary = meses[i] as! NSDictionary
            let anioNum : String = objMes.object(forKey: "anio") as! String
            let mesNum : String = objMes.object(forKey: "mes") as! String
            if mesNum == "7"{
                anioMesActual = anioNum
            }
        }
        
        let fechaReporte = UserGlobalData.sharedInstance.userGlobal.FechaReporte
        let arrFechaReporte = fechaReporte.split(separator: "-")
        let diaReporte = arrFechaReporte[1] == "07" ? arrFechaReporte[2] : "31"
        if anioMesActual != "" {
            fechaProceso = "\(anioMesActual)-07-\(diaReporte)"
            let parametros = [
                "Usuario" : usuario,
                "Servicio" : "0",
                "TipoDocumento" : tdocumento,
                "NroDocumento" : numDocumento,
                "SesionId" : sesion,
                "FechaProceso" : fechaProceso,
                "origenAplicacion" : PATHS.APP_ID,
                "TipoConsulta" : tipoConsulta
                ] as [String : Any]
            OriginData.sharedInstance.mesDetalle(notification: "endDetalleDeudaConsultas_julio", parametros: parametros)
        }
    }
    
    @objc func endDetalleDeudaConsultas(_ notification: NSNotification){
        parentVC?.cargandoMesSeleccionado = false
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as? UserClass
            if data?.codigoWS == "0"{
                usuarioBeanMes = UserGlobalData.sharedInstance.userGlobal
                usuarioBeanMes?.SDT_MSDeudaTitular = data?.SDT_MSDeudaTitular
                self.cargarDatos()
            }
            else
            {
                if data?.CodigoWS != nil {
                    if data?.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                        alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                        self.present(alerta, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        
        var enero : NSDictionary = NSDictionary()
        let array : NSMutableArray = self.devuelveArregloUltimosMeses(fechaUltima: UserGlobalData.sharedInstance.userGlobal.FechaReporte)
        for i in 0 ... array.count - 1 {
            let objMes : NSDictionary = array[i] as! NSDictionary
            let numMes : String = objMes.object(forKey: "mes") as! String
            if numMes == "7"{
                enero = objMes
            }
        }
        let nombre : String = enero.object(forKey: "mes") as! String
        let anio : String = enero.object(forKey: "anio") as! String
        
        let nombreMes : String = self.devuelveNombreMesParticionado(mes: nombre)
        
        let titulo : String = "\(nombreMes).\(anio)"
        return IndicatorInfo(title: titulo, image: UIImage(named: ""))
        
    }
    
    @IBAction func accionReporte(_ sender: Any) {
        EnviarPDFHUDView.showEnviarPDFViewWithDelegate(self, with: "Reporte \(esPremium ? "Detallado" : "Resumido") PDF")
    }
    //MARK: - EnviarPDF
    func enviarPDFHUDDidConfirmWith(option index: Int) {
        enviarPDF = index == 1
        iniGenerarPDF()
    }
}

extension JulioViewController : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count : Int!
        
        if tableView == self.detalleDeudasTV {
            
            count = cantidadArray.count
            
        } else if tableView == self.detalleVencidosTV {
            
            count = vencidosArray.count
            
        }
        
        return count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.detalleDeudasTV {
            
            let identifier = "Cell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! DetalleDeudaTableViewCell
            let array : NSArray = usuarioBeanMes?.SDT_MSDeudaTitular!["InfoSBSMicrof"] as! NSArray
            let objArray : NSDictionary = array[indexPath.row] as!NSDictionary
            cell.descripcionLB.text = objArray.object(forKey: "NombreEntidad") as? String
            let deuda : String = objArray.object(forKey: "SaldoDeuda") as! String
            
            if "0.00" == objArray.object(forKey: "SaldoDeuda") as? String{
                cell.deudaLB.text = "\(deuda)"
            }
            else
            {
                //let deudaDouble = Double(deuda)
                let newDeuda: NSNumber = NSNumber.init( value: Double(deuda)!)
                
                let numberFormatter = Toolbox.getNumberFormmatter()
                let numberString = numberFormatter.string(from: newDeuda)
                
                cell.deudaLB.text = numberString
            }
            let calificacion : Int = (objArray.object(forKey: "CalifEnt") as? Int)!
            let nombreCalificacion : String = (self.devuelveObjetoCalificacion(codigo: calificacion)["nombre"])!
            if nombreCalificacion == "CPP" || nombreCalificacion == "SCAL"{
                cell.califLB.textColor = UIColor.black
            }else{
                cell.califLB.textColor = UIColor.white
            }
            cell.califLB.text = nombreCalificacion
            let coloCalificacion : String = (self.devuelveObjetoCalificacion(codigo: calificacion)["color"])!
            let splitColor = coloCalificacion.split(separator: ",")
            let red :Double = Double(splitColor[0])!
            let green :Double = Double(splitColor[1])!
            let blue :Double = Double(splitColor[2])!
            
            cell.califLB.backgroundColor = UIColor.init(red: CGFloat(red/255.0), green: CGFloat(green/255.0), blue: CGFloat(blue/255.0), alpha: 1.0)
            let diasVencidos : Int64 = objArray.object(forKey: "DiasVencidos") as! Int64
            cell.diasVencidos.text = "\(diasVencidos)"
            let fecha : String = (objArray.object(forKey: "FechaReporte") as? String)!
            cell.fechaInf.text = self.devuelveFechaCorta(fecha: fecha)
            //cell.califLB.text =
            //cell.descripcionLB.text = dataArray[indexPath.row]
            return cell
            
        }
        if tableView == self.detalleVencidosTV {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellD", for: indexPath) as! deudasTableViewCell
            let objArray : NSDictionary = vencidosArray[indexPath.row] as!NSDictionary
            let NombreEntidadVen: String = (objArray.object(forKey: "NombreEntidadVen") as? String)!
            cell.descripcionLB.text = NombreEntidadVen
            let DiasVencidosVen: Int = (objArray.object(forKey: "DiasVencidosVen") as? Int)!
            if DiasVencidosVen == 0 {
                cell.montoLB.text = ""
                cell.diasVencidos.text = ""
            }else{
                let SaldoDeudaVen: String = (objArray.object(forKey: "SaldoDeudaVen") as? String)!
                let deuda : String = SaldoDeudaVen
                
                if "0.00" == objArray.object(forKey: "SaldoDeuda") as? String{
                    cell.montoLB.text = "\(deuda)"
                }else{
                    let newDeuda: NSNumber = NSNumber(value: Double(deuda)!)
                    let numberFormatter = Toolbox.getNumberFormmatter()
                    let numberString = numberFormatter.string(from: newDeuda)
                    cell.montoLB.text = numberString
                }
                cell.diasVencidos.text = "\(DiasVencidosVen)"
                cell.montoLB.textColor = UIColor.black
                var colorMonto:UIColor!
                if DiasVencidosVen == 0 {
                    colorMonto = UIColor.clear
                }else if DiasVencidosVen <= 30 {
                    colorMonto = UIColor(red: 240.0 / 255.0, green: 201.0 / 255.0, blue: 21.0 / 2550.0, alpha: 1.0)
                }else{
                    cell.montoLB.textColor = UIColor.white
                    colorMonto = UIColor(red: 236.0 / 255.0, green: 42.0 / 255.0, blue: 63.0 / 2550.0, alpha: 1.0)
                }
                cell.montoLB.backgroundColor = colorMonto
            }
            return cell
        }
        
        let celda = UITableViewCell()
        return celda
        
    }
}



