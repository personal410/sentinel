//
//  XLPageDetalleMesViewController.swift
//  Sentinel
//
//  Created by Daniel on 24/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import XLPagerTabStrip
class XLPageDetalleMesViewController:ButtonBarPagerTabStripViewController, LoaderVC {
    var cargandoMesSeleccionado:Bool = true
    var mesDefault:Int = UserDefaults.standard.object(forKey: "mesSemaforo") as! Int
    @IBOutlet weak var barBT:ButtonBarView!
    var esPremium = false
    var EneroViewController : EneroViewController!
    var FebreroViewController : FebreroViewController!
    var MarzoViewController : MarzoViewController!
    var AbrilViewController : AbrilViewController!
    var MayoViewController : MayoViewController!
    var JunioViewController : JunioViewController!
    var JulioViewController : JulioViewController!
    var AgostoViewController : AgostoViewController!
    var SetiembreViewController : SetiembreViewController!
    var OctubreViewController : OctubreViewController!
    var NoviembreViewController : NoviembreViewController!
    var DiciembreViewController : DiciembreViewController!
    override func viewDidLoad(){
        self.estilos()
    }
    override func viewDidAppear(_ animated:Bool){
        buttonBarView.selectItem(at: IndexPath(item: mesDefault, section: 0), animated: true, scrollPosition: [])
        buttonBarView.moveTo(index: mesDefault, animated: true, swipeDirection: .right, pagerScroll: .yes)
        if mesDefault > 0 {
            moveToViewController(at: mesDefault, animated: true)
        }else{
            viewControllers.first?.viewDidAppear(true)
        }
    }
    override func viewDidLayoutSubviews(){
        print("viewDidLayoutSubviews")
    }
    func estilos(){
        esPremium = UserGlobalData.sharedInstance.userGlobal.EsPremium == "S"
        if esPremium {
            self.setGradient(uiView: barBT)
        }
        settings.style.buttonBarHeight = 44.0
        settings.style.buttonBarBackgroundColor = !esPremium ? UIColor.fromHex(0x06B150) : UIColor.clear
        settings.style.buttonBarItemBackgroundColor = UIColor.clear
        settings.style.selectedBarBackgroundColor =  UIColor.fromHex(0xffffff)
        settings.style.buttonBarItemTitleColor = esPremium ? UIColor(white: 1, alpha: 0.5) : UIColor.fromHex(0x137d3d)
        settings.style.buttonBarItemFont = UIFont(name: "HelveticaNeue-Bold", size:12) ?? UIFont.systemFont(ofSize: 12)
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 5
        settings.style.buttonBarRightContentInset = 5
        pagerBehaviour = PagerTabStripBehaviour.common(skipIntermediateViewControllers: true)
        changeCurrentIndex = {(oldCell:ButtonBarViewCell?, newCell:ButtonBarViewCell?, animated:Bool) -> Void in
            oldCell?.label.textColor = self.esPremium ? UIColor(white: 1, alpha: 0.5) : UIColor.fromHex(0x137d3d)
            newCell?.label.textColor = UIColor.fromHex(0xffffff)
        }
        super.viewDidLoad()
    }
    
    override func viewControllers(for pagerTabStripController:PagerTabStripViewController) -> [UIViewController] {
        let arrayMesesController = NSMutableArray()
        let arrayMeses = UserGlobalData.sharedInstance.userGlobal.mesesArreglo
        let storyboardMain = UIStoryboard(name: "MisReportes", bundle: nil)
        EneroViewController = storyboardMain.instantiateViewController(withIdentifier: "EneroMRVC") as! EneroViewController
        EneroViewController.parentVC = self
        FebreroViewController = storyboardMain.instantiateViewController(withIdentifier: "FebreroMRVC") as! FebreroViewController
        FebreroViewController.parentVC = self
        MarzoViewController = storyboardMain.instantiateViewController(withIdentifier: "MarzoMRVC") as! MarzoViewController
        MarzoViewController.parentVC = self
        AbrilViewController = storyboardMain.instantiateViewController(withIdentifier: "AbrilMRVC") as! AbrilViewController
        AbrilViewController.parentVC = self
        MayoViewController = storyboardMain.instantiateViewController(withIdentifier: "MayoMRVC") as! MayoViewController
        MayoViewController.parentVC = self
        JunioViewController = storyboardMain.instantiateViewController(withIdentifier: "JunioMRVC") as! JunioViewController
        JunioViewController.parentVC = self
        JulioViewController = storyboardMain.instantiateViewController(withIdentifier: "JulioMRVC") as! JulioViewController
        JulioViewController.parentVC = self
        AgostoViewController = storyboardMain.instantiateViewController(withIdentifier: "AgostoMRVC") as! AgostoViewController
        AgostoViewController.parentVC = self
        SetiembreViewController = storyboardMain.instantiateViewController(withIdentifier: "SetiembreMRVC") as! SetiembreViewController
        SetiembreViewController.parentVC = self
        OctubreViewController = storyboardMain.instantiateViewController(withIdentifier: "OctubreMRVC") as! OctubreViewController
        OctubreViewController.parentVC = self
        NoviembreViewController = storyboardMain.instantiateViewController(withIdentifier: "NoviembreMRVC") as! NoviembreViewController
        NoviembreViewController.parentVC = self
        DiciembreViewController = storyboardMain.instantiateViewController(withIdentifier: "DiciembreMRVC") as! DiciembreViewController
        DiciembreViewController.parentVC = self
        for i in 0 ... arrayMeses.count - 1{
            let objMes = arrayMeses[i] as! NSDictionary
            let numMes = objMes.object(forKey: "mes") as! String
            if numMes == "1" {
                arrayMesesController.add(EneroViewController)
            }else if numMes == "2" {
                arrayMesesController.add(FebreroViewController)
            }else if numMes == "3" {
                arrayMesesController.add(MarzoViewController)
            }else if numMes == "4" {
                arrayMesesController.add(AbrilViewController)
            }else if numMes == "5"{
                arrayMesesController.add(MayoViewController)
            }else if numMes == "6"{
                arrayMesesController.add(JunioViewController)
            }else if numMes == "7"{
                arrayMesesController.add(JulioViewController)
            }else if numMes == "8"{
                arrayMesesController.add(AgostoViewController)
            }else if numMes == "9"{
                arrayMesesController.add(SetiembreViewController)
            }else if numMes == "10"{
                arrayMesesController.add(OctubreViewController)
            }else if numMes == "11"{
                arrayMesesController.add(NoviembreViewController)
            }else if numMes == "12"{
                arrayMesesController.add(DiciembreViewController)
            }
        }
        return arrayMesesController.reversed() as! [UIViewController]
    }
    func setGradient(uiView: UIView) {
        let colorBottom =  UIColor(red: 239.0/255.0, green: 212.0/255.0, blue: 80.0/255.0, alpha: 1.0).cgColor
        let colorTop = UIColor(red: 209.0/255.0, green: 142.0/255.0, blue: 24.0/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.startPoint = CGPoint(x:0.0, y:0.5)
        gradientLayer.endPoint = CGPoint(x:1.0, y:0.5)
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = uiView.bounds
        uiView.layer.insertSublayer(gradientLayer, at: 0)
    }
}
