//
//  XLPageResumenMesViewController.swift
//  Sentinel
//
//  Created by Daniel Montoya on 12/31/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import XLPagerTabStrip
class XLPageResumenMesViewController: ButtonBarPagerTabStripViewController, LoaderVC {
    var cargandoMesSeleccionado:Bool = true
    var mesDefault:Int = UserDefaults.standard.object(forKey: "mesSemaforo") as! Int
    
    @IBOutlet weak var barBB: ButtonBarView!
    var esPremium = false
    var arrayMesesController = NSMutableArray()
    var arrayMeses = NSMutableArray()
    var usuario : UserClass!
    
    var EneroViewController : EneroViewController!
    var FebreroViewController : FebreroViewController!
    var MarzoViewController : MarzoViewController!
    var AbrilViewController : AbrilViewController!
    var MayoViewController : MayoViewController!
    var JunioViewController : JunioViewController!
    var JulioViewController : JulioViewController!
    var AgostoViewController : AgostoViewController!
    var SetiembreViewController : SetiembreViewController!
    var OctubreViewController : OctubreViewController!
    var NoviembreViewController : NoviembreViewController!
    var DiciembreViewController : DiciembreViewController!
    
    var isReload = false
    let graySpotifyColor = UIColor.fromHex(0x06B150)
    var tabActivo = 1
    
    override func viewDidLoad() {
        self.inicializarVariables()
        self.estilos()
    }
    override func viewDidAppear(_ animated:Bool){
        super.viewDidAppear(animated)
        if mesDefault - 9 > 0 {
            moveToViewController(at: mesDefault - 9, animated: true)
        }
    }
    func inicializarVariables(){
        self.usuario = UserGlobalData.sharedInstance.userGlobal
    }
    func estilos(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            self.esPremium = true
            self.setGradient(uiView: barBB)
        }
        settings.style.buttonBarHeight = 44.0
        if esPremium == false {
            settings.style.buttonBarBackgroundColor = graySpotifyColor
        }else{
            settings.style.buttonBarBackgroundColor = UIColor.clear
        }
        settings.style.buttonBarItemBackgroundColor = UIColor.clear
        settings.style.selectedBarBackgroundColor =  UIColor.fromHex(0xffffff)
        settings.style.buttonBarItemFont = UIFont(name: "HelveticaNeue-Bold", size:12) ??
            UIFont.systemFont(ofSize: 12)
        
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor.clear
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        
        settings.style.buttonBarLeftContentInset = 5
        settings.style.buttonBarRightContentInset = 5
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            if userData?.EsPremium == "S" {
                oldCell?.label.textColor = UIColor(white: 1, alpha: 0.5)
            }else{
                oldCell?.label.textColor = UIColor.fromHex(0x137d3d)
            }
            newCell?.label.textColor = UIColor.fromHex(0xffffff)
        }
        super.viewDidLoad()
    }
    
    override func viewControllers(for pagerTabStripController:PagerTabStripViewController) -> [UIViewController] {
        self.arrayMeses = UserGlobalData.sharedInstance.userGlobal.mesesArreglo
        
        let storyboardMain = UIStoryboard(name: "MisReportes", bundle: nil)
        
        EneroViewController = storyboardMain.instantiateViewController(withIdentifier: "EneroMRVC") as! EneroViewController
        EneroViewController.parentVC = self
        FebreroViewController = storyboardMain.instantiateViewController(withIdentifier: "FebreroMRVC") as! FebreroViewController
        FebreroViewController.parentVC = self
        MarzoViewController = storyboardMain.instantiateViewController(withIdentifier: "MarzoMRVC") as! MarzoViewController
        MarzoViewController.parentVC = self
        AbrilViewController = storyboardMain.instantiateViewController(withIdentifier: "AbrilMRVC") as! AbrilViewController
        AbrilViewController.parentVC = self
        MayoViewController = storyboardMain.instantiateViewController(withIdentifier: "MayoMRVC") as! MayoViewController
        MayoViewController.parentVC = self
        JunioViewController = storyboardMain.instantiateViewController(withIdentifier: "JunioMRVC") as! JunioViewController
        JunioViewController.parentVC = self
        JulioViewController = storyboardMain.instantiateViewController(withIdentifier: "JulioMRVC") as! JulioViewController
        JulioViewController.parentVC = self
        AgostoViewController = storyboardMain.instantiateViewController(withIdentifier: "AgostoMRVC") as! AgostoViewController
        AgostoViewController.parentVC = self
        SetiembreViewController = storyboardMain.instantiateViewController(withIdentifier: "SetiembreMRVC") as! SetiembreViewController
        SetiembreViewController.parentVC = self
        OctubreViewController = storyboardMain.instantiateViewController(withIdentifier: "OctubreMRVC") as! OctubreViewController
        OctubreViewController.parentVC = self
        NoviembreViewController = storyboardMain.instantiateViewController(withIdentifier: "NoviembreMRVC") as! NoviembreViewController
        NoviembreViewController.parentVC = self
        DiciembreViewController = storyboardMain.instantiateViewController(withIdentifier: "DiciembreMRVC") as! DiciembreViewController
        DiciembreViewController.parentVC = self
        for i in 0 ... arrayMeses.count - 1 {
            let objMes = arrayMeses[i] as! NSDictionary
            let numMes = objMes.object(forKey: "mes") as! String
            if numMes  == "1" {
                arrayMesesController.add(EneroViewController)
            }else if numMes == "2"{
                arrayMesesController.add(FebreroViewController)
            }else if numMes == "3"{
                arrayMesesController.add(MarzoViewController)
            }else if numMes == "4"{
                arrayMesesController.add(AbrilViewController)
            }else if numMes == "5"{
                arrayMesesController.add(MayoViewController)
            }else if numMes == "6"{
                arrayMesesController.add(JunioViewController)
            }else if numMes == "7"{
                arrayMesesController.add(JulioViewController)
            }else if numMes == "8"{
                arrayMesesController.add(AgostoViewController)
            }else if numMes == "9"{
                arrayMesesController.add(SetiembreViewController)
            }else if numMes == "10"{
                arrayMesesController.add(OctubreViewController)
            }else if numMes == "11"{
                arrayMesesController.add(NoviembreViewController)
            }else if numMes == "12"{
                arrayMesesController.add(DiciembreViewController)
            }
        }
        let swiftArray = arrayMesesController as AnyObject as! [AnyObject]
        let objSwift0 = swiftArray[0]
        let objSwift1 = swiftArray[1]
        let objSwift2 = swiftArray[2]
        
        self.arrayMesesController = NSMutableArray()
        self.arrayMesesController.add(objSwift2)
        self.arrayMesesController.add(objSwift1)
        self.arrayMesesController.add(objSwift0)
        
        let nsArray = NSArray(array: self.arrayMesesController)
        guard isReload else {
            return [nsArray[0] as! UIViewController, nsArray[1] as! UIViewController,nsArray[2] as! UIViewController]
        }
        let childViewControllers = [(nsArray[0] as! UIViewController), (nsArray[1] as! UIViewController),(nsArray[2] as! UIViewController)] as [UIViewController]
        let nItems = 3 + (arc4random() % 8)
        return Array(childViewControllers.prefix(Int(nItems)))
    }
    // MARK: - Métodos generales
    override func reloadPagerTabStripView() {
        isReload = true
        moveToViewController(at: 6, animated: true)
        super.reloadPagerTabStripView()
    }
    func setGradient(uiView: UIView) {
        let colorBottom =  UIColor(red: 239.0/255.0, green: 212.0/255.0, blue: 80.0/255.0, alpha: 1.0).cgColor
        let colorTop = UIColor(red: 209.0/255.0, green: 142.0/255.0, blue: 24.0/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.startPoint = CGPoint(x:0.0, y:0.5)
        gradientLayer.endPoint = CGPoint(x:1.0, y:0.5)
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = uiView.bounds
        uiView.layer.insertSublayer(gradientLayer, at: 0)
    }
}
