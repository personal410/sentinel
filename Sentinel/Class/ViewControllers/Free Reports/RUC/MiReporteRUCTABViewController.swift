//
//  MiReporteRUCTABViewController.swift
//  Sentinel
//
//  Created by Daniel on 25/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
//MiReporteRUCTABViewController
import XLPagerTabStrip
class MiReporteRUCTABViewController:ParentViewController, IndicatorInfoProvider {
    @IBOutlet weak var foto: UIImageView!
    @IBOutlet weak var nombreLB: UILabel!
    @IBOutlet weak var nroDocumentoLB: UILabel!
    @IBOutlet weak var quieroBT: UIButton!
    @IBOutlet weak var semaforoIMG: UIImageView!
    @IBOutlet weak var deudaLB: UILabel!
    @IBOutlet weak var semaforoDosIMG: UIImageView!
    @IBOutlet weak var deudaDosLB: UILabel!
    @IBOutlet weak var signoDeudaLB: UILabel!
    @IBOutlet weak var signoDeudaDos: UILabel!
    @IBOutlet weak var creditosCL: UICollectionView!
    @IBOutlet weak var semaforosCL: UICollectionView!
    @IBOutlet weak var controlTermometro:SFControlTermometro!
    @IBOutlet weak var sinCreditos: UILabel!
    @IBOutlet weak var vInfo:UIView!
    @IBOutlet weak var vInfoPremium:UIView!
    @IBOutlet weak var vComprarPremium:UIView!
    
    var esPremium = ""
    var usuarioBean = UserGlobalData.sharedInstance.userGlobal!
    var creditosArray = NSArray()
    var imgArray = [UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"), UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris"),UIImage(named: "24gris")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserGlobalData.sharedInstance.userGlobal.tabIndicador = 1
    }
    override func viewWillAppear(_ animated: Bool) {
        esPremium = usuarioBean.EsPremium!
        if esPremium == "S" {
            vComprarPremium.isHidden = true
        }
        let SDTCPTMS = usuarioBean.InfoTitular!["SDTCPTMS"] as! NSDictionary
        if let imageMain = getSavedImage(named: "fileName.png") {
            self.foto.image = imageMain
        }
        self.nombreLB.text = SDTCPTMS.object(forKey: "NomRazSoc") as? String
        let dni = SDTCPTMS.object(forKey: "NroDocumento") as! String
        if dni.count == 8 {
            self.nroDocumentoLB.text = "DNI: \(dni)"
        }
        else if dni.count == 11 {
            self.nroDocumentoLB.text = "RUC: \(dni)"
        }
        UserGlobalData.sharedInstance.userGlobal.FechaReporte = SDTCPTMS.object(forKey: "FechaProceso") as! String
        let indicador = usuarioBean.InfoTitular!["SDTIndicadoresCPT"] as! [NSDictionary]
        self.creditosArray = indicador as NSArray
        if self.creditosArray.count == 0 {
            self.sinCreditos.isHidden = false
        }else{
            self.creditosCL.reloadData()
        }
        let sema = SDTCPTMS.object(forKey: "Semaforos") as! String
        if(sema != "" && sema.count == 12){
            imgArray = devuelveArrayIMGSemaforos(semaforos: sema) as! [UIImage?]
        }else{
            imgArray = devuelveArrayIMGSemaforos(semaforos: "GGGGGGGGGGGG") as! [UIImage?]
        }
        self.semaforosCL.reloadData()
        var mostrarScore = esPremium == "S" ? "S" : "N"
        if let infoTit = usuarioBean.InfoTitular, let mostScore = infoTit["MostrarScore"] as? String {
            mostrarScore = mostScore
        }
        if mostrarScore == "S" {
            vInfoPremium.isHidden = false
            vInfo.isHidden = true
            let semaforoActivo = SDTCPTMS.object(forKey: "SemActual") as! String
            self.semaforoDosIMG.image = self.devuelveSemaforo(SemAct: semaforoActivo)
            self.cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: self.signoDeudaDos)
            self.cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: self.deudaDosLB)
            let deuda = SDTCPTMS.object(forKey: "DeudaTotal") as! String
            if deuda == "0.00"{
                self.deudaDosLB.text = deuda
            }else{
                let numberFormatter = NumberFormatter()
                numberFormatter.formatterBehavior = .behavior10_4
                numberFormatter.numberStyle = .decimal
                self.deudaDosLB.text = numberFormatter.string(from: NSNumber(value: Double(deuda)!))
            }
            let dictSentinel = usuarioBean.InfoTitular!["ScoreSentinel"] as! NSDictionary
            let ScoreSentinel = dictSentinel.mutableCopy() as! NSMutableDictionary
            self.controlTermometro.formatoTermometro(ScoreSentinel)
        }else{
            vInfoPremium.isHidden = true
            vInfo.isHidden = false
            var semaforoActivo = SDTCPTMS.object(forKey: "SemActual") as! String
            if semaforoActivo == "G" {
                semaforoActivo = "3"
            }
            self.semaforoIMG.image = self.devuelveSemaforo(SemAct: semaforoActivo)
            self.cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: self.signoDeudaLB)
            self.cambiaColorObjetoConLetra(colorActivo: semaforoActivo, objeto: self.deudaLB)
            let deuda : String = SDTCPTMS.object(forKey: "DeudaTotal") as! String
            if deuda == "0.00"{
                self.deudaLB.text = deuda
            }else{
                let numberFormatter = NumberFormatter()
                numberFormatter.formatterBehavior = .behavior10_4
                numberFormatter.numberStyle = .decimal
                self.deudaLB.text = numberFormatter.string(from: NSNumber(value: Double(deuda)!))
            }
        }
    }
    override func viewDidAppear(_ animated:Bool){
        esPremium = UserGlobalData.sharedInstance.userGlobal.EsPremium!
        self.semaforosCL.scrollToItem(at: IndexPath(item: 11, section: 0), at: .right, animated: false)
    }
    @objc func selectedCell(sender: UIButton)  {
        if UserGlobalData.sharedInstance.userGlobal.EsPremium == "S"{
            UserGlobalData.sharedInstance.userGlobal.mesesArreglo = self.devuelveArregloUltimosMeses(fechaUltima: UserGlobalData.sharedInstance.userGlobal.FechaReporte)
            UserGlobalData.sharedInstance.userGlobal.vieneEmpresas = false
            UserDefaults.standard.set(sender.tag, forKey: "mesSemaforo")
            let nav = UIStoryboard(name: "MisReportes", bundle: nil).instantiateViewController(withIdentifier: "DetalleMesMiReporteVC")
            self.navigationController?.pushViewController(nav, animated: true)
        }else{
            if sender.tag == -1 {
                let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopupVC1")
                self.navigationController?.pushViewController(nav, animated: true)
            }else{
                UserGlobalData.sharedInstance.userGlobal.mesesArreglo = self.devuelveArregloUltimosMeses(fechaUltima: UserGlobalData.sharedInstance.userGlobal.FechaReporte)
                UserGlobalData.sharedInstance.userGlobal.vieneEmpresas = false
                UserDefaults.standard.set(sender.tag, forKey: "mesSemaforo")
                let nav = UIStoryboard(name: "MisReportes", bundle: nil).instantiateViewController(withIdentifier: "ResumidoMesTerceroResumidosVC")
                self.navigationController?.pushViewController(nav, animated: true)
            }
        }
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "REPORTE", image: UIImage(named: ""))
    }
    @IBAction func accionLoquiero(_ sender: Any) {
        let nav = storyboard!.instantiateViewController(withIdentifier: "SentinelPremiumVC")
        self.navigationController?.pushViewController(nav, animated: true)
    }
}

extension MiReporteRUCTABViewController:UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.semaforosCL {
            return imgArray.count
        }else if collectionView == self.creditosCL{
            return creditosArray.count
        }
        return imgArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.semaforosCL {
            let identifier = "SemaforoCVC"
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! SemaforoMiReporteCollectionViewCell
            cell.semaIMG.image = imgArray[indexPath.row]
            let arregloMesDesc = self.devuelveArregloUltimosMeses(fechaUltima: UserGlobalData.sharedInstance.userGlobal.FechaReporte)
            var swiftArray = arregloMesDesc as AnyObject as! [AnyObject]
            let objSwift0 = swiftArray[0]
            let objSwift1 = swiftArray[1]
            let objSwift2 = swiftArray[2]
            let objSwift3 = swiftArray[3]
            let objSwift4 = swiftArray[4]
            let objSwift5 = swiftArray[5]
            let objSwift6 = swiftArray[6]
            let objSwift7 = swiftArray[7]
            let objSwift8 = swiftArray[8]
            let objSwift9 = swiftArray[9]
            let objSwift10 = swiftArray[10]
            let objSwift11 = swiftArray[11]
            
            let arrayMesesController = NSMutableArray()
            arrayMesesController.add(objSwift11)
            arrayMesesController.add(objSwift10)
            arrayMesesController.add(objSwift9)
            arrayMesesController.add(objSwift8)
            arrayMesesController.add(objSwift7)
            arrayMesesController.add(objSwift6)
            arrayMesesController.add(objSwift5)
            arrayMesesController.add(objSwift4)
            arrayMesesController.add(objSwift3)
            arrayMesesController.add(objSwift2)
            arrayMesesController.add(objSwift1)
            arrayMesesController.add(objSwift0)
            
            let objMes = arrayMesesController[indexPath.row] as! NSDictionary
            let nombre = objMes.object(forKey: "mes") as! String
            let anio = objMes.object(forKey: "anio") as! String
            let nombreMes = self.devuelveNombreMesParticionado(mes: nombre)
            cell.mesLB.text = "\(nombreMes)\n\(anio)"
            cell.indexBT.tag = UserGlobalData.sharedInstance.userGlobal.EsPremium == "S" ? indexPath.row : (indexPath.row < 9 ? -1 : indexPath.row)
            cell.indexBT.addTarget(self, action: #selector(self.selectedCell(sender:)), for: .touchUpInside)
            return cell
        }else if collectionView == self.creditosCL {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CreditosCVC", for: indexPath) as! CreditosCollectionViewCell
            let objIndicador = creditosArray[indexPath.row] as! NSDictionary
            let codigo = "\((objIndicador.object(forKey: "CICodInd"))!)"
            cell.aimagenCreditos.image =  self.devuelveCreditosIndicadores(cred: codigo)
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView:UICollectionView, layout collectionViewLayout:UICollectionViewLayout, minimumLineSpacingForSectionAt section:Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var widthItem  = 30
        var heighItem = 30
        if collectionView == self.semaforosCL {
            widthItem = 45
            heighItem = 60
        }else if collectionView == self.creditosCL {
            widthItem  = 30
            heighItem = 40
        }
        return CGSize(width: widthItem, height: heighItem)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.creditosCL {
            if let parentVC = self.parent as? RUCReportesXLBarPagerTabStripViewController {
                parentVC.moveToViewController(at: 1)
            }
        }
    }
}
