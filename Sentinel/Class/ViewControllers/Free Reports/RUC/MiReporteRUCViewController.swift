//
//  MiReporteRUCViewController.swift
//  Sentinel
//
//  Created by Daniel on 25/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//MiReporteRUCViewController
import UIKit
import XLPagerTabStrip
class MiReporteRUCViewController:ParentViewController, EnviarPDFHUDViewDelegate {
    @IBOutlet weak var tittleLB: UILabel!
    @IBOutlet weak var misreportesToolBar: UIToolbar!
    var esPremium = false
    var enviarPDF = false
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.inicializarVariables()
    }
    override func viewWillAppear(_ animated:Bool){
        self.tabBarController?.tabBar.clipsToBounds = false
        self.tabBarController?.tabBar.layer.borderWidth = 0.1
        self.validarPremium()
    }
    
    func inicializarVariables(){
        self.misreportesToolBar.clipsToBounds = true
    }
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            self.esPremium = true
            self.changeStatusBar(cod: 3)
            self.setGradient(uiView: self.misreportesToolBar)
            self.tittleLB.text = "Mi Reporte RUC Premium"
        }
    }
    func iniGenerarPDF(){
        showActivityIndicator()
        let userData = UserGlobalData.sharedInstance.userGlobal!
        let usua : String = userData.user!
        let sesion : String = userData.SesionId!
        var numDoc : String = ""
        var tdoc : String = ""//userData.TDocusu!
        if UserGlobalData.sharedInstance.userGlobal.documentoTipoXPDF == "D"{
            numDoc = userData.user!
            tdoc = "D"
        }
        else
        {
            numDoc = userData.NroDocRel!
            tdoc = "R"
        }
        let servicio : String = userData.NroSerCT!
        var tipoReporte : String = ""
        if userData.EsPremium == "S" {
            tipoReporte = "D"
        }
        else
        {
            tipoReporte = "R"
        }
        
        let parametros = [
            "TDocUsu" : tdoc,
            "Usuario" : usua,
            "SesionId" : sesion,
            "Servicio" : servicio,
            "TipoDocumento" : tdoc,
            "NroDocumento" : numDoc,
            "TipoReporte" : tipoReporte,
            "EnvioMail" : enviarPDF ? "S" : "N"
            ] as [String : Any]
        NotificationCenter.default.addObserver(self, selector: #selector(self.endGenerarPDF(_:)), name: NSNotification.Name("endGenerarPDF"), object: nil)
        OriginData.sharedInstance.generarPDF(notification: "endGenerarPDF", parametros: parametros)
    }
    
    @objc func endGenerarPDF(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if let error = notification.object as? NSError {
            let message = error.code == -1001 ? "Tiempo de espera agotado." : "En estos momentos no es posible realizar la consulta."
            let alerta = UIAlertController(title: "ALERTA", message: message, preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as? UserClass
            if data?.CodigoWS == "0"{
                if enviarPDF {
                    showAlert(PATHS.SENTINEL, mensaje: "En breve le estará llegando a su correo el reporte en PDF.")
                }else{
                    showPDF(from: data?.urlPDF)
                }
            }
            else
            {
                if data?.CodigoWS != nil {
                    if data?.CodigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: data?.MensajeRespuesta, preferredStyle: .alert)
                        let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                        alertaVersion.addAction(actionVersion)
                        self.present(alertaVersion, animated: true, completion: nil)
                    }
                }
            }
        }
    }

    @IBAction func regresar(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func accionSend(_ sender: Any) {
        EnviarPDFHUDView.showEnviarPDFViewWithDelegate(self, with: "Reporte \(esPremium ? "Detallado" : "Resumido") PDF")
    }
    //MARK: - EnviarPDF
    func enviarPDFHUDDidConfirmWith(option index: Int) {
        enviarPDF = index == 1
        iniGenerarPDF()
    }
}
