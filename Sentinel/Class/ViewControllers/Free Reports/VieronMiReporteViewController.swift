//
//  VieronMiReporteViewController.swift
//  Sentinel
//
//  Created by Richy on 10/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class VieronMiReporteViewController: ParentViewController,  UITableViewDelegate, UITableViewDataSource, IndicatorInfoProvider {
    
    @IBOutlet weak var vieronTV: UITableView!
    var arregloVisitas : NSArray = NSArray()
    var dataArray = ["Sentinel Perú SA", "Cmac Piura Sac", "Aquino Salazar Cesar Augusto", "Caja Municipal de ahorro y crédito de Arequipa"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.iniVieron()
    }
    
    func iniVieron(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.endVieron(_:)), name: NSNotification.Name("endVieron"), object: nil)
        self.showActivityIndicator(view: (UIApplication.shared).keyWindow!)
        let userData = UserGlobalData.sharedInstance.userGlobal
        let tipoDoc : String = userData!.TDocusu!
        let nroDoc : String = userData!.NumDocumento!
        let sesion : String = userData!.SesionId!
        
        let parametros = [
            "TDocUsuario" : tipoDoc,
            "Usuario" : nroDoc,
            "SesionId" :  sesion
            ] as [String : Any]
        OriginData.sharedInstance.vieronMiReporte(notification: "endVieron", parametros: parametros)   
    }
    
    @objc func endVieron(_ notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        
        if validaIndicador == 0 {
            
            let data = notification.object as! UserClass
            if data.codigoWS == "0" {
                self.arregloVisitas = NSArray.init(array: data.VieronMiReporte!)
                self.vieronTV.reloadData()
            }
            else
            {
                if data.codigoWS != nil {
                    if data.codigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        iniMensajeError(codigo: (data.codigoWS!))
                    }
                }
                else
                {
                    let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                    alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                    self.present(alerta, animated: true, completion: nil)
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arregloVisitas.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! MiReporteTableViewCell
        let objVisitas :NSDictionary = arregloVisitas[indexPath.row] as! NSDictionary
        let inputFecha = DateFormatter();
        inputFecha.dateFormat = "yyyy-MM-dd"
        let showdata = inputFecha.date(from: objVisitas.object(forKey: "FechaUltAcceso") as! String)
        inputFecha.dateFormat = "dd/MM/yyyy"
        cell.fechaLB.text = inputFecha.string(for: showdata)
        cell.personaLB.text = objVisitas.object(forKey: "NombreRazonSocial") as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        if UserGlobalData.sharedInstance.userGlobal.EsPremium == "S"{
            UserGlobalData.sharedInstance.userGlobal.VieronMiReporteObj = self.arregloVisitas[indexPath.row] as! NSDictionary
        }
        else
        {
            //irComprar()
        }
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        
        return IndicatorInfo(title: "VIERON MI REPORTE", image: UIImage(named: ""))
        
    }

}
