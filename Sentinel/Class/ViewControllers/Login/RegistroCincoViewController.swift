//
//  RegistroCincoViewController.swift
//  Sentinel
//
//  Created by Daniel on 31/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class RegistroCincoViewController:ParentViewController {
    @IBOutlet weak var checkUnoBT: UIButton!
    @IBOutlet weak var checkDosBT: UIButton!
    @IBOutlet weak var aceptoBT: UIButton!
    @IBOutlet weak var noAceptoBT: UIButton!
    
    var dniUsuario:String = ""
    var claveUsuario:String = ""
    var valorUno = 0
    var valorDos = 0
    var AceTyC = ""
    var OtoCon = ""
    var didAccept = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gestureScreen()
        self.estilos()
    }
    func estilos(){
        self.aceptoBT.layer.cornerRadius = 5
        self.noAceptoBT.layer.cornerRadius = 5
    }
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    func validar() -> Bool{
        if self.valorUno == 0 {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe aceptar los términos y condiciones.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func iniRegistroCuatro(){
        showActivityIndicator()
        let parametros = [
            "Usuario" : self.dniUsuario,
            "ClaveInicial" : self.claveUsuario,
            "AceTyC" : AceTyC,
            "OtoCon" : OtoCon,
            "TipoContr" : "1"
            ] as [String : Any]
        NotificationCenter.default.addObserver(self, selector: #selector(self.endRegistroCuatro(_:)), name: NSNotification.Name("endRegistroCuatro"), object: nil)
        OriginData.sharedInstance.registroCuatro(notification: "endRegistroCuatro", parametros: parametros)
    }
    @objc func endRegistroCuatro(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        guard let data = notification.object as? UserClass else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if let codigoWs = data.CodigoWS {
            if codigoWs == "0" {
                let nav = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
                if didAccept {
                    nav.cargaTerminosCondiciones = 1
                    nav.usuaTerminos = self.dniUsuario
                    nav.pwdTerminos = self.claveUsuario
                }
                self.navigationController?.pushViewController(nav, animated: true)
            }else if codigoWs == "99" {
                let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "Su sesión se ha iniciado en otro dispositivo. Esta sesión se cerrará.", preferredStyle: .alert)
                let actionVersionGO = UIAlertAction.init(title: "Terminar", style: .default, handler: { (action: UIAlertAction!) in
                    
                    let nav = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
                    self.navigationController?.pushViewController(nav, animated: true)
                })
                alertaVersion.addAction(actionVersionGO)
                self.present(alertaVersion, animated: true, completion: nil)
            }else{
                iniMensajeError(codigo: codigoWs)
            }
        }else{
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
        }
    }
    @IBAction func accionCheckPrimer(_ sender: Any) {
        if valorUno == 0 {
            valorUno = 1
            self.checkUnoBT.setImage(UIImage.init(named: "check_on"), for: .normal)
            AceTyC = "S"
        }else{
            valorUno = 0
            self.checkUnoBT.setImage(UIImage.init(named: "check_off"), for: .normal)
            AceTyC = "N"
        }
    }
    @IBAction func accionCheckSegundo(_ sender: Any) {
        if valorDos == 0 {
            valorDos = 1
            self.checkDosBT.setImage(UIImage.init(named: "check_on"), for: .normal)
            OtoCon = "S"
        }
        else
        {
            valorDos = 0
            self.checkDosBT.setImage(UIImage.init(named: "check_off"), for: .normal)
            OtoCon = "N"
        }
    }
    
    @IBAction func accionAcepto(_ sender: Any) {
        if validar() {
            didAccept = true
            self.iniRegistroCuatro()
        }
    }
    
    @IBAction func accionNoAcepto(_ sender: Any) {
        let alertaVersion = UIAlertController.init(title: "Sentinel", message: "Al no aceptar, quedará inconcluso el proceso de registro. ¿Desea continuar?", preferredStyle: .alert)
        let actionVersion = UIAlertAction.init(title: "NO", style: .cancel, handler: nil)
        let actionVersionGO = UIAlertAction.init(title: "SI", style: .default, handler: { (action: UIAlertAction!) in
            self.didAccept = false
            self.showActivityIndicator()
            NotificationCenter.default.addObserver(self, selector: #selector(self.endRegistroCuatro(_:)), name: NSNotification.Name("endRegistroCuatro"), object: nil)
            let parametros:[String : Any] = ["Usuario" : self.dniUsuario, "ClaveInicial" : self.claveUsuario, "AceTyC" : "N", "OtoCon" : "N", "TipoContr" : "1"]
            OriginData.sharedInstance.registroCuatro(notification: "endRegistroCuatro", parametros: parametros)
        })
        alertaVersion.addAction(actionVersion)
        alertaVersion.addAction(actionVersionGO)
        self.present(alertaVersion, animated: true, completion: nil)
    }
    @IBAction func atrasAccion(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        self.navigationController?.popViewController(animated: false)
        self.navigationController?.popViewController(animated: false)
        self.navigationController?.popViewController(animated: false)
        self.navigationController?.popViewController(animated: true)
    }
}
