//
//  RegistroCuatroViewController.swift
//  Sentinel
//
//  Created by Daniel on 31/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class RegistroCuatroViewController: ParentViewController {
    @IBOutlet weak var claveTXT: UITextField!
    @IBOutlet weak var confirmarClaveTXT: UITextField!
    @IBOutlet weak var continuarbt: UIButton!
    
    var dniUsuario = ""
    var claveUsuario = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gestureScreen()
        self.estilos()
    }
    
    func estilos(){
        self.continuarbt.layer.cornerRadius = 5
    }
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func validar() -> Bool{
        if self.claveTXT.text == nil || self.claveTXT.text == "" {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe ingresar el la clave de su nueva cuenta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        if self.confirmarClaveTXT.text == nil || self.confirmarClaveTXT.text == "" {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe volver a ingresar la clave.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        if (self.claveTXT.text?.count)! <= 5{
            let alerta = UIAlertController(title: "ALERTA", message: "La clave debe contener más de 5 dígitos.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        if self.claveTXT.text != self.confirmarClaveTXT.text{
            let alerta = UIAlertController(title: "ALERTA", message: "Las claves no coinciden.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    @IBAction func accionContinuar(_ sender: Any) {
        if validar(){
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "RegistroCincoVC") as! RegistroCincoViewController
            nav.dniUsuario = self.dniUsuario
            nav.claveUsuario = self.claveTXT.text!
            self.navigationController?.pushViewController(nav, animated: true)
        }
    }
    @IBAction func accionMostrar(_ sender: UISwitch) {
        if sender.isOn{
            self.claveTXT.isSecureTextEntry = false
            self.confirmarClaveTXT.isSecureTextEntry = false
        }
        else
        {
            self.claveTXT.isSecureTextEntry = true
            self.confirmarClaveTXT.isSecureTextEntry = true
        }
    }
    
    @IBAction func atrasAccion(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        self.navigationController?.popViewController(animated: false)
        self.navigationController?.popViewController(animated: false)
        self.navigationController?.popViewController(animated: true)
    }

  
}
