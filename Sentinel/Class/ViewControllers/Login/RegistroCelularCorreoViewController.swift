//
//  RegistroCelularCorreoViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 1/29/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
class RegistroCelularCorreoViewController:ParentViewController {
    @IBOutlet weak var tfCelular:UITextField!
    @IBOutlet weak var tfCorreo:UITextField!
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let viewCont = segue.destination as! RegistroTerminosViewController
        viewCont.celular = tfCelular.text!
        viewCont.correo = tfCorreo.text!
    }
    
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func continuar(){
        let celular = tfCelular.text!
        if celular.count != 9 || celular[0..<1] != "9" {
            let alertCont = UIAlertController(title: "Alerta", message: "Ingrese un número de celular válido", preferredStyle: .alert)
            alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alertCont, animated: true, completion: nil)
            return
        }
        let correo = tfCorreo.text!
        if correo.isEmpty || !correo.isValidEmail() {
            let alertCont = UIAlertController(title: "Alerta", message: "Ingrese un correo electrónico válido", preferredStyle: .alert)
            alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(alertCont, animated: true, completion: nil)
            return
        }
        performSegue(withIdentifier: "showTerminos", sender: nil)
    }
}
