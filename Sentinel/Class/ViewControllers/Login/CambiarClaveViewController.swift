//
//  CambiarClaveViewController.swift
//  Sentinel
//
//  Created by Daniel on 6/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class CambiarClaveViewController:ParentViewController, UITextFieldDelegate {
    @IBOutlet weak var toolbar:UIToolbar!
    @IBOutlet weak var anteriorTXT: UITextField!
    @IBOutlet weak var nuevaTXT: UITextField!
    @IBOutlet weak var confirmaTXT: UITextField!
    @IBOutlet weak var continuarBT: UIButton!
    @IBOutlet weak var cambiarToolbar: UIToolbar!
    var esPremium = false
    var irMenuPrincipal = true
    let user = UserGlobalData.sharedInstance.userGlobal
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gestureScreen()
        self.estilos()
        if let finalUser = user {
            if finalUser.EsPremium == "S" {
                self.changeStatusBar(cod: 3)
                self.setGradient(uiView: toolbar)
            }
        }
    }
    func estilos(){
        self.continuarBT.layer.cornerRadius = 5
    }
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func validar() -> Bool{
        if self.anteriorTXT.text == nil || self.anteriorTXT.text == "" {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe ingresar la clave actual.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        if self.nuevaTXT.text == nil || self.nuevaTXT.text == "" {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe ingresar la clave nueva.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        if self.confirmaTXT.text == nil || self.confirmaTXT.text == "" {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe volver a ingresar la clave nueva.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        if (self.nuevaTXT.text?.count)! <= 5 {
            let alerta = UIAlertController(title: "ALERTA", message: "La clave debe contener más de 5 dígitos.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        if self.nuevaTXT.text != self.confirmaTXT.text{
            let alerta = UIAlertController(title: "ALERTA", message: "Las claves no coinciden.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func iniCambiarClave(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.endCambiarClave(_:)), name: NSNotification.Name("endCambiarClave"), object: nil)
        showActivityIndicator()
        let userData = UserGlobalData.sharedInstance.userGlobal!
        let parametros = ["Usuario" : userData.user!,
            "IdSesion" : userData.SesionId!,
            "UseCod" : userData.user!,
            "Caso" : "USU",
            "UseClave" : anteriorTXT.text!,
            "UseClaveNuev" : nuevaTXT.text!,
            "UseClaveConf" : confirmaTXT.text!,
            "OrigenAplicacion" : PATHS.APP_ID] as [String : Any]
        OriginData.sharedInstance.cambiarClave(notification: "endCambiarClave", parametros: parametros)
    }
    
    @objc func endCambiarClave(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard notification.object != nil, let data = notification.object as? UserClass else {
            Toolbox.showAlert(with: "ALERTA", and: "En estos momentos no es posible realizar la consulta.", in: self)
            return
        }
        if data.Codigo == "0" {
            if data.EsValido == "S" {
                if irMenuPrincipal {
                    let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarVC")
                    navigationController?.pushViewController(nav, animated: true)
                }else{
                    navigationController?.popViewController(animated: true)
                }
            }else{
                let mensaje : String = data.Mensaje!
                let alerta = UIAlertController(title: "ALERTA", message: mensaje, preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                self.present(alerta, animated: true, completion: nil)
            }
        }else{
            if data.CodigoWS != nil {
                if data.CodigoWS == "99"
                {
                    let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "Su sesión se ha iniciado en otro dispositivo. Esta sesión se cerrará.", preferredStyle: .alert)
                    let actionVersionGO = UIAlertAction.init(title: "Terminar", style: .default, handler: { (action: UIAlertAction!) in
                        
                        let nav = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
                        self.navigationController?.pushViewController(nav, animated: true)
                    })
                    alertaVersion.addAction(actionVersionGO)
                    self.present(alertaVersion, animated: true, completion: nil)
                }
                else
                {
                    iniMensajeError(codigo: (data.CodigoWS!))
                }
            }
            else
            {
                let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                self.present(alerta, animated: true, completion: nil)
            }
        }
    }

    @IBAction func accionContinuar(_ sender: Any) {
        if validar() {
            iniCambiarClave()
        }
    }
    
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
