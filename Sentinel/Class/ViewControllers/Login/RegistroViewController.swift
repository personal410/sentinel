//
//  RegistroViewController.swift
//  Sentinel
//
//  Created by Daniel on 6/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class RegistroViewController: ParentViewController, UITextFieldDelegate {
    @IBOutlet weak var dniTXT: UITextField!
    @IBOutlet weak var correoTXT: UITextField!
    @IBOutlet weak var digitoTXT: UITextField!
    @IBOutlet weak var viewVW: UIView!
    @IBOutlet weak var continuarBT: UIButton!
    @IBOutlet weak var textView:UITextView!
    
    var dni = ""
    var mail = ""
    var digito = ""
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.gestureScreen()
        self.estilos()
        self.inicializarVariables()
        
        let str = NSMutableAttributedString(string: "Aquí podrás encontrar información sobre las condiciones para el tratamiento de tus datos personales términos y condiciones, y política de privacidad.")
        str.addAttribute(NSAttributedStringKey.link, value: "https://misentinel.sentinelperu.com/misentinel/Terminos_y_Condiciones_de_Acceso_y_uso.pdf", range: NSRange(location: 100, length: 22))
        str.addAttribute(NSAttributedStringKey.link, value: "https://www.sentinelperu.com/politica_de_privacidad.pdf", range: NSRange(location: 126, length: 22))
        textView.attributedText = str
    }
    func inicializarVariables(){
        self.dniTXT.delegate = self
        self.digitoTXT.delegate = self
    }
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func estilos(){
        self.viewVW.layer.cornerRadius = 10
        self.continuarBT.layer.cornerRadius = 5
    }
    
    func validar() -> Bool{
        if self.dniTXT.text == nil || self.dniTXT.text == "" {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe ingresar el DNI.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        if self.dniTXT.text?.count != 8 {
            let alerta = UIAlertController(title: "ALERTA", message: "El DNI debe tener 8 caracteres.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        if self.correoTXT.text == nil || self.correoTXT.text == "" {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe ingresar el correo.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        
        let correo : String = self.correoTXT.text!
        if correo.isValidEmail() == false {
            let alerta = UIAlertController(title: "ALERTA", message: "El correo ingresado es inválido.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        if self.digitoTXT.text == nil || self.digitoTXT.text == "" {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe ingresar su dígito verificador.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        return true
    }
    @IBAction func dniEditing(_ sender: Any) {
        
    }
    @IBAction func digitEditing(_ sender: Any) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.dniTXT {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 8
        }
        else if textField == self.digitoTXT {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 1
        }
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= 25
    }
    
    func iniRegistroUno(){
        self.showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endRegistroUno(_:)), name: NSNotification.Name("endRegistroUno"), object: nil)
        
        self.dni = self.dniTXT.text!
        self.mail = self.correoTXT.text!
        self.digito = self.digitoTXT.text!
        
        let parametros = [
            "Usuario" : self.dni,
            "Mail" : self.mail,
            "DigitoVer" : self.digito
            ] as [String : Any]
        OriginData.sharedInstance.registroUno(notification: "endRegistroUno", parametros: parametros)
    }
    
    @objc func endRegistroUno(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if validaIndicador == 0 {
        let data = notification.object as? UserClass
        if data?.CodigoWS == "0"{
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "RegistroDosVC") as! RegistroDosViewController
            nav.dniUsuario = self.dni
            nav.correoUsuario = self.mail
            self.navigationController?.pushViewController(nav, animated: true)
        }
        else
        {
            if data?.CodigoWS != nil {
                if data?.CodigoWS == "99"
                {
                    let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "Su sesión se ha iniciado en otro dispositivo. Esta sesión se cerrará.", preferredStyle: .alert)
                    let actionVersionGO = UIAlertAction.init(title: "Terminar", style: .default, handler: { (action: UIAlertAction!) in
                        
                        let nav = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
                        self.navigationController?.pushViewController(nav, animated: true)
                    })
                    alertaVersion.addAction(actionVersionGO)
                    self.present(alertaVersion, animated: true, completion: nil)
                }
                else
                {
                    let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: data?.MensajeRespuesta, preferredStyle: .alert)
                    let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                    alertaVersion.addAction(actionVersion)
                    self.present(alertaVersion, animated: true, completion: nil)
                }
            }
        }
        }
    }
    
    @IBAction func accionContinuar(_ sender: Any) {
        if validar(){
            self.iniRegistroUno()
        }
    }
    @IBAction func atrasAccion(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func prueba(_ sender: Any) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "RegistroDosVC") as! RegistroDosViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
}

extension String {
    func isValidEmail() -> Bool {
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}
