//
//  RegistroTerminosViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 1/29/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import UIKit
class RegistroTerminosViewController:ParentViewController {
    @IBOutlet weak var checkUnoBT: UIButton!
    @IBOutlet weak var checkDosBT: UIButton!
    @IBOutlet weak var aceptoBT: UIButton!
    @IBOutlet weak var noAceptoBT: UIButton!
    
    var dniUsuario:String = ""
    var claveUsuario:String = ""
    var celular:String!
    var correo:String!
    var valorUno = 0
    var valorDos = 0
    var didAccept = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gestureScreen()
        self.estilos()
    }
    func estilos(){
        self.aceptoBT.layer.cornerRadius = 5
        self.noAceptoBT.layer.cornerRadius = 5
    }
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    func validar() -> Bool{
        if self.valorUno == 0 {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe aceptar los términos y condiciones.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func iniRegistroCuatro(){
        showActivityIndicator()
        let userData = UserGlobalData.sharedInstance.userGlobal!
        let parametros:[String:Any] = ["Usuario" : userData.user!, "Mail" : didAccept ? correo : "", "Celular" : didAccept ? celular : "", "AceptaTyC" : didAccept ? "S" : "N", "OtoCon" : valorDos == 1 ? "S" : "N", "SesionId" : userData.SesionId!]
        hideActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endRegistroCuatro(_:)), name: NSNotification.Name("endRegistroContrato"), object: nil)
        OriginData.sharedInstance.registroContrato(notification: "endRegistroContrato", parametros: parametros)
    }
    
    @objc func endRegistroCuatro(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        guard let data = notification.object as? UserClass else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if let codigoWs = data.CodigoWS {
            if codigoWs == "0" {
                if didAccept {
                    let nav = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarVC")
                    self.navigationController?.present(nav, animated: true, completion: nil)
                }else{
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }else if codigoWs == "99" {
                let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "Su sesión se ha iniciado en otro dispositivo. Esta sesión se cerrará.", preferredStyle: .alert)
                let actionVersionGO = UIAlertAction.init(title: "Terminar", style: .default, handler: { (action: UIAlertAction!) in
                    
                    let nav = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
                    self.navigationController?.pushViewController(nav, animated: true)
                })
                alertaVersion.addAction(actionVersionGO)
                self.present(alertaVersion, animated: true, completion: nil)
            }else{
                iniMensajeError(codigo: codigoWs)
            }
        }else{
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
        }
    }
    @IBAction func accionCheckPrimer(_ sender: Any) {
        if valorUno == 0 {
            valorUno = 1
            self.checkUnoBT.setImage(UIImage.init(named: "check_on"), for: .normal)
        }else{
            valorUno = 0
            self.checkUnoBT.setImage(UIImage.init(named: "check_off"), for: .normal)
        }
    }
    @IBAction func accionCheckSegundo(_ sender: Any) {
        if valorDos == 0 {
            valorDos = 1
            self.checkDosBT.setImage(UIImage.init(named: "check_on"), for: .normal)
        }else{
            valorDos = 0
            self.checkDosBT.setImage(UIImage.init(named: "check_off"), for: .normal)
        }
    }
    @IBAction func accionAcepto(_ sender: Any) {
        if validar() {
            didAccept = true
            iniRegistroCuatro()
        }
    }
    @IBAction func accionNoAcepto(_ sender: Any){
        self.didAccept = false
        iniRegistroCuatro()
    }
    @IBAction func atrasAccion(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
