//
//  RecuperarContrasenaViewController.swift
//  Sentinel
//
//  Created by Daniel on 3/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class RecuperarContrasenaViewController:ParentViewController, UITextFieldDelegate {
    @IBOutlet weak var usuarioTXT: UITextField!
    @IBOutlet weak var correoTXT: UITextField!
    @IBOutlet weak var continuarBT: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gestureScreen()
        self.estilos()
        self.inicializarVariables()
    }
    
    func inicializarVariables(){
        self.usuarioTXT.delegate = self
        self.correoTXT.delegate = self
    }
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func estilos(){
        self.continuarBT.layer.cornerRadius = 5
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.usuarioTXT {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 20
        }
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= 45
    }
    
    func validar() -> Bool{
        if self.usuarioTXT.text == "" {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe ingresar el DNI.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        if self.correoTXT.text == nil || self.correoTXT.text == "" {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe ingresar el correo.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        
        let correo : String = self.correoTXT.text!
        
        if correo.isValidEmail() == false {
            let alerta = UIAlertController(title: "ALERTA", message: "El correo ingresado es inválido.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        
        return true
    }
    
    func validarContrasena(){
        if self.usuarioTXT.text != nil && self.correoTXT.text?.count != nil{
            if self.usuarioTXT.text?.count == 8 && self.correoTXT.text?.count != 0  {
                self.showActivityIndicator()
                NotificationCenter.default.addObserver(self, selector: #selector(self.endOlvideClave(_:)), name: NSNotification.Name("endOlvideClave"), object: nil)
                let usua = self.usuarioTXT.text!
                let mail = self.correoTXT.text!
                let parametros = [
                    "UseCod" : usua,
                    "UseMail" : mail
                    ] as [String : Any]
                OriginData.sharedInstance.validarContrasena(notification: "endOlvideClave", parametros: parametros)
            }
        }
        else
        {
            let alerta = UIAlertController(title: "Recuperar Contraseña", message: "Debe ingresar los campos correctos.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
        }
    }
    
    @objc func endOlvideClave(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        if validaIndicador == 0 {
            let data = notification.object as! UserClass
            if data.CodigoWS == "0"{
                let alerta = UIAlertController.init(title: "Recuperar Contraseña", message: "En breve le llegará un correo con un link para cambiar su contraseña", preferredStyle: .alert)
                
                let actionVersionGO = UIAlertAction.init(title: "Aceptar", style: .default, handler: { (action: UIAlertAction!) in
                    let nav = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
                    self.navigationController?.pushViewController(nav, animated: true)
                })
                alerta.addAction(actionVersionGO)
                self.present(alerta, animated: true, completion: nil)
            }
            else
            {
                if data.CodigoWS != nil {
                    if data.CodigoWS == "99"
                    {
                        let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "Su sesión se ha iniciado en otro dispositivo. Esta sesión se cerrará.", preferredStyle: .alert)
                        let actionVersionGO = UIAlertAction.init(title: "Terminar", style: .default, handler: { (action: UIAlertAction!) in
                            
                            let nav = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
                            self.navigationController?.pushViewController(nav, animated: true)
                        })
                        alertaVersion.addAction(actionVersionGO)
                        self.present(alertaVersion, animated: true, completion: nil)
                    }
                    else
                    {
                        iniMensajeError(codigo: (data.CodigoWS!))
                    }
                }
                else
                {
                    let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                    alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                    self.present(alerta, animated: true, completion: nil)
                }
            }
        }
    }
    
    
    @IBAction func atrasAccion(_ sender: Any) {
//        let nav = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
//        self.navigationController?.pushViewController(nav, animated: true)
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func continuarAccion(_ sender: Any) {
        
        if validar() {
            self.validarContrasena()
        }
    }
}






