//
//  RegistroTresViewController.swift
//  Sentinel
//
//  Created by Daniel on 31/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class RegistroTresViewController: ParentViewController {

    @IBOutlet weak var celularTXT: UITextField!
    @IBOutlet weak var validarBT: UIButton!
    @IBOutlet weak var codigoTXT: UITextField!
    @IBOutlet weak var continuarBT: UIButton!
    
    var celularUsuario : String = ""
    var dniUsuario : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gestureScreen()
        self.estilos()
        self.inicializarVariables()
    }
    
    func estilos(){
        self.validarBT.layer.cornerRadius = 5
        self.continuarBT.layer.cornerRadius = 5
    }
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    func inicializarVariables(){
        if celularUsuario != "" {
            self.celularTXT.text = celularUsuario
        }
    }

    func validar() -> Bool{
        if self.codigoTXT.text == nil || self.codigoTXT.text == "" {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe ingresar el código que se le ha enviado.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func iniRegistroTres(){
        showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endRegistroTres(_:)), name: NSNotification.Name("endRegistroTres"), object: nil)
        let clave = self.codigoTXT.text!
        let parametros = [
            "Usuario" : self.dniUsuario,
            "ClaveSMS" : clave
            ] as [String : Any]
        OriginData.sharedInstance.registroTres(notification: "endRegistroTres", parametros: parametros)
    }
    
    @objc func endRegistroTres(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if validaIndicador == 0 {
        let data = notification.object as? UserClass
        if data?.CodigoWS == "0"{
            let nav = self.storyboard?.instantiateViewController(withIdentifier: "RegistroCuatroVC") as! RegistroCuatroViewController
            nav.dniUsuario = self.dniUsuario
            self.navigationController?.pushViewController(nav, animated: true)
        }
        else
        {
            if data?.CodigoWS != nil {
                if data?.CodigoWS == "99"
                {
                    let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "Su sesión se ha iniciado en otro dispositivo. Esta sesión se cerrará.", preferredStyle: .alert)
                    let actionVersionGO = UIAlertAction.init(title: "Terminar", style: .default, handler: { (action: UIAlertAction!) in
                        
                        let nav = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
                        self.navigationController?.pushViewController(nav, animated: true)
                    })
                    alertaVersion.addAction(actionVersionGO)
                    self.present(alertaVersion, animated: true, completion: nil)
                }
                else
                {
                    iniMensajeError(codigo: (data?.CodigoWS!)!)
                }
            }
            else
            {
                let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                self.present(alerta, animated: true, completion: nil)
            }
        }
        }
    }
    
    @IBAction func accionContinuar(_ sender: Any) {
        if validar() {
            self.iniRegistroTres()
        }
    }
    
    @IBAction func atrasAccion(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        self.navigationController?.popViewController(animated: false)
        self.navigationController?.popViewController(animated: true)
    }
}
