//
//  RegistroDosViewController.swift
//  Sentinel
//
//  Created by Daniel on 31/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class RegistroDosViewController:ParentViewController, UITextFieldDelegate {
    @IBOutlet weak var celularTXT:UITextField!
    @IBOutlet weak var btnSMS:UIButton!
    @IBOutlet weak var btnWhatsapp:UIButton!
    @IBOutlet weak var vIngresarCodigo:UIView!
    @IBOutlet weak var tfCodigo:UITextField!
    
    var dniUsuario = ""
    var correoUsuario = ""
    var celularUsuario = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.gestureScreen()
    }
    
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    @objc func myviewTapped(_ sender:UITapGestureRecognizer){
        self.view.endEditing(true)
    }
    
    func textField(_ textField:UITextField, shouldChangeCharactersIn range:NSRange, replacementString string:String) -> Bool {
        if textField.tag == 0 {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 9
        }else{
            return true
        }
    }
    
    func validar() -> Bool{
        if self.celularTXT.text == "" {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe ingresar el número de celular.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        if self.celularTXT.text?.count != 9 {
            let alerta = UIAlertController(title: "ALERTA", message: "El número de celular debe tener 9 caracteres.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func iniRegistroDos(){
        showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endRegistroDos(_:)), name: NSNotification.Name("endRegistroDos"), object: nil)
        celularUsuario = celularTXT.text!
        let parametros = [ "Usuario" : dniUsuario, "Celular" : celularUsuario, "Mail" : correoUsuario] as [String : Any]
        OriginData.sharedInstance.registroDos(notification: "endRegistroDos", parametros: parametros)
    }
    
    @objc func endRegistroDos(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard notification.object != nil  else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        let data = notification.object as? UserClass
        if data?.CodigoWS == "0"{
            self.habilitarModoCodigo(ocultando: btnSMS)
        }
        else
        {
            if data?.CodigoWS != nil {
                if data?.CodigoWS == "99"
                {
                    let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "Su sesión se ha iniciado en otro dispositivo. Esta sesión se cerrará.", preferredStyle: .alert)
                    let actionVersionGO = UIAlertAction.init(title: "Terminar", style: .default, handler: { (action: UIAlertAction!) in
                        
                        let nav = UIStoryboard.init(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
                        self.navigationController?.pushViewController(nav, animated: true)
                    })
                    alertaVersion.addAction(actionVersionGO)
                    self.present(alertaVersion, animated: true, completion: nil)
                }
                else
                {
                    iniMensajeError(codigo: (data?.CodigoWS!)!)
                }
            }
            else
            {
                let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                self.present(alerta, animated: true, completion: nil)
            }
        }
    }
    //MARK: - Action
    @IBAction func atrasAccion(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func accionEnviarCodigo(_ sender:UIButton){
        if validar() {
            if sender.tag == 0 {
                iniRegistroDos()
            }else{
                celularUsuario = celularTXT.text!
                let dicParams = ["Usuario": dniUsuario, "Celular" : celularUsuario, "Mail" : correoUsuario]
                showActivityIndicator()
                ServiceConnector.connectToUrl("\(PATHS.PATHSENTINEL)RWS_MSPaso2AppWsp", params: dicParams){(r, e) in
                    self.hideActivityIndicator()
                    if e == nil, let dicResult = r as? [String: Any], let codigoWs = dicResult["CodigoWS"] as? String {
                        if codigoWs == "0" {
                            self.habilitarModoCodigo(ocultando: self.btnWhatsapp)
                        }else if codigoWs == "99" {
                            let alertaVersion = UIAlertController(title: PATHS.SENTINEL, message: "Su sesión se ha iniciado en otro dispositivo. Esta sesión se cerrará.", preferredStyle: .alert)
                            let actionVersionGO = UIAlertAction(title: "Terminar", style: .default, handler: { (action: UIAlertAction!) in
                                self.navigationController?.popToRootViewController(animated: true)
                            })
                            alertaVersion.addAction(actionVersionGO)
                            self.present(alertaVersion, animated: true, completion: nil)
                        }else{
                            self.iniMensajeError(codigo: codigoWs)
                        }
                    }else{
                        let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: .alert)
                        alerta.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil));
                        self.present(alerta, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    @IBAction func accionContinuar(){
        if tfCodigo.text!.isEmpty {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe ingresar el código que se le ha enviado.", preferredStyle: .alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil));
            self.present(alerta, animated: true, completion: nil)
        }else{
            let parametros = ["Usuario": dniUsuario, "ClaveSMS": tfCodigo.text!]
            showActivityIndicator()
            ServiceConnector.connectToUrl("\(PATHS.PATHSENTINEL)RWS_MSPaso3App", params: parametros) { (r, e) in
                self.hideActivityIndicator()
                if e == nil, let dicResult = r as? [String: Any], let codigoWs = dicResult["CodigoWS"] as? String {
                    if codigoWs == "0" {
                        let nav = self.storyboard?.instantiateViewController(withIdentifier: "RegistroCuatroVC") as! RegistroCuatroViewController
                        nav.dniUsuario = self.dniUsuario
                        self.navigationController?.pushViewController(nav, animated: true)
                    }else if codigoWs == "99" {
                        let alertaVersion = UIAlertController(title: PATHS.SENTINEL, message: "Su sesión se ha iniciado en otro dispositivo. Esta sesión se cerrará.", preferredStyle: .alert)
                        let actionVersionGO = UIAlertAction(title: "Terminar", style: .default, handler: { (action: UIAlertAction!) in
                            self.navigationController?.popToRootViewController(animated: true)
                        })
                        alertaVersion.addAction(actionVersionGO)
                        self.present(alertaVersion, animated: true, completion: nil)
                    }else{
                        self.iniMensajeError(codigo: codigoWs)
                    }
                }else{
                    let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: .alert)
                    alerta.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil));
                    self.present(alerta, animated: true, completion: nil)
                }
            }
        }
    }
    //MARK: - Auxiliar
    func habilitarModoCodigo(ocultando btn:UIButton){
        celularTXT.isEnabled = false
        btn.isEnabled = false
        vIngresarCodigo.isHidden = false
        
    }
}
