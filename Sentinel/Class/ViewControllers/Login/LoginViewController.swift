//
//  LoginViewController.swift
//  Sentinel
//
//  Created by Daniel on 3/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import Alamofire
import CoreTelephony
import AlamofireImage
import FirebaseCore
import FirebaseInstanceID
import FirebaseAnalytics
struct defaultsKeys {
    static var USER_NAME_KEY = ""
    static var USER_PASSWORD_KEY = ""
    static let DEVICE_TOKEN_KEY = "deviceTokenKey"
}
class LoginViewController:ParentViewController, UITextFieldDelegate {
    @IBOutlet weak var usuariotxt:UITextField!
    @IBOutlet weak var contraseniatxt:UITextField!
    @IBOutlet weak var olvidoBT:UIButton!
    @IBOutlet weak var usuNuevoBT:UIButton!
    @IBOutlet weak var seeIMG:UIButton!
    @IBOutlet weak var vAlertaJailbreak:UIView!
    var user:UserClass?
    var tercero:TerceroClass?
    var usuario:String!
    var pwd:String!
    var activityIndicator:UIActivityIndicatorView!
    var mainstoryBoard = UIStoryboard()
    var cierreSesion = ""
    var cargaTerminosCondiciones = 0
    var usuaTerminos = ""
    var pwdTerminos = ""
    var seguridad = true
    var fbToken = ""
    var showNotificationType = 0
    //MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
        self.clear(cache: true, cookies: true)
        self.gestureScreen()
        self.estilos()
        self.inicializarVariables()
        InstanceID.instanceID().instanceID { (result, error) in
            if error != nil {
                self.fbToken = "error"
            }else if let result = result {
                self.fbToken = result.token
            }
        }
    }
    override func viewWillAppear(_ animated:Bool){
        if isJailBroken() {
            vAlertaJailbreak.isHidden = false
        }else{
            vAlertaJailbreak.isHidden = true
            if cargaTerminosCondiciones == 1 {
                cargaTerminosCondiciones = 0
                iniLoginWithTerminosCondiciones(usuarioTerminos: usuaTerminos, claveTerminos: pwdTerminos)
                showNotificationType = 0
            }else{
                validarLogin()
            }
        }
    }
    //MARK: - ViewCont
    func estilos(){
        self.seeIMG.alpha = 0.25
    }
    func inicializarVariables(){
        mainstoryBoard = UIStoryboard.init(name: "Main", bundle: nil)
        self.usuariotxt.delegate = self
        self.contraseniatxt.delegate = self
    }
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    @objc func myviewTapped(_ sender: UITapGestureRecognizer){
        self.view.endEditing(true)
    }
    func validar() -> Bool{
        if self.usuariotxt.text == nil || self.usuariotxt.text == "" {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe ingresar el DNI de Usuario.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        if self.contraseniatxt.text == nil || self.contraseniatxt.text == "" {
            let alerta = UIAlertController(title: "ALERTA", message: "Debe ingresar la contraseña.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return false
        }
        return true
    }
    func validarLogin(){
        defaultsKeys.USER_NAME_KEY = UserDefaults.standard.string(forKey: "userName")!
        defaultsKeys.USER_PASSWORD_KEY = UserDefaults.standard.string(forKey: "password") ?? ""
        let userName = defaultsKeys.USER_NAME_KEY
        let userPassword = defaultsKeys.USER_PASSWORD_KEY
        if userName != "" && userPassword != "" {
            usuariotxt.text = userName
            contraseniatxt.text = userPassword
            self.iniLoginWithData()
        }else{
            usuariotxt.text = ""
            contraseniatxt.text = ""
            showNotificationType = 0
        }
    }
    func clear(cache: Bool, cookies: Bool) {
        UserGlobalData.sharedInstance.userGlobal = UserClass(userbean: [:])
        UserGlobalData.sharedInstance.misServiciosGlobal = MisServiciosClass(serviciobean: [:])
        UserGlobalData.sharedInstance.alertaGlobal = AlertaClass(userbean: [:])
        UserGlobalData.sharedInstance.terceroGlobal = TerceroClass(userbean: [:])
        if cache { clearCache() }
        if cookies { clearCookies() }
    }
    fileprivate func clearCache() {
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
    }
    fileprivate func clearCookies() {
        let cookieStorage = HTTPCookieStorage.shared
        guard let cookies = cookieStorage.cookies else { return }
        for cookie in cookies {
            cookieStorage.deleteCookie(cookie)
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.usuariotxt {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 20
        }
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= 18
    }
    func iniLoginWithTerminosCondiciones(usuarioTerminos : String, claveTerminos:String){
        let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        let carrier = self.getCarrier()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endLoginWithData(_:)), name: NSNotification.Name("endLoginWithData"), object: nil)
        self.showActivityIndicator()
        usuario = usuarioTerminos
        pwd = claveTerminos
        let parametros:[String : Any] = ["Usuario" : usuarioTerminos, "Password" : claveTerminos, "ValidaCookie" : "N", "Plataforma" : "IOS", "IdOrigen" : PATHS.APP_ID, "VersionId" : version!, "VersOpc" : "", "Operador" : carrier]
        OriginData.sharedInstance.validarLogin(notification: "endLoginWithData", parametros: parametros)
    }
    func iniLoginWithData(){
        if contraseniatxt.text?.count != 0 && usuariotxt.text?.count != 0 {
            let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
            let carrier = self.getCarrier()
            usuario = self.usuariotxt.text
            pwd = self.contraseniatxt.text
            NotificationCenter.default.addObserver(self, selector: #selector(self.endLoginWithData(_:)), name: NSNotification.Name("endLoginWithData"), object: nil)
            showActivityIndicator()
            let usua : String = self.usuario
            let pass : String = self.pwd
            let parametros:[String : Any] = ["Usuario" : usua, "Password" : pass, "ValidaCookie" : "N", "Plataforma" : "IOS", "IdOrigen" : PATHS.APP_ID, "VersionId" : version!, "VersOpc" : "", "Operador" : carrier]
            OriginData.sharedInstance.validarLogin(notification: "endLoginWithData", parametros: parametros)
        }else{
            showNotificationType = 0
        }
    }
    @objc func endLoginWithData(_ notification:NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard let data = notification.object as? UserClass else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if data.CodigoWS == "0" {
            let evento = EventosFbUtilsViewController()
            evento.logLoginEvent()
            var tipoDocumento = ""
            if (data.TDocusu == "D") {
                tipoDocumento = "DNI"
            }else{
                tipoDocumento = "C/E"
            }
            data.tipoDocumento = tipoDocumento
            user = data
            var userx = ""
            if (self.usuariotxt.text != ""){
                userx = self.usuariotxt.text!
            }else{
                userx = usuaTerminos
            }
            user?.ApellidoPaternoConsultado = data.ApePat
            user?.ApellidoMaternoConsultado = data.ApeMat
            user?.NombreConsultado = data.Nombres
            user?.Mail = data.MailUsuario
            user?.Celular = ""
            user?.NumDocumento = userx
            user?.user = userx
            let parametros = ["Usuario": userx, "SesionId": data.SesionId!, "CCVisOrigenId": PATHS.APP_ID] as [String : Any]
            showActivityIndicator()
            NotificationCenter.default.addObserver(self, selector: #selector(self.endVersionApp(_:)), name: NSNotification.Name("endVersionApp"), object: nil)
            OriginData.sharedInstance.getVersion(notification: "endVersionApp", parametros: parametros)
        }else{
            if data.CodigoWS != nil {
                if data.CodigoWS == "99" {
                    let alertaVersion = UIAlertController(title: PATHS.SENTINEL, message: "Su sesión se ha iniciado en otro dispositivo. Esta sesión se cerrará.", preferredStyle: .alert)
                    alertaVersion.addAction(UIAlertAction(title: "Terminar", style: .default, handler: nil))
                    self.present(alertaVersion, animated: true, completion: nil)
                }else{
                    iniMensajeError(codigo: (data.CodigoWS!))
                }
            }else{
                let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                self.present(alerta, animated: true, completion: nil)
            }
        }
    }
    @objc func endVersionApp(_ notification:NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard notification.object != nil else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        let data = notification.object as! UserClass
        if data.CodigoWS == "0" {
            let versionApp = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
            let arrVersionApp = versionApp.split(separator: ".")
            let versionServer = data.CCOVerId!
            let arrVersionServer = versionServer.split(separator: ".")
            var esVersionPermitida = true
            for i in 0 ... 2 {
                if arrVersionApp.count > i && arrVersionServer.count > i {
                    let numVersionApp = (arrVersionApp[i] as NSString).integerValue
                    let numVersionServer = (arrVersionServer[i] as NSString).integerValue
                    if numVersionApp < numVersionServer {
                        esVersionPermitida = false
                    }
                }
            }
            if esVersionPermitida {
                let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                let url = NSURL(string: (user?.Foto)!)! as URL
                NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarFoto(_:)), name: NSNotification.Name("endCargarFoto"), object: nil)
                showActivityIndicator()
                Alamofire.request(url).responseImage { response in
                    switch response.result {
                        case .success:
                            if let image = response.result.value {
                                _ = self.saveImageMain(image: image)
                                self.saveImageInDisk(image: image, fileName: "sent_miimagen", extensionPath: "png", directoryPath:documentsDirectory )
                                self.user?.FotoRutaInterna = "\(documentsDirectory)"
                                UserGlobalData.sharedInstance.userGlobal = self.user
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFoto"), object: nil)
                            }
                            break
                        case .failure(_):
                            let image : UIImage = UIImage.init(named: "CNIGenImaHom")!
                            _ = self.saveImageMain(image: image)
                            UserGlobalData.sharedInstance.userGlobal = self.user
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endCargarFoto"), object: nil)
                            break
                    }
                }
            }else{
                let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "Existe una nueva actualización de la aplicación. ¿Desea actualizar ahora?", preferredStyle: .alert)
                let actionVersion = UIAlertAction.init(title: "Terminar", style: .cancel, handler: nil)
                let actionVersionGO = UIAlertAction.init(title: "Actualizar", style: .default, handler: { (action: UIAlertAction!) in
                    
                    let url: URL = NSURL(string: "https://itunes.apple.com/pe/app/sentinel/id919360397?mt=8")! as URL
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                })
                alertaVersion.addAction(actionVersion)
                alertaVersion.addAction(actionVersionGO)
                self.present(alertaVersion, animated: true, completion: nil)
            }
        }else{
            if data.CodigoWS != nil {
                if data.CodigoWS == "99" {
                    let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "Su sesión se ha iniciado en otro dispositivo. Esta sesión se cerrará.", preferredStyle: .alert)
                    alertaVersion.addAction(UIAlertAction(title: "Terminar", style: .default, handler: nil))
                    self.present(alertaVersion, animated: true, completion: nil)
                }else{
                    iniMensajeError(codigo: (data.CodigoWS!))
                }
            }else{
                let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                self.present(alerta, animated: true, completion: nil)
            }
        }
    }
    @objc func endCargarFoto(_ notification:NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        showActivityIndicator()
        let userData = UserGlobalData.sharedInstance.userGlobal
        let sesion = (userData?.SesionId)!
        let tipoDoc = (userData?.TDocusu)!
        let parametros = ["Usuario": usuario, "SesionId": sesion, "TDocUsuario" : tipoDoc] as [String : Any]
        NotificationCenter.default.addObserver(self, selector: #selector(self.endConsultaAlertas(_:)), name: NSNotification.Name("endConsultaAlertas"), object: nil)
        OriginData.sharedInstance.endConsultaAlertas(notification: "endConsultaAlertas", parametros: parametros)
    }
    @objc func endConsultaAlertas(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard notification.object != nil  else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        let data = notification.object as! AlertaClass
        if (data.CodigoWS != nil) && (data.CodigoWS != "0") {
            let usua = (user?.NumDocumento)!
            let sesion = (user?.SesionId)!
            let servicio = (user?.NroSerCT)!
            let parametros = ["Usuario": usua, "SesionId": sesion, "Servicio": servicio] as [String : Any]
            showActivityIndicator()
            NotificationCenter.default.addObserver(self, selector: #selector(self.endConsultaDatosServicioAlerta(_:)), name: NSNotification.Name("endConsultaDatosServicioAlerta"), object: nil)
            OriginData.sharedInstance.consultaDatosServicio(notification: "endConsultaDatosServicioAlerta", parametros: parametros)
        }else if data.CodigoWS == "0" {
            self.setToken()
        }else if data.CodigoWS == "99" {
            let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "Su sesión se ha iniciado en otro dispositivo. Esta sesión se cerrará.", preferredStyle: .alert)
            alertaVersion.addAction(UIAlertAction(title: "Terminar", style: .default, handler: nil))
            self.present(alertaVersion, animated: true, completion: nil)
        }else{
            iniMensajeError(codigo: data.CodigoWS!)
        }
    }
    @objc func endConsultaDatosServicioAlerta(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard notification.object != nil  else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        let terceroLocal = notification.object as! TerceroClass
        UserGlobalData.sharedInstance.terceroGlobal = terceroLocal
        tercero = UserGlobalData.sharedInstance.terceroGlobal
        tercero?.AFSerGraDisAlerta = terceroLocal.AFSerGraDis
        tercero?.AFSerCPTDisAlerta = terceroLocal.AFSerCPTDis
        UserGlobalData.sharedInstance.terceroGlobal = tercero
        if terceroLocal.CodigoWS == "0" {
            setToken()
        }else{
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
        }
    }
    func setToken(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        let usua = (userData?.user)!
        let sesion = (userData?.SesionId)!
        let parametros:[String:Any] = ["Usuario": usua, "SesionId": sesion, "UseTokenApp": fbToken, "OrigenId": PATHS.APP_ID]
        showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endTokenUsuario(_:)), name: NSNotification.Name("endTokenUsuario"), object: nil)
        OriginData.sharedInstance.setTokenUsuario(notification: "endTokenUsuario", parametros: parametros)
    }
    @objc func endTokenUsuario(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard notification.object != nil  else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        let result = notification.object as! NotificationClass
        if result.CodigoWS == "0"{
            cargarMisServicios()
        }else{
            if result.CodigoWS != nil {
                if result.CodigoWS == "99" {
                    let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "Su sesión se ha iniciado en otro dispositivo. Esta sesión se cerrará.", preferredStyle: .alert)
                    alertaVersion.addAction(UIAlertAction(title: "Terminar", style: .default, handler: nil))
                    self.present(alertaVersion, animated: true, completion: nil)
                }else{
                    iniMensajeError(codigo: (result.CodigoWS!))
                }
            }else{
                let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                self.present(alerta, animated: true, completion: nil)
            }
        }
    }
    func cargarMisServicios(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        let usua : String = (userData?.user)!
        let sesion : String = (userData?.SesionId)!
        let tDoc : String = (userData?.TDocusu)!
        let parametros = ["Usuario": usua, "SesionId": sesion, "TDocUsu": tDoc] as [String : Any]
        showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endCargarMisServicios(_:)), name: NSNotification.Name("endCargarMisServicios"), object: nil)
        OriginData.sharedInstance.listadoMisServicios(notification: "endCargarMisServicios", parametros: parametros)
    }
    @objc func endCargarMisServicios(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard notification.object != nil  else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        let data = notification.object as? MisServiciosClass
        if data?.CodigoWS == "0"{
            UserGlobalData.sharedInstance.misServiciosGlobal = data
            let serviciosGlobal = UserGlobalData.sharedInstance.misServiciosGlobal
            let arrayServicios = data?.ServiciosUsuario
            var arrayMisServicios:[MiObejetoServicio] = []
            for i in 0 ... (arrayServicios?.count)! - 1 {
                let objServicio: NSDictionary = arrayServicios![i] as! NSDictionary
                let objMiServicio : MiObejetoServicio = MiObejetoServicio()
                let numServ = objServicio["NroServicio"] as! String
                if numServ != "459632"{
                    objMiServicio.NroServicio = objServicio["NroServicio"] as! String
                    objMiServicio.DscServicio = objServicio["DscServicio"] as! String
                    objMiServicio.FlgCoop = objServicio["FlgCoop"] as! String
                    objMiServicio.TipServ = objServicio["TipServ"] as! String
                    objMiServicio.FlgSabio = objServicio["FlgSabio"] as! String
                    arrayMisServicios.append(objMiServicio)
                }
            }
            serviciosGlobal?.CodigoWS = data?.CodigoWS
            serviciosGlobal?.ServiciosTipo = arrayMisServicios
            UserGlobalData.sharedInstance.misServiciosGlobal = serviciosGlobal
            loginLoad()
        }else{
            if data?.CodigoWS != nil {
                if data?.CodigoWS == "99" {
                    let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "Su sesión se ha iniciado en otro dispositivo. Esta sesión se cerrará.", preferredStyle: .alert)
                    alertaVersion.addAction(UIAlertAction(title: "Terminar", style: .default, handler: nil))
                    self.present(alertaVersion, animated: true, completion: nil)
                }else{
                    iniMensajeError(codigo: (data?.CodigoWS!)!)
                }
            }else{
                let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                self.present(alerta, animated: true, completion: nil)
            }
        }
    }
    func loginLoad(){
        hideActivityIndicator()
        user = UserGlobalData.sharedInstance.userGlobal
        switch user?.CodigoWS {
            case "0":
                if user?.FlgClave == "S"{
                    if (user?.FlgContratoCG != "S"){
                        self.performSegue(withIdentifier: "showRegCelCorreo", sender: nil)
                    }else{
                        cierreSesion = "N"
                        UserDefaults.standard.set(self.usuario, forKey: "userName")
                        UserDefaults.standard.set(self.pwd, forKey: "password")
                        UserDefaults.standard.synchronize()
                        defaultsKeys.USER_NAME_KEY = self.usuario
                        defaultsKeys.USER_PASSWORD_KEY = self.pwd
                        let title = "LOGUEOEXITOSO"
                        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                            AnalyticsParameterItemID: "id-\(title)-ios",
                            AnalyticsParameterItemName: title,
                            AnalyticsParameterContentType: "cont"
                            ])
                        UserGlobalData.sharedInstance.userGlobal.sesionActive = true
                        PATHS.sesionActive = true
                        //UserGlobalData.sharedInstance.userGlobal.EsPremium = "S"//TODO: VIC
                        let nav = mainstoryBoard.instantiateViewController(withIdentifier: "TabBarVC") as! MainTabBarViewController
                        nav.showNotificationType = showNotificationType
                        showNotificationType = 0
                        contraseniatxt.text = ""
                        self.navigationController?.pushViewController(nav, animated: true)
                    }
                }else if user?.FlgClave == "N"{
                    cierreSesion = "N"
                    let nav = storyboard!.instantiateViewController(withIdentifier: "CambiarClaveVC") as! CambiarClaveViewController
                    nav.irMenuPrincipal = true
                    navigationController?.pushViewController(nav, animated: true)
                }
                break
            case "2":
                cierreSesion = "N"
                break
            default:
                break
        }
    }
    func saveImageInDisk(image:UIImage, fileName:String, extensionPath:String, directoryPath:URL){
        let fileURL = directoryPath.appendingPathComponent(fileName)
        if extensionPath == "png" {
            if let data = UIImagePNGRepresentation(image), !FileManager.default.fileExists(atPath: fileURL.path) {
                do {
                    try data.write(to: fileURL)
                } catch {}
            }
        }else if extensionPath == "jpg"{
            if let data = UIImageJPEGRepresentation(image, 1.0), !FileManager.default.fileExists(atPath: fileURL.path) {
                do {
                    try data.write(to: fileURL)
                } catch {}
            }
        }
    }
    func getCarrier() -> String {
        if let carrierName = CTTelephonyNetworkInfo().subscriberCellularProvider?.carrierName {
            return carrierName
        }
        return ""
    }
    @IBAction func olvidoAccion(_ sender: Any) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "RecuperarContrasenaVC") as! RecuperarContrasenaViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    @IBAction func nuevoAccion(_ sender: Any) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "RegistroVC") as! RegistroViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    @IBAction func accionPrueba(_ sender: Any) {
        let nav = self.storyboard?.instantiateViewController(withIdentifier: "RegistroCincoVC") as! RegistroCincoViewController
        self.navigationController?.pushViewController(nav, animated: true)
    }
    @IBAction func accionMuestraPassword(_ sender: Any) {
        if seguridad == true{
            self.contraseniatxt.isSecureTextEntry = false
            seguridad = false
            self.seeIMG.setImage(UIImage.init(named: "contrasenia.on"), for: .normal)
        }else{
            self.contraseniatxt.isSecureTextEntry = true
            seguridad = true
            self.seeIMG.setImage(UIImage.init(named: "contrasenia.off"), for: .normal)
        }
    }
    @IBAction func loguearAccion(_ sender: Any) {
        if validar() {
            self.iniLoginWithData()
        }
    }
    func isJailBroken() -> Bool {
        if TARGET_IPHONE_SIMULATOR != 1 {
            if FileManager.default.fileExists(atPath: "/Applications/Cydia.app") || FileManager.default.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib") || FileManager.default.fileExists(atPath: "/bin/bash") || FileManager.default.fileExists(atPath: "/usr/sbin/sshd") || FileManager.default.fileExists(atPath: "/etc/apt") || FileManager.default.fileExists(atPath: "/private/var/lib/apt/") || UIApplication.shared.canOpenURL(URL(string:"cydia://package/com.example.package")!){
                return true
            }
            let stringToWrite = "Jailbreak Test"
            do{
                try stringToWrite.write(toFile: "/private/JailbreakTest.txt", atomically:true, encoding:String.Encoding.utf8)
                return true
            }catch{
                return false
            }
        }
        return false
    }
}
