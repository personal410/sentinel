//
//  RECCoincidenciaTableViewCell.swift
//  Sentinel
//
//  Created by Victor Salazar on 3/7/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
class RECCoincidenciaTableViewCell:UITableViewCell {
    @IBOutlet weak var lblNombres:UILabel!
    @IBOutlet weak var lblDocumento:UILabel!
    func bind(with coincidencia:Coincidencia){
        lblNombres.text = coincidencia.descripcion
        lblDocumento.text = "\(coincidencia.tipoDocAbr) \(coincidencia.numDoc)"
    }
}
