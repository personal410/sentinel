//
//  RecuperarBusquedaViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 3/5/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import UIKit
enum TipoBusqueda {
    case documento, nombre
}

class RecuperarBusquedaViewController:ParentViewController {
    //MARK: - Props
    var arrTipoBusqueda:[String] = []
    let usuarioBean = UserGlobalData.sharedInstance.userGlobal!
    var tipo:TipoBusqueda = .documento
    var codServicio = "", tipoDocumentoBusq = "", numeroDocumentoBusq = ""
    var indActual = 0 {
        didSet{
            tfTipoBusqueda.text = "   \(arrTipoBusqueda[indActual])"
        }
    }
    //MARK: - Outlets
    @IBOutlet weak var lblCantDisponibles:UILabel!
    @IBOutlet weak var tfTipoBusqueda:UITextField!
    @IBOutlet weak var tfNumeroDocumento:UITextField!
    @IBOutlet weak var lcNumDocEspacioAbajo:NSLayoutConstraint!
    @IBOutlet weak var svNombre:UIStackView!
    @IBOutlet weak var lcNombreEspacioAbajo:NSLayoutConstraint!
    @IBOutlet weak var tfApellidoPaterno:UITextField!
    @IBOutlet weak var tfApellidoMaterno:UITextField!
    @IBOutlet weak var tfNombres:UITextField!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        if tipo == .documento {
            tfNumeroDocumento.keyboardType = .numberPad
            tfNumeroDocumento.isHidden = false
            svNombre.isHidden = true
            lcNumDocEspacioAbajo.priority = .defaultHigh
            lcNombreEspacioAbajo.priority = .defaultLow
            arrTipoBusqueda = ["AUTOMÁTICO", "DNI", "RUC", "CARNÉ DE EXTRANJERÍA", "PASAPORTE"]
        }else{
            tfNumeroDocumento.isHidden = true
            svNombre.isHidden = false
            lcNumDocEspacioAbajo.priority = .defaultLow
            lcNombreEspacioAbajo.priority = .defaultHigh
            arrTipoBusqueda = ["Persona Natural", "Persona Jurídica", "Otros"]
        }
        indActual = 0
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        tfTipoBusqueda.inputView = pickerView
        let toolbar = UIToolbar(frame: CGRect.zero)
        toolbar.barStyle = .default
        toolbar.isTranslucent = false
        toolbar.sizeToFit()
        let bbiFlexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let bbiOk = UIBarButtonItem(title: "OK", style: .plain, target: self, action: #selector(ok))
        bbiOk.tintColor = UIColor(hex: "FD9526")
        toolbar.items = [bbiFlexible, bbiOk]
        tfTipoBusqueda.inputAccessoryView = toolbar
        if codServicio != "0" {
            showActivityIndicator()
            let dicParams:[String: Any] = ["Usuario": usuarioBean.user!, "SesionId": usuarioBean.SesionId!, "Servicio": codServicio]
            OriginData.sharedInstance.consultarUrl("\(PATHS.PATHSENTINEL)RWS_MSDatosServicio", con: dicParams, notificar: "endConsultarCantidad")
            NotificationCenter.default.addObserver(self, selector: #selector(endConsultarCantidad(_:)), name: NSNotification.Name(rawValue: "endConsultarCantidad"), object: nil)
        }
        #if DEBUG
            tfNumeroDocumento.text = "44647285"
        #endif
    }
    override func viewDidLayoutSubviews(){
        if usuarioBean.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(inNavBar: navBar)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewCont = segue.destination as? CoincidenciaViewController {
            tfNumeroDocumento.text = ""
            tfNombres.text = ""
            tfApellidoPaterno.text = ""
            tfApellidoMaterno.text = ""
            viewCont.codServicio = codServicio
            viewCont.dicBusqueda = sender as! [String: Any]
        }
    }
    //MARK: - Actions
    @IBAction func atras(){
        navigationController?.popViewController(animated: true)
    }
    @IBAction func hideKeyBoard(){
        tfTipoBusqueda.resignFirstResponder()
        tfNumeroDocumento.resignFirstResponder()
        tfApellidoPaterno.resignFirstResponder()
        tfApellidoMaterno.resignFirstResponder()
        tfNombres.resignFirstResponder()
    }
    @IBAction func buscar(){
        if tipo == .documento {
            var tipoDocumento = ""
            let documento = tfNumeroDocumento.text!
            let cantCaracDoc = documento.count
            if indActual == 0 {
                if cantCaracDoc == 8 {
                    tipoDocumento = "D"
                }else if cantCaracDoc == 1 {
                    tipoDocumento = "R"
                }else{
                    Toolbox.showAlert(with: "Alerta", and: "Número de documento no válido, la opción automático solo se aplica para DNI o RUC", in: self)
                    return
                }
            }else if indActual == 1 {
                if cantCaracDoc == 8 {
                    tipoDocumento = "D"
                }else{
                    Toolbox.showAlert(with: "Alerta", and: "Número de documento no válido", in: self)
                    return
                }
            }else if indActual == 2 {
                if cantCaracDoc == 11 {
                    tipoDocumento = "R"
                }else{
                    Toolbox.showAlert(with: "Alerta", and: "Número de documento no válido", in: self)
                    return
                }
            }else if indActual == 3 {
                if cantCaracDoc > 3 {
                    tipoDocumento = "4"
                }else{
                    Toolbox.showAlert(with: "Alerta", and: "Número de documento no válido", in: self)
                    return
                }
            }else if indActual == 4 {
                tipoDocumento = "5"
            }
            if usuarioBean.TDocusu! == tipoDocumento && usuarioBean.NumDocumento! == documento {
                Toolbox.showAlert(with: "Alerta", and: "No se puede buscar a sí mismo.", in: self)
            }else{
                tipoDocumentoBusq = tipoDocumento
                numeroDocumentoBusq = documento
                let dicParams:[String: Any] = ["Usuario": usuarioBean.user!, "TDoc": tipoDocumento, "NDoc": documento, "SesionId": usuarioBean.SesionId!]
                showActivityIndicator()
            OriginData.sharedInstance.consultarUrl("\(PATHS.PATHSENTINEL)\(WEBSERVICES.CONSULTA.RWS_MSTitulares)", con: dicParams, notificar: "endConsultarDocumento")
                NotificationCenter.default.addObserver(self, selector: #selector(endConsultarDocumento(_:)), name: NSNotification.Name(rawValue: "endConsultarDocumento"), object: nil)
            }
        }else{
            if indActual == 0 {
                let nombres = tfNombres.text!
                let apePat = tfApellidoPaterno.text!
                let apeMat = tfApellidoMaterno.text!
                if (!apePat.isEmpty && !apeMat.isEmpty && !nombres.isEmpty) || (!apePat.isEmpty && !nombres.isEmpty) || (!apePat.isEmpty && !apeMat.isEmpty) {
                    var dic:[String: Any] = [:]
                    dic["tipLista"] = 2
                    dic["TipoPersona"] = "N"
                    dic["ApePat"] = apePat
                    dic["ApeMat"] = apeMat
                    dic["Nombres"] = nombres
                    performSegue(withIdentifier: "showCoincidencias", sender: dic)
                }else{
                    Toolbox.showAlert(with: "Alerta", and: "Ingrese los datos mínimos requeridos.", in: self)
                }
            }else{
                let razonSocial = tfNumeroDocumento.text!
                if razonSocial.isEmpty {
                    Toolbox.showAlert(with: "Alerta", and: "No ha ingresado datos.", in: self)
                }else{
                    var dic:[String: Any] = [:]
                    dic["tipLista"] = 2
                    dic["TipoPersona"] = indActual == 1 ? "J" : "O"
                    dic["ApePat"] = razonSocial
                    dic["ApeMat"] = ""
                    dic["Nombres"] = ""
                    performSegue(withIdentifier: "showCoincidencias", sender: dic)
                }
            }
        }
    }
    @IBAction func comprar(){
        performSegue(withIdentifier: "showCompraRecupera", sender: nil)
    }
    //MARK: - Auxiliar
    @objc func ok(){
        tfTipoBusqueda.resignFirstResponder()
    }
    @objc func endConsultarCantidad(_ notification:Notification){
        hideActivityIndicator()
        guard let dic = notification.object as? [String:Any] else {
            return
        }
        if let saldoServicio = dic["SaldoServicio"] as? [String:Any] {
            if let dispDet = saldoServicio["DispDet"] as? String {
                lblCantDisponibles.text = "Disponible: \(dispDet)"
            }
        }
    }
    @objc func endConsultarDocumento(_ notification:Notification){
        hideActivityIndicator()
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        if var dic = notification.object as? [String:Any], let codigoWs = dic["CodigoWS"] as? String {
            if codigoWs == "0" {
                if let apePat = dic["ApePat"] as? String, let nombreRazonSoc = dic["NombreRazSoc"] as? String {
                    if !apePat.isEmpty || !nombreRazonSoc.isEmpty {
                        dic["TipoPersona"] = tipoDocumentoBusq
                        if tipoDocumentoBusq == "R" {
                            dic["ApePat"] = nombreRazonSoc
                        }
                        dic["NumDocumento"] = numeroDocumentoBusq
                        dic["tipLista"] = 1
                        performSegue(withIdentifier: "showCoincidencias", sender: dic)
                    }else{
                        Toolbox.showAlert(with: "Alerta", and: "Número de documento no encontrado.", in: self)
                    }
                    return
                }
            }else if codigoWs == "99" {
                showError99()
                return
            }else{
                iniMensajeError(codigo: codigoWs)
                return
            }
        }
        let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
        alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
        present(alerta, animated: true, completion: nil)
    }
}
//MARK: - Extension - PickerView
extension RecuperarBusquedaViewController:UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView:UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView:UIPickerView, numberOfRowsInComponent component:Int) -> Int {
        return arrTipoBusqueda.count
    }
    func pickerView(_ pickerView:UIPickerView, titleForRow row:Int, forComponent component:Int) -> String? {
        return arrTipoBusqueda[row]
    }
    func pickerView(_ pickerView:UIPickerView, didSelectRow row:Int, inComponent component:Int){
        indActual = row
        if tipo == .nombre {
            if indActual == 0 {
                tfNumeroDocumento.isHidden = true
                svNombre.isHidden = false
                lcNumDocEspacioAbajo.priority = .defaultLow
                lcNombreEspacioAbajo.priority = .defaultHigh
            }else{
                tfNumeroDocumento.placeholder = indActual == 1 ? "Razón Social" : "Nombres"
                tfNumeroDocumento.isHidden = false
                svNombre.isHidden = true
                lcNumDocEspacioAbajo.priority = .defaultHigh
                lcNombreEspacioAbajo.priority = .defaultLow
            }
        }else{
            tfNumeroDocumento.text = ""
            if indActual == 3 {
                tfNumeroDocumento.keyboardType = .default
            }else{
                tfNumeroDocumento.keyboardType = .numberPad
            }
        }
    }
}
//MARK: - Extension - TextField
extension RecuperarBusquedaViewController:UITextFieldDelegate {
    func textField(_ textField:UITextField, shouldChangeCharactersIn range:NSRange, replacementString string:String) -> Bool {
        if tipo == .documento && textField == tfNumeroDocumento {
            let maxLength = indActual == 1 ? 8 : (indActual == 3 ? 20 : 11)
            let count = textField.text!.count + string.count - range.length
            return count <= maxLength
        }
        return true
    }
}
