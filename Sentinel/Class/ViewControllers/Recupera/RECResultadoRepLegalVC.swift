//
//  RECResultadoRepLegalVC.swift
//  Sentinel
//
//  Created by Victor Salazar on 3/7/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//

class RECResultadoRepLegalVC:ParentViewController, UITableViewDelegate, UITableViewDataSource {
    //MARK: - Outlets
    @IBOutlet weak var lblCantCoincidencias:UILabel!
    @IBOutlet weak var tvCoincidencias:UITableView!
    //MARK: - Props
    let usuarioBean = UserGlobalData.sharedInstance.userGlobal!
    var codServicio = ""
    var arrCoincidenciasFinal:[Coincidencia] = []
    var dicBusqueda:[String:Any] = [:]
    var tipLista = 0
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        tipLista = dicBusqueda["tipLista"] as! Int
        let tipoPersona = dicBusqueda["TipoPersona"] as! String
        let apePat = dicBusqueda["ApePat"] as! String
        let apeMat = dicBusqueda["ApeMat"] as! String
        let nombres = dicBusqueda["Nombres"] as! String
        if tipLista == 1 {
            var tipoDoc = ""
            if tipoPersona == "D" {
                tipoDoc = "DNI"
            }else if tipoPersona == "R" {
                tipoDoc = "RUC"
            }else if tipoPersona == "4" {
                tipoDoc = "CE"
            }
            let nomCompelto = "\(apePat) \(apeMat) \(nombres)"
            let dicPersona:[String: Any] = ["tipodoc": tipoPersona, "TipoDocAbr": tipoDoc, "numdoc": dicBusqueda["NumDocumento"] as Any, "descripcion": nomCompelto, "NroRev": 0]
            let coincidencia = Coincidencia(with: dicPersona)
            arrCoincidenciasFinal.append(coincidencia)
            actualizarCantCoincidencias()
        }else{
            let random = arc4random_uniform(900000) + 100000
            let idSesion = "\(random)"
            let dicParamsBusqueda:[String: Any] = ["UserId": usuarioBean.user!, "IdSession": idSesion, "TipoPersona": tipoPersona, "Paterno": apePat, "Materno": apeMat, "nombres": nombres, "UseSeIDSession": usuarioBean.SesionId!]
            NotificationCenter.default.addObserver(self, selector: #selector(endConsultarBusqPorNombre(_:)), name: NSNotification.Name(rawValue: "endConsultarPorNombre"), object: nil)
            OriginData.sharedInstance.consultarUrl("\(PATHS.PATH)RWSBusXNomCT", con: dicParamsBusqueda, notificar: "endConsultarPorNombre")
        }
    }
    override func viewDidLayoutSubviews(){
        if usuarioBean.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(inNavBar: navBar)
        }
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? RecuperaRemitenteViewController {
            if let c = sender as? Coincidencia {
                viewCont.coincidencia = c
                viewCont.codServicio = codServicio
            }
        }
    }
    //MARK: - Actions
    @IBAction func atras(){
        navigationController?.popViewController(animated: true)
    }
    //MARK: - Auxiliar
    @objc func endConsultarBusqPorNombre(_ noti:Notification){
        hideActivityIndicator()
        NotificationCenter.default.removeObserver(self, name: noti.name, object: nil)
        if var dic = noti.object as? [String:Any], let codigoWs = dic["CodigoWS"] as? String {
            if codigoWs == "0" {
                if let arr = dic["SDTBusAlf"] as? [[String: Any]] {
                    arrCoincidenciasFinal = Coincidencia.transformList(arr)
                    tvCoincidencias.reloadData()
                    actualizarCantCoincidencias()
                    return
                }
            }else if codigoWs == "99" {
                showError99()
                return
            }else{
                iniMensajeError(codigo: codigoWs)
                return
            }
        }
        let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
        alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
        present(alerta, animated: true, completion: nil)
    }
    func actualizarCantCoincidencias(){
        let cantCoincidencias = arrCoincidenciasFinal.count
        if cantCoincidencias == 0 {
            lblCantCoincidencias.text = "No se encontraron coincidencias."
        }else{
            if cantCoincidencias > 100 {
                lblCantCoincidencias.text = "Hay más de 100 coincidencias encontradas."
            }else{
                let numero = cantCoincidencias == 1 ? "" : "s"
                lblCantCoincidencias.text = "Hay \(cantCoincidencias) coincidencia\(numero) encontrada\(numero)."
            }
        }
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arrCoincidenciasFinal.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "coincidenciaCell", for: indexPath) as! RECCoincidenciaTableViewCell
        cell.bind(with: arrCoincidenciasFinal[indexPath.row])
        return cell
    }
    func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath){
        let coincidencia = arrCoincidenciasFinal[indexPath.row]
        let alertCont = UIAlertController(title: "Pregunta", message: "¿Desea agregar a \(coincidencia.numDoc) \(coincidencia.descripcion)?", preferredStyle: .alert)
        alertCont.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        alertCont.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action) in
            let currentIndex = self.navigationController?.viewControllers.index(of: self) ?? 0
            if currentIndex > 0, let viewCont = self.navigationController?.viewControllers[currentIndex - 3] as? NotificarPaso1ViewController {
                viewCont.dicRepresentante = coincidencia
                self.navigationController?.popToViewController(viewCont, animated: true)
            }
        }))
        present(alertCont, animated: true, completion: nil)
    }
}
