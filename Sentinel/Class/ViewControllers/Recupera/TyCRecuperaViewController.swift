//
//  TyCRecuperaViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 14/6/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
class TyCRecuperaViewController:UIViewController {
    //MARK: - Actions
    @IBAction func cancelar(){
        view.removeFromSuperview()
        removeFromParentViewController()
    }
    @IBAction func aceptar(){
        view.removeFromSuperview()
        removeFromParentViewController()
    }
}
