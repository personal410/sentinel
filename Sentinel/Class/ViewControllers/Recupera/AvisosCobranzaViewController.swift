//
//  AvisosCobranzaViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 2/27/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
class AvisosCobranzaViewController:ParentViewController, DiscreparDelegate {
    //MARK: - Props
    var arrAvisosCobranza:[[String: Any]] = []
    //MARK: - Outlets
    @IBOutlet weak var tvAvisosCobranza:UITableView!
    //MARK: - ViewCont
    override func viewDidLayoutSubviews(){
        if UserGlobalData.sharedInstance.userGlobal!.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(inNavBar: navBar)
        }
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        let indice = (sender as! UIButton).tag
        let dicAvisoCobranza = arrAvisosCobranza[indice]
        if let viewCont = segue.destination as? AvisoCobranzaDetalleViewController {
            viewCont.dicAvisoCobranza = dicAvisoCobranza
        }else if let viewCont = segue.destination as? DiscreparAvisoCobranzaViewController {
            viewCont.dicAvisoCobranza = dicAvisoCobranza
            viewCont.indice = indice
            viewCont.delegate = self
        }
    }
    //MARK: - Actions
    @IBAction func atras(){
        navigationController?.popViewController(animated: true)
    }
    //MARK: - Discrepar
    func discrepoCorrectamente(con indice:Int){
        arrAvisosCobranza[indice]["CTFlgDiscrep"] = "S"
        tvAvisosCobranza.reloadRows(at: [IndexPath(row: indice, section: 0)], with: .none)
    }
}
extension AvisosCobranzaViewController:UITableViewDataSource {
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arrAvisosCobranza.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AvisoCobranzaTableViewCell
        cell.bind(arrAvisosCobranza[indexPath.row], at: indexPath.row)
        return cell
    }
}
