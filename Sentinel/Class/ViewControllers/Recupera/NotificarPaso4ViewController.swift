//
//  NotificarPaso4ViewController.swift
//  Sentinel
//
//  Created by Daniel on 27/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class NotificarPaso4ViewController:ParentViewController {
    //MARK: - Outlets
    @IBOutlet weak var lblFechaActual:UILabel!
    @IBOutlet weak var lblNombreTercero:UILabel!
    @IBOutlet weak var lblDireccion:UILabel!
    @IBOutlet weak var lblNombreUsuario:UILabel!
    @IBOutlet weak var lblConceptoDeuda:UILabel!
    @IBOutlet weak var lblMontoDeuda:UILabel!
    @IBOutlet weak var lblMonedaDeuda:UILabel!
    @IBOutlet weak var lblFecVencimiento:UILabel!
    @IBOutlet weak var lblTelefono:UILabel!
    //MARK: - Props
    var notificacionMorosidad:NotificacionMorosidad!
    let user = UserGlobalData.sharedInstance.userGlobal!
    //MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cargarData()
        showActivityIndicator()
        let dicParams = ["Usuario": user.user ?? "", "SesionId": user.SesionId ?? ""]
        ServiceConnector.connectToUrl("\(PATHS.PATHSENTINEL)RWS_MSDatosUsuario", params: dicParams){(r, e) in
            self.hideActivityIndicator()
            guard e == nil, let dic = r as? [String: Any] else {
                Toolbox.showAlert(with: "ALERTA", and: "En estos momentos no es posible realizar la consulta.", in: self)
                return
            }
            let celular = (dic["Celular"] as? String ?? "").trimmingCharacters(in: CharacterSet.whitespaces)
            let attrsStringTelefono = NSMutableAttributedString(string: "Si requiere mayor información sobre el presente, puede comunicarse a los teléfonos de nuestros cliente \(celular).")
            attrsStringTelefono.addAttribute(.font, value: UIFont(name: "Roboto-Bold", size: 13)!, range: NSRange(location: 103, length: celular.count))
            self.lblTelefono.attributedText = attrsStringTelefono
        }
    }
    override func viewDidLayoutSubviews(){
        if user.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(inNavBar: navBar)
        }
    }
    //MARK: - Actions
    @IBAction func atras(){
        let alertaVersion = UIAlertController(title: PATHS.SENTINEL, message: "¿Desea cancelar el proceso?", preferredStyle: .alert)
        alertaVersion.addAction(UIAlertAction(title: "SI", style: .default, handler: { (action: UIAlertAction!) in
            self.navigationController?.popToRootViewController(animated: true)
        }))
        alertaVersion.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
        present(alertaVersion, animated: true, completion: nil)
    }
    @IBAction func modificar(){
        navigationController?.popViewController(animated: true)
    }
    @IBAction func enviar(){
        iniPasoCuatro()
    }
    //MARK: - Auxiliar
    func cargarData(){
        lblFechaActual.text = Toolbox.getDate(date: Date(), withFormat: "dd 'de ' MMMM 'del' yyyy")
        let coincidencia = notificacionMorosidad.coincidencia
        lblNombreTercero.text = coincidencia.descripcion
        lblDireccion.text = notificacionMorosidad.direccion
        let remitente = notificacionMorosidad.nombreRemitente
        let attrsStringUsuario = NSMutableAttributedString(string: "Nos es grato dirigirnos a Ud. por encargo de nuestro cliente \(remitente) quién nos ha solicitado registrarlo en nuestra Central de Riesgos SENTINEL, por una deuda vencida que mantiene con el mismo, la cual se detalla a continuación:")
        attrsStringUsuario.addAttribute(.font, value: UIFont(name: "Roboto-Bold", size: 13)!, range: NSRange(location: 61, length: remitente.count))
        lblNombreUsuario.attributedText = attrsStringUsuario
        lblConceptoDeuda.text = notificacionMorosidad.conceptoDeudaDesc
        lblMontoDeuda.text = notificacionMorosidad.monto
        lblMonedaDeuda.text = notificacionMorosidad.moneda == "PEN" ? "soles" : "dolares"
        lblFecVencimiento.text = notificacionMorosidad.fechaVencimiento.split(separator: "-").reversed().joined(separator: "/")
    }
    func iniPasoCuatro(){
        showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endPasoCuatro(_:)), name: NSNotification.Name("endPasoCuatro"), object: nil)
        let coincidencia = notificacionMorosidad.coincidencia
        let parametros = [
            "CTUseCod" :  user.user!,
            "CTConDeuId" : notificacionMorosidad.conceptoDeudaId,
            "CTTDocCPT" : coincidencia.tipoDoc,
            "CTNDocCPT" : coincidencia.numDoc,
            "CTTDocAcre": notificacionMorosidad.tipoDocRemitente,
            "CTNDocAcre": notificacionMorosidad.nroDocRemitente,
            "CTNCorr" : notificacionMorosidad.correlativo,
            "CTPasoReg" :  "4",
            "CTDirecc" :  notificacionMorosidad.direccion,
            "CTMail" :  notificacionMorosidad.correo,
            "CTCelular" :  notificacionMorosidad.celular,
            "CTMonDeu" :  notificacionMorosidad.monto,
            "CTMonedaDeu" :  notificacionMorosidad.moneda,
            "CTFecEmi" :  notificacionMorosidad.fechaEmision,
            "CTTipDocProb" :  notificacionMorosidad.tipoDocumento,
            "CTDocProb" :  notificacionMorosidad.documentoAdjunto,
            "CTFecVen" :  notificacionMorosidad.fechaVencimiento,
            "CTFecPlLim" : notificacionMorosidad.fechaLimite,
            "CTComent" :  notificacionMorosidad.comentarios,
            "CTTipConAce" : "1",
            "SesionId" :  user.SesionId!,
            "CTNDocEmp" :  "",
            "origenAplicacion" : "\(PATHS.APP_ID)"
            ] as [String: Any]
        OriginData.sharedInstance.registroDeudor(notification: "endPasoCuatro", parametros: parametros)
    }
    @objc func endPasoCuatro(_ notification:NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard let data = notification.object as? TerceroClass, let codigoWs = data.CodigoWS else {
            Toolbox.showAlert(with: "ALERTA", and: "En estos momentos no es posible realizar la consulta.", in: self)
            return
        }
        if codigoWs == "0" {
            Toolbox.showAlert(with: PATHS.SENTINEL, and: "Su reporte ha sido enviado satisfactoriamente.", withBtnTitle: "Aceptar", withOkAction: {
                self.navigationController?.popToRootViewController(animated: true)
            }, in: self)
        }else if codigoWs == "99" {
            showError99()
        }else{
            iniMensajeError(codigo: codigoWs)
        }
    }
}
