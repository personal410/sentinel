//
//  RecuRemitenteTableViewCell.swift
//  Sentinel
//
//  Created by Victor Salazar on 24/6/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
class RecuRemitenteTableViewCell:UITableViewCell {
    //MARK: - Outlets
    @IBOutlet weak var lblNombres:UILabel!
    @IBOutlet weak var lblDocumento:UILabel!
    //MARK: - Auxiliar
    func bind(dic dictionary:[String:Any]){
        lblNombres.text = dictionary["NomEmp"] as? String
        let tipoDoc = dictionary["TDocEmp"] as? String ?? ""
        var nomTipoDoc = ""
        if tipoDoc == "D" {
            nomTipoDoc = "DNI"
        }else if tipoDoc == "R" {
            nomTipoDoc = "RUC"
        }
        lblDocumento.text = "\(nomTipoDoc) \(dictionary["NDocEmp"] as? String ?? "")"
    }
}
