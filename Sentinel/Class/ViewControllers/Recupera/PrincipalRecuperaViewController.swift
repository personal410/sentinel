//
//  PrincipalRecuperaViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 2/26/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import UIKit
import ViewPager_Swift
class PrincipalRecuperaViewController:ParentViewController {
    //MARK: - Attributes
    var esPrimeraVez = true
    var codServicio = ""
    //MARK: - Outlets
    @IBOutlet weak var vViewPagerContent:UIView!
    @IBOutlet weak var vMensaje:UIView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        let userData = UserGlobalData.sharedInstance.userGlobal!
        if !userData.flagRecupera {
            vMensaje.isHidden = false
            userData.flagRecupera = true
        }
    }
    override func viewWillAppear(_ animated:Bool){
        super.viewWillAppear(animated)
        if esPrimeraVez {
            esPrimeraVez = false
            let vpOptions = ViewPagerOptions(viewPagerWithFrame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - 64))
            vpOptions.tabType = .basic
            vpOptions.isEachTabEvenlyDistributed = true
            vpOptions.fitAllTabsInView = true
            vpOptions.isTabHighlightAvailable = true
            vpOptions.tabViewBackgroundDefaultColor = UIColor.black
            vpOptions.tabViewBackgroundHighlightColor = UIColor.clear
            vpOptions.tabViewTextDefaultColor = UIColor.gray
            vpOptions.tabViewTextHighlightColor = UIColor(hex: "06B150")
            vpOptions.tabViewTextFont = UIFont.systemFont(ofSize: 12)
            vpOptions.tabIndicatorViewBackgroundColor = UIColor(hex: "06B150")
            let viewPager = ViewPagerController()
            viewPager.options = vpOptions
            viewPager.dataSource = self
            self.addChildViewController(viewPager)
            vViewPagerContent.addSubview(viewPager.view)
            viewPager.didMove(toParentViewController: self)
        }
    }
    override func viewDidLayoutSubviews() {
        let userData = UserGlobalData.sharedInstance.userGlobal!
        if userData.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(inNavBar: navBar)
        }
    }
    //MARK: - Actions
    @IBAction func atras(){
        navigationController?.popViewController(animated: true)
    }
    @IBAction func continuar(){
        vMensaje.isHidden = true
    }
}

extension PrincipalRecuperaViewController:ViewPagerControllerDataSource {
    func numberOfPages() -> Int {
        return 2
    }
    func viewControllerAtPosition(position: Int) -> UIViewController {
        if position == 0 {
            let viewCont = storyboard!.instantiateViewController(withIdentifier: "NotificarVC") as! NotificarViewController
            viewCont.codServicio = codServicio
            return viewCont
        }else{
            return storyboard!.instantiateViewController(withIdentifier: "HistorialVC")
        }
    }
    func tabsForPages() -> [ViewPagerTab] {
        var arrTabs:[ViewPagerTab] = []
        arrTabs.append(ViewPagerTab(title: "Notificar", image: nil))
        arrTabs.append(ViewPagerTab(title: "Historial de notificaciones", image: nil))
        return arrTabs
    }
}
