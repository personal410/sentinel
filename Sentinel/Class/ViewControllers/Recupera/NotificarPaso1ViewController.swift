//
//  NotificarPaso1ViewController.swift
//  Sentinel
//
//  Created by Daniel on 26/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class NotificarPaso1ViewController:ParentViewController {
    //MARK: - Outlets
    @IBOutlet weak var lblMensaje:UILabel!
    @IBOutlet weak var lblNombre:UILabel!
    @IBOutlet weak var lblNroDoc:UILabel!
    @IBOutlet weak var tfDireccion:UITextField!
    @IBOutlet weak var tfEmail:UITextField!
    @IBOutlet weak var tfCelular:UITextField!
    @IBOutlet weak var vRepresentante:UIView!
    @IBOutlet weak var lblNombreRepresentante:UILabel!
    @IBOutlet weak var lblDocumentoRepresentante:UILabel!
    //MARK: - Props
    var notificacionMorosidad:NotificacionMorosidad!
    let userData = UserGlobalData.sharedInstance.userGlobal!
    var tipoDoc:String!
    var dicRepresentante:Coincidencia?
    var dicParams:[String: Any] = [:]
    var correlativo = 0
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        let coincidencia = notificacionMorosidad.coincidencia
        tipoDoc = coincidencia.tipoDoc
        lblNombre.text = coincidencia.descripcion
        lblNroDoc.text = "\(coincidencia.tipoDocAbr) \(coincidencia.numDoc)"
        if tipoDoc == "R" {
            lblMensaje.text = "A continuación ingrese los datos del representante de la empresa a quien va a notificar:"
            vRepresentante.isHidden = false
        }else{
            lblMensaje.text = "A continuación ingrese la información de la persona a quien va a notificar:"
        }
        #if DEBUG
            tfDireccion.text = "Mi casa"
            tfEmail.text = "vic@gmail.com"
            tfCelular.text = "987654321"
        #endif
    }
    override func viewDidLayoutSubviews(){
        if userData.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(inNavBar: navBar)
        }
    }
    override func viewWillAppear(_ animated:Bool){
        if let representante = dicRepresentante {
            lblNombreRepresentante.text = representante.descripcion
            lblDocumentoRepresentante.text = "\(representante.tipoDocAbr) \(representante.numDoc)"
        }
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?) {
        if let viewCont = segue.destination as? NotificarPaso2ViewController {
            viewCont.notificationMorosidad = notificacionMorosidad
        }
    }
    //MARK: - Actions
    @IBAction func atras(){
        navigationController?.popViewController(animated: true)
    }
    @IBAction func ocultarTeclado(){
        view.endEditing(true)
    }
    @IBAction func buscar(){
        
    }
    @IBAction func siguiente(){
        if tipoDoc == "R" && dicRepresentante == nil {
            Toolbox.showAlert(with: "Alerta", and: "No se ha ingresado un representante legal.", in: self)
            return
        }
        if tfDireccion.text!.count < 3 {
            Toolbox.showAlert(with: "Alerta", and: "Dirección no válida.", in: self)
            return
        }
        if !Toolbox.validateEmail(tfEmail.text!) {
            Toolbox.showAlert(with: "Alerta", and: "Email no válido.", in: self)
            return
        }
        let celular = tfCelular.text!
        if celular.count != 9 {
            Toolbox.showAlert(with: "Alerta", and: "Número de celular no válido.", in: self)
            return
        }
        if celular[0..<1] != "9" {
            Toolbox.showAlert(with: "Alerta", and: "Número debe empezar con el número 9.", in: self)
            return
        }
        let coincidencia = notificacionMorosidad.coincidencia
        dicParams = ["CTTDocCPT": tipoDoc, "CTNDocCPT": coincidencia.numDoc, "CTTDocAcre": notificacionMorosidad.tipoDocRemitente, "CTNDocAcre": notificacionMorosidad.nroDocRemitente, "CTNDocEmp": "", "CTDirecc": tfDireccion.text!, "CTMail": tfEmail.text!, "CTCelular": celular, "CTPasoReg": 1, "CTUseCod": userData.user!, "SesionId": userData.SesionId!]
        showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(endPaso1(_:)), name: NSNotification.Name(rawValue: "endPaso1"), object: nil)
    OriginData.sharedInstance.consultarUrl("\(PATHS.PATHSENTINEL)\(WEBSERVICES.TERCEROS.RWS_MSCTRegistroDeudor)", con: dicParams, notificar: "endPaso1")
    }
    //MARK: - Auxiliar
    @objc func endPaso1(_ notification:Notification){
        hideActivityIndicator()
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        if var dic = notification.object as? [String:Any], let codigoWs = dic["CodigoWS"] as? String {
            if codigoWs == "0" {
                if let correlativo = dic["xCTNCorr"] as? Int {
                    notificacionMorosidad.direccion = tfDireccion.text!
                    notificacionMorosidad.correo = tfEmail.text!
                    notificacionMorosidad.celular = tfCelular.text!
                    notificacionMorosidad.correlativo = correlativo
                    performSegue(withIdentifier: "showPaso2", sender: nil)
                    return
                }
            }else if codigoWs == "99" {
                showError99()
                return
            }else{
                iniMensajeError(codigo: codigoWs)
                return
            }
        }
        Toolbox.showAlert(with: "ALERTA", and: "En estos momentos no es posible realizar la consulta.", in: self)
    }
}
