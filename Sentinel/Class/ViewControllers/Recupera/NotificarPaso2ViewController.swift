//
//  NotificarPaso2ViewController.swift
//  Sentinel
//
//  Created by Daniel on 26/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class NotificarPaso2ViewController:ParentViewController, MiPickerComboDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, RecuperaFechaPickerVCDelegate, UITextFieldDelegate {
    //MARK: - Outlets
    @IBOutlet weak var comboConcepto:CTComboPicker!
    @IBOutlet weak var comboMoneda:CTComboPicker!
    @IBOutlet weak var comboTipo:CTComboPicker!
    @IBOutlet weak var littleConceptoBT:UIButton!
    @IBOutlet weak var littleTipoBT:UIButton!
    @IBOutlet weak var detalleDocumentoAdjuntoLB:UILabel!
    @IBOutlet weak var montoTX: UITextField!
    @IBOutlet weak var docAdjuntoBT:UIButton!
    @IBOutlet weak var tipBotonBT:UIButton!
    @IBOutlet weak var conBotonBT:UIButton!
    @IBOutlet weak var dia1:CornerSecondButton!
    @IBOutlet weak var mes1:CornerSecondButton!
    @IBOutlet weak var anio1:CornerSecondButton!
    @IBOutlet weak var dia2:CornerSecondButton!
    @IBOutlet weak var mes2:CornerSecondButton!
    @IBOutlet weak var anio2:CornerSecondButton!
    //MARK: - Props
    var notificationMorosidad:NotificacionMorosidad!
    var arrConceptoFinal = NSArray()
    let arrConceptoTitulo = NSMutableArray()
    var indConceptoSeleccionado = -1
    var arrMonedaTitulo = NSArray(objects: "Soles", "Dólares")
    var indMonedaSeleccionado = 0
    var arrTipoFinal = NSArray()
    let arrTipoTitulo = NSMutableArray()
    var indTipoDocSeleccionado = -1
    var dateFecEmision:Date?
    var dateFecVencimiento:Date?
    var docAdjunto = ""
    var docBase64 = ""
    let user = UserGlobalData.sharedInstance.userGlobal!
    //MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
        inicializar()
        gestureScreen()
        iniConceptoDeuda()
        montoTX.delegate = self
        #if DEBUG
            montoTX.text = "100"
        #endif
    }
    override func viewDidLayoutSubviews(){
        if user.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(inNavBar: navBar)
        }
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? RecuperaFechaPickerViewController {
            viewCont.delegate = self
            let data = sender as! RecuperaFechaData
            viewCont.opcion = data.opcion
            viewCont.minDate = data.minDate
            viewCont.maxDate = data.maxDate
            viewCont.descripcion = data.opcion == 1 ? "La fecha de emisión no puede ser mayor o igual a la fecha de ayer." : "La fecha de vencimiento no puede ser menor o igual a la fecha de emisión, tampoco mayor o igual a la fecha de hoy."
        }else if let viewCont = segue.destination as? NotificarPaso3ViewController {
            viewCont.notificacionMorosidad = notificationMorosidad
        }
    }
    //MARK: - Actions
    @IBAction func atras(){
        let alertCont = UIAlertController(title: PATHS.SENTINEL, message: "¿Desea cancelar el proceso?", preferredStyle: .alert)
        alertCont.addAction(UIAlertAction(title: "SI", style: .default, handler: { (action) in
            self.navigationController?.popToRootViewController(animated: true)
        }))
        alertCont.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
        present(alertCont, animated: true, completion: nil)
    }
    @IBAction func accionConcepto(_ sender:Any){
        comboConcepto.open()
    }
    @IBAction func accionSoles(_ sender:Any){
        comboMoneda.open()
    }
    @IBAction func accionMostrarFechaEmision(){
        let data = RecuperaFechaData()
        data.opcion = 1
        if let dateFechaVencimiento = dateFecVencimiento {
            var dateComps = DateComponents()
            dateComps.day = -1
            data.maxDate = Calendar.current.date(byAdding: dateComps, to: dateFechaVencimiento)
        }else{
            var dateComps = DateComponents()
            dateComps.day = -2
            data.maxDate = Calendar.current.date(byAdding: dateComps, to: Date())
        }
        performSegue(withIdentifier: "showFecha", sender: data)
    }
    @IBAction func accionTipoDoc(_ sender:Any){
        comboTipo.open()
    }
    @IBAction func accionMostrarFechaVencimiento(){
        let data = RecuperaFechaData()
        data.opcion = 2
        if let dateFechaEmision = dateFecEmision {
            let components = Calendar.current.dateComponents([.day], from: dateFechaEmision, to: Date())
            var dateComps = DateComponents()
            if components.day! < 1825 {
                dateComps.day = 1
                data.minDate = Calendar.current.date(byAdding: dateComps, to: dateFechaEmision)
            }else{
                dateComps.day = -1825
                data.minDate = Calendar.current.date(byAdding: dateComps, to: Date())
            }
        }else{
            var dateComps = DateComponents()
            dateComps.day = -1825
            data.minDate = Calendar.current.date(byAdding: dateComps, to: Date())
        }
        var maxDateComps = DateComponents()
        maxDateComps.day = -1
        data.maxDate = Calendar.current.date(byAdding: maxDateComps, to: Date())
        performSegue(withIdentifier: "showFecha", sender: data)
    }
    @IBAction func accionDocAdjunto(_ sender:Any){
        if docAdjunto.isEmpty {
            let alertCont = UIAlertController(title: "Imagen", message: "Adjunte una imagen a la notificación", preferredStyle: .actionSheet)
            alertCont.addAction(UIAlertAction(title: "Camara", style: .default, handler: { (action) in
                if UIImagePickerController.isSourceTypeAvailable(.camera){
                    let myPickerController = UIImagePickerController()
                    myPickerController.delegate = self;
                    myPickerController.sourceType = .camera
                    self.present(myPickerController, animated: true, completion: nil)
                }else{
                    self.showAlert(PATHS.SENTINEL, mensaje: "La cámara no se encuentra disponible.")
                }
            }))
            alertCont.addAction(UIAlertAction(title: "Libreria", style: .default, handler: { (action) in
                if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                    let myPickerController = UIImagePickerController()
                    myPickerController.delegate = self;
                    myPickerController.sourceType = .photoLibrary
                    self.present(myPickerController, animated: true, completion: nil)
                }else{
                    self.showAlert(PATHS.SENTINEL, mensaje: "La cámara no se encuentra disponible.")
                }
            }))
            alertCont.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            present(alertCont, animated: true, completion: nil)
        }else{
            let alertCont = UIAlertController(title: PATHS.SENTINEL, message: "Está por eliminar la foto adjunta. ¿Desea continuar?", preferredStyle: .alert)
            alertCont.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.cancel, handler: {(action) in
                self.docAdjuntoBT.setImage(UIImage(named: "adjuntar-icon2")!, for: .normal)
                self.docAdjunto = ""
                self.detalleDocumentoAdjuntoLB.text = ""
            }))
            alertCont.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.default, handler: nil))
            present(alertCont, animated: true, completion:nil)
        }
    }
    @IBAction func accionSiguiente(_ sender:Any){
        if indConceptoSeleccionado == -1 {
            showAlert(PATHS.SENTINEL, mensaje: "Debe elegir el concepto de la deuda.")
            return
        }
        if montoTX.text!.isEmpty {
            showAlert(PATHS.SENTINEL, mensaje: "Debe ingresar el monto de la deuda.")
            return
        }
        if dateFecEmision == nil {
            showAlert(PATHS.SENTINEL, mensaje: "Debe seleccionar la fecha de emisión.")
            return
        }
        if indTipoDocSeleccionado == -1 {
            showAlert(PATHS.SENTINEL, mensaje: "Debe elegir el tipo de documento probatorio.")
            return
        }
        if docAdjunto.isEmpty {
            showAlert(PATHS.SENTINEL, mensaje: "Debe tomar una foto del documento probatorio.")
            return
        }
        if dateFecVencimiento == nil {
            showAlert(PATHS.SENTINEL, mensaje: "Debe seleccionar la fecha de vecimiento de la deuda.")
            return
        }
        iniSubirFoto()
    }
    //MARK: - Auxiliar
    func inicializar(){
        comboConcepto.delegate = self
        comboMoneda.delegate = self
        comboTipo.delegate = self
        detalleDocumentoAdjuntoLB.text = ""
    }
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    func iniConceptoDeuda(){
        showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endConceptoDeuda(_:)), name: NSNotification.Name("endConceptoDeuda"), object: nil)
        OriginData.sharedInstance.listarConcepto(notification: "endConceptoDeuda", parametros: [:])
    }
    @objc func endConceptoDeuda(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        guard let data = notification.object as? TerceroClass, let arrConceptos = data.SDT_CTConceptosDeuda else {
            hideActivityIndicator()
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        arrConceptoFinal = arrConceptos
        arrConceptoTitulo.removeAllObjects()
        for i in 0 ..< arrConceptoFinal.count {
            let dicConcepto = arrConceptoFinal.object(at: i) as! NSDictionary
            let conceptoTitulo = dicConcepto.object(forKey: "CTConDeuDsc") as! String
            arrConceptoTitulo.add(conceptoTitulo)
        }
        comboConcepto.setConfig((arrConceptoTitulo as! [Any]))
        comboConcepto.setId(1)
        iniTipoDocumento()
    }
    func iniTipoDocumento(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.endTipoDocumento(_:)), name: NSNotification.Name("endTipoDocumento"), object: nil)
        OriginData.sharedInstance.listarTipoDocumento(notification: "endTipoDocumento", parametros: [:])
    }
    @objc func endTipoDocumento(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard let data = notification.object as? TerceroClass, let arrTipos = data.SDT_CTDocProb else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        arrTipoFinal = arrTipos
        arrTipoTitulo.removeAllObjects()
        for i in 0 ..< arrTipoFinal.count {
            let dicTipo = arrTipoFinal.object(at: i) as! NSDictionary
            let tipoTitulo = dicTipo.object(forKey: "CTDscDocProb") as! String
            arrTipoTitulo.add(tipoTitulo)
        }
        comboTipo.setConfig((arrTipoTitulo as! [Any]))
        comboTipo.setId(3)
        cargarData()
    }
    func cargarData(){
        if arrConceptoTitulo.count == 1 {
            indConceptoSeleccionado = 0
            comboConcepto.generalBT.isEnabled = false
            littleConceptoBT.isEnabled = false
            comboConcepto.generalBT.setTitle("    \((arrConceptoTitulo.object(at: 0) as! String))", for: .normal)
            conBotonBT.layer.borderWidth = 0.5
            conBotonBT.layer.borderColor = UIColor.lightGray.cgColor
        }else{
            conBotonBT.layer.borderWidth = 1.0
            conBotonBT.layer.borderColor = UIColor.gray.cgColor
        }
        conBotonBT.layer.cornerRadius = 8.0
        conBotonBT.layer.masksToBounds = true
        comboMoneda.setConfig((arrMonedaTitulo as! [Any]))
        comboMoneda.setId(2)
        if arrTipoTitulo.count == 1 {
            indTipoDocSeleccionado = 0
            comboTipo.generalBT.isEnabled = false
            littleTipoBT.isEnabled = false
            comboTipo.generalBT.setTitle("    \((arrTipoTitulo.object(at: 0) as! String))", for: .normal)
            tipBotonBT.layer.borderWidth = 0.5
            tipBotonBT.layer.borderColor = UIColor.lightGray.cgColor
        }else{
            tipBotonBT.layer.borderWidth = 1.0
            tipBotonBT.layer.borderColor = UIColor.gray.cgColor
        }
        tipBotonBT.layer.cornerRadius = 8.0
        tipBotonBT.layer.masksToBounds = true
    }
    func iniSubirFoto(){
        showActivityIndicator()
        Toolbox.enviarFotoSOAP(con: docBase64, nombre: docAdjunto){(error) in
            if error == nil {
                self.iniPasoDos()
            }else{
                self.hideActivityIndicator()
                Toolbox.showAlert(with: "Alerta", and: "Vuelva a intentarlo", in: self)
            }
        }
    }
    func iniPasoDos(){
        let coincidencia = notificationMorosidad.coincidencia
        let dicConcepto = arrConceptoFinal.object(at: indConceptoSeleccionado) as! [String: Any]
        let conceptoDeudaId = String(dicConcepto["CTConDeuId"]! as! Int)
        notificationMorosidad.conceptoDeudaId = conceptoDeudaId
        notificationMorosidad.conceptoDeudaDesc = arrConceptoTitulo.object(at: indConceptoSeleccionado) as! String
        let monto = montoTX.text!
        notificationMorosidad.monto = monto
        let monedaDeu = indMonedaSeleccionado == 0 ? "PEN" : "US"
        notificationMorosidad.moneda = monedaDeu
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strFecEmision = dateFormatter.string(from: dateFecEmision!)
        notificationMorosidad.fechaEmision = strFecEmision
        notificationMorosidad.dateFecEmision = dateFecEmision!
        let dicTipo = arrTipoFinal.object(at: indTipoDocSeleccionado) as! [String: Any]
        let tipoDocumentoId = dicTipo["CTTipDocProb"]! as! String
        notificationMorosidad.tipoDocumento = tipoDocumentoId
        notificationMorosidad.documentoAdjunto = docAdjunto
        let strFecVencimiento = dateFormatter.string(from: dateFecVencimiento!)
        notificationMorosidad.fechaVencimiento = strFecVencimiento
        notificationMorosidad.dateFecVencimiento = dateFecVencimiento!
        let parametros = [
            "CTUseCod" :  user.user!,
            "CTConDeuId" : conceptoDeudaId,
            "CTTDocCPT" : coincidencia.tipoDoc,
            "CTNDocCPT" : coincidencia.numDoc,
            "CTTDocAcre": notificationMorosidad.tipoDocRemitente,
            "CTNDocAcre": notificationMorosidad.nroDocRemitente,
            "CTNCorr" : notificationMorosidad.correlativo,
            "CTPasoReg" : "2",
            "CTDirecc" : notificationMorosidad.direccion,
            "CTMail" :  notificationMorosidad.correo,
            "CTCelular" :  notificationMorosidad.celular,
            "CTMonDeu" :  monto,
            "CTMonedaDeu" : monedaDeu,
            "CTFecEmi" : strFecEmision,
            "CTTipDocProb" : tipoDocumentoId,
            "CTDocProb" : docAdjunto,
            "CTFecVen" :  strFecVencimiento,
            "CTFecPlLim" :  "",
            "CTComent" :  "",
            "CTTipConAce" :  "0",
            "SesionId" :  user.SesionId!,
            "CTNDocEmp" :  "",
            "origenAplicacion" : "\(PATHS.APP_ID)"
            ] as [String : Any]
        NotificationCenter.default.addObserver(self, selector: #selector(self.endPasoDos(_:)), name: NSNotification.Name("endPasoDos"), object: nil)
        OriginData.sharedInstance.registroDeudor(notification: "endPasoDos", parametros: parametros)
    }
    @objc func endPasoDos(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard let data = notification.object as? TerceroClass else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        if data.CodigoWS == "0"{
            performSegue(withIdentifier: "showPaso3", sender: nil)
        }else{
            if data.codigoWS != nil {
                if data.codigoWS == "99" {
                    showError99()
                }else {
                    self.iniMensajeError(codigo: data.codigoWS!)
                }
            }else{
                let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                self.present(alerta, animated: true, completion: nil)
            }
        }
    }
    //MARK: - ImagePicker
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker:UIImagePickerController, didFinishPickingMediaWithInfo info:[String:Any]) {
        if let imageC = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let usuario = user.user!
            let coincidencia = notificationMorosidad.coincidencia
            let nroDoc = coincidencia.numDoc
            let tipoDoc = coincidencia.tipoDoc
            docAdjunto = "\(usuario)_\(tipoDoc)_\(nroDoc)_\(notificationMorosidad.correlativo)_1.jpg"
            docBase64 = UIImageJPEGRepresentation(imageC, 0.7)!.base64EncodedString()
            detalleDocumentoAdjuntoLB.text = docAdjunto
            docAdjuntoBT.setImage(UIImage(named: "eliminaradjunto-icon2"), for: .normal)
        }
        self.dismiss(animated: true, completion: nil)
    }
    //MARK: - Combo
    func miPickerCombo(_ pickerid:Int, didSelectRow row:Int){
        switch pickerid {
            case 1:
                indConceptoSeleccionado = row
                break
            case 2:
                indMonedaSeleccionado = row
                break
            case 3:
                indTipoDocSeleccionado = row
                break
            default:
                break
        }
    }
    func miPickerCombo(_ MiPickerView:CTComboPicker!, pickerView picker: UIPickerView!, didSelectRow row: Int, inComponent component:Int){}
    //MARK: - Fecha
    func fechaPickerConfirmo(con fecha:Date, con opcion:Int){
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es_ES")
        dateFormatter.dateFormat = "    dd/    MMMM/    yyyy"
        let arrFecha = dateFormatter.string(from: fecha).split(separator: "/").map(){String($0)}
        if opcion == 1 {
            dateFecEmision = fecha
            dia1.setTitle(arrFecha[0], for: .normal)
            mes1.setTitle(arrFecha[1].capitalized, for: .normal)
            anio1.setTitle(arrFecha[2], for: .normal)
        }else if opcion == 2 {
            dateFecVencimiento = fecha
            dia2.setTitle(arrFecha[0], for: .normal)
            mes2.setTitle(arrFecha[1].capitalized, for: .normal)
            anio2.setTitle(arrFecha[2], for: .normal)
        }
    }
    //MARK: - TextField
    func textField(_ textField:UITextField, shouldChangeCharactersIn range:NSRange, replacementString string:String) -> Bool {
        let montoTemp = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if let firstPoint = montoTemp.firstIndex(of: "."), let lastPoint = montoTemp.lastIndex(of: ".") {
            if firstPoint != lastPoint {
                return false
            }else if montoTemp.suffix(from: firstPoint).count > 3 {
                    return false
            }
        }
        return true
    }
}
