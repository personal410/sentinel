//
//  DiscreparAvisoCobranzaViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 2/27/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import Photos
class DiscreparAvisoCobranzaViewController:ParentViewController, UINavigationControllerDelegate {
    //MARK: - Outlets
    @IBOutlet weak var tvDescargo:UITextView!
    @IBOutlet weak var btnAdjuntar:UIButton!
    //MARK: - Props
    var dicAvisoCobranza:[String: Any] = [:]
    var imgAdjunta:UIImage? {
        didSet{
            btnAdjuntar.setImage(UIImage(named: imgAdjunta == nil ? "adjuntar-icon2" : "eliminaradjunto-icon2"), for: .normal)
        }
    }
    lazy var imagePicker:UIImagePickerController = {
        let picker = UIImagePickerController()
        picker.delegate = self
        return picker
    }()
    var indice = 0
    var delegate:DiscreparDelegate?
    //MARK: - ViewCont
    override func viewDidLayoutSubviews(){
        if UserGlobalData.sharedInstance.userGlobal!.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(inNavBar: navBar)
        }
    }
    //MARK: - Actions
    @IBAction func atras(){
        navigationController?.popViewController(animated: true)
    }
    @IBAction func ocultarTeclado(){
        tvDescargo.resignFirstResponder()
    }
    @IBAction func agregarFoto(){
        if imgAdjunta == nil {
            let alertCont = UIAlertController(title: "Elegir fuente", message: nil, preferredStyle: .actionSheet)
            alertCont.addAction(UIAlertAction(title: "Tomar foto", style: .default, handler: { action in
                self.abrirFuente(con: .camera)
            }))
            alertCont.addAction(UIAlertAction(title: "Abrir libreria fotos", style: .default, handler: { action in
                self.abrirFuente(con: .photoLibrary)
            }))
            alertCont.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            present(alertCont, animated: true, completion: nil)
        }else{
            let alertCont = UIAlertController(title: "Alerta", message: "¿Desea eliminar la foto adjunta?", preferredStyle: .alert)
            alertCont.addAction(UIAlertAction(title: "Si", style: .default, handler: { (action) in
                self.imgAdjunta = nil
            }))
            alertCont.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            present(alertCont, animated: true, completion: nil)
        }
    }
    @IBAction func discrepar(){
        let descargo = tvDescargo.text!
        if descargo.isEmpty {
            Toolbox.showAlert(with: "Alerta", and: "Debe ingresar un descargo.", in: self)
            return
        }
        guard let image = imgAdjunta else {
            Toolbox.showAlert(with: "Alerta", and: "Debe ingresar una foto antes de enviar la discrepancia.", in: self)
            return
        }
        let usuarioBean = UserGlobalData.sharedInstance.userGlobal!
        let tipoDoc = dicAvisoCobranza["CTTDocCPT"] as! String
        let nroDoc = dicAvisoCobranza["CTNDocCPT"] as! String
        let correlativo = dicAvisoCobranza["CTNCorr"] as! Int
        let useCode = dicAvisoCobranza["CTUseCod"] as! String
        let usuario = usuarioBean.user!
        let caso = 3
        let nombreFoto = "\(usuario)_\(tipoDoc)_\(nroDoc)_\(correlativo)_\(caso).png"
        let imgBase64 = UIImageJPEGRepresentation(image, 0.7)
        let strBase64 = imgBase64!.base64EncodedString()
        showActivityIndicator()
        Toolbox.enviarFotoSOAP(con: strBase64, nombre: nombreFoto){(error) in
            if error == nil {
                let request = EnviarDiscrepanciaRequest(useCod: useCode, tipoDoc: tipoDoc, nroDoc: nroDoc, correlativo: correlativo, sesionId: usuarioBean.SesionId!, usuario: usuario, archivo: nombreFoto, discrepancia: descargo)
                let data = try! JSONEncoder().encode(request)
                ServiceConnector.connectToUrl("\(PATHS.PATHSENTINEL)RWS_MSCTRegDiscrep", params: data, response:{(result, error) in
                    self.hideActivityIndicator()
                    guard let dic = result as? [String: Any], let codigoWs = dic["CodigoWS"] as? String else {
                        let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                        alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                        self.present(alerta, animated: true, completion: nil)
                        return
                    }
                    if codigoWs == "0" {
                        Toolbox.showAlert(with: "SENTINEL", and: "Su discrepancia ha sido enviada satisfactoriamente.", withBtnTitle: "Aceptar", withOkAction: {() in
                            self.delegate?.discrepoCorrectamente(con: self.indice)
                            self.atras()
                        }, in: self)
                    }else if codigoWs == "99" {
                        self.showError99()
                    }else{
                        self.iniMensajeError(codigo: codigoWs)
                    }
                })
            }else{
                self.hideActivityIndicator()
                Toolbox.showAlert(with: "Alerta", and: "Vuelva a intentarlo", in: self)
            }
        }
        
    }
    //MARK: - Auxiliar
    func abrirFuente(con tipo:UIImagePickerControllerSourceType){
        imagePicker.sourceType = tipo
        present(imagePicker, animated: true, completion: nil)
    }
}
extension DiscreparAvisoCobranzaViewController:UIImagePickerControllerDelegate {
    func imagePickerController(_ picker:UIImagePickerController, didFinishPickingMediaWithInfo info:[String : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        imgAdjunta = info[UIImagePickerControllerOriginalImage] as? UIImage
    }
}
protocol DiscreparDelegate {
    func discrepoCorrectamente(con indice:Int)
}
