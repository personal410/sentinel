//
//  AvisoCobranzaTableViewCell.swift
//  Sentinel
//
//  Created by Victor Salazar on 2/27/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import UIKit
class AvisoCobranzaTableViewCell:UITableViewCell {
    @IBOutlet weak var lblApellidos:UILabel!
    @IBOutlet weak var lblNombres:UILabel!
    @IBOutlet weak var lblDeuda:UILabel!
    @IBOutlet weak var btnVerDetalle:UIButton!
    @IBOutlet weak var btnDiscrepar:UIButton!
    
    func bind(_ dic:[String: Any], at index:Int){
        lblApellidos.text = "\(dic["CTUseApePat"] as? String ?? "") \(dic["CTUseApeMat"] as? String ?? "")"
        lblNombres.text = dic["CTUseNom"] as? String
        var moneda = ""
        if let monedaDeu = dic["CTMonedaDeu"] as? String {
            if monedaDeu == "PEN" {
                moneda = "S/"
            }else if monedaDeu == "USD" {
                moneda = "$"
            }else if monedaDeu == "EUR" {
                moneda = "€"
            }
        }
        var deuda = 0.0
        if let monDeu = dic["CTMonDeu"] as? NSString {
            deuda = monDeu.doubleValue
        }
        lblDeuda.text = "\(moneda) \(Toolbox.getNumberFormmatter().string(from: NSNumber(value: deuda))!)"
        btnVerDetalle.tag = index
        btnDiscrepar.tag = index
        let puedeDiscrepar = (dic["CTFlgDiscrep"] as! String) != "S"
        btnDiscrepar.isEnabled = puedeDiscrepar
        btnDiscrepar.alpha = puedeDiscrepar ? 1 : 0.5
    }
}
