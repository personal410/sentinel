//
//  RecuperaRemitenteViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 24/6/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
class RecuperaRemitenteViewController:ParentViewController, UITableViewDataSource, UITableViewDelegate {
    //MARK: - Outlets
    @IBOutlet weak var lblCantDisponibles:UILabel!
    @IBOutlet weak var tvRemitentes:UITableView!
    @IBOutlet weak var lblNoRepresentanteLegal:UILabel!
    //MARK: - Props
    let usuarioBean = UserGlobalData.sharedInstance.userGlobal!
    var codServicio = ""
    var coincidencia:Coincidencia!
    var arrRemitentes:[[String: Any]] = []
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        showActivityIndicator()
        if codServicio != "0" {
            let dicParams = ["Usuario": usuarioBean.user!, "SesionId": usuarioBean.SesionId!, "Servicio": codServicio]
            ServiceConnector.connectToUrl("\(PATHS.PATHSENTINEL)RWS_MSDatosServicio", params: dicParams){ (result, error) in
                self.hideActivityIndicator()
                if let dic = result as? [String: Any] {
                    if let codigoWs = dic["CodigoWS"] as? String {
                        if codigoWs == "0" {
                            if let saldoServicio = dic["SaldoServicio"] as? [String:Any] {
                                if let dispDet = saldoServicio["DispDet"] as? String {
                                    self.lblCantDisponibles.text = "Disponible: \(dispDet)"
                                }
                            }
                            self.buscarEmpresas()
                        }else if codigoWs == "99" {
                            self.showError99()
                        }else{
                            self.iniMensajeError(codigo: codigoWs)
                        }
                    }
                }
            }
        }else{
            buscarEmpresas()
        }
    }
    override func viewDidLayoutSubviews(){
        if usuarioBean.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(inNavBar: navBar)
        }
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? NotificarPaso1ViewController {
            let dic = arrRemitentes[tvRemitentes.indexPathForSelectedRow!.row]
            let notificacionMorosidad = NotificacionMorosidad()
            notificacionMorosidad.tipoDocRemitente = dic["TDocEmp"] as? String ?? ""
            notificacionMorosidad.nroDocRemitente = dic["NDocEmp"] as? String ?? ""
            notificacionMorosidad.nombreRemitente = dic["NomEmp"] as? String ?? ""
            notificacionMorosidad.coincidencia = coincidencia
            viewCont.notificacionMorosidad = notificacionMorosidad
        }
    }
    //MARK: - Actions
    @IBAction func atras(){
        navigationController?.popViewController(animated: true)
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arrRemitentes.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "remitenteCell", for: indexPath) as! RecuRemitenteTableViewCell
        cell.bind(dic: arrRemitentes[indexPath.row])
        return cell
    }
    //MARK: - Auxiliar
    func buscarEmpresas(){
        let dicParams = ["Usuario": usuarioBean.user!, "SesionId": usuarioBean.SesionId!, "UsuTipDoc": usuarioBean.TDocusu!]
        ServiceConnector.connectToUrl("\(PATHS.PATHSENTINEL)RWS_MSMisEmpresas", params: dicParams){ (result, error) in
            self.hideActivityIndicator()
            guard error == nil, let dic = result as? [String: Any], let codigoWs = dic["CodigoWS"] as? String, let arr = dic["MisEmpresas"] as? [[String: Any]] else {
                Toolbox.showAlert(with: "ALERTA", and: "En estos momentos no es posible realizar la consulta.", in: self)
                return
            }
            if codigoWs == "0" {
                self.arrRemitentes.append(["TDocEmp": self.usuarioBean.TDocusu!, "NDocEmp": self.usuarioBean.user!, "NomEmp": "\(self.usuarioBean.Nombres!) \(self.usuarioBean.ApePat!) \(self.usuarioBean.ApeMat!)"])
                self.arrRemitentes.append(contentsOf: arr)
                self.lblNoRepresentanteLegal.isHidden = !arr.isEmpty
                self.tvRemitentes.reloadData()
            }else if codigoWs == "99" {
                self.showError99()
            }else{
                self.iniMensajeError(codigo: codigoWs)
            }
        }
    }
}
