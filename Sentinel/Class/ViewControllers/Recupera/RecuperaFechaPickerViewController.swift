//
//  RecuperaFechaPickerViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 6/2/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import UIKit

class RecuperaFechaData{
    var opcion = 0
    var minDate:Date?
    var maxDate:Date?
}

protocol RecuperaFechaPickerVCDelegate {
    func fechaPickerConfirmo(con fecha:Date, con opcion:Int)
}

class RecuperaFechaPickerViewController:ParentViewController {
    //MARK: - Props
    var delegate:RecuperaFechaPickerVCDelegate?
    var opcion = 0
    var minDate:Date?
    var maxDate:Date?
    var descripcion:String?
    //MARK: - Outlets
    @IBOutlet weak var lblDescripcion:UILabel!
    @IBOutlet weak var lblFecha:UILabel!
    @IBOutlet weak var dpFecha:UIDatePicker!
    //MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
        dpFecha.minimumDate = minDate
        dpFecha.maximumDate = maxDate
        lblDescripcion.text = descripcion
        accionDetalle(0)
    }
    //MARK: - Actions
    @IBAction func accionDetalle(_ sender:Any){
        let date = dpFecha.date
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es_ES")
        dateFormatter.dateFormat = "EEEE"
        let weekday = dateFormatter.string(from: date).capitalized
        dateFormatter.dateFormat = "MMMM"
        let month = dateFormatter.string(from: date).capitalized
        dateFormatter.dateFormat = "'\(weekday)', dd 'de \(month) de' yyyy"
        lblFecha.text = dateFormatter.string(from: date)
    }
    @IBAction func accionEstablecer(_ sender:Any){
        delegate?.fechaPickerConfirmo(con: dpFecha.date, con: opcion)
        accionCancelar(0)
    }
    @IBAction func accionCancelar(_ sender:Any){
        self.navigationController?.popViewController(animated: false)
    }
}
