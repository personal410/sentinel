//
//  NotificacionesEnviadasViewController.swift
//  Sentinel
//
//  Created by Daniel on 25/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class NotificacionesEnviadasViewController:ParentViewController, UITableViewDelegate, UITableViewDataSource {
    //MARK: - Outlets
    @IBOutlet weak var tvNotificaciones:UITableView!
    //MARK: - Props
    let user = UserGlobalData.sharedInstance.userGlobal!
    var arrNotificaciones:[[String: Any]] = []
    var indiceActual = -1
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        showActivityIndicator()
        let dicParams:[String: Any] = ["CTUseCod": user.user!, "SesionId": user.SesionId!]
        let url = "\(PATHS.PATHSENTINEL)\(WEBSERVICES.RECUPERA.RWS_MSCTListadoNotificaciones)"
        NotificationCenter.default.addObserver(self, selector: #selector(endConsultaNotificacionesEnviadas(_:)), name: NSNotification.Name(rawValue: "endConsultaNotificacionesEnviadas"), object: nil)
        OriginData.sharedInstance.consultarUrl(url, con: dicParams, notificar: "endConsultaNotificacionesEnviadas")
    }
    //MARK: - Actions
    @IBAction func marcarPagado(_ btn:UIButton){
        indiceActual = btn.tag
        let alertCont = UIAlertController(title: "ALERTA", message: "¿Desea confirmar el pago de la deuda?", preferredStyle: UIAlertControllerStyle.alert)
        alertCont.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: { (action) in
            let dicNotificacion = self.arrNotificaciones[self.indiceActual]
            let tipoDoc = dicNotificacion["CTTDocCPT"] as? String ?? ""
            let nroDoc = dicNotificacion["CTNDocCPT"] as? String ?? ""
            let correlativo = dicNotificacion["CTNCorr"] as? Int ?? 0
            let dicParams:[String: Any] = ["Usuario": self.user.user!, "SesionId": self.user.SesionId!, "CTTDocCPT": tipoDoc, "CTNDocCPT" : nroDoc, "CTNCorr": correlativo, "CTEstNot": "Y"]
            let url = "\(PATHS.PATHSENTINEL)\(WEBSERVICES.RECUPERA.RWS_MSCTActEstRegistro)"
            self.showActivityIndicator()
            NotificationCenter.default.addObserver(self, selector: #selector(self.endActualizarEstadoNotificacion(_:)), name: NSNotification.Name(rawValue: "endActualizarEstadoNotificacion"), object: nil)
            OriginData.sharedInstance.consultarUrl(url, con: dicParams, notificar: "endActualizarEstadoNotificacion")
        }))
        alertCont.addAction(UIAlertAction(title: "CANCELAR", style: .cancel, handler: nil));
        present(alertCont, animated: true, completion: nil)
    }
    //MARK: - Auxiliar
    @objc func endConsultaNotificacionesEnviadas(_ notification:Notification){
        hideActivityIndicator()
        guard let dic = notification.object as? [String: Any], let codigoWs = dic["CodigoWS"] as? String, let notificaciones = dic["SDT_CTListadoNotificacion"] as? [[String: Any]], codigoWs == "0" else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            present(alerta, animated: true, completion: nil)
            return
        }
        arrNotificaciones = notificaciones
        tvNotificaciones.reloadData()
    }
    @objc func endActualizarEstadoNotificacion(_ notification:Notification){
        hideActivityIndicator()
        guard let dic = notification.object as? [String: Any], let codigoWs = dic["CodigoWS"] as? String, codigoWs == "0" else {
            indiceActual = -1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            present(alerta, animated: true, completion: nil)
            return
        }
        arrNotificaciones[indiceActual]["CTEstNot"] = "Y"
        tvNotificaciones.reloadRows(at: [IndexPath(row: indiceActual, section: 0)], with: .none)
        indiceActual = -1
    }
    //MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let cantNotificaciones = arrNotificaciones.count
        return cantNotificaciones == 0 ? 1 : cantNotificaciones
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if arrNotificaciones.count == 0 {
            return tableView.dequeueReusableCell(withIdentifier: "noNotificacionesCell", for: indexPath)
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificacionEnviadaCell", for: indexPath) as! NotificacionesEnviadasTableViewCell
        cell.bind(with: arrNotificaciones[indexPath.row], in: indexPath.row)
        return cell
    }
}
