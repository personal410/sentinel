//
//  CoincidenciaViewController.swift
//  Sentinel
//
//  Created by Daniel on 25/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class CoincidenciaViewController:ParentViewController, UITableViewDelegate, UITableViewDataSource {
    //MARK: - Outlets
    @IBOutlet weak var lblCantDisponibles:UILabel!
    @IBOutlet weak var lblCantCoincidencias:UILabel!
    @IBOutlet weak var tvCoincidencias:UITableView!
    //MARK: - Props
    let usuarioBean = UserGlobalData.sharedInstance.userGlobal!
    var codServicio = ""
    var arrCoincidenciasFinal:[Coincidencia] = []
    var dicBusqueda:[String:Any] = [:]
    var tipLista = 0
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        if codServicio != "0" {
            showActivityIndicator()
            let dicParams:[String: Any] = ["Usuario": usuarioBean.user!, "SesionId": usuarioBean.SesionId!, "Servicio": codServicio]
            NotificationCenter.default.addObserver(self, selector: #selector(endConsultarCantidad(_:)), name: NSNotification.Name(rawValue: "endConsultarCantidad"), object: nil)
            OriginData.sharedInstance.consultarUrl("\(PATHS.PATHSENTINEL)RWS_MSDatosServicio", con: dicParams, notificar: "endConsultarCantidad")
        }
        tipLista = dicBusqueda["tipLista"] as! Int
        let tipoPersona = dicBusqueda["TipoPersona"] as! String
        let apePat = dicBusqueda["ApePat"] as! String
        let apeMat = dicBusqueda["ApeMat"] as! String
        let nombres = dicBusqueda["Nombres"] as! String
        if tipLista == 1 {
            var tipoDoc = ""
            if tipoPersona == "D" {
                tipoDoc = "DNI"
            }else if tipoPersona == "R" {
                tipoDoc = "RUC"
            }else if tipoPersona == "4" {
                tipoDoc = "CE"
            }
            let nomCompelto = "\(apePat) \(apeMat) \(nombres)"
            let dicPersona:[String: Any] = ["tipodoc": tipoPersona, "TipoDocAbr": tipoDoc, "numdoc": dicBusqueda["NumDocumento"] as Any, "descripcion": nomCompelto, "NroRev": 0]
            let coincidencia = Coincidencia(with: dicPersona)
            arrCoincidenciasFinal.append(coincidencia)
            actualizarCantCoincidencias()
        }else{
            let random = arc4random_uniform(900000) + 100000
            let idSesion = "\(random)"
            let dicParamsBusqueda:[String: Any] = ["UserId": usuarioBean.user!, "IdSession": idSesion, "TipoPersona": tipoPersona, "Paterno": apePat, "Materno": apeMat, "nombres": nombres, "UseSeIDSession": usuarioBean.SesionId!]
            NotificationCenter.default.addObserver(self, selector: #selector(endConsultarBusqPorNombre(_:)), name: NSNotification.Name(rawValue: "endConsultarPorNombre"), object: nil)
            OriginData.sharedInstance.consultarUrl("\(PATHS.PATH)\(WEBSERVICES.TERCEROS.RWSBusXNomCT)", con: dicParamsBusqueda, notificar: "endConsultarPorNombre")
        }
    }
    override func viewDidLayoutSubviews(){
        if usuarioBean.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(inNavBar: navBar)
        }
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? RecuperaRemitenteViewController {
            if let c = sender as? Coincidencia {
                viewCont.coincidencia = c
                viewCont.codServicio = codServicio
            }
        }
    }
    //MARK: - Actions
    @IBAction func atras(){
        navigationController?.popViewController(animated: true)
    }
    @IBAction func notificarMoroso(_ btn:UIButton){
        let idx = btn.tag
        let coincidencia = arrCoincidenciasFinal[idx]
        let message = "¿Desea notificar a \(coincidencia.numDoc) \(coincidencia.descripcion)?"
        let alertCont = UIAlertController(title: "Alerta", message: message, preferredStyle: .alert)
        alertCont.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action) in
            self.performSegue(withIdentifier: "showRemitente", sender: coincidencia)
        }))
        alertCont.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        present(alertCont, animated: true, completion: nil)
    }
    //MARK: - Auxiliar
    @objc func endConsultarCantidad(_ notification:Notification){
        if tipLista == 1 {
            hideActivityIndicator()
        }
        guard let dic = notification.object as? [String:Any] else {
            return
        }
        if let saldoServicio = dic["SaldoServicio"] as? [String:Any] {
            if let dispDet = saldoServicio["DispDet"] as? String {
                lblCantDisponibles.text = "Disponible: \(dispDet)"
            }
        }
    }
    @objc func endConsultarBusqPorNombre(_ noti:Notification){
        hideActivityIndicator()
        NotificationCenter.default.removeObserver(self, name: noti.name, object: nil)
        if var dic = noti.object as? [String:Any], let codigoWs = dic["CodigoWS"] as? String {
            if codigoWs == "0" {
                if let arr = dic["SDTBusAlf"] as? [[String: Any]] {
                    arrCoincidenciasFinal = Coincidencia.transformList(arr)
                    tvCoincidencias.reloadData()
                    actualizarCantCoincidencias()
                    return
                }
            }else if codigoWs == "99" {
                showError99()
                return
            }else{
                iniMensajeError(codigo: codigoWs)
                return
            }
        }
        let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
        alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
        present(alerta, animated: true, completion: nil)
    }
    func actualizarCantCoincidencias(){
        let cantCoincidencias = arrCoincidenciasFinal.count
        if cantCoincidencias == 0 {
            lblCantCoincidencias.text = "No se encontraron coincidencias."
        }else{
            if cantCoincidencias > 100 {
                lblCantCoincidencias.text = "Hay más de 100 coincidencias encontradas."
            }else{
                let numero = cantCoincidencias == 1 ? "" : "s"
                lblCantCoincidencias.text = "Hay \(cantCoincidencias) coincidencia\(numero) encontrada\(numero)."
            }
        }
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arrCoincidenciasFinal.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "coincidenciaCell", for: indexPath) as! CoincidenciaTableViewCell
        let idx = indexPath.row
        cell.bindWith(coincidencia: arrCoincidenciasFinal[idx], atIndex: idx)
        return cell
    }
}
