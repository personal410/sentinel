//
//  CoincidenciaTableViewCell.swift
//  Sentinel
//
//  Created by Richy on 25/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class CoincidenciaTableViewCell:UITableViewCell {
    @IBOutlet weak var lblNombre:UILabel!
    @IBOutlet weak var vBarra:UIView!
    @IBOutlet weak var lblDocumento:UILabel!
    @IBOutlet weak var stckRevisar:UIStackView!
    @IBOutlet weak var lblVecesBuscado:UILabel!
    @IBOutlet weak var lblVecesDesc:UILabel!
    @IBOutlet weak var btnNotificarMoroso:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let bounds = vBarra.bounds
        let maskPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: ([.topRight, .bottomRight]), cornerRadii: CGSize(width: 8.0, height: 8.0))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = maskPath.cgPath
        vBarra.layer.mask = maskLayer
    }

    func bindWith(coincidencia:Coincidencia, atIndex idx:Int){
        lblNombre.text = coincidencia.descripcion
        lblDocumento.text = "\(coincidencia.tipoDocAbr) \(coincidencia.numDoc)"
        let nroRev = coincidencia.nroRev
        if nroRev == 0 {
            stckRevisar.isHidden = true
        }else{
            stckRevisar.isHidden = false
            lblVecesBuscado.text = "\(nroRev)"
            lblVecesDesc.text = "Ve\(nroRev == 1 ? "z" : "ces") revisado este perfil en los últimos 3 meses."
        }
        btnNotificarMoroso.tag = idx
    }
}
