//
//  RECBusqRepresentanteLegalVC.swift
//  Sentinel
//
//  Created by Victor Salazar on 3/7/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
class RECBusqRepLegalTipoVC:ParentViewController {
    //MARK: - Props
    let userData = UserGlobalData.sharedInstance.userGlobal!
    //MARK: - ViewCont
    override func viewDidLayoutSubviews(){
        super.viewDidLoad()
        if userData.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(inNavBar: navBar)
        }
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let btn = sender as? UIButton, let viewCont = segue.destination as? RECBusqRepLegalVC {
            viewCont.tipo = btn.tag == 0 ? .documento : .nombre
        }
    }
    //MARK: - Actions
    @IBAction func atras(){
        navigationController?.popViewController(animated: true)
    }
}
