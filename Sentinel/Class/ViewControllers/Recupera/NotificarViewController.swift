//
//  NotificarViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 2/27/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import UIKit
class NotificarViewController:ParentViewController {
    //MARK: - Props
    var codServicio = ""
    var cantNotificaciones = 0
    var cantDisponible = 0
    let usuarioBean = UserGlobalData.sharedInstance.userGlobal!
    var arrAvisosCobranza:[[String: Any]] = []
    //MARK: - Outlets
    @IBOutlet weak var lblCantDisponibles:UILabel!
    @IBOutlet weak var lblNumDocumento:UILabel!
    @IBOutlet weak var lblNombreEmpresa:UILabel!
    @IBOutlet weak var lblAvisosCobranza:UILabel!
    //MARK: - ViewCont
    override func viewWillAppear(_ animated:Bool){
        super.viewWillAppear(animated)
        let userData = UserGlobalData.sharedInstance.userGlobal!
        if userData.EsPremium == "S" {
            lblNumDocumento.textColor = UIColor(hex: "D18E18")
            lblNombreEmpresa.textColor = UIColor(hex: "D18E18")
        }
        showActivityIndicator()
        if codServicio != "0" {
            let dicParams = ["Usuario": usuarioBean.user!, "SesionId": usuarioBean.SesionId!, "Servicio": codServicio]
            ServiceConnector.connectToUrl("\(PATHS.PATHSENTINEL)RWS_MSDatosServicio", params: dicParams){(r, e) in
                if let dic = r as? [String: Any] {
                    if let saldoServicio = dic["SaldoServicio"] as? [String:Any] {
                        if let dispDet = saldoServicio["DispDet"] as? String {
                            self.cantDisponible = Int(dispDet) ?? 0
                            self.lblCantDisponibles.text = "Disponible: \(self.cantDisponible)"
                        }
                    }
                }
                self.cargarAvisosCobranza()
            }
        }else{
            cargarAvisosCobranza()
        }
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? RecuperarBusquedaViewController {
            let tag = (sender as! UIButton).tag
            viewCont.tipo = tag == 0 ? .documento : .nombre
            viewCont.codServicio = codServicio
        }else if let viewCont = segue.destination as? AvisosCobranzaViewController {
            viewCont.arrAvisosCobranza = arrAvisosCobranza
        }
    }
    override func shouldPerformSegue(withIdentifier identifier:String, sender:Any?) -> Bool {
        if identifier == "showAvisos" && cantNotificaciones == 0 {
            let alertCont = UIAlertController(title: PATHS.SENTINEL, message: "No tiene avisos de cobranza.", preferredStyle: .alert)
            alertCont.addAction(UIAlertAction(title: "ACEPTAR", style: .default, handler: nil))
            self.present(alertCont, animated: true, completion: nil)
            return false
        }
        if identifier == "showBusqueda" {
            #if DEBUG
                return true
            #else
                if cantDisponible == 0 {
                    mostrarComprarRecupera()
                    return false
                }
            #endif
        }
        return true
    }
    //MARK: - Actions
    @IBAction func comprar(){
        mostrarComprarRecupera()
    }
    //MARK: - Auxiliar
    func mostrarComprarRecupera(){
        let recuperaCompraVC = UIStoryboard(name: "Compra", bundle: nil).instantiateViewController(withIdentifier: "RecuperaCompraVC")
        parent?.navigationController?.pushViewController(recuperaCompraVC, animated: true)
    }
    func cargarAvisosCobranza(){
        let dicParams = ["CTTDocCPT": self.usuarioBean.TDocusu! , "CTNDocCPT": self.usuarioBean.user!, "SesionId": self.usuarioBean.SesionId!]
        ServiceConnector.connectToUrl("\(PATHS.PATHSENTINEL)RWS_MSCTAvisoCobranza", params: dicParams, response:{(r, e) in
            self.hideActivityIndicator()
            if let dic = r as? [String: Any] {
                if let codigoWs = dic["CodigoWS"] as? String {
                    if codigoWs == "0"{
                        if let array = dic["SDT_CTAvisoCobranza"] as? [[String: Any]] {
                            self.arrAvisosCobranza = array
                            self.cantNotificaciones = self.arrAvisosCobranza.count
                            if self.cantNotificaciones > 0 {
                                self.lblAvisosCobranza.text = "Avisos de Cobranza (\(self.cantNotificaciones))"
                            }
                            return
                        }
                    }else if codigoWs == "99" {
                        self.showError99()
                        return
                    }else{
                        self.iniMensajeError(codigo: codigoWs)
                        return
                    }
                }
            }
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
        })
    }
}
