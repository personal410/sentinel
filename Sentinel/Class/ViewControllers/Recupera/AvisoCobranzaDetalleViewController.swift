//
//  AvisoCobranzaDetalleViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 2/27/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
class AvisoCobranzaDetalleViewController:ParentViewController {
    //MARK: - Outlets
    @IBOutlet weak var lblFechaActual:UILabel!
    @IBOutlet weak var lblNombreReceptor:UILabel!
    @IBOutlet weak var lblDireccion:UILabel!
    @IBOutlet weak var lblNombreEmisor:UILabel!
    @IBOutlet weak var lblConceptoDeuda:UILabel!
    @IBOutlet weak var lblMontoDeuda:UILabel!
    @IBOutlet weak var lblMonedaDeuda:UILabel!
    @IBOutlet weak var lblFecVencimiento:UILabel!
    @IBOutlet weak var lblTelefono:UILabel!
    //MARK: - Attributes
    var dicAvisoCobranza:[String: Any] = [:]
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        let usuarioBean = UserGlobalData.sharedInstance.userGlobal!
        lblFechaActual.text = Toolbox.getDate(date: Date(), withFormat: "dd 'de ' MMMM 'del' yyyy")
        lblNombreReceptor.text = "\(usuarioBean.ApePat!) \(usuarioBean.ApeMat!) \(usuarioBean.Nombres!)"
        let remitente = "\(dicAvisoCobranza["CTUseNom"] as! String) \(dicAvisoCobranza["CTUseApePat"] as! String) \(dicAvisoCobranza["CTUseApeMat"] as! String)"
        let attrsStringUsuario = NSMutableAttributedString(string: "Nos es grato dirigirnos a Ud. por encargo de nuestro cliente \(remitente) quién nos ha solicitado registrarlo en nuestra Central de Riesgos SENTINEL, por una deuda vencida que mantiene con el mismo, la cual se detalla a continuación:")
        attrsStringUsuario.addAttribute(.font, value: UIFont(name: "Roboto-Bold", size: 13)!, range: NSRange(location: 61, length: remitente.count))
        lblNombreEmisor.attributedText = attrsStringUsuario
        
        let celular = dicAvisoCobranza["CTCelular"] as? String ?? ""
        let attrsStringTelefono = NSMutableAttributedString(string: "Si requiere mayor información sobre el presente, puede comunicarse a los teléfonos de nuestros cliente \(celular).")
        attrsStringTelefono.addAttribute(.font, value: UIFont(name: "Roboto-Bold", size: 13)!, range: NSRange(location: 103, length: celular.count))
        lblTelefono.attributedText = attrsStringTelefono
        
        let dicParams:[String: Any] = ["UserId": usuarioBean.user!, "CTUseCod": dicAvisoCobranza["CTUseCod"]!, "CTTDocCPT": dicAvisoCobranza["CTTDocCPT"]!, "CTNDocCPT": dicAvisoCobranza["CTNDocCPT"]!, "CTNCorr": dicAvisoCobranza["CTNCorr"]!, "SesionId": usuarioBean.SesionId!]
        NotificationCenter.default.addObserver(self, selector: #selector(endDetalleAvisoCobranza(_:)), name: NSNotification.Name("endDetalleAvisoCobranza"), object: nil)
        showActivityIndicator()
        OriginData.sharedInstance.consultarUrl("\(PATHS.PATHSENTINEL)RWS_MSCTConsultaNotificacion", con: dicParams, notificar: "endDetalleAvisoCobranza")
    }
    override func viewDidLayoutSubviews(){
        if UserGlobalData.sharedInstance.userGlobal!.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(inNavBar: navBar)
        }
    }
    //MARK: - Auxiliar
    @objc func endDetalleAvisoCobranza(_ notification:Notification){
        hideActivityIndicator()
        guard let dic = notification.object as? [String:Any], let codigoWs = dic["CodigoWS"] as? String, let dicData = dic["SDT_CTConsultaNotificacion"] as? [String: Any] else {
            Toolbox.showAlert(with: "ALERTA", and: "En estos momentos no es posible realizar la consulta.", in: self)
            return
        }
        if codigoWs == "0" {
            lblConceptoDeuda.text = dicData["CTConDeuDsc"] as? String
            if let fechaVecicimiento = dicData["CTFecVen"] as? String {
                lblFecVencimiento.text = fechaVecicimiento.split(separator: "-").reversed().joined(separator: "/")
            }
            var moneda = ""
            if let monedaDeu = dicData["CTMonedaDeu"] as? String {
                if monedaDeu == "PEN" {
                    moneda = "soles"
                }else if monedaDeu == "USD" {
                    moneda = "dólares"
                }else if monedaDeu == "EUR" {
                    moneda = "euros"
                }
            }
            var deuda = 0.0
            if let monDeu = dicData["CTMonDeu"] as? NSString {
                deuda = monDeu.doubleValue
            }
            lblMontoDeuda.text = Toolbox.getNumberFormmatter().string(from: NSNumber(value: deuda))!
            lblMonedaDeuda.text = moneda
            lblDireccion.text = dicData["CTDirecc"] as? String
        }else if codigoWs == "99" {
            showError99()
        }else{
            iniMensajeError(codigo: codigoWs)
        }
    }
    //MARK: - Actions
    @IBAction func atras(){
        navigationController?.popViewController(animated: true)
    }
}
