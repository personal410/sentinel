//
//  NotificarPaso3ViewController.swift
//  Sentinel
//
//  Created by Daniel on 27/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class NotificarPaso3ViewController:ParentViewController, RecuperaFechaPickerVCDelegate, UITextViewDelegate {
    //MARK: - Outlets
    @IBOutlet weak var barraVW:UIView!
    @IBOutlet weak var daisRetraso:UILabel!
    @IBOutlet weak var limiteDias:UILabel!
    @IBOutlet weak var dia1:CornerSecondButton!
    @IBOutlet weak var mes1:CornerSecondButton!
    @IBOutlet weak var anio1:CornerSecondButton!
    @IBOutlet weak var comentariosTX:UITextView!
    @IBOutlet weak var lblTerminosCondiciones:UILabel!
    //MARK: - Props
    var notificacionMorosidad:NotificacionMorosidad!
    var parFlg = ""
    var parCon = ""
    var fecha = ""
    var hora = ""
    var dateFechaLimite:Date?
    var aceptoTyC = false
    let user = UserGlobalData.sharedInstance.userGlobal!
    //MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
        estilos()
        gestureScreen()
        iniLimite()
    }
    override func viewDidLayoutSubviews(){
        if user.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(inNavBar: navBar)
        }
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? RecuperaFechaPickerViewController, let data = sender as? RecuperaFechaData {
            viewCont.minDate = data.minDate
            viewCont.delegate = self
        }else if let viewCont = segue.destination as? NotificarPaso4ViewController {
            viewCont.notificacionMorosidad = notificacionMorosidad
        }
    }
    //MARK: - Auxiliar
    func estilos(){
        let bounds = barraVW.bounds
        let maskPath = UIBezierPath(roundedRect: bounds,byRoundingCorners: ([.topRight, .bottomRight]), cornerRadii: CGSize(width: 8.0, height: 8.0))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = maskPath.cgPath
        barraVW.layer.mask = maskLayer
    }
    func gestureScreen(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    func iniLimite(){
        showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endLimite(_:)), name: NSNotification.Name("endLimite"), object: nil)
        let parametros = ["SisTip": "Z", "ParCod": "Z781"] as [String : Any]
        OriginData.sharedInstance.devuelveLimite(notification: "endLimite", parametros: parametros)
    }
    @objc func endLimite(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        guard let data = notification.object as? TerceroClass else {
            hideActivityIndicator()
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            present(alerta, animated: true, completion: nil)
            return
        }
        parFlg = data.ParFlg ?? ""
        parCon = data.ParCon ?? ""
        limiteDias.text = "mínimo \(parCon) días."
        iniHora()
    }
    func iniHora(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.endHora(_:)), name: NSNotification.Name("endHora"), object: nil)
        OriginData.sharedInstance.devuelveHora(notification: "endHora", parametros: [:])
    }
    @objc func endHora(_ notification:NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard let data = notification.object as? TerceroClass else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: .alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil));
            present(alerta, animated: true, completion: nil)
            return
        }
        fecha = data.Fecha ?? ""
        hora = data.Hora ?? ""
        let dateComps = Calendar.current.dateComponents([.day], from: notificacionMorosidad.dateFecEmision, to: notificacionMorosidad.dateFecVencimiento)
        daisRetraso.text = " \(dateComps.day!) días"
    }
    func validar() -> Bool {
        if dateFechaLimite == nil {
            showAlert(PATHS.SENTINEL, mensaje: "Debe seleccionar la fecha límite de pago.")
            return false
        }
        if !aceptoTyC {
            showAlert(PATHS.SENTINEL, mensaje: "Debe aceptar los términos y condiciones para continuar.")
            return false
        }
        return true
    }
    
    func iniPasoTres(){
        showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endPasoTres(_:)), name: NSNotification.Name("endPasoTres"), object: nil)
        let coincidencia = notificacionMorosidad.coincidencia
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strFechaLimite = dateFormatter.string(from: dateFechaLimite!)
        notificacionMorosidad.fechaLimite = strFechaLimite
        let comentarios = comentariosTX.text!
        notificacionMorosidad.comentarios = comentarios
        let parametros = [
            "CTUseCod" :  user.user!,
            "CTConDeuId" : notificacionMorosidad.conceptoDeudaId,
            "CTTDocCPT" : coincidencia.tipoDoc,
            "CTNDocCPT" : coincidencia.numDoc,
            "CTTDocAcre": notificacionMorosidad.tipoDocRemitente,
            "CTNDocAcre": notificacionMorosidad.nroDocRemitente,
            "CTNCorr" : notificacionMorosidad.correlativo,
            "CTPasoReg" : "3",
            "CTDirecc" :  notificacionMorosidad.direccion,
            "CTMail" :  notificacionMorosidad.correo,
            "CTCelular" :  notificacionMorosidad.celular,
            "CTMonDeu" :  notificacionMorosidad.monto,
            "CTMonedaDeu" :  notificacionMorosidad.moneda,
            "CTFecEmi" :  notificacionMorosidad.fechaEmision,
            "CTTipDocProb" :  notificacionMorosidad.tipoDocumento,
            "CTDocProb" :  notificacionMorosidad.documentoAdjunto,
            "CTFecVen" :  notificacionMorosidad.fechaVencimiento,
            "CTFecPlLim" : strFechaLimite,
            "CTComent" :  comentarios,
            "CTTipConAce" : "1",
            "SesionId" :  user.SesionId!,
            "CTNDocEmp" :  "",
            "origenAplicacion" : "\(PATHS.APP_ID)"
            ] as [String : Any]
        OriginData.sharedInstance.registroDeudor(notification: "endPasoTres", parametros: parametros)
    }
    @objc func endPasoTres(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard let data = notification.object as? TerceroClass, let codigoWS = data.CodigoWS else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: .alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil));
            present(alerta, animated: true, completion: nil)
            return
        }
        if codigoWS == "0" {
            performSegue(withIdentifier: "showPaso4", sender: nil)
        }else{
            if codigoWS == "99" {
                showError99()
            }else{
                iniMensajeError(codigo: codigoWS)
            }
        }
    }
    //MARK: - Actions
    @IBAction func accionAtras(){
        let alertCont = UIAlertController(title: PATHS.SENTINEL, message: "¿Desea cancelar el proceso?", preferredStyle: .alert)
        alertCont.addAction(UIAlertAction(title: "SI", style: .default, handler: { (action: UIAlertAction!) in
            self.navigationController?.popToRootViewController(animated: true)
        }))
        alertCont.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
        present(alertCont, animated: true, completion: nil)
    }
    @IBAction func tapGestureLblTyC(_ tapGesture:UITapGestureRecognizer){
        let range = NSMakeRange(36, 22)
        let didTouch = Toolbox.didSelectText(in: lblTerminosCondiciones, with: tapGesture, in: range)
        if didTouch {
            let viewCont = storyboard!.instantiateViewController(withIdentifier: "TyCRecupera")
            self.parent?.parent?.addChildViewController(viewCont)
            self.parent?.parent?.view.addSubview(viewCont.view)
        }
    }
    @IBAction func accionFechaLimite(){
        let data = RecuperaFechaData()
        let fechaHora = "\(fecha) \(hora)"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let date = dateFormatter.date(from: fechaHora), let dias = Int(parCon) {
            var addComps = DateComponents()
            addComps.day = dias
            data.minDate = Calendar.current.date(byAdding: addComps, to: date)
        }
        performSegue(withIdentifier: "showFecha", sender: data)
    }
    @IBAction func checkAceptarTyC(_ btn:UIButton){
        let acepta = !aceptoTyC
        aceptoTyC = acepta
        btn.setImage(UIImage(named: "check_\(acepta ? "on" : "off")"), for: .normal)
    }
    @IBAction func accionSiguiente(_ sender: Any) {
        if self.validar(){
            self.iniPasoTres()
        }
    }
    //MARK: - FechaPicker
    func fechaPickerConfirmo(con fecha:Date, con opcion:Int){
        dateFechaLimite = fecha
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es_ES")
        dateFormatter.dateFormat = "    dd/    MMMM/    yyyy"
        let arrFecha = dateFormatter.string(from: fecha).split(separator: "/").map(){String($0)}
        dia1.setTitle(arrFecha[0], for: .normal)
        mes1.setTitle(arrFecha[1].capitalized, for: .normal)
        anio1.setTitle(arrFecha[2], for: .normal)
    }
    //MARK: - TextView
    func textView(_ textView:UITextView, shouldInteractWith URL:URL, in characterRange:NSRange, interaction:UITextItemInteraction) -> Bool {
        return false
    }
}
