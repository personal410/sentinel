//
//  AvisoCobranzaViewController.swift
//  Sentinel
//
//  Created by Richy on 27/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class AvisoCobranzaViewController: ParentViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var ListaTV: UITableView!
    
    let dataArr = ["Barrios Shimabukuro"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.ListaTV.rowHeight = 187.0
        
    }

    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArr.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        return cell
    }

}
