//
//  PopUpExperianVC.swift
//  Sentinel
//
//  Created by Victor Salazar on 15/11/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
class PopUpExperianVC:ParentViewController{
    //MARK: - Outlets
    @IBOutlet weak var ivDontShow:UIImageView!
    //MARK: - Actions
    @IBAction func actionClose(){
        view.superview?.isHidden = true
        view.removeFromSuperview()
        removeFromParentViewController()
    }
    @IBAction func actionDontShow(){
        if ivDontShow.tag == 0 {
            ivDontShow.tag = 1
            ivDontShow.image = UIImage(named: "check_on")
            let user = UserGlobalData.sharedInstance.userGlobal!
            let params = ["Usuario": user.user!, "SesionId": user.SesionId!, "Modo": "UPD   ", "OrigenAplicacion": "11"]
            ServiceConnector.connectToUrl("\(PATHS.PATHSENTINEL)RWS_MSPopUpInicial", params: params) { (r, e) in
                if let dic = r as? [String: Any] {
                    print("dic: \(dic)")
                }
            }
        }
    }
}
