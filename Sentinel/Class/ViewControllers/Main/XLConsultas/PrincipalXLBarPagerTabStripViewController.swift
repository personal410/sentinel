//
//  PrincipalXLBarPagerTabStripViewController.swift
//  Sentinel
//
//  Created by Daniel on 3/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import XLPagerTabStrip
class PrincipalXLBarPagerTabStripViewController:ButtonBarPagerTabStripViewController {
    // MARK: - Métodos de ciclo de vida
    override func viewDidLoad(){
        settings.style.buttonBarHeight = 44.0
        settings.style.buttonBarBackgroundColor = UIColor(hex: "06B150")
        settings.style.buttonBarItemBackgroundColor = UIColor.clear
        settings.style.selectedBarBackgroundColor =  UIColor(hex: "ffffff")
        settings.style.buttonBarItemFont = UIFont(name: "HelveticaNeue-Bold", size:12) ?? UIFont.systemFont(ofSize: 12)
        settings.style.selectedBarHeight = 3.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor.clear
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 5
        settings.style.buttonBarRightContentInset = 5
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = UIColor.fromHex(0x808080)
            newCell?.label.textColor = UIColor.fromHex(968266)
            oldCell?.imageView.tintColor = UIColor(white: 1, alpha: 0.5)
            newCell?.imageView.tintColor = UIColor.fromHex(968266)
        }
        super.viewDidLoad()
        self.reloadPagerTabStripView()
    }
    //MARK: - PagerTabStripDataSource
    override func viewControllers(for pagerTabStripController:PagerTabStripViewController) -> [UIViewController] {
        let primerViewController = storyboard!.instantiateViewController(withIdentifier: "PrimerTabConsultaVC")
        let segundoViewController = UIStoryboard(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "HistorialVC")
        return [primerViewController, segundoViewController]
    }
}

extension UIColor{
    static func fromHex(_ rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
}

