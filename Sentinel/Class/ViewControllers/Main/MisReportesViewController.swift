//
//  MisReportesViewController.swift
//  Sentinel
//
//  Created by Daniel on 3/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class MisReportesViewController:ParentViewController {
    @IBOutlet weak var primerView: UIView!
    @IBOutlet weak var segundoView: UIView!
    @IBOutlet weak var dniSemaforoIMG: UIImageView!
    @IBOutlet weak var rucSemaforoIMG: UIImageView!
    @IBOutlet weak var empresasSemaforoIMG: UIImageView!
    @IBOutlet weak var noRepresentanteLB: UILabel!
    @IBOutlet weak var misempresasLB: UILabel!
    @IBOutlet weak var toolbarReportes: UIToolbar!
    @IBOutlet weak var empresasTV: UITableView!
    @IBOutlet weak var alturaRuc: NSLayoutConstraint!
    @IBOutlet weak var topMisEmpresas: NSLayoutConstraint!
    
    var esPremium = false
    var relacion = ""
    var misEmpresasLista = NSArray()
    let userData = UserGlobalData.sharedInstance.userGlobal!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializarTab()
        self.estilos()
        self.iniMisEmpresas()
    }
    func inicializarTab(){
        if UserGlobalData.sharedInstance.userGlobal.tabIndicador == 0 {
            self.tabBarController?.selectedIndex = 2
        }else if UserGlobalData.sharedInstance.userGlobal.tabIndicador == 1 {
            self.tabBarController?.selectedIndex = 0
        }
        if userData.NroDocRel != "" && userData.NroDocRel != userData.user{
            
        }else{
            self.segundoView.isHidden = true
            self.alturaRuc.constant = 0
            self.topMisEmpresas.constant = 0
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.clipsToBounds = false
        self.tabBarController?.tabBar.layer.borderWidth = 0.1
        self.validarPremium()
        self.cargarDatos()
    }
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            self.esPremium = true
            self.changeStatusBar(cod: 3)
            self.setGradient(uiView: self.toolbarReportes)
            self.misempresasLB.textColor = UIColor.fromHex(0xD18E18)
        }else{
            self.esPremium = false
        }
    }
    func cargarDatos(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        let semaforoActivo = userData!.SemAct!
        self.dniSemaforoIMG.image = self.devuelveSemaforo(SemAct: semaforoActivo)
        let semaforoRuc = userData!.SemActRel!
        self.rucSemaforoIMG.image = self.devuelveSemaforo(SemAct: semaforoRuc)
        if UserGlobalData.sharedInstance.userGlobal.MisEmpresas != nil {
            if (UserGlobalData.sharedInstance.userGlobal.MisEmpresas?.count)! > 0{
                self.noRepresentanteLB.isHidden = true
                self.empresasTV.isHidden = false
            }else{
                self.noRepresentanteLB.isHidden = false
                self.empresasTV.isHidden = true
            }
        }
    }
    func estilos(){
        self.primerView.layer.cornerRadius = 5
        self.segundoView.layer.cornerRadius = 5
        
        primerView.layer.shadowColor = UIColor.lightGray.cgColor
        primerView.layer.shadowOpacity = 0.5
        primerView.layer.shadowOffset = CGSize.zero
        
        segundoView.layer.shadowColor = UIColor.lightGray.cgColor
        segundoView.layer.shadowOpacity = 0.5
        segundoView.layer.shadowOffset = CGSize.zero
    }
    
    func iniConsulta(tipoDoc:String){
        NotificationCenter.default.addObserver(self, selector: #selector(self.endConsulta(_:)), name: NSNotification.Name("endConsulta"), object: nil)
        showActivityIndicator()
        let usuarioBean = UserGlobalData.sharedInstance.userGlobal!
        var nroDoc = usuarioBean.NumDocumento!
        if tipoDoc == "D"{
            nroDoc = usuarioBean.NumDocumento!
        }else{
            nroDoc = usuarioBean.NroDocRel!
        }
        let sesion = usuarioBean.SesionId!
        let parametros = ["Usuario" : usuarioBean.user! as String,
            "TipoDocumento" :  tipoDoc,
            "NroDocumento" : nroDoc,
            "SesionId" : sesion,
            "origenAplicacion" : PATHS.APP_ID,
            "TipoConsulta" : "D",
            ] as [String : Any]
        OriginData.sharedInstance.miReporteDNI(notification: "endConsulta", parametros: parametros)
    }
    @objc func endConsulta(_ notification: NSNotification){
        hideActivityIndicator()
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        guard notification.object != nil else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        let data = notification.object as! UserClass
        if data.codigoWS == "0" {
            UserGlobalData.sharedInstance.userGlobal.InfoTitular = data.InfoTitular
            if relacion == "R"{
                let nav = self.storyboard?.instantiateViewController(withIdentifier: "MiReporteRUCVC") as! MiReporteRUCViewController
                UserGlobalData.sharedInstance.userGlobal.haciaEmpresas = false
                self.navigationController?.pushViewController(nav, animated: true)
            }
            else if relacion == "D"{
                let nav = self.storyboard?.instantiateViewController(withIdentifier: "MiReporteDniVC") as! MiReporteDniViewController
                UserGlobalData.sharedInstance.userGlobal.haciaEmpresas = false
                self.navigationController?.pushViewController(nav, animated: true)
            }
            else if relacion == "E"{
                let nav  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MisEmpresasVC") as! MisEmpresasViewController
                UserGlobalData.sharedInstance.userGlobal.haciaEmpresas = true
                self.navigationController?.pushViewController(nav, animated: true)
            }
        }else{
            if data.codigoWS != nil {
                if data.codigoWS == "99"
                {
                    showError99()
                }
                else
                {
                    iniMensajeError(codigo: (data.codigoWS!))
                }
            }
            else
            {
                let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                self.present(alerta, animated: true, completion: nil)
            }
        }
    }
    func iniConsultaEmpresa( tipoDoc : String, numDoc : String){
        NotificationCenter.default.addObserver(self, selector: #selector(self.endConsultaEmpresa(_:)), name: NSNotification.Name("endConsultaEmpresa"), object: nil)
        showActivityIndicator()
        let usuarioBean = UserGlobalData.sharedInstance.userGlobal!
        let sesion = usuarioBean.SesionId!
        let parametros = [
            "Usuario" : usuarioBean.user! as String,
            "Servicio" : "0",
            "TipoDocumento" : tipoDoc,
            "NroDocumento" : numDoc,
            "SesionId" : sesion,
            "origenAplicacion" : PATHS.APP_ID,
            "TipoConsulta" : "L",
            ] as [String : Any]
        OriginData.sharedInstance.miReporteDNI(notification: "endConsultaEmpresa", parametros: parametros)
    }
    
    @objc func endConsultaEmpresa(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        
        if validaIndicador == 0 {
            let data = notification.object as! UserClass
            if data.codigoWS == "0" {
                UserGlobalData.sharedInstance.userGlobal.InfoTitularEmpresas = data.InfoTitular
                self.relacion = "E"
                self.iniConsulta(tipoDoc:"D")
            }
            else
            {
                if data.codigoWS != nil {
                    if data.codigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        iniMensajeError(codigo: (data.codigoWS!))
                    }
                }
                else
                {
                    let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                    alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                    self.present(alerta, animated: true, completion: nil)
                }
            }
        }
    }
    
    func iniMisEmpresas(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.endMisEmpresas(_:)), name: NSNotification.Name("endMisEmpresas"), object: nil)
        showActivityIndicator()
        let usuarioBean = UserGlobalData.sharedInstance.userGlobal!
        let UsuTipDoc = usuarioBean.TDocusu!
        let Usuario = usuarioBean.user!
        let SesionId = usuarioBean.SesionId!
        let parametros = ["UsuTipDoc": UsuTipDoc, "Usuario": Usuario, "SesionId": SesionId] as [String : Any]
        OriginData.sharedInstance.misEmpresas(notification: "endMisEmpresas", parametros: parametros)
    }
    
    @objc func endMisEmpresas(_ notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        var validaIndicador = 0
        guard notification.object != nil  else {
            validaIndicador = 1
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            self.hideActivityIndicator()
            return
        }
        
        if validaIndicador == 0 {
            
            let data = notification.object as! UserClass
            if data.CodigoWS == "0" {
                UserGlobalData.sharedInstance.userGlobal.MisEmpresas = data.MisEmpresas
                let arrayEmpresas : NSArray = UserGlobalData.sharedInstance.userGlobal.MisEmpresas!
                if data.MisEmpresas?.count == 0 {
                    self.noRepresentanteLB.isHidden = false
                }
                self.misEmpresasLista = data.MisEmpresas!
                self.empresasTV.reloadData()
            }
            else
            {
                if data.codigoWS != nil {
                    if data.codigoWS == "99"
                    {
                        showError99()
                    }
                    else
                    {
                        iniMensajeError(codigo: (data.codigoWS!))
                    }
                }
                else
                {

                }
            }
        }
    }

    @IBAction func accionReporteRUC(_ sender: Any) {
        self.relacion = "R"
        UserGlobalData.sharedInstance.userGlobal.documentoTipoXPDF = "R"
        self.iniConsulta(tipoDoc: "R")
    }
    
    @IBAction func accionMiReporteDNI(_ sender:UIButton) {
        self.relacion = "D"
        UserGlobalData.sharedInstance.userGlobal.documentoTipoXPDF = "D"
        self.iniConsulta(tipoDoc: "D")
    }
    
    
}

extension MisReportesViewController : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var array : NSArray = NSArray()
        if UserGlobalData.sharedInstance.userGlobal.MisEmpresas != nil {
            array = UserGlobalData.sharedInstance.userGlobal.MisEmpresas!
        }
        
        return array.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : MisEmpresasTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MisEmpresasTVC", for: indexPath) as! MisEmpresasTableViewCell
        let arrayEmpresas : NSArray = UserGlobalData.sharedInstance.userGlobal.MisEmpresas!
        if arrayEmpresas.count != 0 {
            let objEmpresa : NSDictionary = arrayEmpresas[indexPath.row] as! NSDictionary
            cell.tituloLB.text =  objEmpresa.object(forKey: "NomEmp") as! String
            let semaforoEmpresa : String = objEmpresa.object(forKey: "SemActEmp") as! String
            cell.semaforoIMG.image = self.devuelveSemaforo(SemAct: semaforoEmpresa)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dataUser = UserGlobalData.sharedInstance.userGlobal
        let esPremium : String = (dataUser?.EsPremium!)!
        let arrayEmpresas : NSArray = UserGlobalData.sharedInstance.userGlobal.MisEmpresas!
        if arrayEmpresas.count > 0{
            let objEmp : NSDictionary = arrayEmpresas[indexPath.row] as! NSDictionary
            let tipoDoc : String = objEmp.object(forKey: "TDocEmp") as! String
            let numDoc : String = objEmp.object(forKey: "NDocEmp") as! String
            UserGlobalData.sharedInstance.userGlobal.nEmpresa = numDoc
            UserGlobalData.sharedInstance.userGlobal.tEmpresa = tipoDoc
            self.iniConsultaEmpresa(tipoDoc: tipoDoc, numDoc: numDoc)
        }
    }
}
