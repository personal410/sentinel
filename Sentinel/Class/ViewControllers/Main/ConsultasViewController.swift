//
//  ConsultasViewController.swift
//  Sentinel
//
//  Created by Daniel on 3/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import XLPagerTabStrip
class ConsultasViewController:ParentViewController {
    @IBOutlet weak var toolbarConsultas: UIToolbar!
    @IBOutlet weak var toolBarItem: UIBarButtonItem!
    var cargarConsultasFlash = false
    override func viewDidLoad(){
        super.viewDidLoad()
        self.inicializarVariable()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.validarPremium()
        self.tabBarController?.tabBar.clipsToBounds = false
        self.tabBarController?.tabBar.layer.borderWidth = 0.1
    }
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            let servicio = UserGlobalData.sharedInstance.misServiciosGlobal
            if servicio?.TipServ == "6" {
                self.iniBusquedaEmpresarialContador()
            }else{
                let nav = UIStoryboard(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "ConsultasPremiumVC") as! ConsultasPremiumViewController
                nav.cargarConsultasFlash = cargarConsultasFlash
                cargarConsultasFlash = false
                self.navigationController?.pushViewController(nav, animated: false)
            }
        }
        else{
            let servicio = UserGlobalData.sharedInstance.misServiciosGlobal
            if servicio?.TipServ == "6"{
                self.iniBusquedaEmpresarialContador()
            }
        }
    }
    func inicializarVariable(){
        self.toolbarConsultas.clipsToBounds = true
    }
    func iniBusquedaEmpresarialContador(){
        showActivityIndicator()
        NotificationCenter.default.addObserver(self, selector: #selector(self.endBusquedaEmpresarialContador(_:)), name: NSNotification.Name("endBusquedaEmpresarialContador2"), object: nil)
        let userData = UserGlobalData.sharedInstance.userGlobal
        let servicios = UserGlobalData.sharedInstance.misServiciosGlobal!
        let numServ = servicios.NroServicio!
        let tipoServ = servicios.TipServ!
        let sesion = (userData?.SesionId!)!
        let usuario = (userData?.user)!
        let parametros = [
            "AFSerCod" : numServ,
            "FatProCod" :   tipoServ,
            "UseSeIDSession" : sesion,
            "UserID" : usuario
            ] as [String : Any]
        OriginData.sharedInstance.listarDisponiblesEmp(notification: "endBusquedaEmpresarialContador2", parametros: parametros)
    }
    @objc func endBusquedaEmpresarialContador(_ notification:NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        guard notification.object != nil else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        let data : MisServiciosClass = (notification.object as? MisServiciosClass)!
        if data.codigoWs == "0" {
            let SDTDatosservicio = data.SDTDatosservicio!
            UserGlobalData.sharedInstance.userGlobal.AFSerCPTNDisp = SDTDatosservicio.object(forKey: "AFSerCPTNDisp") as! String
            let userData = UserGlobalData.sharedInstance.userGlobal
            if userData?.EsPremium == "S" {
                let nav = UIStoryboard.init(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "ConsultasEmpresarialVC") as! ConsultasEmpresarialViewController
                self.navigationController?.pushViewController(nav, animated: false)
            }else{
                let nav = UIStoryboard.init(name: "Consultas", bundle: nil).instantiateViewController(withIdentifier: "ConsultasEmpresarialVC") as! ConsultasEmpresarialViewController
                self.navigationController?.pushViewController(nav, animated: false)
            }
        }else{
            if data.codigoWs != "" {
                if data.codigoWs == "99"
                {
                    showError99()
                }
                else
                {
                    let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: "En estos momentos no se puede realizar la operación.", preferredStyle: .alert)
                    let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                    alertaVersion.addAction(actionVersion)
                    self.present(alertaVersion, animated: true, completion: nil)
                }
            }
        }
    }
}
