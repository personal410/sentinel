//
//  BannerImagePageViewController.swift
//  Sentinel
//
//  Created by Juan Alberto Carlos Vera on 7/9/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//s
class BannerImagePageViewController:UIPageViewController {
    struct Storyboard {
        static let showBannerViewController = "showBannerViewController"
    }
    
    var arrBannerItems = BannerItem.getItems()
    var indiceAutomatic = 0
    var pageIndex = 0
    var tiempo = Timer()
    
    lazy var controllers: [UIViewController] = {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var controllers = [UIViewController]()
        for item in arrBannerItems {
            let banneImageVC = storyboard.instantiateViewController(withIdentifier: "BannerImageVC")
            controllers.append(banneImageVC)
        }
        return controllers
    }()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        dataSource = self
        delegate = self
        turnToPage(index: 0)
//        let user = UserGlobalData.sharedInstance.userGlobal!
//        let dicParams = ["UsuTDoc": user.TDocusu!, "Usuario": user.user!, "SesionId": user.SesionId!, "CampId": 6, "SegmId": 0] as [String : Any]
//        ServiceConnector.connectToUrl("\(PATHS.PATHSENTINEL)RWS_MSObtienePromoUsuario", params: dicParams){(r, e) in
//            if e == nil, let dicResult = r as? [String: Any], let codigoWs = dicResult["CodigoWS"] as? String, codigoWs == "0", let arrPromos = dicResult["RWS_MSPromoUsuario"] as? [Any], !arrPromos.isEmpty {
//                self.arrBannerItems.insert(BannerItem(UIImage(named: "bannerPromociones")!, "Promociones", "Aprovecha todo el poder de la App Sentinel"), at: 0)
//                self.turnToPage(index: 0)
//            }
//        }
    }
    override func viewWillAppear(_ animated: Bool) {
        tiempo = Timer.scheduledTimer(timeInterval: 6.0, target: self, selector: #selector(self.loadNextController), userInfo: nil, repeats: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        tiempo.invalidate()
    }
    @objc func loadNextController(){
        let user  = UserGlobalData.sharedInstance.userGlobal!
        if PATHS.sesionActive == true {
            if user.EsPremium == "S"{
                if self.indiceAutomatic == 0 {
                    indiceAutomatic = 1
                }
                else if self.indiceAutomatic == 1 {
                    indiceAutomatic = 0
                }
                self.turnToPage(index: indiceAutomatic)
            }else{
                if self.indiceAutomatic == 0 {
                    indiceAutomatic = indiceAutomatic + 1
                }else if self.indiceAutomatic == 1 {
                    indiceAutomatic = indiceAutomatic + 1
                }else if self.indiceAutomatic == 2 {
                    indiceAutomatic = 0
                }
                self.turnToPage(index: indiceAutomatic)
            }
        }
    }
    
    func viewControllerTimer(indiceA:Int) -> UIViewController? {
        return controllers[indiceA]
    }
    
    func turnToPage(index: Int){
        let controller = controllers[index]
        var direction  = UIPageViewControllerNavigationDirection.forward
        if let currentVC = viewControllers?.first{
            let currentIndex = controllers.index(of: currentVC)!
            if currentIndex > index {
                direction = .reverse
            }
        }
        self.configureDisplayer(viewController: controller)
        setViewControllers([controller], direction: direction, animated: true, completion: nil)
    }
    func configureDisplayer(viewController: UIViewController){
        for (index, vc) in controllers.enumerated(){
            if viewController === vc {
                if let bannerImageVC = viewController as? BannerImageViewController{
                    let bannerItem = arrBannerItems[index]
                    bannerImageVC.image = bannerItem.image
                    bannerImageVC.titulo = bannerItem.titulo
                    bannerImageVC.detalle = bannerItem.detalle
                    bannerImageVC.numero = index
                }
            }
        }
    }
}
// MARK: -UIPageViewControllerDelegate
extension BannerImagePageViewController:UIPageViewControllerDelegate{
    func pageViewController(_ pageViewController:UIPageViewController, willTransitionTo pendingViewControllers:[UIViewController]) {
        self.configureDisplayer(viewController: pendingViewControllers.first as! BannerImageViewController)
    }
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if !completed {
            self.configureDisplayer(viewController: previousViewControllers.first as! BannerImageViewController)
        }
    }
}
// MARK: -UIPageViewControllerDataSource
extension BannerImagePageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let index = controllers.index(of: viewController){
            if index < controllers.count - 1 {
                self.indiceAutomatic = index + 1
                return controllers [index + 1]
            }
        }
        return controllers.first
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let index = controllers.index(of: viewController) {
            if index > 0 {
                self.indiceAutomatic = index - 1
                return controllers [index - 1]
            }
        }
        return controllers.last
    }
}
