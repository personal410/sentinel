//
//  BannerImageViewController.swift
//  Sentinel
//
//  Created by Juan Alberto Carlos Vera on 7/9/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class BannerImageViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tituloBanner: UILabel!
    @IBOutlet weak var subtituloBannerLB: UILabel!
    @IBOutlet weak var loQuieroBTN: UIButton!
    @IBOutlet weak var primerView: UIView!
    @IBOutlet weak var segundoView: UIView!
    @IBOutlet weak var tercerView: UIView!
    
    var indexViewController = 0
    var controlador  = 0
    
    var image : UIImage? {
        didSet{
            self.imageView?.image = image
            if image == UIImage(named: "demo01"){
                controlador = 0
            }else if image == UIImage(named: "terceros"){
                controlador = 1
            }else if image == UIImage(named: "pyme1"){
                controlador = 2
            }else if image == UIImage(named: "bannerRecupera"){
                controlador = 3
            }else if image == UIImage(named: "bannerPromociones"){
                controlador = 4
            }
        }
    }
    
    var titulo:String? {
        didSet{
            self.tituloBanner?.text = titulo
        }
    }
    var detalle:String? {
        didSet{
            self.subtituloBannerLB?.text = detalle
        }
    }
    var numero:Int? {
        didSet{
            self.indexViewController = numero!
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageView.image = image
        self.tituloBanner.text = titulo
        self.subtituloBannerLB.text = detalle
        self.loQuieroBTN.layer.cornerRadius = 5
        self.primerView.layer.cornerRadius = 1
        self.segundoView.layer.cornerRadius = 1
        self.tercerView.layer.cornerRadius = 1
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "endMoveBanner"), object: self.indexViewController)
    }
    @IBAction func accionLoquiero(_ sender: UIButton) {
        if controlador == 0 {
            navigationController?.pushViewController(storyboard!.instantiateViewController(withIdentifier: "SentinelPremiumVC"), animated: true)
        }else if controlador == 1 {
            navigationController?.pushViewController(storyboard!.instantiateViewController(withIdentifier: "ReporteDeudasPersonasEmpresasVC"), animated: true)
        }else if controlador == 2 {
            navigationController?.pushViewController(storyboard!.instantiateViewController(withIdentifier: "SentinelPyMEVC"), animated: true)
        }else if controlador == 3 {
            navigationController?.pushViewController(UIStoryboard(name: "Compra", bundle: nil).instantiateViewController(withIdentifier: "RecuperaCompraVC"), animated: true)
        }else if controlador == 4 {
            navigationController?.pushViewController(UIStoryboard(name: "Compra", bundle: nil).instantiateViewController(withIdentifier: "CompraPromocionesVC"), animated: true)
        }
    }
}
