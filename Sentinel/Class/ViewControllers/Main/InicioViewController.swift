//
//  InicioViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 5/14/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import UIKit
class InicioViewController:ParentViewController, MiPickerComboDelegate {
    //MARK: - Outlets
    @IBOutlet weak var comboView:CTComboPicker!
    @IBOutlet weak var btnGeneralSentinel:UIButton!
    @IBOutlet weak var vSemaforo:UIView!
    @IBOutlet weak var ivSemaforo:UIImageView!
    @IBOutlet weak var lblNombre:UILabel!
    @IBOutlet weak var vOtros:UIView!
    @IBOutlet weak var vSabio:UIView!
    @IBOutlet weak var vAnalisis:UIView!
    @IBOutlet weak var vResumen:UIView!
    @IBOutlet weak var vConsultasFlash:UIView!
    //MARK: - Attrs
    var user:UserClass! = UserGlobalData.sharedInstance.userGlobal!
    var servicios = UserGlobalData.sharedInstance.misServiciosGlobal
    var tercero : TerceroClass?
    var combosArray = NSMutableArray()
    var combosIDArray = NSMutableArray()
    var relacion = ""
    var codCombo = ""
    var desCombo = ""
    var origen = ""
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        configBorder(in: comboView)
        configBorder(in: vSemaforo)
        configBorder(in: vSabio)
        configBorder(in: vAnalisis)
        configBorder(in: vResumen)
        ivSemaforo.image = devuelveSemaforo(SemAct: (user.SemAct)!)
        lblNombre.text = "Hola \((user.Nombres)!.split(separator: " ").first!),"
        comboView.delegate = self
        let service = self.servicios
        for serv in 0 ... (service?.ServiciosTipo?.count)! - 1{
            let objServicio = (service?.ServiciosTipo![serv])!
            combosArray.add(objServicio.DscServicio)
            combosIDArray.add(objServicio.NroServicio)
        }
        iniConsultaDatosServicio()
    }
    override func viewDidAppear(_ animated:Bool){
        user = UserGlobalData.sharedInstance.userGlobal!
        self.iniCreditos()
        cargarVistas()
        setGradient(uiView: vConsultasFlash)
    }
    //MARK: - Actions
    @IBAction func actionCombo(){
        self.comboView.setConfig(combosArray as? [Any])
        self.comboView.open()
        self.comboView.setId(1)
    }
    @IBAction func accionDetalle(){
        relacion = "D"
        UserGlobalData.sharedInstance.userGlobal.documentoTipoXPDF = "D"
        iniConsulta(tipoDoc: "D")
    }
    @IBAction func accionSabio() {
        //PRUEBA
        if servicios?.FlgSabio == "F"{
            let nav = UIStoryboard.init(name: "SabioFinanciero", bundle: nil).instantiateViewController(withIdentifier: "SFclienteConyugeVC") as! SFclienteConyugeViewController
            UserGlobalData.sharedInstance.userGlobal.vieneDelUltimoSF = "NO"
            self.navigationController?.pushViewController(nav, animated: true)
        }else if servicios?.FlgSabio == "C"{
            let navComercial = UIStoryboard.init(name: "StoryboardSabioComercial", bundle: nil).instantiateViewController(withIdentifier: "NuevaBusquedaVC") as! SCBuscarTipoViewController
            self.navigationController?.pushViewController(navComercial, animated: true)
        }
    }
    @IBAction func accionAnalisis(){
        let alerta = UIAlertController(title: PATHS.SENTINEL, message: "Será redireccionado a la Web.", preferredStyle: UIAlertControllerStyle.alert)
        alerta.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action: UIAlertAction!) in
            self.origen = "AC"
            self.obtenerUsuarioWeb()
        }))
        alerta.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        present(alerta, animated: true, completion:nil)
    }
    @IBAction func accionResumen(){
        let alerta = UIAlertController(title: PATHS.SENTINEL, message: "Será redireccionado a la Web.", preferredStyle: UIAlertControllerStyle.alert)
        alerta.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { (action: UIAlertAction!) in
            self.origen = "RV"
            self.obtenerUsuarioWeb()
        }))
        alerta.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        present(alerta, animated: true, completion:nil)
    }
    @IBAction func accionConsultas(){
        if let viewCont = self.navigationController?.parent as? MainTabBarViewController {
            viewCont.loadCurrentOption(with: 2)
        }
    }
    @IBAction func accionConsultasFlash(){
        if let viewCont = self.navigationController?.parent as? MainTabBarViewController {
            viewCont.cambiarPantallaConsultas()
        }
    }
    //MARK: - MiPickerCombo
    func miPickerCombo(_ MiPickerView:CTComboPicker!, pickerView picker:UIPickerView!, didSelectRow row:Int, inComponent component:Int){}
    func miPickerCombo(_ pickerid:Int, didSelectRow row:Int){
        codCombo = combosIDArray[row] as! String
        desCombo = combosArray[row] as! String
        let ServiciosTipo = (self.servicios?.ServiciosTipo![row])!
        let numSer = ServiciosTipo.NroServicio
        let tipSer = ServiciosTipo.TipServ
        self.actualizarServicios(numServ: numSer, tipServ: tipSer)
        self.cargarVistas()
    }
    //MARK: - Auxiliar
    func configBorder(in v:UIView){
        v.layer.cornerRadius = 5
        v.layer.shadowColor = UIColor.lightGray.cgColor
        v.layer.shadowOpacity = 0.5
        v.layer.shadowOffset = CGSize.zero
    }
    func iniConsultaDatosServicio(){
        showActivityIndicator()
        let usua = (user.NumDocumento)!
        let sesion = (user.SesionId)!
        let servicio = (user.NroSerCT)!
        let parametros = ["Usuario": usua, "SesionId": sesion, "Servicio": servicio] as [String : Any]
        NotificationCenter.default.addObserver(self, selector: #selector(self.endConsultaDatosServicioAlerta(_:)), name: NSNotification.Name("endConsultaDatosServicioAlerta"), object: nil)
        OriginData.sharedInstance.consultaDatosServicio(notification: "endConsultaDatosServicioAlerta", parametros: parametros)
    }
    @objc func endConsultaDatosServicioAlerta(_ notification: NSNotification){
        hideActivityIndicator()
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        guard notification.object != nil else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        let terceroLocal = notification.object as! TerceroClass
        UserGlobalData.sharedInstance.terceroGlobal = terceroLocal
        tercero = UserGlobalData.sharedInstance.terceroGlobal
        tercero?.DispDet = terceroLocal.SaldoServicio?.object(forKey: "DispDet") as? String
        tercero?.DispRes = terceroLocal.SaldoServicio?.object(forKey: "DispRes") as? String
        tercero?.SalFlash = terceroLocal.SaldoServicio?.object(forKey: "SalFlash") as? String
        UserGlobalData.sharedInstance.terceroGlobal = tercero
        cargarVistaxServicios()
    }
    func cargarVistaxServicios(){
        let arrayServicios = (self.servicios?.ServiciosTipo!)!
        UserGlobalData.sharedInstance.misServiciosGlobal.NroServicio = arrayServicios.first!.NroServicio
        let numServicio : String = UserGlobalData.sharedInstance.userGlobal.NroSerCT!
        for i in 0 ... arrayServicios.count - 1 {
            var servicioObj : MiObejetoServicio = arrayServicios[i]
            if servicioObj.NroServicio == numServicio {
                UserGlobalData.sharedInstance.misServiciosGlobal.NroServicio = numServicio
                UserGlobalData.sharedInstance.misServiciosGlobal.TipServ = servicioObj.TipServ
                UserGlobalData.sharedInstance.misServiciosGlobal.DscServicio = servicioObj.DscServicio
                UserGlobalData.sharedInstance.misServiciosGlobal.FlgCoop = servicioObj.FlgCoop
                UserGlobalData.sharedInstance.misServiciosGlobal.FlgSabio = servicioObj.FlgSabio
                self.btnGeneralSentinel.setTitle("    \(UserGlobalData.sharedInstance.misServiciosGlobal.DscServicio!)", for: .normal)
                return
            }else{
                servicioObj = arrayServicios[0]
                UserGlobalData.sharedInstance.misServiciosGlobal.NroServicio = numServicio
                UserGlobalData.sharedInstance.misServiciosGlobal.TipServ = servicioObj.TipServ
                UserGlobalData.sharedInstance.misServiciosGlobal.DscServicio = servicioObj.DscServicio
                UserGlobalData.sharedInstance.misServiciosGlobal.FlgCoop = servicioObj.FlgCoop
                UserGlobalData.sharedInstance.misServiciosGlobal.FlgSabio = servicioObj.FlgSabio
                self.btnGeneralSentinel.setTitle("    \(UserGlobalData.sharedInstance.misServiciosGlobal.DscServicio!)", for: .normal)
            }
        }
        //PROBAR
        let servicioObj : MiObejetoServicio = arrayServicios[0]
        UserGlobalData.sharedInstance.misServiciosGlobal.NroServicio = servicioObj.NroServicio
        UserGlobalData.sharedInstance.misServiciosGlobal.TipServ = servicioObj.TipServ
        UserGlobalData.sharedInstance.misServiciosGlobal.DscServicio = servicioObj.DscServicio
        UserGlobalData.sharedInstance.misServiciosGlobal.FlgCoop = servicioObj.FlgCoop
        UserGlobalData.sharedInstance.misServiciosGlobal.FlgSabio = servicioObj.FlgSabio
        self.cargarVistas()
    }
    func cargarVistas(){
        servicios = UserGlobalData.sharedInstance.misServiciosGlobal
        if user.EsPremium == "S" {
            vConsultasFlash.isHidden = false
            if servicios?.TipServ != "" && servicios?.TipServ == "6"{
                self.vOtros.isHidden = false
                self.vSemaforo.isHidden = true
                vSabio.isHidden = true
                self.changeStatusBar(cod: 2)
                self.btnGeneralSentinel.setTitleColor(UIColor.fromHex(0x363636), for: UIControlState.normal)
                if servicios?.FlgSabio == "F" || servicios?.FlgSabio == "C" {
                    vSabio.isHidden = false
                }else{
                    vSabio.isHidden = true
                }
                self.changeStatusBar(cod: 2)
            }else{
                self.vOtros.isHidden = true
                self.vSemaforo.isHidden = false
                self.btnGeneralSentinel.setTitleColor(UIColor.fromHex(0xD18E18), for: UIControlState.normal)
                self.changeStatusBar(cod: 3)
            }
        }else{
            vConsultasFlash.isHidden = true
            if servicios?.NroServicio != "" && servicios?.NroServicio != "0" {
                if servicios?.TipServ != "" && servicios?.TipServ == "6"{
                    self.vOtros.isHidden = false
                    self.vSemaforo.isHidden = true
                    self.btnGeneralSentinel.setTitleColor(UIColor.fromHex(0x363636), for: UIControlState.normal)
                    self.changeStatusBar(cod: 2)
                    if servicios?.FlgSabio == "F" || servicios?.FlgSabio == "C"{
                        vSabio.isHidden = false
                    }else{
                        vSabio.isHidden = true
                    }
                }else{
                    self.vOtros.isHidden = true
                    self.vSemaforo.isHidden = false
                    self.btnGeneralSentinel.setTitleColor(UIColor.fromHex(0x06B150), for: UIControlState.normal)
                    self.changeStatusBar(cod: 1)
                }
            }else{
                self.vOtros.isHidden = true
                self.vSemaforo.isHidden = false
                self.btnGeneralSentinel.setTitleColor(UIColor.fromHex(0x06B150), for: UIControlState.normal)
                self.changeStatusBar(cod: 1)
            }
        }
    }
    func iniCreditos(){
        let userData = UserGlobalData.sharedInstance.userGlobal!
        let parametros = ["Usuario": userData.user!, "SesionId": userData.SesionId!] as [String : Any]
        NotificationCenter.default.addObserver(self, selector: #selector(self.endCreditos(_:)), name: NSNotification.Name("endCreditos"), object: nil)
        OriginData.sharedInstance.listadoIndicadores(notification: "endCreditos", parametros: parametros)
    }
    @objc func endCreditos(_ notification:NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        guard notification.object != nil  else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        let data = notification.object as? UserClass
        if data?.CodigoWS == "0"{
            UserGlobalData.sharedInstance.userGlobal.Indicadores = data?.Indicadores
        }else{
            if data?.CodigoWS != nil {
                if data?.CodigoWS == "99"
                {
                    showError99()
                }
                else
                {
                    let alertaVersion = UIAlertController.init(title: PATHS.SENTINEL, message: data?.MensajeRespuesta, preferredStyle: .alert)
                    let actionVersion = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
                    alertaVersion.addAction(actionVersion)
                    self.present(alertaVersion, animated: true, completion: nil)
                }
            }
        }
    }
    func iniConsulta(tipoDoc:String){
        NotificationCenter.default.addObserver(self, selector: #selector(self.endConsulta(_:)), name: NSNotification.Name("endConsulta"), object: nil)
        showActivityIndicator()
        let usuarioBean = UserGlobalData.sharedInstance.userGlobal!
        var nroDoc = usuarioBean.NumDocumento!
        if tipoDoc == "D"{
            nroDoc = usuarioBean.NumDocumento!
        }else{
            nroDoc = usuarioBean.NroDocRel!
        }
        
        let sesion = usuarioBean.SesionId!
        
        let parametros = ["Usuario": usuarioBean.user! as String, "Servicio": "0", "TipoDocumento" :  tipoDoc, "NroDocumento": nroDoc, "SesionId": sesion, "origenAplicacion": PATHS.APP_ID,
                          "TipoConsulta" : "D"] as [String : Any]
        OriginData.sharedInstance.miReporteDNI(notification: "endConsulta", parametros: parametros)
    }
    
    @objc func endConsulta(_ notification: NSNotification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        self.hideActivityIndicator()
        guard notification.object != nil else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
            return
        }
        let data = notification.object as! UserClass
        if data.codigoWS == "0" {
            UserGlobalData.sharedInstance.userGlobal.InfoTitular = data.InfoTitular
            if relacion == "R"{
                let nav = self.storyboard?.instantiateViewController(withIdentifier: "MiReporteRUCVC") as! MiReporteRUCViewController
                self.navigationController?.pushViewController(nav, animated: true)
            }else if relacion == "D"{
                let nav = self.storyboard?.instantiateViewController(withIdentifier: "MiReporteDniVC") as! MiReporteDniViewController
                self.navigationController?.pushViewController(nav, animated: true)
            }
        }else{
            if data.codigoWS != nil {
                if data.codigoWS == "99"
                {
                    showError99()
                }
                else
                {
                    iniMensajeError(codigo: (data.codigoWS!))
                }
            }
            else
            {
                let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
                alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
                self.present(alerta, animated: true, completion: nil)
            }
        }
    }
    func obtenerUsuarioWeb(){
        showActivityIndicator()
        let serviceUrl = "https://www.sentinelconsultagratuita.com/qawsapp/rest/RWS_MSObtieneLinkEmp"
        let nroServicio = UserGlobalData.sharedInstance.misServiciosGlobal.NroServicio ?? ""
        let usuarioBean = UserGlobalData.sharedInstance.userGlobal!
        var nroDoc = usuarioBean.NumDocumento!
        let tipoDoc = user!.TDocusu!
        if tipoDoc == "D" {
            nroDoc = usuarioBean.NumDocumento!
        }else{
            nroDoc = usuarioBean.NroDocRel!
        }
        let dicParams = ["Usuario": user!.user!, "SesionId": user!.SesionId!, "Servicio": nroServicio, "TipoConsulta": origen, "TDocCPT": tipoDoc, "NDocCPT": nroDoc]
        ServiceConnector.connectToUrl(serviceUrl, params: dicParams) { (result, error) in
            self.hideActivityIndicator()
            if let dicResult = result as? [String: String], let codigoWS = dicResult["CodigoWS"], let url = dicResult["URL"] {
                if codigoWS == "0" {
                    if !url.isEmpty {
                        let urlString = "\(url),\(self.origen),\(nroServicio),\(tipoDoc),\(nroDoc)"
                        UIApplication.shared.open(URL(string: urlString)!, options: [:], completionHandler: nil)
                        return
                    }
                }else if codigoWS == "99" {
                    self.showError99()
                    return
                }else{
                    self.iniMensajeError(codigo: codigoWS)
                    return
                }
            }
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            self.present(alerta, animated: true, completion: nil)
        }
    }
}
