//
//  ConfiguracionViewController.swift
//  Sentinel
//
//  Created by Daniel on 6/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class ConfiguracionViewController:ParentViewController, UITableViewDataSource, UITableViewDelegate {
    //MARK: - Outlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var menuTV: UITableView!
    //MARK: - Props
    var arregloMenu:[MenuTabBean] = []
    let user = UserGlobalData.sharedInstance.userGlobal
    var esPremium = false
    //MARK: - ViewCont
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializarVariables()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.validarPremium()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.menuTV.reloadData()
    }
    //MARK: - Auxiliar
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            self.esPremium = true
            self.changeStatusBar(cod: 3)
            self.setGradient(uiView: self.mainView, at: 0)
        }
    }
    
    func inicializarVariables(){
        let menuTabCambiarContra = MenuTabBean()
        menuTabCambiarContra.titulo = "Cambiar Contraseña"
        menuTabCambiarContra.imagen = "contrasena"
        arregloMenu.append(menuTabCambiarContra)
        
        let menuTabCerrarSesion = MenuTabBean()
        menuTabCerrarSesion.titulo = "Cerrar Sesión"
        menuTabCerrarSesion.imagen = "salir72"
        arregloMenu.append(menuTabCerrarSesion)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConfiguracionTableViewCell", for: indexPath) as! ConfiguracionTableViewCell
        let objMenu = arregloMenu[indexPath.row]
        cell.tituloLB.text = objMenu.titulo
        cell.imgConfig.image = UIImage.init(named: objMenu.imagen)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath){
        if indexPath.row == 0 {
            let nav = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "CambiarClaveVC") as! CambiarClaveViewController
            nav.irMenuPrincipal = false
            navigationController?.pushViewController(nav, animated: true)
        }else{
            NotificationCenter.default.removeObserver(self)
            PATHS.sesionActive = false
            defaultsKeys.USER_NAME_KEY = ""
            defaultsKeys.USER_PASSWORD_KEY = ""
            UserDefaults.standard.set("", forKey: "userName")
            UserDefaults.standard.set("", forKey: "password")
            UserDefaults.standard.synchronize()
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name("endMoveBanner"), object: nil)
            navigationController?.navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func accionAtras(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
