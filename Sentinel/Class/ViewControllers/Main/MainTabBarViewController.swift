//
//  MainTabBarViewController.swift
//  Sentinel
//
//  Created by Victor Salazar on 5/13/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import UIKit
class MainTabBarViewController:UIViewController {
    //MARK: - Outlets
    @IBOutlet weak var contentView:UIView!
    @IBOutlet weak var bottomBarView:UIView!
    @IBOutlet weak var ivInicio:UIImageView!
    @IBOutlet weak var ivMisReportes:UIImageView!
    @IBOutlet weak var ivConsultar:UIImageView!
    @IBOutlet weak var ivTienda:UIImageView!
    @IBOutlet weak var ivBlog:UIImageView!
    @IBOutlet weak var ivMenu:UIImageView!
    @IBOutlet weak var vPopUp:UIView!
    //MARK: - Attrs
    var arrImgViews:[UIImageView] = []
    var currentIndex = -1, showNotificationType = 0, loadOptionMenu = -1
    let arrImages = ["Home", "MisReportes", "Consultas", "Tienda", "Blog", "Menu"]
    let user = UserGlobalData.sharedInstance.userGlobal!
    var esPremium = false, cargarCompraDetalleResumido = false, cargarConsultasFlash = false
    var dicChildViewCont:[String: UIViewController] = [:]
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        esPremium = user.EsPremium == "S"
        arrImgViews.append(ivInicio)
        arrImgViews.append(ivMisReportes)
        arrImgViews.append(ivConsultar)
        arrImgViews.append(ivTienda)
        arrImgViews.append(ivBlog)
        arrImgViews.append(ivMenu)
        loadCurrentOption(with: 0)
    }
    override func viewWillAppear(_ animated:Bool){
        super.viewWillAppear(animated)
        if showNotificationType > 0 {
            showNotification(showNotificationType)
            showNotificationType = 0
        }
        let params = ["Usuario": user.user!, "SesionId": user.SesionId!, "Modo": "GET", "OrigenAplicacion": "11"]
        ServiceConnector.connectToUrl("https://misentinel.sentinelperu.com/misentinelws/rest/RWS_MSPopUpInicial", params: params) { (r, e) in
            if let dic = r as? [String: Any], let codigoWs = dic["CodigoWS"] as? String, codigoWs == "0", let mostrarPopUp = dic["MostrarPopUp"] as? String, mostrarPopUp == "S" {
                self.vPopUp.isHidden = false
                let popUpExperianVC = self.storyboard!.instantiateViewController(withIdentifier: "PopUpExperianVC")
                var frame = self.vPopUp.frame
                frame.origin.x = 0
                frame.origin.y = 0
                popUpExperianVC.view.frame = frame
                self.vPopUp.addSubview(popUpExperianVC.view)
                self.addChildViewController(popUpExperianVC)
            }
        }
    }
    //MARK: - Actions
    @IBAction func optionSelected(_ btn:UIButton){
        loadCurrentOption(with: btn.tag)
    }
    //MARK: - Aux
    func loadCurrentOption(with newIdx:Int){
        if currentIndex > -1 {
            arrImgViews[currentIndex].image = UIImage(named: "\(arrImages[currentIndex]).off")
        }
        currentIndex = newIdx
        arrImgViews[currentIndex].image = UIImage(named: "\(arrImages[currentIndex])_on\(esPremium ? "_Pre" : "")")
        for childVC in childViewControllers {
            childVC.view.removeFromSuperview()
            childVC.removeFromParentViewController()
        }
        let newPage = arrImages[currentIndex]
        var newViewCont:UIViewController
        if let viewCont = dicChildViewCont[newPage] {
            newViewCont = viewCont
            if currentIndex < 3 {
                if let navViewController = viewCont as? UINavigationController {
                    let index = currentIndex == 2 ? (esPremium ? 1 : 0) : 0
                    navViewController.popToViewController(navViewController.viewControllers[index], animated: true)
                    if currentIndex == 2 && cargarConsultasFlash {
                        cargarConsultasFlash = false
                        if let viewCont = navViewController.viewControllers[1] as? ConsultasPremiumViewController {
                            viewCont.cargarConsultasFlash = true
                        }
                    }
                }
            }
        }else{
            newViewCont = storyboard!.instantiateViewController(withIdentifier: "\(newPage)NavVC") as! UINavigationController
            if currentIndex == 2 && cargarConsultasFlash {
                cargarConsultasFlash = false
                if let navCont = newViewCont as? UINavigationController, let viewCont = navCont.viewControllers.first as? ConsultasViewController {
                    viewCont.cargarConsultasFlash = true
                }
            }
            dicChildViewCont[newPage] = newViewCont
        }
        if cargarCompraDetalleResumido {
            cargarCompraDetalleResumido = false
            if currentIndex == 3 {
                if let navVC = newViewCont as? UINavigationController {
                    navVC.popToRootViewController(animated: true)
                    if let productosVC = navVC.viewControllers.first as? ProductosViewController {
                        productosVC.mostrarCompraDetalladoResumen = true
                    }
                }
            }else if currentIndex == 5 {
                
            }
        }
        if currentIndex == 2 && cargarConsultasFlash {
            cargarConsultasFlash = false
        }
        newViewCont.view.frame = contentView.frame
        contentView.addSubview(newViewCont.view)
        addChildViewController(newViewCont)
        if currentIndex == 5 && loadOptionMenu > -1 {
            if let navMenuPrincipalVC = childViewControllers.first as? UINavigationController {
                if (navMenuPrincipalVC.viewControllers.count > 1){
                    navMenuPrincipalVC.popToRootViewController(animated: true)
                }
                if let menuPrincipalVC = navMenuPrincipalVC.viewControllers.first as? MenuPrincipalViewController {
                    menuPrincipalVC.load(with: loadOptionMenu)
                    loadOptionMenu = -1
                }
            }
        }
    }
    func cambiarPantallaCompra(){
        cargarCompraDetalleResumido = true
        loadCurrentOption(with: 3)
    }
    func cambiarPantallaConsultas(){
        cargarConsultasFlash = true
        loadCurrentOption(with: 2)
    }
    func showNotification(_ type:Int){
        if type == 1 {//Alerta Personal
            if currentIndex == 5 {
                if let navMenuPrincipalVC = childViewControllers.first as? UINavigationController {
                    if (navMenuPrincipalVC.viewControllers.count > 1){
                        navMenuPrincipalVC.popToRootViewController(animated: true)
                    }
                    if let menuPrincipalVC = navMenuPrincipalVC.viewControllers.first as? MenuPrincipalViewController {
                        loadOptionMenu = -1
                        menuPrincipalVC.load(with: 1)
                    }
                }
            }else{
                loadOptionMenu = 1
                loadCurrentOption(with: 5)
            }
        }else if type == 3 {//Mensaje
            if currentIndex == 5 {
                if let navMenuPrincipalVC = childViewControllers.first as? UINavigationController {
                    if (navMenuPrincipalVC.viewControllers.count > 1){
                        navMenuPrincipalVC.popToRootViewController(animated: true)
                    }
                    if let menuPrincipalVC = navMenuPrincipalVC.viewControllers.first as? MenuPrincipalViewController {
                        loadOptionMenu = -1
                        menuPrincipalVC.load(with: 2)
                    }
                }
            }else{
                loadOptionMenu = 2
                loadCurrentOption(with: 5)
            }
        }else if type == 4 {
            if currentIndex != 0 {
                loadCurrentOption(with: 0)
            }
        }
    }
}
