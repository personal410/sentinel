//
//  MenuPrincipalViewController.swift
//  Sentinel
//
//  Created by Daniel on 5/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class MenuPrincipalViewController:ParentViewController, UITableViewDataSource, UITableViewDelegate {
    //MARK: - Outlet
    @IBOutlet weak var nombreCompletoLB:UILabel!
    @IBOutlet weak var correoLB:UILabel!
    @IBOutlet weak var mainView:UIView!
    @IBOutlet weak var menuTV:UITableView!
    //MARK: - Props
    var arregloMenu:[MenuTabBean] = []
    let user = UserGlobalData.sharedInstance.userGlobal!
    var loadOption = -1
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        inicializarVariables()
        showActivityIndicator()
        let url = "\(PATHS.PATHSENTINEL)/RWS_MSContNotif"
        let dicParams = ["TDocUsu": user.TDocusu!, "SesionId": user.SesionId!, "Usuario": user.user!]
        ServiceConnector.connectToUrl(url, params: dicParams) { (r, e) in
            self.hideActivityIndicator()
            if let dic = r as? [String: Any] {
                if let codigoWs = dic["CodigoWS"] as? String {
                    if codigoWs == "0" {
                        if let recupera = dic["Recupera"] as? Int {
                            if recupera > 0 {
                                let recuperaMt = self.arregloMenu.first!
                                recuperaMt.detalleCompleto = "\(recupera) aviso\(recupera == 1 ? "" : "s") de cobranza"
                                recuperaMt.cantNotificaciones = recupera
                                self.menuTV.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .none)
                            }
                        }
                    }
                }
            }
        }
    }
    override func viewWillAppear(_ animated:Bool){
        self.validarPremium()
    }
    override func viewDidAppear(_ animated:Bool){
        self.menuTV.reloadData()
        if loadOption > -1 {
            load(with: loadOption)
            loadOption = -1
        }
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? PrincipalRecuperaViewController {
            viewCont.codServicio = sender as! String
        }
    }
    //MARK: - Auxiliar
    func validarPremium(){
        let userData = UserGlobalData.sharedInstance.userGlobal
        if userData?.EsPremium == "S" {
            changeStatusBar(cod: 3)
            setGradient(uiView: mainView, at: 0)
        }
    }
    func inicializarVariables(){
        let nombre = user.Nombres!
        let apePat = user.ApellidoPaternoConsultado!
        let firstName = nombre.split(separator: " ").first!
        nombreCompletoLB.text = "\(firstName) \(apePat)"
        correoLB.text = user.Mail
        
        let mtRecupera = MenuTabBean()
        mtRecupera.indPantalla = 4
        mtRecupera.titulo = "Recupera"
        mtRecupera.imagen = "recupera"
        mtRecupera.detalleCompleto = "Recupera tu dinero prestado"
        arregloMenu.append(mtRecupera)
        
        let mtAlertPersonal = MenuTabBean()
        mtAlertPersonal.indPantalla = 0
        mtAlertPersonal.titulo = "Alerta Personal"
        mtAlertPersonal.imagen = "alertapersonal"
        mtAlertPersonal.detalleCompleto = "No tienes ninguna alerta"
        arregloMenu.append(mtAlertPersonal)
        
        let mtMensajes = MenuTabBean()
        mtMensajes.indPantalla = 1
        mtMensajes.titulo = "Mensajes"
        mtMensajes.imagen = "mensajesMenu"
        mtMensajes.detalleCompleto = "No tienes ningún mensaje"
        self.arregloMenu.append(mtMensajes)
        
        let mtPromociones = MenuTabBean()
        mtPromociones.indPantalla = 2
        mtPromociones.titulo = "Promociones"
        mtPromociones.imagen = "promociones"
        mtPromociones.detalleCompleto = "Ingresa tu código promocional"
        self.arregloMenu.append(mtPromociones)
        
        let mtSemaforizacion = MenuTabBean()
        mtSemaforizacion.indPantalla = 3
        mtSemaforizacion.titulo = "Semaforización"
        mtSemaforizacion.imagen = "semaforizacion"
        mtSemaforizacion.detalleCompleto = "Color de los semáforos"
        arregloMenu.append(mtSemaforizacion)
    }
    func validarRecupera(){
        showActivityIndicator()
        let usuarioBean = UserGlobalData.sharedInstance.userGlobal!
        let params = ["TipoProd": "35", "SesionId": usuarioBean.SesionId, "Usuario": usuarioBean.user!, "UsuTipDoc": usuarioBean.TDocusu!]
        NotificationCenter.default.addObserver(self, selector: #selector(endValidarRecupera(_:)), name: Notification.Name("endValidarRecupera"), object: nil)
        OriginData.sharedInstance.servicioTipoProd(notification: "endValidarRecupera", parametros: params as [String : Any])
    }
    @objc func endValidarRecupera(_ notification:Notification){
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        hideActivityIndicator()
        guard let data = notification.object as? ServicioTipoProdResponse else {
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: UIAlertControllerStyle.alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
            present(alerta, animated: true, completion: nil)
            return
        }
        if let codigoWs = data.codigoWs {
            if codigoWs == "0" {
                if let servicio = data.servicio, !servicio.isEmpty || servicio != "0" {
                    performSegue(withIdentifier: "showRecupera", sender: servicio)
                }
            }else if codigoWs == "99" {
                showError99()
            }else{
                iniMensajeError(codigo: codigoWs)
            }
        }else{
            let alerta = UIAlertController(title: "ALERTA", message: "En estos momentos no es posible realizar la consulta.", preferredStyle: .alert)
            alerta.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil));
            present(alerta, animated: true, completion: nil)
        }
    }
    func load(with option:Int){
        if menuTV == nil {
            loadOption = option
        }else{
            tableView(menuTV, didSelectRowAt: IndexPath(row: option, section: 0))
        }
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arregloMenu.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NuevoMenuTableViewCell", for: indexPath) as! NuevoMenuTableViewCell
        let objMenu = arregloMenu[indexPath.row]
        cell.tituloLB.text = objMenu.titulo
        cell.detalleLB.text = objMenu.detalleCompleto
        cell.imageMenu.image = UIImage(named: objMenu.imagen)
        cell.vNotificaciones.isHidden = objMenu.cantNotificaciones == 0
        return cell
    }
    func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath){
        let menuTab = arregloMenu[indexPath.row]
        switch menuTab.indPantalla {
            case 0:
                let nav = UIStoryboard(name: "OpcionesMenu", bundle: nil).instantiateViewController(withIdentifier: "AlertaPersonalVC")
                self.navigationController?.pushViewController(nav, animated: true)
                break
            case 1:
                let nav = UIStoryboard(name: "OpcionesMenu", bundle: nil).instantiateViewController(withIdentifier: "MensajesVC")
                self.navigationController?.pushViewController(nav, animated: true)
                break
            case 2:
                let nav = UIStoryboard(name: "OpcionesMenu", bundle: nil).instantiateViewController(withIdentifier: "PromocionesVC")
                self.navigationController?.pushViewController(nav, animated: true)
                break
            case 3:
                let nav = UIStoryboard(name: "OpcionesMenu", bundle: nil).instantiateViewController(withIdentifier: "SemaforosDetalleVistaVC")
                self.navigationController?.pushViewController(nav, animated: true)
                break
            case 4:
                validarRecupera()
                break
            default:
                break
        }
    }
}
