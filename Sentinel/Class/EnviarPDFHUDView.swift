//
//  EnviarPDFHUDView.swift
//  Sentinel
//
//  Created by Victor Salazar on 4/14/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
@objc protocol EnviarPDFHUDViewDelegate{
    func enviarPDFHUDDidConfirmWith(option index:Int)
}

class EnviarPDFHUDView:UIView{
    var lblTitle:UILabel!
    var delegate:EnviarPDFHUDViewDelegate?
    var title = "" {
        didSet{
            lblTitle.text = title
        }
    }
    static var rateHUDView:EnviarPDFHUDView?
    override init(frame:CGRect){
        super.init(frame: frame)
        backgroundColor = UIColor(rgb: 0, a: 0.7)
        let internalView = UIView(frame: CGRect(x: 40, y: frame.height / 2 - 120, width: frame.width - 80, height: 240))
        internalView.backgroundColor = UIColor.white
        let titleLbl = UILabel(frame: CGRect(x: 10, y: 30, width: internalView.frame.width - 20, height: 21))
        titleLbl.font = UIFont.boldSystemFont(ofSize: 17)
        titleLbl.textAlignment = .center
        internalView.addSubview(titleLbl)
        lblTitle = titleLbl
        let btnEmail = UIButton(frame: CGRect(x: 30, y: 80, width: internalView.frame.width - 60, height: 40))
        btnEmail.setTitle("Enviar a mi correo", for: .normal)
        btnEmail.setTitleColor(UIColor.white, for: .normal)
        btnEmail.backgroundColor = UIColor(hex: "0072BE")
        btnEmail.tag = 1
        btnEmail.layer.cornerRadius = 5
        btnEmail.addTarget(self, action: #selector(selectButton(_:)), for: .touchUpInside)
        internalView.addSubview(btnEmail)
        let btnVer = UIButton(frame: CGRect(x: 30, y: 130, width: internalView.frame.width - 60, height: 40))
        btnVer.setTitle("Ver en pantalla", for: .normal)
        btnVer.setTitleColor(UIColor.white, for: .normal)
        btnVer.backgroundColor = UIColor(hex: "FD9426")
        btnVer.tag = 2
        btnVer.layer.cornerRadius = 5
        btnVer.addTarget(self, action: #selector(selectButton(_:)), for: .touchUpInside)
        internalView.addSubview(btnVer)
        let cancelBtn = UIButton(frame: CGRect(x: 0, y: 210, width: internalView.frame.width - 20, height: 20))
        cancelBtn.setTitle("CANCELAR", for: .normal)
        cancelBtn.setTitleColor(UIColor.lightGray, for: .normal)
        cancelBtn.contentHorizontalAlignment = .right
        cancelBtn.addTarget(self, action: #selector(selectButton(_:)), for: .touchUpInside)
        internalView.addSubview(cancelBtn)
        addSubview(internalView)
    }
    required init?(coder aDecoder:NSCoder){
        fatalError("init(coder:) has not been implemented")
    }
    class func showEnviarPDFViewWithDelegate(_ delegate:EnviarPDFHUDViewDelegate? = nil, with title:String = ""){
        let application = UIApplication.shared
        let window = application.windows.first!
        rateHUDView = EnviarPDFHUDView(frame: window.frame)
        rateHUDView?.delegate = delegate
        rateHUDView?.title = title
        window.addSubview(rateHUDView!)
    }
    class func dismiss(){
        rateHUDView?.removeFromSuperview()
        rateHUDView = nil
    }
    //MARK: - IBAction
    @objc func selectButton(_ btn:UIButton){
        EnviarPDFHUDView.dismiss()
        if btn.tag > 0 {
            delegate?.enviarPDFHUDDidConfirmWith(option: btn.tag)
        }
    }
}
