//
//  ViewController.swift
//  Luhho
//
//  Created by Daniel Montoya on 15/11/17.
//  Copyright © 2017 Daniel Montoya. All rights reserved.
//

import UIKit

var container: UIView = UIView()
var loadingView: UIView = UIView()
var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
var overlayView = UIView()
var bgView = UIView()
extension UIViewController {
    public func showActivityIndicator(view:UIView = UIApplication.shared.keyWindow!) {
        bgView.frame = view.frame
        bgView.backgroundColor = UIColor(hex: "FFFFFF").withAlphaComponent(0.5)
        bgView.addSubview(overlayView)
        bgView.autoresizingMask = [.flexibleLeftMargin,.flexibleTopMargin,.flexibleRightMargin,.flexibleBottomMargin,.flexibleHeight, .flexibleWidth]
        overlayView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        overlayView.center = view.center
        overlayView.autoresizingMask = [.flexibleLeftMargin,.flexibleTopMargin,.flexibleRightMargin,.flexibleBottomMargin]
        overlayView.backgroundColor = UIColor.black
        overlayView.clipsToBounds = true
        overlayView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.activityIndicatorViewStyle = .whiteLarge
        activityIndicator.center = CGPoint(x: overlayView.bounds.width / 2, y: overlayView.bounds.height / 2)
        
        overlayView.addSubview(activityIndicator)
        view.addSubview(bgView)
        activityIndicator.startAnimating()
    }
    func hideActivityIndicator(){
        activityIndicator.stopAnimating()
        bgView.removeFromSuperview()
    }
    
    func showAlert(_ titulo: String, mensaje: String){
        
        let alerta = UIAlertController(title: titulo, message: mensaje, preferredStyle: UIAlertControllerStyle.alert)
        alerta.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil));
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alerta, animated: true, completion: nil)
    }
    
    func devuelveNombreMes(mes : String) -> String{
        
        switch mes {
        case "01":
            return "Enero"
        case "02":
            return "Febrero"
        case "03":
            return "Marzo"
        case "04":
            return "Abril"
        case "05":
            return "Mayo"
        case "06":
            return "Junio"
        case "07":
            return "Julio"
        case "08":
            return "Agosto"
        case "09":
            return "Setiembre"
        case "10":
            return "Octubre"
        case "11":
            return "Noviembre"
        case "12":
            return "Diciembre"
        case "1":
            return "Enero"
        case "2":
            return "Febrero"
        case "3":
            return "Marzo"
        case "4":
            return "Abril"
        case "5":
            return "Mayo"
        case "6":
            return "Junio"
        case "7":
            return "Julio"
        case "8":
            return "Agosto"
        case "9":
            return "Setiembre"
        default:
            return "Mes"
        }
        
    }
    
    func devuelveNombreMesParticionado(mes : String) -> String{
        
        switch mes {
        case "01":
            return "ENE"
        case "02":
            return "FEB"
        case "03":
            return "MAR"
        case "04":
            return "ABR"
        case "05":
            return "MAY"
        case "06":
            return "JUN"
        case "07":
            return "JUL"
        case "08":
            return "AGO"
        case "09":
            return "SET"
        case "10":
            return "OCT"
        case "11":
            return "NOV"
        case "12":
            return "DIC"
        case "1":
            return "ENE"
        case "2":
            return "FEB"
        case "3":
            return "MAR"
        case "4":
            return "ABR"
        case "5":
            return "MAY"
        case "6":
            return "JUN"
        case "7":
            return "JUL"
        case "8":
            return "AGO"
        case "9":
            return "SET"
        default:
            return "Mes"
        }
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
