//
//  EventosFbUtilsViewController.swift
//  Sentinel
//
//  Created by Daniel on 21/08/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
import FBSDKCoreKit

struct EVENTFB {
    static let EVENT_NAME_INITIATED_APP : String = "fb_mobile_initiated_app"
    static let EVENT_NAME_INITIATED_REGISTRATION : String = "fb_mobile_initiated_registration"
    static let EVENT_NAME_LOGIN : String = "fb_mobile_login"
    static let EVENT_NAME_FREE_CONSULTATION : String = "fb_mobile_free_consultation"
    static let EVENT_NAME_CONSULTATION: String = "fb_mobile_consultation"
    static let EVENT_NAME_SEARCH : String = "fb_mobile_search"
    static let EVENT_NAME_PURCHASE_INFO: String = "fb_mobile_purchase_info"
    static let EVENT_NAME_INITIATED_PURCHASE : String = "fb_mobile_initiated_purchase"
    
    static let EVENT_NAME_COMPLETED_REGISTRATION = "fb_mobile_complete_registration"
    static let EVENT_PARAM_REGISTRATION_METHOD = "fb_registration_method";
    static let EVENT_PARAM_CONTENT_ID = "fb_content_id";
    static let EVENT_PARAM_CONTENT_TYPE = "fb_content_type";
}

class EventosFbUtilsViewController: UIViewController {
    
    public func logInitiatedAppEvent (){
        FBSDKAppEvents.logEvent(EVENTFB.EVENT_NAME_INITIATED_APP)
    }
    public func logInitiateRegistratioEvent(){
        FBSDKAppEvents.logEvent(EVENTFB.EVENT_NAME_INITIATED_REGISTRATION)
    }
    public func logCompletedRegistrationEvent(){
        let dict : NSMutableDictionary = NSMutableDictionary()
        dict.setValue("ios", forKey: EVENTFB.EVENT_PARAM_REGISTRATION_METHOD)
        FBSDKAppEvents.logEvent(EVENTFB.EVENT_NAME_COMPLETED_REGISTRATION, parameters: dict as? [AnyHashable : Any])
    }
    public func logLoginEvent(){
        FBSDKAppEvents.logEvent(EVENTFB.EVENT_NAME_LOGIN)
    }
    public func logPurchaseInfoEvent(contentID : String){
        let dict : NSMutableDictionary = NSMutableDictionary()
        dict.setValue(contentID, forKey: EVENTFB.EVENT_PARAM_CONTENT_ID)
        FBSDKAppEvents.logEvent(EVENTFB.EVENT_NAME_PURCHASE_INFO, parameters: dict as? [AnyHashable : Any])
    }
    public func logInitiatedPurchaseEvent(contentID:String){
        let dict : NSMutableDictionary = NSMutableDictionary()
        dict.setValue(contentID, forKey: EVENTFB.EVENT_PARAM_CONTENT_ID)
        FBSDKAppEvents.logEvent(EVENTFB.EVENT_NAME_INITIATED_PURCHASE, parameters: dict as? [AnyHashable : Any])
    }
    public func logPurchasedEvent(contentID:String, purchaseAmount:Double){
        let dict : NSMutableDictionary = NSMutableDictionary()
        dict.setValue(contentID, forKey: EVENTFB.EVENT_PARAM_CONTENT_ID)
        FBSDKAppEvents.logPurchase(purchaseAmount, currency: "PEN", parameters: dict as? [AnyHashable : Any])
    }
    public func logFreeConsultationEvent (){
        FBSDKAppEvents.logEvent(EVENTFB.EVENT_NAME_FREE_CONSULTATION)
    }
    public func logConsultationEvent(contentID:String){
        let dict : NSMutableDictionary = NSMutableDictionary()
        dict.setValue(contentID, forKey: EVENTFB.EVENT_PARAM_CONTENT_ID)
        FBSDKAppEvents.logEvent(EVENTFB.EVENT_NAME_CONSULTATION, parameters: dict as? [AnyHashable : Any])
    }
    public func logSearchEvent(contentID:String){
        let dict : NSMutableDictionary = NSMutableDictionary()
        dict.setValue(contentID, forKey: EVENTFB.EVENT_PARAM_CONTENT_ID)
        FBSDKAppEvents.logEvent(EVENTFB.EVENT_NAME_SEARCH, parameters: dict as? [AnyHashable : Any])
    }
}
