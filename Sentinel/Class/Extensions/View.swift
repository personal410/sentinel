//
//  View.swift
//  Sentinel
//
//  Created by Daniel on 25/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import Foundation
import UIKit
@IBDesignable extension UIView {
    @IBInspectable var borderColor:UIColor? {
        set{
            layer.borderColor = newValue?.cgColor
            layer.borderWidth = 1
        }
        get{
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }else{
                return nil
            }
        }
    }
    class func viewFromNibName(_ name: String) -> UIView? {
        let views = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
        return views?.first as? UIView
    }
}
