//
//  MenuPrincipalTableViewCell.swift
//  Sentinel
//
//  Created by Daniel on 5/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class MenuPrincipalTableViewCell: UITableViewCell {

    @IBOutlet weak var tituloLB: UILabel!
    @IBOutlet weak var detallelb: UILabel!
    @IBOutlet weak var iconIMG: UIButton!
    @IBOutlet weak var circularView: UIView!
    @IBOutlet weak var detalleCompleto: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //self.circularView.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
