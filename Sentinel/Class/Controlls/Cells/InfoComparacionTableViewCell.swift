//
//  InfoComparacionTableViewCell.swift
//  Sentinel
//
//  Created by Daniel on 19/11/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class InfoComparacionTableViewCell: UITableViewCell {
    @IBOutlet weak var infoGeneralLB: UILabel!
    @IBOutlet weak var primerIcono: UIButton!
    @IBOutlet weak var segundoIcono: UIImageView!
    @IBOutlet weak var tercerIcono: UIImageView!
    @IBOutlet weak var detalleTercerIcono: UILabel!
    @IBOutlet weak var detalleSegundoIcono: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
