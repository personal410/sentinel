//
//  BusquedaPersonasTableViewCell.swift
//  Sentinel
//
//  Created by Daniel on 18/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class BusquedaPersonasTableViewCell: UITableViewCell {

    @IBOutlet weak var resultadoView: UIView!
    @IBOutlet weak var nombreLB: UILabel!
    @IBOutlet weak var dniLB: UILabel!
    @IBOutlet weak var fechaLB: UILabel!
    @IBOutlet weak var buttonBT: UIButton!
    @IBOutlet weak var guionLB: UILabel!
    
    @IBOutlet weak var anchoNumeroDocumento: NSLayoutConstraint!//135 - 105
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.estilos()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func estilos (){
        self.resultadoView.layer.cornerRadius = 5
        self.resultadoView.layer.shadowColor = UIColor.lightGray.cgColor
        self.resultadoView.layer.shadowOpacity = 0.5
        self.resultadoView.layer.shadowOffset = CGSize.zero
    }
}
