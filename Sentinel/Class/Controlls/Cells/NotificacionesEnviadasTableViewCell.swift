//
//  NotificacionesEnviadasTableViewCell.swift
//  Sentinel
//
//  Created by Richy on 25/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class NotificacionesEnviadasTableViewCell:UITableViewCell {
    @IBOutlet weak var lblNombres:UILabel!
    @IBOutlet weak var lblMonto:UILabel!
    @IBOutlet weak var lblCantDias:UILabel!
    @IBOutlet weak var lblDias:UILabel!
    @IBOutlet weak var lblPlazo:UILabel!
    @IBOutlet weak var vDias:UIView!
    @IBOutlet weak var btnPagar:UIButton!
    
    func bind(with dic:[String: Any], in ind:Int){
        if let tipoDoc = dic["CTTDocCPT"] as? String {
            if tipoDoc == "D" {
                if let nombres = dic["CTNomCPT"] as? String, let apePat = dic["CTApePatCPT"] as? String, let apeMat = dic["CTApeMatCPT"] as? String {
                    lblNombres.text = "\(nombres) \(apePat) \(apeMat)"
                }else{
                    lblNombres.text = ""
                }
            }else{
                if let razonSocial = dic["CTNomRazSocCPT"] as? String {
                    lblNombres.text = razonSocial
                }else{
                    lblNombres.text = ""
                }
            }
        }else{
            lblNombres.text = ""
        }
        if let monto = dic["CTMonDeu"] as? String {
            lblMonto.text = monto
        }else{
            lblMonto.text = ""
        }
        if let fecLimite = dic["CTFecPlLim"] as? String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            if let date = dateFormatter.date(from: fecLimite) {
                let dateComps = Calendar.current.dateComponents([.day], from: Date(), to: date)
                let cantDias = dateComps.day! + 1
                if cantDias > 0 {
                    lblCantDias.text = String(cantDias)
                    lblDias.text = "dia\(cantDias == 1 ? "" : "s")"
                    vDias.isHidden = false
                    lblPlazo.text = "Plazo vence en: "
                }else{
                    vDias.isHidden = true
                    lblPlazo.text = "Plazo: Vencido"
                }
            }else{
                vDias.isHidden = true
                lblPlazo.text = ""
            }
        }else{
            vDias.isHidden = true
            lblPlazo.text = ""
        }
        btnPagar.layer.cornerRadius = 5
        btnPagar.layer.borderColor = UIColor.black.cgColor
        btnPagar.layer.borderWidth = 1
        if let pago = dic["CTEstNot"] as? String {
            if pago == "Y" {
                btnPagar.isEnabled = false
                btnPagar.backgroundColor = UIColor.white
                btnPagar.layer.borderColor = UIColor.lightGray.cgColor
            }else{
                btnPagar.backgroundColor = UIColor(hex: "f1f1f1")
                btnPagar.layer.borderColor = UIColor.black.cgColor
                btnPagar.isEnabled = true
            }
        }else{
            btnPagar.isEnabled = false
            btnPagar.backgroundColor = UIColor.white
            btnPagar.layer.borderColor = UIColor.lightGray.cgColor
        }
        btnPagar.tag = ind
    }
}
