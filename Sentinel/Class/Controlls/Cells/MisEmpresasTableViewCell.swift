//
//  MisEmpresasTableViewCell.swift
//  Sentinel
//
//  Created by Daniel on 25/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class MisEmpresasTableViewCell: UITableViewCell {

    @IBOutlet weak var tercerView: UIView!
    @IBOutlet weak var tituloLB: UILabel!
    @IBOutlet weak var semaforoIMG: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.tercerView.layer.cornerRadius = 5
        
        tercerView.layer.shadowColor = UIColor.lightGray.cgColor
        tercerView.layer.shadowOpacity = 0.5
        tercerView.layer.shadowOffset = CGSize.zero
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
