//
//  MiReporteTableViewCell.swift
//  App_Seninel
//
//  Created by Richy on 29/08/18.
//  Copyright © 2018 rmdesign. All rights reserved.
//

import UIKit

class MiReporteTableViewCell: UITableViewCell {

    @IBOutlet weak var personaLB: UILabel!
    @IBOutlet weak var fechaLB: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
