//
//  SFCronogramaTableViewCell.swift
//  Sentinel
//
//  Created by Daniel on 12/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class SFCronogramaTableViewCell: UITableViewCell {

    @IBOutlet weak var nroCronoLB: UILabel!
    @IBOutlet weak var fechaPagoLB: UILabel!
    @IBOutlet weak var periodoLB: UILabel!
    @IBOutlet weak var saldoCronoLB: UILabel!
    @IBOutlet weak var amortizacionLB: UILabel!
    @IBOutlet weak var interesLB: UILabel!
    @IBOutlet weak var cuotaLB: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
