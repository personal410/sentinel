//
//  imageCollectionViewCell.swift
//  App_Seninel
//
//  Created by Richy on 29/08/18.
//  Copyright © 2018 rmdesign. All rights reserved.
//

import UIKit

class imageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgIV: UIImageView!
    @IBOutlet weak var mesLB: UILabel!
    
}
