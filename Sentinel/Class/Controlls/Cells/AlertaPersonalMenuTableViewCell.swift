//
//  AlertaPersonalMenuTableViewCell.swift
//  Sentinel
//
//  Created by Daniel on 14/11/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class AlertaPersonalMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var tituloLB: UILabel!
    @IBOutlet weak var variacionIMG: UIImageView!
    @IBOutlet weak var fechaLB: UILabel!
    @IBOutlet weak var alertaView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.alertaView.layer.cornerRadius = 5
        
        self.alertaView.layer.borderColor = UIColor.lightGray.cgColor
        self.alertaView.layer.borderWidth = 1
        self.alertaView.layer.shadowColor = UIColor.lightGray.cgColor
        self.alertaView.layer.shadowOpacity = 0.5
        self.alertaView.layer.shadowOffset = CGSize.zero
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
