//
//  DetalleDeudaTableViewCell.swift
//  App_Seninel
//
//  Created by Richy on 31/08/18.
//  Copyright © 2018 rmdesign. All rights reserved.
//

import UIKit

class DetalleDeudaTableViewCell: UITableViewCell {

    @IBOutlet weak var descripcionLB: UILabel!
    @IBOutlet weak var deudaLB: UILabel!
    @IBOutlet weak var fechaInf: UILabel!
    @IBOutlet weak var califLB: UILabel!
    @IBOutlet weak var diasVencidos: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
