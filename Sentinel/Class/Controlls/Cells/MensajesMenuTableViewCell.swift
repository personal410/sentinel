//
//  MensajesMenuTableViewCell.swift
//  Sentinel
//
//  Created by Daniel on 12/11/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class MensajesMenuTableViewCell:UITableViewCell {
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var tituloLB: UILabel!
    @IBOutlet weak var lblIsNew: UILabel!
    @IBOutlet weak var fechaLB: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewContent.layer.cornerRadius = 5
        self.viewContent.layer.borderColor = UIColor.lightGray.cgColor
        self.viewContent.layer.borderWidth = 0.5
        self.viewContent.layer.shadowColor = UIColor.lightGray.cgColor
        self.viewContent.layer.shadowOpacity = 0.5
        self.viewContent.layer.shadowOffset = CGSize.zero
    }
}
