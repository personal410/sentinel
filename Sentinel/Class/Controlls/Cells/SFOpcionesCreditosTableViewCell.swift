//
//  SFOpcionesCreditosTableViewCell.swift
//  Sentinel
//
//  Created by Daniel on 11/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class SFOpcionesCreditosTableViewCell: UITableViewCell {

    @IBOutlet weak var opcionesView: UIView!
    @IBOutlet weak var plazoLB: UILabel!
    @IBOutlet weak var cuotaLB: UILabel!
    @IBOutlet weak var montoLB: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        opcionesView.layer.cornerRadius = 10.0
        opcionesView.layer.shadowColor = UIColor.black.cgColor
        opcionesView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        opcionesView.layer.shadowRadius = 1.5
        opcionesView.layer.shadowOpacity = 0.2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
