//
//  SFProductosTableViewCell.swift
//  Sentinel
//
//  Created by Daniel on 5/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class SFProductosTableViewCell: UITableViewCell {

    @IBOutlet weak var productosVW: UIView!
    @IBOutlet weak var tituloLB: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.productosVW.layer.cornerRadius = 5
        self.productosVW.layer.shadowColor = UIColor.lightGray.cgColor
        self.productosVW.layer.shadowOpacity = 0.5
        self.productosVW.layer.shadowOffset = CGSize.zero
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
