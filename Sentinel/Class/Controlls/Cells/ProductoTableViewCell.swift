//
//  ProductoTableViewCell.swift
//  Sentinel
//
//  Created by Daniel on 4/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
class ProductoTableViewCell:UITableViewCell {
    @IBOutlet weak var bannerIMG:UIImageView!
    @IBOutlet weak var mainVW:UIView!
    @IBOutlet weak var tituloLB:UILabel!
    @IBOutlet weak var subTituloLB:UILabel!
    @IBOutlet weak var verMasBT:UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        verMasBT.layer.cornerRadius = 5
        mainVW.layer.cornerRadius = 15
        bannerIMG.layer.cornerRadius = 15
    }
}
