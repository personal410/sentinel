//
//  NuevoMenuTableViewCell.swift
//  Sentinel
//
//  Created by Daniel on 6/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import UIKit
class NuevoMenuTableViewCell:UITableViewCell {
    @IBOutlet weak var tituloLB:UILabel!
    @IBOutlet weak var vNotificaciones:UIView!
    @IBOutlet weak var detalleLB:UILabel!
    @IBOutlet weak var imageMenu:UIImageView!
}
