//
//  ConfiguracionTableViewCell.swift
//  Sentinel
//
//  Created by Daniel on 6/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class ConfiguracionTableViewCell: UITableViewCell {

    @IBOutlet weak var tituloLB: UILabel!
    @IBOutlet weak var imgConfig: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
