//
//  AlertaTableViewCell.swift
//  Sentinel
//
//  Created by Daniel on 23/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class AlertaTableViewCell: UITableViewCell {

    @IBOutlet weak var logoIV: UIImageView!
    @IBOutlet weak var montoLB: UILabel!
    @IBOutlet weak var fechaLB: UILabel!
    @IBOutlet weak var estadoSW: UISwitch!
    @IBOutlet weak var empresaLB: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
