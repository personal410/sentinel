//
//  SFCantidadPropuestaTableViewCellDos.swift
//  Sentinel
//
//  Created by Daniel on 10/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class SFCantidadPropuestaTableViewCellDos: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var celdaView: UIView!
    @IBOutlet weak var saldoLB: UILabel!
    @IBOutlet weak var entidadlb: UILabel!
    @IBOutlet weak var creditoLB : UILabel!
    @IBOutlet weak var porcentajeTX: UITextField!
    @IBOutlet weak var porcentajeView: UIView!
    @IBOutlet weak var nuevoSaldoLB: UILabel!
    @IBOutlet weak var montoLB: UILabel!
    
    @IBOutlet weak var numeroDocEntidadTX: UITextField!
    @IBOutlet weak var tipoCreditoTX: UITextField!
    @IBOutlet weak var tipoDocEntidadTX: UITextField!
    
    var nuevoSaldo : Double = 0.0
    var monto : Double = 0.0
    var primerMonto : Double = 0.0
    var montoDosDecimal : String = ""
    var nuevoSaldoDosDecimal : String = ""
    var porcentaje : Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.porcentajeTX.delegate = self
        
        celdaView.layer.cornerRadius = 5.0
        celdaView.layer.shadowColor = UIColor.black.cgColor
        celdaView.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        celdaView.layer.shadowRadius = 2.5
        celdaView.layer.shadowOpacity = 0.2
        
        porcentajeView.layer.borderColor = UIColor.gray.cgColor
        porcentajeView.layer.borderWidth = 1.5
        porcentajeView.layer.cornerRadius = 5.0
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func accionNuevoSaldo(_ sender: Any) {
        if self.porcentajeTX.text == ""{
            self.porcentajeTX.text = "0"
        }
        porcentaje = Int(self.porcentajeTX.text!)!
        
        var newString = self.porcentajeTX.text
        if newString == "01"{
            newString = "1"
        }
        if newString == "02"{
            newString = "2"
        }
        if newString == "03"{
            newString = "3"
        }
        if newString == "04"{
            newString = "4"
        }
        if newString == "05"{
            newString = "5"
        }
        if newString == "06"{
            newString = "6"
        }
        if newString == "07"{
            newString = "7"
        }
        if newString == "08"{
            newString = "8"
        }
        if newString == "09"{
            newString = "9"
        }
        
        if (newString?.count)! >= 4 {
            var myRange: NSRange = NSMakeRange(0, 3)
            self.porcentajeTX.text?.replacingCharacters(in: Range(myRange, in: self.porcentajeTX.text!)! , with: "0")
        }
        
        if porcentaje <= 100 {
            monto = Double(self.saldoLB.text!)! * Double(porcentaje) / 100
            nuevoSaldo = Double(self.saldoLB.text!)! - monto
            
            let formatterSaldo = NumberFormatter()
            formatterSaldo.maximumFractionDigits = 2
            formatterSaldo.roundingMode = .down
            
            nuevoSaldoDosDecimal = formatterSaldo.string(from: nuevoSaldo as NSNumber)!
            
            let formatterMonto = NumberFormatter()
            formatterMonto.maximumFractionDigits = 2
            formatterMonto.roundingMode = .down
            
            montoDosDecimal = formatterMonto.string(from: monto as NSNumber)!
            
            self.nuevoSaldoLB.text = nuevoSaldoDosDecimal
            self.montoLB.text = montoDosDecimal
        }
        else
        {
            self.porcentajeTX.text = ""
            self.nuevoSaldoLB.text = self.saldoLB.text
            self.montoLB.text = "0"
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var newString = ""
        if textField == self.porcentajeTX {
            newString = (self.porcentajeTX.text?.replacingCharacters(in: Range(range, in: self.porcentajeTX.text!)! , with: string))!
        }
        return !(newString.count > 3)
    }
}
