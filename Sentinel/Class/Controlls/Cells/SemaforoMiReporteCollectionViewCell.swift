//
//  SemaforoMiReporteCollectionViewCell.swift
//  Sentinel
//
//  Created by Daniel on 25/09/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class SemaforoMiReporteCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var indexBT: UIButton!
    @IBOutlet weak var mesLB: UILabel!
    @IBOutlet weak var semaIMG: UIImageView!
}
