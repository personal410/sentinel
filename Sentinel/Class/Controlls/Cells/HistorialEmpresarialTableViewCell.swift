//
//  HistorialEmpresarialTableViewCell.swift
//  Sentinel
//
//  Created by Daniel Montoya on 1/10/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//

import UIKit

class HistorialEmpresarialTableViewCell: UITableViewCell {

    @IBOutlet weak var nombreLB: UILabel!
    @IBOutlet weak var fechaLB: UILabel!
    @IBOutlet weak var tipoDetalleLB: UILabel!
    @IBOutlet weak var semaforoIMG: UIImageView!
    @IBOutlet weak var historialBT: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
