//
//  NotificationHUD.swift
//  Sentinel
//
//  Created by Victor Salazar on 15/10/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import UIKit
class NotificationHUD:UIView{
    static var currentHUD:UIView?
    static func showNotificionHUDView(with title:String, and text:String, on type:Int){
        let application = UIApplication.shared
        let window = application.windows.first!
        let margin = 10
        let font13 = UIFont.systemFont(ofSize: 13)
        let font15 = UIFont.systemFont(ofSize: 15)
        let font17 = UIFont.boldSystemFont(ofSize: 17)
        let colorBlack1 = UIColor(hex: "1C1C1C")
        let colorBlack2 = UIColor(hex: "474747")
        let windowWidth = window.frame.width
        
        let strTitle = title as NSString
        let strText = text as NSString
        let maxSize = CGSize(width: windowWidth - CGFloat(40 + 2 * margin), height: CGFloat(Float.infinity))
        let boundsTitle = strTitle.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font17], context: nil)
        let boundsText = strText.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font15], context: nil)
        
        let view = UIView(frame: CGRect(x: margin, y: margin + Int(window.safeAreaInsets.top), width: Int(windowWidth) - 2 * margin, height: Int(boundsTitle.height + boundsText.height) + 60))
        view.backgroundColor = .white
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowRadius = 5
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
        view.layer.shadowOpacity = 0.75
        view.layer.cornerRadius = 5
        
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        btn.tag = type
        btn.addTarget(self, action: #selector(didTouch(_:)), for: .touchUpInside)
        
        let ivIcon = UIImageView(frame: CGRect(x: 20, y: 10, width: 20, height: 20))
        ivIcon.image = UIImage(named: "ic_eye")
        let lblApp = UILabel()
        lblApp.text = "Sentinel"
        lblApp.font = font13
        lblApp.sizeToFit()
        lblApp.textColor = colorBlack2
        lblApp.frame.origin.x = 50
        lblApp.frame.origin.y = 10
        lblApp.frame.size.height = 20
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let lblHour = UILabel()
        lblHour.text = dateFormatter.string(from: Date())
        lblHour.font = font13
        lblHour.sizeToFit()
        lblHour.textColor = colorBlack2
        lblHour.frame.origin.x = view.frame.width - lblHour.frame.width - 20
        lblHour.frame.origin.y = 10
        lblHour.frame.size.height = 20
        
        let lblTitle = UILabel(frame: CGRect(x: 20, y: 35, width: maxSize.width, height: boundsTitle.height))
        lblTitle.text = title
        lblTitle.font = font17
        lblTitle.numberOfLines = 0
        lblTitle.textColor = colorBlack1
        
        let lblText = UILabel(frame: CGRect(x: 20, y: 40 + boundsTitle.height, width: maxSize.width, height: boundsText.height))
        lblText.text = text
        lblText.font = font15
        lblText.numberOfLines = 0
        lblText.textColor = colorBlack2
        
        view.addSubview(btn)
        view.addSubview(ivIcon)
        view.addSubview(lblApp)
        view.addSubview(lblHour)
        view.addSubview(lblTitle)
        view.addSubview(lblText)
        currentHUD = view
        window.addSubview(currentHUD!)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            currentHUD?.removeFromSuperview()
            currentHUD = nil
        }
    }
    @objc static func didTouch(_ btn:UIButton){
        showNotificationFlow(with: btn.tag)
        currentHUD?.removeFromSuperview()
        currentHUD = nil
    }
    static func showNotificationFlow(with type:Int){
        if type > 0 {
            if let navView = UIApplication.shared.windows.first!.rootViewController as? UINavigationController {
                if navView.viewControllers.count == 0 {
                    return
                }
                if navView.viewControllers.count == 1 {
                    if let loginVC = navView.viewControllers.first as? LoginViewController {
                        loginVC.showNotificationType = type
                    }
                    return
                }
                if navView.viewControllers.count == 2 {
                    if let mainTabBarVC = navView.viewControllers[1] as? MainTabBarViewController {
                        mainTabBarVC.showNotification(type)
                    }
                    return
                }
            }
        }
    }
}
