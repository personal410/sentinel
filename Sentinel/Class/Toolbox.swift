//
//  Toolbox.swift
//  Sentinel
//
//  Created by Victor Salazar on 1/29/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
import Foundation
class Toolbox{
    static func getNumberFormmatter() -> NumberFormatter {
        let numberFormatter = NumberFormatter()
        numberFormatter.formatterBehavior = .behavior10_4
        numberFormatter.numberStyle = .decimal
        numberFormatter.minimumFractionDigits = 2
        numberFormatter.maximumFractionDigits = 2
        return numberFormatter
    }
    class func getCalendar() -> Calendar {
        var calendar = Calendar(identifier: .gregorian)
        calendar.locale = Locale(identifier: "en_US")
        return calendar
    }
    class func getMonthWithDate(date:Date, withDirection direction:Int) -> Date {
        var dateComps = DateComponents()
        dateComps.month = direction
        return self.getCalendar().date(byAdding: dateComps, to: date)!
    }
    class func normalizeDate(date:Date) -> Date {
        let calendar = self.getCalendar()
        var dateComps = calendar.dateComponents([.month, .year], from: date as Date)
        dateComps.day = 1
        return calendar.date(from: dateComps)!
    }
    class func getDateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es_ES")
        dateFormatter.calendar = self.getCalendar() as Calendar
        return dateFormatter
    }
    class func getDate(date:Date, withFormat format:String) -> String {
        let dateFormatter = self.getDateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
    class func showAlert(with title:String, and mesage:String, withBtnTitle btnTitle:String = "OK", withOkAction completionhandler:(() -> Void)? = nil, in viewCont:UIViewController){
        let alertCont = UIAlertController(title: title, message: mesage, preferredStyle: .alert)
        alertCont.addAction(UIAlertAction(title: btnTitle, style: .default, handler: { (action) in
            completionhandler?()
        }))
        viewCont.present(alertCont, animated: true, completion: nil)
    }
    class func validateEmail(_ email:String) -> Bool{
        if email.isEmpty {
            return false
        }
        let emailReg = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailReg)
        return emailTest.evaluate(with: email)
    }
    class func didSelectText(in lbl:UILabel, with gesture:UITapGestureRecognizer, in range:NSRange) -> Bool {
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: lbl.attributedText!)
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        textContainer.lineFragmentPadding = 0
        textContainer.lineBreakMode = lbl.lineBreakMode
        textContainer.maximumNumberOfLines = lbl.numberOfLines
        let labelSize = lbl.bounds.size
        textContainer.size = labelSize
        
        let locationOfTouchInLabel = gesture.location(in: lbl)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let txtContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - txtContainerOffset.x, y: locationOfTouchInLabel.y - txtContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, range)
    }
    class func enviarFotoSOAP(con archivo:String, nombre nombreArchivo:String, completition handler:@escaping ((_ error:Error?) -> Void)){
        let userGlobal = UserGlobalData.sharedInstance.userGlobal!
        let soap = SOAPEngine()
        soap.userAgent = "SOAPEngine"
        soap.actionNamespaceSlash = true
        soap.requestTimeout = 120
        soap.licenseKey = Constants.SOAP_KEY
        soap.setValue(userGlobal.user!, forKey: "usuario")
        soap.setValue(userGlobal.SesionId!, forKey: "sesionId")
        soap.setValue(5, forKey: "empresaTipDoc")
        soap.setValue("", forKey: "empresaNroDoc")
        soap.setValue(0, forKey: "solicitudCodigo")
        soap.setValue("", forKey: "solicitudCodigoEmp")
        soap.setValue(0, forKey: "codigoFoto")
        soap.setValue("", forKey: "latitud")
        soap.setValue("", forKey: "longitud")
        soap.setValue("IOS", forKey: "plataforma")
        soap.setValue(archivo, forKey: "file")
        soap.setValue(nombreArchivo, forKey: "fileName")
        soap.requestURL("http://www2.sentinelperu.com/ManagerFileServer/WsFileServer.asmx", soapAction: "http://sentinelperu.com/uploadFile", completeWithDictionary:{(status, result) in
            handler(nil)
        }){(er) in
            print("enviarFotoSOAP error: \(er.debugDescription)")
            handler(er ?? SentinelError())
        }
    }
    class func disableAutocompletition(of view:UIView){
        for childView in view.subviews {
            if let textField = childView as? UITextField {
                textField.autocorrectionType = .no
            }
            if !childView.subviews.isEmpty {
                disableAutocompletition(of: childView)
            }
        }
    }
}
struct SentinelError:Error {}
