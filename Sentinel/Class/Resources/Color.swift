//
//  Color.swift
//  TerraMystica
//
//  Created by victor salazar on 4/2/18.
//  Copyright © 2018 victor salazar. All rights reserved.
//
import UIKit
extension UIColor{
    convenience init(_ red:Int, _ green:Int, _ blue:Int){
        let r = CGFloat(red) / 256.0
        let g = CGFloat(green) / 256.0
        let b = CGFloat(blue) / 256.0
        self.init(red: r, green: g, blue: b, alpha: 1)
    }
    convenience init(hex hexColor:String){
        let indexEndRed = hexColor.index(hexColor.startIndex, offsetBy: 2)
        let indexEndGreen = hexColor.index(indexEndRed, offsetBy: 2)
        let indexEndBlue = hexColor.index(indexEndGreen, offsetBy: 2)
        let sRed = hexColor[hexColor.startIndex ..< indexEndRed]
        let sGreen = hexColor[indexEndRed ..< indexEndGreen]
        let sBlue = hexColor[indexEndGreen ..< indexEndBlue]
        let red = UIColor.transformHexToInt(hex: String(sRed))
        let green = UIColor.transformHexToInt(hex: String(sGreen))
        let blue = UIColor.transformHexToInt(hex: String(sBlue))
        self.init(red, green, blue)
    }
    
    class func transformHexToInt(hex value:String) -> Int {
        let hexValues:[Character] = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"]
        let sec = hexValues.index(of: value.uppercased().first!)!
        let first = hexValues.index(of: value.uppercased().last!)!
        return sec * 16 + first
    }
    convenience init(r:Int, g:Int, b:Int, a:CGFloat = 1){
        let red = CGFloat(r) / 256.0
        let green = CGFloat(g) / 256.0
        let blue = CGFloat(b) / 256.0
        self.init(red: red, green: green, blue: blue, alpha: a)
    }
    convenience init(rgb:Int, a:CGFloat = 1){
        self.init(r: rgb, g: rgb, b: rgb, a: a)
    }
}
