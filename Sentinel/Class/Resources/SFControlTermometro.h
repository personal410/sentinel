//
//  SFControlTermometro.h
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 11/14/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SFControlTermometro.h"

@interface SFControlTermometro : UIView

@property (weak, nonatomic) IBOutlet UILabel *nivelRiesgoLB;
@property (weak, nonatomic) IBOutlet UILabel *numeroTermometroLB;
@property (weak, nonatomic) IBOutlet UIImageView *flechaTermometroIMG;
@property (weak, nonatomic) IBOutlet UIImageView *termometroIMG;
@property (weak, nonatomic) IBOutlet UIView *indicadorTermometroVI;
@property (weak, nonatomic) IBOutlet UIView *contenedorTermometroVI;
@property (strong, nonatomic) IBOutlet UIView *viewFlechaT;
@property (strong, nonatomic) IBOutlet UIView *viewNumeroT;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *espacioTopIndiceLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *alturaIndiceLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *espacioBotIndiceLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topFlechaLC;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topNumeroLC;


-(void)formatoTermometro:(NSMutableDictionary*) SDTSabioFinanciero;

@end
