//
//  CalendarDayBean.swift
//  Tuit-Inves
//
//  Created by Juan Alberto Carlos Vera on 5/5/16.
//  Copyright © 2016 Orbita. All rights reserved.
//

import UIKit

class JCVCalendarDayBean: NSObject {

    var id: Int = 0
    var date: NSDate = NSDate()
    var dateString: String = ""
    var number: Int = 0
    var data: [JCVDataBean] = [JCVDataBean]()
    var isCurrentMonth: Bool = false
    
    var dayNum: String! = ""
    var dayName: String! = ""
    var monthNum: String! = ""
    var monthName: String! = ""
}
