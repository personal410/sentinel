//
//  AlertaBean.swift
//  Tuit-Inves
//
//  Created by Juan Alberto Carlos Vera on 5/5/16.
//  Copyright © 2016 Orbita. All rights reserved.
//

import UIKit

class JCVDataBean: NSObject {

    var id: Int = 0
    var date: NSDate = NSDate()
    var dateString: String = ""
    var value: Double = 0
    var state: String = ""
    var valueBackgroundColor: UIColor = UIColor()
    var valueTextColor: String = ""
    var object: AnyObject = "" as AnyObject
}
