//
//  NSDate.swift
//  Tuit-Inves
//
//  Created by Juan Alberto Carlos Vera on 5/5/16.
//  Copyright © 2016 Orbita. All rights reserved.
//

import UIKit

extension NSDate {
    
    func numberOfMondaysInMonth(month: Int, forYear year: Int) -> Int?
    {
        let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        calendar.firstWeekday = 2 // 2 == Monday
        
        // First monday in month:
        let comps = NSDateComponents()
        comps.month = month
        comps.year = year
        comps.weekday = calendar.firstWeekday
        comps.weekdayOrdinal = 1
        guard let first = calendar.date(from: comps as DateComponents)  else {
            return nil
        }
        
        // Last monday in month:
        comps.weekdayOrdinal = -1
        guard let last = calendar.date(from: comps as DateComponents)  else {
            return nil
        }
        
        // Difference in weeks:
        let weeks = calendar.components(.weekOfMonth, from: first, to: last, options: [])
        return weeks.weekOfMonth! + 1
    }
    
    func getDayOfWeek(today:String)->Int {
        
        let formatter  = DateFormatter()
        //formatter.dateFormat = "yyyy-MM-dd"
        formatter.dateFormat = "dd-MM-yyyy"
        let todayDate = formatter.date(from: today)!
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let myComponents = myCalendar.components(NSCalendar.Unit.weekday, from: todayDate)
        let weekDay = myComponents.weekday
        
        switch weekDay {
        case 1:
            return 7 //"Sun"
        case 2:
            return 1 //"Mon"
        case 3:
            return 2 //"Tue"
        case 4:
            return 3 //"Wed"
        case 5:
            return 4 //"Thu"
        case 6:
            return 5 //"Fri"
        case 7:
            return 6 //"Sat"
        default:
            print("Error fetching days")
            return 0 //"Day"
        }
        
        //return weekDay
    }
    
    func getDayOfWeek_ByDate(todayDate:NSDate)->Int {
        
        let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let myComponents = myCalendar.components(NSCalendar.Unit.weekday, from: todayDate as Date)
        let weekDay = myComponents.weekday
        
        switch weekDay {
        case 1:
            return 7 //"Sun"
        case 2:
            return 1 //"Mon"
        case 3:
            return 2 //"Tue"
        case 4:
            return 3 //"Wed"
        case 5:
            return 4 //"Thu"
        case 6:
            return 5 //"Fri"
        case 7:
            return 6 //"Sat"
        default:
            print("Error fetching days")
            return 0 //"Day"
        }
        
        //return weekDay
    }
    
    func getDayOfWeek_ByComponent(myComponents:NSDateComponents)->Int {
        
        let weekDay = myComponents.weekday
        
        switch weekDay {
        case 1:
            return 7 //"Sun"
        case 2:
            return 1 //"Mon"
        case 3:
            return 2 //"Tue"
        case 4:
            return 3 //"Wed"
        case 5:
            return 4 //"Thu"
        case 6:
            return 5 //"Fri"
        case 7:
            return 6 //"Sat"
        default:
            print("Error fetching days")
            return 0 //"Day"
        }
        
        //return weekDay
    }

    
    func getFirstDateOfMonth(date: NSDate) -> NSDate {
        
        let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        let calendarUnits = NSCalendar.Unit.year.union(NSCalendar.Unit.month)
        let dateComponents = calendar?.components(calendarUnits, from: date as Date)
        
        let fistDateOfMonth = self.returnedDateForMonth(day: 1, month: dateComponents!.month!, year: dateComponents!.year!)
        
        return fistDateOfMonth!
    }
    
    func getLastDateOfMonth(date: NSDate) -> NSDate {
        
        let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        let calendarUnits = NSCalendar.Unit.year.union(NSCalendar.Unit.month)
        let dateComponents = calendar?.components(calendarUnits, from: date as Date)
        
        let lastDateOfMonth = self.returnedDateForMonth(day: 0, month: dateComponents!.month! + 1, year: dateComponents!.year!)
        
        return lastDateOfMonth!
    }
    
    func getLastDateOfLastMonth(date: NSDate) -> NSDate {
        
        let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        let calendarUnits = NSCalendar.Unit.year.union(NSCalendar.Unit.month)
        let dateComponents = calendar?.components(calendarUnits, from: date as Date)
        
        let lastDateOfMonth = self.returnedDateForMonth(day: 0, month: dateComponents!.month!, year: dateComponents!.year!)
        
        return lastDateOfMonth!
    }
    
    func getFirstDateOfNextMonth(date: NSDate) -> NSDate {
        
        let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        let calendarUnits = NSCalendar.Unit.year.union(NSCalendar.Unit.month)
        let dateComponents = calendar?.components(calendarUnits, from: date as Date)
        
        let fistDateOfMonth = self.returnedDateForMonth(day: 1, month: dateComponents!.month! + 1, year: dateComponents!.year!)
        
        return fistDateOfMonth!
    }
    
    func returnedDateForMonth(day: NSInteger, month: NSInteger, year: NSInteger) -> NSDate? {
        let components = NSDateComponents()
        components.day = day
        components.month = month
        components.year = year
        
        let gregorian = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        
        return gregorian!.date(from: components as DateComponents) as! NSDate
    }
    
    func returnedDateStringForMonth(day: NSInteger, month: NSInteger, year: NSInteger) -> String {
        
        let components = NSDateComponents()
        components.day = day
        components.month = month
        components.year = year
        
        let gregorian = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        
        let todaysDate = gregorian!.date(from: components as DateComponents)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let dateInFormat = dateFormatter.string(from: todaysDate!)
        
        return dateInFormat
    }
    
    func returnedDateStringForMonth(date: NSDate) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let dateInFormat = dateFormatter.string(from: date as Date)
        
        return dateInFormat
    }
    
    func returnedFirstDateStringForMonth_EN(date: NSDate) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateInFormat = dateFormatter.string(from: date as Date)
        
        return dateInFormat
    }
    
}
