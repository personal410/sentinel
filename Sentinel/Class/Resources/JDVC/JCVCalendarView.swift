//
//  JCVCalendarView.swift
//  Tuit-Inves
//
//  Created by Juan Alberto Carlos Vera on 5/14/16.
//  Copyright © 2016 Orbita. All rights reserved.
//

import UIKit

class JCVCalendarView: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet var monthNameLB: UILabel!
    @IBOutlet var daysHeaderCV: UICollectionView!
    @IBOutlet var daysDetailCV: UICollectionView!
    
    var monthsArray = [
        ["id" : "1", "name" : "Enero", "short" : "ENE"],
        ["id" : "2", "name" : "Febrero", "short" : "FEB"],
        ["id" : "3", "name" : "Marzo", "short" : "MAR"],
        ["id" : "4", "name" : "Abril", "short" : "ABR"],
        ["id" : "5", "name" : "Mayo", "short" : "MAY"],
        ["id" : "6", "name" : "Junio", "short" : "JUN"],
        ["id" : "7", "name" : "Julio", "short" : "JUL"],
        ["id" : "8", "name" : "Agosto", "short" : "AGO"],
        ["id" : "9", "name" : "Setiembre", "short" : "SEP"],
        ["id" : "10", "name" : "Octubre", "short" : "OCT"],
        ["id" : "11", "name" : "Noviembre", "short" : "NOV"],
        ["id" : "12", "name" : "Diciembre", "short" : "DIC"]
    ]
    var daysArray = [
        ["id" : "1", "name" : "Lunes", "short" : "L"],
        ["id" : "2", "name" : "Martes", "short" : "M"],
        ["id" : "3", "name" : "Miércoles", "short" : "M"],
        ["id" : "4", "name" : "Jueves", "short" : "J"],
        ["id" : "5", "name" : "Viernes", "short" : "V"],
        ["id" : "6", "name" : "Sábado", "short" : "S"],
        ["id" : "7", "name" : "Domingo", "short" : "D"],
        ]
    
    var daysOfMonth: [JCVCalendarDayBean] = [JCVCalendarDayBean]()
    var dataArray: [JCVDataBean] = [JCVDataBean]()
    
    var calendar: NSCalendar!
    var calendarUnits: NSCalendar.Unit!
    var dateComponentsDate: NSDateComponents!
    
    var currentDate: NSDate!
    var currentMonth: Int = 0
    var currentYear: Int = 0
    var backDate: NSDate!
    var backMonth: Int = 0
    var backYear: Int = 0
    var nextMonth: Int = 0
    var nextYear: Int = 0
    
    var numberOfWeeks: Int! = 0
    var numberFirstDay: Int! = 1
    
    var delegateCalendar: JCVCalendarViewDelegate!
    
    var totalCurrentMonth: Double = 0
    /*
    override func viewDidAppear(animated: Bool) {
        
        self.updateCalendar()
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        
        if (self.daysHeaderCV != nil)
        {
            if UIDevice.currentDevice().orientation.isLandscape.boolValue {
                print("Landscape")
                
                self.daysHeaderCV.reloadData()
                self.daysDetailCV.reloadData()
                
            } else {
                print("Portrait")
                
                self.daysHeaderCV.reloadData()
                self.daysDetailCV.reloadData()
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    */
    
    //Metodos
    
    func settingDemo()
    {
        self.setCalendar()
        self.setData()
        self.setCurrentMonth()
    }
    
    func settingDefault()
    {
        self.setCalendar()
        self.setCurrentMonth()
    }
    
    func viewWillTransitionToSize()
    {
        if (self.daysHeaderCV != nil)
        {
            if UIDevice.current.orientation.isLandscape {
                print("Landscape")
                
                self.daysHeaderCV.reloadData()
                self.daysDetailCV.reloadData()
                
            } else {
                print("Portrait")
                
                self.daysHeaderCV.reloadData()
                self.daysDetailCV.reloadData()
            }
        }
        
    }
    
    func setCalendar()
    {
        self.daysHeaderCV.dataSource = self
        self.daysHeaderCV.delegate = self
        
        self.daysDetailCV.dataSource = self
        self.daysDetailCV.delegate = self
        
        self.calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        self.calendarUnits = NSCalendar.Unit.day.union(.month).union(.year)
    }
    
    func setCurrentMonth()
    {
        //current
        let date = NSDate()
        let compDate = calendar.components(calendarUnits, from: date as Date)
        
        self.currentMonth = compDate.month! //5
        self.currentYear = compDate.year! //2016
        
        //back
        if (self.currentMonth == 1)
        {
            self.backMonth = 12
            self.backYear -= 1
        }
        else
        {
            self.backMonth = self.currentMonth - 1
            self.backYear = self.currentYear
        }
        
        //next
        if (self.currentMonth == 12)
        {
            self.nextMonth = 1
            self.nextYear += 1
        }
        else
        {
            self.nextMonth = self.currentMonth + 1
            self.nextYear = self.currentYear
        }
        
        self.backDate = NSDate().returnedDateForMonth(day: 1, month: self.backMonth, year: self.backYear)
        self.currentDate = NSDate().returnedDateForMonth(day: 1, month: self.currentMonth, year: self.currentYear)
        
        self.updateCalendar()
    }
    
    func setNextMonth()
    {
        //back
        self.backMonth = self.currentMonth
        self.backYear = self.currentYear
        
        //current
        if (self.currentMonth == 12)
        {
            self.currentMonth = 1
            self.currentYear += 1
        }
        else
        {
            self.currentMonth += 1
        }
        
        //next
        if (self.currentMonth == 12)
        {
            self.nextMonth = 1
            self.nextYear += 1
        }
        else
        {
            self.nextMonth = self.currentMonth + 1
            self.nextYear = self.currentYear
        }
        
        self.backDate = NSDate().returnedDateForMonth(day: 1, month: self.backMonth, year: self.backYear)
        self.currentDate = NSDate().returnedDateForMonth(day: 1, month: self.currentMonth, year: self.currentYear)
        
        self.updateCalendar()
    }
    
    func setBackMonth()
    {
        //next
        self.nextMonth = self.currentMonth
        self.nextYear = self.currentYear
        
        //current
        if (self.currentMonth == 1)
        {
            self.currentMonth = 12
            self.currentYear -= 1
        }
        else
        {
            self.currentMonth -= 1
        }
        
        //back
        if (self.currentMonth == 1)
        {
            self.backMonth = 12
            self.backYear -= 1
        }
        else
        {
            self.backMonth = self.currentMonth - 1
            self.backYear = self.currentYear
        }
        
        self.backDate = NSDate().returnedDateForMonth(day: 1, month: self.backMonth, year: self.backYear)
        self.currentDate = NSDate().returnedDateForMonth(day: 1, month: self.currentMonth, year: self.currentYear)
        
        self.updateCalendar()
    }
    
    func updateCalendar()
    {
        let lastDateBackMonth = NSDate().getLastDateOfMonth(date: self.backDate)
        let componentLastDateBackMonth = calendar.components(calendarUnits, from: lastDateBackMonth as Date)
        
        let lastDateCurrentMonth = NSDate().getLastDateOfMonth(date: self.currentDate)
        let componentLastDateCurrentMonth = calendar.components(calendarUnits, from: lastDateCurrentMonth as Date)
        
        self.dateComponentsDate = calendar.components(calendarUnits, from: self.currentDate as Date) as NSDateComponents
        
        //Set month name
        self.monthNameLB.text = "\(self.getMonthName(month: dateComponentsDate.month)) \(self.currentYear)"
        
        self.numberOfWeeks = NSDate().numberOfMondaysInMonth(month: dateComponentsDate.month, forYear: dateComponentsDate!.year)
        self.numberFirstDay = NSDate().getDayOfWeek_ByDate(todayDate: self.currentDate)
        
        if (numberFirstDay == 7){
            self.numberOfWeeks = self.numberOfWeeks + 1
        }
        
        //Preparamos el array de dias por mes
        //let totalDias = self.numeroSemanas * 7
        let totalDiasMes : Int = (componentLastDateCurrentMonth.day)!
        
        self.numberOfWeeks = totalDiasMes / 7
        
        if (totalDiasMes >= 28)
        {
            self.numberOfWeeks = self.numberOfWeeks + 1
        }
        if (self.numberFirstDay == 7)
        {
            self.numberOfWeeks = self.numberOfWeeks + 1
        }
        
        let totalDiasCal = self.numberOfWeeks * 7
        
        var calendarDayBean: JCVCalendarDayBean = JCVCalendarDayBean()
        self.daysOfMonth = [JCVCalendarDayBean]()
        
        var firstDayNum: Int = 0
        
        for i in 0 ..< self.numberFirstDay - 1
        {
            firstDayNum = componentLastDateBackMonth.day! - self.numberFirstDay
            firstDayNum += i + 2
            
            calendarDayBean = JCVCalendarDayBean()
            calendarDayBean.number = firstDayNum
            calendarDayBean.date = NSDate().returnedDateForMonth(day: firstDayNum, month: self.backMonth, year: self.backYear)!
            calendarDayBean.dateString = NSDate().returnedDateStringForMonth(date: calendarDayBean.date)
            
            calendarDayBean.dayName = self.getDayName(dayInWeek: NSDate().getDayOfWeek_ByDate(todayDate: calendarDayBean.date))
            let dcd = calendar.components(calendarUnits, from: calendarDayBean.date as Date)
            calendarDayBean.monthName = "\(self.getMonthName(month: dcd.month!))"
            
            self.daysOfMonth.append(calendarDayBean)
        }
        
        for i in 0 ..< totalDiasMes
        {
            firstDayNum = i + 1
            
            calendarDayBean = JCVCalendarDayBean()
            calendarDayBean.number = firstDayNum
            calendarDayBean.date = NSDate().returnedDateForMonth(day: firstDayNum, month: self.currentMonth, year: self.currentYear)!
            calendarDayBean.dateString = NSDate().returnedDateStringForMonth(date: calendarDayBean.date)
            calendarDayBean.isCurrentMonth = true
            
            calendarDayBean.dayName = self.getDayName(dayInWeek: NSDate().getDayOfWeek_ByDate(todayDate: calendarDayBean.date))
            let dcd = calendar.components(calendarUnits, from: calendarDayBean.date as Date)
            calendarDayBean.monthName = "\(self.getMonthName(month: dcd.month!))"
            
            self.daysOfMonth.append(calendarDayBean)
        }
        
        for i in 0 ..< totalDiasCal - totalDiasMes
        {
            firstDayNum = i + 1
            
            calendarDayBean = JCVCalendarDayBean()
            calendarDayBean.number = firstDayNum
            calendarDayBean.date = NSDate().returnedDateForMonth(day: firstDayNum, month: self.nextMonth, year: self.nextYear)!
            calendarDayBean.dateString = NSDate().returnedDateStringForMonth(date: calendarDayBean.date)
            
            calendarDayBean.dayName = self.getDayName(dayInWeek: NSDate().getDayOfWeek_ByDate(todayDate: calendarDayBean.date))
            let dcd = calendar.components(calendarUnits, from: calendarDayBean.date as Date)
            calendarDayBean.monthName = "\(self.getMonthName(month: dcd.month!))"
            
            self.daysOfMonth.append(calendarDayBean)
        }
        
        self.totalCurrentMonth = 0
        
        //Hacemos merge con las alertas
        for calendarDayBean2 in self.daysOfMonth
        {
            for dataBean2: JCVDataBean in self.dataArray
            {
                if (calendarDayBean2.dateString == dataBean2.dateString)
                {
                    calendarDayBean2.data.append(dataBean2)
                    
                    if (calendarDayBean2.isCurrentMonth)
                    {
                        self.totalCurrentMonth += dataBean2.value
                    }
                }
            }
        }
        
        self.daysHeaderCV.reloadData()
        self.daysDetailCV.reloadData()
        
        self.delegateCalendar.didAfterUpdate()
    }
    
    func setData()
    {
        let data1 = JCVDataBean()
        data1.dateString = "15-04-2016"
        data1.value = 350
        data1.state = "AMARILLO"
        
        let data2 = JCVDataBean()
        data2.dateString = "03-05-2016"
        data2.value = 450
        data2.state = "AZUL"
        
        let data3 = JCVDataBean()
        data3.dateString = "03-05-2016"
        data3.value = 550
        data3.state = "ROJO"
        
        let data4 = JCVDataBean()
        data4.dateString = "14-05-2016"
        data4.value = 550
        data4.state = "ROJO"
        
        let data5 = JCVDataBean()
        data5.dateString = "15-05-2016"
        data5.value = 320
        data5.state = "AZUL"
        
        self.dataArray.append(data1)
        self.dataArray.append(data2)
        self.dataArray.append(data3)
        self.dataArray.append(data4)
        self.dataArray.append(data5)
    }
    
    func setData(dataArray: [JCVDataBean])
    {
        self.dataArray = dataArray
        
        self.updateCalendar()
    }
    
    func getMonthName(month: Int) -> String
    {
        var name: String = ""
        
        for i in 0 ... self.monthsArray.count - 1{
            let monthDic : NSDictionary = self.monthsArray[i] as NSDictionary
            let monthId: String = monthDic.object(forKey: "id") as! String
            let monthName: String = monthDic.object(forKey: "name") as! String
            
            if (monthId == String(month))
            {
                name = monthName
            }
        }
        
        
        return name
    }
    
    func getDayName(dayInWeek: Int) -> String
    {
        var name: String = ""
        
        for i in 0 ... self.monthsArray.count - 1{
            let dayDic : NSDictionary = self.monthsArray[i] as NSDictionary
            let dayId: String = dayDic.object(forKey: "id") as! String
            let dayName: String = dayDic.object(forKey: "name") as! String
            
            if (dayId == String(dayInWeek))
            {
                name = dayName
            }
        }
        
        return name
    }
    
    //Actions
    @IBAction func actionBack(sender: AnyObject) {
        
        self.setBackMonth()
    }
    
    @IBAction func actionNext(sender: AnyObject) {
        
        self.setNextMonth()
    }
    
    //Delegates
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        
        if (collectionView == self.daysDetailCV){
            
            return self.numberOfWeeks
        }
        else if (collectionView == self.daysHeaderCV)
        {
            return 1
        }
        else
        {
            return 1
        }
        //row number
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if (collectionView == self.daysDetailCV)
        {
            return 7 //each row have 15 columns
        }
        else if (collectionView == self.daysHeaderCV)
        {
            return 7 //each row have 15 columns
        }
        else
        {
            return 7 //each row have 15 columns
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if (collectionView == self.daysDetailCV){
            
            let cell:JCVDayDetailCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "dayDetailCell", for: indexPath as IndexPath) as! JCVDayDetailCollectionViewCell
            
            let index = ((7 * indexPath.section) + indexPath.row)
            
            let calendarDayBean: JCVCalendarDayBean = self.daysOfMonth[index]
            
            let diaString = String(calendarDayBean.number)
            var totalDouble: Double = 0
            var estadoString = ""
            var valueColor = UIColor()
            
            for alerta in calendarDayBean.data
            {
                if (totalDouble == 0)
                {
                    estadoString = alerta.state
                    valueColor = alerta.valueBackgroundColor
                }
                
                totalDouble += alerta.value
            }
            
            var pagosString = String(calendarDayBean.data.count)
            var totalString = String(Int(totalDouble))
            
             if (pagosString == "0")
            {
                pagosString = ""
            }
            else if (pagosString == "1")
            {
                pagosString = "\(pagosString) pago"
            }
            else
            {
                pagosString = "\(pagosString) pagos"
            }
            
            if (totalString == "0")
            {
                totalString = ""
                
                cell.valueLB.backgroundColor = UIColor.white
            }
            else
            {
                totalString = "S/ \(totalString)"
                
                if (estadoString != "")
                {
                    cell.valueLB.backgroundColor = valueColor
                }
                else
                {
                    cell.valueLB.backgroundColor = UIColor.white
                }
            }
            /*
            if (estadoString == "ROJO")
            {
                cell.valueLB.backgroundColor = UIColor.redColor()
            }
            else if (estadoString == "AZUL")
            {
                cell.valueLB.backgroundColor = UIColor.blueColor()
            }
            else if (estadoString == "AMARILLO")
            {
                cell.valueLB.backgroundColor = UIColor.yellowColor()
            }
            else
            {
                cell.valueLB.backgroundColor = UIColor.clearColor()
            }
            */

            
            if (calendarDayBean.isCurrentMonth)
            {
                cell.dayLB.textColor = UIColor.darkGray
            }
            else
            {
                cell.dayLB.textColor = UIColor.lightGray
            }
            
            cell.dayLB.text = diaString
            cell.detailLB.text = pagosString
            cell.valueLB.text = totalString
            
            let bottomLineView = UIView(frame: CGRect(x: 0, y: cell.bounds.size.height - 1, width: self.bounds.size.width, height: 1))
            bottomLineView.backgroundColor = UIColor.gray
            cell.contentView.addSubview(bottomLineView)
            
            return cell
            
        }else{
            
            let cell: JCVDayHeaderCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "dayHeaderCell", for: indexPath as IndexPath) as! JCVDayHeaderCollectionViewCell
            
            let dia: NSDictionary = self.daysArray[indexPath.row] as NSDictionary
            
            cell.nameLB.text = dia.object(forKey: "short") as? String
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //print("seleccionado1")
        let index = ((7 * indexPath.section) + indexPath.row)
        let day: JCVCalendarDayBean = self.daysOfMonth[index]
        
        //self.didSelectDay(day)
        
        delegateCalendar.didSelectDay(day: day, indexPath: indexPath as NSIndexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let bounds: CGRect = UIScreen.main.bounds
        
        if (collectionView == self.daysDetailCV)
        {
            return CGSize(width: (bounds.width-16)/7, height: 75) //CGSizeMake((bounds.width-16)/7, 75)
        }
        else if (collectionView == self.daysHeaderCV)
        {
            return CGSize(width: (bounds.width-32)/7, height: 30)//CGSizeMake((bounds.width-32)/7, 30)
        }
        else
        {
            return CGSize(width: (bounds.width-16)/7, height: 30)//CGSizeMake((bounds.width-16)/7, 30)
        }
        
    }

}

protocol JCVCalendarViewDelegate {
    
    func didSelectDay(day: JCVCalendarDayBean, indexPath: NSIndexPath)
    
    func didAfterUpdate()
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
}
