//
//  SDTSabioComercial.m
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 12/5/17.
//  Copyright © 2017 Into. All rights reserved.
//
#import "SDTSabioComercial.h"
@implementation SDTSabioComercial
-(id) init{
    self.Termometro = [[TermometroComercialBean alloc] init];
    self.Evaluacion = [[EvaluacionComercial alloc] init];
    self.DescRiesgo = [[NSString alloc] init];
    self.CapacidadPago = [[CapacidadPagoComercial alloc] init];
    return self;
}
@end
