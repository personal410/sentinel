//
//  ProductosSabioComercialBean.h
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 12/5/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductosSabioComercialBean : NSObject

@property (nonatomic,strong) NSString*NombreProducto;
@property (nonatomic,strong) NSString*Resultado;
@property (nonatomic,strong) NSString*ImgProducto;
@property (nonatomic,strong) NSString*ImgEvaluacion;
@property (nonatomic,strong) NSString*ColorEvaluacion;
@property (nonatomic,strong) NSMutableArray* DetalleEvaluacion;

@end
