//
//  SabioFinancieroEntidad.h
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 10/4/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SabioFinancieroEntidad : NSObject

@property (nonatomic,strong) NSString*CodigoWS;
//Obtiene Sabio
@property (nonatomic,strong) NSString*TieneSabio;
@property (nonatomic,strong) NSString*TipoSabio;

//DEUDA SABIO FINANCIERO
@property (nonatomic,strong) NSMutableArray *SDT_InOCSabioFinanciero;
@property (nonatomic,strong) NSString*TDocEnt;
@property (nonatomic,strong) NSString*NDocEnt;
@property (nonatomic,strong) NSString*TCred;
@property (nonatomic,strong) NSString*Saldo;  //CronogramaCreditos
@property (nonatomic,strong) NSString*PorCanc;
@property (nonatomic,strong) NSString*NSaldo;
@property (nonatomic,strong) NSString*MonCanc;

@property (nonatomic,strong) NSString*NombreEntidad;
@property (nonatomic,strong) NSString*DscTCred;

//Lista productos
@property (nonatomic,strong) NSMutableArray *Productos;
@property (nonatomic,strong) NSString*IdProducto;
@property (nonatomic,strong) NSString*NomProducto;
@property (nonatomic,strong) NSString*ImgProducto;

//SABIO FINANCIERO
@property (nonatomic,strong) NSString*TDoc;
@property (nonatomic,strong) NSString*NroDoc;
@property (nonatomic,strong) NSString*Producto;
@property (nonatomic,strong) NSString*TipoDeudor;
@property (nonatomic,strong) NSString*Ingresos;
@property (nonatomic,strong) NSString*MontoPropuesto;
@property (nonatomic,strong) NSString*CuotaMontoPropuesto;
@property (nonatomic,strong) NSString*DeudaEntidad;
@property (nonatomic,strong) NSString*CuotaEntidad;
@property (nonatomic,strong) NSString*TDocCony;
@property (nonatomic,strong) NSString*NDocCony;
@property (nonatomic,strong) NSString*CancelaOC;

@property (nonatomic,strong) NSString*EsValido;
@property (nonatomic,strong) NSString*MensajeRespuesta;

@property (nonatomic,strong) NSMutableArray *Recomendacion;
@property (nonatomic,strong) NSString*Imagen;
@property (nonatomic,strong) SabioFinancieroEntidad* ProductoBean;
@property (nonatomic,strong) NSString*TextoRecomendacion;

@property (nonatomic,strong) NSMutableArray *Termometro;

@property (nonatomic,strong) NSMutableArray *Rangos;

@property (nonatomic,strong) NSString*Inicio;
@property (nonatomic,strong) NSString*Fin;
@property (nonatomic,strong) NSString*Color;

@property (nonatomic,strong) NSString*ColorScore;
@property (nonatomic,strong) NSString*Score;
@property (nonatomic,strong) NSString*Minimo;
@property (nonatomic,strong) NSString*Maximo;
@property (nonatomic,strong) NSString*Riesgo;

@property (nonatomic,strong) NSMutableArray *SESeguimiento;

@property (nonatomic,strong) NSString*NivelRiesgo;
@property (nonatomic,strong) NSString*ColorRiesgo;

@property (nonatomic,strong) NSMutableDictionary *Resultado;

@property (nonatomic,strong) NSString*Indicador;
@property (nonatomic,strong) NSString*ValorEsperado;
@property (nonatomic,strong) NSString*ValorReal;
@property (nonatomic,strong) NSString*Calificacion;

@property (nonatomic,strong) NSMutableDictionary *ProductosColeccion;
//@property (nonatomic,strong) NSString*Nombre;

@property (nonatomic,strong) NSMutableArray *EvaluacionCredicitia;
@property (nonatomic,strong) NSString*Dictamen;
@property (nonatomic,strong) NSString*ColorDictamen;

@property (nonatomic,strong) NSMutableDictionary *DetalleEvaluacion;
@property (nonatomic,strong) NSString*Item;

@property (nonatomic,strong) NSMutableArray *SEOriginacion;
//@property (nonatomic,strong) NSString*NivelRiesgo;
//@property (nonatomic,strong) NSString*ColorRiesgo;
@property (nonatomic,strong) NSString*DscRiesgo;


//@property (nonatomic,strong) NSMutableDictionary *Resultado;
//
//@property (nonatomic,strong) NSString*Indicador;
//@property (nonatomic,strong) NSString*ValorEsperado;
//@property (nonatomic,strong) NSString*ValorReal;
//@property (nonatomic,strong) NSString*Calificacion;

//opcion credito

// ES EL MISMO :NDocCony -> NroDocCony
//@property (nonatomic,strong) NSString*ProdId;
@property (nonatomic,strong) NSMutableArray *SDTOpcionCredito;
@property (nonatomic,strong) NSString*Monto;
@property (nonatomic,strong) NSString*Plazo;
@property (nonatomic,strong) NSString*Cuota;
@property (nonatomic,strong) NSString*CodProp;

@property (nonatomic,strong) NSString*CodOper;

//CRONOGRAMA CREDITOS
@property (nonatomic,strong) NSMutableArray *SDTCronogramaCredito;
@property (nonatomic,strong) NSString*Nro;
//@property (nonatomic,strong) NSString*Saldo;
@property (nonatomic,strong) NSString*Amortizacion;
@property (nonatomic,strong) NSString*Interes;

//CONSULTA CREDITICIA -> CLIENTE, CONYUGE

@property (nonatomic,strong) NSMutableDictionary *ClienteConyuge;

@property (nonatomic,strong) NSMutableArray *ConsultaCrediticia;
@property (nonatomic,strong) NSString*ApePat;
@property (nonatomic,strong) NSString*ApeMat;
@property (nonatomic,strong) NSString*Nombre;
@property (nonatomic,strong) NSString*NombreRazonSocial;
@property (nonatomic,strong) NSString*FechaNac;
@property (nonatomic,strong) NSString*DeudaTotal;
@property (nonatomic,strong) NSString*VarCod;
@property (nonatomic,strong) NSString*SemAct;
@property (nonatomic,strong) NSString*SemPre;
@property (nonatomic,strong) NSString*Sem12m;
@property (nonatomic,strong) NSString*TipoContribuyente;
@property (nonatomic,strong) NSString*EstContribuyente;
@property (nonatomic,strong) NSString*Dependencia;
@property (nonatomic,strong) NSString*InicioAct;
@property (nonatomic,strong) NSString*Condicion;
@property (nonatomic,strong) NSString*Foto;


@property (nonatomic,strong) NSString*codigoCelda;

//Campos CronogramaV2
@property (nonatomic,strong) NSString*CuotaC;
@property (nonatomic,strong) NSString*FechaCuotaInicial;
@property (nonatomic,strong) NSString*MontoPrestamo;
@property (nonatomic,strong) NSString*NumCuotas;
@property (nonatomic,strong) NSString*PeriodoPago;
@property (nonatomic,strong) NSString*TEA;
@property (nonatomic,strong) NSString*TEM;

//Hora y Fecha limite de WS
@property (nonatomic,strong) NSString*ParFlg;
@property (nonatomic,strong) NSString*ParCon;
@property (nonatomic,strong) NSString*error;
@property (nonatomic,strong) NSString*Fecha;
@property (nonatomic,strong) NSString*Hora;
@end













