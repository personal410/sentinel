//
//  SabioComercialPreparado.h
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 12/8/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SabioComercialPreparado : NSObject

//@property (nonatomic,strong) NSMutableArray <RecomendacionSabioBean*>* Recomendacion;
@property (nonatomic,strong) NSMutableDictionary* Termometro;
@property (nonatomic,strong) NSMutableDictionary * SESeguimiento;
@property (nonatomic,strong) NSMutableArray *Productos;

@end
