//
//  SDTSabioFinancieroBean.h
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 11/6/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RecomendacionSabioBean.h"
#import "TermometroSabioBean.h"
#import "SESeguimientoBean.h"
#import "ProductosSabioBean.h"

@interface SDTSabioFinancieroBean : NSObject

@property (nonatomic,strong) NSMutableArray <RecomendacionSabioBean*>* Recomendacion;
@property (nonatomic,strong) NSMutableDictionary* Termometro;
@property (nonatomic,strong) NSMutableDictionary * SESeguimiento;
@property (nonatomic,strong) NSDictionary * OpcionPrestamo;
@property (nonatomic,strong) NSMutableArray *Productos;

@end
