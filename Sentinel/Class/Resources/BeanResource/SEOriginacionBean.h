//
//  SEOriginacionBean.h
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 11/6/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResultadoSabioBean.h"

@interface SEOriginacionBean : NSObject

@property (nonatomic,strong) NSString*NivelRiesgo;
@property (nonatomic,strong) NSString*ColorRiesgo;
@property (nonatomic,strong) NSString*DscRiesgo;
@property (nonatomic,strong) NSMutableArray <ResultadoSabioBean*>* Resultado;

@end
