//
//  EvaluacionCredicitiaBean.h
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 11/6/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DetalleEvaluacionBean.h"


@interface EvaluacionCredicitiaBean : NSObject

@property (nonatomic,strong) NSString*Dictamen;
@property (nonatomic,strong) NSString*ColorDictamen;
@property (nonatomic,strong) NSMutableArray <DetalleEvaluacionBean*> * DetalleEvaluacion;


@end
