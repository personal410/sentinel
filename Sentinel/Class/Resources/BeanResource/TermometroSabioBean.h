//
//  TermometroSabioBean.h
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 11/6/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RangoSabioBean.h"

@interface TermometroSabioBean : NSObject

@property (nonatomic,strong) NSString*ColorScore;
@property (nonatomic,strong) NSString*Score;
@property (nonatomic,strong) NSString*Minimo;
@property (nonatomic,strong) NSString*Maximo;
@property (nonatomic,strong) NSString*Riesgo;
@property (nonatomic,strong) NSMutableArray <RangoSabioBean*> *Rangos;

@end
