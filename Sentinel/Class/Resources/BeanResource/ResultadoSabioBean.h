//
//  ResultadoSabioBean.h
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 11/6/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResultadoSabioBean : NSObject

@property (nonatomic,strong) NSString*Indicador;
@property (nonatomic,strong) NSString*ValorEsperado;
@property (nonatomic,strong) NSString*ValorReal;
@property (nonatomic,strong) NSString*Calificacion;

@end
