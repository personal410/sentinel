//
//  SDT_InOCSabioFinancieroBean.h
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 11/6/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SDT_InOCSabioFinancieroBean : NSObject

@property (nonatomic,strong) NSString*TDocEnt;
@property (nonatomic,strong) NSString*NDocEnt;
@property (nonatomic,strong) NSString*TCred;
@property (nonatomic,strong) NSString*Saldo;
@property (nonatomic,strong) NSString*PorCanc;
@property (nonatomic,strong) NSString*NSaldo;
@property (nonatomic,strong) NSString*MonCanc;

@end
