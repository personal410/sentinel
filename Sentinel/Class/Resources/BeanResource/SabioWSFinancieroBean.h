//
//  SabioWSFinancieroBean.h
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 11/6/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SDT_InOCSabioFinancieroBean.h"
#import "SDTSabioFinancieroBean.h"

@interface SabioWSFinancieroBean : NSObject

@property (nonatomic,strong) NSString*Usuario;
@property (nonatomic,strong) NSString*SesionId;
@property (nonatomic,strong) NSString*Servicio;
@property (nonatomic,strong) NSString*TDoc;
@property (nonatomic,strong) NSString*NroDoc;
@property (nonatomic,strong) NSString*Producto;
@property (nonatomic,strong) NSString*TipoDeudor;
@property (nonatomic,strong) NSString*Ingresos;
@property (nonatomic,strong) NSString*MontoPropuesto;
@property (nonatomic,strong) NSString*CuotaMontoPropuesto;
@property (nonatomic,strong) NSString*DeudaEntidad;
@property (nonatomic,strong) NSString*CuotaEntidad;
@property (nonatomic,strong) NSString*TDocCony;
@property (nonatomic,strong) NSString*NDocCony;
@property (nonatomic,strong) NSString*CancelaOC;
@property (nonatomic,strong) NSMutableArray *SDT_InOCSabioFinanciero;

@property (nonatomic,strong) NSString*EsValido;
@property (nonatomic,strong) NSString*MensajeRespuesta;
@property (nonatomic,strong) NSMutableDictionary *SDTSabioFinanciero;
@property (nonatomic,strong) NSString*CodigoWS;

@end
