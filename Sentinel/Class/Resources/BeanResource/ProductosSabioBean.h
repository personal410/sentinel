//
//  ProductosSabioBean.h
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 11/6/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EvaluacionCredicitiaBean.h"
#import "SEOriginacionBean.h"

@interface ProductosSabioBean : NSObject

@property (nonatomic,strong) NSString*Nombre;
@property (nonatomic,strong) NSMutableDictionary* EvaluacionCredicitia;
@property (nonatomic,strong) NSMutableDictionary* SEOriginacion;

@end
