//
//  SabioComercialEntidad.h
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 12/4/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SDTSabioComercial.h"

@interface SabioComercialEntidad : NSObject

@property (nonatomic,strong) NSString*CTUseCod;
@property (nonatomic,strong) NSString*CTConDeuId;
@property (nonatomic,strong) NSString*CTConDeuDsc;
@property (nonatomic,strong) NSString*CTTDocCPT;
@property (nonatomic,strong) NSString*CTNDocCPT;
@property (nonatomic,strong) NSString*CTNCorr;
@property (nonatomic,strong) NSString*CTPasoReg;

@property (nonatomic,strong) NSString*CTDirecc;
@property (nonatomic,strong) NSString*CTMail;
@property (nonatomic,strong) NSString*CTCelular;
@property (nonatomic,strong) NSString*CTMonDeu;
@property (nonatomic,strong) NSString*CTMonedaDeu;
@property (nonatomic,strong) NSString*CTFecEmi;
@property (nonatomic,strong) NSString*CTTipDocProb;
@property (nonatomic,strong) NSString*CTDocProb;
@property (nonatomic,strong) NSString*CTFecVen;
@property (nonatomic,strong) NSString*CTFecPlLim;
@property (nonatomic,strong) NSString*CTComent;
@property (nonatomic,strong) NSString*CTTipConAce;
//@property (nonatomic,strong) NSString*SesionId;

@property (nonatomic,strong) NSString*UserId;//
@property (nonatomic,strong) NSString*CTSelfieNot;//

@property (nonatomic,strong) NSString*CTNomCPT;
@property (nonatomic,strong) NSString*CTApePatCPT;
@property (nonatomic,strong) NSString*CTApeMatCPT;
@property (nonatomic,strong) NSString*CTNomRazSocCPT;
@property (nonatomic,strong) NSString*CTEstNot;

@property (nonatomic, assign) NSString *CantidadDias;
@property (nonatomic,strong) NSString*tipoDocumentoLetra;

@property (nonatomic,strong) NSDate*fechaEmision;
@property (nonatomic,strong) NSDate*fechaVencimiento;
@property (nonatomic,strong) NSDate*fechaLimite;

@property (nonatomic,strong) NSString*xCTNCorr;
@property (nonatomic,strong) NSString*CodigoWS;

@property (nonatomic,strong) NSString*CTNDocEmp;

@property (nonatomic,strong) NSMutableArray*listaDeudoresConbranza;

@property (nonatomic,strong) NSString*Usuario;
@property (nonatomic,strong) NSString*SesionId;
@property (nonatomic,strong) NSString*Servicio;
@property (nonatomic,strong) NSString*TDoc;
@property (nonatomic,strong) NSString*NroDoc;

@property (nonatomic,strong) NSString*nombreCompleto;
@property (nonatomic,strong) NSString*paterno;
@property (nonatomic,strong) NSString*materno;
@property (nonatomic,strong) NSString*nombres;

@property (nonatomic,strong) NSString *fechaNac;

//Datos Sabio Comercial

@property (nonatomic,strong) NSString * mostrarRiesgo;
@property (nonatomic,strong) NSString * mostrarValorScore;
@property (nonatomic,strong) NSString * mostrarTextoExplicativo;
@property (nonatomic,strong) SDTSabioComercial *SDTSabioComercial;


@end
