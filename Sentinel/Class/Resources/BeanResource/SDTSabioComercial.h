//
//  SDTSabioComercial.h
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 12/5/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TermometroComercialBean.h"
#import "EvaluacionComercial.h"
#import "CapacidadPagoComercial.h"

@interface SDTSabioComercial : NSObject

@property (nonatomic,strong) TermometroComercialBean* Termometro;
@property (nonatomic,strong) EvaluacionComercial * Evaluacion;
@property (nonatomic,strong) NSString * DescRiesgo;
@property (nonatomic,strong) CapacidadPagoComercial * CapacidadPago;

@end
