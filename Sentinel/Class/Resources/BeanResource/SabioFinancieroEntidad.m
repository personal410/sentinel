//
//  SabioFinancieroEntidad.m
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 10/4/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import "SabioFinancieroEntidad.h"

@implementation SabioFinancieroEntidad

-(id) init{
    self.SDT_InOCSabioFinanciero = [[NSMutableArray alloc  ] init];
    self.Productos = [[NSMutableArray alloc  ] init];
    self.SDTOpcionCredito = [[NSMutableArray alloc  ] init];
    self.SDTCronogramaCredito = [[NSMutableArray alloc  ] init];
    self.ConsultaCrediticia = [[NSMutableArray alloc  ] init];
    self.ClienteConyuge = [[NSMutableDictionary alloc  ] init];
    self.Recomendacion = [[NSMutableArray alloc]init];
    return self;
}


@end
