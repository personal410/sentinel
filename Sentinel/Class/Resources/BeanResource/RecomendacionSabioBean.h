//
//  RecomendacionSabioBean.h
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 11/6/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecomendacionSabioBean : NSObject

@property (nonatomic,strong) NSString*Imagen;
@property (nonatomic,strong) NSString*Producto;
@property (nonatomic,strong) NSString*TextoRecomendacion;

@end
