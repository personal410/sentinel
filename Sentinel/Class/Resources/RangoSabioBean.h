//
//  RangoSabioBean.h
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 11/6/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RangoSabioBean : NSObject

@property (nonatomic,strong) NSString*Inicio;
@property (nonatomic,strong) NSString*Fin;
@property (nonatomic,strong) NSString*Color;

@end
