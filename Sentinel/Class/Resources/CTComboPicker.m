//
//  CTComboPicker.m
//  SentinelPersonal
//
//  Created by Juan Alberto Carlos Vera on 6/27/17.
//  Copyright © 2017 Sentinel. All rights reserved.
//
#import "CTComboPicker.h"
@interface CTComboPicker(){
    UIPickerView *mypicker;
    NSArray *comboArray;
    NSInteger *fila;
    UILabel *titleLabel;
    NSString *nombreBoton;
    int valorRow;
    int pickerid;
    UIBarButtonItem *doneBtn;
}
@end
@implementation CTComboPicker
@synthesize delegate;

-(void)setConfig:(NSArray *)lista {
    valorRow = 0;
    comboArray = lista;
    mypicker = [[UIPickerView alloc]init];
    mypicker.dataSource = self;
    mypicker.delegate = self;
    self.ocultoTF.inputView = mypicker;
    UIToolbar *toolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(80.0f , 11.0f, 60.0f, 21.0f)];
    [titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setTextColor:[UIColor colorWithRed:157.0/255.0 green:157.0/255.0 blue:157.0/255.0 alpha:1.0]];
    [titleLabel setText:@"Title"];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"OK" style:UIBarButtonItemStylePlain target:self action:@selector(mostrarSeleccion:)];
    [doneBtn setTintColor:[UIColor orangeColor]];
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    NSArray *itemsP = [[NSArray alloc]initWithObjects: spacer,doneBtn,nil];
    [toolbar setItems:itemsP animated:YES];
    [self.ocultoTF setInputAccessoryView:toolbar];
}

-(void)setId: (int)pid  {
    pickerid = pid;
}
-(int)getSelectedRow{
    return valorRow;
}
-(void)mostrarSeleccion:(id)sender{
    if(nombreBoton == nil || [nombreBoton isEqualToString: @"Seleccione"]) {
        
    }else{
        [self.generalBT setTitle:[NSString stringWithFormat:@"    %@", nombreBoton] forState:normal];
        [self dismissKeyboard];
    }
    [self.delegate miPickerCombo:pickerid didSelectRow:[mypicker selectedRowInComponent:0]];
}

-(void)dismissKeyboard {
    [self.ocultoTF endEditing:YES];
}
-(void)open{
    [self.ocultoTF becomeFirstResponder];
}
#pragma mark - Picker
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return comboArray.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    nombreBoton = comboArray[row];
    return comboArray[row];
}

-(void)selectRow:(UIPickerView *)pickerView :(NSInteger)row inComponent:(NSInteger)component animated:(BOOL)animated{
    if (nombreBoton != nil) {
        [pickerView selectRow:valorRow inComponent:0 animated:YES];
    }
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    nombreBoton = comboArray[row];
    valorRow = (int)row;
    doneBtn.tag = row;
}
@end
