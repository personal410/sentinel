//
//  SFControlTermometro.m
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 11/14/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import "SFControlTermometro.h"
#import "RangoSabioBean.h"

@implementation SFControlTermometro

    NSArray* arregloRGB;

#pragma mark - Métodos generales

-(void)formatoTermometro:(NSMutableDictionary*) SDTSabioFinanciero{
    
    arregloRGB = [[NSArray alloc]init];
    
    NSDictionary *dictTermometro = [[NSDictionary alloc]init];
    dictTermometro = SDTSabioFinanciero.copy;
    
    NSArray <RangoSabioBean*> *arregloRangos = [[NSArray alloc]init];
    arregloRangos = [dictTermometro objectForKey:@"Rangos"];
    
    NSString *score = [dictTermometro objectForKey:@"Score"];
    NSString *colorScore = [dictTermometro objectForKey:@"ColorScore"];
    
    NSArray * arreglo1 = [self llenarRGB:colorScore];
    if ([arreglo1 count] > 0) {
        NSString *red = arreglo1[0];
        NSString *green = arreglo1[1];
        NSString *blue = arreglo1 [2];
        CGFloat Nred = [red floatValue];
        CGFloat Ngreen = [green floatValue];
        CGFloat Nblue = [blue floatValue];
        self.nivelRiesgoLB.textColor = [UIColor colorWithRed:Nred/255.0 green:Ngreen/255.0 blue:Nblue/255.0 alpha:1.0];
    }
    
    self.nivelRiesgoLB.text = [dictTermometro objectForKey:@"Riesgo"];
    self.numeroTermometroLB.text = [NSString stringWithFormat:@"%@",score];
    NSString * Minimo = [NSString stringWithFormat:@"%@",[dictTermometro objectForKey:@"Minimo"]];
    NSString * Maximo = [NSString stringWithFormat:@"%@",[dictTermometro objectForKey:@"Maximo"]];
    int min = [Minimo intValue];
    int max = [Maximo intValue];
    int sco = [score intValue];
    
    [self moverFlecha:min andMaximo:max andScore:sco];
    [self pintarTermometro:min andMaximo:max andScore:sco andRangos:arregloRangos];
}

-(void)pintarTermometro: (int) minimo andMaximo: (int)maximo andScore: (int)score andRangos: (NSArray <RangoSabioBean *>*)  rangos {
    
    _alturaIndiceLC.constant = 15;//69
    _espacioTopIndiceLC.constant = 65;//11
    _espacioBotIndiceLC.constant = 0;
    
    NSString * color;
    
    for (int i = 0; i < rangos.count; i++) {
        NSDictionary *rango = (NSDictionary *)rangos[i];
        int inicio = [[rango objectForKey:@"Inicio"] intValue];
        int final = [[rango objectForKey:@"Fin"] intValue];
        
        if (inicio <= score && score <= final) {
            color = [rango objectForKey:@"Color"];
        }
    }
    
    if (color == nil) {
         color = @"rgb(170,170,170)";
    }
    
    NSArray * arregloColorTermometro = [self llenarRGB:color];
    
    NSString *red = arregloColorTermometro[0];
    NSString *green = arregloColorTermometro[1];
    NSString *blue = arregloColorTermometro [2];
    
    CGFloat Nred = [red floatValue];
    CGFloat Ngreen = [green floatValue];
    CGFloat Nblue = [blue floatValue];
    
    [self.indicadorTermometroVI setTintColor:[UIColor colorWithRed:Nred/255.0 green:Ngreen/255.0 blue:Nblue/255.0 alpha:1.0]];
    [self.indicadorTermometroVI setBackgroundColor:[UIColor colorWithRed:Nred/255.0 green:Ngreen/255.0 blue:Nblue/255.0 alpha:1.0]];
    
    int nuevaFraccion = (maximo - minimo) / 10 ;
    
    if (score <= nuevaFraccion)
    {
        _alturaIndiceLC.constant = 15;
        _espacioTopIndiceLC.constant = 65;
        [self.indicadorTermometroVI updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.indicadorTermometroVI layoutIfNeeded];
        }];
    }
    else if (score <= (nuevaFraccion * 2))
    {
        _alturaIndiceLC.constant = 21;
        _espacioTopIndiceLC.constant = 59;
        [self.indicadorTermometroVI updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.indicadorTermometroVI layoutIfNeeded];
        }];

    }
    else if (score <= (nuevaFraccion * 3))
    {
        _alturaIndiceLC.constant = 27;
        _espacioTopIndiceLC.constant = 53;
        [self.indicadorTermometroVI updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.indicadorTermometroVI layoutIfNeeded];
        }];
    }
    else if (score <= (nuevaFraccion * 4))
    {
        _alturaIndiceLC.constant = 33;
        _espacioTopIndiceLC.constant = 47;
        [self.indicadorTermometroVI updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.indicadorTermometroVI layoutIfNeeded];
        }];
    }
    else if (score <= (nuevaFraccion * 5))
    {
        _alturaIndiceLC.constant = 39;
        _espacioTopIndiceLC.constant = 41;
        [self.indicadorTermometroVI updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.indicadorTermometroVI layoutIfNeeded];
        }];

    }
    else if (score <= (nuevaFraccion * 6))
    {
        _alturaIndiceLC.constant = 45;
        _espacioTopIndiceLC.constant = 35;
        [self.indicadorTermometroVI updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.indicadorTermometroVI layoutIfNeeded];
        }];
    }
    else if (score <= (nuevaFraccion * 7))
    {
        _alturaIndiceLC.constant = 51;
        _espacioTopIndiceLC.constant = 29;
        [self.indicadorTermometroVI updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.indicadorTermometroVI layoutIfNeeded];
        }];
    }
    else if (score <= (nuevaFraccion * 8))
    {
        _alturaIndiceLC.constant = 57;
        _espacioTopIndiceLC.constant = 23;
        [self.indicadorTermometroVI updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.indicadorTermometroVI layoutIfNeeded];
        }];
    }
    else if (score <= (nuevaFraccion * 9))
    {
        _alturaIndiceLC.constant = 63;
        _espacioTopIndiceLC.constant = 17;
        [self.indicadorTermometroVI updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.indicadorTermometroVI layoutIfNeeded];
        }];
    }
    else if (score <= (nuevaFraccion * 10))
    {
        _alturaIndiceLC.constant = 69;
        _espacioTopIndiceLC.constant = 11;
        [self.indicadorTermometroVI updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.indicadorTermometroVI layoutIfNeeded];
        }];
    }
}

-(void)moverFlecha:(int)minimo andMaximo:(int)maximo andScore:(int)score{
    int nuevaFraccion = (maximo - minimo) / 10 ;
    if(score <= nuevaFraccion){
        _topFlechaLC.constant = 64;
        [self.viewFlechaT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewFlechaT layoutIfNeeded];
        }];
        
        _topNumeroLC.constant = 64;
        [self.viewNumeroT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewNumeroT layoutIfNeeded];
        }];
    }else if (score <= (nuevaFraccion * 2)){
        _topFlechaLC.constant = 58;
        [self.viewFlechaT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewFlechaT layoutIfNeeded];
        }];

        _topNumeroLC.constant = 58;
        [self.viewNumeroT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewNumeroT layoutIfNeeded];
        }];

    }
    else if (score <= (nuevaFraccion * 3))
    {
        _topFlechaLC.constant = 52;
        [self.viewFlechaT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewFlechaT layoutIfNeeded];
        }];

        
        _topNumeroLC.constant = 52;
        [self.viewNumeroT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewNumeroT layoutIfNeeded];
        }];
    }
    else if (score <= (nuevaFraccion * 4))
    {
        _topFlechaLC.constant = 46;
        [self.viewFlechaT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewFlechaT layoutIfNeeded];
        }];

        
        _topNumeroLC.constant = 46;
        [self.viewNumeroT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewNumeroT layoutIfNeeded];
        }];
    }
    else if (score <= (nuevaFraccion * 5))
    {
        _topFlechaLC.constant = 40;
        [self.viewFlechaT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewFlechaT layoutIfNeeded];
        }];

        
        _topNumeroLC.constant = 40;
        [self.viewNumeroT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewNumeroT layoutIfNeeded];
        }];

    }
    else if (score <= (nuevaFraccion * 6))
    {
        _topFlechaLC.constant = 34;
        [self.viewFlechaT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewFlechaT layoutIfNeeded];
        }];
        
        _topNumeroLC.constant = 34;
        [self.viewNumeroT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewNumeroT layoutIfNeeded];
        }];

    }
    else if (score <= (nuevaFraccion * 7))
    {
        _topFlechaLC.constant = 28;
        [self.viewFlechaT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewFlechaT layoutIfNeeded];
        }];
        
        _topNumeroLC.constant = 28;
        [self.viewNumeroT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewNumeroT layoutIfNeeded];
        }];

    }
    else if (score <= (nuevaFraccion * 8))
    {
        _topFlechaLC.constant = 22;
        [self.viewFlechaT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewFlechaT layoutIfNeeded];
        }];

        
        _topNumeroLC.constant = 22;
        [self.viewNumeroT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewNumeroT layoutIfNeeded];
        }];

    }
    else if (score <= (nuevaFraccion * 9))
    {
        _topFlechaLC.constant = 16;
        [self.viewFlechaT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewFlechaT layoutIfNeeded];
        }];

        
        _topNumeroLC.constant = 16;
        [self.viewNumeroT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewNumeroT layoutIfNeeded];
        }];

    }
    else if (score <= (nuevaFraccion * 10))
    {
        _topFlechaLC.constant = 10;
        [self.viewFlechaT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewFlechaT layoutIfNeeded];
        }];
        
        _topNumeroLC.constant = 10;
        [self.viewNumeroT updateConstraintsIfNeeded];
        
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.viewNumeroT layoutIfNeeded];
        }];
    }
}

-(NSArray*) llenarRGB : (NSString*)color{
    NSArray *arregloRGB2 = [[NSArray alloc] init];
    if ([color stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
        NSString *haystack = color;
        NSString *prefix = @"rgb(";
        NSString *suffix = @")";
        NSRange prefixRange = [haystack rangeOfString:prefix];
        //NSLog(@"prefixRange: %@", prefixRange);
        NSRange suffixRange = [[haystack substringFromIndex:prefixRange.location+prefixRange.length] rangeOfString:suffix];
        //NSLog(@"suffixRange: %@", suffixRange);
        NSRange needleRange = NSMakeRange(prefixRange.location+prefix.length, suffixRange.location);
        NSString *needle = [haystack substringWithRange:needleRange];
        arregloRGB = [needle componentsSeparatedByString: @","];
        arregloRGB2 = [needle componentsSeparatedByString: @","];
        
    }
    return arregloRGB2;
}

@end
