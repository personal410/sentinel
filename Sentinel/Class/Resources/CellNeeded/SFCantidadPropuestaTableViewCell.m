//
//  SFCantidadPropuestaTableViewCell.m
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 9/18/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import "SFCantidadPropuestaTableViewCell.h"

@implementation SFCantidadPropuestaTableViewCell

{
    double nuevoSaldo;
    double monto;
    double primerSaldo;
    NSString * montoDosDecimal;
    NSString * nuevoSaldoDosDecimal;
    int porcentaje;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.porcentajeTX.delegate = self;
    
    nuevoSaldo = 0.00;
    monto = 0.00;
    montoDosDecimal = @"";
    nuevoSaldoDosDecimal = @"";
    porcentaje = 0;
    
    
    self.celdaView.layer.cornerRadius = 5.0;
    self.celdaView.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.celdaView.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    self.celdaView.layer.shadowRadius = 2.5f;
    self.celdaView.layer.shadowOpacity = 0.2f;
    
    self.porcentajeView.layer.borderColor = [[UIColor grayColor] CGColor];
    self.porcentajeView.layer.borderWidth = 1.5;
    self.porcentajeView.layer.cornerRadius = 5.0;
    self.botonAView.layer.cornerRadius = 5.0;
    self.botonBView.layer.cornerRadius = 5.0;
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)accionNuevoSaldo:(id)sender{
    
    porcentaje = [self.porcentajeTX.text intValue];
    
    NSString * newString = self.porcentajeTX.text;
    
    if ([newString length] >= 4 ){
        [self.porcentajeTX.text stringByReplacingCharactersInRange:NSMakeRange(0, 3) withString:@"0"];
    }
    
    if (porcentaje <= 100) {
        monto = [self.saldoLB.text doubleValue] * porcentaje / 100;
        nuevoSaldo = [self.saldoLB.text doubleValue] - monto;
        
        NSNumberFormatter *formatterSaldo = [[NSNumberFormatter alloc] init];
        [formatterSaldo setMaximumFractionDigits:2];
        [formatterSaldo setRoundingMode: NSNumberFormatterRoundDown];
        
        nuevoSaldoDosDecimal = [formatterSaldo stringFromNumber:[NSNumber numberWithDouble:nuevoSaldo]];
        
        NSNumberFormatter *formatterMonto = [[NSNumberFormatter alloc] init];
        [formatterMonto setMaximumFractionDigits:2];
        [formatterMonto setRoundingMode: NSNumberFormatterRoundDown];
        
        montoDosDecimal = [formatterMonto stringFromNumber:[NSNumber numberWithFloat:monto]];
        
        self.nuevoSaldoLB.text = nuevoSaldoDosDecimal;
        self.montoLB.text = montoDosDecimal;
    }
    else
    {
        [self.porcentajeTX setText:@""];
        self.nuevoSaldoLB.text = self.saldoLB.text;
        self.montoLB.text = @"0";
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSString *newString;
    if (textField == self.porcentajeTX) {
        newString  = [self.porcentajeTX.text stringByReplacingCharactersInRange:range withString:string];
        return !([newString length] > 3);
    }
        
    
    return !([newString length] > 3);
    
}


@end



