//
//  SFCantidadPropuestaTableViewCell.h
//  SentinelDev
//
//  Created by Juan Alberto Carlos Vera on 9/18/17.
//  Copyright © 2017 Into. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SFCantidadPropuestaTableViewCell;

@interface SFCantidadPropuestaTableViewCell : UITableViewCell<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *entidadLB;
@property (weak, nonatomic) IBOutlet UILabel *creditoLB;
@property (weak, nonatomic) IBOutlet UILabel *saldoLB;

@property (weak, nonatomic) IBOutlet UITextField *porcentajeTX;
@property (weak, nonatomic) IBOutlet UILabel *nuevoSaldoLB;
@property (weak, nonatomic) IBOutlet UILabel *montoLB;

@property (weak, nonatomic) IBOutlet UIView *porcentajeView;

@property (weak, nonatomic) IBOutlet UIView *botonAView;
@property (weak, nonatomic) IBOutlet UIView *botonBView;

@property (weak, nonatomic) IBOutlet UIView *celdaView;

@property (strong, nonatomic) IBOutlet UITextField *tipoCreditoTX;
@property (strong, nonatomic) IBOutlet UITextField *tipoDocEntidadTX;
@property (strong, nonatomic) IBOutlet UITextField *numeroDocEntidadTX;

@end
