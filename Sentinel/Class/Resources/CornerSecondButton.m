//
//  CornerSecondButton.m
//  SentinelPersonal
//
//  Created by Juan Alberto Carlos Vera on 6/16/17.
//  Copyright © 2017 Sentinel. All rights reserved.
//

#import "CornerSecondButton.h"

@implementation CornerSecondButton
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [[self layer] setCornerRadius:8.0f];
    [[self layer] setMasksToBounds:YES];
    [[self layer] setBorderWidth:1.0f];
    [[self layer] setBorderColor:[[UIColor grayColor] CGColor]];
    // [[self titleLabel] setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13 ]];
    
}

@end
