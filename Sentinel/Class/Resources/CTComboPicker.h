//
//  CTComboPicker.h
//  SentinelPersonal
//
//  Created by Juan Alberto Carlos Vera on 6/27/17.
//  Copyright © 2017 Sentinel. All rights reserved.
//
#import <UIKit/UIKit.h>
@class CTComboPicker;
@protocol MiPickerComboDelegate <NSObject>
-(void)miPickerCombo:(CTComboPicker *)MiPickerView pickerView:(UIPickerView*)picker didSelectRow : (NSInteger)row inComponent:(NSInteger)component;
-(void)miPickerCombo:(NSInteger)pickerid didSelectRow:(NSInteger)row;
@end

@interface CTComboPicker : UIView <UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *ocultoTF;
@property (weak, nonatomic) IBOutlet UIButton *generalBT;
@property (nonatomic, weak) id <MiPickerComboDelegate> delegate;
-(void)setConfig:(NSArray *)lista;
-(void)setId:(int)pid;
-(void)open;
-(int)getSelectedRow;
@end
