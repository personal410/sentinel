//
//  Config.swift
//  Sentinel
//
//  Created by Victor Salazar on 20/6/19.
//  Copyright © 2019 sentinelperu. All rights reserved.
//
class Config{
    var config = ""
    init(){
        if let dicInfo = Bundle.main.infoDictionary, let conf = dicInfo["Configuration"] as? String {
            let path = Bundle.main.path(forResource: "Configuration", ofType: "plist")
            let dic = NSDictionary(contentsOfFile: path!)
            for (k, v) in dic! {
                if let k = k as? String, k == conf {
                    if let value = v as? [String: Any]{
                        config = value["Config"] as? String ?? ""
                    }
                    break
                }
            }
        }
    }
}
