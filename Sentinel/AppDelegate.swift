//
//  AppDelegate.swift
//  Sentinel
//
//  Created by Juan Alberto Carlos Vera on 7/2/18.
//  Copyright © 2018 sentinelperu. All rights reserved.
//
import CoreData
import GoogleMaps
import Firebase
import UserNotifications
@UIApplicationMain class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    var window:UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UserGlobalData.sharedInstance.userGlobal = UserClass(userbean: [:])
        UserGlobalData.sharedInstance.misServiciosGlobal = MisServiciosClass(serviciobean: [:])
        UserGlobalData.sharedInstance.alertaGlobal = AlertaClass(userbean: [:])
        UserGlobalData.sharedInstance.terceroGlobal = TerceroClass(userbean: [:])
        
        UITabBar.appearance().layer.borderWidth = 0.0
        UITabBar.appearance().clipsToBounds = true
        
        GMSServices.provideAPIKey("AIzaSyAUGWBO_z6Lx6xtiu0QfShtb-JuAnel87M")
        
        //GoogleA
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions:UNAuthorizationOptions = [.alert, .sound, .badge]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, _) in }
        }else{
            let settings:UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .sound, .badge], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        
        var userInfo : NSDictionary?
        if launchOptions != nil {
            userInfo = launchOptions![UIApplicationLaunchOptionsKey.remoteNotification] as? NSDictionary
        }
        if userInfo != nil {
            let ls_data = userInfo!["aps"] as? [AnyHashable: Any]
            let aps_tipo = ls_data?["data"] as? String
            let userDefaults = UserDefaults.standard
            userDefaults.set(aps_tipo, forKey: "aps_tipo")
            userDefaults.synchronize()
        } else {
            let userDefaults = UserDefaults.standard
            userDefaults.set("", forKey: "aps_tipo")
            userDefaults.synchronize()
        }
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        if !launchedBefore {
            UserDefaults.standard.set(true, forKey: "launchedBefore")
            UserDefaults.standard.synchronize()
            UserDefaults.standard.set("", forKey: "userName")
            UserDefaults.standard.set("", forKey: "password")
            UserDefaults.standard.synchronize()
        }
        return true
    }
    func applicationWillResignActive(_ application:UIApplication){}
    func applicationDidEnterBackground(_ application:UIApplication){}
    func applicationWillEnterForeground(_ application:UIApplication){}
    func applicationDidBecomeActive(_ application:UIApplication){}
    func applicationWillTerminate(_ application:UIApplication){
        UserGlobalData.sharedInstance.userGlobal = UserClass(userbean: [:])
        UserGlobalData.sharedInstance.misServiciosGlobal = MisServiciosClass(serviciobean: [:])
        UserGlobalData.sharedInstance.alertaGlobal = AlertaClass(userbean: [:])
        UserGlobalData.sharedInstance.terceroGlobal = TerceroClass(userbean: [:])
    }
    func application(_ application:UIApplication, didReceiveRemoteNotification userInfo:[AnyHashable:Any]){
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        }else{
            UIApplication.shared.cancelAllLocalNotifications()
        }
    }
    func application(_ application:UIApplication, didReceiveRemoteNotification userInfo:[AnyHashable : Any], fetchCompletionHandler completionHandler:@escaping (UIBackgroundFetchResult) -> Void) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        }else{
            UIApplication.shared.cancelAllLocalNotifications()
        }
        if let tipo = userInfo["tipo"] as? NSString {
            NotificationHUD.showNotificationFlow(with: tipo.integerValue)
        }
        completionHandler(.noData)
    }
    func userNotificationCenter(_ center:UNUserNotificationCenter, willPresent notification:UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if let userInfo = notification.request.content.userInfo as? [String: Any], let ls_data = userInfo["aps"] as? [String: Any], let aps_mensaje = ls_data["alert"] as? [String: Any], let title = aps_mensaje["title"] as? String, let text = aps_mensaje["body"] as? String {
            let tipo = userInfo["tipo"] as? NSString ?? "0"
            NotificationHUD.showNotificionHUDView(with: title, and: text, on: tipo.integerValue)
        }
        completionHandler(.alert)
    }
}
