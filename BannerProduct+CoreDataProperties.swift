//
//  BannerProduct+CoreDataProperties.swift
//  
//
//  Created by Juan Alberto Carlos Vera on 7/10/18.
//
//

import Foundation
import CoreData


extension BannerProduct {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BannerProduct> {
        return NSFetchRequest<BannerProduct>(entityName: "BannerProduct")
    }

    @NSManaged public var image: String?
    @NSManaged public var tittle: String?
    @NSManaged public var descrip: String?

}
